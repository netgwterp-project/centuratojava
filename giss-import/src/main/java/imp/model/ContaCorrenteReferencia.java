package imp.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
	@NamedQuery(name = "getContaCorrenteExercicio", query = "from ContaCorrenteReferencia where inscricao = :inscricao and anoReferencia = :anoReferencia and dataParserNotas is null order by referencia "),
	})
public class ContaCorrenteReferencia extends BaseEntity {

	private static final long serialVersionUID = -2419425872674799894L;

	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long               id;
	
	private String inscricao;
	
	private Integer referencia;
	private Integer anoReferencia;
	private String urlDetalhe;
	private BigDecimal totalGerado;
	private BigDecimal compensado;
	private BigDecimal restituido;
	private BigDecimal totalRecolhido;
	private BigDecimal impostoPago;
	private BigDecimal totalAberto;
	private String urlGuias;
	@Temporal(TemporalType.TIMESTAMP)
    private Date              dataParserGuias;
	@Temporal(TemporalType.TIMESTAMP)
    private Date              dataParserNotas;


	private String tipoEscrituracao;


	private Date dataEscrituracao;
	
	
	public String getTipoEscrituracao() {
		return tipoEscrituracao;
	}
	public Date getDataParserGuias() {
		return dataParserGuias;
	}
	public void setDataParserGuias(Date dataParserGuias) {
		this.dataParserGuias = dataParserGuias;
	}
	public Date getDataParserNotas() {
		return dataParserNotas;
	}
	public void setDataParserNotas(Date dataParserNotas) {
		this.dataParserNotas = dataParserNotas;
	}
	public String getUrlDetalhe() {
		return urlDetalhe;
	}
	public void setUrlDetalhe(String urlDetalhe) {
		this.urlDetalhe = urlDetalhe;
	}
	public BigDecimal getTotalGerado() {
		return totalGerado;
	}
	public void setTotalGerado(BigDecimal totalGerado) {
		this.totalGerado = totalGerado;
	}
	public BigDecimal getCompensado() {
		return compensado;
	}
	public void setCompensado(BigDecimal compensado) {
		this.compensado = compensado;
	}
	public BigDecimal getRestituido() {
		return restituido;
	}
	public void setRestituido(BigDecimal restituido) {
		this.restituido = restituido;
	}
	public BigDecimal getTotalRecolhido() {
		return totalRecolhido;
	}
	public void setTotalRecolhido(BigDecimal totalRecolhido) {
		this.totalRecolhido = totalRecolhido;
	}
	public BigDecimal getImpostoPago() {
		return impostoPago;
	}
	public void setImpostoPago(BigDecimal impostoPago) {
		this.impostoPago = impostoPago;
	}
	public BigDecimal getTotalAberto() {
		return totalAberto;
	}
	public void setTotalAberto(BigDecimal totalAberto) {
		this.totalAberto = totalAberto;
	}
	public String getUrlGuias() {
		return urlGuias;
	}
	public void setUrlGuias(String urlGuias) {
		this.urlGuias = urlGuias;
	}
	public Integer getReferencia() {
		return referencia;
	}
	public void setReferencia(Integer referencia) {
		this.referencia = referencia;
	}
	public Integer getAnoReferencia() {
		return anoReferencia;
	}
	public void setAnoReferencia(Integer anoReferencia) {
		this.anoReferencia = anoReferencia;
	}
	public String getInscricao() {
		return inscricao;
	}
	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ContaCorrenteReferencia [inscricao=");
		builder.append(inscricao);
		builder.append(", referencia=");
		builder.append(referencia);
		builder.append(", anoReferencia=");
		builder.append(anoReferencia);
		builder.append(", totalGerado=");
		builder.append(totalGerado);
		builder.append(", compensado=");
		builder.append(compensado);
		builder.append(", restituido=");
		builder.append(restituido);
		builder.append(", totalRecolhido=");
		builder.append(totalRecolhido);
		builder.append(", impostoPago=");
		builder.append(impostoPago);
		builder.append(", totalAberto=");
		builder.append(totalAberto);
		builder.append("]");
		return builder.toString();
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	public void setTipoEscrituracao(String tipoEscrituracao) {
		this.tipoEscrituracao = tipoEscrituracao;
		
	}
	public void setDataEscrituracao(Date dataEscrituracao) {
		this.dataEscrituracao = dataEscrituracao;
		
	}
	public Date getDataEscrituracao() {
		return dataEscrituracao;
	}

	
	
	
}
