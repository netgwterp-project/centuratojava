package imp.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {


	private static final long serialVersionUID = 6949176157933262624L;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BaseEntity other = (BaseEntity) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        } 
        return true;
    }

    public abstract Long getId();


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + (getId() == null ? 0 : getId().hashCode());
        return result;
    }

    @Transient
    public boolean isNew() {
        return getId() == null;
    }

    public abstract void setId(Long id);

}