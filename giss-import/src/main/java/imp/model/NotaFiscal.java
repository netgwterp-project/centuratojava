package imp.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
	@NamedQuery(name = "getNotasExercicio", query = "from NotaFiscal where inscricao = :inscricao and anoReferencia = :anoReferencia and referencia = :referencia order by numero "),
	})
public class NotaFiscal extends BaseEntity {


	
	private static final long serialVersionUID = -40893214904716789L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long               id;
	
	private String inscricao;
	private Integer referencia;
	private Integer anoReferencia;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEmissao;
	private Date dataCompetencia;
	private String numero;
	private String serie;
	private String urlDetalhe;
	private BigDecimal valorNota;
	private BigDecimal baseCalculo;
	private BigDecimal aliquota;
	private BigDecimal imposto;
	private String atividade;
	private String status;
	@Column(length=1000)
	private String tomador;
	private String escrituracao;

	

	public Date getDataCompetencia() {
		return dataCompetencia;
	}
	public void setDataCompetencia(Date dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}
	public BigDecimal getBaseCalculo() {
		return baseCalculo;
	}
	public void setBaseCalculo(BigDecimal baseCalculo) {
		this.baseCalculo = baseCalculo;
	}
	public BigDecimal getAliquota() {
		return aliquota;
	}
	public void setAliquota(BigDecimal aliquota) {
		this.aliquota = aliquota;
	}
	public BigDecimal getImposto() {
		return imposto;
	}
	public void setImposto(BigDecimal imposto) {
		this.imposto = imposto;
	}
	public Integer getReferencia() {
		return referencia;
	}
	public void setReferencia(Integer referencia) {
		this.referencia = referencia;
	}
	public Integer getAnoReferencia() {
		return anoReferencia;
	}
	public void setAnoReferencia(Integer anoReferencia) {
		this.anoReferencia = anoReferencia;
	}
	public String getInscricao() {
		return inscricao;
	}
	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}
	public Date getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getUrlDetalhe() {
		return urlDetalhe;
	}
	public void setUrlDetalhe(String urlDetalhe) {
		this.urlDetalhe = urlDetalhe;
	}
	public BigDecimal getValorNota() {
		return valorNota;
	}
	public void setValorNota(BigDecimal valorNota) {
		this.valorNota = valorNota;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTomador() {
		return tomador;
	}
	public void setTomador(String tomador) {
		this.tomador = tomador;
	}
	public String getEscrituracao() {
		return escrituracao;
	}
	public void setEscrituracao(String escrituracao) {
		this.escrituracao = escrituracao;
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	public String getAtividade() {
		return atividade;
	}
	public void setAtividade(String atividade) {
		this.atividade = atividade;
	}

	
	
	
}
