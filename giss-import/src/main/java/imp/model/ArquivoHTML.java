package imp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
	@NamedQuery(name = "getArquivoHTML", query = "from ArquivoHTML where tagKey = :tagKey"),
	})
public class ArquivoHTML extends BaseEntity {



	private static final long serialVersionUID = -5153620786417193718L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long               id;
	
	@Column(length=1000)
	private String url;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRequest;
	private String tagKey;
	@Column(columnDefinition="MediumText")
	private String htmlText;

	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getDataRequest() {
		return dataRequest;
	}
	public void setDataRequest(Date dataRequest) {
		this.dataRequest = dataRequest;
	}
	public String getTagKey() {
		return tagKey;
	}
	public void setTagKey(String tagKey) {
		this.tagKey = tagKey;
	}
	public String getHtmlText() {
		return htmlText;
	}
	public void setHtmlText(String htmlText) {
		this.htmlText = htmlText;
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
