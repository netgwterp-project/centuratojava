package imp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedNativeQuery(name = "getPrestador", query = "select * from Mobiliario a where a.regime like 'Auto Lan%' and a.dataEncerramento is null and "
		+ " a.inscricao in ( select b.inscricao from ContaCorrenteReferencia b where b.dataParserNotas is null and b.anoReferencia = 2017 ) order by a.inscricao ", resultClass = Mobiliario.class)
@NamedQueries({
		/*@NamedQuery(name = "getPrestador2", query = "from Mobiliario where regime like 'Auto Lan%' and dataEncerramento is null and "
				+ "inscricao in ( select inscricao ContaCorrenteReferencia where dataParserNotas is null ) order by inscricao desc"),*/
		@NamedQuery(name = "getMobInscricao", query = "from Mobiliario where inscricao = :inscricao or inscricaocad = :inscricaocad ") })
public class Mobiliario extends BaseEntity {

	private static final long	serialVersionUID	= 1448117586781462697L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long				id;

	private String				inscricao;
	private String				inscricaocad;
	private String				nome;
	private String				CNPJCPF;
	private String				logradouro;
	private String				numero;
	private String				complemento;
	private String				bairro;
	private String				cidade;
	private String				estado;
	private String				CEP;
	private String				telefone;
	private String				ramal;
	private String				fax;
	@Temporal(TemporalType.TIMESTAMP)
	private Date				dataAbertura;
	@Temporal(TemporalType.TIMESTAMP)
	private Date				dataEncerramento;
	@Temporal(TemporalType.TIMESTAMP)
	private Date				dataInclusaoGISS;
	private String				email;
	@Column(length = 65535, columnDefinition = "Text")
	private String				atividades;
	private String				regime;
	private Long				idTomador;
	@Temporal(TemporalType.TIMESTAMP)
	private Date				dataParserCadastro;
	@Temporal(TemporalType.TIMESTAMP)
	private Date				dataParserCC2017;
	@Temporal(TemporalType.TIMESTAMP)
	private Date				dataParserCC2016;

	public String getAtividades() {
		return atividades;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCEP() {
		return CEP;
	}

	public String getCidade() {
		return cidade;
	}

	public String getCNPJCPF() {
		return CNPJCPF;
	}

	public String getComplemento() {
		return complemento;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public Date getDataInclusaoGISS() {
		return dataInclusaoGISS;
	}

	public Date getDataParserCadastro() {
		return dataParserCadastro;
	}

	public Date getDataParserCC2016() {
		return dataParserCC2016;
	}

	public Date getDataParserCC2017() {
		return dataParserCC2017;
	}

	public String getEmail() {
		return email;
	}

	public String getEstado() {
		return estado;
	}

	public String getFax() {
		return fax;
	}

	@Override
	public Long getId() {
		return id;
	}

	public Long getIdTomador() {
		return idTomador;
	}

	public String getInscricao() {
		return inscricao;
	}

	public String getInscricaocad() {
		return inscricaocad;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getNome() {
		return nome;
	}

	public String getNumero() {
		return numero;
	}

	public String getRamal() {
		return ramal;
	}

	public String getRegime() {
		return regime;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setAtividades(String atividades) {
		this.atividades = atividades;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public void setCNPJCPF(String cNPJCPF) {
		CNPJCPF = cNPJCPF;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public void setDataEncerramento(Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public void setDataInclusaoGISS(Date dataInclusaoGISS) {
		this.dataInclusaoGISS = dataInclusaoGISS;
	}

	public void setDataParserCadastro(Date dataParserCadastro) {
		this.dataParserCadastro = dataParserCadastro;
	}

	public void setDataParserCC2016(Date dataParserCC2016) {
		this.dataParserCC2016 = dataParserCC2016;
	}

	public void setDataParserCC2017(Date dataParserCC2017) {
		this.dataParserCC2017 = dataParserCC2017;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setIdTomador(Long idTomador) {
		this.idTomador = idTomador;
	}

	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}

	public void setInscricaocad(String inscricaocad) {
		this.inscricaocad = inscricaocad;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public void setRegime(String regime) {
		this.regime = regime;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\"");
		builder.append(inscricao);
		builder.append("\";\"");
		builder.append(inscricaocad);
		builder.append("\";\"");
		builder.append(nome);
		builder.append("\";\"");
		builder.append(CNPJCPF);
		builder.append("\";\"");
		builder.append(logradouro);
		builder.append("\";\"");
		builder.append(numero);
		builder.append("\";\"");
		builder.append(complemento);
		builder.append("\";\"");
		builder.append(bairro);
		builder.append("\";\"");
		builder.append(cidade);
		builder.append("\";\"");
		builder.append(estado);
		builder.append("\";\"");
		builder.append(CEP);
		builder.append("\";\"");
		builder.append(telefone);
		builder.append("\";\"");
		builder.append(ramal);
		builder.append("\";\"");
		builder.append(fax);
		builder.append("\";\"");
		builder.append(dataAbertura);
		builder.append("\";\"");
		builder.append(dataEncerramento);
		builder.append("\";\"");
		builder.append(dataInclusaoGISS);
		builder.append("\";\"");
		builder.append(email);
		builder.append("\";\"");
		builder.append(atividades);
		builder.append("\";\"");
		builder.append(regime);
		builder.append("\"");
		return builder.toString();
	}

}
