package imp.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import imp.db.CRUDService;
import imp.db.CRUDServiceImpl;
import imp.model.ArquivoHTML;
import imp.model.ContaCorrenteReferencia;
import imp.model.Guia;
import imp.model.Mobiliario;
import imp.model.NotaFiscal;

public class ParseUtils {

	public static CRUDService crudServiceImpl = new CRUDServiceImpl();

	public static String getText(Element row) {
		String text = row.text();

		Elements select = row.select("a[href]");
		for (int i = 0; i < select.size(); i++) {
			Element first = select.get(i);
			text = text + "#$#" + getText(first.attr("href"));
			if (first.hasAttr("onclick")) {
				text = text + "#$#" + getText(first.attr("onclick"));
			}
		}

		select = row.select("img[title]");
		for (int i = 0; i < select.size(); i++) {
			Element first = select.get(i);
			text = text + "#$#" + getText(first.attr("title"));
		}
		select = row.select("input[value]");
		for (int i = 0; i < select.size(); i++) {
			Element first = select.get(i);
			text = text + "#$#" + getText(first.attr("value"));
		}
		select = row.select("span[title]");
		for (int i = 0; i < select.size(); i++) {
			Element first = select.get(i);
			text = text + "#$#" + first.attr("title");
			if (first.hasClass("tooltip2") || first.hasClass("tooltip")) {
				text = text + "#$#" + getText(first.attr("title"));
			}
		}
		if (row.tagName().equals("td") && row.hasAttr("id") && row.attr("id").startsWith("parcela_")) {
			text = text + "#$#" + getText(row.attr("id"));
		}
		return text;
	}

	public static String getText(String text) {
		return getText(Jsoup.parseBodyFragment(text));

	}

	public static List<Document> processaIFrame(String urlBase, Document document) throws IOException {
		List<Document> iframeDoc = new ArrayList<>();
		Elements es = document.select("iframe[src]");
		for (Element e : es) {
			String src = e.getElementsByTag("iframe").attr("src");
			iframeDoc.add(Jsoup.connect(urlBase + src).get());
		}
		return iframeDoc;
	}

	public static String normalizeURL(URL context, String text) {
		URL uri;
		try {
			uri = new URL(context, text);
			return uri.toString();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return text;

	}

	public static Document readDocumentFromString(String html) {
		if(html==null) return null;
		return Jsoup.parse(html);
	}

	public static Document readDocumentFromURL(String url, Map<String, String> heads, String tagKey) {
		try {
			Document document = null;
			ArquivoHTML arquivoHTML = getArquivo(tagKey);
			if(arquivoHTML.isNew()){
				System.out.println("REQUEST");
				Connection connect = Jsoup.connect(url);
				connect.ignoreHttpErrors(true).timeout(30000);
				Set<String> keySet = heads.keySet();
				for (String key : keySet) {
					connect.header(key, heads.get(key));
				}
				document = connect.get();
				int i = 0;
				while(document.html().length()<100 && i < 3){
					document = connect.get();
					i++;
				}
				if(document.html().length()<100) {
					System.out.println("NO REQUEST");
					return null;
				}
				arquivoHTML.setHtmlText(document.html());
				arquivoHTML.setDataRequest(new Date());
				arquivoHTML.setTagKey(tagKey);
				arquivoHTML.setUrl(url);
				crudServiceImpl.save(arquivoHTML);
				
			}else{
				document = Jsoup.parse(arquivoHTML.getHtmlText());
				System.out.println("USANDO CACHE");
			}
			return document;
			
		} catch (Exception e) {
			System.out.println("NO REQUEST" + e.getMessage());
			return null;
		}

	}
	
	public static ContaCorrenteReferencia getContaCorrenteReferencia(List<ContaCorrenteReferencia> contas, String inscricao, String exercicio, String mes){
		Integer ano = StringUtils.parseInteger(exercicio);
		Integer ref = StringUtils.parseInteger(mes);
		for (ContaCorrenteReferencia cc : contas) {
			if(cc.getAnoReferencia().equals(ano) && cc.getReferencia().equals(ref))
				return cc;
		}
		ContaCorrenteReferencia cc = new ContaCorrenteReferencia();
		cc.setInscricao(inscricao);
		cc.setAnoReferencia(ano);
		cc.setReferencia(ref);
		if(ref>0)
			contas.add(cc);
		return cc;
	}
	
	public static NotaFiscal getNotasReferencia(List<NotaFiscal> contas, String inscricao, String numero){
		for (NotaFiscal cc : contas) {
			if(cc.getNumero().equals(numero))
				return cc;
		}
		NotaFiscal cc = new NotaFiscal();
		cc.setInscricao(inscricao);
		cc.setNumero(numero);
		contas.add(cc);
		return cc;
	}
	
	public static Guia getGuiaReferencia(List<Guia> contas, String inscricao, String numero){
		for (Guia cc : contas) {
			if(cc.getNumero().equals(numero))
				return cc;
		}
		Guia cc = new Guia();
		cc.setInscricao(inscricao);
		cc.setNumero(numero);
		contas.add(cc);
		return cc;
	}
	
	public static ArquivoHTML getArquivo(String tagKey){
		ArquivoHTML arquivoHTML = crudServiceImpl.getArquivoHTML(tagKey);
		if(arquivoHTML!=null)
			return arquivoHTML;
		else
			return new ArquivoHTML();
	}
	
	public static Mobiliario getMobiliario(String inscricao ){
		Mobiliario mobiliario = crudServiceImpl .getMobiliarioInscricao(inscricao);
		if(mobiliario!=null)
			return mobiliario;
		else
			return new Mobiliario();
	}

	public static Document readDocumentFromFile(File file, String tagKey) {
		if(file.exists()){
			try {
				Document document = null;
				ArquivoHTML arquivoHTML = getArquivo(tagKey);
				if(arquivoHTML.isNew()){
					String html = new String(FileUtil.getInstance().read(file));
					document = Jsoup.parse(html);
					arquivoHTML.setHtmlText(html);
					arquivoHTML.setDataRequest(new Date());
					arquivoHTML.setTagKey(tagKey);
					arquivoHTML.setUrl(file.getAbsolutePath());
					crudServiceImpl.save(arquivoHTML);
				}else{
					document = Jsoup.parse(arquivoHTML.getHtmlText());
				}
				return document;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
