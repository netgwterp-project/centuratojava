package imp.util;

import org.apache.commons.beanutils.ContextClassLoaderLocal;

public final class BeanUtil {

	private static final ContextClassLoaderLocal<BeanUtil> beansByClassLoader = new ContextClassLoaderLocal<BeanUtil>() {

		@Override
		protected BeanUtil initialValue() {
			return new BeanUtil();
		}

	};

	public synchronized static BeanUtil getInstance() {
		return beansByClassLoader.get();
	}

	public synchronized static void setInstance(BeanUtil newInstance) {
		beansByClassLoader.set(newInstance);
	}

	//private Hibernate3BeanReplicator replicator;

	private BeanUtil() {
		//replicator = new Hibernate3BeanReplicator();
	}

	public void assertNotEmpty(Object... objects) throws Exception {
		if ((objects.length % 2) == 0) {
			new Exception("objects is invalide");
		}
		for (int i = 0; i < objects.length; i = i + 2) {
			if ((objects[i + 1] == null) || ((objects[i + 1] instanceof String) && objects[i + 1].equals(""))) {
				throw new Exception("object " + objects[i + 1] + "is invalide");
			}
		}

	}

	public void assertNotNull(Object... objects) throws Exception {
		if ((objects.length % 2) == 0) {
			new Exception("objects is invalide");
		}
		for (int i = 0; i < objects.length; i = i + 2) {
			if ((objects[i + 1] == null)) {
				throw new Exception("object " + objects[i + 1] + "is invalide");
			}
		}

	}

	public <T> T copyToJavaPOJO(T entity) {
		return entity;
		//return replicator.deepCopy(entity);
	}

	@SuppressWarnings("unchecked")
	public <T> T newBean(String clazzName) {
		try {
			Class<?> class1 = Class.forName(clazzName);
			Object newInstance = class1.newInstance();
			return (T) newInstance;
		} catch (Exception e) {
			return null;
		}
	}

}
