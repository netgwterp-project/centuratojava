package imp.util;

import java.util.Date;

public class DateUtil {

	public static Integer getDiferencaMinutos(Date inicio, Date fim){
		if (inicio != null && fim != null){
			Long dif = new Long(( fim.getTime() - inicio.getTime() )/ (1000*60*60));
			return dif.intValue();
		}
		return 0;
	}

	@SuppressWarnings("deprecation")
	public static Date newDate(String exercicio, String mes, String dia) {
		// TODO Auto-generated method stub
		return new Date(StringUtils.parseInteger(exercicio)-1900, StringUtils.parseInteger(mes)-1, StringUtils.parseInteger(dia));
	}
}
