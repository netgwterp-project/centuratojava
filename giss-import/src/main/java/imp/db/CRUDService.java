package imp.db;

import java.util.List;

import javax.persistence.EntityManager;

import imp.model.ArquivoHTML;
import imp.model.BaseEntity;
import imp.model.ContaCorrenteReferencia;
import imp.model.Guia;
import imp.model.Mobiliario;
import imp.model.NotaFiscal;

public interface CRUDService {

    public void close();

    public void commitTransaction();

    public <T extends BaseEntity> T get(Class<T> clazz, Integer id);


    public EntityManager getEntityManager();

    public <T extends BaseEntity> List<T> getList(Class<T> clazz);

    public <T extends BaseEntity> List<T> getList(Class<T> clazz, String filter);

    public <T extends BaseEntity> List<T> getListLike(Class<T> clazz, String param, String value);

    public <T extends Object> List<T> getListNativeQuery(Class<T> clazz, String query);

    public void remove(BaseEntity entity);

    public <T extends BaseEntity> List<T> save(List<T> listEntity);

    public <T extends BaseEntity> T save(T entity);

    public <T extends BaseEntity> T saveLote(T entity);

    public void startTransaction();

    public <T extends BaseEntity> List<T> saveLote(List<T> listEntity);

    public void roolBackTransaction();

    public List<Mobiliario> getListPrestrador();

    public Mobiliario getMobiliarioInscricao(String inscricao);

    public ArquivoHTML getArquivoHTML(String tagKey);

    public List<Object[]> getListNativeQuery(String queryStr);

    public List<ContaCorrenteReferencia> getContaCorrenteExercicio(String inscricao, String exercicio);

    public List<NotaFiscal> getNotasExercicio(String inscricao, String exercicio, String mes);

    public List<Guia> getGuiasExercicio(String inscricao, String exercicio, String mes);
}
