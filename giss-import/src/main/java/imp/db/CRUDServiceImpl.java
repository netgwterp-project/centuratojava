package imp.db;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import imp.model.ArquivoHTML;
import imp.model.BaseEntity;
import imp.model.ContaCorrenteReferencia;
import imp.model.Guia;
import imp.model.Mobiliario;
import imp.model.NotaFiscal;
import imp.util.BeanUtil;
import imp.util.StringUtils;

public final class CRUDServiceImpl implements CRUDService {

	private static Logger logger = Logger.getLogger(CRUDServiceImpl.class.getName());

	private static EntityManagerFactory emf;
	private static EntityManager entityManager;

	private void closeManagerPer() {
		if (entityManager != null) {
			entityManager.close();
			entityManager = null;
		}
		if (emf != null) {
			emf.close();
			emf = null;
		}
	}

	public CRUDServiceImpl() {
		getManager();
	}

	private EntityManager getManager() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("gissonline");
		}
		if (entityManager == null) {
			entityManager = emf.createEntityManager();
		}
		return entityManager;
	}

	@Override
	public void close() {
		closeManagerPer();

	}

	@Override
	public void commitTransaction() {
		getManager().getTransaction().commit();
	}

	@Override
	public <T extends BaseEntity> T get(Class<T> clazz, Integer id) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			T pojo = manager.find(clazz, id);
			pojo = BeanUtil.getInstance().copyToJavaPOJO(pojo);
			manager.getTransaction().commit();
			return pojo;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@Override
	public EntityManager getEntityManager() {
		return getManager();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends BaseEntity> List<T> getList(Class<T> clazz) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createQuery("from " + clazz.getSimpleName());
			List<T> pojo = query.getResultList();
			pojo = BeanUtil.getInstance().copyToJavaPOJO(pojo);
			manager.getTransaction().commit();
			return pojo;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends BaseEntity> List<T> getList(Class<T> clazz, String filter) {
		EntityManager manager = getManager();

		try {
			manager.getTransaction().begin();
			Query query = manager.createQuery("from " + clazz.getSimpleName() + " where " + filter);
			List<T> pojo = query.getResultList();
			pojo = BeanUtil.getInstance().copyToJavaPOJO(pojo);
			manager.getTransaction().commit();
			return pojo;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Mobiliario> getListPrestrador() {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNamedQuery("getPrestador");
			List<Mobiliario> pojo = query.getResultList();
			pojo = BeanUtil.getInstance().copyToJavaPOJO(pojo);
			manager.getTransaction().commit();
			return pojo;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@Override
	public Mobiliario getMobiliarioInscricao(String inscricao) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNamedQuery("getMobInscricao");
			query.setParameter("inscricao", inscricao);
			query.setParameter("inscricaocad", inscricao);
			@SuppressWarnings("unchecked")
			List<Mobiliario> list = query.getResultList();
			if (list.size() >= 1) {
				Mobiliario result = list.get(0);
				result = BeanUtil.getInstance().copyToJavaPOJO(result);
				manager.getTransaction().commit();
				return result;
			}

			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
		return null;
	}

	@Override
	public ArquivoHTML getArquivoHTML(String tagKey) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNamedQuery("getArquivoHTML");
			query.setParameter("tagKey", tagKey);
			@SuppressWarnings("unchecked")
			List<ArquivoHTML> list = query.getResultList();
			if (list.size() >= 1) {
				ArquivoHTML result = list.get(0);
				result = BeanUtil.getInstance().copyToJavaPOJO(result);
				manager.getTransaction().commit();
				return result;
			}

			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
		return null;
	}

	@Override
	public <T extends BaseEntity> List<T> getListLike(Class<T> clazz, String param, String value) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			CriteriaBuilder builder = manager.getCriteriaBuilder();
			CriteriaQuery<T> createQuery = builder.createQuery(clazz);
			Root<T> root = createQuery.from(clazz);
			createQuery.select(root);
			Predicate like = builder.like(root.get(param), builder.parameter(String.class, param));
			createQuery.where(like);
			TypedQuery<T> query = manager.createQuery(createQuery);
			query.setParameter(param, value);
			List<T> pojo = query.getResultList();
			pojo = BeanUtil.getInstance().copyToJavaPOJO(pojo);
			manager.getTransaction().commit();
			return pojo;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Object> List<T> getListNativeQuery(Class<T> clazz, String queryStr) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNativeQuery(queryStr);
			List<T> pojo = query.getResultList();
			manager.getTransaction().commit();
			return pojo;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getListNativeQuery(String queryStr) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNativeQuery(queryStr);
			List<Object[]> pojo = query.getResultList();
			manager.getTransaction().commit();
			return pojo;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@Override
	public void remove(BaseEntity entity) {
		EntityManager manager = getManager();
		manager.getTransaction().begin();
		try {
			entity = manager.find(entity.getClass(), entity.getId());
			manager.remove(entity);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
			logger.severe(e.getMessage());
		}
	}

	@Override
	public <T extends BaseEntity> List<T> save(List<T> listEntity) {
		List<T> list = new ArrayList<T>();
		EntityManager manager = getManager();
		manager.getTransaction().begin();
		try {
			for (T t : listEntity) {
				T entityS = manager.merge(t);
				entityS = BeanUtil.getInstance().copyToJavaPOJO(entityS);
				list.add(entityS);
			}
			manager.getTransaction().commit();
			return list;
		} catch (Exception e) {
			logger.severe(e.getMessage());
			manager.getTransaction().rollback();
			return list;
		}
	}

	@Override
	public <T extends BaseEntity> T save(T entity) {
		EntityManager manager = getManager();
		manager.getTransaction().begin();
		try {
			T entityS = manager.merge(entity);
			entityS = BeanUtil.getInstance().copyToJavaPOJO(entityS);
			manager.getTransaction().commit();
			return entityS;
		} catch (Exception e) {
			e.printStackTrace();
			logger.severe(e.getMessage());
			logger.severe(entity.toString());
			manager.getTransaction().rollback();
			return entity;
		}
	}

	@Override
	public <T extends BaseEntity> T saveLote(T entity) {
		EntityManager manager = getManager();
		try {
			T entityS = manager.merge(entity);
			return entityS;
		} catch (Exception e) {
			logger.severe(e.getMessage());
			logger.severe(entity.toString());
			return entity;
		}
	}

	@Override
	public <T extends BaseEntity> List<T> saveLote(List<T> listEntity) {
		List<T> list = new ArrayList<T>();
		try {
			startTransaction();
			for (T t : listEntity) {
				T entityS = saveLote(t);
				list.add(entityS);
			}
			commitTransaction();
			return list;
		} catch (Exception e) {
			roolBackTransaction();
			logger.severe(e.getMessage());
			return list;
		}
	}

	@Override
	public void startTransaction() {
		getManager().getTransaction().begin();
	}

	@Override
	public void roolBackTransaction() {
		getManager().getTransaction().rollback();
	}

	@Override
	public List<ContaCorrenteReferencia> getContaCorrenteExercicio(String inscricao, String exercicio) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNamedQuery("getContaCorrenteExercicio");
			query.setParameter("inscricao", inscricao);
			query.setParameter("anoReferencia", StringUtils.parseInteger(exercicio));
			@SuppressWarnings("unchecked")
			List<ContaCorrenteReferencia> list = query.getResultList();
			list = BeanUtil.getInstance().copyToJavaPOJO(list);
			manager.getTransaction().commit();
			return list;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@Override
	public List<NotaFiscal> getNotasExercicio(String inscricao, String exercicio, String mes) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNamedQuery("getNotasExercicio");
			query.setParameter("inscricao", inscricao);
			query.setParameter("anoReferencia", StringUtils.parseInteger(exercicio));
			query.setParameter("referencia", StringUtils.parseInteger(mes));
			@SuppressWarnings("unchecked")
			List<NotaFiscal> list = query.getResultList();
			list = BeanUtil.getInstance().copyToJavaPOJO(list);
			manager.getTransaction().commit();
			return list;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}

	@Override
	public List<Guia> getGuiasExercicio(String inscricao, String exercicio, String mes) {
		EntityManager manager = getManager();
		try {
			manager.getTransaction().begin();
			Query query = manager.createNamedQuery("getGuiasExercicio");
			query.setParameter("inscricao", inscricao);
			query.setParameter("anoReferencia", StringUtils.parseInteger(exercicio));
			query.setParameter("referencia", StringUtils.parseInteger(mes));
			@SuppressWarnings("unchecked")
			List<Guia> list = query.getResultList();
			list = BeanUtil.getInstance().copyToJavaPOJO(list);
			manager.getTransaction().commit();
			return list;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			return null;
		}
	}
}