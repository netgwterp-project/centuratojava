package imp;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import imp.db.CRUDService;
import imp.model.ContaCorrenteReferencia;
import imp.model.Guia;
import imp.model.Mobiliario;
import imp.model.NotaFiscal;
import imp.util.DateUtil;
import imp.util.ParseUtils;
import imp.util.StringUtils;

public class ParserFodao {

	public static String urlContaCorrente = "curl 'http://www7.gissonline.com.br/contribuinte/conta_corrente/conteudo_mapa_financeiro.asp?ano=2008' -H 'Pragma: no-cache' -H 'DNT: 1' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Cache-Control: no-cache' -H 'Referer: http://www7.gissonline.com.br/contribuinte/conta_corrente/conta_corrente.asp?origem=atendimento&modalidade=C&exib=S' -H 'Cookie: ASPSESSIONIDSCCTTTBR=FFKDJIAALDLLFPDHBJELAACD; MOBI_NUM_CADASTRO=50117025; EMPR_NUM_INSCRICAO=25533; EMPR_CPF_CGC=28%2E552%2E257%2F0001%2D75%20%20; EMPR_NOM_EMPRESA=Erika%20Aparecida%20Budemberg%2013899838890; EMPR_TIPO=J; ASPSESSIONIDSABRRRDR=ENJLALAALPJNMDECOOHOBODB; ASPSESSIONIDQCDSTTBQ=BLBHDMAAJHCGBONMKNHMJAHN; ASPSESSIONIDQABTTTBR=MIEPKMAAIFKONKLJBLAPDPNF; ASPSESSIONIDQCCRQQDQ=FNKPJNAAAIMBAPGDAKIFDACN; ASPSESSIONIDQAARQRDQ=DAOJBOAABDFNAFCOEFEIOAPJ; ASPSESSIONIDQCCSSSBQ=LGCNMOAADPLMGDBEOCAFCDFO; ASPSESSIONIDQACTSTBQ=BKCDEBBAJDKJCIJBJFFAOLMC; ASPSESSIONIDCAADTTCQ=HOKDGHBAGFBPAANFIBBLOCFN; CFID=5242648; CFTOKEN=32827817; EMPR%5FNOM%5FEMPRESA=IRMAOS+FUCHIDA+LTDA+EPP; CFID=5242648; CFTOKEN=32827817; CFGLOBALS=urltoken%3DCFID%23%3D5242648%26CFTOKEN%23%3D32827817%23lastvisit%3D%7Bts%20%272017%2D12%2D06%2013%3A35%3A13%27%7D%23timecreated%3D%7Bts%20%272017%2D12%2D06%2012%3A04%3A03%27%7D%23hitcount%3D16%23cftoken%3D87981207%23cfid%3D5116776%23; PID=4229; MOBI=50116620; ASPSESSIONIDCCCBSTCR=MKELOIBADEPJEKFIKCPBKIBN; ATEND=S; GCONTRIBUINTE=0; mobi=50116620; MODALIDADE=C; userId=25472; pid=4229; NCONTADOR=; CONT%5FNUM%5FCONTADOR=' -H 'Connection: keep-alive' --compressed";

	public static void main(String[] args) {
		ParserFodao parserFodao = new ParserFodao();
		/*parserFodao.loadContaCorrente("10203", "2017",true,false);
		parserFodao.closeService();*/
		boolean cadastro = false;
		boolean cc2016 = false;
		boolean cc2017 = false;
		boolean reverse = false;
		boolean notas = false;
		boolean guias = false;
		int indice = -1;
		if (ArrayUtils.contains(args, "cadastro")) {
			cadastro = true;
		}
		if (ArrayUtils.contains(args, "cc2016")) {
			cc2016 = true;
		}
		if (ArrayUtils.contains(args, "cc2017")) {
			cc2017 = true;
		}
		if (ArrayUtils.contains(args, "reverse")) {
			reverse = true;
		}
		if (ArrayUtils.contains(args, "notas")) {
			notas = true;
		}
		if (ArrayUtils.contains(args, "guias")) {
			guias = true;
		}
		if (ArrayUtils.contains(args, "indice")) {
			int i = ArrayUtils.lastIndexOf(args, "indice");
			if ((i + 1) < args.length) {
				String si = args[i + 1];
				Integer in = StringUtils.parseInteger(si);
				if ((in != null) && (in > 0)) {
					indice = in;
				}
			}
		}
		parserFodao.execute(indice, cadastro, cc2016, cc2017, reverse, notas, guias);
	}

	private CRUDService service;

	public ParserFodao() {
		service = ParseUtils.crudServiceImpl;

	}

	protected void closeService() {
		service.close();
	}

	protected void execute(int indice, boolean cadastro, boolean cc2016, boolean cc2017, boolean reverse, boolean notas,
			boolean guias) {
		if (cadastro) {
			loadCadastros();
		}
		if (cc2016 || cc2017) {
			List<Mobiliario> listPresrador = service.getListPrestrador();
			if (reverse) {
				Collections.reverse(listPresrador);
			}
			int j = 0;
			for (Mobiliario mobiliario : listPresrador) {
				if (j > indice) {
					if (cc2017) {
						loadContaCorrente(mobiliario.getInscricao(), "2017", notas, guias);
					}
					if (cc2016) {
						loadContaCorrente(mobiliario.getInscricao(), "2016", false, false);
					}
				}
				j++;
				System.out.println(
						"Prestador: " + j + " de " + listPresrador.size() + " Inscricao: " + mobiliario.getInscricao());
			}
		}
		closeService();
	}

	protected Map<String, String> getContaCorrenteheadss(String inscricao, String exercicio) {
		Map<String, String> heads = new HashMap<>();
		heads.put("Pragma", "no-cache");
		heads.put("DNT", "1");
		heads.put("Accept-Encoding", "gzip, deflate");
		heads.put("Accept-Language", "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7");
		heads.put("Upgrade-Insecure-Requests", "1");
		heads.put("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
		heads.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		heads.put("Cache-Control", "no-cache");
		heads.put("Connection", "keep-alive");
		heads.put("Referer",
				"http://www7.gissonline.com.br/contribuinte/conta_corrente/conta_corrente.asp?origem=atendimento&modalidade=C&exib=S");
		heads.put("Cookie", "ASPSESSIONIDSCCTTTBR=FFKDJIAALDLLFPDHBJELAACD; MOBI_NUM_CADASTRO=" + inscricao
				+ "; ASPSESSIONIDSABRRRDR=ENJLALAALPJNMDECOOHOBODB; ASPSESSIONIDQCDSTTBQ=BLBHDMAAJHCGBONMKNHMJAHN; ASPSESSIONIDQABTTTBR=MIEPKMAAIFKONKLJBLAPDPNF; ASPSESSIONIDQCCRQQDQ=FNKPJNAAAIMBAPGDAKIFDACN; ASPSESSIONIDQAARQRDQ=DAOJBOAABDFNAFCOEFEIOAPJ; ASPSESSIONIDQCCSSSBQ=LGCNMOAADPLMGDBEOCAFCDFO; ASPSESSIONIDQACTSTBQ=BKCDEBBAJDKJCIJBJFFAOLMC; ASPSESSIONIDCAADTTCQ=HOKDGHBAGFBPAANFIBBLOCFN; CFID=5242648; CFTOKEN=32827817; CFID=5242648; CFTOKEN=32827817; CFGLOBALS=urltoken%3DCFID%23%3D5242648%26CFTOKEN%23%3D32827817%23lastvisit%3D%7Bts%20%272017%2D12%2D06%2013%3A35%3A13%27%7D%23timecreated%3D%7Bts%20%272017%2D12%2D06%2012%3A04%3A03%27%7D%23hitcount%3D16%23cftoken%3D87981207%23cfid%3D5116776%23; PID=4229; MOBI="
				+ inscricao + "; ASPSESSIONIDCCCBSTCR=MKELOIBADEPJEKFIKCPBKIBN; ATEND=S; GCONTRIBUINTE=0; mobi="
				+ inscricao + "; MODALIDADE=C; userId=" + inscricao + "; pid=4229; NCONTADOR=; CONT%5FNUM%5FCONTADOR=");
		return heads;
	}

	protected Map<String, String> getGuiasheadss(String inscricao, String exercicio, String mes) {
		Map<String, String> heads = new HashMap<>();
		heads.put("Pragma", "no-cache");
		heads.put("DNT", "1");
		heads.put("Accept-Encoding", "gzip, deflate");
		heads.put("Accept-Language", "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7");
		heads.put("Upgrade-Insecure-Requests", "1");
		heads.put("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
		heads.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		heads.put("Cache-Control", "no-cache");
		heads.put("Referer", "http://www7.gissonline.com.br/contribuinte/conta_corrente/conteudo.cfm?ano=" + exercicio
				+ "&mes=" + mes + "&pid=4229&mobi=" + inscricao);
		heads.put("Cookie", " ASPSESSIONIDSCCTTTBR=FFKDJIAALDLLFPDHBJELAACD; "
				+ "ASPSESSIONIDSABRRRDR=ENJLALAALPJNMDECOOHOBODB; " + "ASPSESSIONIDQCDSTTBQ=BLBHDMAAJHCGBONMKNHMJAHN; "
				+ "ASPSESSIONIDQABTTTBR=MIEPKMAAIFKONKLJBLAPDPNF; " + "ASPSESSIONIDQCCRQQDQ=FNKPJNAAAIMBAPGDAKIFDACN; "
				+ "ASPSESSIONIDQAARQRDQ=DAOJBOAABDFNAFCOEFEIOAPJ; " + "ASPSESSIONIDQCCSSSBQ=LGCNMOAADPLMGDBEOCAFCDFO; "
				+ "ASPSESSIONIDQACTSTBQ=BKCDEBBAJDKJCIJBJFFAOLMC; " + "ASPSESSIONIDCAADTTCQ=HOKDGHBAGFBPAANFIBBLOCFN; "
				+ "ASPSESSIONIDCCCBSTCR=MKELOIBADEPJEKFIKCPBKIBN; " + "ASPSESSIONIDASQSCTAS=DFCPECCAHJPHADBFJFPKEJCD; "
				+ "ASPSESSIONIDCSSTDSAS=GEOHKGCAMCJIHNKFKKOKCGKH; " + "ASPSESSIONIDASRTCTBT=ICEBJDDAAOFDMIKBJJJEBEIG; "
				+ "GTOMADOR=0; " + "ANO%5FCOMPETENCIA=; " + "MES%5FCOMPETENCIA=; "
				+ "ASPSESSIONIDAQQTDSAT=GNGBLEMAJEOFAEKBFMNJOPKI; " + "MOBI_NUM_CADASTRO=" + inscricao + ";  "
				+ "ASPSESSIONIDASSQCTBT=HFIDNGEBNPKHMPOGJFIIDCND; " + "MOBI=" + inscricao + "; "
				+ "ASPSESSIONIDAQSTCSAT=GINJICGBPFLIPDBKDEBLHHFI; " + "ASPSESSIONIDQABSQQDQ=LLMLEHGBPPDANMOGCCINBGFB; "
				+ "PID=4229;  " + "ASPSESSIONIDSACQRQCR=NCCFJKGBHDJCGDANOOMKPGEA; "
				+ "ASPSESSIONIDCCACTSCQ=MFKLDOGBNOCIAMDAPMHLFOMD; " + "ASPSESSIONIDCACBSSCR=CDFFHCHBFJHHDDJMABOPBEDC; "
				+ "CFID=5265818; " + "CFTOKEN=38593876; " + "ATEND=S; " + "GCONTRIBUINTE=0; " + "mobi=" + inscricao
				+ "; " + "MODALIDADE=C; " + "userId=" + inscricao + "; " + "pid=4229; " + "CONT%5FNUM%5FCONTADOR=; "
				+ "NCONTADOR=; " + "CFID=5265818; " + "CFTOKEN=38593876; "
				+ "CFGLOBALS=urltoken%3DCFID%23%3D5265818%26CFTOKEN%23%3D38593876%23lastvisit%3D%7Bts%20%272017%2D12%2D08%2014%3A48%3A36%27%7D%23timecreated%3D%7Bts%20%272017%2D12%2D06%2012%3A04%3A03%27%7D%23hitcount%3D55%23cftoken%3D87981207%23cfid%3D5116776%23");
		heads.put("Connection", "keep-alive");
		return heads;
	}

	protected Map<String, String> getNotasFiscaisheadss(String inscricao, String exercicio, String mes) {
		Map<String, String> heads = new HashMap<>();
		heads.put("Pragma", "no-cache");
		heads.put("DNT", "1");
		heads.put("Accept-Encoding", "gzip, deflate");
		heads.put("Accept-Language", "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7");
		heads.put("Upgrade-Insecure-Requests", "1");
		heads.put("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
		heads.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		heads.put("Cache-Control", "no-cache");
		heads.put("Connection", "keep-alive");
		heads.put("Referer",
				"http://www7.gissonline.com.br/contribuinte/consulta/default.cfm?btn=true&pid=4229&mes=" + mes + "&ano="
						+ exercicio + "&modalidade=C&Mobi_Num_Cadastro=" + inscricao + "&contador=&user_id=" + inscricao
						+ "&cont_num_contador=&exibe=S");
		heads.put("Cookie", "ASPSESSIONIDSCCTTTBR=FFKDJIAALDLLFPDHBJELAACD; "
				+ "ASPSESSIONIDSABRRRDR=ENJLALAALPJNMDECOOHOBODB; " + "ASPSESSIONIDQCDSTTBQ=BLBHDMAAJHCGBONMKNHMJAHN; "
				+ "ASPSESSIONIDQABTTTBR=MIEPKMAAIFKONKLJBLAPDPNF; " + "ASPSESSIONIDQCCRQQDQ=FNKPJNAAAIMBAPGDAKIFDACN; "
				+ "ASPSESSIONIDQAARQRDQ=DAOJBOAABDFNAFCOEFEIOAPJ; " + "ASPSESSIONIDQCCSSSBQ=LGCNMOAADPLMGDBEOCAFCDFO; "
				+ "ASPSESSIONIDQACTSTBQ=BKCDEBBAJDKJCIJBJFFAOLMC; " + "ASPSESSIONIDCAADTTCQ=HOKDGHBAGFBPAANFIBBLOCFN; "
				+ "ASPSESSIONIDCCCBSTCR=MKELOIBADEPJEKFIKCPBKIBN; " + "ASPSESSIONIDASQSCTAS=DFCPECCAHJPHADBFJFPKEJCD; "
				+ "CFID=6632714; " + "CFTOKEN=85893968; " + "ASPSESSIONIDCSSTDSAS=GEOHKGCAMCJIHNKFKKOKCGKH; "
				+ "ASPSESSIONIDASRTCTBT=ICEBJDDAAOFDMIKBJJJEBEIG; " + "PID=4229; " + "GTOMADOR=0; "
				+ "MES%5FCOMPETENCIA=; " + "ANO%5FCOMPETENCIA=; " + "ATEND=S; GCONTRIBUINTE=0; " + "mobi=" + inscricao
				+ "; " + "MODALIDADE=C; " + "userId=" + inscricao + "; " + "pid=4229; " + "CONT%5FNUM%5FCONTADOR=; "
				+ "NCONTADOR=; " + "CFID=6632714; " + "CFTOKEN=85893968; "
				+ "CFGLOBALS=urltoken%3DCFID%23%3D6632714%26CFTOKEN%23%3D85893968%23lastvisit%3D%7Bts%20%272017%2D12%2D06%2023%3A22%3A40%27%7D%23timecreated%3D%7Bts%20%272017%2D12%2D06%2012%3A04%3A03%27%7D%23hitcount%3D25%23cftoken%3D87981207%23cfid%3D5116776%23");
		return heads;
	}

	private boolean isDataParceCC(Mobiliario mobiliario, String exercicio) {
		if (exercicio.equals("2017")) {
			return mobiliario.getDataParserCC2017() != null;
		} else if (exercicio.equals("2016")) {
			return mobiliario.getDataParserCC2016() != null;
		}
		return true;
	}

	protected void loadCadastros() {
		List<File> files = new ArrayList<>();
		File dir = new File("C:\\Clientes\\SAOROQUE\\chupacabra\\teste");
		files.addAll(Arrays.asList(dir.listFiles()));
		File dir2 = new File("C:\\Clientes\\SAOROQUE\\chupacabra\\novos");
		files.addAll(Arrays.asList(dir2.listFiles()));

		int j = 0;
		for (File file : files) {
			String inscricaocad = file.getName().replaceAll(".html", "");
			try {
				Document doc = ParseUtils.readDocumentFromFile(file, "MOBILIARIO=" + inscricaocad);
				if (doc != null) {
					Mobiliario mobiliario = processaMobiliario(inscricaocad, doc);
					j++;
					System.out.println("Processado: " + j + " de " + files.size() + " Inscricao: " + mobiliario.getInscricao());
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Inscricao: " + inscricaocad + " Erro: " + e.getMessage());
			}
		}
	}

	protected void loadContaCorrente(String inscricao, String exercicio, boolean notas, boolean guias) {
		try {
			Mobiliario mobiliario = service.getMobiliarioInscricao(inscricao);
			List<ContaCorrenteReferencia> processaContaCorrente = null;
			if (!isDataParceCC(mobiliario, exercicio)) {
				System.out.println("PROCESSANDO CC " + inscricao + " ANO=" + exercicio);
				Document document = ParseUtils.readDocumentFromURL(
						"http://www7.gissonline.com.br/contribuinte/conta_corrente/conteudo_mapa_financeiro.asp?ano="
								+ exercicio,
						getContaCorrenteheadss(inscricao, exercicio),
						"MOBICC=" + inscricao + "&EXERCICIO=" + exercicio);
				if (document != null) {
					processaContaCorrente = processaContaCorrente(inscricao, exercicio, document,
							new URL("http://www7.gissonline.com.br/contribuinte/conta_corrente/"));
					service.saveLote(processaContaCorrente);
					setDataParceCC(mobiliario, exercicio, new Date());
					service.save(mobiliario);
				} else {
					processaContaCorrente = new ArrayList<>();
				}
			} else {
				if (notas || guias) {
					processaContaCorrente = service.getContaCorrenteExercicio(inscricao, exercicio);
				} else {
					processaContaCorrente = new ArrayList<>();
				}
				System.out.println("CC PROCESSADO " + inscricao + " ANO=" + exercicio);
			}
			for (ContaCorrenteReferencia contaCorrenteReferencia : processaContaCorrente) {
				if (notas) {
					if (contaCorrenteReferencia.getDataParserNotas() == null) {
						System.out.println("PROCESSANDO NOTAS " + inscricao + " MES="
								+ contaCorrenteReferencia.getReferencia() + " ANO=" + exercicio);
						Document documentEsc = ParseUtils.readDocumentFromURL(
								"http://www7.gissonline.com.br/contribuinte/consulta/default.cfm?btn=true&pid=4229&mes="
										+ contaCorrenteReferencia.getReferencia() + "&ano=" + exercicio
										+ "&modalidade=C&Mobi_Num_Cadastro=" + inscricao + "&contador=&user_id="
										+ inscricao + "&cont_num_contador=&exibe=S",
								getNotasFiscaisheadss(inscricao, exercicio,
										contaCorrenteReferencia.getReferencia().toString()),
								"MOBINOTAM=" + inscricao + "&EXERCICIO=" + exercicio + "&MES="
										+ contaCorrenteReferencia.getReferencia());
						String text = null;
						Elements elements = documentEsc.select("font[face]");
						if (elements.size() > 1) {
							text = ParseUtils.getText(elements.get(1));
							if (text.startsWith("escrituração encerrada em: ")) {
								text = text.substring(27, text.length());
								contaCorrenteReferencia.setDataEscrituracao(StringUtils.parseDateHour(text));
							}
						}
						elements = documentEsc.select("a[target]");
						String inscricaogiss = null;
						if (elements.size() > 0) {
							text = ParseUtils.getText(elements.get(0));
							int indexOf = text.indexOf("&inscrMun=");
							indexOf = indexOf + 10;
							int indexfim = text.indexOf("&paramAno=");
							inscricaogiss = text.substring(indexOf, indexfim);
						}
						if (inscricaogiss != null) {
							Document documentNotas = ParseUtils.readDocumentFromURL(
									"http://www7.gissonline.com.br/escri_conferencia_con.cfm?tipo=C&inscrMun="
											+ inscricaogiss + "&paramAno=" + exercicio + "&y="
											+ contaCorrenteReferencia.getReferencia() + "&pid=4229",
									getNotasFiscaisheadss(inscricao, exercicio,
											contaCorrenteReferencia.getReferencia().toString()),
									"MOBINOTA=" + inscricao + "&EXERCICIO=" + exercicio + "&MES="
											+ contaCorrenteReferencia.getReferencia());
							if (documentNotas != null) {
								List<NotaFiscal> processaNotas = processaNotas(inscricao, exercicio,
										contaCorrenteReferencia.getReferencia().toString(), documentNotas);
								service.saveLote(processaNotas);
								contaCorrenteReferencia.setDataParserNotas(new Date());
								service.save(contaCorrenteReferencia);
							}
						}
					} else {
						System.out.println("NOTAS PROCESSADAS " + inscricao + " MES="
								+ contaCorrenteReferencia.getReferencia() + " ANO=" + exercicio);
					}
				}
				if (guias) {
					if ((contaCorrenteReferencia.getDataParserGuias() == null)
							&& contaCorrenteReferencia.getTipoEscrituracao().equals("NORMAL")) {
						System.out.println("PROCESSANDO GUIAS " + inscricao + " MES="
								+ contaCorrenteReferencia.getReferencia() + " ANO=" + exercicio);
						Document documentGuias = ParseUtils.readDocumentFromURL(
								"http://www7.gissonline.com.br/contribuinte/conta_corrente/conteudo_mapa_financeiro.asp?ano="
										+ exercicio + "&mes=" + contaCorrenteReferencia.getReferencia(),
								getGuiasheadss(inscricao, exercicio,
										contaCorrenteReferencia.getReferencia().toString()),
								"MOBIGUIA=" + inscricao + "&EXERCICIO=" + exercicio + "&MES="
										+ contaCorrenteReferencia.getReferencia());
						if (documentGuias != null) {
							List<Guia> processaGuias = processaGuias(inscricao, exercicio,
									contaCorrenteReferencia.getReferencia().toString(), documentGuias);
							service.saveLote(processaGuias);
							contaCorrenteReferencia.setDataParserGuias(new Date());
							service.save(contaCorrenteReferencia);
						}
					} else {
						System.out.println("GUIAS PROCESSADAS " + inscricao + " MES="
								+ contaCorrenteReferencia.getReferencia() + " ANO=" + exercicio);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected List<ContaCorrenteReferencia> processaContaCorrente(String inscricao, String exercicio, Document document,
			URL urlBase) throws IOException {
		List<ContaCorrenteReferencia> contas = new ArrayList<>();
		contas.addAll(service.getContaCorrenteExercicio(inscricao, exercicio));
		Elements tables = document.select("table");
		for (Element element : tables) {
			Elements trs = element.select("tr");
			for (int i = 1; i < trs.size(); i++) {
				Element tr = trs.get(i);
				if (tr != null) {
					ContaCorrenteReferencia cc = null;
					Elements tds = tr.select("td");
					if (tds.size() == 8) {
						for (int j = 0; j < tds.size(); j++) {
							Element td = tds.get(j);
							String text = ParseUtils.getText(td);
							if (j == 0) {
								String[] split = text.split("#\\$#");
								Integer mes = StringUtils.parseMesExtenso(split[0]);
								cc = ParseUtils.getContaCorrenteReferencia(contas, inscricao, exercicio,
										mes.toString());
								cc.setTipoEscrituracao("NORMAL");
								if (split.length == 2) {
									cc.setUrlDetalhe(ParseUtils.normalizeURL(urlBase, split[1]));
								}
							} else if (j == 7) {
								String[] split = text.split("#\\$#");
								if (split.length == 2) {
									cc.setUrlGuias(ParseUtils.normalizeURL(urlBase, split[1]));
								}
							} else if (j == 1) {
								cc.setTotalGerado(StringUtils.parseBigDecimal(text));
							} else if (j == 2) {
								cc.setCompensado(StringUtils.parseBigDecimal(text));
							} else if (j == 3) {
								cc.setRestituido(StringUtils.parseBigDecimal(text));
							} else if (j == 4) {
								cc.setTotalRecolhido(StringUtils.parseBigDecimal(text));
							} else if (j == 5) {
								cc.setImpostoPago(StringUtils.parseBigDecimal(text));
							} else if (j == 6) {
								cc.setTotalAberto(StringUtils.parseBigDecimal(text));
							}

						}
					} else if (tds.size() == 2) {
						for (int j = 0; j < tds.size(); j++) {
							Element td = tds.get(j);
							String text = ParseUtils.getText(td);
							if (j == 0) {
								String[] split = text.split("#\\$#");
								Integer mes = StringUtils.parseMesExtenso(split[0]);
								cc = ParseUtils.getContaCorrenteReferencia(contas, inscricao, exercicio,
										mes.toString());
								cc.setTipoEscrituracao("SIMPLES");
								//cc.setReferencia(mes);
								//cc.setAnoReferencia(StringUtils.parseInteger(exercicio));
								if (split.length == 2) {
									cc.setUrlDetalhe(ParseUtils.normalizeURL(urlBase, split[1]));
								}
							} else if (j == 1) {
								cc.setTotalGerado(StringUtils.parseBigDecimal(text));
							}
						}
						cc.setDataParserGuias(new Date());
						cc.setCompensado(BigDecimal.ZERO);
						cc.setRestituido(BigDecimal.ZERO);
						cc.setTotalRecolhido(BigDecimal.ZERO);
						cc.setImpostoPago(BigDecimal.ZERO);
						cc.setTotalAberto(BigDecimal.ZERO);
					}
				}
			}
		}

		// System.out.println(document.html());
		return contas;

	}

	protected List<Guia> processaGuias(String inscricao, String exercicio, String mes, Document document) {
		List<Guia> notas = new ArrayList<>();
		notas.addAll(service.getGuiasExercicio(inscricao, exercicio, mes));
		Elements tables = document.select("table");
		for (Element element : tables) {
			Elements trs = element.select("tr");
			for (int i = 1; i < trs.size(); i++) {
				Element tr = trs.get(i);
				if (tr != null) {
					Guia nota = null;
					Elements tds = tr.select("td");
					Date dtEmissao = null;
					Integer ano = null;
					Integer ref = null;
					for (int j = 0; j < tds.size(); j++) {
						Element td = tds.get(j);
						String text = ParseUtils.getText(td);
						if (j == 0) {
							dtEmissao = StringUtils.parseDate(text);
						} else if (j == 1) {
						} else if (j == 2) {
							nota = ParseUtils.getGuiaReferencia(notas, inscricao, text);
							nota.setAnoReferencia(ano);
							nota.setReferencia(ref);
							nota.setDataEmissao(dtEmissao);
							nota.setNumero(text);
						} else if (j == 3) {
							nota.setValorImposto(StringUtils.parseBigDecimal(text));
						} else if (j == 4) {
							nota.setValorPago(StringUtils.parseBigDecimal(text));
						} else if (j == 5) {
							nota.setDataPagamento(StringUtils.parseDate(text));
						} else if (j == 6) {
							nota.setStatus(text);
						} else if (j == 7) {
							nota.setOrigem(text);
						} else if (j == 8) {
							nota.setSituacao(text);
						}
					}
				}
			}
		}
		return notas;

	}

	protected Mobiliario processaMobiliario(String inscricaocad, Document doc) throws IOException {
		Elements tableElements = doc.select("table[cellpadding=5]");
		if (tableElements.size() > 0) {
			// System.out.println("**************DADOS*****************");
			Mobiliario mobiliario = ParseUtils.getMobiliario(inscricaocad);
			mobiliario.setInscricaocad(inscricaocad);
			if (mobiliario.getDataParserCadastro() == null) {
				int line = 1;
				for (int t = 0; t < tableElements.size(); t++) {
					Element table = tableElements.get(t);
					Elements trs = table.select("tr");
					for (int j = 0; j < trs.size(); j++) {
						Element tr = trs.get(j);
						Elements tds = tr.select("td");
						for (int i = 0; i < tds.size(); i++) {
							Element td = tds.get(i);
							String text = ParseUtils.getText(td);
							switch (line) {
								case 2:
									mobiliario.setNome(text);
									break;
								case 4:
									mobiliario.setInscricao(text);
									break;
								case 6:
									mobiliario.setCNPJCPF(StringUtils.removeMask(text));
									break;
								case 8:
									mobiliario.setLogradouro(text);
									break;
								case 10:
									mobiliario.setNumero(text);
									break;
								case 12:
									mobiliario.setComplemento(text);
									break;
								case 14:
									mobiliario.setBairro(text);
									break;
								case 16:
									mobiliario.setCidade(text);
									break;
								case 18:
									mobiliario.setEstado(text);
									break;
								case 20:
									mobiliario.setCEP(StringUtils.removeMask(text));
									break;
								case 22:
									mobiliario.setTelefone(StringUtils.removeMask(text));
									break;
								case 24:
									mobiliario.setRamal(text);
									break;
								case 26:
									mobiliario.setFax(StringUtils.removeMask(text));
									break;
								case 28:
									mobiliario.setDataAbertura(StringUtils.parseDate(text));
									break;
								case 30:
									mobiliario.setDataEncerramento(StringUtils.parseDate(text));
									break;
								case 32:
									mobiliario.setDataInclusaoGISS(StringUtils.parseDate(text));
									break;
								case 34:
									String[] split = text.split("#\\$#");
									if (split.length > 1) {
										mobiliario.setEmail(split[0]);
									}
									break;
								default:
									break;
							}
							line++;
						}
					}
				}

				// System.out.println("**************ATIVIDADES*****************");
				String atividades = "";
				boolean has2017 = false;
				tableElements = doc.select("table[cellpadding=3]");
				for (int t = 0; t < tableElements.size(); t++) {
					Element table = tableElements.get(t);
					Elements trs = table.select("tr");
					for (int j = 0; j < trs.size(); j++) {
						Element tr = trs.get(j);
						Elements tds = tr.select("td");
						for (int i = 0; i < tds.size(); i++) {
							Element td = tds.get(i);
							Elements bs = td.select("td > b");
							for (int x = 0; x < bs.size(); x++) {
								Element b = bs.get(x);
								String text = ParseUtils.getText(b);
								if (text.equals("2017")) {
									has2017 = true;
									// System.out.println(text);
								}
							}
							Elements dds = td.select("dd");
							for (int x = 0; x < dds.size(); x++) {
								Element dd = dds.get(x);
								String text = dd.ownText();
								if (has2017 && !"".equals(text)) {
									atividades = atividades.length() == 0 ? text : atividades + "#" + text;
								}
								// System.out.println(text);
								Elements allElements = dd.select("b");
								for (int y = 0; y < allElements.size(); y++) {
									text = ParseUtils.getText(allElements.get(y));
									if (text.equals("2017")) {
										has2017 = true;
										// System.out.println(text);
									}
								}
							}
						}
					}
				}
				mobiliario.setAtividades(atividades);

				// System.out.println("**************REGIME*****************");
				tableElements = doc.select("table[width=420]");
				for (int t = 0; t < tableElements.size(); t++) {
					Element table = tableElements.get(t);
					Elements trs = table.select("tr");
					for (int j = 0; j < trs.size(); j++) {
						Element tr = trs.get(j);
						Elements tds = tr.select("td");
						String text = null;
						for (int i = 0; i < tds.size(); i++) {
							Element td = tds.get(i);
							text = ParseUtils.getText(td);
							// System.out.println(text);
						}
						if ((text != null) && !"".equals(text)) {
							mobiliario.setRegime(text);
						}
					}
				}
				mobiliario.setDataParserCadastro(new Date());
				mobiliario = service.save(mobiliario);
			}
			return mobiliario;
		}
		return null;

	}

	protected List<NotaFiscal> processaNotas(String inscricao, String exercicio, String mes, Document document) {
		List<NotaFiscal> notas = new ArrayList<>();
		notas.addAll(service.getNotasExercicio(inscricao, exercicio, mes));
		Elements tables = document.select("table.Escrituracao");
		for (Element element : tables) {
			Elements trs = element.select("tr");
			for (int i = 1; i < trs.size(); i++) {
				Element tr = trs.get(i);
				if (tr != null) {
					NotaFiscal nota = null;
					Elements tds = tr.select("td");
					for (int j = 0; j < tds.size(); j++) {
						Element td = tds.get(j);
						String text = ParseUtils.getText(td);
						// System.out.println(text);
						if (j == 0) {
							nota = ParseUtils.getNotasReferencia(notas, inscricao, text);
							nota.setAnoReferencia(StringUtils.parseInteger(exercicio));
							nota.setReferencia(StringUtils.parseInteger(mes));
						} else if (j == 1) {
							nota.setSerie(text);
						} else if (j == 3) {
							nota.setDataCompetencia(DateUtil.newDate(exercicio, mes, text));
						} else if (j == 4) {
							nota.setAtividade(text);
						} else if (j == 5) {
							nota.setValorNota(StringUtils.parseBigDecimal(text));
						} else if (j == 6) {
							nota.setBaseCalculo(StringUtils.parseBigDecimal(text));
						} else if (j == 7) {
							text = text.replaceAll("\\D", "");
							nota.setAliquota(StringUtils.parseBigDecimal(text));
						} else if (j == 8) {
							nota.setImposto(StringUtils.parseBigDecimal(text));
						} else if (j == 9) {
							nota.setStatus(text);
						} else if (j == 10) {
							String id = text.replaceAll("\\D", "");
							try {
								/*
								 * text = Jsoup.connect(
								 * "http://www7.gissonline.com.br/contribuinte/pop-juridica.cfm?id="
								 * + id + "&mobi=" +
								 * inscricao).timeout(8000).get().text();
								 */
								nota.setTomador(id);
							} catch (Exception e) {
								nota.setTomador(id);
							}
						} else if (j == 11) {
							nota.setEscrituracao(text);
						} else if (j == 12) {
							nota.setDataEmissao(StringUtils.parseDateHour(text));
						} else if (j == 14) {
							nota.setUrlDetalhe(text);
						}
					}
				}
			}
		}
		return notas;

	}

	private void setDataParceCC(Mobiliario mobiliario, String exercicio, Date date) {
		if (exercicio.equals("2017")) {
			mobiliario.setDataParserCC2017(date);
		} else if (exercicio.equals("2016")) {
			mobiliario.setDataParserCC2016(date);
		}

	}

}
