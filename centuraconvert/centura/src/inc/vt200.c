extern "C"{
/* interface __MIDL_itf_wtypes_0000_0000 */
/* [local] */ 
//+-------------------------------------------------------------------------
//
//  Microsoft Windows
//  Copyright (c) Microsoft Corporation. All rights reserved.
//
//--------------------------------------------------------------------------
//#include "gti.h"  // basic system defines
/*
 * $History: winbind.h $
 * 
 * *****************  Version 1  *****************
 * User: Jlacerda     Date: 11/04/97   Time: 5:10p
 * Created in $/Phoenix/sqlwin/GTIDEV/GTIINC
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 6/18/96    Time: 7:27p
 * Branched in $/Patriot/sqlwin/GTIDEV/GTIINC
 * Stinger project branched for Patriot
 * 
 * *****************  Version 1  *****************
 * User: Tmcparla     Date: 3/06/95    Time: 11:23a
 * Created in $/SQLWIN/GTIDEV/GTIINC
 * Includes WINDOWS.H and manages warnings.
 *
 */
/*  define number data structure as seen from outside the numeric package */
typedef struct 
{
  BYTE numLength;               /* size of number */
  BYTE numValue[24];      /* value of number */
} NUMBER;
typedef NUMBER  *LPNUMBER;  /* pointer to number */
typedef NUMBER *PNUMBER;       /* pointer to number */
typedef const NUMBER  *LPCNUMBER; /* pointer to constant number */
/* test number for being null */
/* DATETIME */
/* $Header: /Jungfrau/sqlwin/GTIDEV/GTIINC/datatype.h 4     11/12/98 6:18p Jmundsto $ */
/*
  file: datatype.h
  description: defines datatypes used by assortment of Gupta Products
  modification History:
  MDC 02/02/89 created
*/
typedef struct 
{
  BYTE DATETIME_Length;		/* length of date/time          */
  BYTE DATETIME_Buffer[24 /* DB_SIZEDAT */ ];
} DATETIME;
typedef DATETIME  *LPDATETIME;
typedef DATETIME *PDATETIME;
/*
 * $History: datatype.h $
 * 
 * *****************  Version 4  *****************
 * User: Jmundsto     Date: 11/12/98   Time: 6:18p
 * Updated in $/Jungfrau/sqlwin/GTIDEV/GTIINC
 * Support for longer numbers.
 * 
 * *****************  Version 3  *****************
 * User: Omullarn     Date: 9/03/98    Time: 1:02p
 * Updated in $/Jungfrau/sqlwin/GTIDEV/GTIINC
 * Object Nationalizer merge
 * 
 * *****************  Version 1  *****************
 * User: Jlacerda     Date: 11/04/97   Time: 5:09p
 * Created in $/Phoenix/sqlwin/GTIDEV/GTIINC
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 6/18/96    Time: 7:25p
 * Branched in $/Patriot/sqlwin/GTIDEV/GTIINC
 * Stinger project branched for Patriot
 * 
 * *****************  Version 2  *****************
 * User: Tmcparla     Date: 2/06/95    Time: 4:15p
 * Updated in $/SQLWIN/GTIDEV/GTIINC
 *
 */
typedef HANDLE HARRAY;
typedef HARRAY *LPHARRAY;
///////////////////////////////////////////////////////////////////////////////
// public definitions of private data types
//#define INTERNAL
struct HITEM__ { int unused; }; typedef struct HITEM__ *HITEM;
typedef HITEM *LPHITEM;
typedef HITEM* PHITEM;
struct HOUTLINE__ { int unused; }; typedef struct HOUTLINE__ *HOUTLINE;
typedef HOUTLINE *LPHOUTLINE;
typedef ULONG HSTRING;
/* Ref to item in a dynalib. */
typedef DWORD       LIBHITEM,
                   *LPLIBHITEM;
typedef void* HFFILE;
typedef struct 
{
  DWORD farTemplate;
  BYTE tag;
}     TEMPLATETAGGED,
      HITEMTAGGED,
	  *LPTEMPLATETAGGED,
	  *LPHITEMTAGGED;
///////////////////////////////////////////////////////////////////////////////
typedef LPSTR *LPLPSTR;
typedef HWND *LPHWND;
typedef int SQLCURSORNUMBER;
/*
  Templates and HITEM's are two names for the same thing.
*/
typedef DWORD TEMPLATE;
typedef int SQLHANDLENUMBER;
typedef int SESSIONHANDLENUMBER;
/*
  Handle to UDV, user defined variable.
  A UDV is an instance of a Functional Class.
*/
typedef struct
{
  BYTE opaque[12];
} HUDV,
  *LPHUDV;
typedef HWND *PHWND;
typedef CHAR *LPCHAR;
typedef double *LPDOUBLE;
typedef float *LPFLOAT;
typedef ULONG *LPULONG;
typedef HFFILE *LPHFFILE;
typedef HSTRING *LPHSTRING;
typedef SQLHANDLENUMBER *LPSQLHANDLENUMBER;
typedef SESSIONHANDLENUMBER *LPSESSIONHANDLENUMBER;
typedef BOOL   *NPBOOL;
typedef BYTE   *NPBYTE;
typedef CHAR   *NPCHAR;
typedef double *NPDOUBLE;
typedef DWORD  *NPDWORD;
typedef float  *NPFLOAT;
typedef INT    *NPINT;
typedef LONG   *NPLONG;
typedef void   *NPVOID;
typedef WORD HDB;
typedef HDB *LPHDB;         // database handle
// Must match SAL.H
typedef BOOL(*LPPROCDDMWRITECALLBACK)(LPVOID, LPSTR);
typedef BOOL(*LPPROCDDMREADCALLBACK)(LPVOID, LPLPSTR);
typedef BOOL(*LPPROCDDMDISCARDCALLBACK) (LPVOID);
typedef BOOL(*LPPROCDDMPREPARECALLBACK) (LPSTR, BOOL, LPVOID, LPWORD,
				 LPBOOL, LPINT, LPINT);
typedef WORD(*LPPROCDDMERRORCALLBACK) ( WPARAM, LPARAM, LPLONG );
/*
 * $History: cbtype.h $
 * 
 * *****************  Version 4  *****************
 * User: Rgurumur     Date: 6/11/99    Time: 3:16p
 * Updated in $/Matterhorn/SQLWindows/include
 * Session Handle changes
 * 
 * *****************  Version 3  *****************
 * User: Omullarn     Date: 5/18/99    Time: 4:26p
 * Updated in $/Matterhorn/SQLWindows/include
 * Fixed includes for internal consistency
 * 
 * *****************  Version 6  *****************
 * User: Jmundsto     Date: 11/12/98   Time: 6:19p
 * Updated in $/Jungfrau/sqlwin/SRC/H
 * Support for longer numbers.
 * 
 * *****************  Version 5  *****************
 * User: Omullarn     Date: 11/05/98   Time: 4:18p
 * Updated in $/Jungfrau/sqlwin/SRC/H
 * Making SalActiveX functions internal: restored LPDOUBLE typedef
 * 
 * *****************  Version 3  *****************
 * User: Tmcparla     Date: 11/14/97   Time: 4:35p
 * Updated in $/Phoenix/sqlwin/SRC/H
 * Changes for Visual C++ 5.0.
 * 
 * *****************  Version 2  *****************
 * User: Omullarn     Date: 11/07/97   Time: 3:33p
 * Updated in $/Phoenix/sqlwin/SRC/H
 * Tomahawk/Patriot merge
 * 
 * *****************  Version 1  *****************
 * User: Jlacerda     Date: 11/04/97   Time: 5:34p
 * Created in $/Phoenix/sqlwin/SRC/H
 * 
 * *****************  Version 3  *****************
 * User: Rjacobs      Date: 2/07/97    Time: 11:06p
 * Updated in $/Patriot/sqlwin/SRC/H
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 6/18/96    Time: 6:52p
 * Branched in $/Patriot/sqlroute/odbsal32
 * Stinger project branched for Patriot
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 2/28/96    Time: 5:32p
 * Updated in $/WIN32/SqlRouter/odbsal32
 * From initial 10237 build.
 * 
 * *****************  Version 1  *****************
 * User: Joropeza     Date: 2/22/96    Time: 3:54a
 * Created in $/WIN32/SQLWIN/src/h
 * replaces swtype.h
 * 
*/
//------------------------------------------------------
// General MetaConstants
//------------------------------------------------------
//------------------------------------------------------
// Recoverable error action code
//------------------------------------------------------
//------------------------------------------------------
// Debug Flags
//------------------------------------------------------
//------------------------------------------------------
// DOS Flags
//------------------------------------------------------
//------------------------------------------------------
// DRIVE Type Flags
//------------------------------------------------------
//------------------------------------------------------
// DROP Flags
//------------------------------------------------------
//------------------------------------------------------
// Data Type Flags
//------------------------------------------------------
//------------------------------------------------------
// File Attributes
//------------------------------------------------------
//------------------------------------------------------
// File Types
//------------------------------------------------------
//------------------------------------------------------
// Font Types
//------------------------------------------------------
//------------------------------------------------------
// Font Get Flags
//------------------------------------------------------
//------------------------------------------------------
// Item Flags
//------------------------------------------------------
//------------------------------------------------------
// Key States
//------------------------------------------------------
//------------------------------------------------------
// Key Down States
//------------------------------------------------------
//------------------------------------------------------
// Calendar Notification Messages
//------------------------------------------------------
//------------------------------------------------------
// ColorPalette Notification Messages
//------------------------------------------------------
//------------------------------------------------------
// ColorPalette Notification Messages
//------------------------------------------------------
//------------------------------------------------------
// HeaderListView Notification Messages
//------------------------------------------------------
//------------------------------------------------------
// Calendar Styles
//------------------------------------------------------
//------------------------------------------------------
// ColorPalette Styles
//------------------------------------------------------
//------------------------------------------------------
// EditColor Styles
//------------------------------------------------------
//------------------------------------------------------
// List Box Styles
//------------------------------------------------------
//------------------------------------------------------
// Message Box Flags
//------------------------------------------------------
//------------------------------------------------------
// Menu Flags
//------------------------------------------------------
//------------------------------------------------------
// Picture Constants
//------------------------------------------------------
//------------------------------------------------------
// Row Flags
//------------------------------------------------------
//------------------------------------------------------
// Seek Flags
//------------------------------------------------------
//------------------------------------------------------
// Show Flags
//------------------------------------------------------
//------------------------------------------------------
// Sort Flags
//------------------------------------------------------
//------------------------------------------------------
// Error Return Codes
//------------------------------------------------------
//------------------------------------------------------
// Predefined pictures
//------------------------------------------------------
//------------------------------------------------------
// Messages
//------------------------------------------------------
//------------------------------------------------------
// Wait cursor flags
//------------------------------------------------------
//------------------------------------------------------
// Window Flags
//------------------------------------------------------
//------------------------------------------------------
// Handles
//------------------------------------------------------
typedef DWORD     HSUBMEM;    // HIWORD - Global handle of segment, LOWORD - Local handle of alloction
struct HACC__ { int unused; }; typedef struct HACC__ *HACC;
struct HFC__ { int unused; }; typedef struct HFC__ *HFC;
struct HSTBL__ { int unused; }; typedef struct HSTBL__ *HSTBL;
// **********************************
// *** Structure pre-declarations ***
// **********************************
struct  BTN;
typedef BTN  *LPBTN;
class   PIC;
typedef PIC  *LPPIC;
struct  TREE;
typedef TREE  *LPTREE;
class   CPicItemArray;
typedef CPicItemArray  *LPCPicItemArray;
extern "C" {
// ***************************
// *** Function Prototypes ***
// ***************************
LONG            __stdcall VisPopulateFontNames           (HWND hwnd, int nGetFlag);
LONG            __stdcall VisPopulateDrives              (HWND hwnd);
LONG            __stdcall VisPopulateCommonDir           (HWND hwnd, LPCTSTR lpctszCurDir);
LONG            __stdcall VisPopulateDirTree             (HWND hwnd, LPCTSTR lpctszDrive);
LPCPicItemArray __stdcall VisPopulatePictureFromProperty (HWND hwndObject, HWND hwndDisplay);  // Changed from HSTRING.  BLG (9/5/95)
HSTRING         __stdcall VisGetCommonDir                (HWND hwnd, int nLastIndex);
HSTRING         __stdcall VisSetCommonDir                (HWND hwnd, int nIndex);
LONG            __stdcall VisArrayAppend                 (HARRAY harySource, HARRAY haryTarget, int nDataType);
LONG            __stdcall VisArrayCopy                   (HARRAY harySource, HARRAY haryTarget, int nDataType);
LONG            __stdcall VisArrayDeleteItem             (HARRAY hary, LONG lIndex, int nDataType);
BOOL            __stdcall VisArrayFillDateTime           (HARRAY hary, DATETIME dtValue, LONG lCount);
BOOL            __stdcall VisArrayFillNumber             (HARRAY hary, NUMBER numValue, LONG lCount);
BOOL            __stdcall VisArrayFillString             (HARRAY hary, LPCTSTR lpctszValue, LONG lCount);
LONG            __stdcall VisArrayFindDateTime           (HARRAY hary, DATETIME dtSearchFor);
LONG            __stdcall VisArrayFindNumber             (HARRAY hary, NUMBER numSearchFor);
LONG            __stdcall VisArrayFindString             (HARRAY hary, LPCTSTR lpctszSearchFor);
LONG            __stdcall VisArrayInsertItem             (HARRAY hary, LONG lIndex, int nDataType);
LONG            __stdcall VisArraySort                   (HARRAY hary, int nSortOrder, int nDataType);
BOOL            __stdcall VisDebugAssert                 (BOOL bExpression, LPCTSTR lpctszDebugMsg);
BOOL            __stdcall VisDebugBeginTime              ();
BOOL            __stdcall VisDebugEndTime                (LPCTSTR lpctszContext, WORD wDebugLevel);
DWORD           __stdcall VisDebugGetFlags               ();
DWORD           __stdcall VisDebugSetFlags               (DWORD dwFlags, BOOL bMode);
BOOL            __stdcall VisDebugSetLevel               (WORD wDebugLevel);
BOOL            __stdcall VisDebugSetTime                (LPCTSTR lpctszContext);
BOOL            __stdcall VisDebugString                 (LPCTSTR lpctszString, WORD wDebugLevel);
HSTRING         __stdcall VisDosBuildFullName            (LPCTSTR lpctszModuleName, LPCTSTR lpctszFileName);
int             __stdcall VisDosEnumDirs                 (LPCTSTR lpctszDirSpec, HARRAY hary);
int             __stdcall VisDosEnumDirInfo              (LPCTSTR lpctszDirSpec, HARRAY haryDirs, HARRAY haryDateTimes, HARRAY haryAttribs);
int             __stdcall VisDosEnumDrives               (HARRAY hary);
int             __stdcall VisDosEnumFileInfo             (LPCTSTR lpctszDirSpec, WORD wAttribute, HARRAY haryFiles, HARRAY harySizes, HARRAY haryDateTimes, HARRAY haryAttribs);
int             __stdcall VisDosEnumFiles                (LPCTSTR lpctszDirSpec, WORD wAttribute, HARRAY hary);
int             __stdcall VisDosEnumNetConnections       (HARRAY haryDevices, HARRAY haryNetNames);
int             __stdcall VisDosEnumPath                 (HARRAY hary);
BOOL            __stdcall VisDosExist                    (LPCTSTR lpctszTest);
BOOL            __stdcall VisDosIsParent                 (LPCTSTR lpctszDirSpec);
HSTRING         __stdcall VisDosGetCurDir                (LPCTSTR lpctszDrive);
int             __stdcall VisDosGetDriveType             (LPCTSTR lpctszDrive);
int             __stdcall VisDosGetDriveSize             (LPCTSTR lpctszDrive, LPDWORD lpdwTotal, LPDWORD lpdwAvailable);
HSTRING         __stdcall VisDosGetEnvString             (LPCTSTR lpctszKeyword);
WORD            __stdcall VisDosGetFlags                 ();
HSTRING         __stdcall VisDosGetNetName               (LPCTSTR lpctszDevice);
NUMBER          __stdcall VisDosGetVersion               ();
HSTRING         __stdcall VisDosGetVolumeLabel           (LPCTSTR lpctszDrive);
int             __stdcall VisDosMakeAllDir               (LPCTSTR lpctszDir);
HSTRING         __stdcall VisDosMakePath                 (LPCTSTR lpctszDrive, LPCTSTR lpctszDir, LPCTSTR lpctszBase, LPCTSTR lpctszExt);
int             __stdcall VisDosNetConnect               (LPCTSTR lpctszDevice, LPCTSTR lpctszNetName, LPCTSTR lpctszPassword);
int             __stdcall VisDosNetDisconnect            (LPCTSTR lpctszDevice, BOOL bForceClose);
int             __stdcall VisDosSetFlags                 (WORD wFlags, BOOL fState);
int             __stdcall VisDosSetVolumeLabel           (LPCTSTR lpctszDrive, LPCTSTR lpctszLabel);
void            __stdcall VisDosSplitPath                (LPCTSTR lpctszPath, LPHSTRING lphDrive, LPHSTRING lphDir, LPHSTRING lphBase, LPHSTRING lphExt);
int             __stdcall VisFileAppend                  (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
int             __stdcall VisFileClose                   (HFC hfc);
int             __stdcall VisFileCopy                    (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
HSTRING         __stdcall VisFileCreateTemp              (LPCTSTR lpctszPrefix);
int             __stdcall VisFileDelete                  (LPCTSTR lpctszFileSpec);
int             __stdcall VisFileExpand                  (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
HSTRING         __stdcall VisFileFind                    (LPCTSTR lpctszFileName);
INT             __stdcall VisFileGetAttribute            (LPCTSTR lpctszFileName );
LONG            __stdcall VisFileGetSize                 (LPCTSTR lpctszFileName );
int             __stdcall VisFileGetType                 (LPCTSTR lpctszFileName);
int             __stdcall VisFileOpen                    (LPHANDLE lphfc, LPCTSTR lpctszFileName, WORD wFlags);
LONG            __stdcall VisFileRead                    (HFC hfc, LPHSTRING lphstrBuffer, DWORD dwBufSize);
int             __stdcall VisFileReadString              (HFC hfc, LPHSTRING lphstr);
int             __stdcall VisFileRename                  (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
LONG            __stdcall VisFileSeek                    (HFC hfc, LONG lOffset, int nOrigin);
int             __stdcall VisFileSetAttribute            (LPCTSTR lpctszFileSpec, WORD wAttribute, BOOL bState);
int             __stdcall VisFileSetDateTime             (LPCTSTR lpctszFileSpec, DATETIME dtDateTime);
LONG            __stdcall VisFileTell                    (HFC hfc);
int             __stdcall VisFileWrite                   (HFC hfc, HSTRING hstrBuffer, DWORD dwBufSize);
int             __stdcall VisFileWriteString             (HFC hfc, LPCTSTR lpctszString);
LONG            __stdcall VisFontEnum                    (WORD wGetFlag, HARRAY haryFontNames, HARRAY haryFontTypes);
int             __stdcall VisFontFree                    (HFONT hfont);
BOOL            __stdcall VisFontGet                     (HFONT hfont, LPHSTRING lphstrFaceName, LPINT lpiPointSize, LPWORD lpwFontFlags);
HFONT           __stdcall VisFontLoad                    (LPCTSTR lpctszFontName, int nFontSize, WORD wFontEnh);
LONG            __stdcall VisListArrayPopulate           (HWND hwnd, HARRAY hary);
LONG            __stdcall VisListArrayPopulateValue      (HWND hwnd, HARRAY haryText, HARRAY haryValues);
int             __stdcall VisListAddValue                (HWND hwnd, LPCTSTR lpctszText, LONG lValue);
BOOL            __stdcall VisListClearSelection          (HWND hwnd);
LONG            __stdcall VisListDeleteSelected          (HWND hwnd);
LONG            __stdcall VisListFindString              (HWND hwnd, int nIndex, LPCTSTR lpctszText);  // Changed to LPCTSTR.  BLG (5/3/96)
LONG            __stdcall VisListFindValue               (HWND hwnd, LONG lStartIndex, LONG lSearchFor);
BOOL            __stdcall VisListGetDropdownState        (HWND hwnd);
LONG            __stdcall VisListGetFocusIndex           (HWND hwnd);
int             __stdcall VisListGetIndexFromPoint       (HWND hwnd, int nXPos, int nYPos);
HSTRING         __stdcall VisListGetText                 (HWND hwnd, int nIndex);
BOOL            __stdcall VisListGetTextRectangle        (HWND hwnd, int nIndex, LPINT lpnTop, LPINT lpnLeft, LPINT lpnBottom, LPINT lpnRight);
LONG            __stdcall VisListGetValue                (HWND hwnd, int nIndex);
LONG            __stdcall VisListGetVisibleRange         (HWND hwnd, LPINT lpnTop, LPINT lpnBottom);
int             __stdcall VisListInsertValue             (HWND hwnd, int nIndex, LPCTSTR lpctszText, LONG lValue);
BOOL            __stdcall VisListIsMultiSelect           (HWND hwnd);
LONG            __stdcall VisListScroll                  (HWND hwnd, int nIndex);
LONG            __stdcall VisListSetFocusIndex           (HWND hwnd, int nIndex);
BOOL            __stdcall VisListSetDropdownState        (HWND hwnd, BOOL fState);
int             __stdcall VisListSetScrollWidth          (HWND hwnd, int nIndex);
int             __stdcall VisListSetText                 (HWND hwnd, int nIndex, LPCTSTR lpctszText);
int             __stdcall VisListSetValue                (HWND hwnd, int nIndex, LONG lValue);
int             __stdcall VisListDisableItem             (HWND hwnd, int nIndex, COLORREF crColor);
int             __stdcall VisListEnableItem              (HWND hwnd, int nIndex, COLORREF crColor);
LONG            __stdcall VisListGetFlags                (HWND hwnd, int nIndex);
LONG            __stdcall VisListSetFlags                (HWND hwnd, int nIndex, DWORD dwItemFlags, BOOL bState);
DWORD           __stdcall VisListGetStyle                (HWND hwnd);
BOOL            __stdcall VisListSetStyle                (HWND hwnd, DWORD dwSlbStyle, LPCREATESTRUCT lpcs);
LONG            __stdcall VisListAddColor                (HWND hwnd, LPCTSTR lpctszText, COLORREF crColor);
LONG            __stdcall VisListAddColorValue           (HWND hwnd, LPCTSTR lpctszText, COLORREF crColor, LONG lValue);
LONG            __stdcall VisListGetColor                (HWND hwnd, int nIndex);
LONG            __stdcall VisListInsertColor             (HWND hwnd, int nIndex, LPCTSTR lpctszText, COLORREF crColor);
LONG            __stdcall VisListInsertColorValue        (HWND hwnd, int nIndex, LPCTSTR lpctszText, COLORREF crColor, LONG lValue);
BOOL            __stdcall VisListSetColor                (HWND hwnd, int nIndex, COLORREF crColor);
LONG            __stdcall VisListAddFont                 (HWND hwnd, LPCTSTR lpctszText, HFONT hfont);
LONG            __stdcall VisListAddFontValue            (HWND hwnd, LPCTSTR lpctszText, HFONT hfont, LONG lValue);
HFONT           __stdcall VisListGetFont                 (HWND hwnd, int nIndex);
LONG            __stdcall VisListInsertFont              (HWND hwnd, int nIndex, LPCTSTR lpctszText, HFONT hfont);
LONG            __stdcall VisListInsertFontValue         (HWND hwnd, int nIndex, LPCTSTR lpctszText, HFONT hfont, LONG lValue);
BOOL            __stdcall VisListSetFont                 (HWND hwnd, int nIndex, HFONT hfont);
LONG            __stdcall VisListAddPicture              (HWND hwnd, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect);
LONG            __stdcall VisListAddPictureValue         (HWND hwnd, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect, LONG lValue);
INT             __stdcall VisListGetPicture              (HWND hwnd, int nIndex, LPINT lpiNormal, LPINT lpiSelect);
LONG            __stdcall VisListInsertPicture           (HWND hwnd, int nIndex, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect);
LONG            __stdcall VisListInsertPictureValue      (HWND hwnd, int nIndex, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect, LONG lValue);
BOOL            __stdcall VisListSetPicture              (HWND hwnd, int nIndex, LPPIC ppicNormal, LPPIC ppicSelect);
BOOL            __stdcall VisListCollapse                (HWND hwnd, int nIndex);
int             __stdcall VisListDeleteChild             (HWND hwnd, int nIndex);
int             __stdcall VisListDeleteDescendents       (HWND hwnd, int nIndex);
BOOL            __stdcall VisListDemote                  (HWND hwnd, int nIndex, LPPIC ppicNormal, LPPIC ppicSelect);
BOOL            __stdcall VisListDragDrop                (HWND hwndSource, int nSourceIndex, HWND hwndTarget, int nTargetIndex, WORD wDropFlags);
LONG            __stdcall VisListEnumChildren            (LPTREE ptree, HARRAY haryItems);
LONG            __stdcall VisListEnumDescendents         (LPTREE ptree, HARRAY haryItems);
int             __stdcall VisListExpand                  (HWND hwnd, int nExpIndex);
BOOL            __stdcall VisListExpandDescendents       (HWND hwnd, int nExpIndex);
LPTREE          __stdcall VisListFindItemValue           (LPTREE ptree, LONG lSearchFor);
BOOL            __stdcall VisListFreeChild               (LPTREE ptreeChild);
BOOL            __stdcall VisListFreeDescendents         (LPTREE ptreeParent);
LONG            __stdcall VisListGetItemColor            (LPTREE ptree);
BOOL            __stdcall VisListGetItemData             (LPTREE ptree, LPHSTRING lphstrText, LPLONG lplValue, LPDWORD lpdwItemFlags);
DWORD           __stdcall VisListGetItemFlags            (LPTREE ptree);
HFONT           __stdcall VisListGetItemFont             (LPTREE ptree);
LPTREE          __stdcall VisListGetItemHandle           (HWND hwnd, int nIndex);
int             __stdcall VisListGetItemIndex            (LPTREE ptree);
BOOL            __stdcall VisListGetItemPicture          (TREE  * ptree, LPINT lpiNormal, LPINT lpiSelect);
HSTRING         __stdcall VisListGetItemText             (LPTREE ptree);
LONG            __stdcall VisListGetItemValue            (LPTREE ptree);
int             __stdcall VisListGetLevel                (LPTREE ptree);
LPTREE          __stdcall VisListGetNextSibling          (LPTREE ptree);
LPTREE          __stdcall VisListGetParent               (LPTREE ptree);
LPTREE          __stdcall VisListGetPrevSibling          (LPTREE ptree);
LPTREE          __stdcall VisListGetRoot                 (HWND hwnd);
LPTREE          __stdcall VisListLoadChild               (HWND hwnd, LPTREE ptreeParent, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText, LONG lValue, DWORD dwItemFlags);
LONG            __stdcall VisListLoadChildren            (HWND hwnd, LPTREE ptreeParent, LPPIC ppicNormal, LPPIC ppicSelect, HARRAY haryText, HARRAY haryValues, HARRAY haryItemFlags);
LPTREE          __stdcall VisListLoadOutline             (HWND hwnd, HSTRING hstrBlob);
LPTREE          __stdcall VisListLoadOutlineFile         (HWND hwnd, LPCTSTR lpctszFileName, LPPIC ppicParentNormal, LPPIC ppicParentSelect, LPPIC ppicChildNormal, LPPIC ppicChildSelect);
LPTREE          __stdcall VisListLoadSibling             (HWND hwnd, LPTREE ptreeBefore, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText, LONG lValue, DWORD dwItemFlags);
BOOL            __stdcall VisListMoveDown                (HWND hwnd, int nIndex);
BOOL            __stdcall VisListMoveUp                  (HWND hwnd, int nIndex);
BOOL            __stdcall VisListPromote                 (HWND hwnd, int nIndex, LPPIC ppicNormal, LPPIC ppicSelect);
int             __stdcall VisListSaveOutline             (HWND hwnd, LPHSTRING lphstrBlob);
BOOL            __stdcall VisListSetIndent               (HWND hwnd, int nIndent);
BOOL            __stdcall VisListSetItemColor            (LPTREE ptree, COLORREF crColor);
LPTREE          __stdcall VisListSetItemData             (HWND hwnd, LPTREE ptree, LPCTSTR lpctszText, LONG lValue, DWORD dwItemFlags);
BOOL            __stdcall VisListSetItemFlags            (LPTREE ptree, DWORD dwItemFlags, BOOL bState);
BOOL            __stdcall VisListSetItemFont             (LPTREE ptree, HFONT hfont);
BOOL            __stdcall VisListSetItemPicture          (LPTREE ptree, LPPIC hpicNormal, LPPIC ppicSelect);
LPTREE          __stdcall VisListSetItemText             (LPTREE ptree, LPCTSTR lpctszText);
BOOL            __stdcall VisListSetItemValue            (LPTREE ptree, LONG lValue);
BOOL            __stdcall VisListSetOutlineRedraw        (HWND hwnd, BOOL fRedraw);
BOOL            __stdcall VisListShowOutline             (HWND hwnd, int nLevel);
BOOL            __stdcall VisMenuCheck                   (HMENU hmenu, int nPos);
BOOL            __stdcall VisMenuDelete                  (HWND hwnd, HMENU hmenu, int nPos);
BOOL            __stdcall VisMenuDisable                 (HMENU hmenu, int nPos);
BOOL            __stdcall VisMenuEnable                  (HMENU hmenu, int nPos);
LONG            __stdcall VisMenuGetCount                (HMENU hmenu);
HMENU           __stdcall VisMenuGetHandle               (HWND hwnd);
HMENU           __stdcall VisMenuGetPopupHandle          (HMENU hmenu, int nPos);
HMENU           __stdcall VisMenuGetSystemHandle         (HWND hwnd);
HSTRING         __stdcall VisMenuGetText                 (HWND hwnd, HMENU hmenu, int nPos);
int             __stdcall VisMenuInsert                  (HWND hwnd, HMENU hmenu, int nPos, LPCTSTR lpctszText, WORD wFlags);
int             __stdcall VisMenuInsertFont              (HWND hwnd, HMENU hmenu, int nPos, HFONT hfont, LPCTSTR lpctszText);
int             __stdcall VisMenuInsertPicture           (HWND hwnd, HMENU hmenu, int nPos, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText);
BOOL            __stdcall VisMenuIsChecked               (HMENU hmenu, int nPos);
BOOL            __stdcall VisMenuIsEnabled               (HMENU hmenu, int nPos);
BOOL            __stdcall VisMenuSetFont                 (HWND hwnd, HMENU hmenu, int nPos, HFONT hfont, LPCTSTR lpctszText);
BOOL            __stdcall VisMenuSetPicture              (HWND hwnd, HMENU hmenu, int nPos, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText);
BOOL            __stdcall VisMenuSetText                 (HWND hwnd, HMENU hmenu, int nPos, LPCTSTR lpctszText);
BOOL            __stdcall VisMenuUncheck                 (HMENU hmenu, int nPos);
BOOL            __stdcall VisMessageFreeButton           (BTN  *lpbtn);
LPBTN           __stdcall VisMessageLoadButton           (LPCTSTR lpctszLabel, int nReturn);
int             __stdcall VisMessageBox                  (LPCTSTR lpctszMsgText, LPCTSTR lpctszCaption, DWORD dwIcon, HARRAY haryBtnHandles, int nNumButtons);
BOOL            __stdcall VisMessageSetBkgdColor         (COLORREF crBkColor);
int             __stdcall VisErrorRecovery               (LPCTSTR lpctszCaption);
HSTRING         __stdcall VisGetCopyright                ();
WORD            __stdcall VisGetKeyState                 (int nVirtualKey);
int             __stdcall VisGetSystemMetrics            (int nIndex);
DWORD           __stdcall VisGetWinFlags                 ();
HSTRING         __stdcall VisGetVersion                  ();
NUMBER          __stdcall VisGetWinVersion               ();
LRESULT         __stdcall VisSendMsgString               (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void            __stdcall VisWaitCursor                  (int nFlags);
NUMBER          __stdcall VisNumberChoose                (BOOL bExpression, NUMBER numTrue, NUMBER numFalse);
DWORD           __stdcall VisNumberBitClear              (LPDWORD lpdwValue, DWORD dwMask);
DWORD           __stdcall VisNumberBitSet                (LPDWORD lpdwValue, DWORD dwMask);
BYTE            __stdcall VisNumberHighByte              (WORD wNumber);
BYTE            __stdcall VisNumberLowByte               (WORD wNumber);
DWORD           __stdcall VisNumberMakeLong              (WORD wLow, WORD wHigh);
LPPIC           __stdcall VisPicLoad                     (WORD wFlags, LPCTSTR lpctszString1, LPCTSTR lpctszString2);
int             __stdcall VisPicFree                     (LPPIC ppic);
LONG            __stdcall VisProfileEnumStrings          (LPCTSTR lpctszIniFile, LPCTSTR lpctszSection, HARRAY hary);
BOOL            __stdcall VisProfileDelete               (LPCTSTR lpctszIniFile, LPCTSTR lpctszSection, LPCTSTR lpctszEntry);
HSTRING         __stdcall VisStrChoose                   (BOOL bExpression, HSTRING hstrTrue, HSTRING hstrFalse);
HSTRING         __stdcall VisStrExpand                   (LPCTSTR lpctszTemplate, HARRAY hary);
HSTRING         __stdcall VisStrFind                     (HSTBL hstbl, LPCTSTR lpctszContext);
BOOL            __stdcall VisStrFreeTable                (HSTBL hstbl);
HSTRING         __stdcall VisStrLeftTrim                 (LPCTSTR lpctszSource);
HSTBL           __stdcall VisStrLoadTable                (LPCTSTR lpszStrTableFile);
HSTRING         __stdcall VisStrPad                      (LPCTSTR lpctszSource, int nSize);
HSTRING         __stdcall VisStrProper                   (LPCTSTR lpctszSource);
HSTRING         __stdcall VisStrRightTrim                (LPCTSTR lpctszSource);
int             __stdcall VisStrScanReverse              (LPCTSTR lpctszSource, int nPos, LPCTSTR lpctszSearch);
HSTRING         __stdcall VisStrSubstitute               (LPCTSTR lpctszSource, LPCTSTR lpctszSearch, LPCTSTR lpctszReplace);
HSTRING         __stdcall VisStrTrim                     (LPCTSTR lpctszSource);
BOOL            __stdcall VisTblAllRows                  (HWND hwndTable, WORD wFlagsOn, WORD wFlagsOff);
BOOL            __stdcall VisTblAutoSizeColumn           (HWND hwndTable, HWND hwndColumn);
BOOL            __stdcall VisTblClearColumnSelection     (HWND hwndTable);
LONG            __stdcall VisTblFindDateTime             (HWND hwndTable, LONG lStartRow, HWND hwndColumn, DATETIME dtSearchFor);
LONG            __stdcall VisTblFindNumber               (HWND hwndTable, LONG lStartRow, HWND hwndColumn, NUMBER numSearchFor);
LONG            __stdcall VisTblFindString               (HWND hwndTable, LONG lStartRow, HWND hwndColumn, LPTSTR lpctszSearchFor);
HSTRING         __stdcall VisTblGetCell                  (HWND hwndTable, LONG lRow, HWND hwndColumn);
HSTRING         __stdcall VisTblGetColumnText            (LONG lRow, HWND hwndColumn);
HSTRING         __stdcall VisTblGetColumnTitle           (HWND hwndColumn);
BOOL            __stdcall VisTblSetRowPicture            (HWND hwndTable, WORD wRowFlag, LPPIC ppic);
BOOL            __stdcall VisTblSetRowColor              (HWND hwndTable, LONG lRow, COLORREF crTextColor);
LONG            __stdcall VisWinClearAllFields           (HWND hwndParent);
LONG            __stdcall VisWinClearAllEditFlags        (HWND hwndParent);
int             __stdcall VisWinEnumProps                (HWND hwnd, HARRAY haryPropStrings, HARRAY haryValues);
BOOL            __stdcall VisWinFreeAccelerator          (HACC hacc);
DWORD           __stdcall VisWinGetFlags                 (HWND hwnd);
HWND            __stdcall VisWinGetHandle                (HWND hwndParent, LPCTSTR lpctszContext, DWORD dwType);
HANDLE          __stdcall VisWinGetProp                  (HWND hwnd, LPCTSTR lpctszPropString);
LONG            __stdcall VisWinGetStyle                 (HWND hwnd);
HSTRING         __stdcall VisWinGetText                  (HWND hwnd);
BOOL            __stdcall VisWinIsChild                  (HWND hwndParent, HWND hwndChild);
BOOL            __stdcall VisWinIsMaximized              (HWND hwnd);
BOOL            __stdcall VisWinIsMinimized              (HWND hwnd);
HWND            __stdcall VisWinIsRequiredFieldNull      (HWND hwndParent);
BOOL            __stdcall VisWinIsRestored               (HWND hwnd);
BOOL            __stdcall VisWinIsWindow                 (HWND hwnd);
HACC            __stdcall VisWinLoadAccelerator          (HWND hwndNotify, UINT uMsg, int nKeyDownState, WORD wVirtualKey, LONG lValue);
BOOL            __stdcall VisWinMove                     (HWND hwnd, int nLeft, int nTop, int nWidth, int nHeight);
BOOL            __stdcall VisWinRemoveProp               (HWND hwnd, LPCTSTR lpctszPropString);
BOOL            __stdcall VisWinSetFlags                 (HWND hwnd, DWORD dwWinFlags, BOOL bState);
int             __stdcall VisWinSetMeter                 (HWND hwnd, int nPctCmp);
BOOL            __stdcall VisWinSetProp                  (HWND hwnd, LPCTSTR lpctszPropString, WORD wValue);
BOOL            __stdcall VisWinSetSelect                (HWND hwnd, WORD wStartPos, WORD wEndPos);
int             __stdcall VisWinSetStyle                 (HWND hwnd, DWORD dwNewStyle, BOOL bState );
BOOL            __stdcall VisWinSetTabOrder              (HWND hwnd, HWND hwndInsertAfter);
BOOL            __stdcall VisWinShow                     (HWND hwnd, int iShowCmd);
   }