extern "C"{
typedef int NUMITEMTYPE;
typedef LONG   *LPLONG;
typedef DWORD   SYMID;
typedef SYMID *PSYMID;
typedef struct
{
  BYTE numLength;               /* size of number */
  BYTE numValue[24];      /* value of number */
} NUMBER;
typedef NUMBER  *LPNUMBER;  /* pointer to number */
typedef NUMBER *PNUMBER;       /* pointer to number */
typedef const NUMBER  *LPCNUMBER; /* pointer to constant number */
typedef struct
{
  BYTE DATETIME_Length;		/* length of date/time          */
  BYTE DATETIME_Buffer[24 /* DB_SIZEDAT */ ];
} DATETIME;
typedef DATETIME  *LPDATETIME;
typedef DATETIME *PDATETIME;
typedef HANDLE HARRAY;
typedef HARRAY *LPHARRAY;
struct HITEM__ { int unused; }; typedef struct HITEM__ *HITEM;
typedef HITEM *LPHITEM;
typedef HITEM* PHITEM;
struct HOUTLINE__ { int unused; }; typedef struct HOUTLINE__ *HOUTLINE;
typedef HOUTLINE *LPHOUTLINE;
typedef HANDLE HSTRING;
typedef DWORD       LIBHITEM,
                   *LPLIBHITEM;
typedef void* HFFILE;
typedef struct
{
  DWORD farTemplate;
  BYTE tag;
}     TEMPLATETAGGED,
      HITEMTAGGED,
	  *LPTEMPLATETAGGED,
	  *LPHITEMTAGGED;
typedef LPSTR *LPLPSTR;
typedef HWND *LPHWND;
typedef int SQLCURSORNUMBER;
typedef DWORD TEMPLATE;
typedef int SQLHANDLENUMBER;
typedef int SESSIONHANDLENUMBER;
typedef struct
{
  BYTE opaque[12];
} HUDV,
  *LPHUDV;
typedef HWND *PHWND;
typedef CHAR *LPCHAR;
typedef double *LPDOUBLE;
typedef float *LPFLOAT;
typedef ULONG *LPULONG;
typedef HFFILE *LPHFFILE;
typedef HSTRING *LPHSTRING;
typedef SQLHANDLENUMBER *LPSQLHANDLENUMBER;
typedef SESSIONHANDLENUMBER *LPSESSIONHANDLENUMBER;
typedef BOOL   *NPBOOL;
typedef BYTE   *NPBYTE;
typedef CHAR   *NPCHAR;
typedef double *NPDOUBLE;
typedef DWORD  *NPDWORD;
typedef float  *NPFLOAT;
typedef INT    *NPINT;
typedef LONG   *NPLONG;
typedef void   *NPVOID;
typedef WORD HDB;
typedef HDB *LPHDB;         // database handle
typedef BOOL(*LPPROCDDMWRITECALLBACK)(LPVOID, LPSTR);
typedef BOOL(*LPPROCDDMREADCALLBACK)(LPVOID, LPLPSTR);
typedef BOOL(*LPPROCDDMDISCARDCALLBACK) (LPVOID);
typedef BOOL(*LPPROCDDMPREPARECALLBACK) (LPSTR, BOOL, LPVOID, LPWORD,
				 LPBOOL, LPINT, LPINT);
typedef WORD(*LPPROCDDMERRORCALLBACK) ( WPARAM, LPARAM, LPLONG );
BOOL      __stdcall  SalAbort(INT);
void      __stdcall  SalActiveXAutoErrorMode(BOOL);
BOOL      __stdcall  SalActiveXClose(HWND, BOOL);
BOOL      __stdcall  SalActiveXCreate(HWND, LPCSTR);
BOOL      __stdcall  SalActiveXCreateFromData(HWND, HSTRING);
BOOL      __stdcall  SalActiveXCreateFromFile(HWND, LPCSTR);
BOOL      __stdcall  SalActiveXDelete(HWND);
BOOL      __stdcall  SalActiveXDoVerb(HWND, LONG, BOOL);
BOOL      __stdcall  SalActiveXGetActiveObject(HUDV, LPSTR);
BOOL      __stdcall  SalActiveXGetData(HWND, LPHSTRING);
BOOL      __stdcall  SalActiveXGetObject(HWND, HUDV);
LONG      __stdcall  SalActiveXInsertObjectDlg(HWND);
LONG      __stdcall  SalActiveXOLEType(HWND);
BOOL      __stdcall  SalAppDisable(void);
BOOL      __stdcall  SalAppEnable(void);
HWND      __stdcall  SalAppFind(LPSTR, BOOL);
NUMBER    __stdcall  SalArrayAvg(HARRAY);
BOOL      __stdcall  SalArrayDimCount(HARRAY, LPLONG);
BOOL      __stdcall  SalArrayGetLowerBound(HARRAY, INT, LPLONG);
BOOL      __stdcall  SalArrayGetUpperBound(HARRAY, INT, LPLONG);
BOOL      __stdcall  SalArrayIsEmpty(HARRAY);
NUMBER    __stdcall  SalArrayMax(HARRAY);
NUMBER    __stdcall  SalArrayMin(HARRAY);
BOOL      __stdcall  SalArraySetUpperBound(HARRAY, INT, LONG);
NUMBER    __stdcall  SalArraySum(HARRAY);
BOOL      __stdcall  SalBringWindowToTop(HWND);
BOOL      __stdcall  SalCenterWindow(HWND);
BOOL      __stdcall  SalClearField(HWND);
DWORD     __stdcall  SalColorFromRGB(BYTE, BYTE, BYTE);
DWORD     __stdcall  SalColorGet(HWND, INT);
DWORD     __stdcall  SalColorGetSysColor(INT);
BOOL      __stdcall  SalColorSet(HWND, INT, DWORD);
BOOL      __stdcall  SalColorToRGB(DWORD, LPBYTE, LPBYTE, LPBYTE);
INT       __stdcall  SalCompileAndEvaluate(LPSTR, LPINT, LPINT, LPNUMBER, LPHSTRING, LPDATETIME, LPHWND, BOOL, LPSTR);
HSTRING   __stdcall  SalContextBreak(void);
HSTRING   __stdcall  SalContextCurrent(void);
BOOL      __stdcall  SalContextMenuSetPopup(HWND, LPSTR, DWORD);
HWND      __cdecl  SalCreateWindow(HITEMTAGGED, HWND, ...);
HWND      __cdecl  SalCreateWindowEx(TEMPLATETAGGED, HWND, NUMBER, NUMBER, NUMBER, NUMBER, DWORD, ...);
BOOL      __stdcall  SalCursorClear(HWND, WORD);
BOOL      __stdcall  SalCursorSet(HWND, TEMPLATE, WORD);
BOOL      __stdcall  SalCursorSetFile(HWND, LPSTR, WORD);
BOOL      __stdcall  SalCursorSetString(HWND, HSTRING, WORD);
ATOM      __stdcall  SalDDEAddAtom(LPSTR);
HGLOBAL   __stdcall  SalDDEAlloc(void);
ATOM      __stdcall  SalDDEDeleteAtom(ATOM);
BOOL      __stdcall  SalDDEExtract(WPARAM, LPARAM, LPHWND, LPDWORD, LPDWORD);
BOOL      __stdcall  SalDDEExtractCmd(HGLOBAL, LPHSTRING, INT);
BOOL      __stdcall  SalDDEExtractDataText(HGLOBAL, LPWORD, LPHSTRING, INT);
BOOL      __stdcall  SalDDEExtractOptions(HGLOBAL, LPWORD, LPWORD);
ATOM      __stdcall  SalDDEFindAtom(LPSTR);
HGLOBAL   __stdcall  SalDDEFree(HGLOBAL);
UINT      __stdcall  SalDDEGetAtomName(ATOM, LPHSTRING, INT);
HSTRING   __stdcall  SalDDEGetExecuteString(LPARAM);
LONG      __stdcall  SalDDEPost(HWND, UINT, HWND, UINT, UINT);
BOOL      __stdcall  SalDDERequest(HWND, LPSTR, LPSTR, LPSTR, LONG, LPHSTRING);
LONG      __stdcall  SalDDESend(HWND, UINT, HWND, UINT, UINT);
LONG      __stdcall  SalDDESendAll(UINT, HWND, UINT, UINT);
BOOL      __stdcall  SalDDESendExecute(HWND, LPSTR, LPSTR, LPSTR, LONG, LPSTR);
BOOL      __stdcall  SalDDESendToClient(HWND, LPSTR, WPARAM, LONG);
BOOL      __stdcall  SalDDESetCmd(HGLOBAL, LPSTR);
BOOL      __stdcall  SalDDESetDataText(HGLOBAL, WORD, LPSTR);
BOOL      __stdcall  SalDDESetOptions(HGLOBAL, WORD, WORD);
BOOL      __stdcall  SalDDEStartServer(HWND, LPSTR, LPSTR, LPSTR);
BOOL      __stdcall  SalDDEStartSession(HWND, LPSTR, LPSTR, LPSTR, LONG);
BOOL      __stdcall  SalDDEStopServer(HWND);
BOOL      __stdcall  SalDDEStopSession(HWND);
DATETIME  __stdcall  SalDateConstruct(INT, INT, INT, INT, INT, INT);
DATETIME  __stdcall  SalDateCurrent(void);
INT       __stdcall  SalDateDay(DATETIME);
INT       __stdcall  SalDateHour(DATETIME);
INT       __stdcall  SalDateMinute(DATETIME);
INT       __stdcall  SalDateMonth(DATETIME);
DATETIME  __stdcall  SalDateMonthBegin(DATETIME);
INT       __stdcall  SalDateQuarter(DATETIME);
DATETIME  __stdcall  SalDateQuarterBegin(DATETIME);
INT       __stdcall  SalDateSecond(DATETIME);
INT       __stdcall  SalDateToStr(DATETIME, LPHSTRING);
DATETIME  __stdcall  SalDateWeekBegin(DATETIME);
INT       __stdcall  SalDateWeekday(DATETIME);
INT       __stdcall  SalDateYear(DATETIME);
DATETIME  __stdcall  SalDateYearBegin(DATETIME);
BOOL      __stdcall  SalDestroyWindow(HWND);
BOOL      __stdcall  SalDisableWindow(HWND);
BOOL      __stdcall  SalDisableWindowAndLabel(HWND);
BOOL      __stdcall  SalDlgChooseColor(HWND, LPDWORD);
BOOL      __stdcall  SalDlgChooseFont(HWND, LPHSTRING, LPINT, LPWORD, LPDWORD);
BOOL      __stdcall  SalDlgOpenFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING);
BOOL      __stdcall  SalDlgSaveFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING);
BOOL      __stdcall  SalDragDropDisableDrop(void);
BOOL      __stdcall  SalDragDropEnableDrop(void);
BOOL      __stdcall  SalDragDropGetSource(LPHWND, LPINT, LPINT);
BOOL      __stdcall  SalDragDropGetTarget(LPHWND, LPINT, LPINT);
BOOL      __stdcall  SalDragDropStart(HWND);
BOOL      __stdcall  SalDragDropStop(void);
BOOL      __stdcall  SalDrawMenuBar(HWND);
BOOL      __stdcall  SalDropFilesAcceptFiles(HWND, BOOL);
INT       __stdcall  SalDropFilesQueryFiles(HWND, HARRAY);
BOOL      __stdcall  SalDropFilesQueryPoint(HWND, LPINT, LPINT);
BOOL      __stdcall  SalEditCanCopyTo(void);
BOOL      __stdcall  SalEditCanCut(void);
BOOL      __stdcall  SalEditCanPaste(void);
BOOL      __stdcall  SalEditCanPasteFrom(void);
BOOL      __stdcall  SalEditCanUndo(void);
BOOL      __stdcall  SalEditClear(void);
BOOL      __stdcall  SalEditCopy(void);
BOOL      __stdcall  SalEditCopyString(LPSTR);
BOOL      __stdcall  SalEditCopyTo(void);
BOOL      __stdcall  SalEditCut(void);
BOOL      __stdcall  SalEditPaste(void);
BOOL      __stdcall  SalEditPasteFrom(void);
BOOL      __stdcall  SalEditPasteString(LPHSTRING);
BOOL      __stdcall  SalEditUndo(void);
BOOL      __stdcall  SalEnableWindow(HWND);
BOOL      __stdcall  SalEnableWindowAndLabel(HWND);
BOOL      __stdcall  SalEndDialog(HWND, INT);
BOOL      __stdcall  SalFileClose(LPHFFILE);
INT       __stdcall  SalFileCopy(LPSTR, LPSTR, BOOL);
BOOL      __stdcall  SalFileCreateDirectory(LPSTR);
BOOL      __stdcall  SalFileGetC(HFFILE, LPWORD);
INT       __stdcall  SalFileGetChar(HFFILE);
BOOL      __stdcall  SalFileGetCurrentDirectory(LPHSTRING);
BOOL      __stdcall  SalFileGetDateTime(LPSTR, LPDATETIME);
HSTRING   __stdcall  SalFileGetDrive(void);
BOOL      __stdcall  SalFileGetStr(HFFILE, LPHSTRING, INT);
BOOL      __stdcall  SalFileOpen(LPHFFILE, LPSTR, LONG);
BOOL      __stdcall  SalFileOpenExt(LPHFFILE, LPSTR, LONG, LPHSTRING);
BOOL      __stdcall  SalFilePutC(HFFILE, WORD);
BOOL      __stdcall  SalFilePutChar(HFFILE, INT);
INT       __stdcall  SalFilePutStr(HFFILE, LPSTR);
LONG      __stdcall  SalFileRead(HFFILE, LPHSTRING, LONG);
BOOL      __stdcall  SalFileRemoveDirectory(LPSTR);
BOOL      __stdcall  SalFileSeek(HFFILE, LONG, INT);
BOOL      __stdcall  SalFileSetCurrentDirectory(LPSTR);
BOOL      __stdcall  SalFileSetDateTime(LPSTR, DATETIME);
BOOL      __stdcall  SalFileSetDrive(LPSTR);
LONG      __stdcall  SalFileTell(HFFILE);
LONG      __stdcall  SalFileWrite(HFFILE, HSTRING, LONG);
HWND      __stdcall  SalFindWindow(HWND, LPSTR);
BOOL      __stdcall  SalFireEvent(HITEM, ...);
BOOL      __stdcall  SalFmtFieldToStr(HWND, LPHSTRING, BOOL);
HSTRING   __stdcall  SalFmtFormatDateTime(DATETIME, LPSTR);
HSTRING   __stdcall  SalFmtFormatNumber(NUMBER, LPSTR);
INT       __stdcall  SalFmtGetFormat(HWND);
BOOL      __stdcall  SalFmtGetInputMask(HWND, LPHSTRING);
BOOL      __stdcall  SalFmtGetPicture(HWND, LPHSTRING);
BOOL      __stdcall  SalFmtIsValidField(HWND);
BOOL      __stdcall  SalFmtIsValidInputMask(LPSTR);
BOOL      __stdcall  SalFmtIsValidPicture(LPSTR, INT);
BOOL      __stdcall  SalFmtKeepMask(BOOL);
BOOL      __stdcall  SalFmtSetFormat(HWND, INT);
BOOL      __stdcall  SalFmtSetInputMask(HWND, LPSTR);
BOOL      __stdcall  SalFmtSetPicture(HWND, LPSTR);
BOOL      __stdcall  SalFmtStrToField(HWND, LPSTR, BOOL);
BOOL      __stdcall  SalFmtUnmaskInput(HWND, LPHSTRING);
BOOL      __stdcall  SalFmtValidateField(HWND, INT);
BOOL      __stdcall  SalFontGet(HWND, LPHSTRING, LPINT, LPWORD);
INT       __stdcall  SalFontGetNames(WORD, HARRAY);
INT       __stdcall  SalFontGetSizes(WORD, LPSTR, HARRAY);
BOOL      __stdcall  SalFontSet(HWND, LPSTR, INT, WORD);
BOOL      __stdcall  SalFormGetParmNum(HWND, INT, LPNUMBER);
INT       __stdcall  SalFormUnitsToPixels(HWND, NUMBER, BOOL);
INT       __stdcall  SalGetDataType(HWND);
HWND      __stdcall  SalGetDefButton(HWND);
HWND      __stdcall  SalGetFirstChild(HWND, LONG);
HWND      __stdcall  SalGetFocus(void);
BOOL      __stdcall  SalGetItemName(HWND, LPHSTRING);
INT       __stdcall  SalGetMaxDataLength(HWND);
HWND      __stdcall  SalGetNextChild(HWND, LONG);
UINT      __stdcall  SalGetProfileInt(LPCSTR, LPCSTR, INT, LPCSTR);
BOOL      __stdcall  SalGetProfileString(LPCSTR, LPCSTR, LPCSTR, LPHSTRING, LPCSTR);
LONG      __stdcall  SalGetType(HWND);
WORD      __stdcall  SalGetVersion(void);
INT       __stdcall  SalGetWindowLabelText(HWND, LPHSTRING, INT);
BOOL      __stdcall  SalGetWindowLoc(HWND, LPNUMBER, LPNUMBER);
BOOL      __stdcall  SalGetWindowSize(HWND, LPNUMBER, LPNUMBER);
INT       __stdcall  SalGetWindowState(HWND);
INT       __stdcall  SalGetWindowText(HWND, LPHSTRING, INT);
DWORD     __stdcall  SalHStringToNumber(HSTRING);
BOOL      __stdcall  SalHideWindow(HWND);
BOOL      __stdcall  SalHideWindowAndLabel(HWND);
BOOL      __stdcall  SalIdleKick(void);
BOOL      __stdcall  SalIdleRegisterWindow(HWND, UINT, WPARAM, LPARAM);
BOOL      __stdcall  SalIdleUnregisterWindow(HWND);
BOOL      __stdcall  SalInvalidateWindow(HWND);
BOOL      __stdcall  SalIsButtonChecked(HWND);
BOOL      __stdcall  SalIsNull(HWND);
BOOL      __stdcall  SalIsValidDateTime(HWND);
BOOL      __stdcall  SalIsValidDecimal(HWND, INT, INT);
BOOL      __stdcall  SalIsValidInteger(HWND);
BOOL      __stdcall  SalIsValidNumber(HWND);
BOOL      __stdcall  SalIsWindowEnabled(HWND);
BOOL      __stdcall  SalIsWindowVisible(HWND);
INT       __stdcall  SalListAdd(HWND, LPSTR);
BOOL      __stdcall  SalListClear(HWND);
INT       __stdcall  SalListDelete(HWND, INT);
BOOL      __stdcall  SalListFiles(HWND, HWND, LPHSTRING, WORD);
BOOL      __stdcall  SalListGetMultiSelect(HWND, HARRAY);
INT       __stdcall  SalListInsert(HWND, INT, LPSTR);
BOOL      __stdcall  SalListPopulate(HWND, SQLHANDLENUMBER, LPSTR);
INT       __stdcall  SalListQueryCount(HWND);
BOOL      __stdcall  SalListQueryFile(HWND, LPHSTRING);
NUMBER    __stdcall  SalListQueryMultiCount(HWND);
INT       __stdcall  SalListQuerySelection(HWND);
BOOL      __stdcall  SalListQueryState(HWND, INT);
INT       __stdcall  SalListQueryText(HWND, INT, LPHSTRING);
INT       __stdcall  SalListQueryTextLength(HWND, INT);
HSTRING   __stdcall  SalListQueryTextX(HWND, INT);
BOOL      __stdcall  SalListRedraw(HWND, BOOL);
INT       __stdcall  SalListSelectString(HWND, INT, LPSTR);
BOOL      __stdcall  SalListSetMultiSelect(HWND, INT, BOOL);
BOOL      __stdcall  SalListSetSelect(HWND, INT);
BOOL      __stdcall  SalListSetTabs(HWND, HARRAY);
BOOL      __stdcall  SalLoadApp(LPSTR, LPSTR);
BOOL      __stdcall  SalLoadAppAndWait(LPSTR, WORD, LPDWORD);
BOOL      __stdcall  SalMDIArrangeIcons(HWND);
BOOL      __stdcall  SalMDICascade(HWND);
BOOL      __stdcall  SalMDITile(HWND, BOOL);
BOOL      __stdcall  SalMTSCreateInstance(HUDV);
BOOL      __stdcall  SalMTSDisableCommit(void);
BOOL      __stdcall  SalMTSEnableCommit(void);
BOOL      __stdcall  SalMTSGetObjectContext(LPLONG);
BOOL      __stdcall  SalMTSIsCallerInRole(LPSTR, LPBOOL);
BOOL      __stdcall  SalMTSIsInTransaction(LPBOOL);
BOOL      __stdcall  SalMTSIsSecurityEnabled(LPBOOL);
BOOL      __stdcall  SalMTSSetAbort(void);
BOOL      __stdcall  SalMTSSetComplete(void);
BOOL      __stdcall  SalMapEnterToTab(BOOL);
BOOL      __stdcall  SalMessageBeep(WPARAM);
INT       __stdcall  SalMessageBox(LPSTR, LPSTR, UINT);
INT       __cdecl  SalModalDialog(HITEMTAGGED, HWND, ...);
BOOL      __stdcall  SalMoveWindow(HWND, NUMBER, NUMBER);
NUMBER    __stdcall  SalNumberAbs(NUMBER);
NUMBER    __stdcall  SalNumberArcCos(NUMBER);
NUMBER    __stdcall  SalNumberArcSin(NUMBER);
NUMBER    __stdcall  SalNumberArcTan(NUMBER);
NUMBER    __stdcall  SalNumberArcTan2(NUMBER, NUMBER);
NUMBER    __stdcall  SalNumberCos(NUMBER);
NUMBER    __stdcall  SalNumberCosH(NUMBER);
NUMBER    __stdcall  SalNumberExponent(NUMBER);
WORD      __stdcall  SalNumberHigh(DWORD);
NUMBER    __stdcall  SalNumberHypot(NUMBER, NUMBER);
NUMBER    __stdcall  SalNumberLog(NUMBER);
NUMBER    __stdcall  SalNumberLogBase10(NUMBER);
WORD      __stdcall  SalNumberLow(DWORD);
NUMBER    __stdcall  SalNumberMax(NUMBER, NUMBER);
NUMBER    __stdcall  SalNumberMin(NUMBER, NUMBER);
NUMBER    __stdcall  SalNumberMod(NUMBER, NUMBER);
NUMBER    __stdcall  SalNumberPi(NUMBER);
NUMBER    __stdcall  SalNumberPower(NUMBER, NUMBER);
BOOL      __stdcall  SalNumberRandInit(WORD);
INT       __stdcall  SalNumberRandom(void);
NUMBER    __stdcall  SalNumberRound(NUMBER);
NUMBER    __stdcall  SalNumberSin(NUMBER);
NUMBER    __stdcall  SalNumberSinH(NUMBER);
NUMBER    __stdcall  SalNumberSqrt(NUMBER);
NUMBER    __stdcall  SalNumberTan(NUMBER);
NUMBER    __stdcall  SalNumberTanH(NUMBER);
HSTRING   __stdcall  SalNumberToChar(WORD);
HSTRING   __stdcall  SalNumberToHString(DWORD);
INT       __stdcall  SalNumberToStr(NUMBER, INT, LPHSTRING);
HSTRING   __stdcall  SalNumberToStrX(NUMBER, INT);
HWND      __stdcall  SalNumberToWindowHandle(UINT);
NUMBER    __stdcall  SalNumberTruncate(NUMBER, INT, INT);
HUDV      __stdcall  SalObjCreateFromString(LPSTR);
HSTRING   __stdcall  SalObjGetType(HUDV);
BOOL      __stdcall  SalObjIsDerived(HUDV, LPSTR);
BOOL      __stdcall  SalObjIsNull(HUDV);
BOOL      __stdcall  SalObjIsValidClassName(LPSTR);
HWND      __stdcall  SalParentWindow(HWND);
BOOL      __stdcall  SalPicClear(HWND);
INT       __stdcall  SalPicGetDescription(HWND, LPHSTRING, INT);
LONG      __stdcall  SalPicGetImage(HWND, LPHSTRING, LPINT);
LONG      __stdcall  SalPicGetString(HWND, INT, LPHSTRING);
BOOL      __stdcall  SalPicSet(HWND, HITEM, INT);
BOOL      __stdcall  SalPicSetFile(HWND, LPSTR);
BOOL      __stdcall  SalPicSetFit(HWND, INT, INT, INT);
BOOL      __stdcall  SalPicSetHandle(HWND, INT, DWORD);
BOOL      __stdcall  SalPicSetImage(HWND, HSTRING, INT);
BOOL      __stdcall  SalPicSetString(HWND, INT, HSTRING);
NUMBER    __stdcall  SalPixelsToFormUnits(HWND, INT, BOOL);
BOOL      __stdcall  SalPostMsg(HWND, UINT, WPARAM, LPARAM);
BOOL      __stdcall  SalPrtExtractRect(LONG, LPINT, LPINT, LPINT, LPINT);
BOOL      __stdcall  SalPrtGetDefault(LPHSTRING, LPHSTRING, LPHSTRING);
BOOL      __stdcall  SalPrtGetParmNum(INT, LPNUMBER);
BOOL      __stdcall  SalPrtPrintForm(HWND);
BOOL      __stdcall  SalPrtSetDefault(LPSTR, LPSTR, LPSTR);
BOOL      __stdcall  SalPrtSetParmDefaults(void);
BOOL      __stdcall  SalPrtSetParmNum(INT, NUMBER);
BOOL      __stdcall  SalPrtSetup(LPHSTRING, LPHSTRING, LPHSTRING, BOOL);
DWORD     __stdcall  SalQueryArrayBounds(HARRAY, LPLONG, LPLONG);
BOOL      __stdcall  SalQueryFieldEdit(HWND);
BOOL      __stdcall  SalQuit(void);
BOOL      __stdcall  SalReportClose(HWND);
BOOL      __stdcall  SalReportCmd(HWND, INT);
BOOL      __stdcall  SalReportCreate(LPSTR, LPSTR, LPSTR, BOOL, LPINT);
BOOL      __stdcall  SalReportDlgOptions(HWND, LPSTR, LPSTR, LPSTR, LPSTR);
BOOL      __stdcall  SalReportGetDateTimeVar(HWND, LPSTR, LPDATETIME);
BOOL      __stdcall  SalReportGetNumberVar(HWND, LPSTR, LPNUMBER);
BOOL      __stdcall  SalReportGetObjectVar(HWND, LPSTR, LPHSTRING);
BOOL      __stdcall  SalReportGetStringVar(HWND, LPSTR, LPHSTRING);
HWND      __stdcall  SalReportPrint(HWND, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, LPINT);
HWND      __stdcall  SalReportPrintToFile(HWND, LPSTR, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, BOOL, LPINT);
BOOL      __stdcall  SalReportReset(HWND);
BOOL      __stdcall  SalReportSetDateTimeVar(HWND, LPSTR, DATETIME);
BOOL      __stdcall  SalReportSetNumberVar(HWND, LPSTR, NUMBER);
BOOL      __stdcall  SalReportSetObjectVar(HWND, LPSTR, HSTRING);
BOOL      __stdcall  SalReportSetStringVar(HWND, LPSTR, LPSTR);
BOOL      __stdcall  SalReportTableCreate(LPSTR, HWND, LPINT);
HWND      __stdcall  SalReportTablePrint(HWND, LPSTR, HARRAY, LPINT);
HWND      __stdcall  SalReportTableView(HWND, HWND, LPSTR, LPINT);
HWND      __stdcall  SalReportView(HWND, HWND, LPSTR, LPSTR, LPSTR, LPINT);
BOOL      __stdcall  SalScrollGetPos(HWND, LPINT);
BOOL      __stdcall  SalScrollGetRange(HWND, LPINT, LPINT, LPINT, LPINT);
BOOL      __stdcall  SalScrollSetPos(HWND, INT);
BOOL      __stdcall  SalScrollSetRange(HWND, INT, INT, INT, INT);
LONG      __stdcall  SalSendClassMessage(UINT, WPARAM, LPARAM);
LONG      __stdcall  SalSendClassMessageNamed(HITEM, UINT, WPARAM, LPARAM);
LONG      __stdcall  SalSendMsg(HWND, UINT, WPARAM, LPARAM);
BOOL      __stdcall  SalSendMsgToChildren(HWND, UINT, WPARAM, LPARAM);
WORD      __stdcall  SalSendValidateMsg(void);
BOOL      __stdcall  SalSetArrayBounds(HARRAY, LONG, LONG);
BOOL      __stdcall  SalSetDefButton(HWND);
BOOL      __stdcall  SalSetErrorInfo(LONG, LPSTR, LPSTR, DWORD);
BOOL      __stdcall  SalSetFieldEdit(HWND, BOOL);
HWND      __stdcall  SalSetFocus(HWND);
BOOL      __stdcall  SalSetMaxDataLength(HWND, LONG);
BOOL      __stdcall  SalSetProfileString(LPCSTR, LPCSTR, LPCSTR, LPCSTR);
BOOL      __stdcall  SalSetWindowLabelText(HWND, LPSTR);
BOOL      __stdcall  SalSetWindowLoc(HWND, NUMBER, NUMBER);
BOOL      __stdcall  SalSetWindowSize(HWND, NUMBER, NUMBER);
BOOL      __stdcall  SalSetWindowText(HWND, LPSTR);
BOOL      __stdcall  SalShowWindow(HWND);
BOOL      __stdcall  SalShowWindowAndLabel(HWND);
INT       __stdcall  SalStatusGetText(HWND, LPHSTRING, INT);
BOOL      __stdcall  SalStatusSetText(HWND, LPSTR);
BOOL      __stdcall  SalStatusSetVisible(HWND, BOOL);
BOOL      __stdcall  SalStrCompress(LPHSTRING);
BOOL      __stdcall  SalStrFirstC(LPHSTRING, LPWORD);
LONG      __stdcall  SalStrGetBufferLength(HSTRING);
BOOL      __stdcall  SalStrIsValidCurrency(LPSTR, INT, INT);
BOOL      __stdcall  SalStrIsValidDateTime(LPSTR);
BOOL      __stdcall  SalStrIsValidNumber(LPSTR);
LONG      __stdcall  SalStrLeft(HSTRING, LONG, LPHSTRING);
HSTRING   __stdcall  SalStrLeftX(HSTRING, LONG);
LONG      __stdcall  SalStrLength(LPSTR);
INT       __stdcall  SalStrLop(LPHSTRING);
LONG      __stdcall  SalStrLower(HSTRING, LPHSTRING);
HSTRING   __stdcall  SalStrLowerX(HSTRING);
LONG      __stdcall  SalStrMid(HSTRING, LONG, LONG, LPHSTRING);
HSTRING   __stdcall  SalStrMidX(HSTRING, LONG, LONG);
LONG      __stdcall  SalStrProper(HSTRING, LPHSTRING);
HSTRING   __stdcall  SalStrProperX(HSTRING);
LONG      __stdcall  SalStrRepeat(HSTRING, INT, LPHSTRING);
HSTRING   __stdcall  SalStrRepeatX(HSTRING, INT);
LONG      __stdcall  SalStrReplace(HSTRING, INT, INT, HSTRING, LPHSTRING);
HSTRING   __stdcall  SalStrReplaceX(HSTRING, INT, INT, HSTRING);
LONG      __stdcall  SalStrRight(HSTRING, LONG, LPHSTRING);
HSTRING   __stdcall  SalStrRightX(HSTRING, LONG);
INT       __stdcall  SalStrScan(LPSTR, LPSTR);
BOOL      __stdcall  SalStrSetBufferLength(LPHSTRING, LONG);
DATETIME  __stdcall  SalStrToDate(LPSTR);
NUMBER    __stdcall  SalStrToNumber(LPSTR);
INT       __stdcall  SalStrTokenize(LPSTR, LPSTR, LPSTR, HARRAY);
INT       __stdcall  SalStrTrim(HSTRING, LPHSTRING);
HSTRING   __stdcall  SalStrTrimX(HSTRING);
BOOL      __stdcall  SalStrUncompress(LPHSTRING);
LONG      __stdcall  SalStrUpper(HSTRING, LPHSTRING);
HSTRING   __stdcall  SalStrUpperX(HSTRING);
BOOL      __stdcall  SalTBarSetVisible(HWND, BOOL);
BOOL      __stdcall  SalTblAnyRows(HWND, WORD, WORD);
BOOL      __stdcall  SalTblClearSelection(HWND);
NUMBER    __stdcall  SalTblColumnAverage(HWND, WORD, WORD, WORD);
NUMBER    __stdcall  SalTblColumnSum(HWND, WORD, WORD, WORD);
BOOL      __stdcall  SalTblCopyRows(HWND, WORD, WORD);
INT       __stdcall  SalTblCreateColumn(HWND, UINT, NUMBER, INT, LPSTR);
BOOL      __stdcall  SalTblDefineCheckBoxColumn(HWND, DWORD, LPSTR, LPSTR);
BOOL      __stdcall  SalTblDefineDropDownListColumn(HWND, DWORD, INT);
BOOL      __stdcall  SalTblDefinePopupEditColumn(HWND, DWORD, INT);
BOOL      __stdcall  SalTblDefineRowHeader(HWND, LPSTR, INT, WORD, HWND);
BOOL      __stdcall  SalTblDefineSplitWindow(HWND, INT, BOOL);
BOOL      __stdcall  SalTblDeleteRow(HWND, LONG, WORD);
BOOL      __stdcall  SalTblDeleteSelected(HWND, SQLHANDLENUMBER);
BOOL      __stdcall  SalTblDestroyColumns(HWND);
BOOL      __stdcall  SalTblDoDeletes(HWND, SQLHANDLENUMBER, WORD);
BOOL      __stdcall  SalTblDoInserts(HWND, SQLHANDLENUMBER, BOOL);
BOOL      __stdcall  SalTblDoUpdates(HWND, SQLHANDLENUMBER, BOOL);
INT       __stdcall  SalTblFetchRow(HWND, LONG);
BOOL      __stdcall  SalTblFindNextRow(HWND, LPLONG, WORD, WORD);
BOOL      __stdcall  SalTblFindPrevRow(HWND, LPLONG, WORD, WORD);
BOOL      __stdcall  SalTblGetColumnText(HWND, UINT, LPHSTRING);
INT       __stdcall  SalTblGetColumnTitle(HWND, LPHSTRING, INT);
HWND      __stdcall  SalTblGetColumnWindow(HWND, INT, WORD);
LONG      __stdcall  SalTblInsertRow(HWND, LONG);
BOOL      __stdcall  SalTblKillEdit(HWND);
BOOL      __stdcall  SalTblKillFocus(HWND);
BOOL      __stdcall  SalTblObjectsFromPoint(HWND, INT, INT, LPLONG, LPHWND, LPDWORD);
BOOL      __stdcall  SalTblPasteRows(HWND);
BOOL      __stdcall  SalTblPopulate(HWND, SQLHANDLENUMBER, LPSTR, INT);
BOOL      __stdcall  SalTblQueryCheckBoxColumn(HWND, LPDWORD, LPHSTRING, LPHSTRING);
BOOL      __stdcall  SalTblQueryColumnCellType(HWND, LPINT);
BOOL      __stdcall  SalTblQueryColumnFlags(HWND, DWORD);
INT       __stdcall  SalTblQueryColumnID(HWND);
INT       __stdcall  SalTblQueryColumnPos(HWND);
BOOL      __stdcall  SalTblQueryColumnWidth(HWND, LPNUMBER);
LONG      __stdcall  SalTblQueryContext(HWND);
BOOL      __stdcall  SalTblQueryDropDownListColumn(HWND, LPDWORD, LPINT);
BOOL      __stdcall  SalTblQueryFocus(HWND, LPLONG, LPHWND);
BOOL      __stdcall  SalTblQueryLinesPerRow(HWND, LPINT);
INT       __stdcall  SalTblQueryLockedColumns(HWND);
BOOL      __stdcall  SalTblQueryPopupEditColumn(HWND, LPDWORD, LPINT);
BOOL      __stdcall  SalTblQueryRowFlags(HWND, LONG, WORD);
BOOL      __stdcall  SalTblQueryRowHeader(HWND, LPHSTRING, INT, LPINT, LPWORD, LPHWND);
BOOL      __stdcall  SalTblQueryScroll(HWND, LPLONG, LPLONG, LPLONG);
BOOL      __stdcall  SalTblQuerySplitWindow(HWND, LPINT, LPBOOL);
BOOL      __stdcall  SalTblQueryTableFlags(HWND, WORD);
BOOL      __stdcall  SalTblQueryVisibleRange(HWND, LPLONG, LPLONG);
BOOL      __stdcall  SalTblReset(HWND);
BOOL      __stdcall  SalTblScroll(HWND, LONG, HWND, WORD);
BOOL      __stdcall  SalTblSetCellTextColor(HWND, DWORD, BOOL);
BOOL      __stdcall  SalTblSetColumnFlags(HWND, DWORD, BOOL);
BOOL      __stdcall  SalTblSetColumnPos(HWND, INT);
BOOL      __stdcall  SalTblSetColumnText(HWND, UINT, LPSTR);
BOOL      __stdcall  SalTblSetColumnTitle(HWND, LPSTR);
BOOL      __stdcall  SalTblSetColumnWidth(HWND, NUMBER);
BOOL      __stdcall  SalTblSetContext(HWND, LONG);
BOOL      __stdcall  SalTblSetFlagsAnyRows(HWND, WORD, BOOL, WORD, WORD);
BOOL      __stdcall  SalTblSetFocusCell(HWND, LONG, HWND, INT, INT);
BOOL      __stdcall  SalTblSetFocusRow(HWND, LONG);
BOOL      __stdcall  SalTblSetLinesPerRow(HWND, INT);
BOOL      __stdcall  SalTblSetLockedColumns(HWND, INT);
BOOL      __stdcall  SalTblSetRange(HWND, LONG, LONG);
LONG      __stdcall  SalTblSetRow(HWND, INT);
BOOL      __stdcall  SalTblSetRowFlags(HWND, LONG, WORD, BOOL);
BOOL      __stdcall  SalTblSetTableFlags(HWND, WORD, BOOL);
BOOL      __stdcall  SalTblSortRows(HWND, WORD, INT);
BOOL      __stdcall  SalTimerKill(HWND, INT);
BOOL      __stdcall  SalTimerSet(HWND, INT, WORD);
BOOL      __stdcall  SalTrackPopupMenu(HWND, LPSTR, WORD, INT, INT);
BOOL      __stdcall  SalUpdateWindow(HWND);
BOOL      __stdcall  SalUseRegistry(BOOL, LPSTR);
BOOL      __stdcall  SalValidateSet(HWND, BOOL, LONG);
BOOL      __stdcall  SalWaitCursor(BOOL);
BOOL      __stdcall  SalWinHelp(HWND, LPSTR, WORD, DWORD, LPSTR);
HSTRING   __stdcall  SalWindowClassName(HWND);
BOOL      __stdcall  SalWindowGetProperty(HWND, LPSTR, LPHSTRING);
UINT      __stdcall  SalWindowHandleToNumber(HWND);
BOOL      __stdcall  SalWindowIsDerivedFromClass(HWND, TEMPLATE);
BOOL      __stdcall  SalYieldEnable(BOOL);
BOOL      __stdcall  SalYieldQueryState(void);
BOOL      __stdcall  SalYieldStartMessages(HWND);
BOOL      __stdcall  SalYieldStopMessages(void);
BOOL      __stdcall  SqlClearImmediate(void);
BOOL      __stdcall  SqlClose(SQLHANDLENUMBER);
BOOL      __stdcall  SqlCloseAllSPResultSets(SQLHANDLENUMBER);
BOOL      __stdcall  SqlCommit(SQLHANDLENUMBER);
BOOL      __stdcall  SqlCommitSession(SESSIONHANDLENUMBER);
BOOL      __stdcall  SqlConnect(LPSQLHANDLENUMBER);
BOOL      __stdcall  SqlConnectTransaction(LPSQLHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlCreateSession(LPSESSIONHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlCreateStatement(SESSIONHANDLENUMBER, LPSQLHANDLENUMBER);
BOOL      __stdcall  SqlDirectoryByName(LPSTR, HARRAY);
BOOL      __stdcall  SqlDisconnect(LPSQLHANDLENUMBER);
BOOL      __stdcall  SqlDropStoredCmd(SQLHANDLENUMBER, LPSTR);
INT       __stdcall  SqlError(SQLHANDLENUMBER);
BOOL      __stdcall  SqlErrorText(INT, INT, LPHSTRING, INT, LPINT);
BOOL      __stdcall  SqlExecute(SQLHANDLENUMBER);
BOOL      __stdcall  SqlExecutionPlan(SQLHANDLENUMBER, LPHSTRING, INT);
BOOL      __stdcall  SqlExists(LPSTR, LPINT);
BOOL      __stdcall  SqlExtractArgs(WPARAM, LPARAM, LPSQLHANDLENUMBER, LPINT, LPINT);
BOOL      __stdcall  SqlFetchNext(SQLHANDLENUMBER, LPINT);
BOOL      __stdcall  SqlFetchPrevious(SQLHANDLENUMBER, LPINT);
BOOL      __stdcall  SqlFetchRow(SQLHANDLENUMBER, LONG, LPINT);
BOOL      __stdcall  SqlFreeSession(LPSESSIONHANDLENUMBER);
BOOL      __stdcall  SqlGetCmdOrRowsetPtr(SQLHANDLENUMBER, BOOL, LPLONG);
BOOL      __stdcall  SqlGetDSOrSessionPtr(SESSIONHANDLENUMBER, BOOL, LPLONG);
BOOL      __stdcall  SqlGetError(SQLHANDLENUMBER, LPLONG, LPHSTRING);
BOOL      __stdcall  SqlGetErrorPosition(SQLHANDLENUMBER, LPINT);
BOOL      __stdcall  SqlGetErrorText(INT, LPHSTRING);
HSTRING   __stdcall  SqlGetErrorTextX(INT);
HSTRING   __stdcall  SqlGetLastStatement(void);
BOOL      __stdcall  SqlGetModifiedRows(SQLHANDLENUMBER, LPLONG);
BOOL      __stdcall  SqlGetNextSPResultSet(SQLHANDLENUMBER, LPSTR, LPBOOL);
BOOL      __stdcall  SqlGetParameter(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING);
BOOL      __stdcall  SqlGetParameterAll(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING, BOOL);
BOOL      __stdcall  SqlGetResultSetCount(SQLHANDLENUMBER, LPLONG);
BOOL      __stdcall  SqlGetRollbackFlag(SQLHANDLENUMBER, LPBOOL);
BOOL      __stdcall  SqlGetSessionErrorInfo(SESSIONHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING);
BOOL      __stdcall  SqlGetSessionHandle(SQLHANDLENUMBER, LPSESSIONHANDLENUMBER);
BOOL      __stdcall  SqlGetSessionParameter(SESSIONHANDLENUMBER, WORD, LPLONG, LPHSTRING);
BOOL      __stdcall  SqlGetStatementErrorInfo(SQLHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING);
BOOL      __stdcall  SqlImmediate(LPSTR);
BOOL      __stdcall  SqlOpen(SQLHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlOraPLSQLExecute(SQLHANDLENUMBER);
BOOL      __stdcall  SqlOraPLSQLPrepare(SQLHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlOraPLSQLStringBindType(SQLHANDLENUMBER, LPSTR, INT);
BOOL      __stdcall  SqlPLSQLCommand(SQLHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlPrepare(SQLHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlPrepareAndExecute(SQLHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlPrepareSP(SQLHANDLENUMBER, LPSTR, LPSTR);
BOOL      __stdcall  SqlRetrieve(SQLHANDLENUMBER, LPSTR, LPSTR, LPSTR);
BOOL      __stdcall  SqlRollbackSession(SESSIONHANDLENUMBER);
BOOL      __stdcall  SqlSetInMessage(SQLHANDLENUMBER, INT);
BOOL      __stdcall  SqlSetIsolationLevel(SQLHANDLENUMBER, LPSTR);
BOOL      __stdcall  SqlSetLockTimeout(SQLHANDLENUMBER, INT);
BOOL      __stdcall  SqlSetLongBindDatatype(INT, INT);
BOOL      __stdcall  SqlSetOutMessage(SQLHANDLENUMBER, INT);
BOOL      __stdcall  SqlSetParameter(SQLHANDLENUMBER, WORD, LONG, HSTRING);
BOOL      __stdcall  SqlSetParameterAll(SQLHANDLENUMBER, WORD, LONG, HSTRING, BOOL);
BOOL      __stdcall  SqlSetResultSet(SQLHANDLENUMBER, BOOL);
BOOL      __stdcall  SqlSetSessionParameter(SESSIONHANDLENUMBER, WORD, LONG, HSTRING);
BOOL      __stdcall  SqlStore(SQLHANDLENUMBER, LPSTR, LPSTR);
BOOL      __stdcall  SqlVarSetup(SQLHANDLENUMBER);
LPSTR __stdcall SWinStringGetBuffer(HSTRING, LPLONG);
BOOL __stdcall SWinHSCreateDesignHeap(void);
BOOL __stdcall SWinHSDestroyDesignHeap(void);
BOOL __stdcall SWinInitLPHSTRINGParam(LPHSTRING, LONG);
HSTRING __stdcall SalHStringUnRef(HSTRING hString);
void __stdcall SalHStringRef(HSTRING hString);
BOOL __stdcall  SWinMDArrayDateType(HARRAY, LPINT);
BOOL __cdecl SWinMDArrayGetUdv(HARRAY, LPHUDV, LONG,...);
BOOL __cdecl SWinMDArrayGetHandle(HARRAY, LPHANDLE, LONG,...);
BOOL __cdecl SWinMDArrayGetHString(HARRAY, LPHSTRING, LONG,...);
BOOL __cdecl SWinMDArrayGetNumber(HARRAY, LPNUMBER, LONG,...);
BOOL __cdecl SWinMDArrayGetBoolean(HARRAY, LPBOOL, LONG,...);
BOOL __cdecl SWinMDArrayGetDateTime(HARRAY, LPDATETIME, LONG,...);
BOOL __cdecl SWinMDArrayPutHandle(HARRAY, HANDLE, LONG,...);
BOOL __cdecl SWinMDArrayPutHString(HARRAY, HSTRING, LONG,...);
BOOL __cdecl SWinMDArrayPutNumber(HARRAY, LPNUMBER, LONG,...);
BOOL __cdecl SWinMDArrayPutBoolean(HARRAY, BOOL, LONG,...);
BOOL __cdecl SWinMDArrayPutDateTime(HARRAY, LPDATETIME, LONG,...);
BOOL __stdcall  SWinCvtIntToNumber(INT, LPNUMBER);
BOOL __stdcall  SWinCvtWordToNumber(WORD, LPNUMBER);
BOOL __stdcall  SWinCvtLongToNumber(LONG, LPNUMBER);
BOOL __stdcall  SWinCvtULongToNumber(ULONG, LPNUMBER);
BOOL __stdcall  SWinCvtDoubleToNumber(double, LPNUMBER);
BOOL __stdcall  SWinCvtNumberToInt(LPNUMBER, LPINT);
BOOL __stdcall  SWinCvtNumberToWord(LPNUMBER, LPWORD);
BOOL __stdcall  SWinCvtNumberToLong(LPNUMBER, LPLONG);
BOOL __stdcall  SWinCvtNumberToULong(LPNUMBER, LPDWORD);
BOOL __stdcall  SWinCvtNumberToDouble(LPNUMBER, double  *);
LPSTR   __stdcall SWinUdvDeref( HUDV );
HUDV __stdcall SalUdvGetCurrentHandle(void);
LPSTR __stdcall SalGetUDVData(HANDLE h);
void  __stdcall SalAssignUDV(LPHANDLE A, LPHANDLE B, WORD fgs);
LPSTR __stdcall SalNewUDVObject(LPSTR);
BOOL __stdcall SalIsResourceMode();
BOOL __stdcall SWinIsOurWindow(HWND hWnd);
LONG __stdcall SWinGetType(HWND hWnd);
int  __stdcall SWinGetSymbol(HWND hWnd, LPSTR lpszSymbol, int nMaxLength);
HWND __stdcall SWinFindWindow(HWND hWndContainer, LPSTR lpszSymbol);
HWND __stdcall SWinAppFind(LPSTR lpModuleName, BOOL bActivate);
}