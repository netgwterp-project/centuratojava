/* $Header: /Matterhorn/SQLWindows/include/cbtype.h 4     6/11/99 3:16p Rgurumur $ */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
 *  cbtype.h                                                               *
 *  Centura Builder Type Definitions For DLL Interface                     *
 *  Copyright (c) 1999 Centura Software Corporation.  All Rights Reserved. *
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#ifndef CBTYPE_H
//#define CBTYPE_H

#ifdef _MSC_VER
#pragma warning (disable:4209)  /* nonstandard extension used : benign typedef redefinition */
#endif

///////////////////////////////////////////////////////////////////////////////

/***** All these must match SQLWIN:misc.h. *****/
#define TYPEALL(name) \
typedef struct tag##name        name; \
typedef name *                  P##name; \
typedef name NEAR*              NP##name; \
typedef name FAR*               LP##name; \
typedef name HUGE*              HP##name 

#ifndef NUMITEMTYPEDEF
#define NUMITEMTYPEDEF
typedef int NUMITEMTYPE;
#endif

#ifndef SYMIDDEF
#define SYMIDDEF
typedef DWORD   SYMID;  
typedef SYMID *PSYMID;
#endif

#ifndef NUMBER_H
#include "number.h"
#endif

/* DATETIME */

#ifndef DATETIMESIZE
#include "datatype.h"
#endif

#define DTGETLENGTH(dt) ((dt)->DATETIME_Length & DTLENGTHMASK)

typedef HANDLE HARRAY;
typedef HARRAY *LPHARRAY;

///////////////////////////////////////////////////////////////////////////////
// public definitions of private data types
//#define INTERNAL
#ifndef INTERNAL

#define _DEF_HITEM
DECLARE_HANDLE(HITEM);
typedef HITEM *LPHITEM;
typedef HITEM* PHITEM;

#define _DEF_HOUTLINE
DECLARE_HANDLE(HOUTLINE);
typedef HOUTLINE *LPHOUTLINE;

typedef ULONG HSTRING;

/* Ref to item in a dynalib. */
typedef DWORD       LIBHITEM,
                   *LPLIBHITEM;

typedef VOID* HFFILE;

typedef struct 
{
  DWORD farTemplate;
  BYTE tag;
}     TEMPLATETAGGED,
      HITEMTAGGED,
	  *LPTEMPLATETAGGED,
	  *LPHITEMTAGGED;

#endif // INTERNAL

///////////////////////////////////////////////////////////////////////////////

typedef LPSTR *LPLPSTR;
typedef HWND *LPHWND;

typedef int SQLCURSORNUMBER;

#define HARRAYDEF

/*
  Templates and HITEM's are two names for the same thing.
*/
typedef DWORD TEMPLATE;

typedef int SQLHANDLENUMBER;
typedef int SESSIONHANDLENUMBER;


/*
  Handle to UDV, user defined variable.
  A UDV is an instance of a Functional Class.
*/
typedef struct
{
  BYTE opaque[12];
} HUDV,
  *LPHUDV;

typedef HWND *PHWND;
typedef CHAR *LPCHAR;
typedef double *LPDOUBLE;
typedef float *LPFLOAT;
typedef ULONG *LPULONG;
typedef HFFILE *LPHFFILE;
typedef HSTRING *LPHSTRING;
typedef SQLHANDLENUMBER *LPSQLHANDLENUMBER;
typedef SESSIONHANDLENUMBER *LPSESSIONHANDLENUMBER;

typedef BOOL   *NPBOOL;
typedef BYTE   *NPBYTE;
typedef CHAR   *NPCHAR;
typedef double *NPDOUBLE;
typedef DWORD  *NPDWORD;
typedef float  *NPFLOAT;
typedef INT    *NPINT;
typedef LONG   *NPLONG;
typedef VOID   *NPVOID;
typedef WORD HDB;
typedef HDB *LPHDB;         // database handle

// Must match SAL.H
typedef BOOL(*LPPROCDDMWRITECALLBACK)(LPVOID, LPSTR);
typedef BOOL(*LPPROCDDMREADCALLBACK)(LPVOID, LPLPSTR);
typedef BOOL(*LPPROCDDMDISCARDCALLBACK) (LPVOID);
typedef BOOL(*LPPROCDDMPREPARECALLBACK) (LPSTR, BOOL, LPVOID, LPWORD,
				 LPBOOL, LPINT, LPINT);

#define DDM_ERROR_NO_HANDLER 0
#define DDM_ERROR_NO_RETURN  1
#define DDM_ERROR_RETURNED   2
typedef WORD(*LPPROCDDMERRORCALLBACK) ( WPARAM, LPARAM, LPLONG );


#ifdef _MSC_VER
#pragma warning (default:4209)  /* nonstandard extension used : benign typedef redefinition */
#endif

#endif                          /* CBTYPE_H */

/*
 * $History: cbtype.h $
 * 
 * *****************  Version 4  *****************
 * User: Rgurumur     Date: 6/11/99    Time: 3:16p
 * Updated in $/Matterhorn/SQLWindows/include
 * Session Handle changes
 * 
 * *****************  Version 3  *****************
 * User: Omullarn     Date: 5/18/99    Time: 4:26p
 * Updated in $/Matterhorn/SQLWindows/include
 * Fixed includes for internal consistency
 * 
 * *****************  Version 6  *****************
 * User: Jmundsto     Date: 11/12/98   Time: 6:19p
 * Updated in $/Jungfrau/sqlwin/SRC/H
 * Support for longer numbers.
 * 
 * *****************  Version 5  *****************
 * User: Omullarn     Date: 11/05/98   Time: 4:18p
 * Updated in $/Jungfrau/sqlwin/SRC/H
 * Making SalActiveX functions internal: restored LPDOUBLE typedef
 * 
 * *****************  Version 3  *****************
 * User: Tmcparla     Date: 11/14/97   Time: 4:35p
 * Updated in $/Phoenix/sqlwin/SRC/H
 * Changes for Visual C++ 5.0.
 * 
 * *****************  Version 2  *****************
 * User: Omullarn     Date: 11/07/97   Time: 3:33p
 * Updated in $/Phoenix/sqlwin/SRC/H
 * Tomahawk/Patriot merge
 * 
 * *****************  Version 1  *****************
 * User: Jlacerda     Date: 11/04/97   Time: 5:34p
 * Created in $/Phoenix/sqlwin/SRC/H
 * 
 * *****************  Version 3  *****************
 * User: Rjacobs      Date: 2/07/97    Time: 11:06p
 * Updated in $/Patriot/sqlwin/SRC/H
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 6/18/96    Time: 6:52p
 * Branched in $/Patriot/sqlroute/odbsal32
 * Stinger project branched for Patriot
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 2/28/96    Time: 5:32p
 * Updated in $/WIN32/SqlRouter/odbsal32
 * From initial 10237 build.
 * 
 * *****************  Version 1  *****************
 * User: Joropeza     Date: 2/22/96    Time: 3:54a
 * Created in $/WIN32/SQLWIN/src/h
 * replaces swtype.h
 * 
*/
