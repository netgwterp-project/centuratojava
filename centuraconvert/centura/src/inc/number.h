/* $Header: /Jungfrau/sqlwin/GTIDEV/GTIINC/number.h 2     11/13/98 3:44p Jlacerda $ */
#ifndef NUMBER_H
#define NUMBER_H

// include windows.h for definitions of data types used in this file
// include it using winbind.h to suppress compiler warnings
#include <winbind.h>

/*  define number data structure as seen from outside the numeric package */

#define NUMBERSIZE 24

typedef struct 
{
  BYTE numLength;               /* size of number */
  BYTE numValue[NUMBERSIZE];      /* value of number */
} NUMBER;
typedef NUMBER far *LPNUMBER;  /* pointer to number */
typedef NUMBER *PNUMBER;       /* pointer to number */
typedef const NUMBER far *LPCNUMBER; /* pointer to constant number */

/* test number for being null */
#define NumberIsNull(lpNum) (lpNum->numLength  == 0)
#endif
