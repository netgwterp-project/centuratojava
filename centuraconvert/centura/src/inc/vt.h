/*-------------------------------------------------------------------------------
  Name.........: vt.h
  Description..: All definitions to allow Visual Toolchest calls from C
  Author.......: Steve Hlavacek, Copyright 1994, Graphical Business Systems, Inc.

       $Archive: /Matterhorn/Components/VT/vt.h $
      $Revision: 3 $
        $Author: Shailesh $
          $Date: 12/02/99 3:34p $
  -------------------------------------------------------------------------------*/


#ifndef VT_H



#pragma warning(disable: 4001)  // Nonstandard extension 'single line comment'


// Centura data types
#include "cbtype200.h"  // Updated name.  BLG (2/23/96)


//------------------------------------------------------
// General MetaConstants
//------------------------------------------------------
#define INVALID_ON               TRUE   // Used to indicate that InvalidateRect() is requested
#define INVALID_OFF              FALSE
#define MANDATORY_ON             TRUE   // Used to indicate that response is required
#define MANDATORY_OFF            FALSE
#define UPDATE_ON                TRUE   // Used to indicate that UpdateWindow() is requested
#define UPDATE_OFF               FALSE

//------------------------------------------------------
// Recoverable error action code
//------------------------------------------------------
#define ACTION_Retry             (1)
#define ACTION_Abort             (2)

//------------------------------------------------------
// Debug Flags
//------------------------------------------------------
#define DBF_IncrementalTime      (0x00010000L)
#define DBF_RealTime             (0x00020000L)
#define DBF_MessageBox           (0x00080000L)

//------------------------------------------------------
// DOS Flags
//------------------------------------------------------
#define DOS_VerifyFlag           (0x0001)
#define DOS_BreakFlag            (0x0002)
#define DOS_ErrorMode            (0x0004)

//------------------------------------------------------
// DRIVE Type Flags
//------------------------------------------------------
#define DRIVE_Floppy             DRIVE_REMOVABLE
#define DRIVE_Fixed              DRIVE_FIXED
#define DRIVE_Network            DRIVE_REMOTE
#ifndef DRIVE_CDROM
#define DRIVE_CDROM              (5)
#endif
#define DRIVE_RAM                (6)

//------------------------------------------------------
// DROP Flags
//------------------------------------------------------
#define DROP_Child               (0x0001)
#define DROP_Sibling             (0x0002)

//------------------------------------------------------
// Data Type Flags
//------------------------------------------------------
#define DT_Handle                (0x0100)

//------------------------------------------------------
// File Attributes
//------------------------------------------------------
#define FA_Volume                (0x0008)

//------------------------------------------------------
// File Types
//------------------------------------------------------
#define FILETYPE_Program         (1)
#define FILETYPE_Document        (2)
#define FILETYPE_System          (3)
#define FILETYPE_Other           (4)

//------------------------------------------------------
// Font Types
//------------------------------------------------------
#define FONTTYPE_Device          (0x0001)
#define FONTTYPE_Raster          (0x0002)
#define FONTTYPE_TrueType        (0x0004)

//------------------------------------------------------
// Font Get Flags
//------------------------------------------------------
#define FONT_GetScreen           (0x0001)
#define FONT_GetPrinter          (0x0002)

//------------------------------------------------------
// Item Flags
//------------------------------------------------------
#define ITEM_IsDisabled          (0x01000000L)
#define ITEM_IsHidden            (0x02000000L)
#define ITEM_IsSelected          (0x04000000L)
#define ITEM_IsFocus             (0x08000000L)
#define ITEM_IsFrame             (0x10000000L)
#define ITEM_IsDeleted           (0x20000000L)
#define ITEM_IsParent            (0x40000000L)
#define ITEM_IsExpanded          (0x80000000L)
#define ITEM_CanPromote          (0x00010000L)
#define ITEM_CanDemote           (0x00020000L)
#define ITEM_CanMoveUp           (0x00040000L)
#define ITEM_CanMoveDown         (0x00080000L)
#define ITEM_CanExpand           (0x00100000L)
#define ITEM_CanCollapse         (0x00200000L)

//------------------------------------------------------
// Key States
//------------------------------------------------------
#define KS_Down                  (0x80)
#define KS_LockOn                (0x01)

//------------------------------------------------------
// Key Down States
//------------------------------------------------------
#define KDS_None                 (0)
#define KDS_Shift                (1)
#define KDS_Ctrl                 (2)
#define KDS_ShiftCtrl            (3)
#define KDS_Alt                  (4)
#define KDS_AltShift             (5)
#define KDS_AltCtrl              (6)
#define KDS_ShiftAltCtrl         (7)

//------------------------------------------------------
// Calendar Notification Messages
//------------------------------------------------------
#define CALN_ERRSPACE           (-1)  // Control ran out of memory
#define CALN_DATECHANGED         (1)  // Focus date changed
#define CALN_DATEFINISHED        (2)  // Control no longer in use
#define CALN_SCROLL              (3)  // Display changed

//------------------------------------------------------
// ColorPalette Notification Messages
//------------------------------------------------------
#define PALN_ERRSPACE           (-1)  // Control ran out of memory
#define PALN_CHANGED             (1)  // Selection changed
#define PALN_FINISHED            (2)  // Selection completed
#define PALN_MENU                (3)  // Menu chosen

//------------------------------------------------------
// ColorPalette Notification Messages
//------------------------------------------------------
#define SPLITN_Moved             (1)  // Control was moved onscreen

//------------------------------------------------------
// HeaderListView Notification Messages
//------------------------------------------------------
#define HLBN_CHANGED             (1)  // CBN_SELCHANGED - Selection changed
#define HLBN_FINISHED            (8)  // CBN_CLOSEUP    - Selection completed

//------------------------------------------------------
// Calendar Styles
//------------------------------------------------------
#define CAL_AutoSize                (0x00000001)
#define CAL_DropDown                (0x00000002)
#define CAL_EnableButtons           (0x00000004)
#define CAL_EnableKeyboard          (0x00000008)
#define CAL_EtchedLook              (0x00000010)
#define CAL_MultiSelect             (0x00000020)
#define CAL_SelectExtra             (0x00000040)
#define CAL_SelectSpecial           (0x00000080)
#define CAL_ShowExtra               (0x00000100)
#define CAL_ShowFocus               (0x00000200)
#define CAL_ShowGrid                (0x00000400)
#define CAL_ShowSpecial             (0x00000800)
#define CAL_AlternateButtons        (0x00001000)
#define CAL_Default                 (CAL_EnableButtons | CAL_EnableKeyboard | CAL_ShowFocus | CAL_ShowExtra | CAL_SelectExtra | CAL_SelectSpecial)

//------------------------------------------------------
// ColorPalette Styles
//------------------------------------------------------
#define PAL_AutoSize                (0x00000001)
#define PAL_DropDown                (0x00000002)
#define PAL_EnableMRUColors         (0x00000004)
#define PAL_NoHideSelects           (0x00000008)

//------------------------------------------------------
// EditColor Styles
//------------------------------------------------------
#define EDCLR_ShowCaption           (0x00000001)

//------------------------------------------------------
// List Box Styles
//------------------------------------------------------
#define LBS_SubClassMask            (0x08FFFFFFL)
#define LBS_VisExtensions           (0x00000001L)
#define LBS_FmtUppercase            (0x00000002L)
#define LBS_FmtLowercase            (0x00000004L)
#define LBS_ShowSelectColor         (0x00000008L)
#define LBS_ShowSelectText          (0x00000010L)
#define LBS_ShowPicCenter           (0x00000020L)
#define LBS_ShowNoTree              (0x00000040L)
#define LBS_ShowNoEditSelect        (0x00000080L)
#define LBS_ShowNoEditText          (0x00000100L)
#define LBS_KillFocusNoSelect       (0x00000200L)
#define LBS_KillFocusFrameSelect    (0x00000400L)
#define LBS_SelectDescendents       (0x00000800L)
#define LBS_SetWidthEachInsert      (0x00001000L)
#define LBS_ShowNoSelect            (0x00002000L)
#define LBS_ShowSelectTextPicture   (0x00004000L)
#define LBS_ShowSelectBkgd          (0x00008000L)
#define LBS_ShowHorzScrollBar       (0x00010000L)
#define LBS_Explorer                (0x00020000L)
#define LBS_FmtMixedcase            (0x00040000L)

#define LBS_NoSubClassMask          (0xF7000000L)
#define LBS_DragSelect              (0x01000000L)
#define LBS_DisableNoScroll         (0x02000000L)
#define LBS_NoAutoResize            (0x04000000L)
#define LBS_ExtendedCombo           (0x08000001L)

//------------------------------------------------------
// Message Box Flags
//------------------------------------------------------
#define MBF_IconAsterisk         (0x00010000)
#define MBF_IconHand             (0x00020000)
#define MBF_IconQuestion         (0x00040000)
#define MBF_IconExclamation      (0x00080000)
#define MBF_DefButton            (0x00000001)

//------------------------------------------------------
// Menu Flags
//------------------------------------------------------
#define MF_Disable               (0x0003)
#define MF_Check                 (0x0008)
#define MF_Separator             (0x0800)

//------------------------------------------------------
// Picture Constants
//------------------------------------------------------
#define PIC_LoadSWinStr          (0x0100)
#define PIC_LoadResource         (0x0200)
#define PIC_LoadFile             (0x0400)
#define PIC_LoadSWinRes          (0x0800)
#define PIC_LoadSmallIcon        (0x1000)  // Added constant. BLG (10/10/95)
#define PIC_LoadLargeIcon        (0x2000)  // Added constant. BLG (10/10/95)

//------------------------------------------------------
// Row Flags
//------------------------------------------------------
#define ROW_UserFlag1            (0x0080)
#define ROW_UserFlag2            (0x0100)
#define ROW_UserFlag3            (0x0200)
#define ROW_UserFlag4            (0x0400)
#define ROW_UserFlag5            (0x0800)

//------------------------------------------------------
// Seek Flags
//------------------------------------------------------
#define SEEK_Begin               (0)
#define SEEK_Current             (1)
#define SEEK_End                 (2)

//------------------------------------------------------
// Show Flags
//------------------------------------------------------
#define SHOW_Hidden              (0)
#define SHOW_Normal              (1)
#define SHOW_Minimized           (2)
#define SHOW_Maximized           (3)
#define SHOW_AllVisible          (32766)
#define SHOW_AllLevels           (32767)

//------------------------------------------------------
// Sort Flags
//------------------------------------------------------
#define SORT_Ascending           (0)
#define SORT_Descending          (1)

//------------------------------------------------------
// Error Return Codes
//------------------------------------------------------
#define VTERR_Ok                  (0)
#define VTERR_FileNotFound        (-2)
#define VTERR_PathNotFound        (-3)
#define VTERR_TooManyFiles        (-4)
#define VTERR_AccessDenied        (-5)
#define VTERR_InvalidHandle       (-6)
#define VTERR_NoMemory            (-8)
#define VTERR_InvalidData         (-13)
#define VTERR_InvalidDrive        (-15)
#define VTERR_NoMoreFiles         (-18)
#define VTERR_EndOfFile           (-18)
#define VTERR_WriteProtected      (-19)
#define VTERR_DriveNotReady       (-21)
#define VTERR_WriteFault          (-29)
#define VTERR_ReadFault           (-30)
#define VTERR_SharingViolation    (-32)
#define VTERR_LockViolation       (-33)
#define VTERR_DiskFull            (-39)
#define VTERR_BadNetPath          (-53)
#define VTERR_NetworkBusy         (-54)
#define VTERR_NetNameDeleted      (-64)
#define VTERR_NetworkAccessDenied (-65)
#define VTERR_BadNetName          (-67)
#define VTERR_InvalidParameter    (-87)
#define VTERR_NetWriteFault       (-88)
#define VTERR_NetworkUnreachable  (-1231)

//------------------------------------------------------
// Predefined pictures
//------------------------------------------------------
#define VTPIC_BookClosed            _T("bkclosed")
#define VTPIC_BookOpen              _T("bkopen")
#define VTPIC_BookShelf2            _T("bkshelf2")
#define VTPIC_BookShelf3            _T("bkshelf3")
#define VTPIC_CheckBlack            _T("chkblk")
#define VTPIC_CheckWhite            _T("chkwht")
#define VTPIC_Desktop               _T("desktop")
#define VTPIC_DiamondBlackClosed    _T("diablkc")
#define VTPIC_DiamondBlackOpen      _T("diablko")
#define VTPIC_DiamondWhiteClosed    _T("diawhtc")
#define VTPIC_DiamondWhiteOpen      _T("diawhto")
#define VTPIC_DriveCDROM            _T("drvcd2")
#define VTPIC_DriveCDROM2           _T("drvcd3")
#define VTPIC_DriveFloppy           _T("drvflop2")
#define VTPIC_DriveFixed            _T("drvhard2")
#define VTPIC_DriveNetwork          _T("drvnet2")
#define VTPIC_DriveNetworkX         _T("drvnetx")
#define VTPIC_DriveRAM              _T("drvram2")
#define VTPIC_Empty                 _T("empty")
#define VTPIC_FileAllOther          _T("fileall")
#define VTPIC_FileDocuments         _T("filedoc")
#define VTPIC_FilePrograms          _T("fileprog")
#define VTPIC_FileSystem            _T("filesys")
#define VTPIC_Folder                _T("fdr")
#define VTPIC_Folder2               _T("fdr")
#define VTPIC_FolderCur             _T("fdrcur")
#define VTPIC_FolderCurMinus        _T("fdrcurm")
#define VTPIC_FolderCurPlus         _T("fdrcurp")
#define VTPIC_FolderMinus           _T("fdrmnus")
#define VTPIC_FolderOpen            _T("fdropen")
#define VTPIC_FolderOpen2           _T("fdropen")
#define VTPIC_FolderPlus            _T("fdrplus")
#define VTPIC_FontPrinter           _T("fonprtr")
#define VTPIC_FontTrueType          _T("fontrtyp")
#define VTPIC_MinusDark             _T("minusdk")
#define VTPIC_MinusLight            _T("minuslt")
#define VTPIC_MinusWhite            _T("minuswht")
#define VTPIC_MyComputer            _T("mycomp")
#define VTPIC_NetworkNeighborhood   _T("netneigh")
#define VTPIC_PlusDark              _T("plusdk")
#define VTPIC_PlusLight             _T("pluslt")
#define VTPIC_PlusWhite             _T("pluswht")
#define VTPIC_PrevDirectory         _T("prevdir")
#define VTPIC_RadioOn               _T("radon2")
#define VTPIC_RadioOff              _T("radoff2")
#define VTPIC_CheckBoxOn            _T("chkbxon")
#define VTPIC_CheckBoxOff           _T("chkbxoff")

//------------------------------------------------------
// Messages
//------------------------------------------------------
#define VTM_Base                (0x6000)
#define VTM_AddString           (VTM_Base + 1)
#define VTM_InsertString        (VTM_Base + 2)
#define VTM_Click               (VTM_Base + 3)
#define VTM_Destroy             (VTM_Base + 4)
#define VTM_Populate            (VTM_Base + 5)
#define VTM_Accelerator         (VTM_Base + 6)
#define VTM_MenuSelect          (VTM_Base + 7)
#define VTM_ControlCreate       (VTM_Base + 8)
#define VTM_OutlineCornerClick  (VTM_Base + 9)
#define VTM_CreateComplete      (VTM_Base + 10)
#define VTM_DirListPath         (VTM_Base + 11)

//------------------------------------------------------
// Wait cursor flags
//------------------------------------------------------
#define WAIT_TempOn              (2)
#define WAIT_TempOff             (3)
#define WAIT_SysModalOn          (4)
#define WAIT_SysModalOff         (5)

//------------------------------------------------------
// Window Flags
//------------------------------------------------------
#define WF_Required              (0x00000001L)
#define WF_NoClear               (0x00000002L)
#define WF_NoClearEditFlag       (0x00000004L)
#define WF_FitPath               (0x00000008L)
#define WF_DisplayOnly           (0x00000010L)
#define WF_EnableWhenNotNull     (0x00010000L)

//------------------------------------------------------
// Handles
//------------------------------------------------------
typedef DWORD     HSUBMEM;    // HIWORD - Global handle of segment, LOWORD - Local handle of alloction

DECLARE_HANDLE(HACC);
DECLARE_HANDLE(HFC);
DECLARE_HANDLE(HSTBL);


// **********************************
// *** Structure pre-declarations ***
// **********************************

struct  BTN;
typedef BTN FAR *LPBTN;
class   PIC;
typedef PIC FAR *LPPIC;
struct  TREE;
typedef TREE FAR *LPTREE;
class   CPicItemArray;
typedef CPicItemArray FAR *LPCPicItemArray;


#ifdef __cplusplus
extern "C" {
#endif

// ***************************
// *** Function Prototypes ***
// ***************************

LONG            WINAPI VisPopulateFontNames           (HWND hwnd, int nGetFlag);
LONG            WINAPI VisPopulateDrives              (HWND hwnd);
LONG            WINAPI VisPopulateCommonDir           (HWND hwnd, LPCTSTR lpctszCurDir);
LONG            WINAPI VisPopulateDirTree             (HWND hwnd, LPCTSTR lpctszDrive);
LPCPicItemArray WINAPI VisPopulatePictureFromProperty (HWND hwndObject, HWND hwndDisplay);  // Changed from HSTRING.  BLG (9/5/95)
HSTRING         WINAPI VisGetCommonDir                (HWND hwnd, int nLastIndex);
HSTRING         WINAPI VisSetCommonDir                (HWND hwnd, int nIndex);

LONG            WINAPI VisArrayAppend                 (HARRAY harySource, HARRAY haryTarget, int nDataType);
LONG            WINAPI VisArrayCopy                   (HARRAY harySource, HARRAY haryTarget, int nDataType);
LONG            WINAPI VisArrayDeleteItem             (HARRAY hary, LONG lIndex, int nDataType);
BOOL            WINAPI VisArrayFillDateTime           (HARRAY hary, DATETIME dtValue, LONG lCount);
BOOL            WINAPI VisArrayFillNumber             (HARRAY hary, NUMBER numValue, LONG lCount);
BOOL            WINAPI VisArrayFillString             (HARRAY hary, LPCTSTR lpctszValue, LONG lCount);
LONG            WINAPI VisArrayFindDateTime           (HARRAY hary, DATETIME dtSearchFor);
LONG            WINAPI VisArrayFindNumber             (HARRAY hary, NUMBER numSearchFor);
LONG            WINAPI VisArrayFindString             (HARRAY hary, LPCTSTR lpctszSearchFor);
LONG            WINAPI VisArrayInsertItem             (HARRAY hary, LONG lIndex, int nDataType);
LONG            WINAPI VisArraySort                   (HARRAY hary, int nSortOrder, int nDataType);

BOOL            WINAPI VisDebugAssert                 (BOOL bExpression, LPCTSTR lpctszDebugMsg);
BOOL            WINAPI VisDebugBeginTime              ();
BOOL            WINAPI VisDebugEndTime                (LPCTSTR lpctszContext, WORD wDebugLevel);
DWORD           WINAPI VisDebugGetFlags               ();
DWORD           WINAPI VisDebugSetFlags               (DWORD dwFlags, BOOL bMode);
BOOL            WINAPI VisDebugSetLevel               (WORD wDebugLevel);
BOOL            WINAPI VisDebugSetTime                (LPCTSTR lpctszContext);
BOOL            WINAPI VisDebugString                 (LPCTSTR lpctszString, WORD wDebugLevel);

HSTRING         WINAPI VisDosBuildFullName            (LPCTSTR lpctszModuleName, LPCTSTR lpctszFileName);
int             WINAPI VisDosEnumDirs                 (LPCTSTR lpctszDirSpec, HARRAY hary);
int             WINAPI VisDosEnumDirInfo              (LPCTSTR lpctszDirSpec, HARRAY haryDirs, HARRAY haryDateTimes, HARRAY haryAttribs);
int             WINAPI VisDosEnumDrives               (HARRAY hary);
int             WINAPI VisDosEnumFileInfo             (LPCTSTR lpctszDirSpec, WORD wAttribute, HARRAY haryFiles, HARRAY harySizes, HARRAY haryDateTimes, HARRAY haryAttribs);
int             WINAPI VisDosEnumFiles                (LPCTSTR lpctszDirSpec, WORD wAttribute, HARRAY hary);
int             WINAPI VisDosEnumNetConnections       (HARRAY haryDevices, HARRAY haryNetNames);
int             WINAPI VisDosEnumPath                 (HARRAY hary);
BOOL            WINAPI VisDosExist                    (LPCTSTR lpctszTest);
BOOL            WINAPI VisDosIsParent                 (LPCTSTR lpctszDirSpec);
HSTRING         WINAPI VisDosGetCurDir                (LPCTSTR lpctszDrive);
int             WINAPI VisDosGetDriveType             (LPCTSTR lpctszDrive);
int             WINAPI VisDosGetDriveSize             (LPCTSTR lpctszDrive, LPDWORD lpdwTotal, LPDWORD lpdwAvailable);
HSTRING         WINAPI VisDosGetEnvString             (LPCTSTR lpctszKeyword);
WORD            WINAPI VisDosGetFlags                 ();
HSTRING         WINAPI VisDosGetNetName               (LPCTSTR lpctszDevice);
NUMBER          WINAPI VisDosGetVersion               ();
HSTRING         WINAPI VisDosGetVolumeLabel           (LPCTSTR lpctszDrive);
int             WINAPI VisDosMakeAllDir               (LPCTSTR lpctszDir);
HSTRING         WINAPI VisDosMakePath                 (LPCTSTR lpctszDrive, LPCTSTR lpctszDir, LPCTSTR lpctszBase, LPCTSTR lpctszExt);
int             WINAPI VisDosNetConnect               (LPCTSTR lpctszDevice, LPCTSTR lpctszNetName, LPCTSTR lpctszPassword);
int             WINAPI VisDosNetDisconnect            (LPCTSTR lpctszDevice, BOOL bForceClose);
int             WINAPI VisDosSetFlags                 (WORD wFlags, BOOL fState);
int             WINAPI VisDosSetVolumeLabel           (LPCTSTR lpctszDrive, LPCTSTR lpctszLabel);
VOID            WINAPI VisDosSplitPath                (LPCTSTR lpctszPath, LPHSTRING lphDrive, LPHSTRING lphDir, LPHSTRING lphBase, LPHSTRING lphExt);

int             WINAPI VisFileAppend                  (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
int             WINAPI VisFileClose                   (HFC hfc);
int             WINAPI VisFileCopy                    (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
HSTRING         WINAPI VisFileCreateTemp              (LPCTSTR lpctszPrefix);
int             WINAPI VisFileDelete                  (LPCTSTR lpctszFileSpec);
int             WINAPI VisFileExpand                  (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
HSTRING         WINAPI VisFileFind                    (LPCTSTR lpctszFileName);
INT             WINAPI VisFileGetAttribute            (LPCTSTR lpctszFileName );
LONG            WINAPI VisFileGetSize                 (LPCTSTR lpctszFileName );
int             WINAPI VisFileGetType                 (LPCTSTR lpctszFileName);
int             WINAPI VisFileOpen                    (LPHANDLE lphfc, LPCTSTR lpctszFileName, WORD wFlags);
LONG            WINAPI VisFileRead                    (HFC hfc, LPHSTRING lphstrBuffer, DWORD dwBufSize);
int             WINAPI VisFileReadString              (HFC hfc, LPHSTRING lphstr);
int             WINAPI VisFileRename                  (LPCTSTR lpctszSource, LPCTSTR lpctszTarget);
LONG            WINAPI VisFileSeek                    (HFC hfc, LONG lOffset, int nOrigin);
int             WINAPI VisFileSetAttribute            (LPCTSTR lpctszFileSpec, WORD wAttribute, BOOL bState);
int             WINAPI VisFileSetDateTime             (LPCTSTR lpctszFileSpec, DATETIME dtDateTime);
LONG            WINAPI VisFileTell                    (HFC hfc);
int             WINAPI VisFileWrite                   (HFC hfc, HSTRING hstrBuffer, DWORD dwBufSize);
int             WINAPI VisFileWriteString             (HFC hfc, LPCTSTR lpctszString);

LONG            WINAPI VisFontEnum                    (WORD wGetFlag, HARRAY haryFontNames, HARRAY haryFontTypes);
int             WINAPI VisFontFree                    (HFONT hfont);
BOOL            WINAPI VisFontGet                     (HFONT hfont, LPHSTRING lphstrFaceName, LPINT lpiPointSize, LPWORD lpwFontFlags);
HFONT           WINAPI VisFontLoad                    (LPCTSTR lpctszFontName, int nFontSize, WORD wFontEnh);

LONG            WINAPI VisListArrayPopulate           (HWND hwnd, HARRAY hary);
LONG            WINAPI VisListArrayPopulateValue      (HWND hwnd, HARRAY haryText, HARRAY haryValues);
int             WINAPI VisListAddValue                (HWND hwnd, LPCTSTR lpctszText, LONG lValue);
BOOL            WINAPI VisListClearSelection          (HWND hwnd);
LONG            WINAPI VisListDeleteSelected          (HWND hwnd);
LONG            WINAPI VisListFindString              (HWND hwnd, int nIndex, LPCTSTR lpctszText);  // Changed to LPCTSTR.  BLG (5/3/96)
LONG            WINAPI VisListFindValue               (HWND hwnd, LONG lStartIndex, LONG lSearchFor);
BOOL            WINAPI VisListGetDropdownState        (HWND hwnd);
LONG            WINAPI VisListGetFocusIndex           (HWND hwnd);
int             WINAPI VisListGetIndexFromPoint       (HWND hwnd, int nXPos, int nYPos);
HSTRING         WINAPI VisListGetText                 (HWND hwnd, int nIndex);
BOOL            WINAPI VisListGetTextRectangle        (HWND hwnd, int nIndex, LPINT lpnTop, LPINT lpnLeft, LPINT lpnBottom, LPINT lpnRight);
LONG            WINAPI VisListGetValue                (HWND hwnd, int nIndex);
LONG            WINAPI VisListGetVisibleRange         (HWND hwnd, LPINT lpnTop, LPINT lpnBottom);
int             WINAPI VisListInsertValue             (HWND hwnd, int nIndex, LPCTSTR lpctszText, LONG lValue);
BOOL            WINAPI VisListIsMultiSelect           (HWND hwnd);
LONG            WINAPI VisListScroll                  (HWND hwnd, int nIndex);
LONG            WINAPI VisListSetFocusIndex           (HWND hwnd, int nIndex);
BOOL            WINAPI VisListSetDropdownState        (HWND hwnd, BOOL fState);
int             WINAPI VisListSetScrollWidth          (HWND hwnd, int nIndex);
int             WINAPI VisListSetText                 (HWND hwnd, int nIndex, LPCTSTR lpctszText);
int             WINAPI VisListSetValue                (HWND hwnd, int nIndex, LONG lValue);

int             WINAPI VisListDisableItem             (HWND hwnd, int nIndex, COLORREF crColor);
int             WINAPI VisListEnableItem              (HWND hwnd, int nIndex, COLORREF crColor);
LONG            WINAPI VisListGetFlags                (HWND hwnd, int nIndex);
LONG            WINAPI VisListSetFlags                (HWND hwnd, int nIndex, DWORD dwItemFlags, BOOL bState);
DWORD           WINAPI VisListGetStyle                (HWND hwnd);
BOOL            WINAPI VisListSetStyle                (HWND hwnd, DWORD dwSlbStyle, LPCREATESTRUCT lpcs);

LONG            WINAPI VisListAddColor                (HWND hwnd, LPCTSTR lpctszText, COLORREF crColor);
LONG            WINAPI VisListAddColorValue           (HWND hwnd, LPCTSTR lpctszText, COLORREF crColor, LONG lValue);
LONG            WINAPI VisListGetColor                (HWND hwnd, int nIndex);
LONG            WINAPI VisListInsertColor             (HWND hwnd, int nIndex, LPCTSTR lpctszText, COLORREF crColor);
LONG            WINAPI VisListInsertColorValue        (HWND hwnd, int nIndex, LPCTSTR lpctszText, COLORREF crColor, LONG lValue);
BOOL            WINAPI VisListSetColor                (HWND hwnd, int nIndex, COLORREF crColor);

LONG            WINAPI VisListAddFont                 (HWND hwnd, LPCTSTR lpctszText, HFONT hfont);
LONG            WINAPI VisListAddFontValue            (HWND hwnd, LPCTSTR lpctszText, HFONT hfont, LONG lValue);
HFONT           WINAPI VisListGetFont                 (HWND hwnd, int nIndex);
LONG            WINAPI VisListInsertFont              (HWND hwnd, int nIndex, LPCTSTR lpctszText, HFONT hfont);
LONG            WINAPI VisListInsertFontValue         (HWND hwnd, int nIndex, LPCTSTR lpctszText, HFONT hfont, LONG lValue);
BOOL            WINAPI VisListSetFont                 (HWND hwnd, int nIndex, HFONT hfont);

LONG            WINAPI VisListAddPicture              (HWND hwnd, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect);
LONG            WINAPI VisListAddPictureValue         (HWND hwnd, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect, LONG lValue);
INT             WINAPI VisListGetPicture              (HWND hwnd, int nIndex, LPINT lpiNormal, LPINT lpiSelect);
LONG            WINAPI VisListInsertPicture           (HWND hwnd, int nIndex, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect);
LONG            WINAPI VisListInsertPictureValue      (HWND hwnd, int nIndex, LPCTSTR lpctszText, LPPIC ppicNormal, LPPIC ppicSelect, LONG lValue);
BOOL            WINAPI VisListSetPicture              (HWND hwnd, int nIndex, LPPIC ppicNormal, LPPIC ppicSelect);

BOOL            WINAPI VisListCollapse                (HWND hwnd, int nIndex);
int             WINAPI VisListDeleteChild             (HWND hwnd, int nIndex);
int             WINAPI VisListDeleteDescendents       (HWND hwnd, int nIndex);
BOOL            WINAPI VisListDemote                  (HWND hwnd, int nIndex, LPPIC ppicNormal, LPPIC ppicSelect);
BOOL            WINAPI VisListDragDrop                (HWND hwndSource, int nSourceIndex, HWND hwndTarget, int nTargetIndex, WORD wDropFlags);
LONG            WINAPI VisListEnumChildren            (LPTREE ptree, HARRAY haryItems);
LONG            WINAPI VisListEnumDescendents         (LPTREE ptree, HARRAY haryItems);
int             WINAPI VisListExpand                  (HWND hwnd, int nExpIndex);
BOOL            WINAPI VisListExpandDescendents       (HWND hwnd, int nExpIndex);
LPTREE          WINAPI VisListFindItemValue           (LPTREE ptree, LONG lSearchFor);
BOOL            WINAPI VisListFreeChild               (LPTREE ptreeChild);
BOOL            WINAPI VisListFreeDescendents         (LPTREE ptreeParent);
LONG            WINAPI VisListGetItemColor            (LPTREE ptree);
BOOL            WINAPI VisListGetItemData             (LPTREE ptree, LPHSTRING lphstrText, LPLONG lplValue, LPDWORD lpdwItemFlags);
DWORD           WINAPI VisListGetItemFlags            (LPTREE ptree);
HFONT           WINAPI VisListGetItemFont             (LPTREE ptree);
LPTREE          WINAPI VisListGetItemHandle           (HWND hwnd, int nIndex);
int             WINAPI VisListGetItemIndex            (LPTREE ptree);
BOOL            WINAPI VisListGetItemPicture          (TREE FAR * ptree, LPINT lpiNormal, LPINT lpiSelect);
HSTRING         WINAPI VisListGetItemText             (LPTREE ptree);
LONG            WINAPI VisListGetItemValue            (LPTREE ptree);
int             WINAPI VisListGetLevel                (LPTREE ptree);
LPTREE          WINAPI VisListGetNextSibling          (LPTREE ptree);
LPTREE          WINAPI VisListGetParent               (LPTREE ptree);
LPTREE          WINAPI VisListGetPrevSibling          (LPTREE ptree);
LPTREE          WINAPI VisListGetRoot                 (HWND hwnd);
LPTREE          WINAPI VisListLoadChild               (HWND hwnd, LPTREE ptreeParent, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText, LONG lValue, DWORD dwItemFlags);
LONG            WINAPI VisListLoadChildren            (HWND hwnd, LPTREE ptreeParent, LPPIC ppicNormal, LPPIC ppicSelect, HARRAY haryText, HARRAY haryValues, HARRAY haryItemFlags);
LPTREE          WINAPI VisListLoadOutline             (HWND hwnd, HSTRING hstrBlob);
LPTREE          WINAPI VisListLoadOutlineFile         (HWND hwnd, LPCTSTR lpctszFileName, LPPIC ppicParentNormal, LPPIC ppicParentSelect, LPPIC ppicChildNormal, LPPIC ppicChildSelect);
LPTREE          WINAPI VisListLoadSibling             (HWND hwnd, LPTREE ptreeBefore, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText, LONG lValue, DWORD dwItemFlags);
BOOL            WINAPI VisListMoveDown                (HWND hwnd, int nIndex);
BOOL            WINAPI VisListMoveUp                  (HWND hwnd, int nIndex);
BOOL            WINAPI VisListPromote                 (HWND hwnd, int nIndex, LPPIC ppicNormal, LPPIC ppicSelect);
int             WINAPI VisListSaveOutline             (HWND hwnd, LPHSTRING lphstrBlob);
BOOL            WINAPI VisListSetIndent               (HWND hwnd, int nIndent);
BOOL            WINAPI VisListSetItemColor            (LPTREE ptree, COLORREF crColor);
LPTREE          WINAPI VisListSetItemData             (HWND hwnd, LPTREE ptree, LPCTSTR lpctszText, LONG lValue, DWORD dwItemFlags);
BOOL            WINAPI VisListSetItemFlags            (LPTREE ptree, DWORD dwItemFlags, BOOL bState);
BOOL            WINAPI VisListSetItemFont             (LPTREE ptree, HFONT hfont);
BOOL            WINAPI VisListSetItemPicture          (LPTREE ptree, LPPIC hpicNormal, LPPIC ppicSelect);
LPTREE          WINAPI VisListSetItemText             (LPTREE ptree, LPCTSTR lpctszText);
BOOL            WINAPI VisListSetItemValue            (LPTREE ptree, LONG lValue);
BOOL            WINAPI VisListSetOutlineRedraw        (HWND hwnd, BOOL fRedraw);
BOOL            WINAPI VisListShowOutline             (HWND hwnd, int nLevel);

BOOL            WINAPI VisMenuCheck                   (HMENU hmenu, int nPos);
BOOL            WINAPI VisMenuDelete                  (HWND hwnd, HMENU hmenu, int nPos);
BOOL            WINAPI VisMenuDisable                 (HMENU hmenu, int nPos);
BOOL            WINAPI VisMenuEnable                  (HMENU hmenu, int nPos);
LONG            WINAPI VisMenuGetCount                (HMENU hmenu);
HMENU           WINAPI VisMenuGetHandle               (HWND hwnd);
HMENU           WINAPI VisMenuGetPopupHandle          (HMENU hmenu, int nPos);
HMENU           WINAPI VisMenuGetSystemHandle         (HWND hwnd);
HSTRING         WINAPI VisMenuGetText                 (HWND hwnd, HMENU hmenu, int nPos);
int             WINAPI VisMenuInsert                  (HWND hwnd, HMENU hmenu, int nPos, LPCTSTR lpctszText, WORD wFlags);
int             WINAPI VisMenuInsertFont              (HWND hwnd, HMENU hmenu, int nPos, HFONT hfont, LPCTSTR lpctszText);
int             WINAPI VisMenuInsertPicture           (HWND hwnd, HMENU hmenu, int nPos, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText);
BOOL            WINAPI VisMenuIsChecked               (HMENU hmenu, int nPos);
BOOL            WINAPI VisMenuIsEnabled               (HMENU hmenu, int nPos);
BOOL            WINAPI VisMenuSetFont                 (HWND hwnd, HMENU hmenu, int nPos, HFONT hfont, LPCTSTR lpctszText);
BOOL            WINAPI VisMenuSetPicture              (HWND hwnd, HMENU hmenu, int nPos, LPPIC ppicNormal, LPPIC ppicSelect, LPCTSTR lpctszText);
BOOL            WINAPI VisMenuSetText                 (HWND hwnd, HMENU hmenu, int nPos, LPCTSTR lpctszText);
BOOL            WINAPI VisMenuUncheck                 (HMENU hmenu, int nPos);

BOOL            WINAPI VisMessageFreeButton           (BTN FAR *lpbtn);
LPBTN           WINAPI VisMessageLoadButton           (LPCTSTR lpctszLabel, int nReturn);
int             WINAPI VisMessageBox                  (LPCTSTR lpctszMsgText, LPCTSTR lpctszCaption, DWORD dwIcon, HARRAY haryBtnHandles, int nNumButtons);
BOOL            WINAPI VisMessageSetBkgdColor         (COLORREF crBkColor);

int             WINAPI VisErrorRecovery               (LPCTSTR lpctszCaption);
HSTRING         WINAPI VisGetCopyright                ();
WORD            WINAPI VisGetKeyState                 (int nVirtualKey);
int             WINAPI VisGetSystemMetrics            (int nIndex);
DWORD           WINAPI VisGetWinFlags                 ();
HSTRING         WINAPI VisGetVersion                  ();
NUMBER          WINAPI VisGetWinVersion               ();
LRESULT         WINAPI VisSendMsgString               (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
VOID            WINAPI VisWaitCursor                  (int nFlags);

NUMBER          WINAPI VisNumberChoose                (BOOL bExpression, NUMBER numTrue, NUMBER numFalse);
DWORD           WINAPI VisNumberBitClear              (LPDWORD lpdwValue, DWORD dwMask);
DWORD           WINAPI VisNumberBitSet                (LPDWORD lpdwValue, DWORD dwMask);
BYTE            WINAPI VisNumberHighByte              (WORD wNumber);
BYTE            WINAPI VisNumberLowByte               (WORD wNumber);
DWORD           WINAPI VisNumberMakeLong              (WORD wLow, WORD wHigh);

LPPIC           WINAPI VisPicLoad                     (WORD wFlags, LPCTSTR lpctszString1, LPCTSTR lpctszString2);
int             WINAPI VisPicFree                     (LPPIC ppic);

LONG            WINAPI VisProfileEnumStrings          (LPCTSTR lpctszIniFile, LPCTSTR lpctszSection, HARRAY hary);
BOOL            WINAPI VisProfileDelete               (LPCTSTR lpctszIniFile, LPCTSTR lpctszSection, LPCTSTR lpctszEntry);

HSTRING         WINAPI VisStrChoose                   (BOOL bExpression, HSTRING hstrTrue, HSTRING hstrFalse);
HSTRING         WINAPI VisStrExpand                   (LPCTSTR lpctszTemplate, HARRAY hary);
HSTRING         WINAPI VisStrFind                     (HSTBL hstbl, LPCTSTR lpctszContext);
BOOL            WINAPI VisStrFreeTable                (HSTBL hstbl);
HSTRING         WINAPI VisStrLeftTrim                 (LPCTSTR lpctszSource);
HSTBL           WINAPI VisStrLoadTable                (LPCTSTR lpszStrTableFile);
HSTRING         WINAPI VisStrPad                      (LPCTSTR lpctszSource, int nSize);
HSTRING         WINAPI VisStrProper                   (LPCTSTR lpctszSource);
HSTRING         WINAPI VisStrRightTrim                (LPCTSTR lpctszSource);
int             WINAPI VisStrScanReverse              (LPCTSTR lpctszSource, int nPos, LPCTSTR lpctszSearch);
HSTRING         WINAPI VisStrSubstitute               (LPCTSTR lpctszSource, LPCTSTR lpctszSearch, LPCTSTR lpctszReplace);
HSTRING         WINAPI VisStrTrim                     (LPCTSTR lpctszSource);

BOOL            WINAPI VisTblAllRows                  (HWND hwndTable, WORD wFlagsOn, WORD wFlagsOff);
BOOL            WINAPI VisTblAutoSizeColumn           (HWND hwndTable, HWND hwndColumn);
BOOL            WINAPI VisTblClearColumnSelection     (HWND hwndTable);
LONG            WINAPI VisTblFindDateTime             (HWND hwndTable, LONG lStartRow, HWND hwndColumn, DATETIME dtSearchFor);
LONG            WINAPI VisTblFindNumber               (HWND hwndTable, LONG lStartRow, HWND hwndColumn, NUMBER numSearchFor);
LONG            WINAPI VisTblFindString               (HWND hwndTable, LONG lStartRow, HWND hwndColumn, LPTSTR lpctszSearchFor);
HSTRING         WINAPI VisTblGetCell                  (HWND hwndTable, LONG lRow, HWND hwndColumn);
HSTRING         WINAPI VisTblGetColumnText            (LONG lRow, HWND hwndColumn);
HSTRING         WINAPI VisTblGetColumnTitle           (HWND hwndColumn);
BOOL            WINAPI VisTblSetRowPicture            (HWND hwndTable, WORD wRowFlag, LPPIC ppic);
BOOL            WINAPI VisTblSetRowColor              (HWND hwndTable, LONG lRow, COLORREF crTextColor);

LONG            WINAPI VisWinClearAllFields           (HWND hwndParent);
LONG            WINAPI VisWinClearAllEditFlags        (HWND hwndParent);
int             WINAPI VisWinEnumProps                (HWND hwnd, HARRAY haryPropStrings, HARRAY haryValues);
BOOL            WINAPI VisWinFreeAccelerator          (HACC hacc);
DWORD           WINAPI VisWinGetFlags                 (HWND hwnd);
HWND            WINAPI VisWinGetHandle                (HWND hwndParent, LPCTSTR lpctszContext, DWORD dwType);
HANDLE          WINAPI VisWinGetProp                  (HWND hwnd, LPCTSTR lpctszPropString);
LONG            WINAPI VisWinGetStyle                 (HWND hwnd);
HSTRING         WINAPI VisWinGetText                  (HWND hwnd);
BOOL            WINAPI VisWinIsChild                  (HWND hwndParent, HWND hwndChild);
BOOL            WINAPI VisWinIsMaximized              (HWND hwnd);
BOOL            WINAPI VisWinIsMinimized              (HWND hwnd);
HWND            WINAPI VisWinIsRequiredFieldNull      (HWND hwndParent);
BOOL            WINAPI VisWinIsRestored               (HWND hwnd);
BOOL            WINAPI VisWinIsWindow                 (HWND hwnd);
HACC            WINAPI VisWinLoadAccelerator          (HWND hwndNotify, UINT uMsg, int nKeyDownState, WORD wVirtualKey, LONG lValue);
BOOL            WINAPI VisWinMove                     (HWND hwnd, int nLeft, int nTop, int nWidth, int nHeight);
BOOL            WINAPI VisWinRemoveProp               (HWND hwnd, LPCTSTR lpctszPropString);
BOOL            WINAPI VisWinSetFlags                 (HWND hwnd, DWORD dwWinFlags, BOOL bState);
int             WINAPI VisWinSetMeter                 (HWND hwnd, int nPctCmp);
BOOL            WINAPI VisWinSetProp                  (HWND hwnd, LPCTSTR lpctszPropString, WORD wValue);
BOOL            WINAPI VisWinSetSelect                (HWND hwnd, WORD wStartPos, WORD wEndPos);
int             WINAPI VisWinSetStyle                 (HWND hwnd, DWORD dwNewStyle, BOOL bState );
BOOL            WINAPI VisWinSetTabOrder              (HWND hwnd, HWND hwndInsertAfter);
BOOL            WINAPI VisWinShow                     (HWND hwnd, int iShowCmd);

#ifdef __cplusplus
   }
#endif 


#define VT_H
#endif 
