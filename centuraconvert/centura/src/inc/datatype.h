/* $Header: /Jungfrau/sqlwin/GTIDEV/GTIINC/datatype.h 4     11/12/98 6:18p Jmundsto $ */
/*
  file: datatype.h
  description: defines datatypes used by assortment of Gupta Products
  modification History:
  MDC 02/02/89 created

*/

#ifndef DATETIMEFLAG
#ifndef NUMBER_H
#include "number.h"
#endif
#define DATETIMESIZE NUMBERSIZE

#define DTLENGTHMASK    0x0F       /* mask to get actual length */
#define DTDATETIMEMASK  0xF0       /* mask to get code in place */
#define DTDATEFORMATCODE 0x80      /* flag to specify is special code */
#define DTDATECODEMASK   0x7       /* mask to get code as small int */
#define DTDATETIMESHIFT 4          /* shift to insert code      */

typedef struct 
{
  BYTE DATETIME_Length;		/* length of date/time          */
  BYTE DATETIME_Buffer[DATETIMESIZE /* DB_SIZEDAT */ ];
} DATETIME;


#define DTGETLENGTH(dt) ((dt)->DATETIME_Length & DTLENGTHMASK)
#define DATETIMELENGTH(l) (l&DTLENGTHMASK);
#define ISDTFMTCODE(dt) ((dt->DATETIME_Length)&DTDATETIMEMASK)
#define ISDATEFMTCODE(a) (a&DTDATEFORMATCODE)
#define MAKEDATEFMTCODE(a) ((a<<DTDATETIMESHIFT)|DTDATEFORMATCODE)
#define GETDATEFMTCODE(a)  ((a>>DTDATETIMESHIFT)&DTDATECODEMASK)
#define MAKEDATESIZE(l) (l>15?15:l)

typedef DATETIME far *LPDATETIME;
typedef DATETIME *PDATETIME;
#define DATETIMEFLAG
#endif

/*
 * $History: datatype.h $
 * 
 * *****************  Version 4  *****************
 * User: Jmundsto     Date: 11/12/98   Time: 6:18p
 * Updated in $/Jungfrau/sqlwin/GTIDEV/GTIINC
 * Support for longer numbers.
 * 
 * *****************  Version 3  *****************
 * User: Omullarn     Date: 9/03/98    Time: 1:02p
 * Updated in $/Jungfrau/sqlwin/GTIDEV/GTIINC
 * Object Nationalizer merge
 * 
 * *****************  Version 1  *****************
 * User: Jlacerda     Date: 11/04/97   Time: 5:09p
 * Created in $/Phoenix/sqlwin/GTIDEV/GTIINC
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 6/18/96    Time: 7:25p
 * Branched in $/Patriot/sqlwin/GTIDEV/GTIINC
 * Stinger project branched for Patriot
 * 
 * *****************  Version 2  *****************
 * User: Tmcparla     Date: 2/06/95    Time: 4:15p
 * Updated in $/SQLWIN/GTIDEV/GTIINC
 *
 */
