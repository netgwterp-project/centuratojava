/* $Header: /Patriot/sqlwin/GTIDEV/GTIINC/tbl.h 5     10/03/96 9:17a Dmadison $*/

/*
1.      Module name.
        tbl.h - Table engine export header file.

2.      Module type.
        Header

3.      Functional description.

4.      Modification history.
        M. D. ROSS      01/05/89  Creation

5.      NOTICE:  Copyriht (C) 1989 Gupta Technologies Inc.
*/
#ifndef TBL_H
#define TBL_H

#if defined(__cplusplus)
extern "C"
{                               /* Assume C declarations for C++ */
#endif                          /* __cplusplus */

#include"tblrc.h"

// Some required typedefs

#ifndef GTI_H
typedef unsigned FLAG;
#endif

#if defined(PMEM)
#undef  PMEM
#endif

  typedef LPVOID HMEM;          /* These were originally in farheap.h */
  typedef HMEM FAR *LPHMEM;
  typedef LPVOID PMEM;

  typedef BOOL (EXPORTAPI *LPVIEWINITVIEW)     (HWND, WORD, LPVOID FAR *);
  typedef BOOL (EXPORTAPI *LPVIEWDESTROYVIEW)  (HWND, LPVOID FAR *);
  typedef BOOL (EXPORTAPI *LPVIEWREGISTERVIEW) (HINSTANCE, HINSTANCE, LPSTR,
                                      LPVIEWINITVIEW *, LPVIEWDESTROYVIEW *);

  // This function should be called when the client application instance
  // is initialized. The parameters are: HANDLE to the current hInstance,
  // HANDLE to the previous instance,
  // LPSTR to Class name to use for registration, int extra table bytes,
  // int extra column bytes, and pointers to view registration functions.
  // The first function pointer is the Form view registration function.
  // It can be TblFormRegisterView or NULL.
  // The second function pointer identifies the List view registration
  // function.
  // It can be TblListRegisterView or NULL.
  // If either function pointer is NULL then the view is not defined.

  BOOL EXPORTAPI TblInstanceInit(HINSTANCE, HINSTANCE, LPSTR,
                        int, int, int, int,
                        LPVIEWREGISTERVIEW, LPVIEWREGISTERVIEW);


// Functions passed to TblRegisterView

  BOOL EXPORTAPI TblFormRegisterView(HINSTANCE, HINSTANCE, LPSTR,
                            LPVIEWINITVIEW *, LPVIEWDESTROYVIEW *);
  BOOL EXPORTAPI TblListRegisterView(HINSTANCE, HINSTANCE, LPSTR,
                            LPVIEWINITVIEW *, LPVIEWDESTROYVIEW *);

// This function is called to find out whether a window is a table
  // or column window.  The return value is non-zero if the window is a
  // table or column window.

  WORD EXPORTAPI TblIsWindowOfTable(HWND);

  // This function is called to check Init the Gupta Table DLL.
  // returns TRUE is the DLL is properly initialized.

  BOOL EXPORTAPI TblInitTableDLL( void );

  // This function is called by the Sal compiler and by SQL processing 
  // whenever a runtime error is encountered. Without this, fetching can
  // get into an endless loop reposting GTM_REFETCHROWS if WM_PAINT arrives
  // during the fetch. Generally, it should be called by any data source
  // that might be supplying row data if the data source needs to display
  // an error dialog regarding the error. 

  VOID EXPORTAPI TblStopFetching( VOID );

#define           TBL_ISTABLEHWND         1
#define           TBL_ISCOLUMNHWND        2
#define           TBL_ISNOTWINDOWOFTABLE  0


/* This define is used to create the window classes and windows themselves
** needed for the Table Editor. You don't need to change this if it conficts,
** just register a diffent name.
*/

#define CLASS_TABLEWIN        "Centura:Table"

/* This defines that names used to createthe global table window classes
** and windows needed for the Table Editor. You can no longer register your 
** own class names.
*/

#define CLASS_SWTABLEWIN "Centura:Table"
#define CLASS_SWCHILDTABLEWIN "Centura:ChildTable"

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* The amount of extra bytes to allocate with each column window data
** structure. For the future, any bytes from zero upto COLUMN_EXTRA are
** reserved by the Table Engine. Any Client words needed per column can
** be specified when the table engine is registered. DO NOT CHANGE THESE
** VALUES!! The client should always index starting at the byte offset of
** COLUMN_EXTRA.
*/

#define COLUMN_EXTRA    2

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* The amount of extra bytes to allocate with each Table window data
** structure. THE FIRST 2 EXTRA WORDS ARE RESERVED FOR THE TABLE! Any
** additional words needed can be specified when the table engine is
** registered. DO NOT CHANGE THESE VALUES!! The client should always
** index starting at the byte offset of TABLE_EXTRA.
*/
/*
** Note: to support window title bar icons under Win95, an extra flag
** word has been added to the window extra data for table windows. So
** far, only one bit of this word is used. OM 4/15/96
*/

#define TABLE_HEADER    0
#define TABLE_EXTRA     6

// the offset within TABLE_EXTRA for the flag word
#define TABLE_FLAGS     4 

// the flag indicating that the table window icon has been set
#define TF_ICONSET    0x0001 

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


/*  This is a window style bit that is used when a table window is
**  is defined within a dialog box.  It seems that the create lParam is
**  set to something that the table engine does not understand.  This
**  tells the table engine to treat the create lparam as NULL.
*/

// NOTE:  This must be different than style bit used by SqlWindows.
  // At the time this is written, SqlWindows uses 0x8000L.

#define TBLWS_DLGITEM     0x4000L
#define TBLWS_MDICHILD    0x0001L


/* Views supported by the table window.
*/

#define TV_NullView    0    //  No user-interface 
#define TV_ListView    1    //  Spreadsheet-like view.
#define TV_FormView    2    //  Dialog box-like view 
#define TV_FieldView   3    //  Form view without borders or ribbon
#define TV_VCRView     4    //  VCR View

#define TV_ViewCount   5    //  Number of views

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* Values returned by the TBH_VALIDATEDATA hook proc.
*/

#define VALIDATE_CANCEL    0
#define VALIDATE_OK        1
#define VALIDATE_OKCLEAR   2
#define VALIDATE_OKRESET   3  // Reset field to original value

/* Hit Testing Flags for the GTM_HITMOUSEMOVE message.
*/

#define HT_NOAREA          0x0000
#define HT_SIZEAREA        0x0001
#define HT_MOVEAREA        0x0002
#define HT_INSERTAREA      0x0004
#define HT_SELECTAREA      0x0008
#define HT_ROWHEADERSIZEAREA 0x0010
#define HT_ROWSIZEAREA     0x0020

#define HT_ALLAREAS        0x003f

// Row Header FLAGS that indicate the type of row header

#define RH_None            0x0000
#define RH_UserDraw        0x0001
#define RH_MapColumn       0x0002
#define RH_UserSizeable    0x0004
#define RH_Visible         0x0008
#define RH_ShareColor      0x0010
#define RH_RowsSizeable    0x0020


/* Status FLAGS that can be set or retrieved for each row in the Table
** Editor.
*/

#define RF_InMemory        0x0001
#define RF_Selected        0x0002
#define RF_Edited          0x0004
#define RF_Inserted        0x0008
#define RF_Deleted         0x0010
// Note RF_Selected bit is not set here
#define RF_NotDiscardable  (RF_Edited | RF_Inserted | RF_CellAttribute)

#define RF_User            0x0020
#define RF_UserRange       0x0fe0
#define RF_UserMax         0x0800
#define RF_CellAttribute   0x1000
#define RF_Hidden          0x2000

// Internal flags
#define RF_Inverted        0x4000
#define RF_DontIsolate     0x8000

// Status FLAGS that can be set or retrieved for each column in the table.

#define CF_Visible         0x00000001L
#define CF_Editable        0x00000002L
#define CF_Selected        0x00000004L
#define CF_Inserted        0x00000008L
#define CF_FmtInvisible    0x00000010L
#define CF_CaptionGrayed   0x00000020L
#define CF_NoChangeCaption 0x00000040L

// CF_LeftJustify is the default...
  // CF_RightJustify and CF_CenterJustify are mutually exclusive...

#define CF_LeftJustify     0x00000000L
#define CF_RightJustify    0x00000080L
#define CF_CenterJustify   0x00000100L

#define CF_NoEditText      0x00000200L
// CF_FmtUpperCase and CF_FmtLowerCase are mutually exclusive
#define CF_FmtUpperCase    0x00000400L
#define CF_FmtLowerCase    0x00000800L
#define CF_Overflow        0x00001000L    // display overflow char 
#define CF_User            0x00002000L
#define CF_UserRange       0x0000e000L
#define CF_UserMax         0x00008000L

#define CF_FormView1Line      0x00010000L
#define CF_XYPosSpecified     0x00020000L
#define CF_ColIsRadioButton   0x00040000L
#define CF_ColIsCheckBox      0x00080000L
#define CF_ColIsMultiLine     0x00100000L
#define CF_ColCntrlIgnoreCase 0x00200000L
#define CF_ColIsBiDi          0x00400000L
#define CF_MultilineCell      0x00800000L
#define CF_ColCntrlEtched     0x01000000L

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* Column Type Constants. TYPE_EDIT must be defined as zero to support
** earlier version of the Table Engine.
*/

#define CT_EDIT            0
#define CT_LONGEDIT        1
#define CT_OWNERDRAW       3

/*  Drop down style bits
*/
#define TBLDDNSTYLE_ButtonOnSide  0x0001 // Position arrow button
#define TBLDDNSTYLE_ButtonOnTop   0x0002
#define TBLDDNSTYLE_NoButton      0x0004
#define TBLDDNSTYLE_DropBelow     0x0008 // Position drop down
#define TBLDDNSTYLE_DropOver      0x0010
#define TBLDDNSTYLE_ShowOnFocus   0x0020
#define TBLDDNSTYLE_VScroll       0x0040
#define TBLDDNSTYLE_Sort          0x0080
#define TBLDDNSTYLE_UserCanHideShow  0x0100
#define TBLDDNSTYLE_SizeToFitWidth  0x0200
#define TBLDDNSTYLE_SizeToFitHeight  0x0400
#define TBLDDNSTYLE_AlwaysTrapKeys   0x0800

/* Extended column types
*/
#define ECT_Standard       0
#define ECT_CheckBox       1
#define ECT_DropDownList   2
#define ECT_DropDownListAndEdit   3
#define ECT_DropDownEdit   4
#define ECT_DropDownUser   5

/* Types of extended data associated with a column
*/
#define EDT_CheckBoxOnOff  1   // Two null terminated strings.  The first
                               // Indicates the value when checked, the
                               // second when not checked.

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

// Constants to pass into the GTM_CLEARSEL message. Pass in the wParam
  // to specify which to clear.

#define CLS_ROWS           0
#define CLS_COLUMNS        1
#define CLS_BOTH           2

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

// Constants to pass to GTM_QUERYSCROLL and GTM_QUERYVISIBLERANGE

#define QS_NORMAL       0
#define QS_SPLIT        1


// Any color value greater than this is invalid
  // this bit in the high order byte is set if the color is a palette color
#define MAXVALIDCOLOR  0x02ffffff


/* Assorted Table Editor defines...
*/

#define INVALID            -1
#define MAXTITLE           254

#ifdef _MAC
#define TBL_ERROR          0x8000
#define TBL_MAX            0x7FF0
#define TBL_MIN            -TBL_MAX
#define TBL_TEMPROW        TBL_ERROR
#define TBL_CACHELIMIT     TBL_MAX + 1
#define TBL_MINSPLIT       (TBL_MIN+2)
#else
#define TBL_ERROR          0x80000000  //0x7FFF1591
#define TBL_MAX            0x7FFF1590
#define TBL_MIN            -TBL_MAX
#define TBL_TEMPROW        TBL_ERROR
#define TBL_CACHELIMIT     TBL_MAX + 1
#define TBL_MINSPLIT       (TBL_MIN+2)
#endif /* _MAC */

#define TBL_AUTOSCROLL     0
#define TBL_SCROLLBOTTOM   2
#define TBL_SCROLLTOP      1
#define TBL_SCROLLCARET    3
#define TBL_DONTSCROLL     0xffff


#define TBL_NOADJUST       0
#define TBL_ADJUST         1

// Fetch hook and TblMsgFetchRow return values
#define TBL_NOMOREROWS     0
#define TBL_ROWFETCHED     1
#define TBL_ROWDELETED     2
// TBL_FETCHCANCELED should be returned when the user has indicated that they
// want to abort the fetch. However, it is the table engine that has the final
// say if the fetch can be aborted. Therefore, the fetchRowProc should only
// return TBL_FETCHCANCELED once. If the table engine determines that it really
// needs the row. It will continue to ask until TBL_FETCHCANCELED is not returned
#define TBL_FETCHCANCELED  3

// Table capabilities
#define TC_SIZECOLUMNS      0x0001      /* Can columns be sized?         */
#define TC_MOVECOLUMNS      0x0002      /* Can columns be moved?         */
#define TC_ALWAYSVERTSCROLL 0x0004      /* Always display scroll bar?    */
#define TC_SELECTROWS       0x0008      /* Hit area for rows?            */
#define TC_IGNOREINSKEY     0x0010      /* Insert key is not used        */
#define TC_ADJUSTSPLIT      0x0020      /* Split bar can be dragged      */
#define TC_NOHIDESEL        0x0040      /* Used ES_NOHIDESEL style       */
#define TC_SELECTCOLUMNS    0x0080      /* Click on caption selects column */
#define TC_HSCROLLBYCOLUMNS 0x0100      /* Scroll by columns, like Excel   */
#define TC_GRAYHEADERS      0x0200      /* Column and row headers are gray */
#define TC_WAITCURSOR       0x0400      /* Show wait cursor ?              */
#define TC_SUPPRESSROWLINES 0x0800      /* Don't display horiz. grid lines */
#define TC_SINGLESELECTION  0x1000      /* Single selection only ?         */
#define TC_SUPPRESSLASTCOLLINE 0x2000   /* Don't display last vertical line */
#define TC_EDITLEFTJUSTIFY  0x4000      /* Always left justify when edit */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* These notification Codes are sent by the Table Editor to the parent
** (if any) window. The standard child control notification conventions
** are followed. An attempt is made to define a range of notification
** messages that are owned uniquely by each engine. To that end, the table
** engine has been assigned the range of 0x0700 to 0x072f.
*/

#ifndef NOCTLMGR

#define TBN_SETFOCUS       0x0700
#define TBN_KILLFOCUS      0x0701
#define TBN_CHANGE         0x0702
#define TBN_UPDATE         0x0703
#define TBN_ERRSPACE       0x0704
// note TBN_VSCROLL and TBN_HSCROLL are not supported
#define TBN_HSCROLL        0x0705
#define TBN_VSCROLL        0x0706
#define TBN_ERRHEAP        0x0707
#define TBN_CLICK          0x0708
#define TBN_DOUBLECLK      0x0709
#define TBN_DESTROY        0x070a
#define TBN_CACHEFULL      0x070b
#define TBN_COLUMNSIZE     0x070c
#define TBN_COLUMNMOVE     0x070d
#define TBN_COLUMNSELECT   0x070e
#define TBN_COLUMNINSERT   0x070f
#define TBN_COLUMNDELETE   0x0710
#define TBN_TOGGLECAPTION  0x0711
#define TBN_COLUMNUNSELECT 0x0712
#define TBN_FIRSTCHANGE    0x0713
#define TBN_KILLEDIT       0x0714
#define TBN_SELCHANGE      0x0715
#define TBN_CAPTIONDOUBLECLK    0x0716
#define TBN_ROWHEADERCLICK      0x0717
#define TBN_ROWHEADERDOUBLECLK  0x0718
#define TBN_CORNERCLICK         0x0719
#define TBN_CORNERDOUBLECLK     0x0720
#define TBN_COLUMNSELECTCLICK   0x0721
#define TBN_CAPTIONRIGHTCLK     0x0722
#define TBN_RIGHTCLK            0x0723
#define TBN_CORNERRIGHTCLK      0x0724
#define TBN_FIELDDOUBLECLICK    0x0725
#define TBN_FOCUSROWCHANGED     0x0726
#define TBN_CONTROLSTYLE        0x0727
#define TBN_DISCARDINGROW       0x0728
#define TBN_DROPDOWN            0x0729
#define TBN_ROWSIZE             0x072a
#endif

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* These message are used to control the Table Editor control. They follow
** standard control message conventions.
*/

#ifndef NOWINMESSAGES

/* This message is unique in that it is sent by the Table Editor to the
** Parent, requesting that he issue the neccessary messages to update
** data in the specified row. TBR_ means TaBle Request. the message format
** is wParam = Table editor child ID if applicable, LOWORD of the TABLELPARAM 
** struct is set to HWND of Table editor, HIWORD set to the row to fetch.
*/

#define TBR_FETCHROW          (WM_USER+0)

/* This message is unique in that it is sent by the Table Editor to the
** Parent, requesting that he issue the neccessary messages to insert a
** column at the specified location. TBR_ means TaBle Request. the message
** format is wParam = Table editor child ID if applicable, LOWORD of
** the LPARAM struct is set to HWND of Table editor, HIWORD set to the 
** virtual column (screen ordering) of the column left of the insertion request.
*/

#define TBR_INSERTCOLUMN      (WM_USER+1)

/* More Table Editor Messages... */

#define GTM_SETHOOK           (WM_USER+11)

/* These are passed in wParam of the GTM_SETHOOK message. The FAR pointer
** to the appropriate function is cast and passed in the lParam. Below,
** Each TBH_ hook message is followed by the parameters needed for each
** function, and a brief description.
*/

#define TBH_MARK              0

/* VOID MarkProc(HWND, HDC);
**
** This function is used to visually mark the Table Editor in programs that
** support a design mode. It is called each time the Table Editor is
** repainted. Parameters are the Handle of the Table Editor and the HDC used
** for repainting.
*/

#define TBH_YIELD             1

/* BOOL YieldProc(LPLONG, HWND, UINT, WPARAM, LPARAM);
**
** This function serves as a filter function for all messages sent to the
** Table Editor. Called once for each message, this hook allows the parent
** to intercept and process any message. If YieldProc returns TRUE, the LONG
** value pointed to by LPLONG is returned by the table editor call-back. If
** FALSE, message is processed normally.
*/

#define TBH_INITMENU          2

/* VOID InitMenuProc(HWND, HMENU, WORD, BOOL);
**
** This function is called when the Table Editor recieves a WM_INITMENUPOPUP
** message. Parameters are the HWND of the table editor, the HMENU of the
** popup that is about to be displayed, the index of the menu (from the left,
** with the first menu being zero), and BOOL which is TRUE is this is the
** system menu.
*/

#define TBH_PAINTICON         3

/* VOID PaintIconProc(HWND, WORD, LONG);
**
** This function is called when the table editor recieves a WM_PAINTICON
** message. Parameters are the HWND of the table editor, the wParam and
** the lParam of the WM_PAINTICON message.
*/

#define TBH_MENUSELECT        4

/* VOID MenuSelectProc(HWND, HMENU, WORD, WORD);
**
** This function is called when the table editor recieves a WM_MENUSELECT
** message. Parameters are the HWND of the table editor, the HMENU of the
** selected popup, a WORD which is the menu command value and a WORD
** containing the OR'ed MF_ flags of the selected menu.
*/

#define TBH_SYSCOMMAND        5

/* BOOL SysCommandProc(HWND, WORD);
**
** This function is called when the table editor recieves a WM_SYSCOMMAND
** message. Parameters are the HWND of the table editor and a WORD which
** is the requested SC_ system command.
*/

#define TBH_TABLEDESIGN       6

/* BOOL TableDesignProc(LPLONG, HWND, UINT, WPARAM, LPARAM);
**
** This function is called when the table editor is in Design mode. This
** mode switch is done with the GTM_SETUSERMODE message. This function need
** NOT be exported as windows never calls it directly. Its parameter and
** return value are identical to the table editor call-back, with the
** exception of the LPLONG parameter. If this function returns true, design
** time processing is aborted and the value of LPLONG is returned.
*/

#define TBH_COLFORMAT         7

/* BOOL ColFormatProc(HWND, int);
**
** This function is called when the Current column recieves a WM_DESTROY
** message. Parameters are the HWND of the column and a WORD which
** is always 0. This hook is good for zeroing out global format data when
** a column is destroyed.
*/

#define TBH_CTLCOLOR          8

/* HBRUSH CtlColorProc(HWND, HDC);
**
** This function is called to set the text and background color of a
** table window.  The application should respond as if processing
** WM_CTLCOLOR.
*/

#define TBH_FETCHROW          9

/* int  FetchRowProc(int, HWND, long);
**
** This function is clled when either the table editor or the program
** issues a TblMsgFetchRow function call. If the requested row is not in the
** cache and the TBH_FETCHROW hook is installed, This proc is called. The
** first int value is the table editors child of a child or -1 if top-level.
** the HWND param is the handle to the table editor and the long value is
** the row that needs to be fetched.
** The proc returns TBL_NOMOREROWS, TBL_ROWFETCHED, or TBL_ROWDELETED,
** or TBL_FETCHCANCELED to indicate status.
*/

#define TBH_TABLENOTIFY       10

/* VOID TableNotifyProc(HWND, WORD, int);
**
** This function can be used to intercept all table editor notification
** messages that are normally sent to the parent. This should be used when
** top level table windows are created. The HWND param is the handle of the
** current table editor, the WORD param is the notification code (see above)
** and the int param is child id if a child or -1 if top level.
*/

#define TBH_LONGDATA          11

/* VOID LongDataProc(HWND, int, WORD);
**
** This function can be used to process for column cell of type TYPE_LONGEDIT.
** The HWND is the Handle to the Table, the int is the current row and the
** WORD is the Column Id of the cell requesting long data handling.
*/

#define TBH_DEFWINDOW         12

/* LONG DefWindowProc(HWND, UINT, WPARAM, LPARAM);
**
** This function can provide any alternate default processing the client
** my wish to do with the table window. If a DefWindowProc() hook is
** installed, the hook MUST call the Windows DefWindowProc() for any
** unprocessed messages.
*/

#define TBH_OPEN              13

/* BOOL OpenProc(HWND);
**
** This function provides a way for the tables client to perform any
** initialization prior to the Table being displayed. This function
** is called after internal table structures have been initialized. If
** the client returns FALSE to this call, the the table is destroyed
** and creation is aborted.
*/

#define TBH_CLOSE             14

/* BOOL CloseProc(HWND);
**
** This function provides a way for the tables client to perform any
** cleanup prior to the table destroying itself. If this function returns
** FALSE, the the table will NOT destroy itself.
*/

#define TBH_LOCALALLOC        15

/* LOCALHANDLE LocalAllocProc(UINT, UINT);
**
** This function provides a way for the tables client to install a
** different Local Allocation subset.
*/

#define TBH_LOCALREALLOC      16

/* LOCALHANDLE LocalReAllocProc(LOCALHANDLE, WORD, WORD);
**
** This function provides a way for the tables client to install a
** different Local ReAllocation subset.
*/

#define TBH_GLOBALALLOC       17

/* GLOBALHANDLE GlobalAllocProc(WORD, DWORD);
**
** This function provides a way for the tables client to install a
** different Global Allocation subset.
*/

#define TBH_GLOBALREALLOC     18

/* GLOBALHANDLE GlobalReAllocProc(GLOBALHANDLE, DWORD, WORD);
**
** This function provides a way for the tables client to install a
** different Global ReAllocation subset.
*/

#define TBH_INSERTCOLUMN      19

/* BOOL InsertColProc(int, HWND, int);
**
** This function is called when the user makes a request to insert
** a new Column. If the fInsertColumn flag is set, either this function
** is called if the TBH_INSERTCOLUMN hook is set, or a TBR_INSERTCOLUMN
** message is send to the parent (if any). The first int is the ID of
** the table engine (-1 if not a child), the HWND if the table itself, and
** the last int is the virtual index (screen ordering) of the column left
** of the insertion request.
*/

#define TBH_VALIDATEDATA      20

/* int ValidateDataProc(HWND, LONG, LONG);
**
** This function is called under these conditions:
**    1) When the focus is switched to table/cell from another control.
**       In this case if the table has a valid cell control, the first
**       LONG will be -1L and the second long will contain the Row/Column
**       of the Cell control that is to receive the focus. If there is no
**       valid cell control, then both LONG's contain -1L.
**
**    2) When for any reason, a cell control is about to be destroyed. This
**       can happen by tabbing between cells, mouse selecting a cell, or
**       killing edit mode with the insert or esc key. There are also some
**       messages that are sent by the client that would trigger the
**       destruction of the cell control. In all these cases, the first LONG
**       will always contain the Row/Column of the cell control that
**       currently has the focus. In cases where the focus is being transfered
**       to another cell control, the second LONG will contain its Row/Column.
**
**    3) When the focus is switched to another control from the table/cell.
**       In this case, if the table has a valid cell control, the first
**       LONG will contain the Row/Column of that cell control, else both
**       LONG's will contain -1L.
**
**    4) When the focus is switched to another focus row while in row
**       selection mode.
**       In this case, the first
**       LONG will contain the Row that we were in if any, and the second
**       LONG will contain the row we are moving to..
**
**    5) When the focus is killed using GTM_KILLFOCUS.
**
**    In all cases, the engines processing can be altered based on the
**    values that is returned. If the client returns VALIDATE_CANCEL, then
**    the cell control destruction or focus change is terminated. If the
**    client returns VALIDATE_OK, the processing continues as it should. If
**    the client returns VALIDATE_OKCLEAR, processing continues as it
**    should, with the controls modify flag being set to zero prior to
**    destroying the cell control.
*/

#define TBH_SCROLLTABLE       21

/* BOOL ScrollTableProc(HWND, LPPOINT, LPRECT);
**
** This function is called after the table engine calculates the percentage
** of scroll and the scrolling rectangle and just prior to calling scroll
** window. The parameters are: the HWND of the table window, an LPPOINT
** of the x and y to scroll the window ( these values will be negative for
** scrolling left and up, positive for scrolling right and down), and an
** LPRECT to the coordinates (client area) to scroll. If this function returns
** TRUE, Then ScrollWindow() is called else it is not.
*/

#define TBH_DRAWROWHEADER       22

/* BOOL DrawRowHeader(HWND, HDC, int, WORD, LPRECT);
**
** This function is called to draw the row header of a particular row.
** The parameters are: Handle of the table window,  HDC to paint to,
** the row number, row flags, and the rectangle to paint.
** If this function returns FALSE, Then the table engine paints the row
** header.
*/

#define TBH_ENDCELLTAB          23

/* BOOL EndCellTab(HWND);
**
** This function is called when the user attempts to tab past the
** last cell in the table.  A typical use for this hook is to insert
** a row at that time.
** If TRUE is returned then it assumed that the application processed the
** tab.  Otherwise, the edit is killed and the focus row is selected.
*/

#define TBH_COUNTROWS             24

/* int CountRows(HWND);
**
** This function is called to get the number of rows that can
** be fetched.  It is used when the user homes down and dynamic scroll
** range is active.  If this hook is not provided, the fetch hook will
** be used to count rows.  If this hook returns -1, the fetch hook will
** be used to count rows.
*/


#define TBH_INITCELLEDIT        25

/* VOID InitCellEdit(HWND, int, WORD);
**
** This function is called after an edit window has been created to
** edit column text.  HWND is the handle of the edit window that has been
** created.  int is the row number.  WORD is the Column ID of the column
** that is being edited.
*/

#define TBH_GETCHARBOX           26

/* void GetCharBox( HFONT, LPPOINT);
**
** This function gets the dimensions of an average font character that
** can be used to resize columns when the font is changed.  If the font
** parameter is null then the size of the system font is returned.
*/

#define TBH_OWNERDRAWCOL         27

// VOID OwnerDraw(HWND, int, WORD, HDC, LPRECT);

// This function is called whenever an owner draw cell needs to be painted.
  // The parameters are: HWND of table window, int row, WORD col, HDC for
  // painting, LPRECT for clipping...

#define TBH_QUERYYIELDSTATE      28

// BOOL DBSIsYielding(VOID);

// This function is called to determine if application is yielding.
  // If we are yielding, table engine will queue row fetch requests for
  // later processing.


#define TBH_EDITCELL             29

/* BOOL CellEditProc(LPLONG, HWND, HWND, unsigned, WORD, LONG);
**
** This function is called when a cell is being edited.
** It passes all messages that are sent to a cell to the application.
** The last four parameters
** are identical to the windows call-back.  The first HWND parameter
** is the handle of the column window that is being edited.
** If this function returns true, edit
** control processing is aborted and the value of LPLONG is returned.
*/

/* More Table Editor Messages... */

#define GTM_CLEARSEL          (WM_USER+12)

/* wParam - CLS_ROWS for rows and CLS_COLUMNS for columns or CLS_BOTH
            for both
**
** lParam - Not used.
**
** returns - TRUE is successful.
**
** This message clears the currently selected rows.
*/

#define GTM_DELETEROW         (WM_USER+13)

/* wParam - Either the TBL_ADJUST or the TBL_NOADJUST flags.
**
** lParam - The LOWORD of the TABLELPARAM struct contains the row to 
**          delete and the HIWORD is TRUE
**          if the Table Editor should be repainted afterwards.
**
** returns - TRUE if successful.
**
** Deletes specified row from Table Editor.
*/

#define GTM_FETCHROW          (WM_USER+14)

/* wParam - Not used.
**
** lParam - Contains the row to be fetched.
**
** returns - TRUE if successful.
**
** Insures that valid data is in the specified row. First the cache is
** checked for current data. If valid data is'nt present, a TBR_FETCHROW
** message is sent to the parent, requesting that the cache be updated.
*/

#define GTM_FINDNEXTROW       (WM_USER+15)

/* wParam - Not used.
**
** lParam - FAR pointer to FINDROW struct.
**
** returns - TRUE if Successful.
**
** See the FINDROW struct for more info. This message is used to find the
** next row that matches the specified flags.
*/

#define GTM_FINDPREVROW       (WM_USER+16)

/* wParam - Not used.
**
** lParam - FAR pointer to FINDROW struct.
**
** returns - TRUE if Successful.
**
** See the FINDROW struct for more info. This message is used to find the
** next row that matches the specified flags.
*/

#define GTM_INSERTROW         (WM_USER+17)

/* wParam - Not used.
**
** lParam - Contains the row to insert the new row, or TBL_MAX to 
**          append to the end.
**
** returns - returns the index of the new row, or TBL_ERROR if unsuccessful.
**
** This message inserts a new row into the table editor.
*/

#define GTM_KILLEDIT          (WM_USER+18)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if Successful.
**
** This message terminates the the edit mode of the current cell. The current
** row is then selected.
*/

#define GTM_KILLFOCUS         (WM_USER+19)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** This message removes the focus frame (if it exists) from the current row.
*/

#define GTM_PAINTROWS         (WM_USER+20)

/* wParam - Not used.
**
** lParam - The LOWORD and HIWORD of the TABLELPARAM struct each contain 
**          the starting and ending rows
**          that need repainting. Selecting repainting is currently not
**          implemented.
**
** returns - TRUE if successful.
**
**          This messages caused the table editor to be repainted.
*/

#define GTM_QUERYCONTEXT      (WM_USER+21)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - The current row if set, or TBL_ERROR if error or not set.
**
*/

#define GTM_QUERYFOCUS        (WM_USER+22)

/* wParam - Not used.
**
** lParam - FAR pointer to TABLEFOCUS struct.
**
** returns - TRUE if successful.
**
** This message fills the TABLEFOCUS struct with the index of the current
** row, and the hWnd of the current column (if valid).
*/

#define GTM_QUERYROWFLAGS     (WM_USER+23)

/* wParam - Flags to check against row.
**
** lParam - Contains the row to query.
**
** returns - TRUE if successful.
**
** This message checks the state of the specified row flags.
*/

#define GTM_QUERYSCROLL       (WM_USER+24)

/* wParam - QS_NORMAL or QS_SPLIT for lower part of split window
**
** lParam - FAR pointer to TABLERANGE struct.
**
** returns - TRUE if successful.
**
** This function fills in the TABLERANGE struct with the current scroll
** data.
*/

#define GTM_RESET             (WM_USER+25)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** This message resets (clears) the table editor by re-initializing the
** internal cache.
*/

#define GTM_QUERYVISIBLERANGE (WM_USER+26)

/* wParam - QS_NORMAL or QS_SPLIT for lower part of split window
**
** lParam - FAR pointer to TABLERANGE struct.
**
** returns - TRUE if successful.
**
** This message fills the nMin and mMax elements of the TABLERANGE struct
** with the range of rows that are currently visible.
*/

#define GTM_SCROLL            (WM_USER+27)

/* wParam - Either the TBL_AUTOSCROLL, TBL_SCROLLBOTTOM or TBL_SCROLLTOP
**          flags.
**
** lParam - The LOWORD of the TABLELPARAM struct is the row to be made 
**          visible, the HIWORD is the hWndCol to be made visible.
**
** returns - TRUE if successful.
**
** This message scrolls the specified row/column into view. If the the
** HIWORD is NULL, it is ignored, and if the LOWORD is -1, it is ignored.
*/

#define GTM_SETCONTEXT        (WM_USER+28)

/* wParam - Not used.
**
** lParam - Is the row to set as current.
**
** returns - TRUE if successful.
**
** Set the context (current) row of the table editor. If the row is'nt
** alloc'ed in cache, allocate it.
*/

#define GTM_SETCONTEXTONLY    (WM_USER+29)

/* wParam - Not used.
**
** lParam - Is the row to set as current.
**
** returns - TRUE is successful.
**
** Same as GTM_SETCONTEXT, except cache row is not allocated if needed.
*/

#define GTM_SETFOCUSCELL      (WM_USER+30)

/* wParam - Not used.
**
** lParam - FAR pointer to TABLEFOCUS struct.
**
** returns - TRUE if successful.
**
** The hRow, hWndCol, nSelMin and nSelMax elements are filled into the
** TABLEFOCUS struct prior to sending this message. The focus is then set
** as per specified.
*/

#define GTM_SETFOCUSROW       (WM_USER+31)

/* wParam - Not used.
**
** lParam - Is the row to set the focus too.
**
** returns - TRUE if successful.
**
** This message sets the focus to the specified row.
*/

#define GTM_SETROWFLAGS       (WM_USER+32)

/* wParam - The row flags to set or clear.
**
** lParam - The LOWORD of the TABLELPARAM struct is the row to use, 
**          the HIWORD is a BOOL specifying
**          whether to set (TRUE) or clear (FALSE).
**
** returns - TRUE if successful.
**
** This message sets or clears the given flags for a row.
*/

#define GTM_SETRANGE          (WM_USER+33)

/* wParam - Not used.
**
** lParam - The LOWORD of the TABLELPARAM struct is the nMin and the 
**          HIWORD is the nMax of the Range
**          to set the vertical scroll bar too.
**
** returns - TRUE if successful.
**
*/

#define GTM_DEBUG             (WM_USER+34)

/* wParam - Not used.
**
** lParam - Is the debug option, the only supported option is
**          0 - Dump the Cache.
**
** returns - TRUE if successful.
**
** For internal use only. The cache data is dumped in a message box.
*/

#define GTM_SETUSERMODE       (WM_USER+35)

/* wParam - TRUE if Table in user mode, FALSE if design mode.
**
** lParam - Not used.
**
** returns - VOID.
**
** Toggles table editor user and design mode.
*/

#define GTM_QUERYUSERMODE         (WM_USER+36)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE is table editor is in user mode, FALSE if in design mode.
**
*/

#define GTM_INITHEADER        (WM_USER+37)

/* wParam - HANDLE to current program instance.
**
** lParam - FAR pointer to TABLETEMPLATE struct.
**
** returns - TRUE if successful.
**
*/

#define GTM_SETCOLUMNTEXT     (WM_USER+38)

/* wParam - Column ID.
**
** lParam - LPSTR of string for column.
**
** returns - TRUE if Successful.
**
** This message is usually sent in response to a TBR_FETCHROW message. It
** acts on the current row. If this message is set to column, then wParam
** can be omitted.
*/

#define GTM_GETCOLUMNHWND     (WM_USER+39)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - HWND if Successful.
**
** This message is used to retreive the HWND of the the desired column.
** It is useful if you wish to set the column focus without the HWND.
*/

#define GTM_GETCOLUMNTEXT     (WM_USER+40)

/* wParam - Not used.
**
** lParam - LP to GETTEXT structure.
**
** returns - HWND if Successful.
**
** This message is used to retreive the Text from the desired column.
** If the column is type is CT_LONGEDIT if bitmaps are used,
** then the first byte will contain the bitmap index.
** This message acts on the current row.
*/

#define GTM_GETCELLTEXT       (WM_USER+41)

/* wParam - Not used.
**
** lParam - LPGETTEXT like GTM_GETCOLUMNTEXT
**
** returns - Length of the text in bytes.
**
** This message is used to retreive the Text from the desired cell.
** If the column is type CT_LONGEDIT if bitmaps are used,
** then the first byte will contain the bitmap index.
**
*/

#define GTM_QUERYCOLUMNTYPE   (WM_USER+42)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - The type of the column specified by wParam.
**
** This message is used to retrieve the specified column type. The value
** returned will be one of the column types - CT_* defined above. If this
** message is sent directly to a column, the Column ID in wParam is not
** needed.
*/

#define GTM_QUERYCOLUMNSIZE   (WM_USER+43)

/* wParam - The column ID.
**
** lParam - Not used.
**
** returns - Size of column in pixels.
**
** This message is used to retrieve the size of the specified column in
** pixels. If this message is sent directly to a column, the
** column ID in wParam is not needed.
*/

#define GTM_RESETCACHE        (WM_USER+44)

/* wParam - Max. Number of Rows to Cache.
**
** lParam - See Below.
**
** returns - TRUE if successful.
**
** This message resets the cache to the number of rows specified by wParam.
** If number or rows * row size (which is static and calculated when the
** table is created) > 64k, then number of rows is adjusted appropriately.
** If the LOWORD of lParam is zero, then the table state is reset also,
** else the table state is preserved. If the HIWORD of lParam is > zero,
** the table is not refreshed after the cache and state changes.
*/

#define GTM_QUERYCOLUMNPOS    (WM_USER+45)

/* wParam - The column ID.
**
** lParam - Not used.
**
** returns - >= 0 if successful.
**
** This message is used to retrieve the position of the specified column
** in the table engines virtual column list. This message is usually sent
** in response to a TBN_COLUMNMOVE notification message, which is only
** sent when a table is created with the fColumnMove flag on, and the user
** moves a column to a new position. The new position is returned, with
** 0 being the first column position.
*/

#define GTM_SETCOLUMNPOS      (WM_USER+46)

/* wParam - The column ID.
**
** lParam - New Position of Column
**
** returns - TRUE if Successful.
**
*/

#define GTM_SETCOLUMNSIZE     (WM_USER+47)

/* wParam - The column ID.
**
** lParam - Size of Column in pixels.
**
** returns - TRUE if Successful.
**
*/

#define GTM_QUERYCOLUMNCOUNT  (WM_USER+48)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Number of Columns in Table.
**
*/

#define GTM_QUERYCOLUMNFLAGS  (WM_USER+49)

/* wParam - Not Used.
**
** lParam - Flags to check against column.
**
** returns - TRUE if any of the specifed flags are valid.
**
** This message checks the state of the specified Column flags. This
** message is only processed by and can only be sent to columns.
*/

#define GTM_SETCOLUMNFLAGS    (WM_USER+50)

/* wParam - TRUE to set, FALSE to turn off.
**
** lParam - Flags to check against column.
**
** returns - TRUE if change was successful.
**
** This message sets the state of the specified Column flags. This
** message is only processed by and can only be sent to columns.
*/

#define GTM_INSERTCOLUMN      (WM_USER+51)

/* wParam - Virtual ID to Insert Column.
**
** lParam - LPCOLTEMPLATE of Column to Insert.
**
** returns - TRUE if insertion was successful.
**
** This message inserts a column described by LPCOLUMNTEMPLATE into
** the engines internal structures. The physical ordering of existing
** columns is never changed, as the new column info node is appended to
** the end of the column array. Hence, updating the column is cols + 1.
** If the table is in user mode, all data currently in the cache is
** preserved.
*/

#define GTM_QUERYVIRTUALPOS   (WM_USER+52)

/* wParam - The Virtual Position.
**
** lParam - Not used.
**
** returns - >= 0 if successful.
**
** This message is used to retrieve the Column ID of the specified virtual
** column position in the table engines virtual column list. This message
** is usually sent in response to a TBN_COLUMNMOVE notification message,
** which is only sent when a table is created with the fColumnMove flag on,
** and the user moves a column to a new position. The Column ID is returned.
*/

#define GTM_HITMOUSEMOVE      (WM_USER+53)

/* wParam - HT_* Flags to Test.
**
** lParam - HIWORD is y coord, LOWORD is x coord. (same as MOUSEMOVE lParam)
**
** returns - HT_* Value.
**
** This message can be used anytime the client recieves a WM_MOUSEMOVE
** from the runtime or design hook procs to check whether user has moved
** the mouse over one of the engines active hit areas. Pass in HT_* flags
** only for the area you need to test for better performance. One of the
** Valid HT_ areas or HT_NOAREA is returned.
*/

#define GTM_GETMETRICS        (WM_USER+54)

/* wParam - Not used.
**
** lParam - FAR pointer to TBLMETRICS struct.
**
** returns - VOID.
**
** This message returns the metrics used by the table engine. the lParam is
** a FAR pointer to a TBLMETRICS struct that is filled by this message.
*/

#define GTM_SETZEROCOLHIT     (WM_USER+55)

/* wParam - First Column Hit Offset - must be <= sizeof int.
**
** lParam - Not Used.
**
** returns - VOID.
**
** This message allows the client to offset the zero column insert hit
** area. The Value supplied must be <= sizeof int.
*/

#define GTM_GETCELLTEXTLENGTH       (WM_USER+56)

/* wParam - Column ID.
**
** lParam - Contains the row number
**
** returns - Length of text
**
** This message is used to retreive the length of the text in
** the specified cell, excluding the NULL terminator.
** 0 will be returned if an error occurs or the cell is not text.
**
*/

#define GTM_GETCOLUMNTEXTLENGTH     (WM_USER+57)

/* wParam - Column ID.
**
** lParam - Not used
**
** returns - Length of text
**
** This message is used to retreive the length of text in the
** specified column of the current row.
** 0 will be returned if an error occurs or the cell is not text.
*/

#define GTM_PAINTROWHEADER     (WM_USER+58)

/* wParam - TRUE if background should be repainted.
**
** lParam - LOWORD of the TABLELPARAM struct is the first row to repaint, 
**          HIWORD is last row to repaint.
**
** returns - TRUE if successful
**
** This message is used to repaint the row header.
*/

#define GTM_SETCOLUMNLONGDATA     (WM_USER+59)

/* wParam - Column ID.
**
** lParam - LPSETLONGDATA
**
** returns - TRUE if successful.
**
** This message is used to set the data for a LONG column.
**
*/

#define GTM_GETCELLLONGDATA       (WM_USER+60)

/* wParam - Not used.
**
** lParam - LPGETLONGDATA like GTM_GETCOLUMNLONGDATA
**
** returns - The length of the data retrieved.
**
** This message is used to retreive the data from the desired CT_LONGEDIT cell.
**
*/

#define GTM_GETCOLUMNLONGDATA       (WM_USER+61)

/* wParam - Not used.
**
** lParam - LPGETLONGDATA like GTM_GETCELLLONGDATA
**
** returns - The length of the data retrieved.
**
** This message is used to retreive the data from the desired CT_LONGEDIT cell.
** nRow is ignored.  The context row determines the cell.
**
*/


#define GTM_GETCELLLONGDATALENGTH       (WM_USER+62)

/* wParam - Column ID.
**
** lParam - Contains the row number
**
** returns - Length of long data
**
** This message is used to retreive the length of the data in
** the specified CT_LONGEDIT cell.
** 0 will be returned if an error occurs or the cell is not a LONG.
**
*/

#define GTM_GETCOLUMNLONGDATALENGTH     (WM_USER+63)

/* wParam - Column ID.
**
** lParam - Not used
**
** returns - Length of text
**
** This message is used to retreive the length of of the data in
** the specified CT_LONGEDIT column of the current row.
** 0 will be returned if an error occurs or the cell is not a LONG.
*/

#define GTM_SPLITWINDOW     (WM_USER+64)

/* wParam - TRUE to split, FALSE to unsplit.
**
** lParam - The number of row in lower half of window.
**
** returns - TRUE if successful.
**
** This message is used to split the table window into two panes.
*/

#define GTM_SETRANGEDYNAMIC  (WM_USER+65)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** This message is used instead of GTM_SETRANGE to tell the
** table engine that the number of rows is not known.   The FetchRowProc
** returns FALSE when an attempt is made to fetch beyond the last row.
*/

#define GTM_LIMITDISCARDABLEROWS  (WM_USER+66)

/* wParam - TRUE to cause nMaxRows to apply to discardable rows only.
**          FALSE to cause nMaxRows to apply to total rows in cache.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** The affect of this message is the same as setting fLimitDiscardableRows.
** This message is used to change the way that a full cache is defined.  If
** TRUE then the cache is not full until there are nMaxRows non-discardable
** rows.  Otherwise, the cache is not full until there are nMaxRows of any
** type in the cache.  Rows with RF_Inserted or RF_Edited
** flags are not discardable.
*/

#define GTM_SETCOLUMNDATAWIDTH  (WM_USER+67)

/* wParam - The column ID.
**
** lParam - Data width of column in bytes.
**
** returns - TRUE if successful.
**
** This message sets the maximum data width of a column.
*/

#define GTM_SETCELLTEXTCOLOR  (WM_USER+68)

/* wParam - TRUE if the color information is discardable,
**          FALSE if the context row should not be discarded.
**
** lParam - RGB color for the cell text.
**
** returns - TRUE if successful.
**
** This message sets the color of cell text.
** RGB values greater than MAXVALIDCOLOR clears the
** color setting. This message can only be sent to
** columns.
*/

#define GTM_QUERYCELLTEXTCOLOR  (WM_USER+69)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - RGB color of cell text.
**
** This message retrieves the color of cell text.
** This message can only be sent to columns.
*/

#define GTM_SETTABLECAPABILITIES (WM_USER+70)

/* wParam - Identifies the capability
**
** lParam - TRUE to enable, FALSE to disable
**
** returns - TRUE if successful
**
** This message alters the capabilities of a table.
*/

#define GTM_QUERYTABLECAPABILITIES (WM_USER+71)

/* wParam - Identifies the capability
**
** lParam - No used.
**
** returns - TRUE if the capability is enabled.
**
** This message queries the capabilities of a table.
*/

#define GTM_QUERYHOOK (WM_USER+72)

/* wParam - Identifies the hook
**
** lParam - Not used.
**
** returns - A FAR pointer to the current hook function, or NULL if no
**           hook is defined.
**
** Returns a FAR pointer to a hook function defined with GTM_SETHOOK.
*/

#define GTM_QUERYSPLITWINDOW     (WM_USER+73)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Number of rows in lower half of window or 0 if not split.
**
** This message is used to determine how a table window is split.
*/

#define GTM_SETROWHEADERFLAGS        (WM_USER+74)

/* wParam - Flags to check against row header.
**
** lParam - TRUE to set, FALSE to turn off.
**
** returns - TRUE if change was successful.
**
** This message sets the state of the specified row header flags.
*/

#define GTM_SETROWHEADERSIZE         (WM_USER+75)

/* wParam - Width of row header.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** This message is used set the width of a row header
*/

#define GTM_SETROWHEADERMAPCOLUMN        (WM_USER+76)

/* wParam - Column ID to display in row header if RH_MapColumn.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** This message is used define the column displayed if RH_MapColumn
** row header.
*/

#define GTM_QUERYROWHEADERFLAGS        (WM_USER+77)

/* wParam - Flags to check against row header.
**
** lParam - not used.
**
** returns - TRUE if flags set, false if cleared.
**
** This message querys the state of the specified row header flags.
*/

#define GTM_QUERYROWHEADERSIZE         (WM_USER+78)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Width of row header.
**
** This message is used query the width of a row header
*/

#define GTM_QUERYROWHEADERMAPCOLUMN        (WM_USER+79)

/* wParam - Not used. ID to display in row header if RH_MapColumn.
**
** lParam - Not used.
**
** returns - Map column ID.
**
** This message is used query the column displayed if RH_MapColumn
** row header.
*/

#define GTM_SETROWHEADERTITLETEXT    (WM_USER+80)

/* wParam - Not used.
**
** lParam - LPSTR of string for row header title.
**
** returns - TRUE if Successful.
**
** Sets the text to be displayed above the row header.
*/

#define GTM_GETROWHEADERTITLETEXT    (WM_USER+81)

/* wParam - Maximum length of text.
**
** lParam - LPSTR of buffer to receive text.
**
** returns - Length of text.
**
** Retrieves the text to displayed above the row header.
*/

#define GTM_GETROWHEADERTITLETEXTLENGTH    (WM_USER+82)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Length of text.
**
** Retrieves the length of the row header title.
*/


#define GTM_SETLOCKEDCOLUMNCOUNT     (WM_USER+83)

/* wParam - Number of columns to lock.  The leftmost columns
**          in the table window are locked.
**
** lParam - Unused.
**
** returns - TRUE if successful.
**
** This message is used to lock columns so that they don't scroll.
*/

#define GTM_QUERYLOCKEDCOLUMNCOUNT     (WM_USER+84)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Number of columns that are locked.
**
** This message is used to determine how many columns are locked.
** Use GTM_QUERYVIRTUALPOS to find out which columns are on the left
** of the table.
*/

#define GTM_SETOVERFLOWINDICATOR       (WM_USER+85)

/* wParam - Character to be used to indicate overflow in columns with
**          CF_Overflow flag.  The default is '#'.
**
** lParam - Not used.
**
** returns - TRUE.
**
** This message is used to set the overflow character.  Columns with
** the CF_Overflow flag display this character when the data in the column
** can't be displayed within the columns width.
*/

#define GTM_QUERYOVERFLOWINDICATOR     (WM_USER+86)

/* wParam - Character to be used to indicate overflow in columns with
**          CF_Overflow flag.
**
** lParam - Not used.
**
** returns - Character used to indicate overflow in columns with
**           CF_Overflow flag.
**
** This message is used to get the overflow character.  Columns with
** the CF_Overflow flag display this character when the data in the column
** can't be displayed within the columns width.
*/

#define GTM_QUERYPIXELORIGIN             (WM_USER+87)

/* wParam - Unused.
**
** lParam - Unused.
**
** returns - LOWORD is the x value and HIWORD is the y value.
**
** This message is used to get the origin of the table window in pixels.
** This origin can be used to set the brush origin when painting on top of
** the table window.
*/


#define GTM_QUERYFOCUSROW                  (WM_USER + 88)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - current focus row.
**
** This message returns the current focus row.
*/

#define GTM_SWITCHDATASET                  (WM_USER + 89)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if table window is in split window mode, and
**           data set has been switched.
**
** This message is used to move the focus row between the halves of
** a split window.
**
*/

#define GTM_QUERYDYNAMICSCROLL             (WM_USER + 90)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if the scroll range is currently dynamic.  Note that
**           a dynamic scroll ranges becomes static when the last row
**           is fetched.
**
*/

#define GTM_QUERYMAXDATAWIDTH              (WM_USER + 91)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - Maximum data width for the specified column.
**
** This message returns the maximum data width for the specified
** column. If sent to a column, the wParam can be ommited.
*/

#define GTM_FINDACTUALLASTROW              (WM_USER + 92)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Last row in the table window.
**
** This message is used to get the number of the last row in the
** table window.  This number will be different from TableRange.nMax
** retrieved by GTM_QUERYSCROLL if the table range is dynamic.  This
** message forces the counting of rows in a dynamic range.
*/


#define GTM_SETVIEW                        (WM_USER + 93)

/* wParam - One of the TV_ flags.
**
** lParam - Not used.
**
** returns - VOID.
**
** Toggles table editor between different views (e.g., TV_ListView,
** TV_FormView).
*/

#define GTM_QUERYVIEW                     (WM_USER + 94)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - The current TV_ view identifier.
**
** Returns current view.
*/

#define GTM_QUERYCELLVALIDATESTATUS       (WM_USER + 95)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Last VALIDATE_* value returned from the validate hook.
**
** Returns the status of the last cell validation.
*/

#define GTM_FETCHROWNOCACHE               (WM_USER + 96)

/* wParam - Not used.
**
** lParam - Contains the row to be fetched.
**
** returns - TRUE if successful.
**
** Fetches rows >32k and bypasses the table window cache.
** Sets context to TBL_TEMPROW to avoid cache and FetchRowProc to
** fetch row data into temp area. This means TBH_FETCHROW hook must be set.
*/

#define GTM_REFETCHROWS                   (WM_USER + 97)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** This message is used to avoid reentrant row fetches by causing those
** fetches to happen at a later time. Global linked-list of rows to be
** fetched is maintained. If this message is received and we are still
** yielding, it is simply reposted otherwise the queue row fetches are
** processed.
*/


#define GTM_DESTROYCOLUMNS                (WM_USER + 98)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** Destroys all of the table columns, and resets the cache. Only works
** if the table is in user mode, otherwise returns 0. The table window
** is left with no columns after this message is processed.
*/

#define GTM_SETCOLUMNXYPOSITION   (WM_USER+99)

/* wParam - Column ID.
**
** lParam - X position in LOWORD, and Y position in HIWORD
**
** returns - TRUE if Successful.
**
** This message may be used to set the x,y positions (coordinates) of a
** table column. The x,y values must in pixel values relative to the parent
** table window.
** This message may only be used with table windows which are in Form or Field
** view, and have the flag fFormViewDragDrop set to TRUE. Returns TRUE if
** the column position is set, and returns FALSE if the parameters are
** not correct (i.e. the table window is in list view). If the message
** is sent to the column itself, then wParam may be omitted.
*/

#define GTM_QUERYCOLUMNXYPOSITION   (WM_USER+100)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - X position in LOWORD, and Y position in HIWORD
**
** This message may be used to get the x,y positions (coordinates) of a
** table column. The x,y values are in pixel values relative to the parent
** table window.
** This message may only be used with table windows which are in Form or Field
** view, and have the flag fFormViewDragDrop set to TRUE. Returns the X
** position in the LOWORD and the Y position in the HIWORD; returns 0L
** if parameters are incorrect (i.e. the table window is in list view).
** If this message is sent to the column itself, then wParam may be omitted.
*/

#define GTM_QUERYDEFAULTFIELDPOSITIONS (WM_USER+101)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - TRUE if field positions have not been modified
**
** This message is used to determine if any of the fields in the form or
** field view have been moved to new positions. This includes any use of
** the GTM_SETCOLUMNXYPOSITION message.
*/

#define GTM_RADIOBUTTONCOLUMN          (WM_USER+102)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - TRUE if column changed to be represented by radio buttons.
**
** This message activates a dialog which allows the end-user to specify
** the representation of a form/field view column to be in the form of a
** group of radio buttons. Returns TRUE if the column is transformed to
** radio buttons, otherwise returns FALSE. This message is only available
** when the table window is in user mode.
*/

#define GTM_CHECKBOXCOLUMN           (WM_USER+103)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - TRUE if column changed to be represented by check box.
**
** This message activates a dialog which allows the end-user to specify
** the representation of a form/field view column to be in the form of a
** of a check box. Returns TRUE if the column is transformed to a
** check box, otherwise returns FALSE. This message is only available
** when the table window is in user mode.
*/

#define GTM_QUERYCOLCNTRLDATALEN       (WM_USER+104)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - The number of bytes required to store a columns radio button,
**           or check box control data information.
**
** This message may be used in order to allocate a buffer large enough to
** be used with the GTM_GETCOLCNTRLDATA message. Returns 0 if the column
** is not a check box or table column.
*/

#define GTM_GETCOLCNTRLDATA            (WM_USER+105)

/* wParam - Column ID.
**
** lParam - LPSTR provided by the client to store the control data.
**
** returns - TRUE if column control data is returned in lParam, FALSE if
**           otherwise.
**
** This message returns a check box or radio button field's data. This
** data consists of a list of 0 terminated strings. The list itself is
** terminated with a 00 value. List of a check box column consists of the
** check box's title, followed by the database value which causes an ON
** state, ending with the database value which represents an OFF state.
** The list of radio buttons consists of the group box title, followed
** by a sequence of radio button titles each followed by its own
** corresponding database value.
*/

#define GTM_QUERYDEFRADIOBUTTON       (WM_USER+106)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - The index of the default radio button.
**
** This message returns the index of the default radio button, in a
** field that's represented by radio buttons. This index starts at 0.
*/

#define GTM_SETCOLCNTRLDATA           (WM_USER+107)

/* wParam - Column ID.
**
** lParam - FAR pointer to COLCNTRLDATA structure filled in by the client.
**
** returns -  TRUE if the column control data for the column is set.
**
** This message may be used to set the column control information for a
** column within a field or form view table window. The lColCntrlType element
** of this structure indicates the type of control, radion buttons, check box,
** or multiline; the other elements of the structure must be filled in by the
** client accordingly. Returns FALSE if the table window does not have the
** field or form view.
*/

#define GTM_SWAPROWS                  (WM_USER+108)

/* wParam - Not used.
**
** lParam - One row ID in the LOWORD of the TABLELPARAM struct, the 
**          other row ID in HIWORD.
**
** returns - TRUE if the data of one row is swapped with the data of the other
**           row.
**
** This message may be used to swap the data and flags of two rows. Both rows
** must be valid rows in memory, otherwise FALSE is returned.
*/


#define GTM_QUERYFOCUSCOL             (WM_USER+109)

/* wParam - Table focus notification, TBN_SETFOCUS or TBN_KILLFOCUS.
**
** lParam - Not used.
**
** returns - The HWND of the column that must recieve the notification.
**
** This message may be used by the client to get the HWND of the column
** which must recieve a TBN_SETFOCUS or a TBN_KILLFOCUS notification. With
** a TBN_SETFOCUS notification the notify proc is sent the HWND of the table
** window, and the HWND of the column that used to have the focus. This
** information is used by the client to notify the table window itself. The
** client must use this message to get the HWND of the column that must
** recieve the TBN_SETFOCUS notification, in order to notify the column
** gaining the focus. The reverse of this is true for the TBN_KILLFOCUS
** notification.
*/

#define GTM_QUERYFOCUSWIN             (WM_USER+110)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - The HWND of the window that used to have the focus.
**
** This message can be used to get the HWND of the window outside the table
** windows that used to have the focus before the focus is switch over to a
** table column.
*/

#define GTM_DESIGNCACHECREATE          (WM_USER+111)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns -  TRUE if the cache is created successfully.
**
** This message can be used to create a cache in design mode. The cache may be
** used to display sample data.
*/

#define GTM_QUERYCLICKONEMPTYROW       (WM_USER+112)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns -  TRUE if the last user click was on an empty row.
**
** This message will return FALSE if the table is not table view.
** This message may be used to check whether if the last click was on an
** empty row.
*/

#define GTM_GETLONGDATAFORDISCARD      (WM_USER+113)

/* wParam - Not used.
**
** lParam - LPGETLONGDATA like GTM_GETCOLUMNLONGDATA
**
** returns - The length of the data retrieved.
**
** This message is used to retreive the data from the desired CT_LONGEDIT
** cell. This message assumes the row is in memory just changes the context
** row temporarily.
**
*/

#define GTM_QUERYKILLFOCUSROW          (WM_USER+114)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - The row number on a kill focus notification.
**
** This message is used to retreive the row number of the row containing
** the cell which is being destroyed. The client can use this message
** to fetch the row number on a TBN_KILLFOCUS notification.
**
*/

#define GTM_MOVECOLUMN          (WM_USER+115)

/* wParam - None.
**
** lParam - The LOWORD contains the old column position, and the HIWORD
**          contains the new column position.
**
** returns - TRUE if successfull, and FALSE if invalid parameters have
**           been passed in.
**
** This message may be used to change the virtual position of a column.
**
*/

#define GTM_QUERYSETFOCUS      (WM_USER+116)

/* wParam - None.
**
** lParam - None.
**
** returns - TRUE if the table engin is in the middle of processing a
**           SETFOCUS message, otherwise returns FALSE.
**
** This message may be used to see if the table engin is in the middle of
** processing a WM_SETFOCUS message. This may be used by the client to delay
** setting the focus, till the table engin is done processing WM_SETFOCUS.
**
*/

#define GTM_SETDROPDOWNFLAGS       (WM_USER+117)

/* wParam - TRUE to set, FALSE to turn off.
**
** lParam - Flags to check against column's drop down flags.
**
** returns - TRUE if change was successful.
**
** This message sets the state of the specified drop down flags
** associated with a column. This
** message is only processed by and can only be sent to columns.
*/

#define GTM_QUERYDROPDOWNFLAGS       (WM_USER+118)

/* wParam - Not used.
**
** lParam - Flags to check against columns drop down flags.
**
** returns - TRUE if any of the specifed flags are valid.
**
** This message checks the state of the drop down flags. This
** message is only processed by and can only be sent to columns.
*/

#define GTM_QUERYDROPDOWNWINDOW        (WM_USER+119)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - Window handle of drop down window associated with this column.
**
**
*/

#define GTM_SETROWLINES            (WM_USER+120)

/* wParam - New row height in lines.
**
** lParam - DDN_* drop down type.
**
** returns - TRUE if successful.
**
** Indicate how many lines to display in each row of a list view.
**
*/

#define GTM_QUERYROWLINES         (WM_USER+121)

/* wParam - Not used.
**
** lParam - Not used.
**
** returns - Number of lines per row.
**
** Indicate the number of lines in each row of the list view.
**
*/

#define GTM_SETEXTENDEDCOLUMNTYPE        (WM_USER+122)

/* wParam - Column ID.
**
** lParam - ECT_ extension to CT_EDIT column type.
**
** returns - TRUE if successful.
**
** Gives a table window list column an extended type, such as radio button.
**
*/

#define GTM_QUERYEXTENDEDCOLUMNTYPE        (WM_USER+123)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - ECT_ extended column type.
**
** Indicate the extended column type of a column.
**
*/

#define GTM_SETDROPDOWNUSER        (WM_USER+124)

/* wParam - Column ID.
**
** lParam - Window handle of user defined drop down.
**
** returns - TRUE if successful.
**
** Defines the specified window handle as the drop down for a column.
** The drop down type will be defined as DDN_User
**
*/

#define GTM_QUERYMAXDROPDOWNLINES     (WM_USER+125)

/* wParam - Column ID.
**
** lParam - Not used.
**
** returns - TRUE if successful.
**
** Retrieve the maximum number of drop down lines.
** 0 indicates the default.
**
*/

#define GTM_SETMAXDROPDOWNLINES        (WM_USER+126)

/* wParam - Column ID.
**
** lParam - Maximim drop down lines.  0 indicates default.
**
** returns - TRUE if successful.
**
** Defines the maximum number of lines displayed in a drop down window.
**
*/

#define GTM_QUERYTABLEBIDI                (WM_USER+127)

/* wParam - None.
**
** lParam - None.
**
** returns - TRUE if the table is a bi-directional table.
**           otherwise returns FALSE.
**
** This message may be used to see if the table is a BiDi table.
**
*/

#define GTM_REPOSITIONDROPDOWN            (WM_USER+128)
/* wParam - None.
**
** lParam - None.
**
** returns - Not used.
**
** Tells the table window to reposition drop down windows because the
** table window may have been moved.
** 
*/

#define GTM_SETEXTENDEDCOLUMNDATA            (WM_USER+129)
/* wParam - wType
**
** lParam - LPSETEXTENDEDCOLUMNDATA.
**
** returns - TRUE if data can be set 
**
** Set extended column data (e.g.; check box values).  Sent to a column.
** 
*/

#define GTM_GETEXTENDEDCOLUMNDATA            (WM_USER+130)
/* wParam - wType
**
** lParam - LPGETEXTENDEDCOLUMNDATA.
**
** returns - TRUE if this data exists
**
** Get extended column data (e.g.; check box values).  Sent to a column.
** 
*/

#define GTM_GETEXTENDEDCOLUMNDATALENGTH            (WM_USER+131)
/* wParam - wType
**
** lParam - Not used.
**
** returns - Length of data. 
**
** Get extended column data (e.g.; check box values) length in bytes.  
** Sent to a column.
** 
*/

  #define TAREA_YOverColumnHeader 0x0001
  #define TAREA_YOverSplitRows 0x0002
  #define TAREA_YOverNormalRows 0x0004
  #define TAREA_YOverSplitBar  0x0008

  #define TAREA_XOverRowHeader  0x0100
  #define TAREA_XOverLockedColumns  0x0200
  #define TAREA_XOverUnlockedColumns  0x0400
  #define TAREA_XOverLockedColumnsBorder 0x0800

  typedef struct tagOBJECTSFROMPOINT
  {
    POINT ptClient;
    int nRow;
    HWND hWndCol;
    DWORD dwArea;
  } OBJECTSFROMPOINT;

  typedef OBJECTSFROMPOINT FAR *LPOBJECTSFROMPOINT;

#define GTM_OBJECTSFROMPOINT            (WM_USER+132)
/* wParam - Not used
**
** lParam - LPOBJECTSFROMPOINT
**
** returns - FALSE if function failed because point was outside of client area. 
**
** Get row and column under a client area location
** 
*/


#define GTM_CONTEXTMENUSELECTROWS           (WM_USER+133)
/* wParam - Not used
**
** lParam - LOWORD and HIWORD are X and Y screen coordinates
**
** returns - FALSE if function failed because point was outside of client area. 
**
** Select the row under the mouse unless it is already selected.  This function is
** is called in response to the WM_CONTEXTMENU message, before the caller displays
** a popup menu.
** 
*/


#define GTM_USER              (WM_USER+200)

/*
**  Messages reserved for private use by the application.
**
*/

#endif

// These prototypes are for The Memory Subunit Hooks...

  typedef LPVOID(*LPLOCALALLOCPROC) (UINT);
  typedef LPVOID(*LPLOCALREALLOCPROC) (LPVOID, DWORD);
  typedef LPVOID(*LPGLOBALALLOCPROC) (DWORD);
  typedef LPVOID(*LPGLOBALREALLOCPROC) (LPVOID, DWORD);

// This prototype is for the data validation hook...

  typedef int (*LPVALIDATEPROC) (HWND, LONG, int, LONG, int);

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* A far pointer to a TABLETEMPLATE structure is passed in the LPSTR
** lpCreateParam element of the CreateWindow Call. After the Table Editor
** is created, this structure can be discarded. A far pointer to this
** structure can also be passed with the GTM_INITHEADER message.
*/

typedef struct tagCOLTEMPLATE
{
  int nWidth;                   /* Width of Column               */
  int nDataWidth;               /* Width of Data                 */
  DWORD dwFlags;                /* Column Status Flags...        */
  BYTE bColumnType;             /* Column Type                   */
  LPSTR lpTitle;                /* Column Title                  */
  int nXPos;                    /* Col X Pos in form, field view */
  int nYPos;                    /* Col Y Pos in form, field view */
  int nCntrlDataLen;            /* Number of bytes in cntrl data */
  LPSTR lpCntrlData;            /* Data to define the edit cntrl */
  int nDefButton;               /* Index to the def radio button */
  int nMaxDropDownLines;        /* Maximum lines in drop down window.  0 indicates defaults. */
  int nExtendedColumnType;      /* Standard edit vs check box vs list */
  DWORD dwDropDownFlags;        /* Style of drop down window,if any */
  BYTE bExtendedDataType;       /* Type of extended data associated with column */
  int nExtendedDataLen;         /* Number of bytes to go with extended column type */
  LPSTR lpExtendedData;         /* Data to go with extended column type */
} COLTEMPLATE;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

typedef struct tagTABLETEMPLATE
{
  int nColumns;                 /* Number of Columns in Table    */
  int nMaxRows;                 /* If bUserMode, Maximum Rows    */
  WORD wView;                   /* Defines table's basic appearance  */
  int nLinesPerRow;             /* Number of lines in each row */
  FLAG fToggleCaption:1;        /* User toggle Caption?          */
  FLAG fSizeColumns:1;          /* Can columns be sized?         */
  FLAG fMoveColumns:1;          /* Can columns be moved?         */
  FLAG fUserMode:1;             /* Create Cache Structure?       */
  FLAG fDiscardable:1;          /* If fUserMode, Discard Rows?   */
  FLAG fSelectColumns:1;        /* Can Columns be selected....   */
  FLAG fInsertColumns:1;        /* Can Columns be Inserted...    */
  FLAG fAlwaysVertScroll:1;     /* Always display scroll bar?    */
  FLAG fSelectRows:1;           /* Hit area for rows?            */
  FLAG fUnused:1;               /* Unused flag                   */
  FLAG fIgnoreInsKey:1;         /* Insert key is not used        */
  FLAG fAdjustSplit:1;          /* Split bar can be dragged      */
  FLAG fNoHideSel:1;            /* Used ES_NOHIDESEL style       */
  FLAG fLimitDiscardableRows:1;
  FLAG fHScrollByColumns:1;     /* Scroll by columns, like Excel   */
  FLAG fNewLook:1;              /* Display shaded headers */

    FLAG fSuppressRowLines:1;   /* Show horz. grid lines ?        */
    FLAG fSuppressLastColLine:1;/* Show last column grid line     */
    FLAG fSingleSelection:1;    /* Single selection only ?        */
    FLAG fWaitCursor:1;         /* Display wait cursor */
    FLAG fAutoHScroll:1;        /* Horz scroll when sizing/moving columns? */
    FLAG fEditLeftJustify:1;    /* Left justify a cell when editing */
    FLAG fFormViewDragDrop:1;   /* Let form view columns to be moved */
    FLAG fTableIsBiDi:1;        /* If TRUE table is bi-directional */
	FLAG fDisableRightMouseRowSelection:1; /* Use the right mouse for row selection */
	FLAG fReserved:7;           /* Unused bits */
	
    HFONT hFont;                /* Optional table window font    */
    WORD wRowHeaderFlags;       /* Row Header flags              */
    int nRowHeaderWidth;        /* Width of Row Header           */
    WORD wRowHeaderMapColumn;   /* Column mapped to row header   */
    LPSTR lpRowHeaderTitle;     /* Row Header Title     */

    LPVALIDATEPROC lpValidateProc;      /* Cell Data Validation Proc     */

    LPLOCALALLOCPROC lpLocalAllocProc;  /* Local Allocator Replacement   */
    LPLOCALREALLOCPROC lpLocalReAllocProc;      /* Local ReAllocator Replacement */
    LPGLOBALALLOCPROC lpGlobalAllocProc;        /* Global Allocator Replacement  */
    LPGLOBALREALLOCPROC lpGlobalReAllocProc;    /* Global ReAllocator Replacment */

    COLTEMPLATE Columns[1];     /* Must be last!                 */
  } TABLETEMPLATE;

  typedef COLTEMPLATE FAR *LPCOLTEMPLATE;
  typedef TABLETEMPLATE FAR *LPTABLETEMPLATE;

#define COLTEMPSIZE     sizeof(COLTEMPLATE)
#define TABLETEMPSIZE   sizeof(TABLETEMPLATE)

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* A FAR pointer to this struct is passed in lParam of the GTM_FINDNEXTROW
** and the GTM_FINDPREVROW messages. The Initial starting values are set
** prior to calling the GTM_ messages, and then updated before returning.
*/

  typedef struct tagFINDROW
  {
    int nRow;
    WORD wFlagsOn;
    WORD wFlagsOff;
  } FINDROW;

  typedef FINDROW FAR *LPFINDROW;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* A FAR pointer to this struct is passed in lParam of the GTM_QUERYFOCUS
** and the GTM_SETFOCUSCELL messages. The Initial starting values are set
** prior to calling the GTM_ messages, and then updated before returning.
*/

  typedef struct tagTABLEFOCUS
  {
    int nRow;
    HWND hWndCol;
    int nSelMin;
    int nSelMax;
  } TABLEFOCUS;

  typedef TABLEFOCUS FAR *LPTABLEFOCUS;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* A FAR pointer to this struct is passed in lParam of the GTM_QUERYSCROLL
** and the GTM_QUERYVISIBLERANGE messages. The elements of this structure
** are filled in by these messages.
*/

  typedef struct tagTABLERANGE
  {
    int nPos;
    int nMin;
    int nMax;
    int nXPos;
  } TABLERANGE;

  typedef TABLERANGE FAR *LPTABLERANGE;

/* A FAR pointer to this struct is passed in lParam of the GTM_GETCOLUMNTEXT
** and the GTM_GETCELLTEXT messages. The elements of this structure
** are filled in by these messages.
*/

  typedef struct tagGETTEXT
  {
    int nRow;
    UINT wColID;
    LPSTR lpBuff;
    int nMaxLen;
  } GETTEXT;

  typedef GETTEXT FAR *LPGETTEXT;

/* A FAR pointer to this struct is passed in lParam of the GTM_GETMETRICS
** message. The elements of this structure are filled by this message.
*/

  typedef struct tagTBLMETRICS
  {
    int nHeaderHeight;
    int nCellHeight;
    POINT ptCharBox;
    POINT ptLineSize;
    POINT ptCellLeading;
    int nSplitBarTop;
    int nSplitBarHeight;
    int nLockedColumnsWidth;
  } TBLMETRICS;

  typedef TBLMETRICS FAR *LPTBLMETRICS;


/* A FAR pointer to this struct is passed in lParam of the
** GTM_GETCOLUMNLONGDATA and the GTM_GETCELLLONGDATA messages.
** The elements of this structure are filled in by these messages.
*/
  typedef struct tagGETLONGDATA
  {
    int nRow;
    WORD wColID;
    LPSTR lpBuff;
    int nMaxLen;
  } GETLONGDATA;

  typedef GETLONGDATA FAR *LPGETLONGDATA;

/* A FAR pointer to this struct is passed in lParam of the
** GTM_SETCOLUMNLONGDATA.
*/
  typedef struct tagSETLONGDATA
  {
    LPSTR lpBuff;
    int nLen;
  } SETLONGDATA;

  typedef SETLONGDATA FAR *LPSETLONGDATA;


/* A FAR pointer to this struct is passed in lParam of the GTM_SETCOLCNTRLDATA
** message. The elements of this structure are filled by the client and used by
** the table engin. The lColCntrlType element of this struct must be set to one
** of flag values CF_ColIsCheckBox, CF_ColIsRadioButton, or CF_ColIsMultiLine.
*/
  typedef struct tagCOLCNTRLDATA
  {
    int nCntrlDataLen;
    LPSTR lpCntrlData;
    LONG lColCntrlType;
    int nDefButton;
  } COLCNTRLDATA;

  typedef COLCNTRLDATA FAR *LPCOLCNTRLDATA;

/* A FAR pointer to this struct is passed in lParam of the 
** GTM_SETEXTENDEDCOLUMNDATA
*/
typedef struct tagSETEXTENDEDCOLUMNDATA
   {
   LPVOID lpData;   
   int   nLength;
   } SETEXTENDEDCOLUMNDATA;

typedef SETEXTENDEDCOLUMNDATA FAR * LPSETEXTENDEDCOLUMNDATA;

/* A FAR pointer to this struct is passed in lParam of the 
** GTM_GETEXTENDEDCOLUMNDATA
*/
typedef struct tagGETEXTENDEDCOLUMNDATA
   {
   LPVOID lpData;   
   int   nMaxLength;
   } GETEXTENDEDCOLUMNDATA;

typedef GETEXTENDEDCOLUMNDATA FAR * LPGETEXTENDEDCOLUMNDATA;

#if defined(__cplusplus)
}                               /* End of extern "C" { */

#endif                          /* __cplusplus */


typedef struct tagTABLELPARAM
{
  int nLow;
  int nHigh;
} TABLELPARAM;

typedef TABLELPARAM FAR * LPTABLELPARAM;

#define TBL_SETLPARAM(nL, nH)   \
        {tableLParam.nLow = nL; tableLParam.nHigh = nH;}
#define TBL_GETLPARAM                ((LPARAM)&tableLParam)
#define TBL_LOWLPARAM(lParam)        (((LPTABLELPARAM)lParam)->nLow)
#define TBL_HIGHLPARAM(lParam)       (((LPTABLELPARAM)lParam)->nHigh)

#endif                          /* TBL_H */

/*
 * $History: tbl.h $
 * 
 * *****************  Version 5  *****************
 * User: Dmadison     Date: 10/03/96   Time: 9:17a
 * Updated in $/Patriot/sqlwin/GTIDEV/GTIINC
 * BITTS 58540. Centura hangs as a consequence of using an incompletely
 * qualified sql into variable.
 * Files: gtidev/utildll/table/table32.def
 *         gtidev/utildll/table/tblumsg1.c	
 *         gtidev/utildll/table/tblstrct.h	
 *         src/db/dbsql.c	                
 *         src/os/osfixed.c	
 *         gtidev/gtiinc/tbl.h
 * 
 * 
 * *****************  Version 4  *****************
 * User: Rgegoux      Date: 9/18/96    Time: 2:02
 * Updated in $/Patriot/sqlwin/GTIDEV/GTIINC
 * 
 * *****************  Version 3  *****************
 * User: Rgegoux      Date: 9/03/96    Time: 0:18
 * Updated in $/Patriot/sqlwin/GTIDEV/GTIINC
 * Changed the prototype for TblFindAcualLastRow
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 6/18/96    Time: 7:26p
 * Branched in $/Patriot/sqlwin/GTIDEV/GTIINC
 * Stinger project branched for Patriot
 * 
 * *****************  Version 18  *****************
 * User: Jtierney     Date: 4/18/96    Time: 7:17a
 * Updated in $/win32/sqlwin/GTIDEV/GTIINC
 * Table uses ES_READONLY for non-editable drop down cells.
 * Cell background color is the same as table background color.
 * Double clicking on a cell to enter edit mode works correctly.
 * 
 * 
 * *****************  Version 17  *****************
 * User: Omullarn     Date: 4/17/96    Time: 6:34p
 * Updated in $/WIN32/SQLWIN/GTIDEV/GTIINC
 * Extra space for a flag word used by fed to mark the setting of the
 * table window's icon (so it only does it once)
 *
 */
