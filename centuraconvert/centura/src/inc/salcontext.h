/* The following defines the code execution context while running Sal code */

struct tagSALCONTEXT
{
	LPSTR SC_lpMemoryTop;         /* top of code block     */
	HITEM SC_hItemProgramCounter; /* currently executing item.  */
	HITEM SC_hItemObject;         /* currently executing object.   */
	#ifdef CTD2000
	HOUTLINE SC_hItemOutline;     /* Outline of currently executing object.   */
	#endif
	HWND SC_hWndRunCurrentField;  /* hWndItem      */
	HWND SC_hWndRunCurrentForm;   /* hWndForm      */
	HWND SC_hWndRunMDI;           /* hWndMDI       */
	LPSTR SC_lpRunCurrentForm;    /* LP to variable ds for form */
	LPSTR SC_lpRunCurrentObject;  /* LP to class's instance vars. */
	LPSTR SC_lpRunCurrentFormClass;       /* LP to current form's, or mdi's, class data. */
	LPSTR SC_lpRunCurrentDataSeg; /* LP to data seg of cur form, mdi, dlg, table, or global.       */
	UINT   SC_uCurrentMessage;    /* message */
	LPARAM SC_lCurrentlParam;       /* lParam     */
	WPARAM SC_wCurrentwParam;       /* wParam     */
	LPOUTLINE SC_lpOutline;       /* Current outline. */
	PSTR	SC_pGEvalInternalFunctionVars;	/* -> internal fun locals	*/
	PSTR	SC_pGEvalInternalFunctionParams;/* -> internal fun params      */
} SALCONTEXT *LPSALCONTEXT;