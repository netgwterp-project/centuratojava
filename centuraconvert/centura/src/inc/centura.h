#define TYPEALL(name) \
typedef struct tag##name        name; \
typedef name *                  P##name; \
typedef name NEAR*              NP##name; \
typedef name FAR*               LP##name; \
typedef name HUGE*              HP##name

#include <wtypes.h>
