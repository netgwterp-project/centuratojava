/* $Header: /Eiger/sqlwin/SRC/H/cbtype.h 4     1/05/99 1:16p Nkulkarn $ */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
 *  cbtype.h                                                               *
 *  Centura Builder Type Definitions For DLL Interface                     *
 *  Copyright (c) 1996 Centura Software Corporation.  All Rights Reserved. *
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#ifndef CBTYPE_H
//#define CBTYPE_H

#ifdef _MSC_VER
#pragma warning (disable:4209)  /* nonstandard extension used : benign typedef redefinition */
#endif

/***** All these must match SQLWIN:misc.h. *****/

#ifndef TYPEALL
#define TYPEALL(name) \
typedef struct tag##name        name; \
typedef name *                  P##name; \
typedef name NEAR*              NP##name; \
typedef name FAR*               LP##name; \
typedef name HUGE*              HP##name
#endif

#ifndef NUMITEMTYPEDEF
#define NUMITEMTYPEDEF
typedef int NUMITEMTYPE;
#endif

#ifndef SYMIDDEF
#define SYMIDDEF
typedef DWORD   SYMID;  
typedef SYMID *PSYMID;
#endif

#ifndef NUMBERSIZE

#define NUMBERSIZE   12		/* numeric buffer size */

typedef struct tagNUMBER
{
  BYTE numLength;
  BYTE numValue[NUMBERSIZE];
} NUMBER;
typedef NUMBER FAR *LPNUMBER;
typedef NUMBER *PNUMBER;

#endif

/* DATETIME */

#define DTLENGTHMASK    0x0F

#ifndef MISC_H
typedef struct tagDATETIME
{
  BYTE DATETIME_Length;         /* length of date/time           */
  BYTE DATETIME_Buffer[12];     /* SQLSDAT */
} DATETIME;
typedef DATETIME FAR *LPDATETIME;
typedef DATETIME *PDATETIME;
#endif

#define DTGETLENGTH(dt) ((dt)->DATETIME_Length & DTLENGTHMASK)

/* typedef signed char CHAR; */
/* typedef unsigned char UCHAR; */
/* typedef signed int INT; */
/* typedef unsigned int UINT; */
/* typedef unsigned long ULONG; */

#if !defined(GTI_H)
typedef LPSTR FAR *LPLPSTR;
typedef HWND FAR *LPHWND;
#endif

typedef int SQLCURSORNUMBER;

#ifndef HARRAYDEF
#define HARRAYDEF
typedef HANDLE HARRAY;
#endif

/*
  Templates and HITEM's are two names for the same thing.
*/

typedef DWORD TEMPLATE;

#ifndef MISC_H
typedef ULONG HSTRING;
#endif

#if !defined(SAL_H)
typedef VOID* HFFILE;
typedef int SQLHANDLENUMBER;
#endif

/*
  Handle to UDV, user defined variable.
  A UDV is an instance of a Functional Class.
*/

#ifndef MISC_H
typedef struct
{
  BYTE opaque[12];
}         HUDV,
  FAR * LPHUDV;
#endif

/* typedef BOOL *PBOOL; */
typedef HWND *PHWND;

/* typedef BOOL FAR *LPBOOL; */
typedef CHAR FAR *LPCHAR;
typedef double FAR *LPDOUBLE;
typedef float FAR *LPFLOAT;
typedef ULONG FAR *LPULONG;
typedef HFFILE FAR *LPHFFILE;
typedef HSTRING FAR *LPHSTRING;
typedef SQLHANDLENUMBER FAR *LPSQLHANDLENUMBER;

typedef BOOL NEAR *NPBOOL;
typedef BYTE NEAR *NPBYTE;
typedef CHAR NEAR *NPCHAR;
typedef double NEAR *NPDOUBLE;
typedef DWORD NEAR *NPDWORD;
typedef float NEAR *NPFLOAT;
typedef INT NEAR *NPINT;
typedef LONG NEAR *NPLONG;
typedef VOID NEAR *NPVOID;

/* typedef WORD NEAR *NPWORD; */

typedef WORD HDB;
typedef HDB FAR *LPHDB;         /* far -> database handle        */

/* MUST MATCH SAL.H!!!! */

typedef BOOL(*LPPROCDDMWRITECALLBACK)(LPVOID, LPSTR);
typedef BOOL(*LPPROCDDMREADCALLBACK)(LPVOID, LPLPSTR);
typedef BOOL(*LPPROCDDMDISCARDCALLBACK) (LPVOID);
typedef BOOL(*LPPROCDDMPREPARECALLBACK) (LPSTR, BOOL, LPVOID, LPWORD,
				 LPBOOL, LPINT, LPINT);
#define DDM_ERROR_NO_HANDLER 0
#define DDM_ERROR_NO_RETURN  1
#define DDM_ERROR_RETURNED   2
typedef WORD(*LPPROCDDMERRORCALLBACK) ( WPARAM, LPARAM, LPLONG );


#ifndef OAM_H
#ifndef OAMHANDL_H

#if !defined(_DEF_HITEM)
#define _DEF_HITEM
//typedef DWORD HITEM;
DECLARE_HANDLE(HITEM);
typedef HITEM FAR *LPHITEM;
#endif     // !_DEF_HITEM

#if !defined(_DEF_HOUTLINE)
#define _DEF_HOUTLINE
DECLARE_HANDLE(HOUTLINE);
typedef HOUTLINE FAR *LPHOUTLINE;
#endif     // !_DEF_HOUTLINE

#if !defined(_DEF_LIBHITEM)
#define _DEF_LIBHITEM
/* Ref to item in a dynalib. */
typedef DWORD       LIBHITEM,
            FAR*  LPLIBHITEM;
#endif     // !_DEF_LIBHITEM

#endif     // ifndef OAMHANDL_H

#if !defined(_DEF_HITEMTAGGED)
#define _DEF_HITEMTAGGED
typedef struct tagTEMPLATETAGGED
{
  DWORD farTemplate;
  BYTE tag;
} TEMPLATETAGGED,

 FAR * LPTEMPLATETAGGED,
 FAR * LPHITEMTAGGED,
 HITEMTAGGED;
#endif     // !_DEF_HITEMTAGGED

#endif     // ifndef OAM_H


#ifdef _MSC_VER
#pragma warning (default:4209)  /* nonstandard extension used : benign typedef redefinition */
#endif

#endif                          /* CBTYPE_H */

/*
 * $History: cbtype.h $
 * 
 * *****************  Version 4  *****************
 * User: Nkulkarn     Date: 1/05/99    Time: 1:16p
 * Updated in $/Eiger/sqlwin/SRC/H
 * Checked in for CTD 1.5.1
 * 
 * *****************  Version 3  *****************
 * User: Tmcparla     Date: 11/14/97   Time: 4:35p
 * Updated in $/Phoenix/sqlwin/SRC/H
 * Changes for Visual C++ 5.0.
 * 
 * *****************  Version 2  *****************
 * User: Omullarn     Date: 11/07/97   Time: 3:33p
 * Updated in $/Phoenix/sqlwin/SRC/H
 * Tomahawk/Patriot merge
 * 
 * *****************  Version 1  *****************
 * User: Jlacerda     Date: 11/04/97   Time: 5:34p
 * Created in $/Phoenix/sqlwin/SRC/H
 * 
 * *****************  Version 3  *****************
 * User: Rjacobs      Date: 2/07/97    Time: 11:06p
 * Updated in $/Patriot/sqlwin/SRC/H
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 6/18/96    Time: 6:52p
 * Branched in $/Patriot/sqlroute/odbsal32
 * Stinger project branched for Patriot
 * 
 * *****************  Version 2  *****************
 * User: Dking        Date: 2/28/96    Time: 5:32p
 * Updated in $/WIN32/SqlRouter/odbsal32
 * From initial 10237 build.
 * 
 * *****************  Version 1  *****************
 * User: Joropeza     Date: 2/22/96    Time: 3:54a
 * Created in $/WIN32/SQLWIN/src/h
 * replaces swtype.h
 * 
*/
