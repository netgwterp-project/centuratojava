.head 0 +  Application Description: Distributed Services Base Library
.head 1 -  Outline Version - 4.0.28
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000080100 002C000000020000 0003000000FFFFFF FFFFFFFFFFF8FFFF
0040: FFE1FFFFFF160000 0016000000A20200 00E4010000010000 0001000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
.head 2 -  Outline Window State: Normal
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 020000004700DCFF 1F005405C50E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010004 000F0000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 010004000E000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010004001100 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
.head 3 -  Left: 0.475"
.head 3 -  Top: 0.135"
.head 3 -  Width:  7.225"
.head 3 -  Height: 4.521"
.head 2 +  Options Box Location
.data VIEWINFO
0000: C418E907B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.375"
.head 3 -  Top: 0.073"
.head 3 -  Width:  4.338"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 4.975"
.head 3 -  Top: 0.375"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? Yes
.head 2 -  Use Release 4.0 Scope Rules? No
.head 1 +  Libraries
.head 2 -  File Include: qckbase.apl
.head 2 -  ! File Include: qckcics.apl
.head 2 -  File Include: qckdvc.apl
.head 2 -  File Include: qckmail.apl
.head 2 -  File Include: qckrpt.apl
.head 2 -  File Include: qckspds.apl
.head 2 -  File Include: qcktabs.apl
.head 2 -  ! File Include: qcktux.apl
.head 2 -  File Include: qckwax.apl
.head 2 -  File Include: qckweb.apl
.head 2 -  File Include: qckwebds.apl
.head 2 -  File Include: qckwebmgrbase.apl
.head 2 -  File Include: qckwebraw.apl
.head 2 -  File Include: qwebmsg.apl
.head 2 -  File Include: pagelist.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 +  External Functions
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
.head 3 +  System
.head 4 -  Number: SWP_NOZORDER_PageList = 	    0x0004
.head 4 -  Number: PAGEM_Start = SAM_User + 1000
.head 4 -  Number: PAGEM_Initialize =  PAGEM_Start
.head 4 -  Number: PAGEM_Apply = PAGEM_Start + 1
.head 4 -  Number: PAGEM_Activate = PAGEM_Start + 2
.head 4 -  Number: PAGEM_Deactivate = PAGEM_Start + 3
.head 3 -  User
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 +  Variables
.data RESOURCE 0 0 1 2176053494
0000: 6600000038000000 0000000000000000 020000030000003E 0000001019000000
0020: 010000FE00FF0357 0000000400800000 0102000000FF0780 01000000FF605B00
0040: 00001900E9010000 FF3F
.enddata
.head 2 +  Internal Functions
.head 2 +  Named Menus
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 2189245827
0000: 35060000DB020000 0000000000000000 0200000900FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F9D003C00 000B5F005F457272
0040: 6F7242610073658A 0100000D003C0000 1900A40000010000 FFFF6619001900ED
0060: 0100FF3F32005800 00190001FB00FF0F 4B000000D6190001 FE00FF0364000000
0080: 0400840000020000 00FFFF6068000000 0400ED0200FF3F6C 00580000040002FB
00A0: 00FF0F7000000016 0400030000FE00FF 837400000004B500 0200FFFF60780000
00C0: 000400ED0200FF3F 7C00580000040003 FB00FF2F800000D6 040003FE00FF8384
00E0: 00000019B5000100 FFFF100180190000 01000100065F5F46 6C61C06722000000
0100: 01DE0019DA000100 FF7F01C880320000 0200000B63517569 636B005461626C65
0120: 400000EC0002002D 19000100FD00FF37 1900196B000100FF FF0101803A000000
0140: 03F3000B03454D61 696C7CB200000400 B71900F4010000FF DF19AC00190001FD
0160: 00FF373200046B00 0200FFFFC1360000 0004DA000200FF7F 01C0806300000004
0180: 0000095F5F635765 62C04D6772120100 78000000046B0002 00FFFFCD040004DA
01A0: 000200FF7F08B000 00000400F60200FF 1F0C0000AC000400 03FD00FF07100000
01C0: 00046B000300FFFF C114000000044200 0700000001C000D8 050000007F18B000
01E0: 00001900F2010000 FF1F310000AC0019 0001FD00FF074A00 0000196B000100FF
0200: FF21018010000005 C20000125F5F0353 657269616C004E75 6D6265727C00D900
0220: 04005B040002FB00 FF6F0400D6040002 FE00FF9B080004B5 000300FFFF660C00
0240: 0400ED0300FFBF01 06320006000000DA 0B574D40466F726D 400000F602009619
0260: 000100FE00FF9B19 0019B5000100FFFF 8001802900000007 E1000A5F5F015061
0280: 67659A0000EC0005 00AD040002FD00FF 370400046B000200 FFFFCD080004DA00
02A0: 0200FF7F0CB30004 00F60200FFDF102C 0019000100FD00FF 87018036000C0008
02C0: 000D5F5F6F4D4207 5E000000037B0019 4B00010000FFFFCD 190004DA000300FF
02E0: 7F1DB00000001900 F60100FF1F
.enddata
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field: cQuickMLField
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Check Box:
.head 3 -  Child Table: cQuickTable
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture: cQuickPicture
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickCicsDS
.head 2 -  Application Actions
