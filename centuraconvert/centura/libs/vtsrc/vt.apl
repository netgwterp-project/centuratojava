.head 0 +  Application Description: Visual Toolchest
vt.apl - Comprehensive library include.
 $Archive: /Centura Software/Visual Toolchest/exe/vt.apt $
$Revision: 4 $
  $Author: Cgrove $
    $Date: 11/14/96 3:03p $
.head 1 -  Outline Version - 4.0.28
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 00000000000C0100 002C000000020000 0003000000FFFFFF FFFFFFFFFFF8FFFF
0040: FFE1FFFFFF340000 0034000000640300 0003020000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
.head 2 -  Outline Window State: Normal
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0200000000000000 0000002803130500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E6
0040: 1E840A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E61E840A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000FA1E0E0D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
.head 3 -  Left: 0.01"
.head 3 -  Top: 0.008"
.head 3 -  Width:  7.99"
.head 3 -  Height: 4.992"
.head 2 +  Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? No
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 6.39"
.head 3 -  Top: 1.075"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? Yes
.head 2 -  Use Release 4.0 Scope Rules? No
.head 1 +  Libraries
.head 2 -  File Include: vtarray.apl
.head 2 -  File Include: vtcal.apl
.head 2 -  File Include: vtcomdlg.apl
.head 2 -  File Include: vtcomm.apl
.head 2 -  File Include: vtdebug.apl
.head 2 -  File Include: vtdos.apl
.head 2 -  File Include: vtfile.apl
.head 2 -  File Include: vtfontnm.apl
.head 2 -  File Include: vtlbx.apl
.head 2 -  File Include: vtlstvw.apl
.head 2 -  File Include: vtmenu.apl
.head 2 -  File Include: vtmenuext.apl
.head 2 -  File Include: vtmeter.apl
.head 2 -  File Include: vtmisc.apl
.head 2 -  File Include: vtmsgbox.apl
.head 2 -  File Include: vtpal.apl
.head 2 -  File Include: vtspin.apl
.head 2 -  File Include: vtsplit.apl
.head 2 -  File Include: vtstr.apl
.head 2 -  File Include: vttblwin.apl
.head 2 -  File Include: vtwin.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 +  External Functions
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
.head 3 +  System
.head 3 +  User
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 -  Variables
.data RESOURCE 0 0 1 1537672241
0000: 2200000013000000 0000000000000000 0200000100000004 000000D019000000
0020: 01FE00FF03
.enddata
.head 2 +  Internal Functions
.head 2 -  Named Menus
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 3760730002
0000: 41040000E1010000 0000000000000000 0200000500FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F13013800 00000F6300446573
0040: 6B746F704C006973 74426F784E01C200 0B00000043190000 0001FA0000FF6F19
0060: 00D6190001FE00FF 833200000019B500 0100FFFF604B0000 001900ED0100FF3F
0080: 6400580000190001 FB00FF0F7D000000 D6190001FE00FF83 9600000019B50001
00A0: 00FFFF60AF000000 1900ED0100FF3FC8 00580000190001FB 00FF0FE1000000D6
00C0: 190001FE00FF83FA 00000019B5000100 FFFF100180640000 010001000B634469
00E0: 724C3E697C00C000 00040000005B1900 01FB00FF6F1900D6 190001FE00FF9B32
0100: 0019B5000100FFFF 664B001900ED0100 FF3F0180044B0000 020000001E0F5472
0120: 65F8654C69005E00 0000030000006F19 00E9010000FFBF19 5900190001FB00FF
0140: 6F3200D6190001FE 00FF430180AF0000 0603000C634669F8 6C654C01D6000000
0160: 070000DE0019D200 010000FF7F19B300 1900F60100FFDF32 AC00190001FD00FF
0180: 374B00196B000100 FFFFCD640019DA00 0100FF7F7DB30019 00F60100FFDF96AC
01A0: 00190001FD00FF87 01807D000C000400 0D6352C06164696F 4C69079A00000005
01C0: 790000194B000100 00FFFFCD190019DA 000100FF7F32B300 1900F60100FFDF4B
01E0: AC00190001FD00FF 376400196B000100 FFFF01
.enddata
.head 2 +  Default Classes
.head 3 -  MDI Window:
.head 3 -  Form Window: cBaseForm
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture: cQuickPicture
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control:
.head 2 -  Application Actions
