Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalWsk_32.apl
		* Description	: Winsock support
		* Author	: Gianluca Pivato
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 00000000006A0000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFFFFFFFF FF000000007C0200 004D010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Normal
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.388"
			Top:    0.729"
		Fully Qualified External References? Yes
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
		File Include: XSalRob_32.apl
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: 3D Face Color
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSal2 Winsock support
			Library name: xsal2_32.dll
				Function: wsockStart
					Description: HWND wsockStart( 
								LONG p_lTimeoutLimit, 
								INT p_iMaxSockets, 
								HWND p_hwndCallback, 
								FARPROC p_fpCallback );
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						Number: LONG
						Number: INT
						Window Handle: HWND
						Number: DWORD
				Function: wsockStop
					Description: BOOL wsockStop( 
								HWND p_hwndWsock );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
				Function: wsockCreate
					Description: SOCKET wsockCreate(  
								HWND p_hwndWsock, 
								LPSTR p_lpszIP, 
								u_short p_iPort, 
								int p_iType, 
								int p_iEvents );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
						Number: INT
						Number: INT
				Function: wsockDestroy
					Description: BOOL wsockDestroy( 
								SOCKET p_socket, 
								HWND p_hwndWsock );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: INT
						Window Handle: HWND
				Function: wsockConnect
					Description: INT wsockConnect( 
								SOCKET p_socket, 
								HWND p_hwndWsock, 
								LPSTR p_lpszIP, 
								u_short p_iPort );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: INT
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: wsockListen
					Description: INT wsockListen( 
								SOCKET p_socket, 
								HWND p_hwndWsock, 
								INT p_iBackLog );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: INT
						Window Handle: HWND
						Number: INT
				Function: wsockAccept
					Description: SOCKET wsockAccept( 
								SOCKET p_socket, 
								HWND p_hwndWsock );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: INT
						Window Handle: HWND
				Function: wsockSend
					Description: INT wsockSend( 
								SOCKET p_socket, 
								HWND p_hwndWsock, 
								LPSTR p_lpBuffer, 
								ULONG p_lLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: INT
						Window Handle: HWND
						String: LPVOID
						Number: LONG
				Function: wsockSendTo
					Description: INT wsockSendTo( 
								SOCKET p_socket, 
								HWND p_hwndWsock, 
								LPSTR p_lpszIP, 
								u_short p_iPort, 
								LPSTR p_lpBuffer, 
								INT p_iLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: INT
						Window Handle: HWND
						String: LPSTR
						Number: WORD
						String: LPVOID
						Number: LONG
				Function: wsockRead
					Description: INT wsockRead( 
								SOCKET p_socket, 
								HWND p_hwndWsock, 
								LPSTR p_lpBuf, 
								ULONG p_lLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: INT
						Window Handle: HWND
						String: LPVOID
						Number: LONG
				Function: wsockReadFrom
					Description: INT wsockReadFrom( 
								SOCKET p_socket, 
								HWND p_hwndWsock, 
								LPSTR p_lpBuf, 
								ULONG p_lLen, 
								struct in_addr FAR * p_lpIP );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: INT
						Window Handle: HWND
						String: LPVOID
						Number: LONG
						Receive Number: LPLONG
				Function: wsockHasData
					Description: LONG wsockHasData( 
								SOCKET p_socket );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: INT
				Function: wsockGetIP
					Description: ULONG wsockGetIP( 
								SOCKET p_socket );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: INT
				Function: wsockGetHostByName
					Description: INT wsockGetHostByName( 
								HWND p_hwndWsock, 
								LPSTR p_lpszHostName, 
								LPLONG p_lpIP );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						String: LPSTR
						Receive Number: LPLONG
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! *** XSal2 Winsock message
				Number: SAM_WinsockEvent	= 0x4500
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture:
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control:
		Application Actions
