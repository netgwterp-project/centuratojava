.head 0 +  Application Description: * Pivato Consulting, inc.
* XSal - eXtended Sal
*
* File name     : XSal_32.apl
* Description	: XSal2
* Author	: Gianluca Pivato
* Version	: 2.10.2

Copyright � 1998-2002 by Pivato Consulting, inc. All Rights Reserved.
.head 1 -  Outline Version - 4.0.26
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000700000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000EF0100 0053010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D01000000075769 6E646F7773
.enddata
.data DT_MAKERUNDLG
0000: 00000000001C433A 5C57494E444F5753 5C4465736B746F70 5C424B4733322E65
0020: 78651C433A5C5749 4E444F57535C4465 736B746F705C424B 4733322E646C6C1C
0040: 433A5C57494E444F 57535C4465736B74 6F705C424B473332 2E61706300000101
0060: 0121443A5C4D7376 635C5853616C5C42 4B475C312E305C33 325C424B4733322E
0080: 72756E21443A5C4D 7376635C5853616C 5C424B475C312E30 5C33325C424B4733
00A0: 322E646C6C21443A 5C4D7376635C5853 616C5C424B475C31 2E305C33325C424B
00C0: 4733322E61706300 0001010121443A5C 4D7376635C585361 6C5C424B475C312E
00E0: 305C33325C424B47 33322E6170642144 3A5C4D7376635C58 53616C5C424B475C
0100: 312E305C33325C42 4B4733322E646C6C 21443A5C4D737663 5C5853616C5C424B
0120: 475C312E305C3332 5C424B4733322E61 7063000001010121 443A5C4D7376635C
0140: 5853616C5C424B47 5C312E305C33325C 424B4733322E6170 6C21443A5C4D7376
0160: 635C5853616C5C42 4B475C312E305C33 325C424B4733322E 646C6C21443A5C4D
0180: 7376635C5853616C 5C424B475C312E30 5C33325C424B4733 322E617063000001
01A0: 0101
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600010002001B00 0200000000000000 0000EF1FD9090500 1D00FFFF4D61696E
0020: 00
.enddata
.data VIEWSIZE
0000: 2100
.enddata
.head 3 -  Left:   0.138"
.head 3 -  Top:    0.292"
.head 3 -  Width:  6.05"
.head 3 -  Height: 2.719"
.head 2 +  Options Box Location
.data VIEWINFO
0000: 0418B80BB80B2500
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left:   4.15"
.head 3 -  Top:    1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left:   0.19"
.head 3 -  Top:    0.033"
.head 3 -  Width:  5.2"
.head 3 -  Height: 2.633"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left:   8.375"
.head 3 -  Top:    1.094"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? No
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 1 +  Libraries
.head 2 -  ! *** XSalBackground
.head 2 -  File Include: XSalBkg_32.apl
.head 2 -  ! *** XSalTableWindow
.head 2 -  File Include: XSalCpt_32.apl
.head 2 -  ! *** XSalSplitter
.head 2 -  File Include: XSalSpl_32.apl
.head 2 -  ! *** XSalToolbar
.head 2 -  File Include: XSalTlb_32.apl
.head 2 -  ! *** XSalImage
.head 2 -  File Include: XSalImg_32.apl
.head 2 -  ! *** XSalSybase
.head 2 -  File Include: XSalSyb_32.apl
.head 2 -  ! *** XSalUDV
.head 2 -  File Include: XSalUdv_32.apl
.head 2 -  ! *** XSalTabControl
.head 2 -  File Include: XSalTab_32.apl
.head 2 -  ! *** XSalTooltip
.head 2 -  File Include: XSalTtp_32.apl
.head 2 -  ! *** XSalRemoteObjects Client
.head 2 -  File Include: XSalRbc_32.apl
.head 2 -  ! *** XSalRemoteObjects Server
.head 2 -  File Include: XSalRbs_32.apl
.head 2 -  ! *** XSalZip
.head 2 -  File Include: XSalZip_32.apl
.head 2 -  ! *** XSalString
.head 2 -  File Include: XSalStr_32.apl
.head 2 -  ! *** XSalString
.head 2 -  File Include: XSalStr_32.apl
.head 2 -  ! *** XSalZip
.head 2 -  File Include: XSalZip_32.apl
.head 2 -  ! *** XSalShortcut
.head 2 -  File Include: XSalShc_32.apl
.head 2 -  ! *** XSalScript
.head 2 -  File Include: XSalScr_32.apl
.head 2 -  ! ** XSalMap
.head 2 -  File Include: XSalMap_32.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 +  External Functions
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
.head 3 +  System
.head 3 -  User
.head 2 -  Resources
.head 2 +  Variables
.head 2 -  Internal Functions
.head 2 -  Named Menus
.head 2 +  Class Definitions
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window: cCPTableWindow
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field: cQuickMLField
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture: cQuickPicture
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control:
.head 2 -  Application Actions
