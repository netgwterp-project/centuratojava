Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalRob_32.apl
		* Description	: XSalRemoteObjects
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000950000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000E10100 0035010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000081EC2120500 1D00FFFF4D61696E
0020: 0029000100040000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000400 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000400 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
			Left:   7.2"
			Top:    1.479"
			Width:  2.75"
			Height: 4.26"
		Options Box Location
.data VIEWINFO
0000: D4181509B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   8.675"
			Top:    2.0"
		Fully Qualified External References? No
		Reject Multiple Window Instances? Yes
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! *** Server notification message
				! //
						! // XSalRemoteObjects Constants
						! //
				! *** Messages
				Number: SAM_IncomingMessage	= 0x5001
					! The callback window receives this message when the client
							  receives a message from another client
								wParam =	not used.
								lParam =	message buffer pointer
				Number: SAM_AsyncCompleted	= 0x5002
					! The callback window receives this message when the asyncronous
							  service is completed on the server.	
								wParam =	error code.
								lParam =	asyncronous service ID
				Number: SAM_ServerCallback	= 0x5003
					! The callback window receives this message for each
							  server operation.
								wParam =	not used.
								lParam =	pointer to the notification info structure
				! *** Command codes
				Number: ROB_ALLOCATE	=1
				Number: ROB_FREE	=2
				Number: ROB_SETM	=3
				Number: ROB_GETM	=4
				Number: ROB_SETC	=5
				Number: ROB_GETC	=6
				Number: ROB_SETB	=7
				Number: ROB_GETB	=8
				Number: ROB_EXEC	=9
				Number: ROB_EXECASYNC	=10
				Number: ROB_TESTASYNC	=11
				Number: ROB_ABORTASYNC	=12
				Number: ROB_SETOBJ	=13
				Number: ROB_GETOBJ	=14
				Number: ROB_ASYNCEND	=0x80000000
				! *** Errors
				Number: ROBERR_ALLOCFAILED	=0x0001
				Number: ROBERR_ARRAYFAILED	=0x0002
				Number: ROBERR_MEMORYFAILED	=0x0003
				Number: ROBERR_EVALFAILED	=0x0004
				Number: ROBERR_UNKNOWNOBJECT	=0x0005
				Number: ROBERR_VALUETRUNCATED	=0x0006
				Number: ROBERR_INVALIDNAME	=0x0007
				Number: ROBERR_MSGTOOLONG	=0x0008
				Number: ROBERR_TOOMANYASYNC	=0x0009
				Number: ROBERR_ALLOCREFUSED	=0x0010
				Number: ROBERR_INVALIDASYNCID	=0x0011
				Number: ROBERR_COMMUNICATION	=0x0012
				Number: ROBERR_SERVERBUSY	=0x0013
				Number: ROBERR_INVALIDTEMPLATE	=0x0014
				Number: ROBERR_ASYNCEXECUTING	=0x0015
				! *** Async services status
				Number: ROBASYNC_ERROR		=0
				Number: ROBASYNC_DONE		=1
				Number: ROBASYNC_EXECUTING	=2
				Number: ROBASYNC_WAITING	=3
				! *** Winsock events
				Number: FD_READ         	=0x01
				Number: FD_ACCEPT       	=0x08
				Number: FD_CLOSE        	=0x20
				Number: FD_EXECASYNC		=0x80
				! *** Winsock errors
				Number: WSABASEERR              =10000
				Number: WSAEINTR                =(WSABASEERR+4)
				Number: WSAEBADF                =(WSABASEERR+9)
				Number: WSAEACCES               =(WSABASEERR+13)
				Number: WSAEFAULT               =(WSABASEERR+14)
				Number: WSAEINVAL               =(WSABASEERR+22)
				Number: WSAEMFILE               =(WSABASEERR+24)
				Number: WSAEWOULDBLOCK          =(WSABASEERR+35)
				Number: WSAEINPROGRESS          =(WSABASEERR+36)
				Number: WSAEALREADY             =(WSABASEERR+37)
				Number: WSAENOTSOCK             =(WSABASEERR+38)
				Number: WSAEDESTADDRREQ         =(WSABASEERR+39)
				Number: WSAEMSGSIZE             =(WSABASEERR+40)
				Number: WSAEPROTOTYPE           =(WSABASEERR+41)
				Number: WSAENOPROTOOPT          =(WSABASEERR+42)
				Number: WSAEPROTONOSUPPORT      =(WSABASEERR+43)
				Number: WSAESOCKTNOSUPPORT      =(WSABASEERR+44)
				Number: WSAEOPNOTSUPP           =(WSABASEERR+45)
				Number: WSAEPFNOSUPPORT         =(WSABASEERR+46)
				Number: WSAEAFNOSUPPORT         =(WSABASEERR+47)
				Number: WSAEADDRINUSE           =(WSABASEERR+48)
				Number: WSAEADDRNOTAVAIL        =(WSABASEERR+49)
				Number: WSAENETDOWN             =(WSABASEERR+50)
				Number: WSAENETUNREACH          =(WSABASEERR+51)
				Number: WSAENETRESET            =(WSABASEERR+52)
				Number: WSAECONNABORTED         =(WSABASEERR+53)
				Number: WSAECONNRESET           =(WSABASEERR+54)
				Number: WSAENOBUFS              =(WSABASEERR+55)
				Number: WSAEISCONN              =(WSABASEERR+56)
				Number: WSAENOTCONN             =(WSABASEERR+57)
				Number: WSAESHUTDOWN            =(WSABASEERR+58)
				Number: WSAETOOMANYREFS         =(WSABASEERR+59)
				Number: WSAETIMEDOUT            =(WSABASEERR+60)
				Number: WSAECONNREFUSED         =(WSABASEERR+61)
				Number: WSAELOOP                =(WSABASEERR+62)
				Number: WSAENAMETOOLONG         =(WSABASEERR+63)
				Number: WSAEHOSTDOWN            =(WSABASEERR+64)
				Number: WSAEHOSTUNREACH         =(WSABASEERR+65)
				Number: WSAENOTEMPTY            =(WSABASEERR+66)
				Number: WSAEPROCLIM             =(WSABASEERR+67)
				Number: WSAEUSERS               =(WSABASEERR+68)
				Number: WSAEDQUOT               =(WSABASEERR+69)
				Number: WSAESTALE               =(WSABASEERR+70)
				Number: WSAEREMOTE              =(WSABASEERR+71)
				Number: WSASYSNOTREADY          =(WSABASEERR+91)
				Number: WSAVERNOTSUPPORTED      =(WSABASEERR+92)
				Number: WSANOTINITIALISED       =(WSABASEERR+93)
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cQuickGraph
		Application Actions
