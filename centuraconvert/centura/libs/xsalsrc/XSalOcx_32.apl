Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalOcx_32.apl
		* Description	: XSalActiveX class libary
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000760000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFFFFFFFF FF000000007C0200 004D010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Normal
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: 1018B80BB80B2500
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.388"
			Top:    0.729"
		Fully Qualified External References? No
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? No
		Use Release 4.0 Scope Rules? No
	Libraries
		File Include: vtarray.apl
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: 3D Face Color
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** System Tray support API
			Library name: user32.dll
				Function: SetWindowLongA
					Description:
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Window Handle: HWND
						Number: INT
						Number: LONG
				Function: GetWindowLongA
					Description:
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Window Handle: HWND
						Number: INT
				Function: GetParent
					Description:
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						Window Handle: HWND
			Library name: shell32.dll
				Function: Shell_NotifyIconA
					Description: BOOL Shell_NotifyIconA(
								DWORD dwMessage,
								PNOTIFYICONDATAA lpData);
							
							typedef struct _NOTIFYICONDATAA {
							        DWORD cbSize; (88)
							        HWND hWnd;
							        UINT uID;
							        UINT uFlags;
							        UINT uCallbackMessage;
							        HICON hIcon;
							        CHAR   szTip[64];
							
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						structPointer
							Number: DWORD
							Window Handle: HWND
							Number: DWORD
							Number: DWORD
							Number: DWORD
							Number: DWORD
							String: char[64]
			! *** GlobalAtoms communication support API
			Library name: kernel32.dll
				Function: GlobalAddAtomA
					Description:
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						String: LPSTR
				Function: GlobalDeleteAtom
					Description:
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: LONG
				Function: GlobalGetAtomNameA
					Description:
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: LONG
						Receive String: LPSTR
						Number: LONG
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! //
						! // XSalActiveX Constants
						! //
				! *** Messages from ActiveX Host
				Number: XSAM_CreateChildForm 	= 0x4001
				Number: XSAM_SetMember 		= 0x4002
				Number: XSAM_GetMember 		= 0x4003
				Number: XSAM_CloseChildForm 	= 0x4004
				! *** Messages to ActiveX Host
				Number: XSAM_SQLWindowsEvent 	= 0x4005
				! *** Atoms length
				Number: XSALOCX_MAXNAMELENGTH 	= 256
				Number: XSALOCX_MAXVALUELENGTH 	= 0xFFFF
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
			! *** XSalActiveX classes
			General Window Class: cXSalActiveXChild
				Description:
				Derived From
				Class Variables
				Instance Variables
				Functions
					! Public
					Function: raiseEvent
						Description:
						Returns
						Parameters
							String: p_sEventName
							Number: p_nEventValue
						Static Variables
						Local variables
							Number: hAtomEvent
						Actions
							Set hAtomEvent = GlobalAddAtomA( p_sEventName )
							If NOT SalSendMsg( 
										SalParentWindow( hWndForm ), 
										XSAM_SQLWindowsEvent, 
										hAtomEvent, 
										p_nEventValue )
								Call GlobalDeleteAtom( hAtomEvent )
					! Private
					Function: __ocxSetMember
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_hAtomMember
							Number: p_hAtomValue
						Static Variables
						Local variables
							Boolean: bOk
							String: sValue
							String: sMemberName
							String: sMemberValue
						Actions
							! Extract atoms
							Call SalStrSetBufferLength( sMemberName, XSALOCX_MAXNAMELENGTH )
							Set bOk = GlobalGetAtomNameA( p_hAtomMember, sMemberName, XSALOCX_MAXNAMELENGTH )
							Call SalStrSetBufferLength( sMemberValue, XSALOCX_MAXVALUELENGTH )
							Set bOk = bOk AND GlobalGetAtomNameA( p_hAtomValue, sMemberValue, XSALOCX_MAXVALUELENGTH )
							! Set member
							Set bOk = __evaluate( 
										"Set " || sMemberName || "=" || sMemberValue,
										sValue )
							If bOk
								! Free atoms
								Call GlobalDeleteAtom( p_hAtomMember )
								Call GlobalDeleteAtom( p_hAtomValue )
							Return bOk
					Function: __ocxGetMember
						Description:
						Returns
							Number:
						Parameters
							Number: p_hAtomMember
						Static Variables
						Local variables
							Boolean: bOk
							String: sValue
							String: sMemberName
							Number: hAtomReturn
						Actions
							! Extract atoms
							Call SalStrSetBufferLength( sMemberName, XSALOCX_MAXNAMELENGTH )
							Set bOk = GlobalGetAtomNameA( p_hAtomMember, sMemberName, XSALOCX_MAXNAMELENGTH )
							! Set member
							Set bOk = bOk AND __evaluate( sMemberName, sValue )
							If bOk
								! Free name atom
								Call GlobalDeleteAtom( p_hAtomMember )
								! Prepare return atom
								Set hAtomReturn = GlobalAddAtomA( sValue )
								Return hAtomReturn
					Function: __evaluate
						Description:
						Returns
							Boolean:
						Parameters
							String: p_sCommand
							Receive String: pr_sValue
						Static Variables
						Local variables
							Number: nType
							Number: nError
							Number: nErrorPos
							Number: nReturn
							String: sReturn
							Date/Time: dtReturn
							Window Handle: hWndReturn
						Actions
							Set nType = 
										SalCompileAndEvaluate( 
											p_sCommand, 
											nError, 
											nErrorPos, 
											nReturn, 
											sReturn, 
											dtReturn, 
											hWndReturn, 
											NOT ( frmXSalActiveXFormServerConsole.m_bDebugMode ), 
											..iGetContext() )
							Select Case nType
								Case EVAL_Number
									Set pr_sValue = SalFmtFormatNumber( nReturn, "0.############" )
									Break
								Case EVAL_String
									Set pr_sValue = sReturn
									Break
								Case EVAL_Date
									Call SalDateToStr( dtReturn, pr_sValue )
									Break
								Default
									Return FALSE
							Return TRUE
					! Virtual implementation
					Function: iGetContext
						Description: You should implement this in your
								objects to avoid conflicts with controls 
								with a similar name
						Returns
							String:
						Parameters
						Static Variables
						Local variables
						Actions
							If frmXSalActiveXFormServerConsole.m_bDebugMode
								Call SalMessageBox( 
										"iGetContext() should be implemented in each template to
										avoid conflicts between controls with similar names.",
										"XSalActiveX Warning", MB_IconAsterisk  )
							Return SalContextCurrent(  )
				Message Actions
					! On 0x000F !WM_PAINT
						      Call SalInvalidateWindow( hWndForm )
					On SAM_Destroy
						Call SalSendMsg( frmXSalActiveXFormServerConsole, XSAM_CloseChildForm, 1, SalWindowHandleToNumber( hWndForm ) )
					On XSAM_GetMember
						Return __ocxGetMember( wParam )
					On XSAM_SetMember
						Return __ocxSetMember( wParam, lParam )
			Form Window Class: cXSalActiveXFormWindow
				Title:
				Icon File:
				Accesories Enabled? Class Default
				Visible? Class Default
				Display Settings
					Display Style? Class Default
					Visible at Design time? Yes
					Automatically Created at Runtime? No
					Initial State: Class Default
					Maximizable? Class Default
					Minimizable? Class Default
					System Menu? Class Default
					Resizable? Class Default
					Window Location and Size
						Left:  
						Top:   
						Width:  5.017"
						Width Editable? Class Default
						Height: 2.786"
						Height Editable? Class Default
					Form Size
						Width:  Class Default
						Height: Class Default
						Number of Pages: Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
				Next Class Child Key: 0
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalActiveXChild
				Menu
				Tool Bar
					Display Settings
						Display Style? Class Default
						Location? Class Default
						Visible? Class Default
						Size: Class Default
						Size Editable? Class Default
						Font Name: Class Default
						Font Size: Class Default
						Font Enhancement: Class Default
						Text Color: Class Default
						Background Color: Class Default
					Contents
				Contents
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Table Window Class: cXSalActiveXTableWindow
				Title:
				Icon File:
				Accesories Enabled? Class Default
				Visible? Class Default
				Display Settings
					Visible at Design time? Yes
					Automatically Created at Runtime? No
					Initial State: Class Default
					Maximizable? Class Default
					Minimizable? Class Default
					System Menu? Class Default
					Resizable? Class Default
					Window Location and Size
						Left:  
						Top:   
						Width:  6.717"
						Width Editable? Class Default
						Height: 3.095"
						Height Editable? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					View: Class Default
					Allow Row Sizing? Class Default
					Lines Per Row: Class Default
				Memory Settings
					Maximum Rows in Memory: Class Default
					Discardable? Class Default
				Next Class Child Key: 0
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalActiveXChild
				Menu
				Tool Bar
					Display Settings
						Display Style? Class Default
						Location? Class Default
						Visible? Class Default
						Size: Class Default
						Size Editable? Class Default
						Font Name: Class Default
						Font Size: Class Default
						Font Enhancement: Class Default
						Text Color: Class Default
						Background Color: Class Default
					Contents
				Contents
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Form Window Class: cXSalActiveXFormServer
				Title:
				Icon File:
				Accesories Enabled? Class Default
				Visible? Class Default
				Display Settings
					Display Style? Class Default
					Visible at Design time? Yes
					Automatically Created at Runtime? Class Default
					Initial State: Class Default
					Maximizable? Class Default
					Minimizable? Class Default
					System Menu? Class Default
					Resizable? Class Default
					Window Location and Size
						Left:  
						Top:   
						Width:  5.117"
						Width Editable? Class Default
						Height: 2.143"
						Height Editable? Class Default
					Form Size
						Width:  Class Default
						Height: Class Default
						Number of Pages: Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
				Next Class Child Key: 1
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
				Menu
				Tool Bar
					Display Settings
						Display Style? Class Default
						Location? Class Default
						Visible? Class Default
						Size: Class Default
						Size Editable? Class Default
						Font Name: Class Default
						Font Size: Class Default
						Font Enhancement: Class Default
						Text Color: Class Default
						Background Color: Class Default
					Contents
				Contents
					List Box: lbList
						Class Child Ref Key: 0
						Class ChildKey: 1
						Class:
						Property Template:
						Class DLL Name:
						Window Location and Size
							Left:   0.0"
							Top:    0.0"
							Width:  4.933"
							Width Editable? Yes
							Height: 1.774"
							Height Editable? Yes
						Visible? Yes
						Multiple selection? No
						Sorted? Yes
						Vertical Scroll? Yes
						Font Name: Default
						Font Size: Default
						Font Enhancement: Default
						Text Color: Default
						Background Color: Default
						Horizontal Scroll? No
						List Initialization
						Message Actions
							On 0x0081 !WM_NCCREATE
								Call SetWindowLongA( hWndItem, -20,
											GetWindowLongA( hWndItem, -20 ) & (0xffffffff-0x00000200) )
								Call SetWindowLongA( hWndItem, -16,
											GetWindowLongA( hWndItem, -16 ) & (0xffffffff-0x00800000) | 0x0100 )
				Class Variables
				Instance Variables
					Number: m_nChildCount
					Number: m_hWndChild[*]
					Number: m_hIcon
					Boolean: m_bDebugMode
				Functions
					! Private
					Function: __onCreate
						Description:
						Returns
						Parameters
						Static Variables
						Local variables
							String: sTitle
							Number: nPos
							Number: nLen
						Actions
							! Call SalHideWindow( hWndForm )
							Set sTitle = SalStrUpperX( strArgArray[0] )
							If NOT sTitle
								Set sTitle = "\\XSALOCX.EXE"
							! Extract the executable name
							Set nLen = SalStrLength( sTitle )
							Set nPos = nLen - 1
							While nPos > 0
								Set nPos = nPos - 1
								If SalStrMidX( sTitle, nPos, 1 ) = "\\"
									Break
							Call SalSetWindowText( 
										hWndForm, 
										SalStrMidX( sTitle, nPos+1, nLen-nPos-5 ) )
					Function: __onDestroy
						Description:
						Returns
						Parameters
						Static Variables
						Local variables
							Number: nIndex
						Actions
							While nIndex < m_nChildCount
								Call SalDestroyWindow( 
											SalNumberToWindowHandle( m_hWndChild[nIndex] ) )
								Set nIndex = nIndex + 1
					Function: __onCreateChildForm
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_hAtomForm
							Number: p_hWndOcxParent
						Static Variables
						Local variables
							Number: nWidth
							Number: nHeight
							String: sFormName
							Window Handle: hWndChild
							Window Handle: hWndParent
						Actions
							If p_hWndOcxParent
								! Close the destruction timer
								Call SalTimerKill( hWndForm, 1 )
								! Get parent handle
								Set hWndParent = SalNumberToWindowHandle( p_hWndOcxParent )
								! Extract form name
								Call SalStrSetBufferLength( sFormName, XSALOCX_MAXNAMELENGTH )
								Call GlobalGetAtomNameA( p_hAtomForm, sFormName, XSALOCX_MAXNAMELENGTH )
								! Read parent size
								Call SalGetWindowSize( hWndParent, nWidth, nHeight )
								! Create child form
								Set hWndChild
											= SalCreateWindowEx( 
												sFormName,
												hWndParent,
												0, 0,
												nWidth, nHeight,
												CREATE_AsChild )
								If NOT hWndChild
									Return FALSE
								! Add the window to the list
								Set m_hWndChild[m_nChildCount] = SalWindowHandleToNumber( hWndChild )
								Set m_nChildCount = m_nChildCount + 1
								! Add the child to the shared list
								Call SalListAdd( lbList, sFormName )
							! Free atom
							Call GlobalDeleteAtom( p_hAtomForm )
							Return TRUE
					Function: __onCloseChildForm
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_hWndChild
							Boolean: p_bClosing
						Static Variables
						Local variables
							Number: nIndex
							String: sFormName
						Actions
							! Remove the form from the shared list
							Call SalGetItemName( SalNumberToWindowHandle( p_hWndChild ), sFormName )
							Set nIndex = SalListSelectString( lbList, -1, sFormName )
							If nIndex != LB_Err
								Call SalListDelete( lbList, nIndex )
							! Remove the window handle from the handle list
							Set nIndex = VisArrayFindNumber( m_hWndChild, p_hWndChild )
							If nIndex > -1
								Call VisArrayDeleteItem( m_hWndChild, nIndex, DT_Number )
							! Destroy the form
							If p_bClosing
								Set m_nChildCount = m_nChildCount - 1
								! If there are no more forms, start the timer to close the host
								If m_nChildCount = 0
									Call SalTimerSet( hWndForm, 1, 20000 )
							Else
								Call SalDestroyWindow( SalNumberToWindowHandle( p_hWndChild ) )
							Return TRUE
				Message Actions
					On 0x0005 !WM_SIZE
						Call SalSetWindowSize( 
									lbList,
									SalPixelsToFormUnits( hWndForm, SalNumberLow( lParam ), FALSE ), 
									SalPixelsToFormUnits( hWndForm, SalNumberHigh( lParam ), TRUE ) )
					On SAM_Create
						Call __onCreate()
						! Get the window's icon handle
						Set m_hIcon = SalSendMsg( hWndForm, 0x007F, 1, 0 )
						! Set the tray icon
						Call Shell_NotifyIconA(
										0,
										88,
										hWndForm,
										1,
										7,
										SAM_User,
										m_hIcon,
										"XSalActiveX Form Server Console" )
						Call SalHideWindow( hWndForm )
					On SAM_Destroy
						Call __onDestroy()
						! Remove the tray icon
						Call Shell_NotifyIconA( 2, 88, hWndForm, 1, 7, 0, 0, STRING_Null )
					On SAM_Timer
						Call SalDestroyWindow( hWndForm )
						Call SalQuit(  )
					On XSAM_CreateChildForm
						Return __onCreateChildForm( wParam, lParam )
					On XSAM_CloseChildForm
						Return __onCloseChildForm( lParam, wParam )
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture:
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cText
		Application Actions
	! *** XSalActiveX Form Server Console
	Form Window: frmXSalActiveXFormServerConsole
		Class: cXSalActiveXFormServer
		Property Template:
		Class DLL Name:
		Title:
		Icon File:
		Accesories Enabled? Class Default
		Visible? Class Default
		Display Settings
			Display Style? Class Default
			Visible at Design time? Yes
			Automatically Created at Runtime? Class Default
			Initial State: Class Default
			Maximizable? Class Default
			Minimizable? Class Default
			System Menu? Class Default
			Resizable? Class Default
			Window Location and Size
				Left:   5.613"
				Top:    2.729"
				Width:  5.283"
				Width Editable? Class Default
				Height: 3.0"
				Height Editable? Class Default
			Form Size
				Width:  0.01"
				Height: 0.01"
				Number of Pages: Class Default
			Font Name: Class Default
			Font Size: Class Default
			Font Enhancement: Class Default
			Text Color: Class Default
			Background Color: Class Default
		Description:
		Named Menus
			Menu: MenuOnTray
				Resource Id: 48744
				Title:
				Description:
				Enabled when:
				Status Text:
				Menu Item Name:
				Menu Item: &Show console
					Resource Id: 48745
					Keyboard Accelerator: (none)
					Status Text:
					Menu Settings
						Enabled when:
						Checked when:
					Menu Actions
						Call SalShowWindow( hWndForm )
					Menu Item Name:
		Menu
			Popup Menu: Server
				Resource Id: 23349
				Enabled when:
				Status Text:
				Menu Item Name:
				Menu Item: Debug mode
					Resource Id: 23350
					Keyboard Accelerator: (none)
					Status Text:
					Menu Settings
						Enabled when:
						Checked when: m_bDebugMode
					Menu Actions
						Set m_bDebugMode = NOT m_bDebugMode
					Menu Item Name:
				Menu Item: Hide
					Resource Id: 23347
					Keyboard Accelerator: (none)
					Status Text:
					Menu Settings
						Enabled when:
						Checked when:
					Menu Actions
						Call SalHideWindow( hWndForm )
					Menu Item Name:
				Menu Item: Quit
					Resource Id: 23348
					Keyboard Accelerator: (none)
					Status Text:
					Menu Settings
						Enabled when:
						Checked when:
					Menu Actions
						Call SalQuit(  )
					Menu Item Name:
		Tool Bar
			Display Settings
				Display Style? Default
				Location? Top
				Visible? Yes
				Size: Default
				Size Editable? Yes
				Font Name: Default
				Font Size: Default
				Font Enhancement: Default
				Text Color: Default
				Background Color: Default
			Contents
		Contents
			List Box: lbList
				Class Child Ref Key: 1
				Class ChildKey: 0
				Class: cXSalActiveXFormServer
				Property Template:
				Class DLL Name:
				Window Location and Size
					Left:   Class Default
					Top:    Class Default
					Width:  Class Default
					Width Editable? Class Default
					Height: Class Default
					Height Editable? Class Default
				Visible? Class Default
				Multiple selection? Class Default
				Sorted? Class Default
				Vertical Scroll? Class Default
				Font Name: Class Default
				Font Size: Class Default
				Font Enhancement: Class Default
				Text Color: Class Default
				Background Color: Class Default
				Horizontal Scroll? Class Default
				List Initialization
				Message Actions
		Functions
		Window Parameters
		Window Variables
		Message Actions
			On SAM_User
				Select Case lParam
					Case 0x0203 !WM_LBUTTONDBLCLICK
						Call SalCenterWindow( hWndForm )
						Call SalShowWindow( hWndForm )
						Break
					Case 0x0202 !WM_LBUTTONUP
					Case 0x0205 !WM_RBUTTONUP
						Call SalTrackPopupMenu( hWndForm, "MenuOnTray", TPM_CursorX|TPM_CursorY, 0, 0 )
						Break
			On SAM_Close
				Call SalHideWindow( hWndForm )
				Return FALSE
