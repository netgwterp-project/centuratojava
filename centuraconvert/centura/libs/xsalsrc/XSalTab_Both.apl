Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalTab_32.apl
		* Description	: XSalTabControl
		* Author	: Gianluca Pivato
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000580000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFFFFFFFF FF000000007C0200 004D010000010000 0001000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Normal
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: 1018B80BB80B2500
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.388"
			Top:    0.729"
		Fully Qualified External References? Yes
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
		File Include: qcktabs.apl
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: 3D Face Color
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalTabControl
			Library name: XSal2_32.dll
				Function: __XSalTabSubclass
					Description: BOOL XSalTabSubclass( 
								HWND p_hwnd );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
				Function: __XSalTabSetTab
					Description: BOOL XSalTabSetTab( 
								HWND p_hwnd, 
								INT p_iTabIdx, 
								LPSTR p_lpszLabel, 
								LPSTR p_lpszBitmap, 
								COLORREF p_lColor, 
								BOOL p_bEnabled );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						String: LPSTR
						String: LPSTR
						Number: LONG
						Boolean: BOOL
				Function: __XSalTabSetNumberOfTabs
					Description: BOOL XSalTabSetNumberOfTabs( 
								HWND p_hwnd, 
								INT p_iTabCount );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
				Function: __XSalTabSetActive
					Description: INT XSalTabSetActive( 
								HWND p_hwnd, 
								INT p_iTabIdx );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						Number: INT
				Function: __XSalTabSetBitmap
					Description: BOOL XSalTabSetBitmap( 
								HWND p_hwnd, 
								INT p_iTabIdx,
								LPSTR p_lpszBitmap );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						String: LPSTR
				Function: __XSalTabSetColor
					Description: BOOL XSalTabSetColor( 
								HWND p_hwnd, 
								INT p_iTabIdx,
								COLORREF p_lColor );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						Number: LONG
				Function: __XSalTabEnable
					Description: BOOL XSalTabEnable( 
								HWND p_hwnd, 
								INT p_iTabIdx,
								BOOL p_bEnabled );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						Boolean: BOOL
				Function: __XSalTabSetLabel
					Description: BOOL XSalTabSetLabel( 
								HWND p_hwnd, 
								INT p_iTabIdx, 
								LPSTR p_lpszLabel );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						String: LPSTR
				Function: __XSalTabDelete
					Description: BOOL XSalTabDelete( 
								HWND p_hwnd, 
								INT p_iTabIdx );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
				Function: __XSalTabInsert
					Description: BOOL XSalTabInsert( 
								HWND p_hwnd, 
								INT p_iTabIdx );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
				Function: __XSalTabSetMetrics
					Description: BOOL XSalTabSetMetrics( 
								HWND p_hwnd, 
								INT p_iType, 
								LONG p_lValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						Number: LONG
				Function: __XSalTabIndexFromPoint
					Description: INT XSalTabIndexFromPoint(
								HWND p_hwnd,
								LPPOINT p_lppoint );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						structPointer
							Number: INT
							Number: INT
				Function: __XSalTabSetFont
					Description: BOOL XSalTabSetFont( 
								HWND p_hwnd, 
								LPSTR p_lpszFontName, 
								INT p_iFontSize )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: INT
			! *** Centura Standard Tab Control
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! Low level Tab frame messages.  Processing these messages may change the default behavior of 
						the tab frame.
				! // 
						! // XSalTabControl Constants
						! //
				! *** Metrics
				Number: XTAB_PICTABSHANDLE	= 0
				Number: XTAB_ORIENTATION	= 1
				Number: XTAB_STYLE		= 2
				! *** Orientation
				Number: XTAB_TABSONTOP		= 0
				Number: XTAB_TABSONBOTTOM	= 1
				! *** Style
				Number: XTAB_STANDARDTABS	= 0
				Number: XTAB_SUQAREDTABS	= 1
			User
		Resources
			Bitmap: bmp1
				File Name: apply.bmp
		Variables
		Internal Functions
		Named Menus
		Class Definitions
			! *** XSalTabControl class
			Picture Class: cXSalTabs
				Window Location and Size
					Left:  
					Top:   
					Width:  0.0"
					Width Editable? No
					Height: 0.0"
					Height Editable? No
				Visible? No
				Editable? Class Default
				File Name:
				Storage: Class Default
				Picture Transparent Color: Class Default
				Fit: Class Default
				Scaling
					Width:  Class Default
					Height:  Class Default
				Corners: Class Default
				Border Style: No Border
				Border Thickness: Class Default
				Tile To Parent? Class Default
				Border Color: Class Default
				Background Color: Class Default
				List in Tool Palette? No
				Property Template:
				Class DLL Name:
				Description:
				Derived From
				Class Variables
				Instance Variables
					! *** Centura's tab control
					Window Handle: m_hWndPicTabs
				Functions
					! Private
					Function: __onCreate
						Description:
						Returns
						Parameters
						Static Variables
						Local variables
							Number: nIdx
							Number: nTabCount
							String: sLabel
							String: sBitmap
							Number: nColor
							Number: nPos
							String: sRet
							Number: nX
							Number: nY
							String: sFontName
							String: sFontSize
						Actions
							! Fine the Tab control
							Set m_hWndPicTabs = SalFindWindow( SalParentWindow( hWndItem ), "picTabs" )
							If NOT m_hWndPicTabs
								Return FALSE
							Set m_hWndPicTabs.cXSalQuickTabs.m_hWndXSalTabs = hWndItem
							Call m_hWndPicTabs.cXSalQuickTabs.OnSAMCreate()
							! Subclass the tab control
							If NOT __XSalTabSubclass( hWndItem )
								Return FALSE
							Call __XSalTabSetMetrics( hWndItem, XTAB_PICTABSHANDLE, SalWindowHandleToNumber( m_hWndPicTabs ) )
							! Get default text color
							Set nColor = SalColorGet( m_hWndPicTabs, COLOR_IndexWindowText )
							Call SalColorSet( hWndItem, COLOR_IndexWindow, SalColorGet( m_hWndPicTabs, COLOR_IndexWindow ))
							! Get tab count
							Set nTabCount = SWTabsGetCount( m_hWndPicTabs )
							Call __XSalTabSetNumberOfTabs( hWndItem, nTabCount )
							! Set labels
							While nIdx < nTabCount
								Call SalStrSetBufferLength( sLabel, 100 )
								Call SWTabsGetLabel( m_hWndPicTabs, nIdx, sLabel, 100 )
								! Try to extract the resource name
								Set nPos = SalStrScan( sLabel, "[%]" )
								If nPos > -1
									Set sBitmap = SalStrMidX( sLabel, nPos+1, SalStrLength( sLabel )-nPos-2 )
									Set sLabel = SalStrLeftX( sLabel, nPos )
								Else
									Set sBitmap = ''
								! Add the tab item
								Call __XSalTabSetTab( hWndItem, nIdx, sLabel, sBitmap, nColor, TRUE )
								Set nIdx = nIdx + 1
							! Set style
							Call SalWindowGetProperty( m_hWndPicTabs, "TabOrientation", sRet )
							Call __XSalTabSetMetrics( hWndItem, XTAB_ORIENTATION, 
										( sRet = "TabsOnBottom" ) * XTAB_TABSONBOTTOM )
							! Set font
							Call SalWindowGetProperty( m_hWndPicTabs, "TabUseFont", sRet )
							If sRet = "Parent"
								Call __XSalTabSetFont( hWndItem, "", 0 )
							Else
								Call SalWindowGetProperty( m_hWndPicTabs, "TabFontName", sFontName )
								Call SalWindowGetProperty( m_hWndPicTabs, "TabFontSize", sFontSize )
								Call __XSalTabSetFont( hWndItem, sFontName, SalStrToNumber( sFontSize ) )
							! Set active tab
							Call __XSalTabSetActive( hWndItem, SWTabsGetTop( m_hWndPicTabs ) )
							! Hide Centura'a tab control
							Call SalHideWindow( m_hWndPicTabs )
							! Show this control
							Call SalGetWindowLoc( m_hWndPicTabs, nX, nY )
							Call SalSetWindowLoc( hWndItem, nX, nY )
							Call SalGetWindowSize( m_hWndPicTabs, nX, nY )
							Call SalSetWindowSize( hWndItem, nX, nY )
							Call SalShowWindow( hWndItem )
				Message Actions
					On SAM_Create
						Call __onCreate()
					On __TABSM_TabActivate
						! Notify the object
						If SalSendMsg( m_hWndPicTabs, __TABSM_UserRequest, wParam, 0 )
							! If the user didn't cancel, activate the tab
							Call SWTabsBringToTop( m_hWndPicTabs, wParam, FALSE )
							Return TRUE
					On 0x0200  ! WM_MOUSEMOVE
						Return SalSendMsg( m_hWndPicTabs, 0x0200, wParam, lParam )
					On 0x0204  ! WM_RBUTTONDOWN
						Return SalSendMsg( m_hWndPicTabs, 0x0204, wParam, lParam )
			! *** XSalTabControl modified classes
			! New functions:
						cQuickTabs.SetColor( Index, Color )
						cQuickTabs.SetBitmap( Index, BitmapResourceName )
					
			Picture Class: cXSalQuickTabs
				Window Location and Size
					Left:  
					Top:   
					Width:  Class Default
					Width Editable? Class Default
					Height: Class Default
					Height Editable? Class Default
				Visible? Class Default
				Editable? Class Default
				File Name:
				Storage: Class Default
				Picture Transparent Color: Class Default
				Fit: Class Default
				Scaling
					Width:  Class Default
					Height:  Class Default
				Corners: Class Default
				Border Style: Class Default
				Border Thickness: Class Default
				Tile To Parent? Yes
				Border Color: Class Default
				Background Color: Class Default
				List in Tool Palette? No
				Property Template:
				Class DLL Name: qtabi20.dll
				Description: Tab frame window class.  Instances of this class
						should be created at children of cQuickTabsForm objects
						or cQuickTabsDialog objects.
				Derived From
					Class: cXSalSWTabs
					Class: cTabPageList
				Class Variables
				Instance Variables
					Number: nTab
					String: sName
					Boolean: m_bTabFrameInitialized
					Boolean: m_bPageListInitialized
				Functions
					Function: OnSAMCreate
						Description: Process SAM_Create message
						Returns
							Boolean:
						Parameters
						Static Variables
						Local variables
						Actions
							! Initialize page list
							Call AttachFormPagesParent( hWndForm )
							! Initialize the tab manager functional class
							Call AttachWindow( hWndItem )
							! Subclass this window
							Call Subclass()
							! Initialize tabs based on property values
							Call InitFromProps( )
							Set m_bTabFrameInitialized = TRUE
							! Initialize the form page list 
							Call __TabSetFormPageSizeThunk()
							Call __InitPageList()
							Set m_bPageListInitialized = TRUE
							! Pass our own create message for backwards compatibility
							Call SalSendMsg( hWndItem, TABSM_Create, wParam, lParam )
							! Call a late bound function so that derived class can know when the tab
									was created
							Call __TabCreateThunk( hWndItem )
							Call __ShowFormPage( GetTop() )
							Return TRUE
					Function: __InitPageList
						Description: Populate the table with tab names and labels
						Returns
							Boolean:
						Parameters
						Static Variables
						Local variables
							String: sFormArray[*]
							Number: nFormCount
							Number: nNameCount
							Number: nForm
							String: sNameArray[*]
							String: sName
							String: sForms
							String: sNames
						Actions
							! Use tab as delimiter
							Call SalWindowGetProperty(hWndItem, 'TabFormPages', sForms )
							Set nFormCount = SalStrTokenize( sForms, '', '	', sFormArray )
							If nFormCount
								Call SalWindowGetProperty(hWndItem, 'TabNames', sNames )
								Set nNameCount = SalStrTokenize( sNames, '', '	', sNameArray )
								Set nForm = 0
								While nForm < nFormCount AND nForm < nNameCount
									If sFormArray[nForm] != '' AND sNameArray[nForm] != ''
										Call AddPage( sNameArray[nForm], sFormArray[nForm], hWndNULL )
									Set nForm = nForm + 1
							Return TRUE
					Function: __ShowFormPage
						Description:
						Returns
							Boolean:
						Parameters
							Number: nTab
						Static Variables
						Local variables
							String: sName
						Actions
							If m_bPageListInitialized
								! Show page
								If nTab >= 0 
									Call GetName( nTab, sName )
								Call SetCurrentPage( sName )
							Return TRUE
					Function: __GetQuickTabsParent
						Description: Get the handle of the parent window.  Return hWndNULL if
								the parent is not of the correct class.
						Returns
							Window Handle:
						Parameters
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							Set hWndParent = SalParentWindow(hWndItem)
							If SalWindowIsDerivedFromClass( hWndParent, cQuickTabsParent )
								Return hWndParent
							Else
								Return hWndNULL
					Function: __TabActivateStartThunk
						Description: Make late bound call to function in parent
						Returns
							Boolean:
						Parameters
							Number: nTab
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							Set hWndParent = __GetQuickTabsParent()
							If hWndParent != hWndNULL
								Call hWndParent.cQuickTabsParent..TabActivateStart( hWndItem, nTab )
							Return TRUE
					Function: __TabActivateFinishThunk
						Description: Make late bound call to function in parent
						Returns
							Boolean:
						Parameters
							Number: nTab
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							Set hWndParent = __GetQuickTabsParent()
							If hWndParent != hWndNULL
								Call hWndParent.cQuickTabsParent..TabActivateFinish( hWndItem, nTab )
							Return TRUE
					Function: __TabCreateThunk
						Description: Make late bound call to function in parent
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							Set hWndParent = __GetQuickTabsParent()
							If hWndParent != hWndNULL
								Call hWndParent.cQuickTabsParent..TabCreate( hWnd )
							Return TRUE
					Function: __TabUserRequestThunk
						Description: Make late bound call to function in parent
						Returns
							Boolean:
						Parameters
							Number: nTab
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							Set hWndParent = __GetQuickTabsParent()
							If hWndParent != hWndNULL
								Call hWndParent.cQuickTabsParent..TabUserRequest( hWndItem, nTab )
							Return TRUE
					Function: __TabFrameResizeThunk
						Description: Make late bound call to function in parent
						Returns
							Boolean:
						Parameters
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							Set hWndParent = __GetQuickTabsParent()
							If hWndParent != hWndNULL
								Call hWndParent.cQuickTabsParent..TabFrameResize( hWndItem )
							Return TRUE
					Function: __TabSetFormPageSizeThunk
						Description: Make late bound call to function in parent
						Returns
							Boolean:
						Parameters
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							Set hWndParent = __GetQuickTabsParent()
							If hWndParent != hWndNULL
								Call hWndParent.cQuickTabsParent..TabSetFormPageSize( hWndItem )
							Return TRUE
					! 
							*** Added by XSal ***
					! Public
					Function: SetBitmap
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_nIndex
							String: p_sBitmap
						Static Variables
						Local variables
						Actions
							Return __XSalTabSetBitmap( m_hWndXSalTabs, p_nIndex, p_sBitmap )
					Function: SetColor
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_nIndex
							Number: p_nColor
						Static Variables
						Local variables
						Actions
							Return __XSalTabSetColor( m_hWndXSalTabs, p_nIndex, p_nColor )
				Message Actions
					! 
							*** Removed by XSal ***
					! On SAM_Create
						                   Call OnSAMCreate(  )
					On __TABSM_TabActivate
						! A tab has been activated
						! Pass our own messages that included the selected tab index
						Set nTab = cXSalQuickTabs.GetClicked(  )
						! Generate message for backward compatibility
						Call SalSendMsg( hWndItem, TABSM_TabActivateStart, nTab, 0 )
						! Call late bound function in parent window
						Call __TabActivateStartThunk( nTab )
						! Hide/Show child windows when a tab is selected
						Call ShowSiblings( )
						! Show page
						Call __ShowFormPage( nTab )
						! Generate message for backward compatibility
						Call SalSendMsg( hWndItem, TABSM_TabActivateFinish, nTab, 0 )
						! Call late bound function in parent window
						Call __TabActivateFinishThunk( nTab )
					On __TABSM_UserRequest
						! The user has requested a tab activatation.
						! Pass our own messages that included the selected tab index
						! 
								*** Changed by XSal
						! Set nTab = cQuickTabs.GetClicked(  )
						Set nTab = wParam
						Set m_bCancelMode = FALSE
								
						! Generate message for backward compatibility
						Call SalSendMsg( hWndItem, TABSM_UserRequest, nTab, 0 )
						! Call late bound function in parent window
						Call __TabUserRequestThunk( nTab )
						! 
								*** Added by XSal
						Return NOT m_bCancelMode
					On 0x0005  ! WM_Size
						If m_bTabFrameInitialized
							Call __TabSetFormPageSizeThunk()
							Call __TabFrameResizeThunk( )
							! 
									*** Added by XSal ***
							Call SalSendMsg( m_hWndXSalTabs, 0x0005, wParam, lParam )
			Functional Class: cXSalSWTabs
				Description: Class used to manage a tab frame at run time
				Derived From
				Class Variables
				Instance Variables
					Window Handle: m_hWnd
					! 
							*** Added by XSal ***
					Boolean: m_bCancelMode
					Window Handle: m_hWndXSalTabs
				Functions
					Function: AttachWindow
						Description: Indicate the window that this class operates
								on.
								The AttachWindow function must be called first
								to associate a tab frame with this object.
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
						Actions
							Set m_hWnd = hWnd
							Return TRUE
					Function: Subclass
						Description: Subclass this tab frame so that swtabs.dll can
								take over painting, mouse handling, etc.
						Returns
							Boolean:
						Parameters
						Static Variables
						Local variables
						Actions
							Call SWTabsSubclass( m_hWnd )
							Return TRUE
					Function: Add
						Description: Append a tab. Redraw is optional
								Return an index
						Returns
							Number:
						Parameters
							Boolean: bRedraw
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsAdd( m_hWnd, bRedraw )
							! 
									*** Added by XSal ***
							Call __XSalTabSetNumberOfTabs( m_hWndXSalTabs, GetCount() )
									
							Return nReturn
					Function: Insert
						Description: Insert a tab at a location.  Redraw is optional.
								Return TRUE or FALSE.
						Returns
							Boolean: BOOL
						Parameters
							Number: nIndex
							Boolean: bRedraw
						Static Variables
						Local variables
						Actions
							! 
									*** Added by XSal ***
							Call __XSalTabInsert( m_hWndXSalTabs, nIndex )
									
							Return SWTabsInsert( m_hWnd, nIndex, bRedraw )
					Function: Delete
						Description: Delete a tab at a location.  Redraw is optional.
								Return TRUE or FALSE.
						Returns
							Boolean: BOOL
						Parameters
							Number: nIndex
							Boolean: bRedraw
						Static Variables
						Local variables
						Actions
							! 
									*** Added by XSal ***
							Call __XSalTabDelete( m_hWndXSalTabs, nIndex )
									
							Return SWTabsDelete( m_hWnd, nIndex, bRedraw )
					Function: SetName
						Description: Associate a name with a tab
						Returns
							Boolean:
						Parameters
							Number: nIndex
							String: sName
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Set bReturn = SWTabsSetName( m_hWnd, nIndex, sName )
							Return bReturn
					Function: SetLabel
						Description: Set the label displayed on a tab.
								Redraw is optional.
						Returns
							Boolean:
						Parameters
							Number: nIndex
							String: sName
							Boolean: bRedraw
						Static Variables
						Local variables
							Boolean: bReturn
							String: sBitmap
							Number: nPos
						Actions
							! 
									*** Added by XSal ***
							! Try to extract the resource name
							Set nPos = SalStrScan( sName, "[%]" )
							If nPos > -1
								Set sBitmap = SalStrMidX( sName, nPos+1, SalStrLength( sName )-nPos-2 )
								Set sName = SalStrLeftX( sName, nPos )
							Else
								Set sBitmap = ''
							Call __XSalTabSetBitmap( m_hWndXSalTabs, nIndex, sBitmap )
							Call __XSalTabSetLabel( m_hWndXSalTabs, nIndex, sName )
									
							Set bReturn = SWTabsSetLabel( m_hWnd, nIndex, sName, bRedraw )
							Return bReturn
					Function: GetName
						Description: Get the name associated with a tab
						Returns
							Boolean:
						Parameters
							Number: nIndex
							Receive String: sName
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Call SalStrSetBufferLength( sName, 100 )
							Set bReturn = SWTabsGetName( m_hWnd, nIndex, sName, 100 )
							Return bReturn
					Function: GetLabel
						Description: Get the label displayed on a tab
						Returns
							Boolean:
						Parameters
							Number: nIndex
							Receive String: sLabel
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Call SalStrSetBufferLength( sLabel, 100 )
							Set bReturn = SWTabsGetLabel( m_hWnd, nIndex, sLabel, 100 )
							Return bReturn
					Function: Clear
						Description: Clear all tabs from the frame. 
								Redraw is optional.
						Returns
							Boolean:
						Parameters
							Boolean: bRedraw
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							! 
									*** Added by XSal ***
							Call __XSalTabSetNumberOfTabs( m_hWndXSalTabs, 0 )
									
							Set bReturn = SWTabsClear( m_hWnd, bRedraw )
					Function: BringToTop
						Description: Activate a tab
						Returns
							Boolean:
						Parameters
							Number: nIndex
							Boolean: bRedraw
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							! 
									*** Added by XSal ***
							Call __XSalTabSetActive( m_hWndXSalTabs, nIndex )
									
							Set bReturn = SWTabsBringToTop( m_hWnd, nIndex, bRedraw )
					Function: GetTop
						Description: Get the index of the active tab
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsGetTop( m_hWnd )
							Return nReturn
					Function: GetClicked
						Description: Get the index of the tab being clicked.
								This should only be called during a 
								click notification.
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsGetUserEventIndex( m_hWnd )
							Return nReturn
					Function: GetCount
						Description: Get the count of tabs
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsGetCount( m_hWnd )
							Return nReturn
					Function: Next
						Description: Activate the next tab
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsNext( m_hWnd )
							! 
									*** Added by XSal ***
							Call __XSalTabSetActive( m_hWndXSalTabs, nReturn )
									
							Return nReturn
					Function: Prev
						Description: Activate the previous tab
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsPrev( m_hWnd )
							! 
									*** Added by XSal ***
							Call __XSalTabSetActive( m_hWndXSalTabs, nReturn )
									
							Return nReturn
					Function: FindName
						Description: Find a tab give a tab name
						Returns
							Number:
						Parameters
							String: sName
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsFindName( m_hWnd, sName )
							Return nReturn
					Function: FindLabel
						Description: Find a tab give a tab label
						Returns
							Number:
						Parameters
							String: sLabel
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsFindLabel( m_hWnd, sLabel )
							Return nReturn
					Function: CancelMode
						Description: Cancel a user action.  This can
								be called when processing a TABSM_ message.
						Returns
							Boolean: BOOL
						Parameters
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Set bReturn = SWTabsCancelMode( m_hWnd )
							! 
									*** Added by XSal ***
							Set m_bCancelMode = TRUE
									
							Return bReturn
					Function: Redraw
						Description: Arranges tabs on the frame and repaints.  
								Call after adding tabs or clearing with a FALSE 
								redraw flag.
						Returns
							Boolean: BOOL
						Parameters
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							! 
									*** Added by XSal ***
							Call SalInvalidateWindow( m_hWndXSalTabs )
									
							Set bReturn = SWTabsRedraw( m_hWnd )
							Return bReturn
					Function: InitFromProps
						Description: Read properties from the outline and reconfigure
								the tab frame.
						Returns
							Boolean: BOOL
						Parameters
						Static Variables
						Local variables
						Actions
							Return SWTabsInitWindowFromProps( m_hWnd )
					Function: ShowSiblings
						Description: Show or hide child windows based on the
								top tab
						Returns
							Boolean: BOOL
						Parameters
						Static Variables
						Local variables
							Number: nTop
							String: sName
						Actions
							Set nTop = GetTop()
							If nTop >= 0
								Call GetName( nTop, sName )
								! Set focus to the tab frame
								Call SalSetFocus( m_hWnd )
								Return SWTabsShowSiblings( m_hWnd, sName )
							Return FALSE
					Function: Enable
						Description: Enable or disable a tab
						Returns
							Boolean: BOOL
						Parameters
							Number: nIndex
							Boolean: bEnable
						Static Variables
						Local variables
						Actions
							! 
									*** Added by XSal ***
							Call __XSalTabEnable( m_hWndXSalTabs, nIndex, bEnable )
									
							Return SWTabsEnable( m_hWnd, nIndex, bEnable )
					Function: IsEnabled
						Description: Indicate whether a tab is enabled
						Returns
							Boolean: BOOL
						Parameters
							Number: nIndex
						Static Variables
						Local variables
						Actions
							Return SWTabsIsEnabled( m_hWnd, nIndex )
					Function: GetContentsRect
						Description: Get the area available for child controls.  Coordinates
								are relative to the parent window.
						Returns
							Boolean:
						Parameters
							Receive Number: nLeft
							Receive Number: nTop
							Receive Number: nRight
							Receive Number: nBottom
						Static Variables
						Local variables
							Number: nLeftPixels
							Number: nRightPixels
							Number: nBottomPixels
							Number: nTopPixels
							Window Handle: hWndParent
						Actions
							Call GetContentsRectPixels(  
									nLeftPixels, nTopPixels, nRightPixels, nBottomPixels )
							Set nLeft =SalPixelsToFormUnits( m_hWnd, nLeftPixels, FALSE )
							Set nTop =SalPixelsToFormUnits( m_hWnd, nTopPixels, TRUE )
							Set nRight =SalPixelsToFormUnits( m_hWnd, nRightPixels, FALSE )
							Set nBottom =SalPixelsToFormUnits( m_hWnd, nBottomPixels, TRUE )
							Return TRUE
					Function: GetContentsRectPixels
						Description: Get the area available for child controls.  Coordinates
								are relative to the parent window.
						Returns
							Boolean:
						Parameters
							Receive Number: nLeftPixels
							Receive Number: nTopPixels
							Receive Number: nRightPixels
							Receive Number: nBottomPixels
						Static Variables
						Local variables
						Actions
							Call SWTabsGetContentsRect( m_hWnd, 
									nLeftPixels, nTopPixels, nRightPixels, nBottomPixels )
							Return TRUE
					Function: GetContentsBorderRect
						Description: Get the area between the contents rect and the edge of the tab frame. For 
								example nLeft will indicate the width of the left border, nTop will indicate the 
								combined height of the top border and tab area.
						Returns
							Boolean:
						Parameters
							Receive Number: nLeft
							Receive Number: nTop
							Receive Number: nRight
							Receive Number: nBottom
						Static Variables
						Local variables
							Number: nLeftPixels
							Number: nRightPixels
							Number: nBottomPixels
							Number: nTopPixels
							Window Handle: hWndParent
						Actions
							Call SWTabsGetContentsBorderRect( m_hWnd, 
									nLeftPixels, nTopPixels, nRightPixels, nBottomPixels )
							Set nLeft =SalPixelsToFormUnits( m_hWnd, nLeftPixels, FALSE )
							Set nTop =SalPixelsToFormUnits( m_hWnd, nTopPixels, TRUE )
							Set nRight =SalPixelsToFormUnits( m_hWnd, nRightPixels, FALSE )
							Set nBottom =SalPixelsToFormUnits( m_hWnd, nBottomPixels, TRUE )
							Return TRUE
					Function: GetRowCount
						Description: Retrieve the number rows of tabs
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsGetPageCount( m_hWnd )
							Return nReturn
					Function: SetRowCount
						Description: Set the number rows of tabs
						Returns
							Number:
						Parameters
							Number: nRows
							Boolean: bRedraw
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Set bReturn = SWTabsSetPageCount( m_hWnd, nRows, bRedraw )
							Return bReturn
					Function: GetDrawStyle
						Description: Retrieve the draw style
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
							Number: nReturn
						Actions
							Set nReturn = SWTabsGetDrawStyle( m_hWnd )
							Return nReturn
					Function: SetDrawStyle
						Description: Change the draw style
						Returns
							Number:
						Parameters
							Number: nStyle
							Boolean: bRedraw
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							! 
									*** Added by XSal ***
							If nStyle = TABS_DrawWin95
								Set bReturn = __XSalTabSetMetrics( m_hWndXSalTabs, XTAB_STYLE, XTAB_SUQAREDTABS )
							Else
								Set bReturn = __XSalTabSetMetrics( m_hWndXSalTabs, XTAB_STYLE, XTAB_STANDARDTABS )
							!
							Set bReturn = SWTabsSetDrawStyle( m_hWnd, nStyle, bRedraw )
							Return bReturn
					Function: ShowWindow
						Description: Use this function instead of SalShowWindow because it works properly
								with the class function HideWindow
						Returns
							Boolean: BOOL
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Set bReturn = SWTabsShowWindow( m_hWnd, hWnd )
							Return bReturn
					Function: ShowWindowAndLabel
						Description: Use this function instead of SalShowWindowAndLabel because it works properly
								with the class function HideWindowAndLabel.
						Returns
							Boolean: BOOL
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Set bReturn = SWTabsShowWindowAndLabel( m_hWnd, hWnd )
							Return bReturn
					Function: HideWindow
						Description: Use this function instead of SalHideWindow.  This function will keep the window
								hidden as tabs are selected.  Use the class function ShowWindow to undo the affect
								of this function.
						Returns
							Boolean: BOOL
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Set bReturn = SWTabsHideWindow( m_hWnd, hWnd )
							Return bReturn
					Function: HideWindowAndLabel
						Description: Use this function instead of SalHideWindowAndLabel.  This function will keep the window
								hidden as tabs are selected.  Use the class function ShowWindowWindowAndLabel to 
								undo the affect of this function.
						Returns
							Boolean: BOOL
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
							Boolean: bReturn
						Actions
							Set bReturn = SWTabsHideWindowAndLabel( m_hWnd, hWnd )
							Return bReturn
					Function: GetMinimumWidth
						Description: The the minimum tab frame width.  If the tab frame is sized below this
								width, tab text will be clipped.  The returned width is in form units.
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
						Actions
							Return SalPixelsToFormUnits( m_hWnd, SWTabsGetMinimumWidth( m_hWnd ), FALSE )
					Function: SetMinimumResizeSize
						Description: Define the minimum size of the tiled-to-parent tabbed frame when it is resized.
								Measurements are in form units.
						Returns
							Boolean:
						Parameters
							Number: nWidth
							Number: nHeight
							Boolean: bResize
						Static Variables
						Local variables
						Actions
							Return SWTabsSetMinimumResizeSize( m_hWnd, nWidth, nHeight, bResize )
					Function: GetMarginRect
						Description: Get the margins defined for a tiled-to-parent tabbed frame
						Returns
							Number:
						Parameters
							Receive Number: nLeft
							Receive Number: nTop
							Receive Number: nRight
							Receive Number: nBottom
						Static Variables
						Local variables
						Actions
							Return SWTabsGetMarginRect( m_hWnd, nLeft, nTop, nRight, nBottom )
					Function: SetWorkspaceBoundary
						Description: Define the boundaries of the workspace so that
								a tiled-to-parent tab frame won't be resized smaller than this boundary
						Returns
							Boolean:
						Parameters
							Number: nRightBoundary
							Number: nBottomBoundary
						Static Variables
						Local variables
							Number: nMinWidth
							Number: nMinHeight
							Number: nRequiredWidth
							Number: nLeftBorder
							Number: nRightBorder
							Number: nTopBorder
							Number: nBottomBorder
							Number: nLeftMargin
							Number: nRightMargin
							Number: nTopMargin
							Number: nBottomMargin
						Actions
							! The space between the contents rect and the edge of the tab frame (in pixels)
							Call GetContentsBorderRect( nLeftBorder, nTopBorder, nRightBorder, nBottomBorder )
							! The the margins defined for this tile-to-parent frame
							Call GetMarginRect( nLeftMargin, nTopMargin, nRightMargin, nBottomMargin )
							! Calculate the minimum width and height of the tabframe.  We need to account
									for the following:
									1) The space between the tab frame and the left and top of the form
									2) The space, on right and bottom, between the tab frame contents area and the 
									edge of the tab frame.  This space is used to display the frame border.
							Set nMinWidth = nRightBoundary - nLeftMargin + nRightBorder 
							Set nMinHeight = nBottomBoundary - nTopMargin + nBottomBorder 
							! Make sure this minimum is wide enough to display all of the 
									tabs without clipping them
							Set nRequiredWidth = GetMinimumWidth()
							If nMinWidth < nRequiredWidth
								Set nMinWidth =  nRequiredWidth
							! Indicate the minimum width and height of the tab frame.  TRUE
									means to resize the frame.
							Call SetMinimumResizeSize( nMinWidth, nMinHeight,  TRUE )
							Return TRUE
					Function: FindTabFrame
						Description: Finds the tab frame window on a form or dialog box
						Returns
							Window Handle:
						Parameters
							Window Handle: hWndForm
						Static Variables
						Local variables
							Window Handle: hWnd
						Actions
							Set hWnd = SalGetFirstChild( hWndForm, TYPE_Picture )
							While hWnd != hWndNULL
								If IsTabFrame( hWnd )
									Return hWnd
								Set hWnd = SalGetNextChild( hWnd, TYPE_Picture )
							Return hWndNULL
					Function: IsTabFrame
						Description: Indicate whether a window is a tab frame
						Returns
							Boolean: BOOL
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
						Actions
							Return SWTabsIsTabControl( hWnd )
					Function: IndexFromPoint
						Description: Get the tab, if any, under a given point (in pixels).  A negative number is returned if the point
								is not over a tab.
						Returns
							Number:
						Parameters
							Number: nX
							Number: nY
						Static Variables
						Local variables
						Actions
							! 
									*** Added by XSal ***
							Return __XSalTabIndexFromPoint( m_hWndXSalTabs, nX, nY )
									
							! Return SWTabIndexFromChildPoint( m_hWnd, nX, nY )
					Function: AreFormPagesSupported
						Description: Indicate whether child forms are supported by this version of Centura
						Returns
							Boolean: BOOL
						Parameters
						Static Variables
						Local variables
						Actions
							Return TRUE
					Function: SendMsgToAssociated
						Description: Send a message to all child windows associated with a tab
						Returns
							Boolean:
						Parameters
							Number: nIndex
							Number: nMsg
							Number: wParam
							Number: lParam
						Static Variables
						Local variables
						Actions
							Return SWTabsSendMsgToAssociated( m_hWnd, nIndex, nMsg, wParam, lParam )
					Function: IsAssociatedWithTab
						Description: Indicate wether a child window is associated with a tab
						Returns
							Boolean:
						Parameters
							Number: nIndex
							Window Handle: hWnd
						Static Variables
						Local variables
						Actions
							Return SWTabsIsAssociatedWithTab( m_hWnd, nIndex, hWnd )
			! *** Centura Standard Tab control classes
			General Window Class: cXSalQuickTabsParent
				Description: Defines late bound functions in QuickTabs parent Form or Dialog Box that
						will be called when an event occurs in the tab control.
				Derived From
				Class Variables
				Instance Variables
				Functions
					Function: TabCreate
						Description: Indicates that a tab has been created. 
								This function is called when the tab control receives SAM_Create.
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
						Actions
							! Don't add code here.  This is a late bound function call.  Override in the 
									form or dialog box.
							Return TRUE
					Function: TabActivateStart
						Description: Indicates that a tab has been activated.
								This function is called before the child windows have been shown
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
							Number: nTab
						Static Variables
						Local variables
						Actions
							! Don't add code here.  This is a late bound function call.  Override in the 
									form or dialog box.
							Return TRUE
					Function: TabActivateFinish
						Description: Indicates that a tab has been activated.
								This function is called after the child windows have been shown
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
							Number: nTab
						Static Variables
						Local variables
							Window Handle: hWndParent
						Actions
							! Don't add code here.  This is a late bound function call.  Override in the 
									form or dialog box.
							Return TRUE
					Function: TabUserRequest
						Description: Indicates that user is attempting to change the current tab by clicking,
								 tabbing or some other user action.
								NOTE: Call CancelMode() to deny the user's request
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
							Number: nTab
						Static Variables
						Local variables
						Actions
							! Don't add code here.  This is a late bound function call.  Override in the 
									form or dialog box.
							Return TRUE
					Function: TabFrameResize
						Description: Indicates that the size of the tab frame may have changed
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
						Actions
							! Don't add code here.  This is a late bound function call.  Override in the 
									form or dialog box.
							Return TRUE
					Function: TabSetFormPageSize
						Description: Define the page size for child forms created on the tab frame.
								This function can be overriden in the tab form or tab dialog box
						Returns
							Boolean:
						Parameters
							Window Handle: hWnd
						Static Variables
						Local variables
							Number: nLeft
							Number: nTop
							Number: nRight
							Number: nBottom
						Actions
							Call hWnd.cXSalQuickTabs.GetContentsRect( nLeft, nTop, nRight, nBottom )
							Call hWnd.cXSalQuickTabs.SetPageSize( nLeft, nTop,  nRight, nBottom, TRUE)
							Return TRUE
				Message Actions
			Form Window Class: cXSalQuickTabsForm
				Title:
				Icon File:
				Accesories Enabled? Class Default
				Visible? Class Default
				Display Settings
					Display Style? Class Default
					Visible at Design time? Yes
					Automatically Created at Runtime? Class Default
					Initial State: Class Default
					Maximizable? Class Default
					Minimizable? Class Default
					System Menu? Class Default
					Resizable? Class Default
					Window Location and Size
						Left:  
						Top:   
						Width:  5.12"
						Width Editable? Class Default
						Height: 2.5"
						Height Editable? Class Default
					Form Size
						Width:  0.1"
						Height: 0.1"
						Number of Pages: Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
				Next Class Child Key: 5
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name: qtchi20.dll
				Description: Form window with a tab frame.  In addition to 
						containing a built in tab frame.  This class also
						has the smarts to manage other child windows 
						on the form so that they can be associated with
						specific tabs.
				Derived From
					Class: cXSalQuickTabsParent
				Menu
				Tool Bar
					Display Settings
						Display Style? Class Default
						Location? Class Default
						Visible? Class Default
						Size: Class Default
						Size Editable? Class Default
						Font Name: Class Default
						Font Size: Class Default
						Font Enhancement: Class Default
						Text Color: Class Default
						Background Color: Class Default
					Contents
				Contents
					Picture: picTabs
.data INHERITPROPS
0000: 0100
.enddata
.data CLASSPROPSSIZE
0000: DA00
.enddata
.data CLASSPROPS
0000: 5461624C6566744D 617267696E000200 3000005461624375 7272656E74000600
0020: 4E616D6530000054 6162426F74746F6D 4D617267696E0002 0030000054616250
0040: 616765436F756E74 0002003100005461 624C6162656C7300 07004C6162656C30
0060: 00005461624E616D 65730006004E616D 6530000054616252 696768744D617267
0080: 696E000200300000 5461624472617753 74796C65000B0057 696E39355374796C
00A0: 650000546162466F 726D506167657300 0100000054616254 6F704D617267696E
00C0: 0002003000000000 0000000000000000 0000000000000000 0000
.enddata
						Class Child Ref Key: 0
						Class ChildKey: 1
						Class: cXSalQuickTabs
						Property Template:
						Class DLL Name:
						Window Location and Size
							Left:   0.38"
							Top:    0.3"
							Width:  4.3"
							Width Editable? Class Default
							Height: 1.692"
							Height Editable? Class Default
						Visible? Class Default
						Editable? Yes
						File Name:
						Storage: Class Default
						Picture Transparent Color: Class Default
						Fit: Class Default
						Scaling
							Width:  Class Default
							Height:  Class Default
						Corners: Class Default
						Border Style: Class Default
						Border Thickness: Class Default
						Tile To Parent? Class Default
						Border Color: Default
						Background Color: 3D Face Color
						Message Actions
					Picture: XSalTabs
						Class Child Ref Key: 0
						Class ChildKey: 5
						Class: cXSalTabs
						Property Template:
						Class DLL Name:
						Window Location and Size
							Left:   0.0"
							Top:    0.0"
							Width:  Class Default
							Width Editable? Class Default
							Height: Class Default
							Height Editable? Class Default
						Visible? Class Default
						Editable? Class Default
						File Name:
						Storage: Class Default
						Picture Transparent Color: Class Default
						Fit: Class Default
						Scaling
							Width:  Class Default
							Height:  Class Default
						Corners: Class Default
						Border Style: Class Default
						Border Thickness: Class Default
						Tile To Parent? Class Default
						Border Color: Class Default
						Background Color: Class Default
						Message Actions
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Dialog Box Class: cXSalQuickTabsDialog
				Title:
				Accesories Enabled? Class Default
				Visible? Class Default
				Display Settings
					Display Style? Class Default
					Visible at Design time? Yes
					Type of Dialog: Modal
					Window Location and Size
						Left:  
						Top:   
						Width:  3.92"
						Width Editable? Class Default
						Height: 2.533"
						Height Editable? Class Default
					Absolute Screen Location? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
				Next Class Child Key: 4
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name: qtchi20.dll
				Description: Dialog Box window parent of a tab frame.  This 
						class allows editing of the properties of child windows
						identifying their associated tab.
				Tool Bar
					Display Settings
						Display Style? Class Default
						Location? Class Default
						Visible? Class Default
						Size: Class Default
						Size Editable? Class Default
						Font Name: Class Default
						Font Size: Class Default
						Font Enhancement: Class Default
						Text Color: Class Default
						Background Color: Class Default
					Contents
				Contents
					Picture: picTabs
.data INHERITPROPS
0000: 0100
.enddata
.data CLASSPROPSSIZE
0000: DD00
.enddata
.data CLASSPROPS
0000: 5461624C6566744D 617267696E000200 3000005461624375 7272656E74000600
0020: 4E616D6530000054 616252696768744D 617267696E000200 3000005461624E61
0040: 6D65730006004E61 6D65300000546162 4C6162656C730007 004C6162656C3000
0060: 0054616250616765 436F756E74000200 310000546162426F 74746F6D4D617267
0080: 696E000500302E34 3500005461624472 61775374796C6500 0B0057696E393553
00A0: 74796C6500005461 62466F726D506167 6573000100000054 6162546F704D6172
00C0: 67696E0002003000 0000000000000000 0000000000000000 0000000000
.enddata
						Class Child Ref Key: 0
						Class ChildKey: 1
						Class: cXSalQuickTabs
						Property Template:
						Class DLL Name:
						Window Location and Size
							Left:   0.08"
							Top:    0.1"
							Width:  3.64"
							Width Editable? Class Default
							Height: 1.542"
							Height Editable? Class Default
						Visible? Class Default
						Editable? Yes
						File Name:
						Storage: Class Default
						Picture Transparent Color: Class Default
						Fit: Class Default
						Scaling
							Width:  Class Default
							Height:  Class Default
						Corners: Class Default
						Border Style: Class Default
						Border Thickness: Class Default
						Tile To Parent? Yes
						Border Color: Default
						Background Color: 3D Face Color
						Message Actions
					Picture: XSalTabs
						Class Child Ref Key: 0
						Class ChildKey: 4
						Class: cXSalTabs
						Property Template:
						Class DLL Name:
						Window Location and Size
							Left:   0.2"
							Top:    0.167"
							Width:  Class Default
							Width Editable? Class Default
							Height: Class Default
							Height Editable? Class Default
						Visible? Class Default
						Editable? Class Default
						File Name:
						Storage: Class Default
						Picture Transparent Color: Class Default
						Fit: Class Default
						Scaling
							Width:  Class Default
							Height:  Class Default
						Corners: Class Default
						Border Style: Class Default
						Border Thickness: Class Default
						Tile To Parent? Class Default
						Border Color: Class Default
						Background Color: Class Default
						Message Actions
				Derived From
					Class: cXSalQuickTabsParent
				Class Variables
				Instance Variables
				Functions
				Message Actions
			! Functional Class: cTabPageList
.winattr
.end
				   Description: Keep track of list of tab pages
				   Derived From 
					   Class: cFormPageList
				   Class Variables 
				   Instance Variables 
				   Functions 
					   Function: CreatePageWindowAsChild
						   Description: Override base class function.
								Create a form as a hidden child window. 
						   Returns 
							   Window Handle: 
						   Parameters 
							   String: sForm
							   Window Handle: hWndParent
							   Number: nLeft  ! Form units
							   Number: nTop
							   Number: nRight
							   Number: nBottom
						   Static Variables 
						   Local variables 
						   Actions 
							   Return SalCreateWindowEx( sForm, hWndParent,
									nLeft, nTop, nRight - nLeft, nBottom - nTop, 
									CREATE_AsChild | CREATE_Hidden )
			! Functional Class: cFormPageList
.winattr
.end
				   Description: Keep track of a list of form windows that can be displayed
						as pages on a form.  This is an abstract class.  The CreateFormAsChild class function must
						be defined in a dervied class. 
				   Derived From 
				   Class Variables 
				   Instance Variables 
					   Window Handle: m_hWndParent
					   String: m_sCurrent
					   Number: m_nPageLeft
					   Number: m_nPageTop
					   Number: m_nPageBottom
					   Number: m_nPageRight
					   : Items[*]
					   String: m_sFormPagesData
				   Functions 
					   Function: AttachFormPagesParent
						   Description: Initialization function. 
								Associate a parent window with this class.  
								Pages will be created as children of this form, toolbar, or dialog box window.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
							   Window Handle: hWndParent
								! The parent Window Handle
						   Static Variables 
						   Local variables 
						   Actions 
							   Set m_hWndParent = hWndParent
							   Return TRUE
					   Function: InitializeFormPages
						   Description: Pass initialization data.
								Call this function to define a string that will be passed to each page using the PAGEM_Initialize message.
								Typically this strings passes information that is used by the page windows to initialize themselves.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
							   String: sData
								! The initialization string
						   Static Variables 
						   Local variables 
							   Number: nIndex
							   Number: nMax
						   Actions 
							   Set m_sFormPagesData = sData
							   Call SalArrayGetUpperBound( Items, 1, nMax )
							   Set nIndex = 0
							   While nIndex <= nMax
								   Call __SendInitializeMessage( nIndex )
									   Return nIndex
								   Set nIndex = nIndex + 1
							   Return TRUE
					   Function: ApplyFormPageChanges
						   Description: Apply changes to all pages.
								Call this function to cause the PAGEM_ApplyChanges message to be sent to all form pages.
								Typically this function is called when the "OK" or "Apply Changes" button is selected.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
						   Static Variables 
						   Local variables 
							   Number: nIndex
							   Number: nMax
						   Actions 
							   Call SalArrayGetUpperBound( Items, 1, nMax )
							   Set nIndex = 0
							   While nIndex <= nMax
								   Call __SendApplyMessage( nIndex )
									   Return nIndex
								   Set nIndex = nIndex + 1
							   Return TRUE
					   Function: __CheckPageHandles
						   Description: Validate the initialization of this class
						   Returns 
							   Boolean: 
						   Parameters 
						   Static Variables 
						   Local variables 
						   Actions 
							   If m_hWndParent = hWndNULL
								   Call SalMessageBox( 'Parent window has not been defined by call to AttachParent', 'cPageList', MB_Ok )
								   Return FALSE
							   Return TRUE
					   Function: SetPageSize
						   Description: Define the location of form pages.
								Call this function to define a rectangle within the parent to display pages.
								Pages will not be displayed properly unless this function is called before any pages are shown.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
							   Number: nLeft
							   Number: nTop
							   Number: nRight
							   Number: nBottom
							   Boolean: bResize
						   Static Variables 
						   Local variables 
						   Actions 
							   Set m_nPageLeft = nLeft
							   Set m_nPageTop = nTop
							   Set m_nPageRight = nRight
							   Set m_nPageBottom = nBottom
							   If bResize
								   Call ResizePages()
							   Return TRUE
					   Function: ResizePages
						   Description: Resize all pages.  
								Call this function to resize all form pages.  
								The active page is resized first.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
						   Static Variables 
						   Local variables 
							   Number: nMax
							   Number: nIndexCurrent
							   Number: nIndex
							   Number: nCurrent
						   Actions 
							   If __CheckPageHandles( ) = FALSE
								   Return FALSE
							   Set nCurrent = __FindPageOfTag( m_sCurrent )
							   If nCurrent != -1
								   Call Items[nCurrent].Resize( m_hWndParent, m_nPageLeft, m_nPageTop, m_nPageRight, m_nPageBottom )
							   Call SalArrayGetUpperBound( Items, 1, nMax )
							! Search for an unused item
							   Set nIndex = 0
							   While nIndex <= nMax
								   If nIndex != nCurrent
									   Call Items[nIndex].Resize( m_hWndParent, m_nPageLeft, m_nPageTop, m_nPageRight, m_nPageBottom )
										   Return nIndex
								   Set nIndex = nIndex + 1
							   Return TRUE
					   Function: AddPage
						   Description: Add a page.
								Call this function to add a page to the page list.  
								The page window will not be created until the page is activated.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
							   String: sTag
								! A unique identifier for the form page
							   String: sForm
								! The name of the form page
							   Window Handle: hWnd  
								! The Window Handle of the page.  Typically this is hWndNULL. 
						   Static Variables 
						   Local variables 
							   Number: nIndex
						   Actions 
							   Set nIndex = __NewPageItem()
							   Set Items[nIndex].m_sForm = sForm
							   Set Items[nIndex].m_sTag = sTag
							   Set Items[nIndex].m_hWndPage = hWnd
							   Return TRUE
					   Function: CreatePage
						   Description: Create a page window. 
								Call this function to cause the page window to be created.
								Typically it is not necessary to call this function because SetCurrentPage will take care of creating the page window.
								The page window will not be shown until SetCurrentPage is called.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
							   String: sTag
						   Static Variables 
						   Local variables 
							   Number: nIndex
							   Window Handle: hWnd
						   Actions 
							   If __CheckPageHandles() = FALSE
								   Return FALSE
							   Set nIndex = __FindPageOfTag( sTag )
							   If nIndex >= 0
								   Set hWnd =  __CreatePageWindow( nIndex )
							   Return ( hWnd != hWndNULL )
					   Function: DestroyPage
						   Description: Destroy a page window.
								Call this function to destroy a page window.
								The page is not removed from the list.  The page window can be recreated..
								The return value is TRUE or FALSE.  
						   Returns 
							   Boolean: 
						   Parameters 
							   String: sTag
								! Page identifier
						   Static Variables 
						   Local variables 
							   Number: nIndex
							   Window Handle: hWnd
						   Actions 
							   If __CheckPageHandles() = FALSE
								   Return FALSE
							   Set nIndex = __FindPageOfTag( sTag )
							   If nIndex >= 0
								   Call __DestroyPageWindow( nIndex )
								   If m_sCurrent = sTag
									   Set m_sCurrent = ''
							   Return ( hWnd = hWndNULL )
					   Function: CreatePageWindowAsChild
						   Description: Create a form as a hidden child window.  
								This function must be implemented by the derived class.
								The return value is the Window Handle of the created window or hWndNULL.
						   Returns 
							   Window Handle: 
						   Parameters 
							   String: sForm
							   Window Handle: hWndParent
							   Number: nLeft  ! Form units
							   Number: nTop
							   Number: nRight
							   Number: nBottom
						   Static Variables 
						   Local variables 
						   Actions 
							! This function must be implemented by the derived class
							   Call SalMessageBox( 'CreatePageWindowAsChild must be implemented by the derived class', 'CPageList', MB_Ok )
							   Return hWndNULL
					   Function: ClearPages
						   Description: Clear all pages.  
								Call this function to remove all entries from the page list and destroy any existing windows.
								The return value is TRUE of FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
						   Static Variables 
						   Local variables 
							   Number: nIndex
							   Number: nMax
						   Actions 
							   Call SalArrayGetUpperBound( Items, 1, nMax )
							   Set nIndex = 0
							   While nIndex <= nMax
								   Call DestroyPage( Items[nIndex].m_sTag )
									   Return nIndex
								   Set nIndex = nIndex + 1
							   Call SalArraySetUpperBound( Items, 1, -1 )
							   Return TRUE
					   Function: SetCurrentPage
						   Description: Activate a page.
								Call this function to cause a page to be created and/or shown.
								All other pages will be hidden.
								The return value is TRUE or FALSE.
						   Returns 
							   Boolean: 
						   Parameters 
							   String: sTag
						   Static Variables 
						   Local variables 
							   String: sLast
						   Actions 
							   If sTag != '' AND __FindPageOfTag( sTag ) < 0
								   Set sTag = ''
							   Set sLast = m_sCurrent
							   Set m_sCurrent = sTag
							   If sLast != sTag
								   If sTag != ''
									   Call CreatePage( sTag )
									   Call __ShowPage( sTag )
								   If sLast != ''
									   Call __HidePage( sLast )
							   Return TRUE
					   Function: GetPageWindow
						   Description: Get the window of a page. 
								Call this function to get the Window Handle of a page.
								The return value is the Window Handle of the page.  hWndNULL is returned if the page window has not been created. 
						   Returns 
							   Window Handle: 
						   Parameters 
							   String: sTag
						   Static Variables 
						   Local variables 
							   Number: nIndex
						   Actions 
							   Set nIndex = __FindPageOfTag( sTag )
							   If nIndex != -1
								   Return Items[nIndex].m_hWndPage
							   Else 
								   Return hWndNULL
					   Function: __ShowPage
						   Description: Show a page
						   Returns 
							   Boolean: 
						   Parameters 
							   String: sTag
						   Static Variables 
						   Local variables 
							   Number: nIndex
						   Actions 
							   Set nIndex = __FindPageOfTag( sTag )
							   If nIndex != -1
								   If Items[nIndex].m_hWndPage
									   Call __SendActivateMessage( nIndex )
								   Return Items[nIndex].Show()
							   Return FALSE
					   Function: __HidePage
						   Description: Hide a page
						   Returns 
							   Boolean: 
						   Parameters 
							   String: sTag
						   Static Variables 
						   Local variables 
							   Number: nIndex
						   Actions 
							   Set nIndex = __FindPageOfTag( sTag )
							   If nIndex != -1
								   If Items[nIndex].Hide()
									   If Items[nIndex].m_hWndPage
										   Call __SendDeactivateMessage( nIndex )
									   Return TRUE
								   Else 
									   Return FALSE
							   Return FALSE
					   Function: __NewPageItem
						   Description: Return the index of an unused element
						   Returns 
							   Number: 
						   Parameters 
						   Static Variables 
						   Local variables 
							   Number: nMax
							   Number: nIndex
						   Actions 
							   Call SalArrayGetUpperBound( Items, 1, nMax )
							! Search for an unused item
							   Set nIndex = 0
							   While nIndex <= nMax
								   If Items[nIndex].m_sTag = ''
									   Return nIndex
								   Set nIndex = nIndex + 1
							! Return the next item in the array
							   Return nMax + 1
					   Function: __FindPageOfForm
						   Description: Locate a page with a give window name
						   Returns 
							   Number: INT
						   Parameters 
							   String: sForm
						   Static Variables 
						   Local variables 
							   Number: nMax
							   Number: nIndex
						   Actions 
							   Call SalArrayGetUpperBound( Items, 1, nMax )
							   Set nIndex = 0
							   While nIndex <= nMax
								   If Items[nIndex].m_sForm = sForm
									   Return nIndex
								   Set nIndex = nIndex + 1
							   Return -1
					   Function: __FindPageOfTag
						   Description: Locate a page with a give tag
						   Returns 
							   Number: 
						   Parameters 
							   String: sTag
						   Static Variables 
						   Local variables 
							   Number: nMax
							   Number: nIndex
						   Actions 
							   Call SalArrayGetUpperBound( Items, 1, nMax )
							   Set nIndex = 0
							   While nIndex <= nMax
								   If Items[nIndex].m_sTag = sTag
									   Return nIndex
								   Set nIndex = nIndex + 1
							   Return -1
					   Function: __CreatePageWindow
						   Description: Create as a window or return existing window handle
						   Returns 
							   Window Handle: 
						   Parameters 
							   Number: nIndex
						   Static Variables 
						   Local variables 
						   Actions 
							   If Items[nIndex].m_hWndPage != hWndNULL
								   Return Items[nIndex].m_hWndPage
							   Else 
								   Set Items[nIndex].m_hWndPage = ..CreatePageWindowAsChild( Items[nIndex].m_sForm, 
										 m_hWndParent, m_nPageLeft, m_nPageTop, m_nPageRight, m_nPageBottom )
								   If Items[nIndex].m_hWndPage
									   Call __SendInitializeMessage( nIndex )
								   Return Items[nIndex].m_hWndPage
					   Function: __DestroyPageWindow
						   Description: Destroy a window
						   Returns 
							   Boolean: 
						   Parameters 
							   Number: nIndex
						   Static Variables 
						   Local variables 
						   Actions 
							   If Items[nIndex].m_hWndPage != hWndNULL
								   Call SalDestroyWindow( Items[nIndex].m_hWndPage )
								   Set Items[nIndex].m_hWndPage = hWndNULL
							   Return TRUE
					   Function: __SendInitializeMessage
						   Description: Let a page form initialize itself
						   Returns 
							   Boolean: 
						   Parameters 
							   Number: nIndex
						   Static Variables 
						   Local variables 
						   Actions 
							   If Items[nIndex].m_hWndPage != hWndNULL
								   Call SalSendMsg( Items[nIndex].m_hWndPage, PAGEM_Initialize, 0, SalHStringToNumber( m_sFormPagesData ) )
							   Return TRUE
					   Function: __SendApplyMessage
						   Description: Let a page form apply its changes
						   Returns 
							   Boolean: 
						   Parameters 
							   Number: nIndex
						   Static Variables 
						   Local variables 
						   Actions 
							   If Items[nIndex].m_hWndPage != hWndNULL
								   Call SalSendMsg( Items[nIndex].m_hWndPage, PAGEM_Apply, 0, 0 )
							   Return TRUE
					   Function: __SendActivateMessage
						   Description: Let a page form know that it is being show
						   Returns 
							   Boolean: 
						   Parameters 
							   Number: nIndex
						   Static Variables 
						   Local variables 
						   Actions 
							   If Items[nIndex].m_hWndPage != hWndNULL
								   Call SalSendMsg( Items[nIndex].m_hWndPage, PAGEM_Activate, 0, 0 )
							   Return TRUE
					   Function: __SendDeactivateMessage
						   Description: Let a page form know that it has been hidden
						   Returns 
							   Boolean: 
						   Parameters 
							   Number: nIndex
						   Static Variables 
						   Local variables 
						   Actions 
							   If Items[nIndex].m_hWndPage != hWndNULL
								   Call SalSendMsg( Items[nIndex].m_hWndPage, PAGEM_Deactivate, 0, 0 )
							   Return TRUE
			! Functional Class: cFormPageItem
.winattr
.end
				   Description: Store information about a page in CPageList
				   Derived From 
				   Class Variables 
				   Instance Variables 
					   String: m_sTag
					   String: m_sForm
					   Window Handle: m_hWndPage
				   Functions 
					   Function: Show
						   Description: 
						   Returns 
							   Boolean: 
						   Parameters 
						   Static Variables 
						   Local variables 
						   Actions 
							   If m_hWndPage
								   Call SalShowWindow( m_hWndPage )
							   Return TRUE
					   Function: Hide
						   Description: 
						   Returns 
							   Boolean: 
						   Parameters 
						   Static Variables 
						   Local variables 
						   Actions 
							   If m_hWndPage
								   Call SalHideWindow( m_hWndPage )
							   Return TRUE
					   Function: Resize
						   Description: Resize the window
						   Returns 
							   Boolean: 
						   Parameters 
							   Window Handle: hWndParent
							   Number: nLeft
							   Number: nTop
							   Number: nRight
							   Number: nBottom
						   Static Variables 
						   Local variables 
						   Actions 
							   If m_hWndPage
								   Call SalSetWindowLoc( m_hWndPage, nLeft, nTop )
								   Call SalSetWindowSize( m_hWndPage, nRight - nLeft, nBottom - nTop )
							   Return TRUE
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture:
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: ctest
		Application Actions
