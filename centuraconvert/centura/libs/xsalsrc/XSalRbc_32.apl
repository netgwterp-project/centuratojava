Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalRbc_32.apl
		* Description	: XSalRemoteObjects Client
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 00000000008C0000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000E10100 0035010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000081EC2120500 1D00FFFF4D61696E
0020: 0029000100040000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000400 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000400 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
			Left:   7.2"
			Top:    1.479"
			Width:  2.75"
			Height: 4.26"
		Options Box Location
.data VIEWINFO
0000: D4181509B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   8.675"
			Top:    2.0"
		Fully Qualified External References? No
		Reject Multiple Window Instances? Yes
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
		! *** XSalRemoteObjects base library
		File Include: XSalRob_32.apl
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalRemoteObjects Client
			Library name: XSal2_32.dll
				! ** Client
				Function: XSalRobConnect
					Description: HWND XSalRobConnect( 
								LPSTR p_lpszServerIP, 
								USHORT p_iServerPort, 
								LONG p_lTimeoutLimit );
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						String: LPSTR
						Number: WORD
						Number: LONG
				Function: XSalRobDisconnect
					Description: BOOL XSalRobDisconnect( 
								HWND FAR * p_lphwndClient );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Window Handle: LPHWND
				Function: XSalRobNewObject
					Description: HOBJECT XSalRobNewObject( 
								HWND p_hwndClient, 
								LPSTR p_lpszObjectName );
					Export Ordinal: 0
					Returns
						Number: DWORD
					Parameters
						Window Handle: HWND
						String: LPSTR
				! Function: XSalRobFreeObject
					    Description: BOOL XSalRobFreeObject( 
								HWND p_hwndClient, 
								HOBJECT FAR * p_lphObject );
					    Export Ordinal: 0
					    Returns 
						    Boolean: BOOL
					    Parameters 
						    Window Handle: HWND
						    Receive Number: LPDWORD
				Function: XSalRobFreeObject
					Description: BOOL XSalRobFreeObject( 
								HWND p_hwndClient, 
								HOBJECT FAR * p_lphObject );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
				Function: XSalRobSetObjectMember
					Description: BOOL XSalRobSetObjectMember( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								LPSTR p_lpszValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						String: LPSTR
				Function: XSalRobGetObjectMember
					Description: INT XSalRobGetObjectMember( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								LPSTR p_lpszValue, 
								LONG p_lMaxLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						Receive String: LPSTR
						Number: LONG
				Function: XSalRobGetObjectMemberMem
					Description: HGLOBAL XSalRobGetObjectMemberMem( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName );
					Export Ordinal: 0
					Returns
						Number: DWORD
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
				Function: XSalRobSetObjectClassMember
					Description: BOOL XSalRobSetObjectClassMember( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								LPSTR p_lpszValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						String: LPSTR
				Function: XSalRobSetObjectBlobMember
					Description: BOOL XSalRobSetObjectBlobMember( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								LPSTR p_lpsBlob, 
								LONG p_lLen );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						String: LPVOID
						Number: LONG
				Function: XSalRobGetObjectClassMember
					Description: INT XSalRobGetObjectClassMember( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								LPSTR p_lpszValue, 
								LONG p_lMaxLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						Receive String: LPSTR
						Number: LONG
				Function: XSalRobGetObjectClassMemberMem
					Description: HGLOBAL XSalRobGetObjectClassMemberMem( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName );
					Export Ordinal: 0
					Returns
						Number: DWORD
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
				Function: XSalRobGetObjectBlobMember
					Description: INT XSalRobGetObjectBlobMember( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								LPSTR p_lpsBlob, 
								LONG FAR * p_lplMaxLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						Receive String: LPVOID
						Receive Number: LPLONG
				Function: XSalRobGetObjectBlobMemberMem
					Description: HGLOBAL XSalRobGetObjectBlobMemberMem( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName );
					Export Ordinal: 0
					Returns
						Number: DWORD
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
				Function: XSalRobExecute
					Description: INT XSalRobExecute( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								LPSTR p_lpszValue, 
								LONG p_lMaxLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						Receive String: LPSTR
						Number: LONG
				Function: XSalRobExecuteMem
					Description: HGLOBAL XSalRobExecuteMem( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName );
					Export Ordinal: 0
					Returns
						Number: DWORD
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
				Function: XSalRobExecuteAsync
					Description: LONG XSalRobExecuteAsync( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszName, 
								HWND p_hwndCallback );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
						Window Handle: HWND
				Function: XSalRobGetAsyncStatus
					Description: INT XSalRobGetAsyncStatus( 
								HWND p_hwndClient, 
								LONG p_lAsyncID );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Window Handle: HWND
						Number: DWORD
				Function: XSalRobAbortAsync
					Description: BOOL XSalRobAbortAsync( 
								HWND p_hwndClient, 
								LONG p_lAsyncID );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
				Function: XSalRobGetLastError
					Description: INT XSalRobGetLastError( void );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
				Function: XSalRobSetClientTimeout
					Description: LONG XSalRobSetClientTimeout( 
								HWND p_hwndClient, 
								LONG p_lTimeout );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Window Handle: HWND
						Number: LONG
				! *** Helper
				Function: XSalRobGetString
					Description: HSTRING XSalRobGetString( HGLOBAL p_hMem );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						Number: DWORD
				Function: XSalRobSetObject
					Description: BOOL XSalRobSetObject( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								HUDV p_hUdv, 
								LPSTR p_lpszTemplate );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						Functional Class Object: HUDV
						String: LPSTR
				Function: XSalRobGetObject
					Description: BOOL XSalRobGetObject( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								HUDV p_hUdv, 
								LPSTR p_lpszTemplate );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						Functional Class Object: HUDV
						String: LPSTR
				Function: XSalRobSetObjectThis
					Description: BOOL XSalRobSetObjectThis( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszTemplate );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
				Function: XSalRobGetObjectThis
					Description: BOOL XSalRobGetObjectThis( 
								HWND p_hwndClient, 
								HOBJECT p_hObject, 
								LPSTR p_lpszTemplate );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						String: LPSTR
				! *** client_to_client
				Function: XSalRobStartMessages
					Description: HWND XSalRobStartMessages( 
								HWND p_hwndCallback, 
								USHORT p_iPort );
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						Window Handle: HWND
						Number: WORD
				Function: XSalRobStopMessages
					Description: BOOL XSalRobStopMessages( 
								HWND FAR * p_hwndMsg );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Window Handle: LPHWND
				Function: XSalRobSendMessage
					Description: BOOL XSalRobSendMessage( 
								HWND p_hwndMsg, 
								LPSTR p_lpszMsgIP, 
								USHORT p_iMsgPort, 
								LPSTR p_lpszMsg );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
						String: LPSTR
				Function: XSalRobReadMessage
					Description: INT XSalRobReadMessage( 
								LPSTR p_lpszMessageIn, 
								LPSTR p_lpszMessageOut, 
								USHORT p_iMaxLen );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: LONG
						Receive String: LPSTR
						Number: INT
				Function: XSalRobGetLastMessageIP
					Description: INT XSalRobGetLastMessageIP( 
								LPSTR p_lpszIP );
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Receive String: LPSTR
				Function: XSalRobGetLocalIP
					Description: LPSTR XSalRobGetLocalIP( 
								HWND p_hWndWSock );
					Export Ordinal: 0
					Returns
						String: LPSTR
					Parameters
						Window Handle: HWND
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cQuickGraph
		Application Actions
