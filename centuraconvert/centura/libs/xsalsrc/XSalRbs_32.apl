Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalRbs_32.apl
		* Description	: XSalRemoteObjects Server
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000640000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000E90100 0035010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000081EC2120500 1D00FFFF4D61696E
0020: 0029000100040000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000400 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000400 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   7.688"
			Top:    0.604"
		Fully Qualified External References? No
		Reject Multiple Window Instances? Yes
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
		! *** XSalRemoteObjects base library
		File Include: XSalRob_32.apl
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalRemoteObjects Server
			Library name: XSal2_32.dll
				Function: __rob__GetHArray
					Description: Private function, don't use directly.
					Export Ordinal: 0
					Returns
						Number: DWORD
					Parameters
						Window Handle: HARRAY
				Function: XSalRobStartServer
					Description: hServer = XSalRobStartServer( 
									sServerIP, 
									nServerPort,
									nMaxClients,
									nTimeout,
									hWndCallback )
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						String: LPSTR
						Number: WORD
						Number: WORD
						Number: LONG
						Window Handle: HWND
				Function: XSalRobStopServer
					Description: bOk = XSalRobStopServer( hServer )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
				Function: XSalRobSetDebugMode
					Description: bPrevValue = XSalRobSetDebugMode( hServer, bOn )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Boolean: BOOL
				Function: XSalRobGetNotificationInfo
					Description: bOk = XSalRobGetNotificationInfo(
									lParam,
									nSocket,
									nSockError,
									nSockEvent,
									sClientIP,
									nRobError,
									nRobEvent,
									sCommandString )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: LONG
						Receive Number: LPINT
						Receive Number: LPWORD
						Receive Number: LPWORD
						Receive String: LPHSTRING
						Receive Number: LPDWORD
						Receive Number: LPDWORD
						Receive String: LPHSTRING
				Function: XSalRobSetServerTimeout
					Description: nOldTimeout = XSalRobSetServerTimeout(
									hServer,
									nNewTimeout )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Window Handle: HWND
						Number: LONG
				Function: XSalRobGetObjectName
					Description:
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						Window Handle: HWND
						Number: DWORD
				Function: XSalRobGetObjectIndex
					Description:
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
			User
		Resources
		Variables
			! *** XSalRemoteObjects buffer string
			Long String: __rob__sString
		Internal Functions
		Named Menus
		Class Definitions
			! *** XSalRemoteObjects Classes
			Functional Class: cXSalRemoteObject
				Description: Base class for Remote Objects.
				Derived From
				Class Variables
				Instance Variables
					! *** Current object handle
					Number: __rob__hMyHandle
				Functions
					! *** Public functions
					Function: getHandle
						Description:
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
						Actions
							Return __rob__hMyHandle
					! *** Virtual public interface functions
					Function: iNew
						Description: Called by the server when the
								object is created.
								
								It returns TRUE to confirm the allocation or
								FALSE to deny it.
						Returns
							Boolean: TRUE = allow allocation
						Parameters
							! *** Client's IP
							String: p_sIP
						Static Variables
						Local variables
						Actions
							! *** If it's not implemented, it always returns TRUE
							Return TRUE
					Function: iFree
						Description: Called by the server when the
								object is release.
						Returns
						Parameters
						Static Variables
						Local variables
						Actions
					Function: iGetContext
						Description: This function MUST be overwritten 
								in case this class exposes 
								class members.
						Returns
							String:
						Parameters
						Static Variables
						Local variables
						Actions
							! *** Error message, this function is needed to access class members.
							Call SalMessageBox( 
									"You must provide the late-bind function iGetContext() returning SalContextCurrent()
									if you want to access class members.",
									"ROB Server error", 
									MB_IconStop )
							! *** If implemented, this function MUST Return SalContextCurrent(  )
					Function: iGetTemplate
						Description:
						Returns
							String:
						Parameters
						Static Variables
						Local variables
						Actions
							! *** Error message, this function is needed to set/get objects using serialization.
							Call SalMessageBox( 
									"You must provide the late-bind function iGetTemplate() returning 
									the template string for this object.",
									"ROB Server error", 
									MB_IconStop )
							! *** If implemented, this function MUST Return a template string.
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cQuickGraph
		Application Actions
