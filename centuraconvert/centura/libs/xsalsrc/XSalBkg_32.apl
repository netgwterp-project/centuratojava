Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalBkg_32.apl
		* Description	: XSalBackground
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000920000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000EF0100 0053010000010000 0001000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
.data DT_MAKERUNDLG
0000: 00000000001B453A 5C5853616C325C41 706C5C5853616C42 6B675F33322E6578
0020: 6516453A5C585361 6C325C41706C5C42 4B4733322E646C6C 16453A5C5853616C
0040: 325C41706C5C424B 4733322E61706300 0001010121443A5C 4D7376635C585361
0060: 6C5C424B475C312E 305C33325C424B47 33322E72756E2144 3A5C4D7376635C58
0080: 53616C5C424B475C 312E305C33325C42 4B4733322E646C6C 21443A5C4D737663
00A0: 5C5853616C5C424B 475C312E305C3332 5C424B4733322E61 7063000001010121
00C0: 443A5C4D7376635C 5853616C5C424B47 5C312E305C33325C 424B4733322E6170
00E0: 6421443A5C4D7376 635C5853616C5C42 4B475C312E305C33 325C424B4733322E
0100: 646C6C21443A5C4D 7376635C5853616C 5C424B475C312E30 5C33325C424B4733
0120: 322E617063000001 010121443A5C4D73 76635C5853616C5C 424B475C312E305C
0140: 33325C424B473332 2E61706C21443A5C 4D7376635C585361 6C5C424B475C312E
0160: 305C33325C424B47 33322E646C6C2144 3A5C4D7376635C58 53616C5C424B475C
0180: 312E305C33325C42 4B4733322E617063 0000010101
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600010002001B00 0200000000000000 0000EF1FD9090500 1D00FFFF4D61696E
0020: 00
.enddata
.data VIEWSIZE
0000: 2100
.enddata
			Left:   0.138"
			Top:    0.292"
			Width:  6.05"
			Height: 2.719"
		Options Box Location
.data VIEWINFO
0000: 1018B80BB80B2500
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.19"
			Top:    0.033"
			Width:  5.2"
			Height: 2.633"
		Tool Palette Location
			Visible? No
			Left:   8.375"
			Top:    1.094"
		Fully Qualified External References? No
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? No
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalBackground
			Library name: XSal2_32.dll
				Function: XSalSetWindowImage
					Description: BOOL XSalSetWindowImage( 
								HWND p_hwnd, 
								HIMAGE p_hImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						Number: WORD
				Function: XSalGetWindowImage
					Description: BOOL XSalGetWindowImage( 
								HWND p_hwnd, 
								HIMAGE FAR * p_hImage );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
				Function: XSalSetWindowImageFile
					Description: BOOL XSalSetWindowImageFile( 
								HWND p_hwnd, 
								LPSTR p_sImageFile, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalSetWindowHotSpot
					Description: BOOL XSalSetWindowHotSpot( 
								HWND p_hwnd, 
								short p_nID, 
								short p_nX, 
								short p_nY, 
								short p_nWidth, 
								short p_nHeight );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: WORD
						Number: WORD
						Number: WORD
						Number: WORD
						Number: WORD
				Function: XSalGetWindowHotSpot
					Description: BOOL XSalGetWindowHotSpot( 
								HWND p_hwnd, 
								short p_nID, 
								short FAR * p_lpnX, 
								short FAR * p_lpnY, 
								short FAR * p_lpnWidth, 
								short FAR * p_lpnHeight );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: WORD
						Receive Number: LPWORD
						Receive Number: LPWORD
						Receive Number: LPWORD
						Receive Number: LPWORD
				Function: XSalGetClientWindow
					Description: HWND XSalGetClientWindow( 
								HWND p_hwnd );
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						Window Handle: HWND
				Function: XSalSetWindowBackColor
					Description: BOOL XSalSetWindowBackColor( 
								HWND p_hwnd, 
								COLORREF p_cColor, 
								short p_nFlags );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Number: WORD
				Function: XSalGetWindowBackColor
					Description: BOOL XSalGetWindowBackColor( 
								HWND p_hwnd, 
								COLORREF FAR * p_lpcColor, 
								short FAR * p_lpnFlags );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPLONG
						Receive Number: LPWORD
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! //
						! // XSalBackground Constants
						! //
				! *** Hotspots messages
				Number: SAM_HotSpotClick	= 0x5001
					! The form, dialog or MDI receive this message
							  when the user pushes the left mouse button on the
							  hotspot.
								
								wParam =	HotSpot ID
								lParam =	Mouse position
				Number: SAM_HotSpotMouseMove	= 0x5002
					! The form, dialog or MDI receive this message
							  when the user moves the mouse on the hotspot.
								
								wParam =	HotSpot ID if the mouse is entering the hotspot or
										NULL if the mouse is leaving the hotspot.
								lParam =	Mouse position
				! *** Image style
				Number: XSalBackground_ImgNormal	= 0x0001
				Number: XSalBackground_ImgFillAll	= 0x0002
				Number: XSalBackground_ImgTiled		= 0x0004
				Number: XSalBackground_ImgCenter	= 0x0008
				Number: XSalBackground_ImgTransparent	= 0x0100
				! *** Background color styles
				Number: XSalBackground_ColorSolid	= 0x0001
				Number: XSalBackground_ColorFadeTop	= 0x0002
				Number: XSalBackground_ColorFadeBottom	= 0x0004
				Number: XSalBackground_ColorFadeLeft	= 0x0008
				Number: XSalBackground_ColorFadeRight	= 0x0010
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
			! *** XSalBackground classes
			Custom Control Class: cXSalTransparentText
				DLL Name: XSal2_32.dll
				Display Settings
					DLL Name: XSal2_32.dll
					MS Windows Class Name: XSal:TransparentText32
					Style:  Class Default
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Class Default
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
				Class Variables
				Instance Variables
				Functions
				Message Actions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window: cCPTableWindow
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field: cQuickMLField
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table: cCPTChildTableWindow
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame: cstaticMinimumSize
			Custom Control: cXSalTransparentText
		Application Actions
