Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalStr_32.apl
		* Description	: String Extensions
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000870000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000150200 0033010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000081EC2120500 1D00FFFF4D61696E
0020: 0029000100000000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000000 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000000 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.4"
			Top:    0.729"
		Fully Qualified External References? No
		Reject Multiple Window Instances? Yes
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalString
			Library name: XSal2_32.dll
				Function: XSalStrCompress
					Description: BOOL XSalStrCompress(
								HSTRING p_hsIn,
								int p_iLevel,
								LPHSTRING p_lphsOut )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						String: HSTRING
						Number: INT
						Receive String: LPHSTRING
				Function: XSalStrUncompress
					Description: BOOL XSalStrCompress(
								HSTRING p_hsIn,
								LPHSTRING p_lphsOut )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						String: HSTRING
						Receive String: LPHSTRING
				Function: XSalStrScan
					Description: LONG XSalStrScan( 
								HSTRING hString, 
								LPSTR lpszChar, 
								LONG lStart );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						String: HSTRING
						String: LPSTR
						Number: LONG
				Function: XSalStrScanR
					Description: LONG XSalStrScanR( 
								HSTRING hString, 
								LPSTR lpszChar, 
								LONG lStart );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						String: HSTRING
						String: LPSTR
						Number: LONG
				Function: XSalStrCount
					Description: LONG XSalStrCount( 
								HSTRING hString, 
								LPSTR lpszChar, 
								LONG lStart, 
								LONG lNChar, 
								BOOL bLeft );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						String: HSTRING
						String: LPSTR
						Number: LONG
						Number: LONG
						Boolean: BOOL
				Function: XSalStrFit
					Description: HSTRING XSalStrFit( 
								HWND hwnd, 
								HSTRING hString, 
								HSTRING hStrEllipse );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						Window Handle: HWND
						String: HSTRING
						String: HSTRING
				Function: XSalStrAny
					Description: LONG XSalStrAny( LPSTR lpszString, LPSTR lpszSet );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						String: LPSTR
						String: LPSTR
				Function: XSalStrNAny
					Description: LONG XSalStrNAny( LPSTR lpszString, LPSTR lpszSet );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						String: LPSTR
						String: LPSTR
				Function: XSalStrToHex
					Description: HSTRING XSalStrToHex( HSTRING hString );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: HSTRING
				Function: XSalStrFromHex
					Description: HSTRING XSalStrFromHex( HSTRING hString );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: LPSTR
				Function: XSalStrFormat
					Description: HSTRING XSalStrFormat( LPSTR lpszString, LPSTR lpszMask );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: LPSTR
						String: LPSTR
				Function: XSalStrMidX
					Description: HSTRING XSalStrMidX( HSTRING hsOrig, LONG lStart, LONG lLen );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: HSTRING
						Number: LONG
						Number: LONG
				Function: XSalStrLeftX
					Description: HSTRING XSalStrLeftX( HSTRING hsOrig, LONG lLen );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: HSTRING
						Number: LONG
				Function: XSalStrRightX
					Description: HSTRING XSalStrRightX( HSTRING hsOrig, LONG lLen );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: HSTRING
						Number: LONG
				Function: XSalStrCompare
					Description: BOOL XSalStrCompare( 
								LPSTR p_lpszSource, 
								LPSTR p_lpszCompare, 
								LONG p_lStart, 
								LONG p_lSize, 
								BOOL p_bCase );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						String: LPSTR
						String: LPSTR
						Number: LONG
						Number: LONG
						Boolean: BOOL
				Function: XSalStrToNumber
					Description: NUMBER	XSalStrToNumber( LPSTR p_lpszString );
					Export Ordinal: 0
					Returns
						Number: NUMBER
					Parameters
						String: LPSTR
				Function: XSalStrFormatDateTime
					Description: HSTRING XSalStrFormatDateTime( 
								DATETIME p_dtDate, 
								LPSTR p_szFormat, 
								LPSTR p_szLanguage );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						Date/Time: DATETIME
						String: LPSTR
						String: LPSTR
				Function: XSalStrFormatDateTimeX
					Description: HSTRING XSalStrFormatDateTime( 
								DATETIME p_dtDate, 
								LPSTR p_szFormat, 
								LPSTR p_szDays, LPSTR p_szShortDays,
								LPSTR p_szMonths, LPSTR p_szShortMonths );
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						Date/Time: DATETIME
						String: LPSTR
						String: LPSTR
						String: LPSTR
						String: LPSTR
						String: LPSTR
						String: LPSTR
				Function: XSalStrMetaphone
					Description: HSTRING XSalStrMetaphone( LPSTR p_lpszText )
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: LPSTR
				Function: XSalStrGetFirstToken
					Description: HSTRING XSalStrGetFirstToken( 
								LPSTR p_lpszList, 
								LPSTR p_lpszSeparators )
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
						String: LPSTR
						String: LPSTR
				Function: XSalStrGetNextToken
					Description: HSTRING XSalStrGetNextToken( )
					Export Ordinal: 0
					Returns
						String: HSTRING
					Parameters
				Function: XSalStrGetCharAt
					Description: char XSalStrGetCharAt( LPCSTR p_szString, long p_lPos )
					Export Ordinal: 0
					Returns
						Number: CHAR
					Parameters
						String: LPSTR
						Number: LONG
				Function: XSalStrSetCharAt
					Description: char XSalStrSetCharAt( LPCSTR p_szString, long p_lPos, char p_NewChar )
					Export Ordinal: 0
					Returns
						Number: CHAR
					Parameters
						String: LPSTR
						Number: LONG
						Number: CHAR
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field: cQuickMLField
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table: cQuickTable
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cQuickGraph
		Application Actions
