Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalSpl_32.apl
		* Description	: XSalSplitter
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000A50000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFFFFFFFF FF000000007C0200 004D010000010000 0001000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Normal
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.388"
			Top:    0.729"
		Fully Qualified External References? Yes
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: 3D Face Color
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalSplitter
			Library name: XSal2_32.dll
				Function: XSalSplitSetFrameChild
					Description: BOOL XSalSplitGetFrameChild( 
								HWND p_hwnd, 
								HWND p_hwndChild, 
								INT p_iFrame );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Window Handle: HWND
						Number: INT
				Function: XSalSplitGetFrameChild
					Description: BOOL XSalSplitGetFrameChild( 
								HWND p_hwnd, 
								INT p_iFrame );
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						Window Handle: HWND
						Number: INT
				Function: XSalSplitSetBarSize
					Description: BOOL XSalSplitSetBarSize( 
								HWND p_hwnd, 
								INT p_iNewSize );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
				Function: XSalSplitGetBarPosition
					Description: LONG XSalSplitGetBarPosition( 
								HWND p_hwnd );
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Window Handle: HWND
				Function: XSalSplitSetBarPosition
					Description: BOOL XSalSplitSetBarPosition( 
								HWND p_hwnd, 
								LONG p_iNewPos );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
				Function: XSalSplitSetStyle
					Description: BOOL XSalSplitSetStyle( 
								HWND p_hwnd, 
								LONG p_iStyle, 
								BOOL p_bOn );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Boolean: BOOL
				Function: XSalSplitGetStyle
					Description: LONG XSalSplitGetStyle( 
								HWND p_hwnd );
							
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Window Handle: HWND
				Function: XSalSplitSetMinMax
					Description: BOOL XSalSplitSetMinMax( 
								HWND p_hwnd, 
								LONG p_iMin, 
								LONG p_iMax );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Number: LONG
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! //
						! // XSalSplitter Constants
						! //
				! *** Styles
				Number: XSalSplitter_Vertical		=0x00000001
				Number: XSalSplitter_Invisible		=0x00000010
				Number: XSalSplitter_NotResizable	=0x00000020
				Number: XSalSplitter_Frame1AbsSize	=0x00000040
				Number: XSalSplitter_Frame2AbsSize	=0x00000080
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
			! *** XSalSplitter classes
			Custom Control Class: cXSalSplitter
				DLL Name: XSal2_32.dll
				Display Settings
					DLL Name: XSal2_32.dll
					MS Windows Class Name: XSal:Splitter32
					Style:  Class Default
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  1.533"
						Width Editable? Class Default
						Height: 0.976"
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Class Default
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? No
				Property Template:
				Class DLL Name:
				Description:
				Derived From
				Class Variables
				Instance Variables
				Functions
					! Public
					Function: setBarSize
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_nSize
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetBarSize( hWndItem, p_nSize )
					Function: setBarPosition
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_nPos
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetBarPosition( hWndItem, p_nPos )
					Function: getBarPosition
						Description:
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
						Actions
							Return XSalSplitGetBarPosition( hWndItem )
					Function: setMinMaxPosition
						Description:
						Returns
						Parameters
							Number: p_nMin
							Number: p_nMax
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetMinMax( hWndItem, p_nMin, p_nMax )
					Function: setStyle
						Description:
						Returns
							Boolean:
						Parameters
							Number: p_nStyleFlag
							Boolean: p_bOn
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetStyle( hWndItem, p_nStyleFlag, p_bOn )
					Function: getStyle
						Description:
						Returns
							Number:
						Parameters
						Static Variables
						Local variables
						Actions
							Return XSalSplitGetStyle( hWndItem )
				Message Actions
			Custom Control Class: cXSalVSplitter
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  0x00000001
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Yes
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalSplitter
				Class Variables
				Instance Variables
				Functions
					! Public
					Function: setLeftFrame
						Description:
						Returns
							Boolean:
						Parameters
							Window Handle: p_hWndChild
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetFrameChild( hWndItem, p_hWndChild, 0 )
					Function: setRightFrame
						Description:
						Returns
							Boolean:
						Parameters
							Window Handle: p_hWndChild
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetFrameChild( hWndItem, p_hWndChild, 1 )
					Function: getLeftFrame
						Description:
						Returns
							Window Handle:
						Parameters
						Static Variables
						Local variables
						Actions
							Return XSalSplitGetFrameChild( hWndItem, 0 )
					Function: getRightFrame
						Description:
						Returns
							Window Handle:
						Parameters
						Static Variables
						Local variables
						Actions
							Return XSalSplitGetFrameChild( hWndItem, 1 )
				Message Actions
			Custom Control Class: cXSalVSplitterInvisible
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  0x00000011
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Yes
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalVSplitter
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Custom Control Class: cXSalHSplitter
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  Class Default
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Yes
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalSplitter
				Class Variables
				Instance Variables
				Functions
					! Public
					Function: setTopFrame
						Description:
						Returns
							Boolean:
						Parameters
							Window Handle: p_hWndChild
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetFrameChild( hWndItem, p_hWndChild, 0 )
					Function: setBottomFrame
						Description:
						Returns
							Boolean:
						Parameters
							Window Handle: p_hWndChild
						Static Variables
						Local variables
						Actions
							Return XSalSplitSetFrameChild( hWndItem, p_hWndChild, 1 )
					Function: getTopFrame
						Description:
						Returns
							Window Handle:
						Parameters
						Static Variables
						Local variables
						Actions
							Return XSalSplitGetFrameChild( hWndItem, 0 )
					Function: getBottomFrame
						Description:
						Returns
							Window Handle:
						Parameters
						Static Variables
						Local variables
						Actions
							Return XSalSplitGetFrameChild( hWndItem, 1 )
				Message Actions
			Custom Control Class: cXSalHSplitterInvisible
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  0x00000010
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Yes
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalHSplitter
				Class Variables
				Instance Variables
				Functions
				Message Actions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture:
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cXSalHSplitter
		Application Actions
