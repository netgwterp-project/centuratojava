Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalTlb_32.apl
		* Description	: XSalToolbar
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000A20000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000100200 0053010000010000 0001000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000081EC2120500 1D00FFFF4D61696E
0020: 0029000100000000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000000 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000000 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: 1018B80BB80B2500
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.3"
			Top:    0.042"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.4"
			Top:    0.729"
		Fully Qualified External References? No
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalToolbar
			Library name: XSal2_32.dll
				Function: XSalToolboxCreate
					Description: hWndToolbox = XSalToolboxCreate( 
										hWndParent, 
										sToolboxDialogName,
										bShowNow,
										iStyle )
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalToolboxDestroy
					Description: bOk = XSalToolboxDestroy( 
									hWndParent,
									sToolboxDialogName )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
				Function: XSalToolboxShow
					Description: bOk = XSalToolboxShow( 
									hWndParent,
									sToolboxDialogName )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
				Function: XSalToolboxHide
					Description: bOk = XSalToolboxHide( 
									hWndParent,
									sToolboxDialogName )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
				Function: XSalToolboxShowAll
					Description: bOk = XSalToolboxShowAll( 
									hWndParent )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
				Function: XSalToolboxHideAll
					Description: bOk = XSalToolboxHideAll( 
									hWndParent )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
				Function: XSalToolboxIsVisible
					Description: bVisible = XSalToolboxIsVisible( 
									hWndParent,
									sToolboxDialogName )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
				Function: XSalToolboxGetPosition
					Description: nPosition = XSalToolboxGetPosition( 
									hWndParent,
									sToolboxDialogName )
					Export Ordinal: 0
					Returns
						Number: WORD
					Parameters
						Window Handle: HWND
						String: LPSTR
				Function: XSalToolboxSetPosition
					Description: bOk = XSalToolboxSetPosition( 
									hWndParent,
									sToolboxDialogName,
									nNewPosition )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalToolboxGetStyle
					Description: nPosition = XSalToolboxGetPosition( 
									hWndParent,
									sToolboxDialogName )
					Export Ordinal: 0
					Returns
						Number: WORD
					Parameters
						Window Handle: HWND
						String: LPSTR
				Function: XSalToolboxSetButtonStyle
					Description: bOk = XSalToolboxMakeFlat( 
									hWndPushbutton,
									nButtonStyle (TLBS_*) )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
				Function: XSalToolboxSetMetrics
					Description: bOk = XSalToolboxSetMetrics( 
									nValue,
									nValueType (TLBM_*) )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: WORD
						Number: WORD
				Function: XSalToolboxGetButtonHandle
					Description: hWndButton = XSalToolboxGetButtonHandle( )
					Export Ordinal: 0
					Returns
						Window Handle: HWND
					Parameters
				Function: XSalToolboxUpdate
					Description: bOk = XSalToolboxUpdate( 
									p_hwndParent, 
									p_lpszName )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! // 
						! // XSalToobar Constants
						! //
				! *** Messages
				Number: SAM_ToolboxSetPosition 			= 0xC540
					! The toolbox receives this message when it's orientation
							  and position are being changed. This allows the program
							  to change some visual aspect of the toolbox.
								wParam =	The new orientation flag
								lParam =	not used
				! *** Toolbox styles
				Number: XSalToolbar_PositionFloating		=0x1000
				Number: XSalToolbar_PositionLeft		=0x0100
				Number: XSalToolbar_PositionTop			=0x0200
				Number: XSalToolbar_PositionRight		=0x0400
				Number: XSalToolbar_PositionBottom		=0x0800
				Number: XSalToolbar_PositionHorizontal		=0x0A00
				Number: XSalToolbar_PositionVertical		=0x0500
				! *** Toolbox styles
				Number: XSalToolbar_ToolboxVisible		=0x0001
				Number: XSalToolbar_ToolboxNewline		=0x0002
				Number: XSalToolbar_ToolboxFillClient		=0x0004
				Number: XSalToolbar_ToolboxFixedPosition	=0x0008
				Number: XSalToolbar_ToolboxResizable		=0x0010
				! *** Button styles
				Number: XSalToolbar_ButtonFlat			=0x0001
				Number: XSalToolbar_ButtonThinBorder		=0x0002
				Number: XSalToolbar_ButtonLeftImage		=0x0010
				Number: XSalToolbar_ButtonRightImage		=0x0020
				! *** Metrics
				Number: XSalToolbar_Metricsheight		=1
				Number: XSalToolbar_MetricsWidth		=2
				Number: XSalToolbar_MetricsLeftOffset		=3
				Number: XSalToolbar_MetricsTopOffset		=4
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
			! *** XSalToolbar classes
			Dialog Box Class: cXSalToolbox
				Title:
				Accesories Enabled? Class Default
				Visible? Class Default
				Display Settings
					Display Style? Class Default
					Visible at Design time? Yes
					Type of Dialog: Class Default
					Window Location and Size
						Left:  
						Top:   
						Width:  2.675"
						Width Editable? Class Default
						Height: 0.542"
						Height Editable? Class Default
					Absolute Screen Location? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
				Next Class Child Key: 3
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Tool Bar
					Display Settings
						Display Style? Class Default
						Location? Class Default
						Visible? Class Default
						Size: Class Default
						Size Editable? Class Default
						Font Name: Class Default
						Font Size: Class Default
						Font Enhancement: Class Default
						Text Color: Class Default
						Background Color: Class Default
					Contents
				Contents
					Custom Control: ccToolboxHandle
						Class Child Ref Key: 0
						Class ChildKey: 3
						Class: cXSalSeparatorDoubleRaised
						Property Template:
						Class DLL Name:
						Display Settings
							DLL Name:
							MS Windows Class Name:
							Style:  Class Default
							ExStyle:  Class Default
							Title:
							Window Location and Size
								Left:   0.0"
								Top:    0.021"
								Width:  Class Default
								Width Editable? Class Default
								Height: Class Default
								Height Editable? Class Default
							Visible? Class Default
							Border? Class Default
							Etched Border? Class Default
							Hollow? Class Default
							Vertical Scroll? Class Default
							Horizontal Scroll? Class Default
							Tab Stop? Class Default
							Tile To Parent? Class Default
							Font Name: Class Default
							Font Size: Class Default
							Font Enhancement: Class Default
							Text Color: Class Default
							Background Color: Class Default
							DLL Settings
						Message Actions
				Derived From
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Pushbutton Class: cXSalFlatButton
				Title:
				Window Location and Size
					Left:  
					Top:   
					Width:  0.3"
					Width Editable? Class Default
					Height: 0.24"
					Height Editable? Class Default
				Visible? Class Default
				Keyboard Accelerator: Class Default
				Font Name: Class Default
				Font Size: Class Default
				Font Enhancement: Class Default
				Picture File Name:
				Picture Transparent Color: Class Default
				Image Style: Class Default
				Text Color: Class Default
				Background Color: Class Default
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
				Class Variables
				Instance Variables
				Functions
				Message Actions
					On SAM_Create
						Call XSalToolboxSetButtonStyle( hWndItem, 
									XSalToolbar_ButtonFlat | XSalToolbar_ButtonThinBorder )
			Custom Control Class: cXSalSeparator
				DLL Name: XSal2_32.dll
				Display Settings
					DLL Name: XSal2_32.dll
					MS Windows Class Name: XSal:Separator32
					Style:  Class Default
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  0.163"
						Width Editable? Class Default
						Height: 0.25"
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Class Default
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? No
				Property Template:
				Class DLL Name:
				Description:
				Derived From
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Custom Control Class: cXSalSeparatorSingleRaised
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  0x00001004
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Class Default
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalSeparator
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Custom Control Class: cXSalSeparatorDoubleRaised
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  0x00001008
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Class Default
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalSeparator
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Custom Control Class: cXSalSeparatorEtched
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  0x00001002
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Class Default
					Etched Border? Class Default
					Hollow? Class Default
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalSeparator
				Class Variables
				Instance Variables
				Functions
				Message Actions
			Custom Control Class: cXSalSeparatorInvisible
				DLL Name:
				Display Settings
					DLL Name:
					MS Windows Class Name:
					Style:  0x00001001
					ExStyle:  Class Default
					Title:
					Window Location and Size
						Left:  
						Top:   
						Width:  Class Default
						Width Editable? Class Default
						Height: Class Default
						Height Editable? Class Default
					Visible? Class Default
					Border? Yes
					Etched Border? Class Default
					Hollow? Class Default
					Vertical Scroll? Class Default
					Horizontal Scroll? Class Default
					Tab Stop? Class Default
					Tile To Parent? Class Default
					Font Name: Class Default
					Font Size: Class Default
					Font Enhancement: Class Default
					Text Color: Class Default
					Background Color: Class Default
					DLL Settings
				List in Tool Palette? Yes
				Property Template:
				Class DLL Name:
				Description:
				Derived From
					Class: cXSalSeparator
				Class Variables
				Instance Variables
				Functions
				Message Actions
		Default Classes
			MDI Window:
			Form Window:
			Dialog Box: Toolbox
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton: cXSalFlatButton
			Radio Button:
			Option Button:
			Check Box:
			Child Table: cQuickTable
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cXSalSeparatorDoubleRaised
		Application Actions
