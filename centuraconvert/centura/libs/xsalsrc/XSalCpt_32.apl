Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalCpt_32.apl
		* Description	: XSalTableWindow
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2003 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000700000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000EF0100 0053010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600010002001B00 0200000000000000 0000EF1FD9090500 1D00FFFF4D61696E
0020: 00
.enddata
.data VIEWSIZE
0000: 2100
.enddata
			Left:   0.138"
			Top:    0.292"
			Width:  6.05"
			Height: 2.719"
		Options Box Location
.data VIEWINFO
0000: D4185307B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.19"
			Top:    0.033"
			Width:  5.2"
			Height: 2.633"
		Tool Palette Location
			Visible? No
			Left:   8.375"
			Top:    1.094"
		Fully Qualified External References? No
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? No
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalTableWindow
			Library name: XSal2_32.dll
				! *** Cell functions
				Function: XSalTblSetCellBackColor
					Description: BOOL XSalTblSetCellBackColor( 
								HWND p_hwndCol, 
								COLORREF p_cBackColor );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
				Function: XSalTblGetCellBackColor
					Description: BOOL XSalTblGetCellBackColor( 
								HWND p_hwndCol, 
								COLORREF FAR * p_lpcBackColor );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPLONG
				Function: XSalTblSetCellFont
					Description: BOOL XSalTblSetCellFont( 
								HWND p_hwndCol, 
								HFONT p_hFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
				Function: XSalTblSetCellFontName
					Description: BOOL XSalTblSetCellFontName( 
								HWND p_hwndCol, 
								LPSTR p_szFontName, 
								int p_nFontSize, 
								WORD p_nFontEnh );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: INT
						Number: WORD
				Function: XSalTblGetCellFont
					Description: BOOL XSalTblGetCellFont( 
								HWND p_hwndCol, 
								HFONT FAR * p_lphFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
				Function: XSalTblSetCellImage
					Description: BOOL XSalTblSetCellImage( 
								HWND p_hwndCol, 
								HIMAGE p_hImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						Number: WORD
				Function: XSalTblSetCellImageFile
					Description: BOOL XSalTblSetCellImageFile( 
								HWND p_hwndCol, 
								LPSTR p_sImageFile, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalTblSetCellImageString
					Description: BOOL XSalTblSetCellImageString( 
								HWND p_hwndCol, 
								HSTRING p_sImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: HSTRING
						Number: WORD
				Function: XSalTblSetCellImageResource
					Description: BOOL XSalTblSetCellImageResource( 
								HWND p_hwndCol, 
								LPSTR p_szImageResource, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalTblGetCellImage
					Description: BOOL XSalTblGetCellImage( 
								HWND p_hwndCol, 
								HIMAGE FAR * p_lphImage, 
								short FAR * p_lpnPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
						Receive Number: LPWORD
				Function: XSalTblSetCellCustom
					Description: BOOL XSalTblSetCellCustom( 
								HWND p_hwndCol, 
								LONG p_nCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
				Function: XSalTblGetCellCustom
					Description: BOOL XSalTblGetCellCustom( 
								HWND p_hwndCol, 
								LONG FAR * p_lpnCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPLONG
				! *** Column functions
				Function: XSalTblSetColumnBackColor
					Description: BOOL XSalTblSetColumnBackColor( 
								HWND p_hwndCol, 
								COLORREF p_cBackColor );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
				Function: XSalTblGetColumnBackColor
					Description: BOOL XSalTblGetColumnBackColor( 
								HWND p_hwndCol, 
								COLORREF FAR * p_lpcBackColor );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPLONG
				Function: XSalTblSetColumnFont
					Description: BOOL XSalTblSetColumnFont( 
								HWND p_hwndCol, 
								HFONT p_hFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
				Function: XSalTblSetColumnFontName
					Description: BOOL XSalTblSetColumnFontName( 
								HWND p_hwndCol, 
								LPSTR p_szFontName, 
								int p_nFontSize, 
								WORD p_nFontEnh );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: INT
						Number: WORD
				Function: XSalTblGetColumnFont
					Description: BOOL XSalTblGetColumnFont( 
								HWND p_hwndCol, 
								HFONT FAR * p_lphFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
				Function: XSalTblSetColumnImage
					Description: BOOL XSalTblSetColumnImage( 
								HWND p_hwndCol, 
								HIMAGE p_hImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						Number: WORD
				Function: XSalTblSetColumnImageFile
					Description: BOOL XSalTblSetColumnImageFile( 
								HWND p_hwndCol, 
								LPSTR p_sImageFile, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalTblSetColumnImageString
					Description: BOOL XSalTblSetColumnImageString( 
								HWND p_hwndCol, 
								HSTRING p_sImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: HSTRING
						Number: WORD
				Function: XSalTblSetColumnImageResource
					Description: BOOL XSalTblSetColumnImageResource( 
								HWND p_hwndCol, 
								LPSTR p_szImageResource, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalTblGetColumnImage
					Description: BOOL XSalTblGetColumnImage( 
								HWND p_hwndCol, 
								HIMAGE FAR * p_lphImage, 
								short FAR * p_lpnPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
						Receive Number: LPWORD
				Function: XSalTblSetColumnCustom
					Description: BOOL XSalTblSetColumnCustom( 
								HWND p_hwndCol, 
								LONG p_nCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
				Function: XSalTblGetColumnCustom
					Description: BOOL XSalTblGetColumnCustom( 
								HWND p_hwndCol, 
								LONG FAR * p_lpnCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPLONG
				! *** Row functions
				Function: XSalTblSetRowBackColor
					Description: BOOL XSalTblSetRowBackColor( 
								HWND p_hwndCol, 
								LONG p_nRow, 
								COLORREF p_cBackColor );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Number: LONG
				Function: XSalTblGetRowBackColor
					Description: BOOL XSalTblGetRowBackColor( 
								HWND p_hwndCol, 
								LONG p_nRow, 
								COLORREF FAR * p_lpcBackColor );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Receive Number: LPLONG
				Function: XSalTblSetRowFont
					Description: BOOL XSalTblSetRowFont( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								HFONT p_hFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Number: DWORD
				Function: XSalTblSetRowFontName
					Description: BOOL XSalTblSetRowFontName( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								LPSTR p_szFontName, 
								int p_nFontSize, 
								WORD p_nFontEnh );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						String: LPSTR
						Number: INT
						Number: WORD
				Function: XSalTblGetRowFont
					Description: BOOL XSalTblGetRowFont( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								HFONT FAR * p_lphFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Receive Number: LPDWORD
				Function: XSalTblSetRowImage
					Description: BOOL XSalTblSetRowImage( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								HIMAGE p_hImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Number: DWORD
						Number: WORD
				Function: XSalTblSetRowImageFile
					Description: BOOL XSalTblSetRowImageFile( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								LPSTR p_sImageFile, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						String: LPSTR
						Number: WORD
				Function: XSalTblSetRowImageString
					Description: BOOL XSalTblSetRowImageString( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								HSTRING p_sImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						String: HSTRING
						Number: WORD
				Function: XSalTblSetRowImageResource
					Description: BOOL XSalTblSetRowImageResource( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								LPSTR p_szImageResource, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						String: LPSTR
						Number: WORD
				Function: XSalTblGetRowImage
					Description: BOOL XSalTblGetRowImage( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								HIMAGE FAR * p_lphImage, 
								short FAR * p_lpnPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Receive Number: LPDWORD
						Receive Number: LPWORD
				Function: XSalTblSetRowCustom
					Description: BOOL XSalTblSetRowCustom( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								LONG p_nCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Number: LONG
				Function: XSalTblGetRowCustom
					Description: BOOL XSalTblGetRowCustom( 
								HWND p_hwndTable, 
								LONG p_nRow, 
								LONG FAR * p_lpnCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
						Receive Number: LPLONG
				! *** Custom-drawing function
				Function: XSalTblGetCustomInfo
					Description: BOOL XSalTblGetCustomInfo( 
								LPCUSTOMCELL p_lpCustomCell, 
									HDC FAR * p_lphdc,
									LPINT p_lpnCol, LPLONG p_lpnRow, 
									LPLONG p_lpnCustomValue,
									LPRECT p_lpRect );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						Receive Number: LPDWORD
						Receive Number: LPINT
						Receive Number: LPLONG
						Receive Number: LPLONG
						structPointer
							Receive Number: INT
							Receive Number: INT
							Receive Number: INT
							Receive Number: INT
				Function: XSalTblSetClipRect
					Description: BOOL XSalTblGetCustomInfo( 
								LPCUSTOMCELL p_lpCustomCell, 
								LPRECT p_lpRect );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						structPointer
							Number: INT
							Number: INT
							Number: INT
							Number: INT
				! *** Column-header functions
				Function: XSalTblSetColHeaderFont
					Description: BOOL XSalTblSetColHeaderFont( 
								HWND p_hwndCol, 
								HFONT p_hFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
				Function: XSalTblSetColHeaderFontName
					Description: BOOL XSalTblSetColHeaderFontName( 
								HWND p_hwndCol, 
								LPSTR p_szFontName, 
								int p_nFontSize, 
								WORD p_nFontEnh );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: INT
						Number: WORD
				Function: XSalTblGetColHeaderFont
					Description: BOOL XSalTblGetColHeaderFont( 
								HWND p_hwndCol, 
								HFONT FAR * p_lphFont );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
				Function: XSalTblSetColHeaderImage
					Description: BOOL XSalTblSetColHeaderImage( 
								HWND p_hwndCol, 
								HIMAGE p_hImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: DWORD
						Number: WORD
				Function: XSalTblSetColHeaderImageFile
					Description: BOOL XSalTblSetColHeaderImageFile( 
								HWND p_hwndCol, 
								LPSTR p_sImageFile, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalTblSetColHeaderImageString
					Description: BOOL XSalTblSetColHeaderImageString( 
								HWND p_hwndCol, 
								HSTRING p_sImage, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: HSTRING
						Number: WORD
				Function: XSalTblSetColHeaderImageResource
					Description: BOOL XSalTblSetColHeaderImageResource( 
								HWND p_hwndCol, 
								LPSTR p_szImageResource, 
								short p_nPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						String: LPSTR
						Number: WORD
				Function: XSalTblGetColHeaderImage
					Description: BOOL XSalTblGetColHeaderImage( 
								HWND p_hwndCol, 
								HIMAGE FAR * p_lphImage, 
								short FAR * p_lpnPosition );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPDWORD
						Receive Number: LPWORD
				Function: XSalTblSetColHeaderCustom
					Description: BOOL XSalTblSetColHeaderCustom( 
								HWND p_hwndCol, 
								LONG p_nCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: LONG
				Function: XSalTblGetColHeaderCustom
					Description: BOOL XSalTblGetColHeaderCustom( 
								HWND p_hwndCol, 
								LONG FAR * p_lpnCustomValue );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Receive Number: LPLONG
				! *** Sort and Clipboard
				Function: XSalTblSortRows
					Description: BOOL XSalTblSortRows( 
								HWND p_hwndTable, 
								INT p_nColumndID, 
								INT p_nFlags )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						Number: INT
				Function: XSalTblCopyRows
					Description: BOOL XSalTblCopyRows( 
								HWND p_hwndTable, 
								WORD p_nFlagsOn, 
								WORD p_nFlagsOff, 
								INT p_nCopyFlags )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: WORD
						Number: WORD
						Number: INT
				Function: XSalTblPasteRows
					Description: BOOL XSalTblPasteRows( 
								HWND p_hwndTable, 
								INT p_nCopyFlags )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
				Function: XSalTblSetDelimiters
					Description: BOOL XSalTblSetDelimiters( 
								LPSTR p_szColDel, 
								LPSTR p_szRowDel )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						String: LPSTR
						String: LPSTR
				Function: XSalTblCopyRowsToString
					Description: BOOL XSalTblCopyRows( 
								HWND p_hwndTable, 
								WORD p_nFlagsOn, 
								WORD p_nFlagsOff, 
								INT p_nCopyFlags,
								LPHSTRING p_sCsvString )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: WORD
						Number: WORD
						Number: INT
						Receive String: LPHSTRING
				Function: XSalTblPasteRowsFromString
					Description: BOOL XSalTblPasteRows( 
								HWND p_hwndTable, 
								INT p_nCopyFlags,
								HSTRING p_sCsvString )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Window Handle: HWND
						Number: INT
						String: HSTRING
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! //
						! // XSalTableWindow Constants
						! //
				! *** Callback message for cell drawing
				Number: SAM_DrawCell	= 0x5000
					! The tablewindow receives this message each time a custom drawn
							  cell needs to be repainted.
								
								wParam =	cell selected flag (TRUE or FALSE)
								lParam =	pointer to a structure with drawing information. 
										Use it with XSalTblGetCustomInfo().
				! *** Notification message from XSalTblSortRows
				Number: SAM_SwapRows	= 0x5001
					! The tablewindow receives this message each time the quick-sort
							routine swaps two rows.
								
								wParam =	first row
								lParam =	second row
				! *** Sort flags
				Number: XSalTableWindow_SortAscending	= 0x0000
				Number: XSalTableWindow_SortDescending	= 0x0001
				Number: XSalTableWindow_SortAsDateTime	= 0x2000
				Number: XSalTableWindow_SortAsNumber	= 0x4000
				Number: XSalTableWindow_SortCase	= 0x8000
				! *** Clipboard flags
				Number: XSalTableWindow_SkipHiddenCols	= 0x0001
				Number: XSalTableWindow_ColumnTitles	= 0x0002
				! *** Image position flags
				Number: XSalTableWindow_ImgBottomLeft		=1
				Number: XSalTableWindow_ImgBottomCenter		=2
				Number: XSalTableWindow_ImgBottomRight		=3
				Number: XSalTableWindow_ImgCenterLeft		=4
				Number: XSalTableWindow_ImgCenterCenter		=5
				Number: XSalTableWindow_ImgCenterRight		=6
				Number: XSalTableWindow_ImgTopLeft		=7
				Number: XSalTableWindow_ImgTopCenter		=8
				Number: XSalTableWindow_ImgTopRight		=9
				Number: XSalTableWindow_ImgFillAll		=0
				Number: XSalTableWindow_ImgTransparent		=128
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window: cCPTableWindow
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field: cQuickMLField
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table: cCPTChildTableWindow
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame: cstaticMinimumSize
			Custom Control: cc
		Application Actions
