Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalSyb_32.apl
		* Description	: XSalSybase
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 00000000009C0000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000F70100 0035010000010000 0001000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000081EC2120500 1D00FFFF4D61696E
0020: 0029000100000000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000000 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000000 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: D418D007B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.4"
			Top:    0.729"
		Fully Qualified External References? No
		Reject Multiple Window Instances? Yes
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalSybase
			Library name: XSal2_32.dll
				Function: XSalSybConnect
					Description: BOOL XSalSybConnect( 
								LPSQLHANDLENUMBER );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Sql Handle: LPHSQLHANDLE
				Function: XSalSybDisconnect
					Description: BOOL XSalSybDisconnect( 
								LPSQLHANDLENUMBER );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Sql Handle: LPHSQLHANDLE
				Function: XSalSybPrepare
					Description: BOOL XSalSybPrepare( 
								SQLHANDLENUMBER hSql, 
								LPSTR lpszSqlCmd );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Sql Handle: HSQLHANDLE
						String: LPSTR
				Function: XSalSybExecute
					Description: BOOL XSalSybExecute( 
								SQLHANDLENUMBER hSql );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Sql Handle: HSQLHANDLE
				Function: XSalSybFetchRow
					Description: BOOL XSalSybFetchRow( 
								SQLHANDLENUMBER hSql, 
								LONG lRow, 
								LPINT lpiRet );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Sql Handle: HSQLHANDLE
						Number: LONG
						Receive Number: LPINT
				Function: XSalSybFetchNext
					Description: BOOL XSalSybFetchNext( 
								SQLHANDLENUMBER hSql, 
								LPINT lpiRet );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Sql Handle: HSQLHANDLE
						Receive Number: LPINT
				Function: XSalSybFetchPrevious
					Description: BOOL XSalSybFetchPrevious( 
								SQLHANDLENUMBER hSql, 
								LPINT lpiRet );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Sql Handle: HSQLHANDLE
						Receive Number: LPINT
				Function: XSalSybVarSetup
					Description: BOOL XSalSybVarSetup( 
								SQLHANDLENUMBER hSql );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Sql Handle: HSQLHANDLE
				Function: XSalSybSetLongBindDatatype
					Description: BOOL XSalSybSetLongBindDatatype( 
								INT iColNumber, 
								INT iType );
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: INT
						Number: INT
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
				! //
						! // XSalSybase Constants
						! //
				Number: SYBTEXT		= 22
				Number: SYBIMAGE	= 23
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field: cQuickMLField
			Pushbutton: cQuickCommander
			Radio Button:
			Option Button:
			Check Box:
			Child Table: cQuickTable
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cQuickGraph
		Application Actions
