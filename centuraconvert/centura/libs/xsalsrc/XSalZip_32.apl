Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalZip_32.apl
		* Description	: ZIP Extensions
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2000 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000550000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF160000 0016000000150200 0033010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
		Outline Window State: Maximized
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000081EC2120500 1D00FFFF4D61696E
0020: 0029000100000000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000000 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000000 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.4"
			Top:    0.729"
		Fully Qualified External References? No
		Reject Multiple Window Instances? Yes
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: System Default
				Font Size: System Default
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: Use Parent
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalZIP
			Library name: XSal2_32.dll
				! GZIP file handling
				Function: XSalGZipOpen
					Description: BOOL XSalGZipOpen(
								gzFile* p_lpFile,
								LPSTR p_lpszFileName,
								LPSTR p_lpszMode )
							
							Opens a XSalGZip (.gz) file for reading or writing. The mode parameter
							is as in fopen ("rb" or "wb") but can also include a compression level
							("wb9") or a strategy: 'f' for filtered data as in "wb6f", 'h' for
							Huffman only compression as in "wb1h".
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Number: LPDWORD
						String: LPSTR
						String: LPSTR
				Function: XSalGZipClose
					Description: BOOL XSalGZipClose(
								gzFile* p_lpFile )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Number: LPDWORD
				Function: XSalGZipGetError
					Description: LPSTR XSalGZipGetError(
								gzFile p_File,
								LPINT p_lpiError )
					Export Ordinal: 0
					Returns
						String: LPSTR
					Parameters
						Number: DWORD
						Receive Number: LPINT
				Function: XSalGZipRead
					Description: LONG XSalGZipRead(
								gzFile p_File,
								LPHSTRING p_lphsBufferIn,
								LONG p_lSize )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
						Receive String: LPHSTRING
						Number: LONG
				Function: XSalGZipWrite
					Description: LONG XSalGZipWrite(
								gzFile p_File,
								LPVOID p_lpBufferOut,
								LONG p_lSize )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
						String: LPVOID
						Number: LONG
				Function: XSalGZipGetStr
					Description: LONG XSalGZipGetStr(
								gzFile p_File,
								LPHSTRING p_lphsBufferIn,
								LONG p_lSize )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
						Receive String: LPHSTRING
						Number: LONG
				Function: XSalGZipPutStr
					Description: INT XSalGZipPutStr(
								gzFile p_File,
								LPSTR p_lpBufferOut )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
						String: LPSTR
				Function: XSalGZipGetChar
					Description: CHAR XSalGZipGetStr( gzFile p_File )
					Export Ordinal: 0
					Returns
						Number: CHAR
					Parameters
						Number: DWORD
				Function: XSalGZipPutChar
					Description: BOOL XSalGZipPutChar(
								gzFile p_File,
								CHAR p_cChar )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						File Handle: HFILE
						Number: CHAR
				Function: XSalGZipSeek
					Description: LONG XSalGZipSeek(
								gzFile p_File,
								LONG p_lOffset,
								INT p_iWhence )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
						Number: LONG
						Number: INT
				Function: XSalGZipRewind
					Description: BOOL XSalGZipRewind( gzFile p_File )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				Function: XSalGZipTell
					Description: LONG XSalGZipTell( gzFile p_File )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
				Function: XSalGZipEof
					Description: BOOL XSalGZipEof( gzFile p_File )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				Function: XSalGZipFlush
					Description: BOOL XSalGZipFlush( gzFile p_File )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				! ZIP archive handling
				Function: XSalZipOpenArchive
					Description: BOOL XSalZipOpenArchive( 
								LPHZIPFILE p_lpZip, 
								LPSTR p_lpszZipFileName,
								INT p_iMode )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Number: LPDWORD
						String: LPSTR
						Number: INT
				Function: XSalZipCloseArchive
					Description: BOOL XSalZipCloseArchive( LPHZIPFILE p_lpZip )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Receive Number: LPDWORD
				Function: XSalZipCreateFile
					Description: BOOL XSalZipCreateFileInArchive( 
								HZIPFILE p_Zip, 
								LPSTR p_lpszZipFileName, 
								DATETIME p_dtFileDate,
								LPSTR p_lpszComment, 
								INT p_iLevel )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						String: LPSTR
						Date/Time: DATETIME
						String: LPSTR
						Number: INT
				Function: XSalZipWrite
					Description: BOOL XSalZipWrite( 
								HZIPFILE p_Zip, 
								LPVOID p_lpsBuffer, 
								LONG p_lSize )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						String: LPVOID
						Number: LONG
				Function: XSalZipFindFirstFile
					Description: BOOL XSalZipFindFirstFile( HZIPFILE p_Zip )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				Function: XSalZipFindNextFile
					Description: BOOL XSalZipFindFirstFile( HZIPFILE p_Zip )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				Function: XSalZipFindFile
					Description: BOOL XSalZipFindFile( 
								HZIPFILE p_Zip, 
								LPSTR p_lpszFileName )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						String: LPSTR
				Function: XSalZipOpenFile
					Description: BOOL XSalZipOpenFile( HZIPFILE p_Zip )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				Function: XSalZipCloseFile
					Description: BOOL XSalZipCloseFile( HZIPFILE p_Zip )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				Function: XSalZipRead
					Description: LONG XSalZipRead( 
								HZIPFILE p_Zip, 
								LPHSTRING p_lphsBufferIn, 
								LONG p_lSize )
					Export Ordinal: 0
					Returns
						Number: LONG
					Parameters
						Number: DWORD
						Receive String: LPHSTRING
						Number: LONG
				Function: XSalZipGetFileInfo
					Description: BOOL XSalZipGetFileInfo( 
									HZIPFILE p_Zip, 
									LPHSTRING p_lphsFileName,
									LPHSTRING p_lphsComment,
									LPDATETIME p_lpdtDate,
									LPLONG p_lplCompressedSize,
								 	LPLONG p_lplUncompressedSize )
							
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						Receive String: LPHSTRING
						Receive String: LPHSTRING
						Receive Date/Time: LPDATETIME
						Receive Number: LPLONG
						Receive Number: LPLONG
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
			User
		Resources
		Variables
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field: cQuickMLField
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table: cQuickTable
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture: cQuickPicture
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control: cQuickGraph
		Application Actions
