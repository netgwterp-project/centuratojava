Application Description: * Pivato Consulting, inc.
		* XSal - eXtended Sal
		*
		* File name     : XSalScr_32.apl
		* Description	: XSalScript
		* Author	: Gianluca Pivato
		
		Copyright � 1998-2002 by Pivato Consulting, inc. All Rights Reserved.
	Outline Version - 4.0.26
	Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 00000000004B0000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFFFFFFFF FF000000007C0200 004D010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
.data DT_MAKERUNDLG
0000: 000000000021633A 5C77696E646F7773 5C6465736B746F70 5C5853616C536372
0020: 5F33322E65786521 633A5C77696E646F 77735C6465736B74 6F705C5853616C53
0040: 63725F33322E646C 6C21633A5C77696E 646F77735C646573 6B746F705C585361
0060: 6C5363725F33322E 6170630000010101 1A453A5C5853616C 5C41706C5C585361
0080: 6C5363725F33322E 72756E1A453A5C58 53616C5C41706C5C 5853616C5363725F
00A0: 33322E646C6C1A45 3A5C5853616C5C41 706C5C5853616C53 63725F33322E6170
00C0: 6300000101011A45 3A5C5853616C5C41 706C5C5853616C53 63725F33322E6170
00E0: 641A453A5C585361 6C5C41706C5C5853 616C5363725F3332 2E646C6C1A453A5C
0100: 5853616C5C41706C 5C5853616C536372 5F33322E61706300 000101011A453A5C
0120: 5853616C5C41706C 5C5853616C536372 5F33322E61706C1A 453A5C5853616C5C
0140: 41706C5C5853616C 5363725F33322E64 6C6C1A453A5C5853 616C5C41706C5C58
0160: 53616C5363725F33 322E617063000001 0101
.enddata
		Outline Window State: Normal
		Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
			Left:   -0.013"
			Top:    0.0"
			Width:  8.013"
			Height: 4.969"
		Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
			Visible? Yes
			Left:   4.15"
			Top:    1.885"
			Width:  3.8"
			Height: 2.073"
		Class Editor Location
			Visible? No
			Left:   0.575"
			Top:    0.094"
			Width:  5.063"
			Height: 2.719"
		Tool Palette Location
			Visible? No
			Left:   6.388"
			Top:    0.729"
		Fully Qualified External References? Yes
		Reject Multiple Window Instances? No
		Enable Runtime Checks Of External References? Yes
		Use Release 4.0 Scope Rules? No
	Libraries
	Global Declarations
		Window Defaults
			Tool Bar
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Form Window
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Dialog Box
				Display Style? Etched
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Top Level Table Window
				Font Name: MS Sans Serif
				Font Size: 8
				Font Enhancement: System Default
				Text Color: System Default
				Background Color: System Default
			Data Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Multiline Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Spin Field
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Background Text
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Pushbutton
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Radio Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Check Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Option Button
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
			Group Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Child Table Window
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			List Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Combo Box
				Font Name: Use Parent
				Font Size: Use Parent
				Font Enhancement: Use Parent
				Text Color: Use Parent
				Background Color: Use Parent
			Line
				Line Color: Use Parent
			Frame
				Border Color: Use Parent
				Background Color: 3D Face Color
			Picture
				Border Color: Use Parent
				Background Color: Use Parent
		Formats
			Number: 0'%'
			Number: #0
			Number: ###000
			Number: ###000;'($'###000')'
			Date/Time: hh:mm:ss AMPM
			Date/Time: M/d/yy
			Date/Time: MM-dd-yy
			Date/Time: dd-MMM-yyyy
			Date/Time: MMM d, yyyy
			Date/Time: MMM d, yyyy hh:mm AMPM
			Date/Time: MMMM d, yyyy hh:mm AMPM
		External Functions
			! *** XSalScript
			Library name: XSal2_32.dll
				Function: XSalScriptCreate
					Description: HSCRIPT XSalScriptCreate( 
								LPSTR p_lpszScript )
					Export Ordinal: 0
					Returns
						Number: DWORD
					Parameters
						String: LPSTR
				Function: XSalScriptFree
					Description: BOOL XSalScriptFree( 
								HSCRIPT p_hScript )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
				Function: XSalScriptAddLine
					Description: BOOL XSalScriptAddLine( 
								HSCRIPT p_hScript, 
								LPSTR p_lpszLine )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						String: LPSTR
				Function: XSalScriptSetContext
					Description: BOOL XSalScriptSetContext( 
								HSCRIPT p_hScript, 
								HSTRING p_hsContext )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						String: HSTRING
				Function: XSalScriptRun
					Description: BOOL XSalScriptRun( 
								HSCRIPT p_hScript, 
								LPHSTRING p_lphsReturn )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						Receive String: LPHSTRING
				Function: XSalScriptGetErrors
					Description: INT XSalScriptGetErrors( 
								HSCRIPT p_hScript, 
								LPHSTRING p_lphsReturn )
					Export Ordinal: 0
					Returns
						Number: INT
					Parameters
						Number: DWORD
						Receive String: LPHSTRING
				Function: XSalScriptSetTimeout
					Description: BOOL XSalScriptSetTimeout( 
								HSCRIPT p_hScript, 
								LONG p_lTimeoutSecs )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						Number: LONG
				Function: XSalScriptSetDebugMode
					Description: BOOL XSalScriptSetDebugMode( 
								HSCRIPT p_hScript, 
								BOOL p_bDebugModeOn )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						Boolean: BOOL
				Function: XSalScriptGetLocalName
					Description: BOOL XSalScriptGetLocalName( 
								HSCRIPT p_hScript, 
								LPSTR p_szLocalName,
								LPHSTRING p_hsVarName )
					Export Ordinal: 0
					Returns
						Boolean: BOOL
					Parameters
						Number: DWORD
						String: LPSTR
						Receive String: LPHSTRING
		Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
			System
			User
		Resources
		Variables
			! *** XSalScript Local Symbols Buffer
			String: __xsc__sSymbols[*]
			Number: __xsc__nSymbols[*]
			Date/Time: __xsc__dtSymbols[*]
			Window Handle: __xsc__hSymbols[*]
		Internal Functions
		Named Menus
		Class Definitions
		Default Classes
			MDI Window: cBaseMDI
			Form Window:
			Dialog Box:
			Table Window:
			Quest Window:
			Data Field:
			Spin Field:
			Multiline Field:
			Pushbutton:
			Radio Button:
			Option Button:
			Check Box:
			Child Table:
			Quest Child Window: cQuickDatabase
			List Box:
			Combo Box:
			Picture:
			Vertical Scroll Bar:
			Horizontal Scroll Bar:
			Column:
			Background Text:
			Group Box:
			Line:
			Frame:
			Custom Control:
		Application Actions
