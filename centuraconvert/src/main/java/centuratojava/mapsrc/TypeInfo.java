package centuratojava.mapsrc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TypeInfo implements Serializable, Comparable<TypeInfo> {

	private static final long serialVersionUID = -59012271764917990L;

	private Integer id;
	private String nameJava;
	private String packageJava;
	private Integer owner;
	@JsonIgnore
	private TypeInfo ownerJava;
	private Set<String> nameCDK = new TreeSet<>();
	private String fileStr;
	private String superType;
	private String returnType;
	private String referenceType;
	private boolean array = false;
	private Set<String> interfaceType = new TreeSet<String>();
	private TypeJavaEnum typeJava;
	private Set<TypeInfo> childrens = new TreeSet<>();
	private boolean api = false;
	private boolean fixed = false;
	@JsonIgnore
	private Object source = null;
	@JsonIgnore
	private MapType mapType = null;
	
	
	@JsonIgnore
	public TypeInfo addChildren(String type, TypeJavaEnum typeJava, String typeSrc) {
		if (type == null)
			return null;
		TypeInfo typeInfo = getChildren(type, typeJava);
		if (typeInfo == null) {
			typeInfo = new TypeInfo();
			typeInfo.setId(MapType.genId());
			typeInfo.setNameJava(type);
			typeInfo.setTypeJava(typeJava);
			typeInfo.setPackageJava(getPackageJava());
			typeInfo.setMapType(getMapType());
			addChildren(typeInfo);
		}
		typeInfo.getNameCDK().add(typeSrc);
		return typeInfo;
	}
	
	@JsonIgnore
	public TypeInfo addChildren(TypeInfo typeInfo) {
		typeInfo.setOwnerJava(this);
		typeInfo.setOwner(getId());
		typeInfo.setMapType(getMapType());
		if(!getChildrens().contains(typeInfo))
			getChildrens().add(typeInfo);
		return typeInfo;
	}
	
	@JsonIgnore
	public TypeInfo getChildren(String type, TypeJavaEnum typeJavaEnum) {
		if (type == null)
			return null;
		Optional<TypeInfo> findFirst = getChildrens().stream().filter(t -> t.getNameJava().equals(type) && t.getTypeJava().equals(typeJavaEnum)).findFirst();
		TypeInfo typeInfo = null;
		if (findFirst != null && findFirst.isPresent())
			typeInfo = findFirst.get();
		return typeInfo;
	}

	public String getFileStr() {
		return fileStr;
	}

	public Set<String> getNameCDK() {
		return nameCDK;
	}

	public String getNameJava() {
		return nameJava;
	}

	public String getPackageJava() {
		return packageJava;
	}

	public TypeJavaEnum getTypeJava() {
		return typeJava;
	}

	public boolean isApi() {
		return api;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setApi(boolean api) {
		if (!isFixed()) {
			this.api = api;
		}
	}

	public void setFileStr(String fileStr) {
		if (!isFixed()) {
			this.fileStr = fileStr;
		}
	}

	public void setFixed(boolean fixed) {
		if (!isFixed()) {
			this.fixed = fixed;
		}
	}

	public void setNameCDK(Set<String> nameCDK) {
		if (!isFixed()) {
			this.nameCDK = nameCDK;
		}
	}

	public void setNameJava(String nameJava) {
		if (!isFixed()) {
			this.nameJava = nameJava;
		}
	}

	public void setPackageJava(String packageJava) {
		if (!isFixed()) {
			this.packageJava = packageJava;
		}
	}

	public void setTypeJava(TypeJavaEnum typeJava) {
		if (!isFixed()) {
			this.typeJava = typeJava;
		}
	}

	public String getSuperType() {
		return superType;
	}

	public void setSuperType(String superType) {
		this.superType = superType;
	}

	public Set<String> getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(Set<String> interfaceType) {
		this.interfaceType = interfaceType;
	}

	public TypeInfo getOwnerJava() {
		return ownerJava;
	}

	public void setOwnerJava(TypeInfo ownerJava) {
		this.ownerJava = ownerJava;
	}

	public Object getSource() {
		return source;
	}

	public void setSource(Object source) {
		this.source = source;
	}

	public MapType getMapType() {
		return mapType;
	}

	public void setMapType(MapType mapType) {
		this.mapType = mapType;
	}



	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TypeInfo [typeJava=");
		builder.append(typeJava);
		//builder.append(", id=");
		//builder.append(id);
		builder.append(", nameJava=");
		builder.append(nameJava);
		builder.append(", referenceType=");
		builder.append(referenceType);
		builder.append(", returnType=");
		builder.append(returnType);
		builder.append(", packageJava=");
		builder.append(packageJava);
		/*builder.append(", owner=");
		builder.append(owner);
		builder.append(", ownerJava=");
		builder.append(ownerJava);
		builder.append(", nameCDK=");
		builder.append(nameCDK);
		builder.append(", superType=");
		builder.append(superType);
		builder.append(", array=");
		builder.append(array);
		builder.append(", interfaceType=");
		builder.append(interfaceType);
		builder.append(", api=");
		builder.append(api);
		builder.append(", fixed=");
		builder.append(fixed);*/
		builder.append("]");
		return builder.toString();
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	@JsonIgnore
	public TypeInfo resolveIdentify(String string) {
		return resolveIdentify(string, true, true);
	}

	@JsonIgnore
	public TypeInfo resolveIdentify(String string, boolean testeAPI, boolean recursive) {
		if (getMapType() != null) {
			if (getTypeJava().equals(TypeJavaEnum.INTERFACEINCLUDE)) {
				if (listType(TypeJavaEnum.STATICVAR).size() > 0) {
					List<TypeInfo> collectVar = listType(TypeJavaEnum.STATICVAR).stream()
							.filter(t -> t.getNameJava().equals(string)).collect(Collectors.toList());
					if (collectVar.size() == 1)
						return collectVar.get(0);
				}
				if (listType(TypeJavaEnum.CONSTANT).size() > 0) {
					List<TypeInfo> collectConstant = listType(TypeJavaEnum.CONSTANT).stream()
							.filter(t -> t.getNameJava().equals(string)).collect(Collectors.toList());
					if (collectConstant.size() == 1)
						return collectConstant.get(0);
				}
				if (listType(TypeJavaEnum.GLOBALFUNCTION).size() > 0) {
					List<TypeInfo> collectFunction = listType(TypeJavaEnum.GLOBALFUNCTION).stream()
							.filter(t -> t.getNameJava().equals(string)).collect(Collectors.toList());
					if (collectFunction.size() == 1)
						return collectFunction.get(0);
				}
				if (getMapType().getDependency() != null) {
					for (String dep : getMapType().getDependency()) {
						MapType instanceDep = MapType.getInstanceName(dep);
						if(instanceDep!=null){
							List<TypeInfo> listInterType = instanceDep.listType(TypeJavaEnum.INTERFACEINCLUDE);
							if (listInterType.size() == 1) {
								TypeInfo typeInterInfo = listInterType.get(0);
								TypeInfo resolveIdentify = typeInterInfo.resolveIdentify(string, false, false);
								if (resolveIdentify != null)
									return resolveIdentify;
							}
						}
					}
				}
			} else {
				if (getTypeJava().equals(TypeJavaEnum.GLOBALFUNCTION) || getTypeJava().equals(TypeJavaEnum.FUNCTION)
						|| getTypeJava().equals(TypeJavaEnum.EXTERNALFUNCTION)) {
					if (listType(TypeJavaEnum.VAR).size() > 0) {
						List<TypeInfo> collectVar = listType(TypeJavaEnum.VAR).stream()
								.filter(t -> t.getNameJava().equals(string)).collect(Collectors.toList());
						if (collectVar.size() == 1)
							return collectVar.get(0);
					}
					if (listType(TypeJavaEnum.PARAMETER).size() > 0) {
						List<TypeInfo> collectParameter = listType(TypeJavaEnum.PARAMETER).stream()
								.filter(t -> t.getNameJava().equals(string)).collect(Collectors.toList());
						if (collectParameter.size() == 1)
							return collectParameter.get(0);
					}
				}
				if (getTypeJava().equals(TypeJavaEnum.GLOBALFUNCTION) || getTypeJava().equals(TypeJavaEnum.FUNCTION)) {
					if (listType(TypeJavaEnum.STATICVAR).size() > 0) {
						List<TypeInfo> collectVar = listType(TypeJavaEnum.STATICVAR).stream()
								.filter(t -> t.getNameJava().equals(string)).collect(Collectors.toList());
						if (collectVar.size() == 1)
							return collectVar.get(0);
					}
				}
				if (recursive && getOwnerType() != null){
					TypeInfo typeResolved = getOwnerType().resolveIdentify(string, false, false);
					if(typeResolved!=null) return typeResolved;
				}
			}

		}
		if (testeAPI) {
			MapType instanceDep = MapType.getInstanceAPI();
			List<TypeInfo> listInterType = instanceDep.listType(TypeJavaEnum.INTERFACEINCLUDE);
			if (listInterType.size() == 1) {
				TypeInfo typeInterInfo = listInterType.get(0);
				TypeInfo resolveIdentify = typeInterInfo.resolveIdentify(string, false, false);
				if (resolveIdentify != null)
					return resolveIdentify;
			}
		}
		return null;
	}

	@JsonIgnore
	public List<TypeInfo> listType(TypeJavaEnum typeJava) {
		List<TypeInfo> ret = new ArrayList<>();
		getChildrens().stream().forEach(t -> {
			if (t.getTypeJava().equals(typeJava))
				ret.add(t);
		});
		return ret;
	}

	@JsonIgnore
	public TypeInfo getOwnerType() {
		if (getMapType() != null)
			return getMapType().getType(getOwner());
		return null;
	}

	public Set<TypeInfo> getChildrens() {
		return childrens;
	}

	public void setChildrens(Set<TypeInfo> childrens) {
		this.childrens.clear();
		if(childrens!=null){
			for (TypeInfo typeInfo : childrens) {
				addChildren(typeInfo);
			}
		}
	}

	public boolean isArray() {
		return array;
	}

	public void setArray(boolean array) {
		this.array = array;
	}

	@Override
	public int compareTo(TypeInfo o) {
		int result = getTypeJava().compareTo(o.getTypeJava());
		if(result != 0) return result;
		if(getNameJava()==null) 
			result = -1;
		else
			result = getNameJava().compareTo(o.getNameJava());
		if(result != 0) return result;
		result = getId().compareTo(o.getId());
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nameJava == null) ? 0 : nameJava.hashCode());
		result = prime * result + ((typeJava == null) ? 0 : typeJava.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeInfo other = (TypeInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nameJava == null) {
			if (other.nameJava != null)
				return false;
		} else if (!nameJava.equals(other.nameJava))
			return false;
		if (typeJava != other.typeJava)
			return false;
		return true;
	}
	
	
}