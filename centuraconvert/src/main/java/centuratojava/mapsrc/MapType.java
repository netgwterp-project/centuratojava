package centuratojava.mapsrc;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import centuratojava.NamesUtils;
import centuratojava.log.Logger;

public class MapType implements Serializable {

	private static long idStart = System.currentTimeMillis()*-1;

	@JsonIgnore
	private static final long serialVersionUID = -1924247056533557386L;

	@JsonIgnore
	private static Map<String, MapType> mapsType = new HashMap<>();
	@JsonIgnore
	private static MapType mapTypeAPI;

	public static MapType getInstanceAPI() {
		if (mapTypeAPI == null) {
			mapTypeAPI = readMapTypeAPI("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\api\\api","CenturaAPI");
		}
		return mapTypeAPI;
	}

	@JsonIgnore
	public static MapType getInstance(String fileMap, String name) {
		getInstanceAPI();

		MapType mapType = mapsType.get(fileMap);

		if (mapType == null) {
			mapType = readMapTypeAPI(fileMap, name);
			if(mapType.getName()==null){
				mapType.setName(name);
				mapType.save();
			}
			mapsType.put(fileMap, mapType);
		}
		return mapType;
	}

	@JsonIgnore
	public static MapType getInstanceName(String name) {
		getInstanceAPI();
		List<MapType> collect = mapsType.values().stream().filter(mapsType -> mapsType.getName().equals(name))
				.collect(Collectors.toList());
		if (collect.size() == 1)
			return collect.get(0);
		return null;
	}

	@JsonIgnore
	public static MapType[] getAllInstance() {
		return mapsType.values().toArray(new MapType[mapsType.values().size()]);
	}

	public static MapType readMapTypeAPI(String fileMap, String name) {
		MapType mapType = null;
		String fileName = FilenameUtils.removeExtension(fileMap) + ".mapsrc.json";
		File file = new File(fileName);
		if (file.exists()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				mapType = mapper.readValue(file, MapType.class);
				mapType.buildReverse();
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (mapType == null) {
			mapType = new MapType();
			mapType.setFileMap(fileMap);
			mapType.setName(name);
		}
		return mapType;
	}

	private String fileMap;
	private String name;
	@JsonIgnore
	private Map<TypeJavaEnum, Map<String, TypeInfo>> mapTypeJava = new ConcurrentHashMap<>();
	@JsonIgnore
	private Map<String, Map<TypeJavaEnum, String>> mapReverse = new ConcurrentHashMap<>();
	private List<String> dependency = new ArrayList<String>();
	private String packageBase;
	private Set<TypeInfo> childrens = new TreeSet<TypeInfo>();
	

	protected MapType() {
	}

	public String getFileMap() {
		return fileMap;
	}

	public void setFileMap(String fileMap) {
		this.fileMap = fileMap;
	}
	
	@JsonIgnore
	public TypeInfo addChildren(String type, TypeJavaEnum typeJava, String typeSrc) {
		if (type == null)
			return null;
		TypeInfo typeInfo = getChildren(type, typeJava);
		if (typeInfo == null) {
			typeInfo = new TypeInfo();
			typeInfo.setId(MapType.genId());
			typeInfo.setNameJava(type);
			typeInfo.setTypeJava(typeJava);
			typeInfo.setMapType(this);
			addChildren(typeInfo);
		}
		typeInfo.getNameCDK().add(typeSrc);
		return typeInfo;
	}
	
	@JsonIgnore
	public TypeInfo addChildren(TypeInfo typeInfo) {
		typeInfo.setMapType(this);
		if(!getChildrens().contains(typeInfo))
			getChildrens().add(typeInfo);
		return typeInfo;
	}
	
	@JsonIgnore
	public TypeInfo getChildren(String type, TypeJavaEnum typeJavaEnum) {
		if (type == null)
			return null;
		Optional<TypeInfo> findFirst = getChildrens().stream().filter(t -> t.getNameJava().equals(type) && t.getTypeJava().equals(typeJavaEnum)).findFirst();
		TypeInfo typeInfo = null;
		if (findFirst != null && findFirst.isPresent())
			typeInfo = findFirst.get();
		return typeInfo;
	}

	@JsonIgnore
	public TypeInfo addType(String type, TypeJavaEnum typeJava, String typeSrc) {
		if (type == null)
			return null;
		TypeInfo typeInfo = getType(type, typeJava);
		if (typeInfo == null) {
			typeInfo = getTypeFromSrc(typeSrc, typeJava);
		}
		if (typeInfo == null) {
			typeInfo = new TypeInfo();
			typeInfo.setId(genId());
			typeInfo.setNameJava(type);
			typeInfo.setTypeJava(typeJava);
			Map<String, TypeInfo> map = mapTypeJava.get(typeJava);
			if (map == null) {
				map = new ConcurrentHashMap<>();
				mapTypeJava.put(typeJava, map);
			}
			typeInfo.setMapType(this);
			map.put(typeInfo.getId().toString(), typeInfo);
		}
		typeInfo.getNameCDK().add(typeSrc);
		save();
		buildReverse();
		return typeInfo;
	}

	protected static synchronized Integer genId() {
		return (int) idStart++;
	}

	@JsonIgnore
	@Deprecated
	public TypeInfo addTypeSrc(String typeSrc) {
		return addTypeSrc(typeSrc, TypeJavaEnum.CLASS);
	}

	@JsonIgnore
	public TypeInfo addTypeSrc(String typeSrc, TypeJavaEnum typeJava) {
		String type = NamesUtils.normalizeClassName(NamesUtils.normalizeName(typeSrc));
		return addType(type, typeJava, typeSrc);
	}

	@JsonIgnore
	public TypeInfo getType(Integer id) {
		Set<TypeJavaEnum> keySet = Collections.unmodifiableSet(mapTypeJava.keySet());
		for (TypeJavaEnum key : keySet) {
			Map<String, TypeInfo> map = mapTypeJava.get(key);
			Set<String> keySet1 = Collections.unmodifiableSet(map.keySet());
			for (String key2 : keySet1) {
				TypeInfo typeInfo = map.get(key2);
				if (typeInfo.getId().equals(id))
					return typeInfo;
			}
		}
		return null;
	}

	@JsonIgnore
	public TypeInfo getType(String type, TypeJavaEnum typeJavaEnum) {
		if (type == null)
			return null;
		Map<String, TypeInfo> map = mapTypeJava.get(typeJavaEnum);
		TypeInfo typeInfo = null;
		if (map != null)
			typeInfo = map.get(type);
		if (typeInfo == null && this != mapTypeAPI)
			typeInfo = mapTypeAPI.getType(type, typeJavaEnum);
		return typeInfo;
	}

	@JsonIgnore
	@Deprecated
	public TypeInfo getTypeFromSrc(String typeSrc, boolean add) {
		return getTypeFromSrc(typeSrc, TypeJavaEnum.CLASS, add);
	}

	@JsonIgnore
	public TypeInfo getTypeFromSrc(String typeSrc, TypeJavaEnum typeJavaEnum, boolean add) {
		TypeInfo typeInfo = null;
		String type = getTypeReverse(typeSrc, typeJavaEnum);
		if (type != null) {
			typeInfo = getType(type, typeJavaEnum);
		} else if (add) {
			typeInfo = addTypeSrc(typeSrc, typeJavaEnum);
		}
		return typeInfo;

	}

	public String getTypeReverse(String typeSrc, TypeJavaEnum typeJavaEnum) {
		if (typeSrc == null)
			return null;
		Map<TypeJavaEnum, String> map = mapReverse.get(typeSrc);
		String type = null;
		if (map != null) {
			type = map.get(typeJavaEnum);
		}
		if (type == null && this != mapTypeAPI) {
			type = mapTypeAPI.getTypeReverse(typeSrc, typeJavaEnum);
		}
		return type;
	}

	@JsonIgnore
	@Deprecated
	public TypeInfo getTypeFromSrc(String typeSrc) {
		return getTypeFromSrc(typeSrc, TypeJavaEnum.CLASS, false);
	}

	@JsonIgnore
	public TypeInfo getTypeFromSrc(String typeSrc, TypeJavaEnum typeJavaEnum) {
		return getTypeFromSrc(typeSrc, typeJavaEnum, false);
	}

	@JsonIgnore
	public void removeAllTypes() {
		Set<TypeJavaEnum> keySet = Collections.unmodifiableSet(mapTypeJava.keySet());
		for (TypeJavaEnum key : keySet) {
			Map<String, TypeInfo> map = mapTypeJava.get(key);
			if (map != null) {
				Set<String> keySet2 = Collections.unmodifiableSet(map.keySet());
				for (String string : keySet2) {
					TypeInfo typeInfo = map.get(string);
					if (!typeInfo.isFixed()) {
						map.remove(string);
					}
				}
				if (map.size() == 0)
					mapTypeJava.remove(key);
			}
		}
		buildReverse();
	}

	@JsonIgnore
	public void buildReverse() {
		mapReverse.clear();
		Set<TypeJavaEnum> keySet = Collections.unmodifiableSet(mapTypeJava.keySet());
		for (TypeJavaEnum key : keySet) {
			Map<String, TypeInfo> map = mapTypeJava.get(key);
			Set<String> keySet1 = Collections.unmodifiableSet(map.keySet());
			for (String key2 : keySet1) {
				TypeInfo typeInfo = map.get(key2);
				Set<String> nameCDK = typeInfo.getNameCDK();
				for (String string : nameCDK) {
					Map<TypeJavaEnum, String> map2 = mapReverse.get(string);
					if (map2 == null) {
						map2 = new ConcurrentHashMap<>();
					}
					map2.put(key, key2);
					mapReverse.put(string, map2);
				}
			}
		}
	}
	
	public void save(){
		save(FilenameUtils.removeExtension(getFileMap()) + ".mapsrc.json");
	}

	@JsonIgnore
	public void save(String fileName) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		try {
			mapper.writeValue(new File(fileName), this);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@JsonIgnore
	public List<TypeInfo> listType(TypeJavaEnum typeJava) {
		return listType(typeJava, null);
	}

	@JsonIgnore
	public List<TypeInfo> listType(TypeJavaEnum typeJava, Integer owner) {
		List<TypeInfo> ret = new ArrayList<>();
		getChildrens().stream().forEach(t -> {
			if ((owner == null || t.getOwner().equals(owner)) && t.getTypeJava().equals(typeJava))
				ret.add(t);
		});
		return ret;
	}

	public String getPackageBase() {
		return packageBase;
	}

	public void setPackageBase(String packageBase) {
		this.packageBase = packageBase;
	}

	public Map<TypeJavaEnum, Map<String, TypeInfo>> getMapTypeJava() {
		return mapTypeJava;
	}

	public void setMapTypeJava(Map<TypeJavaEnum, Map<String, TypeInfo>> mapTypeJava) {
		if (mapTypeJava != null) {
			this.mapTypeJava.clear();
			Set<Entry<TypeJavaEnum, Map<String, TypeInfo>>> entrySet = mapTypeJava.entrySet();
			for (Entry<TypeJavaEnum, Map<String, TypeInfo>> entry : entrySet) {
				ConcurrentHashMap<String, TypeInfo> concurrentHashMap = new ConcurrentHashMap<>();
				entry.getValue().entrySet().stream().forEach(e -> {
					e.getValue().setMapType(this);
					if(e.getValue().getId()==null){
						e.getValue().setId(genId());
					}else{
						if(e.getValue().getId() < 0) e.getValue().setId(e.getValue().getId()*-1);
					}
					if(e.getValue().getOwner()!=null && e.getValue().getOwner() < 0) e.getValue().setOwner(e.getValue().getOwner()*-1);
					concurrentHashMap.put(e.getValue().getId().toString(), e.getValue());
				});
				this.mapTypeJava.put(entry.getKey(), concurrentHashMap);
			}
			buildReverse();
		}
	}

	public static String getFullJavaName(String string, TypeJavaEnum interfaceinclude) {
		TypeInfo type = getTypeFromSrcAll(string, interfaceinclude);
		if (type != null)
			return getFullJavaName(type);
		return string;
	}

	public static String getFullOwnerJavaName(String string, TypeJavaEnum interfaceinclude) {
		return getFullOwnerJavaName(getTypeFromSrcAll(string, interfaceinclude));
	}

	public static String getFullOwnerJavaName(TypeInfo type) {
		if (type != null && type.getOwnerJava() != null)
			return getFullJavaName(type.getOwnerJava().getNameJava(), type.getOwnerJava().getTypeJava());
		return null;
	}

	public static String getFullOwnerPackageName(String string, TypeJavaEnum interfaceinclude) {
		return getFullOwnerPackageName(getTypeFromSrcAll(string, interfaceinclude));
	}

	public static String getFullOwnerPackageName(TypeInfo type) {
		if (type != null && type.getOwnerJava() != null)
			return getFullPackageName(type.getOwnerJava().getNameJava(), type.getOwnerJava().getTypeJava());
		return null;
	}

	public static String getFullJavaName(TypeInfo type) {
		if (type != null)
			return getFullPackageName(type) + '.' + type.getNameJava();
		return "";
	}

	public static TypeInfo getTypeFromSrcAll(String string, TypeJavaEnum interfaceinclude) {
		MapType[] allInstance = getAllInstance();
		for (MapType mapType : allInstance) {
			TypeInfo type = mapType.getTypeFromSrc(string, interfaceinclude);
			if (type != null)
				return type;
		}
		return null;
	}

	public static String getFullPackageName(String string, TypeJavaEnum interfaceinclude) {
		TypeInfo type = getTypeFromSrcAll(string, interfaceinclude);
		if (type != null)
			return getFullPackageName(type);
		return string;
	}

	public static String getFullPackageName(TypeInfo type) {
		try {
			if (type != null)
				return type.getMapType().getPackageBase()
						+ (type.getPackageJava() != null && !"".equals(type.getPackageJava())
								? '.' + type.getPackageJava() : "");
		} catch (Exception e) {
			Logger.error(e.getLocalizedMessage());
		}
		return "";
	}

	public List<String> getDependency() {
		return dependency;
	}

	public void setDependency(List<String> dependency) {
		this.dependency = dependency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<TypeInfo> getChildrens() {
		return childrens;
	}

	public void setChildrens(Set<TypeInfo> childrens) {
		this.childrens.clear();
		if(childrens!=null){
			for (TypeInfo typeInfo : childrens) {
				addChildren(typeInfo);
			}
		}
	}
}
