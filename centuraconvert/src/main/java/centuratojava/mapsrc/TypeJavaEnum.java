package centuratojava.mapsrc;

import javax.lang.model.element.Modifier;

public enum TypeJavaEnum {

	BASETYPE, 
	INTERFACE(Modifier.PUBLIC),
	INTERFACEINCLUDE(Modifier.PUBLIC), 
	CONSTANT(Modifier.PUBLIC,Modifier.STATIC,Modifier.FINAL),
	CLASS(Modifier.PUBLIC), 
	EMUN(Modifier.PUBLIC), 
	GLOBALFUNCTION(Modifier.PUBLIC,Modifier.STATIC,Modifier.FINAL),
	EXTERNALFUNCTION, 
	STATICFUNCTION, 
	STATICVAR, 
	VAR, 
	PARAMETER, 
	MESSAGEACTION, 
	FUNCTION, 
	TOKEN, 
	ANNOTATION;

	private Modifier[] modifier;

	private TypeJavaEnum(Modifier... modifier) {
		this.setModifier(modifier);
	}
	

	public Modifier[] getModifier() {
		return modifier;
	}

	public void setModifier(Modifier[] modifier) {
		this.modifier = modifier;
	}
}
