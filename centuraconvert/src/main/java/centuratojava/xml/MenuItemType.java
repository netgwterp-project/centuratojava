package centuratojava.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MenuItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="MenuItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}BaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}Action" minOccurs="0"/&gt;
 *         &lt;element ref="{}MenuItem" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MenuItemType", propOrder = {
    "action",
    "menuItem"
})
@Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
public class MenuItemType
    extends BaseType
    implements Serializable
{

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    private final static long serialVersionUID = 12345678L;
    @XmlElement(name = "Action")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected ItemType action;
    @XmlElement(name = "MenuItem")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<MenuItemType> menuItem;

    /**
     * Obtém o valor da propriedade action.
     * 
     * @return
     *     possible object is
     *     {@link ItemType }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public ItemType getAction() {
        return action;
    }

    /**
     * Define o valor da propriedade action.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setAction(ItemType value) {
        this.action = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetAction() {
        return (this.action!= null);
    }

    /**
     * Gets the value of the menuItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the menuItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMenuItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MenuItemType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<MenuItemType> getMenuItem() {
        if (menuItem == null) {
            menuItem = new ArrayList<MenuItemType>();
        }
        return this.menuItem;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetMenuItem() {
        return ((this.menuItem!= null)&&(!this.menuItem.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetMenuItem() {
        this.menuItem = null;
    }

}
