package centuratojava.xml;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de scopeType.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="scopeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="local"/&gt;
 *     &lt;enumeration value="instance"/&gt;
 *     &lt;enumeration value="static"/&gt;
 *     &lt;enumeration value="global"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "scopeType")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
public enum ScopeType {

    @XmlEnumValue("local")
    LOCAL("local"),
    @XmlEnumValue("instance")
    INSTANCE("instance"),
    @XmlEnumValue("static")
    STATIC("static"),
    @XmlEnumValue("global")
    GLOBAL("global");
    private final String value;

    ScopeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ScopeType fromValue(String v) {
        for (ScopeType c: ScopeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
