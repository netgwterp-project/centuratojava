package centuratojava.xml;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de libraryTypeType.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="libraryTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DLL"/&gt;
 *     &lt;enumeration value="APD"/&gt;
 *     &lt;enumeration value="APL"/&gt;
 *     &lt;enumeration value="APT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "libraryTypeType")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
public enum LibraryTypeType {

    DLL,
    APD,
    APL,
    APT;

    public String value() {
        return name();
    }

    public static LibraryTypeType fromValue(String v) {
        return valueOf(v);
    }

}
