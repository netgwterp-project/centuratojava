package centuratojava.xml;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the centuratojava.xml package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Application_QNAME = new QName("", "Application");
    private final static QName _Package_QNAME = new QName("", "Package");
    private final static QName _LibraryInfo_QNAME = new QName("", "LibraryInfo");
    private final static QName _Dependency_QNAME = new QName("", "Dependency");
    private final static QName _Library_QNAME = new QName("", "Library");
    private final static QName _ExternalLibrary_QNAME = new QName("", "ExternalLibrary");
    private final static QName _Format_QNAME = new QName("", "Format");
    private final static QName _WindowDef_QNAME = new QName("", "WindowDef");
    private final static QName _Constant_QNAME = new QName("", "Constant");
    private final static QName _Resource_QNAME = new QName("", "Resource");
    private final static QName _Enumeration_QNAME = new QName("", "Enumeration");
    private final static QName _Variable_QNAME = new QName("", "Variable");
    private final static QName _WindowVariable_QNAME = new QName("", "WindowVariable");
    private final static QName _StaticVariable_QNAME = new QName("", "StaticVariable");
    private final static QName _Function_QNAME = new QName("", "Function");
    private final static QName _Parameter_QNAME = new QName("", "Parameter");
    private final static QName _Return_QNAME = new QName("", "Return");
    private final static QName _WindowParameter_QNAME = new QName("", "WindowParameter");
    private final static QName _BaseClasse_QNAME = new QName("", "BaseClasse");
    private final static QName _Classe_QNAME = new QName("", "Classe");
    private final static QName _TopLevelWindow_QNAME = new QName("", "TopLevelWindow");
    private final static QName _Menu_QNAME = new QName("", "Menu");
    private final static QName _NamedMenu_QNAME = new QName("", "NamedMenu");
    private final static QName _MenuItem_QNAME = new QName("", "MenuItem");
    private final static QName _MessageAction_QNAME = new QName("", "MessageAction");
    private final static QName _Action_QNAME = new QName("", "Action");
    private final static QName _Item_QNAME = new QName("", "Item");
    private final static QName _COMAttribute_QNAME = new QName("", "COMAttribute");
    private final static QName _ChildWindow_QNAME = new QName("", "ChildWindow");
    private final static QName _ToolBar_QNAME = new QName("", "ToolBar");
    private final static QName _Code_QNAME = new QName("", "Code");
    private final static QName _JavaMap_QNAME = new QName("", "JavaMap");
    private final static QName _Property_QNAME = new QName("", "Property");
    private final static QName _ListInit_QNAME = new QName("", "ListInit");
    private final static QName _Statement_QNAME = new QName("", "Statement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: centuratojava.xml
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ApplicationType }
     * 
     */
    public ApplicationType createApplicationType() {
        return new ApplicationType();
    }

    /**
     * Create an instance of {@link PackageType }
     * 
     */
    public PackageType createPackageType() {
        return new PackageType();
    }

    /**
     * Create an instance of {@link LibraryInfoType }
     * 
     */
    public LibraryInfoType createLibraryInfoType() {
        return new LibraryInfoType();
    }

    /**
     * Create an instance of {@link LibraryType }
     * 
     */
    public LibraryType createLibraryType() {
        return new LibraryType();
    }

    /**
     * Create an instance of {@link ExternalLibraryType }
     * 
     */
    public ExternalLibraryType createExternalLibraryType() {
        return new ExternalLibraryType();
    }

    /**
     * Create an instance of {@link BaseType }
     * 
     */
    public BaseType createBaseType() {
        return new BaseType();
    }

    /**
     * Create an instance of {@link PropertyType }
     * 
     */
    public PropertyType createPropertyType() {
        return new PropertyType();
    }

    /**
     * Create an instance of {@link EnumerationType }
     * 
     */
    public EnumerationType createEnumerationType() {
        return new EnumerationType();
    }

    /**
     * Create an instance of {@link VariableType }
     * 
     */
    public VariableType createVariableType() {
        return new VariableType();
    }

    /**
     * Create an instance of {@link FunctionType }
     * 
     */
    public FunctionType createFunctionType() {
        return new FunctionType();
    }

    /**
     * Create an instance of {@link ClasseType }
     * 
     */
    public ClasseType createClasseType() {
        return new ClasseType();
    }

    /**
     * Create an instance of {@link MenuType }
     * 
     */
    public MenuType createMenuType() {
        return new MenuType();
    }

    /**
     * Create an instance of {@link MenuItemType }
     * 
     */
    public MenuItemType createMenuItemType() {
        return new MenuItemType();
    }

    /**
     * Create an instance of {@link ActionType }
     * 
     */
    public ActionType createActionType() {
        return new ActionType();
    }

    /**
     * Create an instance of {@link ItemType }
     * 
     */
    public ItemType createItemType() {
        return new ItemType();
    }

    /**
     * Create an instance of {@link JavaMapType }
     * 
     */
    public JavaMapType createJavaMapType() {
        return new JavaMapType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Application")
    public JAXBElement<ApplicationType> createApplication(ApplicationType value) {
        return new JAXBElement<ApplicationType>(_Application_QNAME, ApplicationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Package")
    public JAXBElement<PackageType> createPackage(PackageType value) {
        return new JAXBElement<PackageType>(_Package_QNAME, PackageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LibraryInfoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LibraryInfo")
    public JAXBElement<LibraryInfoType> createLibraryInfo(LibraryInfoType value) {
        return new JAXBElement<LibraryInfoType>(_LibraryInfo_QNAME, LibraryInfoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LibraryInfoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Dependency")
    public JAXBElement<LibraryInfoType> createDependency(LibraryInfoType value) {
        return new JAXBElement<LibraryInfoType>(_Dependency_QNAME, LibraryInfoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LibraryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Library")
    public JAXBElement<LibraryType> createLibrary(LibraryType value) {
        return new JAXBElement<LibraryType>(_Library_QNAME, LibraryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExternalLibraryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ExternalLibrary")
    public JAXBElement<ExternalLibraryType> createExternalLibrary(ExternalLibraryType value) {
        return new JAXBElement<ExternalLibraryType>(_ExternalLibrary_QNAME, ExternalLibraryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Format")
    public JAXBElement<BaseType> createFormat(BaseType value) {
        return new JAXBElement<BaseType>(_Format_QNAME, BaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WindowDef")
    public JAXBElement<BaseType> createWindowDef(BaseType value) {
        return new JAXBElement<BaseType>(_WindowDef_QNAME, BaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Constant")
    public JAXBElement<PropertyType> createConstant(PropertyType value) {
        return new JAXBElement<PropertyType>(_Constant_QNAME, PropertyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Resource")
    public JAXBElement<BaseType> createResource(BaseType value) {
        return new JAXBElement<BaseType>(_Resource_QNAME, BaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnumerationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Enumeration")
    public JAXBElement<EnumerationType> createEnumeration(EnumerationType value) {
        return new JAXBElement<EnumerationType>(_Enumeration_QNAME, EnumerationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Variable")
    public JAXBElement<VariableType> createVariable(VariableType value) {
        return new JAXBElement<VariableType>(_Variable_QNAME, VariableType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WindowVariable")
    public JAXBElement<VariableType> createWindowVariable(VariableType value) {
        return new JAXBElement<VariableType>(_WindowVariable_QNAME, VariableType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StaticVariable")
    public JAXBElement<VariableType> createStaticVariable(VariableType value) {
        return new JAXBElement<VariableType>(_StaticVariable_QNAME, VariableType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FunctionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Function")
    public JAXBElement<FunctionType> createFunction(FunctionType value) {
        return new JAXBElement<FunctionType>(_Function_QNAME, FunctionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Parameter")
    public JAXBElement<VariableType> createParameter(VariableType value) {
        return new JAXBElement<VariableType>(_Parameter_QNAME, VariableType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Return")
    public JAXBElement<BaseType> createReturn(BaseType value) {
        return new JAXBElement<BaseType>(_Return_QNAME, BaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WindowParameter")
    public JAXBElement<VariableType> createWindowParameter(VariableType value) {
        return new JAXBElement<VariableType>(_WindowParameter_QNAME, VariableType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BaseClasse")
    public JAXBElement<BaseType> createBaseClasse(BaseType value) {
        return new JAXBElement<BaseType>(_BaseClasse_QNAME, BaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClasseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Classe")
    public JAXBElement<ClasseType> createClasse(ClasseType value) {
        return new JAXBElement<ClasseType>(_Classe_QNAME, ClasseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClasseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TopLevelWindow")
    public JAXBElement<ClasseType> createTopLevelWindow(ClasseType value) {
        return new JAXBElement<ClasseType>(_TopLevelWindow_QNAME, ClasseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MenuType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Menu")
    public JAXBElement<MenuType> createMenu(MenuType value) {
        return new JAXBElement<MenuType>(_Menu_QNAME, MenuType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MenuType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NamedMenu")
    public JAXBElement<MenuType> createNamedMenu(MenuType value) {
        return new JAXBElement<MenuType>(_NamedMenu_QNAME, MenuType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MenuItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MenuItem")
    public JAXBElement<MenuItemType> createMenuItem(MenuItemType value) {
        return new JAXBElement<MenuItemType>(_MenuItem_QNAME, MenuItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MessageAction")
    public JAXBElement<ActionType> createMessageAction(ActionType value) {
        return new JAXBElement<ActionType>(_MessageAction_QNAME, ActionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Action")
    public JAXBElement<ItemType> createAction(ItemType value) {
        return new JAXBElement<ItemType>(_Action_QNAME, ItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Item")
    public JAXBElement<ItemType> createItem(ItemType value) {
        return new JAXBElement<ItemType>(_Item_QNAME, ItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "COMAttribute")
    public JAXBElement<BaseType> createCOMAttribute(BaseType value) {
        return new JAXBElement<BaseType>(_COMAttribute_QNAME, BaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClasseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChildWindow")
    public JAXBElement<ClasseType> createChildWindow(ClasseType value) {
        return new JAXBElement<ClasseType>(_ChildWindow_QNAME, ClasseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClasseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ToolBar")
    public JAXBElement<ClasseType> createToolBar(ClasseType value) {
        return new JAXBElement<ClasseType>(_ToolBar_QNAME, ClasseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Code")
    public JAXBElement<ItemType> createCode(ItemType value) {
        return new JAXBElement<ItemType>(_Code_QNAME, ItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JavaMapType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "JavaMap")
    public JAXBElement<JavaMapType> createJavaMap(JavaMapType value) {
        return new JAXBElement<JavaMapType>(_JavaMap_QNAME, JavaMapType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Property")
    public JAXBElement<PropertyType> createProperty(PropertyType value) {
        return new JAXBElement<PropertyType>(_Property_QNAME, PropertyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ListInit")
    public JAXBElement<PropertyType> createListInit(PropertyType value) {
        return new JAXBElement<PropertyType>(_ListInit_QNAME, PropertyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Statement")
    public JAXBElement<String> createStatement(String value) {
        return new JAXBElement<String>(_Statement_QNAME, String.class, null, value);
    }

}
