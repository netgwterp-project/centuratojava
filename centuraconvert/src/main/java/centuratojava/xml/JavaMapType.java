package centuratojava.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de JavaMapType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="JavaMapType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *       &lt;attribute name="fileSrc" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="javaName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="javaType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="enclosedJavaName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="packageName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="superClass" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="scope" type="{}scopeType" /&gt;
 *       &lt;attribute name="implements" type="{}listType" /&gt;
 *       &lt;attribute name="cdkName" type="{}listType" /&gt;
 *       &lt;attribute name="shadowed" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JavaMapType")
@Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
public class JavaMapType
    implements Serializable
{

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    private final static long serialVersionUID = 12345678L;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String id;
    @XmlAttribute(name = "fileSrc")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String fileSrc;
    @XmlAttribute(name = "javaName")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String javaName;
    @XmlAttribute(name = "javaType")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String javaType;
    @XmlAttribute(name = "enclosedJavaName")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String enclosedJavaName;
    @XmlAttribute(name = "packageName")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String packageName;
    @XmlAttribute(name = "superClass")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String superClass;
    @XmlAttribute(name = "scope")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected ScopeType scope;
    @XmlAttribute(name = "implements")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<String> _implements;
    @XmlAttribute(name = "cdkName")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<String> cdkName;
    @XmlAttribute(name = "shadowed")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected String shadowed;

    /**
     * Obtém o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setId(String value) {
        this.id = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetId() {
        return (this.id!= null);
    }

    /**
     * Obtém o valor da propriedade fileSrc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getFileSrc() {
        return fileSrc;
    }

    /**
     * Define o valor da propriedade fileSrc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setFileSrc(String value) {
        this.fileSrc = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetFileSrc() {
        return (this.fileSrc!= null);
    }

    /**
     * Obtém o valor da propriedade javaName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getJavaName() {
        return javaName;
    }

    /**
     * Define o valor da propriedade javaName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setJavaName(String value) {
        this.javaName = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetJavaName() {
        return (this.javaName!= null);
    }

    /**
     * Obtém o valor da propriedade javaType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getJavaType() {
        return javaType;
    }

    /**
     * Define o valor da propriedade javaType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setJavaType(String value) {
        this.javaType = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetJavaType() {
        return (this.javaType!= null);
    }

    /**
     * Obtém o valor da propriedade enclosedJavaName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getEnclosedJavaName() {
        return enclosedJavaName;
    }

    /**
     * Define o valor da propriedade enclosedJavaName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setEnclosedJavaName(String value) {
        this.enclosedJavaName = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetEnclosedJavaName() {
        return (this.enclosedJavaName!= null);
    }

    /**
     * Obtém o valor da propriedade packageName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getPackageName() {
        return packageName;
    }

    /**
     * Define o valor da propriedade packageName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setPackageName(String value) {
        this.packageName = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetPackageName() {
        return (this.packageName!= null);
    }

    /**
     * Obtém o valor da propriedade superClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getSuperClass() {
        return superClass;
    }

    /**
     * Define o valor da propriedade superClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setSuperClass(String value) {
        this.superClass = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetSuperClass() {
        return (this.superClass!= null);
    }

    /**
     * Obtém o valor da propriedade scope.
     * 
     * @return
     *     possible object is
     *     {@link ScopeType }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public ScopeType getScope() {
        return scope;
    }

    /**
     * Define o valor da propriedade scope.
     * 
     * @param value
     *     allowed object is
     *     {@link ScopeType }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setScope(ScopeType value) {
        this.scope = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetScope() {
        return (this.scope!= null);
    }

    /**
     * Gets the value of the implements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the implements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImplements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<String> getImplements() {
        if (_implements == null) {
            _implements = new ArrayList<String>();
        }
        return this._implements;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetImplements() {
        return ((this._implements!= null)&&(!this._implements.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetImplements() {
        this._implements = null;
    }

    /**
     * Gets the value of the cdkName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cdkName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCdkName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<String> getCdkName() {
        if (cdkName == null) {
            cdkName = new ArrayList<String>();
        }
        return this.cdkName;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetCdkName() {
        return ((this.cdkName!= null)&&(!this.cdkName.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetCdkName() {
        this.cdkName = null;
    }

    /**
     * Obtém o valor da propriedade shadowed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public String getShadowed() {
        return shadowed;
    }

    /**
     * Define o valor da propriedade shadowed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setShadowed(String value) {
        this.shadowed = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetShadowed() {
        return (this.shadowed!= null);
    }

}
