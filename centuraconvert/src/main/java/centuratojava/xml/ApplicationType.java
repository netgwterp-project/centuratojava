package centuratojava.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ApplicationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ApplicationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}BaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}WindowDef" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Format" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Library" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Constant" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Resource" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Enumeration" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}StaticVariable" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Function" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Classe" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}TopLevelWindow" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}NamedMenu" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}ExternalLibrary" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}MessageAction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlRootElement(name="Application")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationType", propOrder = {
    "windowDef",
    "format",
    "library",
    "constant",
    "resource",
    "enumeration",
    "staticVariable",
    "function",
    "classe",
    "topLevelWindow",
    "namedMenu",
    "externalLibrary",
    "messageAction"
})
@Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
public class ApplicationType
    extends BaseType
    implements Serializable
{

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    private final static long serialVersionUID = 12345678L;
    @XmlElement(name = "WindowDef")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<BaseType> windowDef;
    @XmlElement(name = "Format")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<BaseType> format;
    @XmlElement(name = "Library")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<LibraryType> library;
    @XmlElement(name = "Constant")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<PropertyType> constant;
    @XmlElement(name = "Resource")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<BaseType> resource;
    @XmlElement(name = "Enumeration")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<EnumerationType> enumeration;
    @XmlElement(name = "StaticVariable")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<VariableType> staticVariable;
    @XmlElement(name = "Function")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<FunctionType> function;
    @XmlElement(name = "Classe")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<ClasseType> classe;
    @XmlElement(name = "TopLevelWindow")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<ClasseType> topLevelWindow;
    @XmlElement(name = "NamedMenu")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<MenuType> namedMenu;
    @XmlElement(name = "ExternalLibrary")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<ExternalLibraryType> externalLibrary;
    @XmlElement(name = "MessageAction")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<ActionType> messageAction;

    /**
     * Gets the value of the windowDef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the windowDef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWindowDef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaseType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<BaseType> getWindowDef() {
        if (windowDef == null) {
            windowDef = new ArrayList<BaseType>();
        }
        return this.windowDef;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetWindowDef() {
        return ((this.windowDef!= null)&&(!this.windowDef.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetWindowDef() {
        this.windowDef = null;
    }

    /**
     * Gets the value of the format property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the format property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaseType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<BaseType> getFormat() {
        if (format == null) {
            format = new ArrayList<BaseType>();
        }
        return this.format;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetFormat() {
        return ((this.format!= null)&&(!this.format.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetFormat() {
        this.format = null;
    }

    /**
     * Gets the value of the library property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the library property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLibrary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LibraryType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<LibraryType> getLibrary() {
        if (library == null) {
            library = new ArrayList<LibraryType>();
        }
        return this.library;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetLibrary() {
        return ((this.library!= null)&&(!this.library.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetLibrary() {
        this.library = null;
    }

    /**
     * Gets the value of the constant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the constant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConstant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<PropertyType> getConstant() {
        if (constant == null) {
            constant = new ArrayList<PropertyType>();
        }
        return this.constant;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetConstant() {
        return ((this.constant!= null)&&(!this.constant.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetConstant() {
        this.constant = null;
    }

    /**
     * Gets the value of the resource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaseType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<BaseType> getResource() {
        if (resource == null) {
            resource = new ArrayList<BaseType>();
        }
        return this.resource;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetResource() {
        return ((this.resource!= null)&&(!this.resource.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetResource() {
        this.resource = null;
    }

    /**
     * Gets the value of the enumeration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enumeration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnumeration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnumerationType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<EnumerationType> getEnumeration() {
        if (enumeration == null) {
            enumeration = new ArrayList<EnumerationType>();
        }
        return this.enumeration;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetEnumeration() {
        return ((this.enumeration!= null)&&(!this.enumeration.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetEnumeration() {
        this.enumeration = null;
    }

    /**
     * Gets the value of the staticVariable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the staticVariable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStaticVariable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VariableType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<VariableType> getStaticVariable() {
        if (staticVariable == null) {
            staticVariable = new ArrayList<VariableType>();
        }
        return this.staticVariable;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetStaticVariable() {
        return ((this.staticVariable!= null)&&(!this.staticVariable.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetStaticVariable() {
        this.staticVariable = null;
    }

    /**
     * Gets the value of the function property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the function property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFunction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FunctionType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<FunctionType> getFunction() {
        if (function == null) {
            function = new ArrayList<FunctionType>();
        }
        return this.function;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetFunction() {
        return ((this.function!= null)&&(!this.function.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetFunction() {
        this.function = null;
    }

    /**
     * Gets the value of the classe property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the classe property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClasse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClasseType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<ClasseType> getClasse() {
        if (classe == null) {
            classe = new ArrayList<ClasseType>();
        }
        return this.classe;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetClasse() {
        return ((this.classe!= null)&&(!this.classe.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetClasse() {
        this.classe = null;
    }

    /**
     * Gets the value of the topLevelWindow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the topLevelWindow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTopLevelWindow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClasseType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<ClasseType> getTopLevelWindow() {
        if (topLevelWindow == null) {
            topLevelWindow = new ArrayList<ClasseType>();
        }
        return this.topLevelWindow;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetTopLevelWindow() {
        return ((this.topLevelWindow!= null)&&(!this.topLevelWindow.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetTopLevelWindow() {
        this.topLevelWindow = null;
    }

    /**
     * Gets the value of the namedMenu property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the namedMenu property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNamedMenu().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MenuType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<MenuType> getNamedMenu() {
        if (namedMenu == null) {
            namedMenu = new ArrayList<MenuType>();
        }
        return this.namedMenu;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetNamedMenu() {
        return ((this.namedMenu!= null)&&(!this.namedMenu.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetNamedMenu() {
        this.namedMenu = null;
    }

    /**
     * Gets the value of the externalLibrary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the externalLibrary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExternalLibrary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExternalLibraryType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<ExternalLibraryType> getExternalLibrary() {
        if (externalLibrary == null) {
            externalLibrary = new ArrayList<ExternalLibraryType>();
        }
        return this.externalLibrary;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetExternalLibrary() {
        return ((this.externalLibrary!= null)&&(!this.externalLibrary.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetExternalLibrary() {
        this.externalLibrary = null;
    }

    /**
     * Gets the value of the messageAction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageAction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageAction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActionType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<ActionType> getMessageAction() {
        if (messageAction == null) {
            messageAction = new ArrayList<ActionType>();
        }
        return this.messageAction;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetMessageAction() {
        return ((this.messageAction!= null)&&(!this.messageAction.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetMessageAction() {
        this.messageAction = null;
    }

}
