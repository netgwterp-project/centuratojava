package centuratojava.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ClasseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ClasseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}BaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}BaseClasse" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}StaticVariable" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Variable" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}Function" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}MessageAction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}ListInit" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}ChildWindow" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}ToolBar" minOccurs="0"/&gt;
 *         &lt;element ref="{}Menu" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}WindowParameter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}WindowVariable" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClasseType", propOrder = {
    "baseClasse",
    "staticVariable",
    "variable",
    "function",
    "messageAction",
    "listInit",
    "childWindow",
    "toolBar",
    "menu",
    "windowParameter",
    "windowVariable"
})
@Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
public class ClasseType
    extends BaseType
    implements Serializable
{

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    private final static long serialVersionUID = 12345678L;
    @XmlElement(name = "BaseClasse")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<BaseType> baseClasse;
    @XmlElement(name = "StaticVariable")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<VariableType> staticVariable;
    @XmlElement(name = "Variable")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<VariableType> variable;
    @XmlElement(name = "Function")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<FunctionType> function;
    @XmlElement(name = "MessageAction")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<ActionType> messageAction;
    @XmlElement(name = "ListInit")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<PropertyType> listInit;
    @XmlElement(name = "ChildWindow")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<ClasseType> childWindow;
    @XmlElement(name = "ToolBar")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected ClasseType toolBar;
    @XmlElement(name = "Menu")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<MenuType> menu;
    @XmlElement(name = "WindowParameter")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<VariableType> windowParameter;
    @XmlElement(name = "WindowVariable")
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    protected List<VariableType> windowVariable;

    /**
     * Gets the value of the baseClasse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the baseClasse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBaseClasse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaseType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<BaseType> getBaseClasse() {
        if (baseClasse == null) {
            baseClasse = new ArrayList<BaseType>();
        }
        return this.baseClasse;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetBaseClasse() {
        return ((this.baseClasse!= null)&&(!this.baseClasse.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetBaseClasse() {
        this.baseClasse = null;
    }

    /**
     * Gets the value of the staticVariable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the staticVariable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStaticVariable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VariableType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<VariableType> getStaticVariable() {
        if (staticVariable == null) {
            staticVariable = new ArrayList<VariableType>();
        }
        return this.staticVariable;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetStaticVariable() {
        return ((this.staticVariable!= null)&&(!this.staticVariable.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetStaticVariable() {
        this.staticVariable = null;
    }

    /**
     * Gets the value of the variable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the variable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVariable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VariableType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<VariableType> getVariable() {
        if (variable == null) {
            variable = new ArrayList<VariableType>();
        }
        return this.variable;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetVariable() {
        return ((this.variable!= null)&&(!this.variable.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetVariable() {
        this.variable = null;
    }

    /**
     * Gets the value of the function property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the function property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFunction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FunctionType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<FunctionType> getFunction() {
        if (function == null) {
            function = new ArrayList<FunctionType>();
        }
        return this.function;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetFunction() {
        return ((this.function!= null)&&(!this.function.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetFunction() {
        this.function = null;
    }

    /**
     * Gets the value of the messageAction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageAction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageAction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActionType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<ActionType> getMessageAction() {
        if (messageAction == null) {
            messageAction = new ArrayList<ActionType>();
        }
        return this.messageAction;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetMessageAction() {
        return ((this.messageAction!= null)&&(!this.messageAction.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetMessageAction() {
        this.messageAction = null;
    }

    /**
     * Gets the value of the listInit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listInit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListInit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<PropertyType> getListInit() {
        if (listInit == null) {
            listInit = new ArrayList<PropertyType>();
        }
        return this.listInit;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetListInit() {
        return ((this.listInit!= null)&&(!this.listInit.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetListInit() {
        this.listInit = null;
    }

    /**
     * Gets the value of the childWindow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the childWindow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChildWindow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClasseType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<ClasseType> getChildWindow() {
        if (childWindow == null) {
            childWindow = new ArrayList<ClasseType>();
        }
        return this.childWindow;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetChildWindow() {
        return ((this.childWindow!= null)&&(!this.childWindow.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetChildWindow() {
        this.childWindow = null;
    }

    /**
     * Obtém o valor da propriedade toolBar.
     * 
     * @return
     *     possible object is
     *     {@link ClasseType }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public ClasseType getToolBar() {
        return toolBar;
    }

    /**
     * Define o valor da propriedade toolBar.
     * 
     * @param value
     *     allowed object is
     *     {@link ClasseType }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void setToolBar(ClasseType value) {
        this.toolBar = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetToolBar() {
        return (this.toolBar!= null);
    }

    /**
     * Gets the value of the menu property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the menu property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMenu().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MenuType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<MenuType> getMenu() {
        if (menu == null) {
            menu = new ArrayList<MenuType>();
        }
        return this.menu;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetMenu() {
        return ((this.menu!= null)&&(!this.menu.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetMenu() {
        this.menu = null;
    }

    /**
     * Gets the value of the windowParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the windowParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWindowParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VariableType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<VariableType> getWindowParameter() {
        if (windowParameter == null) {
            windowParameter = new ArrayList<VariableType>();
        }
        return this.windowParameter;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetWindowParameter() {
        return ((this.windowParameter!= null)&&(!this.windowParameter.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetWindowParameter() {
        this.windowParameter = null;
    }

    /**
     * Gets the value of the windowVariable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the windowVariable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWindowVariable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VariableType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public List<VariableType> getWindowVariable() {
        if (windowVariable == null) {
            windowVariable = new ArrayList<VariableType>();
        }
        return this.windowVariable;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public boolean isSetWindowVariable() {
        return ((this.windowVariable!= null)&&(!this.windowVariable.isEmpty()));
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2018-03-10T09:58:31-03:00", comments = "JAXB RI v2.2.11")
    public void unsetWindowVariable() {
        this.windowVariable = null;
    }

}
