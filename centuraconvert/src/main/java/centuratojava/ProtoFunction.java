package centuratojava;

import com.squareup.javapoet.MethodSpec;

public class ProtoFunction extends ProtoDec<ProtoFunction> {
	private ProtoParam[] parans;
	private ProtoType returnType;
	private MethodSpec methodSpec;
	private MethodSpec methodSpec2;
	public ProtoParam[] getParans() {
		return parans;
	}
	public void setParans(ProtoParam[] parans) {
		this.parans = parans;
	}
	public ProtoType getReturnType() {
		return returnType;
	}
	public void setReturnType(ProtoType returnType) {
		this.returnType = returnType;
	}
	public void setMethodSpec(MethodSpec methodSpec) {
		this.methodSpec = methodSpec;
		
	}
	
	public MethodSpec getMethodSpec() {
		return methodSpec;
	}
	public void setMethodSpec2(MethodSpec methodSpec2) {
		this.methodSpec2 = methodSpec2;
		
	}
	public MethodSpec getMethodSpec2() {
		return methodSpec2;
	}
	public String[] getParamNames() {
		String names[] = new String[parans.length];
		int i = 0;
		for (ProtoParam param : parans) {
			names[i] = param.getName();
			i++;
		}
		return names;
	}
	
	
}
