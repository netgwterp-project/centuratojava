package centuratojava;

public class ProtoType extends ProtoDec<ProtoType>{
	
	//HSTRING ,BOOL,INT,DATETIME,HWND, LPSTR, HARRAY, LPINT, LPHSTRING, HGLOBAL, ATOM,UINT,VOID,LPBYTE,DWORD;
	private ProtoType parent;
	private boolean atomic=false;
	
	public ProtoType getParent() {
		return parent;
	}
	public void setParent(ProtoType parent) {
		this.parent = parent;
	}
	
	@Override
	public void setName(String name) {
		super.setName(name.toUpperCase());
	}
	
	public boolean isAtomic() {
		return atomic;
	}
	public void setAtomic(boolean atomic) {
		this.atomic = atomic;
	}
	
	
	
}
