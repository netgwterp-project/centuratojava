package centuratojava;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class QuickConvert {
	public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		/*ReadAPP readAPP = new ReadAPP(null);
		ApplicationType readXMLApplication = readAPP.readXMLApplication("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\qck\\qwebmsg.xml");
		List<PropertyType> collect2 = readXMLApplication.getConstant().stream().filter(p -> p.getName().equals("__WEB_MSG_BASE")).collect(Collectors.toList());
		MapType mapType = MapType.getInstance("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\qck\\qwebmsg.xml");
		List<TypeInfo> listType = mapType.listType(TypeJavaEnum.CONSTANT);
		List<TypeInfo> collect = listType.stream().filter(t -> t.getNameJava().equals("__WEB_MSG_BASE")).collect(Collectors.toList());
		collect.get(0).setSource(collect2.get(0));
		SourceGen sourceGen = new SourceGen();
		sourceGen.setConstantsType(collect);
		sourceGen.genConstants();*/
		
		ConfigConvert configConvert = new ConfigConvert();
		configConvert.setName("qck");
		//configConvert.setSkipConstants(true);
		configConvert.setPackageNameBase("com.centura.api.qck");
		configConvert.setJavaSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\java");
		configConvert.setMapSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\qck");
		configConvert
				.setSourceFilePrincipal("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\qcksrc\\qck.apl");
		configConvert.setSourceFileCompiled("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\qcksrc\\qck.app");
		configConvert.addLibraryPath("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\qcksrc", null, "apl",
				null, "dlls");
		configConvert.setMapSourceScanFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources");
		configConvert.setClearJavaPackage(true);
		//configConvert.setClearMapSourceFolder(true);
		configConvert.setSkippFase1(true);
		//configConvert.setSkippFase2(true);
		//configConvert.setSkippFase3(true);
		configConvert.setScanPackage(true);
		ReadAPP readAPP = new ReadAPP(configConvert);
		readAPP.processa();
	}
}
