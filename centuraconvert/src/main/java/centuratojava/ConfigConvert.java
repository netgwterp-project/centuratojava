package centuratojava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConfigConvert {
	private List<ConfigPath>	paths					= new ArrayList<>();
	private String				mapSourceFolder;
	private String				packageNameBase;
	private String				sourceFilePrincipal;
	private String				sourceFileCompiled;
	private boolean				skipConstants			= false;
	private boolean				skipStaticVars			= false;
	private String				pathCbi20exe			= "C:\\javadev\\Ctd2000\\cbi20.exe";
	private String				pathgenXML				= "C:\\javadev\\centuratojava\\centuraconvert\\centura\\genxml.exe";
	private String				javaSourceFolder;
	private String				name;
	private boolean				clearMapSourceFolder	= false;
	private boolean				clearJavaPackage		= false;
	private boolean				skippFase1				= false;
	private boolean				skippFase2				= false;
	private boolean				skippFase3				= false;
	private boolean				scanPackage;
	private String				mapSourceScanFolder;

	public void addLibraryPath(String sourceLibDir, String outputFolder, String extensionFilter, String packageName,
			String packageNameNative) {
		ConfigPath configPath = new ConfigPath();
		configPath.setSourceFolder(sourceLibDir);
		configPath.setOutputFolder(
				(outputFolder == null) || "".equals(outputFolder) ? getMapSourceFolder() : outputFolder);
		configPath
				.setExtensionFilter((extensionFilter == null) || "".equals(extensionFilter) ? "apl" : extensionFilter);
		configPath.setPackageName(packageName == null ? getPackageNameBase()
				: packageName.startsWith(".") ? getPackageNameBase() + packageName : packageName);
		configPath.setPackageNameNative(
				(packageNameNative == null) || "".equals(packageNameNative) ? configPath.getPackageName() + ".dlls"
						: configPath.getPackageName() + "." + packageNameNative);
		paths.add(configPath);
	}

	public String getJavaSourceFolder() {
		return javaSourceFolder;
	}

	public List<ConfigPath> getLibraryPaths() {
		return Collections.unmodifiableList(paths);
	}

	public String getMapSourceFolder() {
		return mapSourceFolder;
	}

	public String getMapSourceScanFolder() {
		return mapSourceScanFolder;
	}

	public String getName() {
		return name;
	}

	public String getPackageNameBase() {
		return packageNameBase;
	}

	public String getPathCbi20exe() {
		return pathCbi20exe;
	}

	public String getPathgenXML() {
		return pathgenXML;
	}

	public String getSourceFileCompiled() {
		return sourceFileCompiled;
	}

	public String getSourceFilePrincipal() {
		return sourceFilePrincipal;
	}

	public boolean isClearJavaPackage() {
		return clearJavaPackage;
	}

	public boolean isClearMapSourceFolder() {
		return clearMapSourceFolder;
	}

	public boolean isScanPackage() {
		return scanPackage;
	}

	public boolean isSkipConstants() {
		return skipConstants;
	}

	public boolean isSkippFase1() {
		return skippFase1;
	}

	public boolean isSkippFase2() {
		return skippFase2;
	}

	public boolean isSkippFase3() {
		return skippFase3;
	}

	public void setClearJavaPackage(boolean clearJavaPackage) {
		this.clearJavaPackage = clearJavaPackage;
	}

	public void setClearMapSourceFolder(boolean clearMapSourceFolder) {
		this.clearMapSourceFolder = clearMapSourceFolder;

	}

	public void setJavaSourceFolder(String javaSourceFolder) {
		this.javaSourceFolder = javaSourceFolder;

	}

	public void setMapSourceFolder(String mapSourceFolder) {
		this.mapSourceFolder = mapSourceFolder;
	}

	public void setMapSourceScanFolder(String mapSourceScanFolder) {
		this.mapSourceScanFolder = mapSourceScanFolder;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPackageNameBase(String packageNameBase) {
		this.packageNameBase = packageNameBase;
	}

	public void setPathCbi20exe(String pathCbi20exe) {
		this.pathCbi20exe = pathCbi20exe;
	}

	public void setPathgenXML(String pathgenXML) {
		this.pathgenXML = pathgenXML;
	}

	public void setScanPackage(boolean scanPackage) {
		this.scanPackage = scanPackage;
	}

	public void setSkipConstants(boolean skipConstants) {
		this.skipConstants = skipConstants;
	}

	public void setSkippFase1(boolean skippFase1) {
		this.skippFase1 = skippFase1;
	}

	public void setSkippFase2(boolean skippFase2) {
		this.skippFase2 = skippFase2;
	}

	public void setSkippFase3(boolean skippFase3) {
		this.skippFase3 = skippFase3;
	}

	public void setSourceFileCompiled(String sourceFileCompiled) {
		this.sourceFileCompiled = sourceFileCompiled;
	}

	public void setSourceFilePrincipal(String sourceFilePrincipal) {
		this.sourceFilePrincipal = sourceFilePrincipal;
	}

	public boolean isSkipStaticVars() {
		return skipStaticVars;
	}

	public void setSkipStaticVars(boolean skipStaticVars) {
		this.skipStaticVars = skipStaticVars;
	}
}

class ConfigPath {
	private String	packageName;
	private String	extensionFilter;
	private String	sourceFolder;
	private String	outputFolder;
	private String	packageNameNative;

	public ConfigPath() {
	}

	public ConfigPath(String sourceFolder, String outputFolder) {
		this.sourceFolder = sourceFolder;
		this.outputFolder = outputFolder;
	}

	public String getExtensionFilter() {
		return extensionFilter;
	}

	public String getOutputFolder() {
		return outputFolder;
	}

	public String getPackageName() {
		return packageName;
	}

	public String getPackageNameNative() {
		return packageNameNative;
	}

	public String getSourceFolder() {
		return sourceFolder;
	}

	public void setExtensionFilter(String extensionFilter) {
		this.extensionFilter = extensionFilter;
	}

	public void setOutputFolder(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setPackageNameNative(String packageNameNative) {
		this.packageNameNative = packageNameNative;

	}

	public void setSourceFolder(String sourceFolder) {
		this.sourceFolder = sourceFolder;
	}
}
