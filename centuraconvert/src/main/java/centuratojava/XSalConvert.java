package centuratojava;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class XSalConvert {
	public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		ConfigConvert configConvert = new ConfigConvert();
		configConvert.setName("xsal");
		//configConvert.setSkipConstants(true);
		configConvert.setPackageNameBase("com.centura.api.xsal");
		configConvert.setJavaSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\java");
		configConvert.setMapSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\xsal");
		configConvert
				.setSourceFilePrincipal("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\xsalsrc\\XSal_32.apl");
		configConvert.setSourceFileCompiled("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\xsalsrc\\XSal_32.app");
		configConvert.addLibraryPath("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\xsalsrc", null, "apl",
				null, "dlls");
		configConvert.setMapSourceScanFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources");
		configConvert.setClearJavaPackage(true);
		configConvert.setClearMapSourceFolder(true);
		//configConvert.setSkippFase1(true);
		//configConvert.setSkippFase2(true);
		//configConvert.setSkippFase3(true);
		configConvert.setScanPackage(true);
		ReadAPP readAPP = new ReadAPP(configConvert);
		readAPP.processa();
	}
}
