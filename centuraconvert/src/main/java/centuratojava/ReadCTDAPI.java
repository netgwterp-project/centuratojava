package centuratojava;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.lang.model.element.Modifier;

import org.apache.commons.lang.StringUtils;

import com.centura.api.type.CBoolean;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.CodeBlock.Builder;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

public class ReadCTDAPI {

	public static void main(String[] args) throws IOException {
		ReadCTDAPI readCTDAPI = new ReadCTDAPI();
		readCTDAPI.readSource();
		readCTDAPI.writeSource();
	}

	String dest = "../centuraapi/src/main/java";
	String pac = "com.centura.api";

	@SuppressWarnings("unused")
	private void writeSource() throws IOException {
		if (false) {
			List<FieldSpec> fields = new ArrayList<FieldSpec>();
			for (ProtoConstant protoConstant : constants) {
				try {
					if (isInteger(protoConstant.getValue())) {
						FieldSpec fieldSpec = FieldSpec
								.builder(TypeName.INT, protoConstant.getName(), Modifier.STATIC, Modifier.FINAL,
										Modifier.PUBLIC)
								.initializer(protoConstant.getValue()).addJavadoc(protoConstant.getSource()).build();
						protoConstant.setFieldSpec(fieldSpec);
						fields.add(fieldSpec);
					}else{
						System.err.println(protoConstant);
					}
				} catch (Exception e) {
					System.err.println(protoConstant);
					System.err.println(e.getMessage());
				}
			}
			TypeSpec consta = TypeSpec.classBuilder("CenturaConstants").addModifiers(Modifier.FINAL, Modifier.PUBLIC)
					.addFields(fields).build();
			JavaFile javaFile = JavaFile.builder(pac, consta).build();
			for (ProtoConstant protoConstant : constants) {
				protoConstant.setTypeSpec(consta);
				protoConstant.setJavaFile(javaFile);
			}
			javaFile.writeTo(new File(dest));
			System.out.println("Constants: " + constants.size());
		}

		if (true) {
			new File(dest+"com/centura/teste/typedef").delete();
			for (ProtoType protoType : types) {
				if(!TypeMAP.isTypeClassFinal(TypeMAP.getTypeFinal(protoType.getName()))){
				try {
					TypeSpec typeSpec = TypeSpec.classBuilder("C" + protoType.getName()).addModifiers(Modifier.PUBLIC)
							.superclass(ParameterizedTypeName.get(AtomicReference.class, String.class))
							.addField(FieldSpec
									.builder(TypeName.LONG, "serialVersionUID", Modifier.STATIC, Modifier.FINAL,
											Modifier.PRIVATE)
									.initializer("1L").addJavadoc(protoType.getSource()).build())
							.build();
					JavaFile javaFile2 = JavaFile.builder(pac+".typedef", typeSpec).build();
					protoType.setTypeSpec(typeSpec);
					protoType.setJavaFile(javaFile2);
					javaFile2.writeTo(new File(dest));
				} catch (Exception e) {
					System.err.println(protoType);
					System.err.println(e.getMessage());
				}
				}
			}
			System.out.println("Write Types: " + types.size());
		}

		if (true) {
			List<MethodSpec> methods = new ArrayList<MethodSpec>();
			List<MethodSpec> methods2 = new ArrayList<MethodSpec>();
			List<MethodSpec> methods3 = new ArrayList<MethodSpec>();

			for (ProtoFunction protoFunction : functions) {
				try {
					List<ParameterSpec> parameterSpecs = new ArrayList<ParameterSpec>();
					for (int i = 0; i < protoFunction.getParans().length; i++) {
						ParameterSpec parameterSpec = ParameterSpec.builder(
								getTypeName(protoFunction.getParans()[i].getProtoType().getName()),
								protoFunction.getParans()[i].getName()).build();
						parameterSpecs.add(parameterSpec);
					}
					
					TypeName retTypeName = getTypeName(protoFunction.getReturnType().getName());
					
					MethodSpec.Builder builder3 = MethodSpec.methodBuilder(protoFunction.getName())
							.returns(retTypeName)
							.addJavadoc(protoFunction.getSource())
							.addModifiers(Modifier.PUBLIC);
					if(parameterSpecs.size()>0){
						builder3.addParameters(parameterSpecs);
					}
					if(!retTypeName.equals(TypeName.VOID)){
						if(retTypeName.equals(TypeName.get(CBoolean.class))){
							builder3.addStatement("return $T.FALSE",TypeName.get(CBoolean.class));
						}else{
							builder3.addStatement("return null");
						}
					}
					MethodSpec methodSpec3 = builder3.build();
					methods3.add(methodSpec3);
					
					MethodSpec.Builder builder = MethodSpec.methodBuilder(protoFunction.getName())
							.returns(retTypeName)
							.addJavadoc(protoFunction.getSource())
							.addModifiers(Modifier.PUBLIC, Modifier.FINAL, Modifier.STATIC);
					if(parameterSpecs.size()>0){
						builder.addParameters(parameterSpecs);
					}
					if(!retTypeName.equals(TypeName.VOID)){
						String[] paramNames = protoFunction.getParamNames();
						String paramns = StringUtils.join(paramNames,","); 
						builder.addStatement("return centuraAPIimpl.$N($L)",protoFunction.getName(),paramns);
					}
					MethodSpec methodSpec = builder.build();
					protoFunction.setMethodSpec(methodSpec);
					methods.add(methodSpec);
					
					MethodSpec.Builder builder2 = MethodSpec.methodBuilder(protoFunction.getName())
							.returns(retTypeName)
							.addJavadoc(protoFunction.getSource())
							.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);
					if(parameterSpecs.size()>0){
						builder2.addParameters(parameterSpecs);
					}
					MethodSpec methodSpec2 = builder2.build();
					protoFunction.setMethodSpec2(methodSpec2);
					methods2.add(methodSpec2);
					

					
				} catch (Exception e) {
					System.err.println(protoFunction);
					System.err.println(e.getMessage());
				}
			}
			
			List<FieldSpec> fields = new ArrayList<FieldSpec>();
			FieldSpec fieldSpec = FieldSpec
					.builder(ClassName.get(pac,
							"ICenturaAPI"), "centuraAPIimpl", Modifier.STATIC, Modifier.FINAL,
							Modifier.PRIVATE)
					.initializer("new $T()",ClassName.get(pac+".impl",
							"DefaultCenturaAPI")).build();
			fields.add(fieldSpec);
			
			TypeSpec func = TypeSpec.classBuilder("CenturaAPI").addModifiers(Modifier.FINAL, Modifier.PUBLIC)
					.addMethods(methods).addFields(fields).build();
			JavaFile javaFile3 = JavaFile.builder(pac, func).build();
			for (ProtoFunction protoFunction : functions) {
				protoFunction.setTypeSpec(func);
				protoFunction.setJavaFile(javaFile3);
			}
			javaFile3.writeTo(new File(dest));
			
			TypeSpec funci = TypeSpec.interfaceBuilder("ICenturaAPI").addModifiers(Modifier.PUBLIC)
					.addMethods(methods2).build();
			
			JavaFile javaFile4 = JavaFile.builder(pac, funci).build();
			javaFile4.writeTo(new File(dest));
			
			
			TypeSpec funcimpl = TypeSpec.classBuilder("DefaultCenturaAPI").addModifiers(Modifier.PUBLIC)
					.addSuperinterface(ClassName.get(pac,
							"ICenturaAPI"))
					.addMethods(methods3).build();
			JavaFile javaFile5 = JavaFile.builder(pac+".impl", funcimpl).build();
			javaFile5.writeTo(new File(dest));
			
			System.out.println("Write Functions: " + functions.size());
		}

		if (false) {
			FieldSpec fieldSpec = FieldSpec
					.builder(ParameterizedTypeName.get(HashMap.class, String.class, String.class), "typemap",
							Modifier.STATIC, Modifier.FINAL, Modifier.PUBLIC)
					.initializer("new HashMap<String,String>()").build();
			ParameterSpec parameterSpecM = ParameterSpec.builder(TypeName.get(String.class), "name").build();
			MethodSpec methodSpecM = MethodSpec.methodBuilder("getTypeFinal")
					.addModifiers(Modifier.FINAL, Modifier.PUBLIC, Modifier.STATIC).addParameter(parameterSpecM)
					.returns(TypeName.get(String.class)).addCode("return typemap.containsKey(name)?typemap.get(name):name;").build();
			Builder builder = CodeBlock.builder();
			for (ProtoType protoType : types) {
				//builder.add("/*" + protoType.getSource() + "*/" + '\n');
				builder.addStatement("typemap.put($S,$S)", protoType.getName(), protoType.getName());
			}
			CodeBlock codeBlock = builder.build();
			TypeSpec typeMap = TypeSpec.classBuilder("TypeMAP").addModifiers(Modifier.FINAL, Modifier.PUBLIC)
					.addMethod(methodSpecM).addField(fieldSpec).addStaticBlock(codeBlock).build();
			JavaFile.builder("centuratojava", typeMap).build().writeTo(new File(dest));
		}

	}

	private TypeName getTypeName(String name) {
		String typeFinal = TypeMAP.getTypeFinal(name);
		if(typeFinal.equals("CVOID")) return TypeName.VOID;
		if(TypeMAP.isTypeClassFinal(typeFinal)){
			return TypeMAP.getTypeClassFinal(typeFinal);
		}else{
			return ClassName.get(pac+".typedef",
					"C" + name);
		}
	}

	private boolean isInteger(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (Exception e) {
			try{
				Integer.decode(value);
				return true;
			}
			catch (Exception e2) {
				try{
					Long.decode(value);
					return true;
				}
				catch (Exception e3) {
					
				}
			}
		}
		return isExpression(value);
	}
	
	private boolean isExpression(String subjectString){
		try {
			return subjectString.startsWith("WM_") || subjectString.startsWith("HELP_") || subjectString.contains("TBL_") || subjectString.contains("MB_") || subjectString.contains("DDE_");
		} catch (PatternSyntaxException ex) {
			// Syntax error in the regular expression
		}
		return false;
	}

	private SortedSet<ProtoFunction> functions = new TreeSet<ProtoFunction>();
	private SortedSet<ProtoConstant> constants = new TreeSet<ProtoConstant>();
	private SortedSet<ProtoType> types = new TreeSet<ProtoType>();


	public ReadCTDAPI() {
	}

	protected void process(Path p, String line) {
		if (!line.contains("_declspec") && !line.startsWith("#define CENTURA_H") && !line.startsWith("#define Is")) {
			if (line.startsWith("#define"))
				parseProtoConstant(p, line);
			if (line.contains(" CBEXPAPI "))
				parseProtoFunction(p, line);
			if (line.startsWith("typedef "))
				parseProtoType(p, line);
		}
	}

	private void parseProtoType(Path p, String line) {
		String sourcestring = line;
		Pattern re = Pattern.compile("^\\s*typedef\\s+(\\S+)\\s+(\\S+)?\\s*",
				Pattern.DOTALL | Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher m = re.matcher(sourcestring);
		if (!m.matches() || m.groupCount() != 2)
			return;
		String name = m.group(1);
		String name2 = m.group(2);
		if (name != null && !"".equals(name)) {
			name = name.replaceAll("\\;", "");
			name = name.replaceAll("\\,", "");
			if (!name.contains("(*") && !name.endsWith("*")) {
				ProtoType protoType = new ProtoType();
				protoType.setName(TypeMAP.getTypeFinal(name));
				protoType = types.stream().filter(protoType::equals).findAny().orElse(protoType);
				protoType.addSource(line + " file: " + p.getFileName());
				if (name2 != null && !"".equals(name2)) {
					if (name2.startsWith("*")) {
						protoType.setAtomic(true);
						name2 = name2.replaceAll("\\*", "");
					}
					name2 = name2.replaceAll("\\;", "");
					name2 = name2.replaceAll("\\,", "");
					ProtoType parente = new ProtoType();
					parente.setName(TypeMAP.getTypeFinal(name2));
					parente = types.stream().filter(parente::equals).findAny().orElse(parente);
					parente.addSource(line + " file: " + p.getFileName());
					protoType.setParent(parente);
					types.add(parente);
				}
				types.add(protoType);
			}
		}

	}

	private void parseProtoFunction(Path p, String line) {
		String sourcestring = line;
		Pattern re = Pattern.compile("^(\\S+)\\s+\\S*CBEXPAPI\\s+(\\S+)\\((.+)\\);$",
				Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher m = re.matcher(sourcestring);
		if (!m.matches() || m.groupCount() != 3)
			return;
		String returnName = m.group(1);
		String functionName = m.group(2);
		String paramTypeNames = m.group(3);
		ProtoFunction protoFunction = new ProtoFunction();
		protoFunction.setName(functionName);
		protoFunction.setReturnType(getProtoType(returnName, line));
		protoFunction.setParans(getProtoParams(paramTypeNames, line));
		protoFunction.addSource(line + " file: " + p.getFileName());
		functions.add(protoFunction);
	}

	@SuppressWarnings("unused")
	private ProtoParam[] getProtoParams(String name3, String source) {
		String[] split = name3.split("\\,");
		ProtoParam[] params = new ProtoParam[split.length];
		if(split.length==1 && split[0].trim().toUpperCase().equals("VOID"))
			return new ProtoParam[0];
		for (int i = 0; i < split.length; i++) {
			String name = split[i].trim();
			String[] split2 = name.split(" ");
			ProtoParam protoParam = new ProtoParam();
			if (split2.length == 2) {
				protoParam.setName(split2[1].trim());
			} else {
				protoParam.setName("var" + (i + 1));
			}
			String typeName = split2[0].trim();
			protoParam.setProtoType(getProtoType(split2[0].trim(), source));
			protoParam.addSource(source);
			params[i] = protoParam;
		}
		return params;
	}

	private ProtoType getProtoType(String name, String source) {
		ProtoType protoType = new ProtoType();
		protoType.setName(TypeMAP.getTypeFinal(name));
		protoType = types.stream().filter(protoType::equals).findAny().orElse(protoType);
		protoType.addSource(source);
		types.add(protoType);
		return protoType;
	}

	public void parseProtoConstant(Path p, String line) {
		String sourcestring = line;
		// Pattern re =
		// Pattern.compile("^\\s*void\\s+(\\S+)\\s+[(]([^)]*)[)]\\s*[{]",Pattern.CASE_INSENSITIVE
		// | Pattern.MULTILINE | Pattern.DOTALL);
		Pattern re = Pattern.compile("^\\s*#define\\s+(\\S+)\\s+[(]?([^\\)]*)[)]?\\s*",
				Pattern.DOTALL | Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher m = re.matcher(sourcestring);
		if (!m.matches() || m.groupCount() < 2){
			return;
		}
		ProtoConstant constant = new ProtoConstant();
		constant.setName(m.group(1));
		constant.setValue(m.group(2));
		String group = m.group(2);
		if(group.indexOf('/') > 0){
			group = group.substring(0, group.indexOf('/'));
		}
		group = group.trim();
		if(group.startsWith("0x") && group.endsWith("L")){
			group = group.substring(0, group.length()-1);
			
		}
		constant.setValue(group.trim());
		constant.addSource(line + " file: " + p.getFileName());
		constants.add(constant);

	}

	public void readSource() throws IOException {
		try (DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get("centura/src/inc"), "*.h")) {
			paths.forEach(path -> {
				try {
					Files.lines(path, StandardCharsets.ISO_8859_1).forEachOrdered(line -> process(path, line));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		}
	}

}
