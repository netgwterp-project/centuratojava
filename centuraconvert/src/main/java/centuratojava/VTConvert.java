package centuratojava;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class VTConvert {
	public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		ConfigConvert configConvert = new ConfigConvert();
		configConvert.setName("vt");
		//configConvert.setSkipConstants(true);
		configConvert.setPackageNameBase("com.centura.api.vt");
		configConvert.setJavaSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\java");
		configConvert.setMapSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\vt");
		configConvert
				.setSourceFilePrincipal("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\vtsrc\\vt.apl");
		configConvert.setSourceFileCompiled("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\vtsrc\\vt.app");
		configConvert.addLibraryPath("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\vtsrc", null, "apl",
				null, "dlls");
		configConvert.setClearJavaPackage(true);
		configConvert.setClearMapSourceFolder(true);
		//configConvert.setSkippFase1(true);
		//configConvert.setSkippFase2(true);
		//configConvert.setSkippFase3(true);
		ReadAPP readAPP = new ReadAPP(configConvert);
		readAPP.processa();
	}
}
