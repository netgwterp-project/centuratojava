package centuratojava;

import com.squareup.javapoet.FieldSpec;

public class ProtoConstant extends ProtoDec<ProtoConstant> {
	private ProtoType protoType;
	private String value;
	private FieldSpec fieldSpec;
	
	public ProtoType getProtoType() {
		return protoType;
	}
	public void setProtoType(ProtoType protoType) {
		this.protoType = protoType;
	}

	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "ProtoConstant [protoType=" + protoType + ", name=" + getName() + ", value=" + value + "]";
	}
	
	
	/*@Override
	public int compareTo(ProtoConstant c) {
		return greatestCommonPrefix(getName(), c.getName()).length() <= 2 ? getName().compareTo(c.getName())
				: getValue().compareTo(c.getValue());
	}*/
	
	public String greatestCommonPrefix(String a, String b) {
	    int minLength = Math.min(a.length(), b.length());
	    for (int i = 0; i < minLength; i++)
			if (a.charAt(i) != b.charAt(i))
				return a.substring(0, i);
	    return a.substring(0, minLength);
	}
	public FieldSpec getFieldSpec() {
		return fieldSpec;
	}
	public void setFieldSpec(FieldSpec fieldSpec) {
		this.fieldSpec = fieldSpec;
	}
	
	public String getValue() {
		return value;
	}
	
	
	
}
