package centuratojava;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.centura.api.cdk.CenturaAPI;
import com.centura.api.type.CWndHandle;
import com.squareup.javapoet.TypeName;

import centuratojava.mapsrc.MapType;
import centuratojava.mapsrc.TypeInfo;
import centuratojava.mapsrc.TypeJavaEnum;

public class CorrigeAPI {

	private static final String CENTURA_LIBS_API = "C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\api\\api";
	private static List<Method> methods;

	public static void main(String[] args) {
		//MapType.getInstanceAPI();//
		MapType instanceAPI = MapType.readMapTypeAPI(CENTURA_LIBS_API,"CenturaAPI");
		methods = Arrays.asList(CenturaAPI.class.getMethods());
		//setReturnType(instanceAPI);
		//changeEstruct(instanceAPI);
		listChildren(instanceAPI);
		instanceAPI.save(CENTURA_LIBS_API+".mapsrc.json");
	}

	private static void listChildren(MapType instanceAPI) {
		List<TypeInfo> childrens = instanceAPI.listType(TypeJavaEnum.INTERFACEINCLUDE);
		for (TypeInfo typeInfo : childrens) {
			System.out.println(typeInfo);
			listChildren(typeInfo, 1);
		}
		
	}

	private static void listChildren(TypeInfo typeInfoP, int i) {
		Set<TypeInfo> childrens = typeInfoP.getChildrens();
		for (TypeInfo typeInfo : childrens) {
			if(typeInfo.getTypeJava().equals(TypeJavaEnum.GLOBALFUNCTION)){
				String nameJava = typeInfo.getNameJava();
        		List<Method> collect = methods.stream().filter(m -> m.getName().equals(nameJava)).collect(Collectors.toList());
        		if(collect.size()==0){
        			System.out.println(StringUtils.repeat("   ", i) + typeInfo);
        		}
				//processa(typeInfo);
				
				//listChildren(typeInfo, i + 1);
			}
		}
		
	}

	private static void processa(TypeInfo typeInfo) {
		if(typeInfo.getTypeJava().equals(TypeJavaEnum.GLOBALFUNCTION)){
			if(typeInfo.getReturnType()==null){
				typeInfo.setReturnType(SourceGen.CNUMBER.toString());
			}if(typeInfo.getReturnType().equalsIgnoreCase("CBOOLEAN")){
				typeInfo.setReturnType(SourceGen.CBOOLEAN.toString());
			}if(typeInfo.getReturnType().equalsIgnoreCase("CNUMBER")){
				typeInfo.setReturnType(SourceGen.CNUMBER.toString());
			}
		}if(typeInfo.getTypeJava().equals(TypeJavaEnum.STATICVAR)){
			if(typeInfo.getReferenceType().equalsIgnoreCase("BOOLEAN")){
				typeInfo.setReferenceType(SourceGen.CBOOLEAN.toString());
			}if(typeInfo.getReferenceType().equalsIgnoreCase("NUMBER")){
				typeInfo.setReferenceType(SourceGen.CNUMBER.toString());
			}if(typeInfo.getReferenceType().equalsIgnoreCase("STRING")){
				typeInfo.setReferenceType(SourceGen.CSTRING.toString());
			}if(typeInfo.getReferenceType().equalsIgnoreCase("Date/Time")){
				typeInfo.setReferenceType(SourceGen.CDATETIME.toString());
			}if(typeInfo.getReferenceType().equalsIgnoreCase("Window Handle")){
				typeInfo.setReferenceType(TypeName.get(CWndHandle.class).toString());
			}
		}
		
	}

	private static void changeEstruct(MapType instanceAPI) {
		instanceAPI.getChildrens().clear();
		List<TypeInfo> listType = instanceAPI.listType(TypeJavaEnum.INTERFACEINCLUDE);
		for (TypeInfo typeInfo : listType) {
			instanceAPI.addChildren(typeInfo);
			List<TypeInfo> listType2 = instanceAPI.listType(TypeJavaEnum.CONSTANT);
			for (TypeInfo typeInfo2 : listType2) {
				typeInfo.addChildren(typeInfo2);
			}
			List<TypeInfo> listType3 = instanceAPI.listType(TypeJavaEnum.GLOBALFUNCTION);
			for (TypeInfo typeInfo3 : listType3) {
				typeInfo.addChildren(typeInfo3);
			}
			List<TypeInfo> listType4 = instanceAPI.listType(TypeJavaEnum.STATICVAR);
			for (TypeInfo typeInfo4 : listType4) {
				typeInfo.addChildren(typeInfo4);
			}
		}
		List<TypeInfo> listType2 = instanceAPI.listType(TypeJavaEnum.BASETYPE);
		for (TypeInfo typeInfo : listType2) {
			instanceAPI.addChildren(typeInfo);
		}
		List<TypeInfo> listType3 = instanceAPI.listType(TypeJavaEnum.ANNOTATION);
		for (TypeInfo typeInfo : listType3) {
			instanceAPI.addChildren(typeInfo);
		}
		List<TypeInfo> listType4 = instanceAPI.listType(TypeJavaEnum.TOKEN);
		for (TypeInfo typeInfo : listType4) {
			instanceAPI.addChildren(typeInfo);
		}
		List<TypeInfo> listType5 = instanceAPI.listType(TypeJavaEnum.MESSAGEACTION);
		for (TypeInfo typeInfo : listType5) {
			instanceAPI.addChildren(typeInfo);
		}
		List<TypeInfo> listType6 = instanceAPI.listType(TypeJavaEnum.FUNCTION);
		for (TypeInfo typeInfo : listType6) {
			instanceAPI.addChildren(typeInfo);
		}
		List<TypeInfo> listType7 = instanceAPI.listType(TypeJavaEnum.INTERFACE);
		for (TypeInfo typeInfo : listType7) {
			instanceAPI.addChildren(typeInfo);
		}
	}

	private static void setReturnType(MapType instanceAPI) {
		
		List<Method> methods = Arrays.asList(CenturaAPI.class.getMethods());
		List<TypeInfo> listType = instanceAPI.listType(TypeJavaEnum.INTERFACEINCLUDE);
        for (TypeInfo typeInfoI : listType) {
        	List<TypeInfo> listType2 = typeInfoI.listType(TypeJavaEnum.GLOBALFUNCTION);
        	for (TypeInfo typeInfo : listType2) {
        		String nameJava = typeInfo.getNameJava();
        		List<Method> collect = methods.stream().filter(m -> m.getName().equals(nameJava)).collect(Collectors.toList());
        		if(collect.size()==1){
        			Method method = collect.get(0);
        			String name = method.getReturnType().getSimpleName();
        			typeInfo.setReturnType(name);
        			Parameter[] parameters = method.getParameters();
        			for (Parameter parameter : parameters) {
						TypeInfo addChildren = typeInfo.addChildren(parameter.getName(), TypeJavaEnum.PARAMETER, parameter.getName());
						addChildren.setReferenceType(parameter.getType().getSimpleName());
					}
        			
        		}else{
        			System.out.println(nameJava);
        		}
        	}
		}
	}

}
