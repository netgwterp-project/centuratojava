package centuratojava.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Logger {
	
	private static String TABBED = "          ";
	private static int total = 0;
	private static int fase = 0;
	private static int progress=-1;
	private static Date start;
	private static Date finished; 
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static Date faseStart;
	private static Date faseFinished; 
	
	public static void info(String message){
		System.out.println(message);
	}
	
	public static void infoTabbed(String message){
		info(TABBED + message);
	}
	
	public static void infoFase(String message){
		info("Fase  " + fase + " - " + message);
	}
	
	public static void initProgress(int total){
		Logger.total = total;
		progress=-1;
		if(total>=1){
			updateProgress(0);
		}
	}
	public static void updateProgress(int count) {

        int currentProgress = total > 0 ? (count * 100) / total : 100;

        if (currentProgress == progress) {
            return;
        }

        progress = currentProgress;

        StringBuilder bar = new StringBuilder(TABBED + "[");

        for(int i = 0; i < 50; i++){
            if( i < (currentProgress/2)){
                bar.append("=");
            }else if( i == (currentProgress/2)){
                bar.append(">");
            }else{
                bar.append(" ");
            }
        }

        bar.append("]   " + currentProgress + "%     " + count + "/" + total);
        System.out.print("\r" + bar.toString());
        if(currentProgress==100)
        	System.out.println("");

    }
	
	public static void finishProgress(){
		updateProgress(total);
		Logger.total = 0;
		progress = -1;
	}

	public static void start() {
		start = new Date();
		info("Centura 2 Java Convert started: " + getStartedString());
		line();
	}
	
	public static void line() {
		info("-------------------------------------------------------------------");
	}
	
	public static void startFase(int fase, String message) {
		Logger.fase = fase;
		faseStart = new Date();
		line();
		info("Fase  " + fase + " - " + message);
	}
	
	public static void finishFase(int fase, String message) {
		Logger.fase = fase;
		faseFinished = new Date();
		info("Fase  " + fase + " - " + message + " Tempo: " + getElapsedTimeFaseString());
		line();
	}
	
	public static void skippFase(int fase, String message) {
		Logger.fase = fase;
		faseFinished = new Date();
		infoFase(message + " - Skipped");
		line();
	}
	
	public static String getElapsedTimeFaseString() {
		return "Seconds: " + convertTimeToSecond(getElapsedTime(faseStart, faseFinished));
	}
	public static String getElapsedTimeString() {
		return "Seconds: " + convertTimeToSecond(getElapsedTime(start, finished));
	}
	private static long getElapsedTime(Date d1, Date d2){
		return d2.getTime() - d1.getTime();
	}
	
	private static long convertTimeToSecond(long milles){
		return milles/1000;
	}

	public static void finish(){
		finished = new Date();
		line();
		info("Centura 2 Java Convert finished: " + getElapsedTimeString());
		line();
	}

	public static String getStartedString() {
		return dateFormat.format(start);
	}
	
	public static String getFinishString() {
		return dateFormat.format(finished);
	}

	public static void error(String message) {
		System.err.println("ERROR: " + message);
	Thread.dumpStack();
		System.exit(0);
	}

}
