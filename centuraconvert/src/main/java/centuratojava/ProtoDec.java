package centuratojava;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;

public class ProtoDec<T extends ProtoDec<?>> implements Comparable<T> {

	private String name;
	private String source;
	private TypeSpec typeSpec;
	private JavaFile javaFile;
	
	@Override
	public int compareTo(T o) {
		return getName().compareTo(o.getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProtoDec<?> other = (ProtoDec<?>) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [name=" + name + ", source=" + getSource() + "]";
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public TypeSpec getTypeSpec() {
		return typeSpec;
	}

	public void setTypeSpec(TypeSpec typeSpec) {
		this.typeSpec = typeSpec;
	}

	public JavaFile getJavaFile() {
		return javaFile;
	}

	public void setJavaFile(JavaFile javaFile) {
		this.javaFile = javaFile;
	}
	
	public void addSource(String source){
		if(getSource()==null) setSource(source);
		else setSource(getSource() + '\n' + source);
	}
}
