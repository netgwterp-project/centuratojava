package centuratojava;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.logging.Logger;

import org.apache.commons.beanutils.ContextClassLoaderLocal;
import org.apache.commons.io.FilenameUtils;

public class FileUtil {

    static Logger                                          logger             = Logger.getLogger(FileUtil.class.getName());

    private static final ContextClassLoaderLocal<FileUtil> beansByClassLoader = new ContextClassLoaderLocal<FileUtil>() {

                                                                                  @Override
                                                                                  protected FileUtil initialValue() {
                                                                                      return new FileUtil();
                                                                                  }
                                                                              };

    public synchronized static FileUtil getInstance() {
        return beansByClassLoader.get();
    }

    public synchronized static void setInstance(FileUtil newInstance) {
        beansByClassLoader.set(newInstance);
    }

    private String homeDir;

    private String currentDir;

    private String separatorDir;

    private FileUtil() {
    }

    public void copyFile(File source, File dest) throws IOException {
        source = new File(normalizePath(source.getAbsolutePath()));
        dest = new File(normalizePath(dest.getAbsolutePath()));
        Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    public void copyFileNotSame(File source, File dest) throws IOException {
        source = new File(normalizePath(source.getAbsolutePath()));
        dest = new File(normalizePath(dest.getAbsolutePath()));
        if (source.exists() && (!dest.exists() || !Files.isSameFile(source.toPath(), dest.toPath()))) {
            Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public void createDir(String dir) {
        createDir(dir, false);
    }

    public void createDir(String dir, boolean clear) {
        File file = new File(dir);
        if (!file.exists()) {
            file.mkdirs();
        } else if (clear && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (File file2 : listFiles) {
                file2.delete();
            }
        }
    }

    public String getCurrentDir() {
        if (currentDir == null) {
            setCurrentDir(System.getProperty("user.dir"));
        }
        return currentDir;
    }

    public File getFile(String pathname) {
        return new File(normalizePath(pathname));
    }

    public String getFilenameNoExtension(String filename) {
        int i = filename.lastIndexOf('.');
        if (i > 0) {
            return filename.substring(0, i);
        } else {
            return filename;
        }
    }

    public synchronized String getHomeDir() {
        if (homeDir != null) {
            return homeDir;
        }
        homeDir = normalizePath(System.getProperty("user.home"));
        return homeDir;
    }

    public File getNewFile(String pathname) throws IOException {
        pathname = normalizePath(pathname);
        removeFile(pathname);
        File file = getFile(pathname);
        if (file.createNewFile()) {
            return file;
        } else {
            return null;
        }
    }

    public String getPathFileFromCurrentDir(String file) {
        return getPathFileFromDir(getCurrentDir(), file);
    }

    public String getPathFileFromDir(String dir, String file) {
        return normalizePath(dir + getSeparator() + file);
    }

    public String getPathFileFromHomeDir(String file) {
        return getPathFileFromDir(getHomeDir(), file);
    }

    public String getPathFolderFromCurrentDir(String... folders) {
        return getPathFolderFromDir(getCurrentDir(), folders);
    }

    public String getPathFolderFromDir(String dir, String... folders) {
        String path = dir;
        for (String folder : folders) {
            path = path + getSeparator() + folder;
        }
        return normalizePath(path);
    }

    public String getPathFolderFromHomeDir(String... folders) {
        return getPathFolderFromDir(getHomeDir(), folders);
    }

    public synchronized String getSeparator() {
        if (separatorDir != null) {
            return separatorDir;
        }
        separatorDir = FileSystems.getDefault().getSeparator();
        return separatorDir;
    }

    public String getSimpleFilename(String filename) {
        int i = filename.lastIndexOf(getSeparator());
        if (i > 0) {
            return filename.substring(i + 1, filename.length());
        } else {
            return filename;
        }
    }

    public synchronized String loadResource(String basePath, String name) throws IOException {
        BufferedInputStream stream = new BufferedInputStream(FileUtil.class.getResourceAsStream(basePath + name));
        int t = -1;
        StringBuilder sb = new StringBuilder();
        while (-1 != (t = stream.read())) {
            sb.append((char) t);
        }
        stream.close();
        return sb.toString();
    }

    public synchronized InputStream loadResourceToInputStream(String basePath, String name) throws IOException {
        return new BufferedInputStream(FileUtil.class.getResourceAsStream(basePath + name));
    }

    private String normalizePath(String path) {
        return windowsHack(FilenameUtils.normalize(path));
    }

    public byte[] read(File target) throws IOException {
        BufferedInputStream input = new BufferedInputStream(new FileInputStream(target));
        return read(input, (int) target.length());
    }

    public byte[] read(InputStream input, int size) throws IOException {
        byte[] buffer = new byte[size];
        input.read(buffer);
        input.close();
        return buffer;
    }

    public void removeFile(String pathname) throws IOException {
        File file = new File(normalizePath(pathname));
        if (file.exists()) {
            file.delete();
        }
    }

    public void setCurrentDir(String currentDir) {
        this.currentDir = normalizePath(currentDir);
    }

    public void validPath(String pathname) throws Exception {
        File file = new File(normalizePath(pathname));
        if (!file.exists()) {
            throw new Exception("path is invalide");
        }
    }
    
    public boolean isValidPath(String pathname) {
    	if(pathname == null)return false;
    	File file = new File(normalizePath(pathname));
        if (file.exists()) {
            return true;
        }
        return false;
    }
    
    public void clearFolder(String mapSourceFolder) {
		File file = new File(mapSourceFolder);
		if(file.exists()){
			try {
				if(file.isDirectory()){
						Files.walk(file.toPath()).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
				}
			} catch (IOException e) {
			}
		}
		
	}
    
    public boolean isValidDir(String pathname) {
        if(pathname == null)return false;
    	File file = new File(normalizePath(pathname));
        if (file.exists() && file.isDirectory()) {
            return true;
        }
        return false;
    }

    private String windowsHack(String filePath) {
        try {
            return URLDecoder.decode(filePath, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return filePath;
        }
    }

    public void write(File target, byte[] bytes) throws IOException {
        FileOutputStream fos = new FileOutputStream(target);
        write(fos, bytes);
        fos.close();
    }

    public void write(FileOutputStream target, byte[] bytes) throws IOException {
        target.write(bytes, 0, bytes.length);
    }

}
