package centuratojava;

public class ProtoParam extends ProtoDec<ProtoParam>{
	
	private ProtoType protoType;

	public ProtoType getProtoType() {
		return protoType;
	}
	public void setProtoType(ProtoType protoType) {
		this.protoType = protoType;
	}
	
	
}
