package centuratojava;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class SalConvert {
	public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		ConfigConvert configConvert = new ConfigConvert();
		configConvert.setName("sal");
		configConvert.setSkipConstants(true);
		//configConvert.setSkipStaticVars(true);
		configConvert.setPackageNameBase("com.centura.api.sal");
		configConvert.setJavaSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\java");
		configConvert.setMapSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\sal");
		configConvert
				.setSourceFilePrincipal("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\salsrc\\SalContext.apl");
		configConvert.addLibraryPath("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\salsrc", null, "apl",
				null, "dlls");
		configConvert.setMapSourceScanFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\sal");
		configConvert.setClearJavaPackage(true);
		configConvert.setClearMapSourceFolder(true);
		//configConvert.setSkippFase1(true);
		//configConvert.setSkippFase2(true);
		//configConvert.setSkippFase3(true);
		configConvert.setScanPackage(true);
		ReadAPP readAPP = new ReadAPP(configConvert);
		readAPP.processa();
	}
}
