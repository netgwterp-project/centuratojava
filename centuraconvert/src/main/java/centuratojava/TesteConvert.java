package centuratojava;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class TesteConvert {
	public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException,
			InvocationTargetException, NoSuchMethodException {
	//	MapType instanceAPI = MapType.getInstanceAPI();
		//instanceAPI.save("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\api\\api.mapsrc.json");
		ConfigConvert configConvert = new ConfigConvert();
		configConvert.setName("teste");
		//configConvert.setSkipConstants(true);
		configConvert.setPackageNameBase("com.centura.api.teste");
		configConvert.setJavaSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\java");
		configConvert.setMapSourceFolder("C:\\javadev\\centuratojava\\centuraapi\\src\\main\\resources\\teste");
		configConvert
				.setSourceFilePrincipal("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\tstsrc\\teste.apl");
		configConvert
				.setSourceFileCompiled("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\tstsrc\\teste.app");
		configConvert.addLibraryPath("C:\\javadev\\centuratojava\\centuraconvert\\centura\\libs\\tstsrc", null, "apl",
				null, "dlls");
		configConvert.setClearJavaPackage(true);
		configConvert.setClearMapSourceFolder(true);
		//configConvert.setSkippFase1(true);
		//configConvert.setSkippFase2(true);
		//configConvert.setSkippFase3(true);
		ReadAPP readAPP = new ReadAPP(configConvert);
		readAPP.processa();
		/*TypeInfo typeInfo = processa.listType(TypeJavaEnum.GLOBALFUNCTION).get(0);
		System.out.println();
		System.out.println(typeInfo.getNameJava());
		TypeInfo type = typeInfo.resolveIdentify("hWndF");
		System.out.println(type);*/
	}
}
