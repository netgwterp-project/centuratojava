package centuratojava;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.base.CaseFormat;

public class NamesUtils {

	public static String normalizeName(String name) {
		return StringUtils.removeEnd(StringUtils.deleteWhitespace(StringUtils.capitalize(name)), "Object");
	}
	
	public static String normalizeClassName(String name) {
		return normalize(name,CaseFormat.UPPER_CAMEL);
	}
	
	public static String getPackageFromFile(String filename){
		String pak = StringUtils.replace(FilenameUtils.removeExtension(filename), "\\", ".").toLowerCase();
		return pak;
	}

	public static String getClassNameFromFile(String filename){
		String pak = StringUtils.capitalize(FilenameUtils.getBaseName(filename).toLowerCase());
		return pak;
	}
	
	private static String normalize(String name, CaseFormat format) {
		String normalized = name.substring(0, 1).toLowerCase();
		if (name.length() > 1) {
			normalized += name.substring(1);
		}
		return CaseFormat.LOWER_CAMEL.to(format, normalized.replaceAll("[^\\w\\d]", ""));
	}

	public static String normalizeJavaString(String value) {
		return value.replaceAll("'", "\"");
	};
}
