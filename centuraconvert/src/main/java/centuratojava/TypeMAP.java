package centuratojava;

import java.util.HashMap;

import com.centura.api.cdk.IFunctionalClass;
import com.centura.api.type.CArray;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CFile;
import com.centura.api.type.CHandle;
import com.centura.api.type.CNumber;
import com.centura.api.type.CSQLHandle;
import com.centura.api.type.CString;
import com.centura.api.type.CStruct;
import com.centura.api.type.CWndHandle;
import com.squareup.javapoet.TypeName;

public final class TypeMAP {
	public static final HashMap<String, String>	typemap		= new HashMap<>();
	@SuppressWarnings("rawtypes")
	public static final HashMap<String, Class>	typefinal	= new HashMap<>();
	@SuppressWarnings("rawtypes")
	public static final HashMap<String, Class>	typefinalv	= new HashMap<>();

	public static final HashMap<TypeName, String>	initValue	= new HashMap<>();
	static {
		typemap.put("...", "CARRAY");
		typemap.put("ATOM", "CNUMBER");
		typemap.put("BOOL", "CBOOLEAN");
		typemap.put("Boolean", "CBOOLEAN");
		typemap.put("Receive Boolean", "REFCBOOLEAN");
		typemap.put("BYTE", "CNUMBER");
		typemap.put("CHAR", "CSTRING");
		typemap.put("DATETIME", "CDATETIME");
		typemap.put("Date/Time", "CDATETIME");
		typemap.put("Receive Date/Time", "REFCDATETIME");
		typemap.put("Window Handle", "CWNDHANDLE");
		typemap.put("Receive Window Handle", "REFCWNDHANDLE");
		typemap.put("DOUBLE", "CNUMBER");
		typemap.put("DWORD", "CNUMBER");
		typemap.put("FLAG", "CNUMBER");
		typemap.put("FLOAT", "CNUMBER");
		typemap.put("HANDLE", "CHANDLE");
		typemap.put("HARRAY", "CARRAY");
		typemap.put("HDB", "CHANDLE");
		typemap.put("HFFILE", "CFILE");
		typemap.put("HFILE", "CFILE");
		typemap.put("HGLOBAL", "CHANDLE");
		typemap.put("HITEM", "CHANDLE");
		typemap.put("HOUTLINE", "CHANDLE");
		typemap.put("HSTRING", "CSTRING");
		typemap.put("HUDV", "CHANDLE");
		typemap.put("Functional Class Object", "IFUNCTIONALCLASS");
		typemap.put("HWND", "CWNDHANDLE");
		typemap.put("INT", "CNUMBER");
		typemap.put("LPUINT", "REFCNUMBER");
		typemap.put("Integer", "CNUMBER");
		typemap.put("LIBHITEM", "CHANDLE");
		typemap.put("LONG", "CLONG");
		typemap.put("LPARAM", "CLPARAM");
		typemap.put("LPBOOL", "CBOOLEAN");
		typemap.put("LPBYTE", "CBYTE");
		typemap.put("LPCHAR", "CSTRING");
		typemap.put("LPCSTR", "CSTRING");
		typemap.put("LPLPSTR", "REFCSTRING");
		typemap.put("LPCWSTR", "CSTRING");
		typemap.put("LPDATETIME", "REFCDATETIME");
		typemap.put("LPDOUBLE", "CDOUBLE");
		typemap.put("LPDWORD", "CSHORT");
		typemap.put("LPFLOAT", "CFLOAT");
		typemap.put("LPHANDLE", "REFCHANDLE");
		typemap.put("LPHARRAY", "REFCARRAY");
		typemap.put("LPHFFILE", "REFCFILE");
		typemap.put("LPHFILE", "REFCFILE");
		typemap.put("File Handle", "CFILE");
		typemap.put("Receive File Handle", "REFCFILE");
		typemap.put("LPHITEM", "REFCHANDLE");
		typemap.put("LPHOUTLINE", "REFCHANDLE");
		typemap.put("LPHSTRING", "REFCSTRING");
		typemap.put("LPHWND", "REFCWNDHANDLE");
		typemap.put("LPINT", "REFCNUMBER");
		typemap.put("LPLONG", "REFCNUMBER");
		typemap.put("LPPSTR", "REFCSTRING");
		typemap.put("LPPTSTR", "REFCSTRING");
		typemap.put("LPNUMBER", "REFCNUMBER");
		typemap.put("LPSESSIONHANDLENUMBER", "REFCSQLHANDLE");
		typemap.put("LPSQLHANDLENUMBER", "REFCSQLHANDLE");
		typemap.put("LPHSQLHANDLE", "REFCSQLHANDLE");
		typemap.put("LPHSESSIONHANDLE", "REFCSQLHANDLE");
		typemap.put("LPSTR", "CSTRING");
		typemap.put("LPTSTR", "REFCSTRING");
		typemap.put("LPULONG", "REFCNUMBER");
		typemap.put("LPWCHAR", "REFCSTRING");
		typemap.put("String", "CSTRING");
		typemap.put("Receive String", "REFCSTRING");
		typemap.put("LPWORD", "REFCNUMBER");
		typemap.put("LPWSTR", "CSTRING");
		typemap.put("NPBOOL", "CBOOLEAN");
		typemap.put("NPBYTE", "CNUMBER");
		typemap.put("NPCHAR", "CSTRING");
		typemap.put("NPDOUBLE", "CNUMBER");
		typemap.put("NPDWORD", "CNUMBER");
		typemap.put("NPFLOAT", "CNUMBER");
		typemap.put("NPINT", "CNUMBER");
		typemap.put("NPLONG", "CNUMBER");
		typemap.put("NPVOID", "CVOID");
		typemap.put("NUMBER", "CNUMBER");
		typemap.put("NUMITEMTYPE", "CNUMBER");
		typemap.put("PDATETIME", "CDATETIME");
		typemap.put("PHWND", "CWNDHANDLE");
		typemap.put("PNUMBER", "CNUMBER");
		typemap.put("Number", "CNUMBER");
		typemap.put("Receive Number", "REFCNUMBER");
		typemap.put("PSYMID", "CNUMBER");
		typemap.put("ID", "CNUMBER");
		typemap.put("Long String", "CSTRING");
		typemap.put("Receive Long String", "REFCSTRING");
		typemap.put("SESSIONHANDLENUMBER", "CSQLHANDLE");
		typemap.put("SQLCURSORNUMBER", "CSQLHANDLE");
		typemap.put("SQLHANDLENUMBER", "CSQLHANDLE");
		typemap.put("Sql Handle", "CSQLHANDLE");
		typemap.put("Receive Sql Handle", "REFCSQLHANDLE");
		typemap.put("Receive Session Handle", "REFCSQLHANDLE");
		typemap.put("Session Handle", "CSQLHANDLE");
		typemap.put("HSQLHANDLE", "CSQLHANDLE");
		typemap.put("HSESSIONHANDLE", "CSQLHANDLE");
		typemap.put("STRUCT", "CSTRUCT");
		typemap.put("structPointer", "REFCSTRUCT");
		typemap.put("SYMID", "CINT");
		typemap.put("TAGCOLTEMPLATE", "CSTRUCT");
		typemap.put("TAGDATETIME", "CSTRUCT");
		typemap.put("TAGGETEXTENDEDCOLUMNDATA", "CSTRUCT");
		typemap.put("TAGNUMBER", "CSTRUCT");
		typemap.put("TAGSETEXTENDEDCOLUMNDATA", "CSTRUCT");
		typemap.put("TAGSWCCCREATESTRUCT", "CSTRUCT");
		typemap.put("TAGSWCCSETTINGS", "CSTRUCT");
		typemap.put("TAGTABLELPARAM", "CSTRUCT");
		typemap.put("TAGTABLETEMPLATE", "CSTRUCT");
		typemap.put("TAGTEMPLATETAGGED", "CSTRUCT");
		typemap.put("TEMPLATE", "CSTRUCT");
		typemap.put("Template", "CSTRUCT");
		typemap.put("UINT", "CNUMBER");
		typemap.put("ULONG", "CNUMBER");
		typemap.put("UNSIGNED", "CNUMBER");
		typemap.put("VOID", "CVOID");
		typemap.put("LPVOID", "CVOID");
		typemap.put("LPLPVOID", "CVOID");
		typemap.put("WCHAR", "CSTRING");
		typemap.put("WORD", "CNUMBER");
		typemap.put("WPARAM", "CWPARAM");
		typemap.put("LPLPARAM", "CLPARAM");
		typefinal.put("CBOOLEAN", CBoolean.class);
		typefinal.put("REFCBOOLEAN", CBoolean.ByReference.class);
		typefinal.put("CVOID", Void.class);
		typefinal.put("REFCSTRING", CString.ByReference.class);
		typefinal.put("CSTRING", CString.class);
		typefinal.put("REFCDATETIME", CDateTime.ByReference.class);
		typefinal.put("CDATETIME", CDateTime.class);
		typefinal.put("CLONG", CNumber.class);
		typefinal.put("REFCNUMBER", CNumber.ByReference.class);
		typefinal.put("CNUMBER", CNumber.class);
		typefinal.put("CBYTE", CNumber.class);
		typefinal.put("CSHORT", CNumber.class);
		typefinal.put("CINT", CNumber.class);
		typefinal.put("CDOUBLE", CNumber.class);
		typefinal.put("CFLOAT", CNumber.class);
		typefinal.put("CWPARAM", CNumber.class);
		typefinal.put("CLPARAM", CNumber.class);
		typefinal.put("REFCHANDLE", CHandle.ByReference.class);
		typefinal.put("CHANDLE", CHandle.class);
		typefinal.put("REFCWNDHANDLE", CWndHandle.ByReference.class);
		typefinal.put("CWNDHANDLE", CWndHandle.class);
		typefinal.put("REFCSQLHANDLE", CSQLHandle.ByReference.class);
		typefinal.put("CSQLHANDLE", CSQLHandle.class);
		typefinal.put("REFCFILE", CFile.ByReference.class);
		typefinal.put("CFILE", CFile.class);
		typefinal.put("REFCSTRUCT", CStruct.ByReference.class);
		typefinal.put("CSTRUCT", CStruct.class);
		typefinal.put("CARRAY", CArray.class);
		typefinal.put("REFCARRAY", CArray.ByReference.class);
		typefinal.put("IFUNCTIONALCLASS", IFunctionalClass.class);
		typefinalv.put("CBOOLEAN", CBoolean.ByValue.class);
		typefinalv.put("CSTRING", CString.ByValue.class);
		typefinalv.put("CDATETIME", CDateTime.ByValue.class);
		typefinalv.put("CLONG", CNumber.ByValue.class);
		typefinalv.put("CNUMBER", CNumber.ByValue.class);
		typefinalv.put("CBYTE", CNumber.ByValue.class);
		typefinalv.put("CSHORT", CNumber.ByValue.class);
		typefinalv.put("CINT", CNumber.ByValue.class);
		typefinalv.put("CDOUBLE", CNumber.ByValue.class);
		typefinalv.put("CFLOAT", CNumber.ByValue.class);
		typefinalv.put("CWPARAM", CNumber.ByValue.class);
		typefinalv.put("CLPARAM", CNumber.ByValue.class);
		typefinalv.put("CHANDLE", CHandle.ByValue.class);
		typefinalv.put("CWNDHANDLE", CWndHandle.ByValue.class);
		typefinalv.put("CSQLHANDLE", CSQLHandle.ByValue.class);
		typefinalv.put("CFILE", CFile.ByValue.class);
		typefinalv.put("CSTRUCT", CStruct.ByValue.class);
		typefinalv.put("CARRAY", CArray.ByValue.class);
		typefinalv.put("IFUNCTIONALCLASS", IFunctionalClass.class);
		initValue.put(TypeName.get(CBoolean.class), "$T.FALSE");
		initValue.put(TypeName.get(CNumber.class), "$T.NUMBER_Null");
		initValue.put(TypeName.get(CDateTime.class), "$T.DATETIME_Null");
		initValue.put(TypeName.get(CString.class), "$T.STRING_Null");
		initValue.put(TypeName.get(CWndHandle.class), "$T.hWndNULL");
	}

	public static final TypeName getTypeClassFinal(String name) {
		if (name == null) {
			return null;
		}
		return TypeName.get(typefinal.get(name));
	}
	public static final TypeName getTypeClassFinalV(String name) {
		if (name == null) {
			return null;
		}
		@SuppressWarnings("rawtypes")
		Class class1 = typefinalv.get(name);
		if(class1==null)
			class1 = typefinal.get(name);
		return TypeName.get(class1);
	}

	public static final String getDefaultInitValue(TypeName name) {
		String initVal = initValue.get(name);
		if (initVal == null) {
			return "new $T()";
		}
		return initVal;
	}

	public static final String getDefaultNullValue(TypeName name) {
		String initVal = initValue.get(name);
		if (initVal == null) {
			return "null";
		}
		return initVal;
	}

	public static final String getTypeFinal(String name) {
		if(name.equals("LPHSQLHANDLE")) {
			typemap.get(name);
		}
		return typemap.containsKey(name) ? typemap.get(name) : name;
	}

	public static final boolean isTypeClassFinal(String name) {
		if (name == null) {
			return false;
		}

		return typefinal.containsKey(name);
	}
}
