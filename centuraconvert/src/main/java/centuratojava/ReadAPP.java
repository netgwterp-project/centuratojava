package centuratojava;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.beanutils.locale.LocaleBeanUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import com.centura.api.cdk.CenturaAPI;

import centuratojava.log.Logger;
import centuratojava.mapsrc.MapType;
import centuratojava.mapsrc.TypeInfo;
import centuratojava.mapsrc.TypeJavaEnum;
import centuratojava.xml.ApplicationType;
import centuratojava.xml.ExternalLibraryType;
import centuratojava.xml.FunctionType;
import centuratojava.xml.JavaMapType;
import centuratojava.xml.LibraryInfoType;
import centuratojava.xml.LibraryType;
import centuratojava.xml.LibraryTypeType;
import centuratojava.xml.PackageType;
import centuratojava.xml.PropertyType;
import centuratojava.xml.VariableType;

public class ReadAPP {

	private ConfigConvert configConvert;

	public ReadAPP(ConfigConvert configConvert) {
		setConfigConvert(configConvert);
	}

	private void clearFolder(String mapSourceFolder) {
		Logger.infoFase("Clear Folder: " + mapSourceFolder);
		FileUtil.getInstance().clearFolder(mapSourceFolder);
		FileUtil.getInstance().createDir(mapSourceFolder, true);
	}

	private void clearPackage(String mapSourceFolder, String packageNameBase) {
		String folderPackage = packageNameBase.replaceAll("\\.", "\\\\");
		clearFolder(FileUtil.getInstance().getPathFileFromDir(mapSourceFolder, folderPackage));

	}

	private void configContext() {
		if (getConfigConvert().isClearMapSourceFolder()) {
			clearFolder(getConfigConvert().getMapSourceFolder());
		}
		if (getConfigConvert().isClearJavaPackage()) {
			clearPackage(getConfigConvert().getJavaSourceFolder(), getConfigConvert().getPackageNameBase());
		}
		if (getConfigConvert().isScanPackage()) {
			scanPackageXML();
		}
	}

	protected void generateDynaLibsXML(String output, String pathSource, String pathGen, String packageBase)
			throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		File dir = new File(pathSource);
		if (dir.exists()) {
			FileUtil.getInstance().createDir(pathGen, true);
			PackageType packageType = readXMLPackage(output + "\\package.xml");
			String packageName = FilenameUtils.getName(pathGen);
			if (packageType == null) {
				packageType = new PackageType();
				packageType.setPackageBase(packageBase);
				packageType.setName(packageName.toLowerCase());
				packageType.setDirName(pathGen);
			}
			File[] filesList = dir.listFiles();
			Map<String, ApplicationType> libs = new HashMap<>();
			for (File file : filesList) {
				String absolutePath = file.getAbsolutePath();
				if (!FilenameUtils.getName(absolutePath).equals("package.xml") && absolutePath.endsWith(".xml")
						&& file.isFile()) {
					ApplicationType application = readXMLApplication(absolutePath);
					if (application != null) {
						List<LibraryType> librarys = application.getLibrary();
						for (LibraryType library : librarys) {
							if (library.getName() != null) {
								String extension = FilenameUtils.getExtension(library.getName()).toLowerCase();
								if ("apd".equals(extension)) {
									String baseName = library.getNormalizedName();
									ApplicationType libapp = libs.get(baseName);
									if (libapp == null) {
										libapp = readXMLApplication(pathGen + "\\" + baseName + ".xml");
										if (libapp == null) {
											libapp = new ApplicationType();
											PropertyType propName = new PropertyType();
											propName.setName("Name");
											propName.setValue(baseName);
											libapp.getProperty().add(propName);
											PropertyType propDescription = new PropertyType();
											propDescription.setName("AppDescription");
											propDescription.setValue(library.getName());
											libapp.getProperty().add(propDescription);
											PropertyType propSource = new PropertyType();
											propSource.setName("SourceName");
											propSource.setValue("Generated from Centura2java");
											libapp.getProperty().add(propSource);
											PropertyType propNormalized = new PropertyType();
											propNormalized.setName("NormalizedName");
											propNormalized.setValue(baseName);
											libapp.getProperty().add(propNormalized);
											List<FunctionType> function = application.getFunction();
											for (FunctionType functionType : function) {
												if (functionType.isSetInclude()
														&& functionType.getInclude().equals(library.getName())) {
													FunctionType cloneBean = (FunctionType) LocaleBeanUtils
															.cloneBean(functionType);
													cloneBean.setInclude("");
													libapp.getFunction().add(cloneBean);
												}
											}
											libs.put(baseName, libapp);
										}
									}
								}
							}
						}

					}
				}
			}
			int i = 0;
			Set<Entry<String, ApplicationType>> entrySet = libs.entrySet();
			for (Entry<String, ApplicationType> entry : entrySet) {
				saveXML(entry.getValue(), pathGen + "\\" + entry.getKey() + ".xml");
				String name = getProperty(entry.getValue().getProperty(), "NormalizedName");
				String sourceName = getProperty(entry.getValue().getProperty(), "AppDescription");
				LibraryTypeType typeType = LibraryTypeType
						.fromValue(FilenameUtils.getExtension(sourceName).toUpperCase());
				LibraryInfoType infoType = getLibraryInfoType(packageType, typeType, name);

				infoType.setName(name);
				infoType.setOrdem(i);
				infoType.setLibraryType(typeType);
				infoType.setSourceName(sourceName);
				infoType.setBaseName(FilenameUtils.getBaseName(sourceName));
				infoType.setXmlSourceName(
						StringUtils.removeStart(pathGen + "\\" + entry.getKey() + ".xml", output + "\\"));
				infoType.setPackageName(packageName);
				packageType.getLibraryInfo().add(infoType);
				i++;
			}
			saveXML(packageType, output + "\\package.xml");
		} else {
			Logger.infoFase("generateDynaLibsXML: Skipped");
		}

	}

	protected void generateExternalLibsXML(String output, String pathSource, String pathGen, String packageBase)
			throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		File dir = new File(pathSource);
		if (dir.exists()) {
			FileUtil.getInstance().createDir(pathGen, true);
			PackageType packageType = readXMLPackage(output + "\\package.xml");
			String packageName = FilenameUtils.getName(pathGen);
			if (packageType == null) {
				packageType = new PackageType();
				packageType.setPackageBase(packageBase);
				packageType.setName(packageName.toLowerCase());
				packageType.setDirName(pathGen);
			}

			File[] filesList = dir.listFiles();
			Map<String, ApplicationType> libs = new HashMap<>();
			for (File file : filesList) {
				String absolutePath = file.getAbsolutePath();
				if (!FilenameUtils.getName(absolutePath).equals("package.xml") && absolutePath.endsWith(".xml")
						&& file.isFile()) {
					ApplicationType application = readXMLApplication(absolutePath);
					if (application != null) {
						List<ExternalLibraryType> externalLibrarys = application.getExternalLibrary();
						for (ExternalLibraryType externalLibrary : externalLibrarys) {
							String baseName = FilenameUtils.getBaseName(externalLibrary.getName().toLowerCase());
							ApplicationType libapp = libs.get(baseName);
							if (libapp == null) {
								libapp = readXMLApplication(pathGen + "\\" + baseName + ".xml");
								if (libapp == null) {
									libapp = new ApplicationType();
									PropertyType propName = new PropertyType();
									propName.setName("Name");
									propName.setValue(baseName);
									libapp.getProperty().add(propName);
									PropertyType propDescription = new PropertyType();
									propDescription.setName("AppDescription");
									propDescription.setValue(externalLibrary.getName());
									libapp.getProperty().add(propDescription);
									PropertyType propSource = new PropertyType();
									propSource.setName("SourceName");
									propSource.setValue("Generated from Centura2java");
									libapp.getProperty().add(propSource);
								}
								libs.put(baseName, libapp);
							}
							List<FunctionType> functions = externalLibrary.getFunction();
							for (FunctionType function : functions) {
								Object[] array = libapp.getFunction().stream()
										.filter(f -> f.getName().equals(function.getName())).toArray();
								if ((array == null) || (array.length == 0)) {
									FunctionType cloneBean = (FunctionType) LocaleBeanUtils.cloneBean(function);
									cloneBean.getParameter().addAll(function.getParameter());
									cloneBean.setInclude("");
									libapp.getFunction().add(cloneBean);
								}
							}
						}

					}
				}
			}
			Set<Entry<String, ApplicationType>> entrySet = libs.entrySet();
			for (Entry<String, ApplicationType> entry : entrySet) {
				saveXML(entry.getValue(), pathGen + "\\" + entry.getKey() + ".xml");
				String name = getProperty(entry.getValue().getProperty(), "Name");
				String sourceName = getProperty(entry.getValue().getProperty(), "AppDescription");
				LibraryTypeType typeType = LibraryTypeType
						.fromValue(FilenameUtils.getExtension(sourceName).toUpperCase());
				LibraryInfoType infoType = getLibraryInfoType(packageType, typeType, name);
				infoType.setName(name);
				infoType.setLibraryType(typeType);
				infoType.setSourceName(sourceName);
				infoType.setXmlSourceName(
						StringUtils.removeStart(pathGen + "\\" + entry.getKey() + ".xml", output + "\\"));
				infoType.setBaseName(FilenameUtils.getBaseName(sourceName));
				infoType.setPackageName(packageName);
				packageType.getLibraryInfo().add(infoType);
			}
			saveXML(packageType, output + "\\package.xml");
		} else {
			Logger.infoFase("generateExternalLibsXML: Skipped");
		}
	}

	protected void generateLibSource(String filePath, String outputFolder) {
		Logger.infoFase("generateLibSource: " + outputFolder);
		PackageType packageType = readXMLPackage(filePath + "\\package.xml");
		List<LibraryInfoType> libraryInfo = packageType.getLibraryInfo();
		if (libraryInfo.size() > 0) {
			Logger.initProgress(libraryInfo.size());
			int count = 0;
			for (LibraryInfoType libraryInfoType : libraryInfo) {
				String xmlSource = filePath + "\\" + libraryInfoType.getXmlSourceName();
				MapType mapType = MapType.getInstance(xmlSource, libraryInfoType.getName());
				TypeInfo typeInfoInteface = null;
				TypeInfo typeInfoClass = null;
				List<TypeInfo> listTypeInter = mapType.listType(TypeJavaEnum.INTERFACE);
				if (listTypeInter.size() > 0) {
					for (TypeInfo typeInfo : listTypeInter) {
						typeInfoInteface = typeInfo;
						try {
							SourceGen sourceGen = new SourceGen();
							sourceGen.setSourceFolder(outputFolder);
							sourceGen.setPackageName(MapType.getFullPackageName(typeInfo));
							sourceGen.setTypeName(typeInfo.getNameJava());
							sourceGen.setFunctionsType(getFuncTeste(typeInfo));
							sourceGen.genInterface();
						} catch (Exception e) {
							Logger.error(typeInfo.toString());
							Logger.error(e.getLocalizedMessage());
						}
					}
				}
				List<TypeInfo> listTypeClass = mapType.listType(TypeJavaEnum.CLASS);
				if (listTypeClass.size() > 0) {
					for (TypeInfo typeInfo : listTypeClass) {
						typeInfoClass = typeInfo;
						try {
							SourceGen sourceGen = new SourceGen();
							sourceGen.setSourceFolder(outputFolder);
							sourceGen.setPackageName(MapType.getFullPackageName(typeInfo));
							sourceGen.setTypeName(typeInfo.getNameJava());
							sourceGen.setFunctionsType(getFuncTeste(typeInfo));
							sourceGen.addSuperType(MapType.getFullJavaName(typeInfoInteface));
							sourceGen.genClassDefaultImpl();
						} catch (Exception e) {
							Logger.error(typeInfo.toString());
							Logger.error(e.getLocalizedMessage());
						}
					}
				}
				List<TypeInfo> listTypeInclude = mapType.listType(TypeJavaEnum.INTERFACEINCLUDE);
				if (listTypeInclude.size() > 0) {
					for (TypeInfo typeInfo : listTypeInclude) {
						try {
							SourceGen sourceGen = new SourceGen();
							sourceGen.setSourceFolder(outputFolder);
							sourceGen.setPackageName(MapType.getFullPackageName(typeInfo));
							sourceGen.setTypeName(typeInfo.getNameJava());
							if(!getConfigConvert().isSkipConstants())
								sourceGen.setConstantsType(typeInfo.listType(TypeJavaEnum.CONSTANT));
							if(!getConfigConvert().isSkipStaticVars())
								sourceGen.setStaticVarType(typeInfo.listType(TypeJavaEnum.STATICVAR));
							sourceGen.setFunctionsType(getFuncTeste(typeInfo));
							sourceGen.setTypeInfoClass(typeInfoClass);
							sourceGen.setTypeInfoInteface(typeInfoInteface);
							for (String interfaceTypeName : typeInfo.getInterfaceType()) {
								sourceGen.addSuperType(
										MapType.getFullJavaName(interfaceTypeName, TypeJavaEnum.INTERFACEINCLUDE));
							}
							sourceGen.genInterfaceInclude();
						} catch (Exception e) {
							e.printStackTrace();
							Logger.error(typeInfo.toString());
						}
					}
				}
				Logger.updateProgress(++count);
				Logger.infoFase(xmlSource);
			}
			Logger.finishProgress();
		}
		Logger.infoFase("generateLibSource: Ok");
	}

	private List<TypeInfo> getFuncTeste(TypeInfo typeInfo) {
		List<Method> methods = Arrays.asList(CenturaAPI.class.getMethods());
		List<TypeInfo> listTypeF = new ArrayList<>();
		List<TypeInfo> listType = typeInfo.listType(TypeJavaEnum.GLOBALFUNCTION);
		for (TypeInfo typeInfoA : listType) {
			List<Method> collect = methods.stream().filter(m -> m.getName().equals(typeInfoA.getNameJava())).collect(Collectors.toList());
    		if(collect.size()==0){
    			listTypeF.add(typeInfoA);
    		}
		}
		return listTypeF;
	}

	protected void generateMapType(String filePath) {
		Logger.infoFase("generateMapType: " + filePath);
		PackageType packageType = readXMLPackage(filePath + "\\package.xml");
		List<LibraryInfoType> libraryInfo = packageType.getLibraryInfo();
		if (libraryInfo.size() > 0) {
			Logger.initProgress(libraryInfo.size());
			int count = 0;
			for (LibraryInfoType libraryInfoType : libraryInfo) {
				String xmlSource = filePath + "\\" + libraryInfoType.getXmlSourceName();

				ApplicationType application = readXMLApplication(xmlSource);
				if (application != null) {
					MapType mapType = MapType.getInstance(xmlSource, libraryInfoType.getName());
					mapType.removeAllTypes();
					mapType.setPackageBase(packageType.getPackageBase());
					String packageName = libraryInfoType.getPackageName();
					String javaName = NamesUtils.normalizeClassName(libraryInfoType.getName());
					TypeInfo INTERINCLUDE = mapType.addChildren(javaName, TypeJavaEnum.INTERFACEINCLUDE,
							libraryInfoType.getName());
					INTERINCLUDE.setPackageJava(packageName);
					TypeInfo INTERFACE = null;
					TypeInfo CLASSIMPL = null;
					if (libraryInfoType.getLibraryType().equals(LibraryTypeType.APD)
							|| libraryInfoType.getLibraryType().equals(LibraryTypeType.DLL)) {
						INTERFACE = mapType.addChildren("I" + javaName, TypeJavaEnum.INTERFACE, libraryInfoType.getName());
						INTERFACE.setPackageJava(packageName);
						CLASSIMPL = mapType.addChildren("Default" + javaName, TypeJavaEnum.CLASS,
								libraryInfoType.getName());
						CLASSIMPL.setPackageJava(packageName);
						CLASSIMPL.getInterfaceType().add(MapType.getFullJavaName(INTERFACE));
					}
					for (LibraryType tLibrary : application.getLibrary()) {
						if (!tLibrary.isSetInclude() || "".equals(tLibrary.getInclude())) {
							INTERINCLUDE.getInterfaceType().add(MapType.getFullJavaName(tLibrary.getNormalizedName(),
									TypeJavaEnum.INTERFACEINCLUDE));
						}
					}

					/*for (ExternalLibraryType tLibrary : application.getExternalLibrary()) {
						if (!tLibrary.isSetInclude() || "".equals(tLibrary.getInclude())) {
							INTERINCLUDE.getInterfaceType().add(MapType.getFullJavaName(tLibrary.getNormalizedName(),
									TypeJavaEnum.INTERFACEINCLUDE));
							for (FunctionType tFunction : tLibrary.getFunction()) {
								if (!tFunction.isSetInclude() || "".equals(tFunction.getInclude())) {
									processaFunction(mapType, INTERINCLUDE, tFunction);
								}
							}
						}
					}*/

					for (PropertyType tConstant : application.getConstant()) {
						if (!tConstant.isSetInclude() || "".equals(tConstant.getInclude())) {
							TypeInfo addConstant = INTERINCLUDE.addChildren(tConstant.getName(), TypeJavaEnum.CONSTANT,
									tConstant.getName());
							addConstant.setPackageJava(INTERINCLUDE.getPackageJava());
							addConstant.setOwnerJava(INTERINCLUDE);
							addConstant.setSource(tConstant);
							addConstant.setOwner(INTERINCLUDE.getId());
							addConstant.setReferenceType(tConstant.getType());
						}
					}
					for (VariableType tStaticVar : application.getStaticVariable()) {
						if (!tStaticVar.isSetInclude() || "".equals(tStaticVar.getInclude())) {
							TypeInfo addStaticVar = INTERINCLUDE.addChildren(tStaticVar.getName(), TypeJavaEnum.STATICVAR,
									tStaticVar.getName());
							addStaticVar.setPackageJava(INTERINCLUDE.getPackageJava());
							addStaticVar.setOwnerJava(INTERINCLUDE);
							addStaticVar.setSource(tStaticVar);
							addStaticVar.setOwner(INTERINCLUDE.getId());
							addStaticVar.setReferenceType(tStaticVar.getType());
						}
					}
					for (FunctionType tFunction : application.getFunction()) {
						if (!tFunction.isSetInclude() || "".equals(tFunction.getInclude())) {
							processaFunction(mapType, INTERINCLUDE, tFunction);
							if(INTERFACE!=null)processaFunction(mapType, INTERFACE, tFunction);
							if(CLASSIMPL!=null)processaFunction(mapType, CLASSIMPL, tFunction);
						}
					}
					mapType.save();
				}
				Logger.updateProgress(++count);
			}
			Logger.finishProgress();
		}
		Logger.infoFase("generateMapType: Ok");

	}

	private void processaFunction(MapType mapType, TypeInfo owner, FunctionType tFunction) {
		
			TypeInfo addFunction = owner.addChildren(tFunction.getName(), TypeJavaEnum.GLOBALFUNCTION,
					tFunction.getName());
			addFunction.setPackageJava(owner.getPackageJava());
			addFunction.setOwnerJava(owner);
			addFunction.setReturnType(
					tFunction.getReturn() != null ? tFunction.getReturn().getType() : "Number");
			addFunction.setSource(tFunction);
			addFunction.setOwner(owner.getId());
			for (VariableType variableType : tFunction.getParameter()) {
				TypeInfo addTypeVar = addFunction.addChildren(variableType.getName(), TypeJavaEnum.PARAMETER,
						variableType.getName());
				if (addTypeVar != null) {
					addTypeVar.setPackageJava(addFunction.getPackageJava());
					addTypeVar.setOwnerJava(addFunction);
					addTypeVar.setReferenceType(variableType.getType());
					addTypeVar.setOwner(addFunction.getId());
				}
			}
			for (VariableType variableType : tFunction.getVariable()) {
				TypeInfo addTypeVar = addFunction.addChildren(variableType.getName(), TypeJavaEnum.VAR,
						variableType.getName());
				if (addTypeVar != null) {
					addTypeVar.setPackageJava(addFunction.getPackageJava());
					addTypeVar.setOwnerJava(addFunction);
					addTypeVar.setSource(variableType);
					addTypeVar.setReferenceType(variableType.getType());
					addTypeVar.setOwner(addFunction.getId());
				}
			}
			for (VariableType variableType : tFunction.getStaticVariable()) {
				TypeInfo addTypeVar = addFunction.addChildren(variableType.getName(), TypeJavaEnum.STATICVAR,
						variableType.getName());
				if (addTypeVar != null) {
					addTypeVar.setPackageJava(addFunction.getPackageJava());
					addTypeVar.setOwnerJava(addFunction);
					addTypeVar.setSource(variableType);
					addTypeVar.setReferenceType(variableType.getType());
					addTypeVar.setOwner(addFunction.getId());
				}
		}
	}

	protected void generatePackageXML(String output, String path, String packageBase, String packageName) {
		File dir = new File(path);
		if (dir.exists()) {
			PackageType packageType = new PackageType();
			packageType.setPackageBase(packageBase);
			packageType.setName(packageName.toLowerCase());
			packageType.setDirName(path);
			File[] filesList = dir.listFiles();
			for (File file : filesList) {
				String absolutePath = file.getAbsolutePath();
				if (!FilenameUtils.getName(absolutePath).equals("package.xml") && absolutePath.endsWith(".xml")
						&& file.isFile()) {
					ApplicationType applib = readXMLApplication(absolutePath);
					if (applib != null) {
						String name = getProperty(applib.getProperty(), "Name");
						if ((name != null) && !"".equals(name)) {
							String sourceName = FilenameUtils.getName(getProperty(applib.getProperty(), "SourceName"));
							String normalizedName = getProperty(applib.getProperty(), "NormalizedName");
							LibraryTypeType typeType = LibraryTypeType
									.fromValue(FilenameUtils.getExtension(sourceName).toUpperCase());
							LibraryInfoType infoType = new LibraryInfoType();
							infoType.setName(normalizedName);
							infoType.setLibraryType(typeType);
							infoType.setSourceName(sourceName);
							infoType.setXmlSourceName(StringUtils.removeStart(absolutePath, output + "\\"));
							infoType.setBaseName(FilenameUtils.getBaseName(sourceName));
							infoType.setPackageName("");
							if (applib.isSetLibrary()) {
								List<LibraryType> librarys = applib.getLibrary();
								for (LibraryType library : librarys) {
									if ((library.getName() != null) && library.isSetName() && !library.isSetInclude()) {
										String baseName = StringUtils.deleteWhitespace(
												FilenameUtils.getBaseName(library.getName()).toLowerCase());
										LibraryInfoType dep = new LibraryInfoType();
										dep.setName(library.getNormalizedName());
										if (FilenameUtils.getExtension(library.getName()).equals("APT")) {
											dep.setName(library.getNormalizedName());
										}

										dep.setLibraryType(LibraryTypeType.fromValue(
												FilenameUtils.getExtension(library.getName()).toUpperCase()));
										dep.setSourceName(library.getName());
										dep.setBaseName(baseName);
										infoType.getDependency().add(dep);
									}
								}
							}
							if (applib.isSetExternalLibrary()) {
								List<ExternalLibraryType> librarys = applib.getExternalLibrary();
								for (ExternalLibraryType library : librarys) {
									if ((library.getName() != null) && library.isSetName() && !library.isSetInclude()) {
										String baseName = StringUtils.deleteWhitespace(
												FilenameUtils.getBaseName(library.getName()).toLowerCase());
										LibraryInfoType dep = new LibraryInfoType();
										dep.setName(library.getNormalizedName());
										if (FilenameUtils.getExtension(library.getName()).equals("APT")) {
											dep.setName(library.getNormalizedName());
										}

										dep.setLibraryType(LibraryTypeType.fromValue(
												FilenameUtils.getExtension(library.getName()).toUpperCase()));
										dep.setSourceName(library.getName());
										dep.setBaseName(baseName);
										infoType.getDependency().add(dep);
									}
								}
							}
							packageType.getLibraryInfo().add(infoType);
						}
					}
				}
			}
			saveXML(packageType, output + "\\package.xml");
		} else {
			Logger.infoFase("generatePackageXML: Skipped");
		}

	}

	protected void generatePropertyList() {
		Set<JavaMapType> names = new TreeSet<>(new Comparator<JavaMapType>() {

			@Override
			public int compare(JavaMapType o1, JavaMapType o2) {

				return o1.getJavaName().compareToIgnoreCase(o2.getJavaName());
			}
		});
		File file = new File("C:\\javadev\\centuratojava\\centuratojava\\centura\\libs\\Teste\\sonlib.xml");
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
			Document doc;
			try {
				doc = Jsoup.parse(inputStream, "UTF-8", "", Parser.xmlParser());

				for (Element e : doc.select("Classe")) {
					if (e.attr("name").startsWith("pc")) {
						JavaMapType javaMapType = new JavaMapType();
						javaMapType.setJavaName(e.attr("name"));
						javaMapType.getCdkName().add(e.attr("name"));
						if (e.hasAttr("type")) {
							javaMapType.getCdkName().add(e.attr("type"));
						}
						if (e.hasAttr("className")) {
							javaMapType.getCdkName().add(e.attr("className"));
						}
						Elements elements = e.getElementsByTag("BaseClasse");
						for (Element element : elements) {
							javaMapType.getCdkName().add(element.attr("name"));
							javaMapType.getImplements().add(element.attr("name"));
						}
						names.add(javaMapType);
					}
				}

				/*
				 * for (Element e : doc.select("[name]")) {
				 * names.add(e.attr("name")); } for (Element e :
				 * doc.select("[type]")) { String val = e.attr("type"); if
				 * (!"".equals(val) && !names.contains(val)) { types.add(val); }
				 * } for (Element e : doc.select("Parameter[name]")) { String
				 * val = e.attr("name"); if (!"".equals(val) &&
				 * val.toUpperCase().equals(val)) { types.add(val); } }
				 */

				for (JavaMapType type : names) {
					System.out.println(ReflectionToStringBuilder.toString(type, ToStringStyle.SHORT_PREFIX_STYLE));
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

	}

	private void generateXML()
			throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		List<ConfigPath> sourceList = new ArrayList<>();
		List<ConfigPath> packageList = new ArrayList<>();
		if (getConfigConvert().getSourceFileCompiled() != null) {
			sourceList.add(new ConfigPath(getConfigConvert().getSourceFileCompiled(),
					getConfigConvert().getMapSourceFolder() + "\\compiled"));
		}
		sourceList.add(
				new ConfigPath(getConfigConvert().getSourceFilePrincipal(), getConfigConvert().getMapSourceFolder()));

		List<ConfigPath> libraryPaths = getConfigConvert().getLibraryPaths();
		for (ConfigPath configPath : libraryPaths) {
			File dir = new File(configPath.getSourceFolder());
			if (dir.exists()) {
				File[] filesList = dir.listFiles();
				for (File file : filesList) {
					String absolutePath = file.getAbsolutePath();
					String extension = FilenameUtils.getExtension(absolutePath.toLowerCase());
					if ((configPath.getExtensionFilter() == null) || "".equals(configPath.getExtensionFilter())
							|| configPath.getExtensionFilter().toLowerCase().equals(extension)) {
						sourceList.add(new ConfigPath(absolutePath, configPath.getOutputFolder()));
					}
				}
			}
			ConfigPath packPath = new ConfigPath();
			packPath.setSourceFolder(configPath.getSourceFolder());
			packPath.setOutputFolder(configPath.getOutputFolder());
			packPath.setPackageName(configPath.getPackageName());
			packPath.setPackageNameNative(configPath.getPackageNameNative());
			packageList.add(packPath);
		}
		Logger.initProgress(sourceList.size());
		int count = 0;
		for (ConfigPath configPath : sourceList) {
			generateXMLSource(configPath.getSourceFolder(), configPath.getOutputFolder());
			Logger.updateProgress(++count);
		}
		Logger.finishProgress();
		Logger.infoFase("generateXMLSource - Ok");
		Logger.initProgress(packageList.size());
		count = 0;
		for (ConfigPath configPath : packageList) {
			generatePackageXML(configPath.getOutputFolder(), configPath.getOutputFolder(), configPath.getPackageName(),
					getConfigConvert().getName());
			Logger.updateProgress(++count);
		}
		Logger.finishProgress();
		Logger.infoFase("generatePackageXML - Ok");
		Logger.initProgress(packageList.size());
		count = 0;
		for (ConfigPath configPath : packageList) {
			generateExternalLibsXML(configPath.getOutputFolder(), configPath.getOutputFolder(),
					configPath.getOutputFolder() + "\\dlls", configPath.getPackageNameNative());
			Logger.updateProgress(count++);
		}
		Logger.finishProgress();
		Logger.infoFase("generateExternalLibsXML - Ok");
		generateDynaLibsXML(configConvert.getMapSourceFolder(), getConfigConvert().getMapSourceFolder() + "\\compiled",
				getConfigConvert().getMapSourceFolder() + "\\apds", configConvert.getPackageNameBase() + ".apds");
		Logger.infoFase("generateDynaLibsXML - Ok");
	}

	protected void generateXMLLib(String sourcePath, String outputPath, String filter) {
		try {
			Logger.infoFase("generateXMLLib: " + outputPath);
			List<String> command = new ArrayList<>();
			command.add("cmd.exe");
			command.add("/C");
			command.add(getConfigConvert().getPathgenXML());
			command.add("lib");
			command.add(sourcePath);
			command.add(outputPath);
			command.add(filter);

			ProcessBuilder processBuilder = new ProcessBuilder(command);
			processBuilder
					.directory(new File(FilenameUtils.getFullPathNoEndSeparator(getConfigConvert().getPathgenXML())));
			processBuilder.redirectErrorStream(true);
			Process process = processBuilder.start();
			if (process != null) {
				InputStream in = process.getInputStream();
				BufferedInputStream br = new BufferedInputStream(in);
				byte[] cbuf = new byte[512];
				while (br.read(cbuf, 0, 512) != -1) {
					System.out.println(new String(cbuf));
				}

				br.close();
				in.close();
			}
			Logger.infoFase("generateXMLSource: Ok");
		} catch (Exception ex) {
			Logger.error("generateXMLSource: " + ex.getLocalizedMessage());
		}
	}

	protected void generateXMLSource(String sourcePath, String outputPath) {
		try {
			List<String> command = new ArrayList<>();
			command.add("cmd.exe");
			command.add("/C");
			command.add(getConfigConvert().getPathgenXML());
			command.add("source");
			command.add(sourcePath);
			command.add(outputPath);

			ProcessBuilder processBuilder = new ProcessBuilder(command);
			processBuilder
					.directory(new File(FilenameUtils.getFullPathNoEndSeparator(getConfigConvert().getPathgenXML())));
			processBuilder.redirectErrorStream(true);
			Process process = processBuilder.start();
			if (process != null) {
				// process.waitFor();
				InputStream in = process.getInputStream();
				BufferedInputStream br = new BufferedInputStream(in);
				byte[] cbuf = new byte[512];
				while (br.read(cbuf, 0, 512) != -1) {
					System.out.println(new String(cbuf));
				}
				br.close();
				in.close();
			}
		} catch (Exception ex) {
			Logger.error("generateXMLSource: " + ex.getLocalizedMessage());
		}
	}

	public ConfigConvert getConfigConvert() {
		return configConvert;
	}

	private LibraryInfoType getLibraryInfoType(PackageType packageType, LibraryTypeType typeType, String name) {
		LibraryInfoType infoType = null;
		List<LibraryInfoType> collect = packageType.getLibraryInfo().stream()
				.filter(p -> p.getName().equals(name) && p.getLibraryType().equals(typeType))
				.collect(Collectors.toList());
		if (collect.size() == 1) {
			infoType = collect.get(0);
		} else {
			infoType = new LibraryInfoType();
		}
		return infoType;
	}

	protected String getProperty(List<PropertyType> propertys, String name) {
		List<PropertyType> collect = propertys.stream().filter(p -> p.getName().equals(name))
				.collect(Collectors.toList());
		if (collect.size() == 1) {
			return collect.get(0).getValue();
		}
		return null;
	}

	public void processa()
			throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		Logger.start();
		Logger.startFase(0, "configContext");
		configContext();
		Logger.finishFase(0, "configContext");

		if (!getConfigConvert().isSkippFase1()) {
			Logger.startFase(1, "generateXML");
			generateXML();
			Logger.finishFase(1, "generateXML");
		} else {
			Logger.skippFase(1, "generateXML");
		}

		if (!getConfigConvert().isSkippFase2()) {
			Logger.startFase(2, "processXML");
			generateMapType(configConvert.getMapSourceFolder());
			Logger.finishFase(2, "processXML");
		} else {
			Logger.skippFase(2, "processXML");
		}

		if (!getConfigConvert().isSkippFase3()) {
			Logger.startFase(3, "generateSource");
			generateLibSource(configConvert.getMapSourceFolder(), configConvert.getJavaSourceFolder());
			Logger.finishFase(3, "generateSource");
		} else {
			Logger.skippFase(3, "generateSource");
		}
		Logger.finish();
	}

	public ApplicationType readXMLApplication(String path) {
		ApplicationType application = null;
		if (path != null) {
			File file = new File(path);
			if (file.exists()) {
				try {
					InputStream inputStream = new FileInputStream(file);
					try {
						String packageName = "centuratojava.xml";
						JAXBContext jc = JAXBContext.newInstance(packageName);
						Unmarshaller u = jc.createUnmarshaller();
						@SuppressWarnings("unchecked")
						JAXBElement<ApplicationType> doc = (JAXBElement<ApplicationType>) u.unmarshal(inputStream);
						application = doc.getValue();
					} catch (Exception e) {
						Logger.error(path);
						Logger.error(e.getLocalizedMessage());
					} finally {
						inputStream.close();
					}
				} catch (Exception e2) {
					Logger.error(path);
					Logger.error(e2.getLocalizedMessage());
				}
			}
		}
		return application;
	}

	public PackageType readXMLPackage(String path) {
		PackageType application = null;
		if (path != null) {
			File file = new File(path);
			if (file.exists()) {
				try {
					InputStream inputStream = new FileInputStream(file);
					try {
						String packageName = "centuratojava.xml";
						JAXBContext jc = JAXBContext.newInstance(packageName);
						Unmarshaller u = jc.createUnmarshaller();
						@SuppressWarnings("unchecked")
						JAXBElement<PackageType> doc = (JAXBElement<PackageType>) u.unmarshal(inputStream);
						application = doc.getValue();
					} catch (Exception e) {
						Logger.error(path);
						Logger.error(e.getLocalizedMessage());
					} finally {
						inputStream.close();
					}
				} catch (Exception e2) {
					Logger.error(path);
					Logger.error(e2.getLocalizedMessage());
				}
			}
		}
		return application;
	}

	public void saveXML(ApplicationType application, String path) {
		if (path != null) {
			File file = new File(path);
			if (file.exists()) {
				file.delete();
			}
			try {
				try {
					JAXBContext jc = JAXBContext.newInstance(ApplicationType.class);
					Marshaller m = jc.createMarshaller();
					m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.w3.org/2001/XMLSchema-instance");
					m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
					m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
					m.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,
							"file:///D:/JAVADEV/CENTURATOJAVA/CENTURATOJAVA/SRC/MAIN/XSD/app.xsd");
					m.marshal(application, file);

				} catch (Exception e) {
					Logger.error(e.getMessage());
				} finally {
				}
			} catch (Exception e2) {
				Logger.error(e2.getMessage());
			}
		}
	}

	public void saveXML(PackageType packageType, String path) {
		Map<String, LibraryInfoType> mapincludes = new HashMap<>();
		List<LibraryInfoType> libraryInfo = packageType.getLibraryInfo();
		for (LibraryInfoType infoType : libraryInfo) {
			mapincludes.put(infoType.getName(), infoType);
		}
		Set<String> sortlibs = new TreeSet<>(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				LibraryInfoType list1 = mapincludes.get(o1);
				LibraryInfoType list2 = mapincludes.get(o2);
				if (!list1.getLibraryType().equals(list2.getLibraryType())) {
					return list1.getLibraryType().compareTo(list2.getLibraryType());
				} else if (list1.getDependency().stream().anyMatch(p -> p.getName().equals(list2.getName()))) {
					return 1;
				} else if (list2.getDependency().stream().anyMatch(p -> p.getName().equals(list1.getName()))) {
					return -1;
				} else if (list1.getDependency().size() == list2.getDependency().size()) {
					return list1.getName().compareTo(list2.getName());
				} else if (list1.getDependency().size() > list2.getDependency().size()) {
					return 1;
				} else if (list1.getDependency().size() < list2.getDependency().size()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		for (String lib : mapincludes.keySet()) {
			sortlibs.add(lib);
		}
		int i = 1000;
		packageType.getLibraryInfo().clear();
		for (String lib : sortlibs) {
			LibraryInfoType libraryInfoType = mapincludes.get(lib);
			libraryInfoType.setOrdem(i);
			packageType.getLibraryInfo().add(libraryInfoType);
			i++;
		}
		if (path != null) {
			File file = new File(path);
			if (file.exists()) {
				file.delete();
			}
			try {
				try {
					JAXBContext jc = JAXBContext.newInstance(PackageType.class);
					Marshaller m = jc.createMarshaller();
					m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
					m.marshal(packageType, file);

				} catch (Exception e) {
					Logger.error(e.getMessage());
				} finally {
				}
			} catch (Exception e2) {
				Logger.error(e2.getMessage());
			}
		}
	}

	public void scanPackageXML() {
		if (getConfigConvert().getMapSourceScanFolder() != null) {
			File dir = new File(getConfigConvert().getMapSourceScanFolder());
			if (dir.exists()) {
				Path p = Paths.get(dir.getAbsolutePath());
				final int maxDepth = 10;
				Stream<Path> matches;
				try {
					matches = Files.find(p, maxDepth,
							(path, basicFileAttributes) -> String.valueOf(path).endsWith("package.xml"));
					List<Path> packgesList = matches.collect(Collectors.toList());
					for (Path path2 : packgesList) {
						PackageType packageType = readXMLPackage(path2.toFile().getAbsolutePath());
						if (packageType != null) {
							List<LibraryInfoType> libraryInfo = packageType.getLibraryInfo();
							if (libraryInfo.size() > 0) {
								for (LibraryInfoType libraryInfoType : libraryInfo) {
									String xmlSource = FilenameUtils.getFullPath(path2.toFile().getAbsolutePath())
											+ libraryInfoType.getXmlSourceName();
									ApplicationType application = readXMLApplication(xmlSource);
									if (application != null) {
										MapType instance = MapType.getInstance(xmlSource, libraryInfoType.getName());
										for (LibraryInfoType depType : libraryInfoType.getDependency()) {
											instance.getDependency().add(depType.getName());
										}
									}
								}
							}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void setConfigConvert(ConfigConvert configConvert) {
		this.configConvert = configConvert;
	}

}
