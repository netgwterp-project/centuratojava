package centuratojava;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.lang.model.element.Modifier;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.beanutils.locale.LocaleBeanUtils;
import org.apache.commons.lang.StringUtils;

import com.centura.antlr4.Centura2000Lexer;
import com.centura.antlr4.Centura2000Parser;
import com.centura.antlr4.Centura2000Parser.AdditiveExpressionContext;
import com.centura.antlr4.Centura2000Parser.ArgumentsContext;
import com.centura.antlr4.Centura2000Parser.CallStatementContext;
import com.centura.antlr4.Centura2000Parser.ConcatExpressionContext;
import com.centura.antlr4.Centura2000Parser.DecimalLiteralExpressionContext;
import com.centura.antlr4.Centura2000Parser.DimsContext;
import com.centura.antlr4.Centura2000Parser.ElseIfStatementContext;
import com.centura.antlr4.Centura2000Parser.ExpressionStatementContext;
import com.centura.antlr4.Centura2000Parser.FunctionExpressionContext;
import com.centura.antlr4.Centura2000Parser.IdentifierNameContext;
import com.centura.antlr4.Centura2000Parser.IdentifierNameExpressionContext;
import com.centura.antlr4.Centura2000Parser.IfStatementContext;
import com.centura.antlr4.Centura2000Parser.IntegerLiteralExpressionContext;
import com.centura.antlr4.Centura2000Parser.MultiplicativeExpressionContext;
import com.centura.antlr4.Centura2000Parser.ParenthesizedExpressionContext;
import com.centura.antlr4.Centura2000Parser.ReturnStatementContext;
import com.centura.antlr4.Centura2000Parser.SetStatementContext;
import com.centura.antlr4.Centura2000Parser.SingleExpressionContext;
import com.centura.antlr4.Centura2000Parser.StatementContext;
import com.centura.antlr4.Centura2000Parser.StringLiteralExpressionContext;
import com.centura.antlr4.Centura2000Parser.UnaryMinusExpressionContext;
import com.centura.antlr4.Centura2000Parser.UnaryPlusExpressionContext;
import com.centura.antlr4.Centura2000Parser.WhileStatementContext;
import com.centura.api.type.CArray;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CNumber;
import com.centura.api.type.CString;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;
import com.squareup.javapoet.TypeVariableName;

import centuratojava.log.Logger;
import centuratojava.mapsrc.MapType;
import centuratojava.mapsrc.TypeInfo;
import centuratojava.mapsrc.TypeJavaEnum;
import centuratojava.xml.FunctionType;
import centuratojava.xml.ItemType;
import centuratojava.xml.PropertyType;
import centuratojava.xml.VariableType;

public class SourceGen {
	public static TypeName OBJECT = TypeName.OBJECT;
	public static TypeName VOID = TypeName.VOID;
	public static TypeName STRING = TypeName.get(String.class);
	public static TypeName CSTRING = TypeName.get(CString.class);
	public static TypeName BOOLEAN = TypeName.BOOLEAN.box();
	public static TypeName CBOOLEAN = TypeName.get(CBoolean.class);
	public static TypeName DATE = TypeName.get(Date.class);
	public static TypeName CDATETIME = TypeName.get(CDateTime.class);
	
	public static TypeName INTEGER = TypeName.INT.box();
	public static TypeName NUMBER = TypeVariableName.get("NUMBER", TypeName.INT.box(), TypeName.LONG.box(), TypeName.FLOAT.box(), TypeName.DOUBLE.box());
	public static TypeName CNUMBER = TypeName.get(CNumber.class);

	public static TypeName getTypeName(String name) {
		return getTypeName(name, false);
	}

	public static TypeName getTypeName(String name, boolean isArray) {
		String typeFinal = TypeMAP.getTypeFinal(name);
		if (typeFinal == null) {
			return VOID;
		}
		if (isArray) {
			return ParameterizedTypeName.get(ClassName.get(CArray.class), getTypeName(name));
		}
		if (typeFinal.equals("CVOID")) {
			return VOID;
		}
		if (TypeMAP.isTypeClassFinal(typeFinal)) {
			return TypeMAP.getTypeClassFinal(typeFinal);
		} else {
			return OBJECT;
		}
	}
	public static TypeName getTypeNameParam(String name, boolean isArray) {
		String typeFinal = TypeMAP.getTypeFinal(name);
		if (typeFinal == null) {
			return VOID;
		}
		if (isArray) {
			return ParameterizedTypeName.get(ClassName.get(CArray.class), getTypeName(name));
		}
		if (typeFinal.equals("CVOID")) {
			return VOID;
		}
		if (TypeMAP.isTypeClassFinal(typeFinal)) {
			return TypeMAP.getTypeClassFinalV(typeFinal);
		} else {
			return OBJECT;
		}
	}

	private List<FieldSpec>		fields			= new ArrayList<>();
	private String				sourceFolder;
	private String				packageName;

	private String				typeName;

	private List<MethodSpec>	methods			= new ArrayList<>();
	private List<TypeSpec>	nestedType			= new ArrayList<>();

	private Set<String>			superType		= new TreeSet<>();

	private Set<String>			staticImports	= new TreeSet<>();

	private TypeInfo			typeInfoInteface;
	private TypeInfo			typeInfoClass;
	private List<TypeInfo>		functionsType;
	private List<TypeInfo>		constantsType;
	private List<TypeInfo>		staticVarType;
	private String				fieldImpl;
	private TypeInfo			context;
	private Map<String, Object>	mapSymbols		= new HashMap<>();
	private int					idname			= 1;

	public SourceGen() {
	}

	public void addStaticImport(String typeName) {
		if ((typeName != null) && !"".equals(typeName)) {
			getStaticImports().add(typeName);
		}
	}

	public void addSuperType(String typeName) {
		getSuperType().add(typeName);
	}

	private List<RuleContext> exploreContext(RuleContext ctx, int deepth) {
		List<RuleContext> list = new ArrayList<>();
		for (int i = 0; i < ctx.getChildCount(); i++) {
			ParseTree element = ctx.getChild(i);
			if (element instanceof RuleContext) {
				list.add((RuleContext) element);
			}
		}
		if (deepth > 1) {
			for (RuleContext ruleContext : list) {
				list.addAll(exploreContext(ruleContext, deepth - 1));
			}
		}
		return list;
	}

	public void genClassDefaultImpl() throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		genFunctionsClassDefault();
		Builder builder = TypeSpec.classBuilder(typeName).addModifiers(Modifier.PUBLIC);
		if (superType != null) {
			for (String string : superType) {
				builder.addSuperinterface(ClassName.bestGuess(string));
			}
		}
		builder.addMethods(methods);
		JavaFile javaFile = JavaFile.builder(packageName, builder.build()).build();
		javaFile.writeTo(new File(sourceFolder));

	}

	public void genConstants() {
		if (constantsType != null) {
			Collections.sort(constantsType, new Comparator<TypeInfo>() {

				@Override
				public int compare(TypeInfo o1, TypeInfo o2) {
					PropertyType p1 = (PropertyType) o1.getSource();
					PropertyType p2 = (PropertyType) o2.getSource();
					if (p1.getValue().contains(p2.getName())) {
						return 1;
					} else if (p2.getValue().contains(p1.getName())) {
						return -1;
					} else {
						return p1.getName().compareTo(p2.getName());
					}
				}
			});
			for (TypeInfo protoConstant : constantsType) {
				try {
					setContext(protoConstant);
					PropertyType constant = (PropertyType) protoConstant.getSource();
					String value = constant.getValue();
					StatementContext statementContext = parseExpression(value);
					TypeName typeName = resolveType(protoConstant);
					String valueTranslate = translateCentura(statementContext, typeName);
					resolveStaticImports(statementContext);
					FieldSpec fieldSpec = FieldSpec.builder(typeName, protoConstant.getNameJava(), Modifier.STATIC,
							Modifier.FINAL, Modifier.PUBLIC).initializer(valueTranslate).build();
					fields.add(fieldSpec);
				} catch (Exception e) {
					e.printStackTrace();
					Logger.error(protoConstant.toString());
					Logger.error(e.getMessage());
				}
			}
		}
	}

	public void genFieldImpl() {
		if ((typeInfoClass != null) && (typeInfoClass != null)) {
			if (fieldImpl == null) {
				fieldImpl = typeInfoInteface.getNameJava().toLowerCase() + "Impl";
			}
			FieldSpec fieldSpec = FieldSpec
					.builder(
							ClassName.get(MapType.getFullPackageName(typeInfoInteface), typeInfoInteface.getNameJava()),
							fieldImpl, Modifier.STATIC, Modifier.FINAL, Modifier.PUBLIC)
					.initializer("new $T()",
							ClassName.get(MapType.getFullPackageName(typeInfoClass), typeInfoClass.getNameJava()))
					.build();
			fields.add(fieldSpec);
		}
	}

	public void genFunctionsClassDefault() throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		if (functionsType != null) {
			for (TypeInfo typeInfo : functionsType) {
				setContext(typeInfo);
				methods.add(genFunction(typeInfo,false, false,Modifier.PUBLIC));
			}
		}
	}

	public void genFunctionsInterface() throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		if (functionsType != null) {
			for (TypeInfo typeInfo : functionsType) {
				setContext(typeInfo);
				/*FunctionType protoFunction = (FunctionType) typeInfo.getSource();
				TypeName retTypeName = TypeName.get(CNumber.class);
				if (protoFunction.isSetReturn()) {
					retTypeName = getTypeName(protoFunction.getReturn().getType());
				}

				List<ParameterSpec> parameterSpecs = new ArrayList<>();
				List<String> parameterName = new ArrayList<>();
				for (VariableType tParameter : protoFunction.getParameter()) {
					ParameterSpec parameterSpec = getParameterSpec(tParameter, parameterName);
					parameterSpecs.add(parameterSpec);
					parameterName.add(parameterSpec.name);
				}

				MethodSpec.Builder builder = MethodSpec.methodBuilder(protoFunction.getName()).returns(retTypeName)
						.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);
				if (parameterSpecs.size() > 0) {
					builder.addParameters(parameterSpecs);
				}
				MethodSpec methodSpec = builder.build();*/
				methods.add(genFunction(typeInfo, true, false, Modifier.PUBLIC, Modifier.ABSTRACT));
			}
		}
	}

	public void genFunctionsInterfaceInclude() throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		if (functionsType != null) {
			for (TypeInfo typeInfo : functionsType) {
				setContext(typeInfo);
				methods.add(genFunction(typeInfo,false, false,Modifier.PUBLIC, Modifier.STATIC));
			}
		}
	}

	private MethodSpec genFunction(TypeInfo typeInfo, boolean skipCode, boolean skipStaticVar, Modifier... modifiers ) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		FunctionType protoFunction = (FunctionType) typeInfo.getSource();
		TypeName retTypeName = TypeName.get(CNumber.class);
		if (protoFunction.isSetReturn()) {
			retTypeName = getTypeName(protoFunction.getReturn().getType());
		}
		
		List<String> parameterName = new ArrayList<>();
		List<ParameterSpec> parameterSpecs = new ArrayList<>();
		for (VariableType tParameter : protoFunction.getParameter()) {
			ParameterSpec parameterSpec = getParameterSpec(tParameter, parameterName);
			parameterSpecs.add(parameterSpec);
			parameterName.add(parameterSpec.name);
		}
		MethodSpec.Builder builder = MethodSpec.methodBuilder(protoFunction.getName()).returns(retTypeName)
				.addModifiers(modifiers);
		if (parameterSpecs.size() > 0) {
			builder.addParameters(parameterSpecs);
		}
		if(!skipCode){
			String[] paramNames = parameterName.toArray(new String[parameterName.size()]);
			String paramns = StringUtils.join(paramNames, ",");
			if(skipStaticVar || protoFunction.getStaticVariable().isEmpty()){
				if (fieldImpl != null) {
					if (!retTypeName.equals(TypeName.VOID)) {
						builder.addStatement("return $N.$N($L)", fieldImpl, protoFunction.getName(), paramns);
					} else {
						builder.addStatement("$N.$N($L)", fieldImpl, protoFunction.getName(), paramns);
					}
				} else {
					builder.addCode(getStatementBlock(protoFunction, paramNames));
				}
			} else{
				if (!retTypeName.equals(TypeName.VOID)) {
					builder.addStatement("return $N.$N($L)", protoFunction.getName()+"Function", protoFunction.getName(), paramns);
				} else {
					builder.addStatement("$N.$N($L)", protoFunction.getName()+"Function", protoFunction.getName(), paramns);
				}
				genFunctionStatic(typeInfo);
			}
		}
		return builder.build();
	}

	private void genFunctionStatic(TypeInfo typeInfo) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		Builder builder = TypeSpec.classBuilder(typeInfo.getNameJava()+"Function").addModifiers(Modifier.STATIC);
		List<TypeInfo> listType = typeInfo.listType(TypeJavaEnum.STATICVAR);
		for (TypeInfo staticVar : listType) {
				try {
					TypeName typeName = resolveType(staticVar);
					FieldSpec fieldSpec = FieldSpec
							.builder(typeName, staticVar.getNameJava(), Modifier.STATIC, Modifier.PUBLIC).build();
					builder.addField(fieldSpec);
				} catch (Exception e) {
					Logger.error(staticVar.toString());
					Logger.error(e.getMessage());
				}
			
		}
		TypeInfo cloneBean = (TypeInfo) LocaleBeanUtils.cloneBean(typeInfo);
		builder.addMethod(genFunction(cloneBean,false, true,Modifier.STATIC, Modifier.PUBLIC));
		nestedType.add(builder.build());
		
	}

	public void genInterface() throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		genFunctionsInterface();
		Builder builder = TypeSpec.interfaceBuilder(typeName).addModifiers(Modifier.PUBLIC);
		if (superType != null) {
			for (String string : superType) {
				builder.addSuperinterface(ClassName.bestGuess(string));
			}
		}
		builder.addMethods(methods);
		JavaFile javaFile = JavaFile.builder(packageName, builder.build()).build();
		javaFile.writeTo(new File(sourceFolder));

	}

	public void genInterfaceInclude() throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		genConstants();
		genStaticVariables();
		genFieldImpl();
		genFunctionsInterfaceInclude();
		Builder builder = TypeSpec.classBuilder(typeName).addModifiers(Modifier.PUBLIC, Modifier.FINAL);
		builder.addFields(fields);
		builder.addMethods(methods);
		builder.addTypes(nestedType);
		//getStaticImports().add(CenturaAPI.class.getName());
		com.squareup.javapoet.JavaFile.Builder javaFileBuilder = JavaFile.builder(packageName, builder.build());
		if (getStaticImports() != null) {
			for (String string : getStaticImports()) {
				javaFileBuilder.addStaticImport(ClassName.bestGuess(string), "*");
			}
		}
		JavaFile javaFile = javaFileBuilder.build();
		javaFile.writeTo(new File(sourceFolder));

	}

	private boolean genItemType(CodeBlock.Builder builder, boolean retItem, ItemType item) {
		if (!item.getType().equals("!")) {
			try {
				StatementContext statementContext = parseExpression(item.getType() + " " + item.getStatement());
				String translate = translateCentura(statementContext);
				if (item.isSetItem()) {
					builder.addNamed(translate + "{\n", getMapSymbols());
					builder.indent();
					for (ItemType itemC : item.getItem()) {
						retItem = genItemType(builder, retItem, itemC);
					}
					builder.unindent();
					builder.add("}\n");
				} else {
					builder.addNamed(translate + ";\n", getMapSymbols());
				}
				if (item.getType().toUpperCase().equals("RETURN")) {
					retItem = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return retItem;
	}

	private String genNameMap(Object value) {
		String map = "nmap" + idname++;
		getMapSymbols().put(map, value);
		return map;
	}

	private void genStaticVariables() {
		if (staticVarType != null) {
			for (TypeInfo protoConstant : staticVarType) {
				try {
					TypeName typeName = resolveType(protoConstant);
					FieldSpec fieldSpec = FieldSpec
							.builder(typeName, protoConstant.getNameJava(), Modifier.STATIC, Modifier.PUBLIC).build();
					fields.add(fieldSpec);
				} catch (Exception e) {
					Logger.error(protoConstant.toString());
					Logger.error(e.getMessage());
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private RuleContext getChild(RuleContext ctx) {
		List<RuleContext> list = exploreContext(ctx, 1);
		if (list.size() > 0) {
			RuleContext child = list.get(0);
			if (child instanceof ParenthesizedExpressionContext) {
				child = getChild(child);
			}
			return child;
		}
		return null;
	}

	public List<TypeInfo> getConstantsType() {
		return constantsType;
	}

	public TypeInfo getContext() {
		return context;
	}

	public String getFieldImpl() {
		return fieldImpl;
	}

	public List<FieldSpec> getFields() {
		return fields;
	}

	public List<TypeInfo> getFunctionsType() {
		return functionsType;
	}

	public Map<String, Object> getMapSymbols() {
		return mapSymbols;
	}

	public List<MethodSpec> getMethods() {
		return methods;
	}

	public String getPackageName() {
		return packageName;
	}

	public ParameterSpec getParameterSpec(VariableType tParameter, List<String> names) {
		String name = tParameter.getName();
		if (name == null) {
			name = "PARAM1";
		}
		TypeName typeName = getTypeNameParam(tParameter.getType(), tParameter.isIsArray());
		if (names.contains(name)) {
			name = name + "" + (names.size() + 1);
		}
		return ParameterSpec.builder(typeName, name).build();
	}

	public String getSourceFolder() {
		return sourceFolder;
	}

	private CodeBlock getStatementBlock(FunctionType function, String[] paramNames) {
		CodeBlock.Builder builder = CodeBlock.builder();
		List<String> varNames = new ArrayList<>();
		varNames.addAll(Arrays.asList(paramNames));
		List<VariableType> variable = function.getVariable();
		for (VariableType variableType : variable) {
			String varName = variableType.getName();
			TypeName typeName = getTypeName(variableType.getType(), variableType.isIsArray());
			builder.addStatement("$T $N = " + TypeMAP.getDefaultInitValue(typeName), typeName, varName, typeName);
			varNames.add(varName);
		}
		boolean retItem = false;
		try {
			ItemType code = function.getCode();
			if(code!=null){
				for (ItemType item : code.getItem()) {
					retItem = genItemType(builder, retItem, item);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		TypeName retTypeName = CNUMBER;
		if (function.isSetReturn()) {
			retTypeName = getTypeName(function.getReturn().getType());
		}
		if (!retItem && !retTypeName.equals(TypeName.VOID)) {
			builder.addStatement("return " + TypeMAP.getDefaultNullValue(retTypeName), retTypeName);
		}
		return builder.build();
	}

	

	public Set<String> getStaticImports() {
		return staticImports;
	}

	public List<TypeInfo> getStaticVarType() {
		return staticVarType;
	}

	public Set<String> getSuperType() {
		return superType;
	}

	public TypeInfo getTypeInfoClass() {
		return typeInfoClass;
	}

	public TypeInfo getTypeInfoInteface() {
		return typeInfoInteface;
	}

	public String getTypeName() {
		return typeName;
	}


	@SuppressWarnings("unused")
	private boolean isParentStatement(RuleContext context, int ruleparente) {
		if (context.getParent() == null) {
			return false;
		} else if (context.getParent().getRuleIndex() == ruleparente) {
			return true;
		}
		return isParentStatement(context.getParent(), ruleparente);
	}

	private StatementContext parseExpression(String value) {
		Lexer lexer = new Centura2000Lexer(CharStreams.fromString(value));
		TokenStream tokenStream = new CommonTokenStream(lexer);
		Centura2000Parser parser = new Centura2000Parser(tokenStream);
		return parser.statement();

	}

	/*private String processaWrapper(Class<?> clazzWrapper, RuleContext ctx, String expr) {
		RuleContext child = getChild(ctx);
		if ((child instanceof LogicalAndExpressionContext) || (child instanceof LogicalOrExpressionContext)
				|| (child instanceof RelationalExpressionContext) || (child instanceof NotExpressionContext)
				|| (child instanceof EqualityExpressionContext) || (child instanceof InequalityExpressionContext)) {
			return expr;
		}
		return "CBoolean.wrapper( " + expr + " ).get()";
	}*/

	private void resolveStaticImports(StatementContext statementContext) {
		List<String> ruleText = getRuleText(statementContext.getRuleContext(),
				Centura2000Parser.RULE_identifierName);
		for (String string : ruleText) {
			String fullJavaName = MapType.getFullOwnerJavaName(string, TypeJavaEnum.CONSTANT);
			String fullPackageName = MapType.getFullOwnerPackageName(string, TypeJavaEnum.CONSTANT);
			if ((fullPackageName != null) && !fullPackageName.equals(packageName)) {
				addStaticImport(fullJavaName);
			}
		}
	}
	
	public List<String> getRuleText(RuleContext ctx, int rule) {
		return exploreRule(rule, ctx, 0);
	}

	private List<String> exploreRule(int rule, RuleContext ctx, int indentation) {
		List<String> list = new ArrayList<>();
		if (ctx.getRuleIndex() == rule) {
			list.add(ctx.getText());
		}
		if (indentation < 100) {
			for (int i = 0; i < ctx.getChildCount(); i++) {
				ParseTree element = ctx.getChild(i);
				if (element instanceof RuleContext) {
					list.addAll(exploreRule(rule, (RuleContext) element, indentation + 1));
				}
			}
		}
		return list;
	}

	/*private TypeName resolveType(String name) {
		return resolveType(name, TypeJavaEnum.VAR);
	}

	private TypeName resolveType(String str, TypeJavaEnum typeJavaEnum) {
		TypeInfo type = getContext().getMapType().getType(str, typeJavaEnum);
		if (type != null) {
			return getTypeName(type.getReferenceType());
		}
		return TypeName.OBJECT;
	}*/

	public void setConstantsType(List<TypeInfo> constantsType) {
		this.constantsType = constantsType;
	}

	public void setContext(TypeInfo context) {
		this.context = context;
		mapSymbols.clear();
	}

	public void setFieldImpl(String fieldImpl) {
		this.fieldImpl = fieldImpl;
	}

	public void setFields(List<FieldSpec> fields) {
		this.fields = fields;
	}

	public void setFunctionsType(List<TypeInfo> functionsType) {
		this.functionsType = functionsType;
	}

	public void setMapSymbols(Map<String, Object> mapSymbols) {
		this.mapSymbols = mapSymbols;
	}

	public void setMethods(List<MethodSpec> methods) {
		this.methods = methods;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setSourceFolder(String sourceFolder) {
		this.sourceFolder = sourceFolder;
	}

	public void setStaticImports(Set<String> staticImports) {
		this.staticImports = staticImports;
	}

	public void setStaticVarType(List<TypeInfo> staticVarType) {
		this.staticVarType = staticVarType;
	}

	public void setSuperType(Set<String> superType) {
		this.superType = superType;
	}

	public void setTypeInfoClass(TypeInfo typeInfoClass) {
		this.typeInfoClass = typeInfoClass;
	}

	public void setTypeInfoInteface(TypeInfo typeInfoInteface) {
		this.typeInfoInteface = typeInfoInteface;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	private String stringNormalize(String str) {
		if (str.startsWith("\'") && str.endsWith("\'")) {
			str = "\"" + str.substring(1, str.length() - 1).replaceAll("\"", "\\\\\"") + "\"";
		}
		str = str.replaceAll("\\\\'", "'");
		String[] split = str.split("\\r\\n");
		if (split.length > 1) {
			str = "";
			for (int i = 0; i < split.length; i++) {
				if (i == 0) {
					str = split[i] + "\\r\\n\" +";
				} else if (i < (split.length - 1)) {
					str = str + '\r' + '\n' + "\"" + split[i] + "\\r\\n\" +";
				} else {
					str = str + '\r' + '\n' + "\"" + split[i];
				}
			}
		}
		return str;
	}

	public String translateCentura(RuleContext ctx) {
		return translateCentura(ctx, TypeName.VOID);
	}

	public String translateCentura(RuleContext ctx, TypeName expected) {
		if (ctx == null)
			return "";
		
		String str = "";
		
		if (ctx.getRuleIndex() == Centura2000Parser.RULE_statement) {
			if (ctx.getChildCount() > 0) {
				for (int i = 0; i < ctx.getChildCount(); i++) {
					ParseTree element = ctx.getChild(i);
					if (element instanceof RuleContext) {
						str = str + translateCentura((RuleContext) element, expected);
					}
				}
			}
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_singleExpression) {
			if (ctx instanceof UnaryMinusExpressionContext) {
				str = "-" + translateCentura(((UnaryMinusExpressionContext) ctx).singleExpression(), NUMBER);
				str = processaWrapper(expected, NUMBER, str);
			} else if (ctx instanceof UnaryPlusExpressionContext) {
				str = translateCentura(((UnaryPlusExpressionContext) ctx).singleExpression(), NUMBER);
				str = processaWrapper(expected, NUMBER, str);
			} else if (ctx instanceof ConcatExpressionContext) {
				ConcatExpressionContext mctx = (ConcatExpressionContext) ctx;
				str = translateCentura(mctx.singleExpression(0), STRING)
						+ translateCentura(mctx.concatCharOp())
						+ translateCentura(mctx.singleExpression(1), STRING);
				str = processaWrapper(expected, STRING, str);
			} else if (ctx instanceof MultiplicativeExpressionContext) {
				MultiplicativeExpressionContext mctx = (MultiplicativeExpressionContext) ctx;
				String op = mctx.multiplyOp() != null ? translateCentura(mctx.multiplyOp())
						: translateCentura(mctx.divideOp());
				str = translateCentura(mctx.singleExpression(0), NUMBER) + op
						+ translateCentura(mctx.singleExpression(1), NUMBER);
				str = processaWrapper(expected, NUMBER, str);
			} else if (ctx instanceof AdditiveExpressionContext) {
				AdditiveExpressionContext mctx = (AdditiveExpressionContext) ctx;
				String op = mctx.plusOp() != null ? translateCentura(mctx.plusOp()) : translateCentura(mctx.minusOp());
				str = translateCentura(mctx.singleExpression(0), NUMBER) + op
						+ translateCentura(mctx.singleExpression(1), NUMBER);
				str = processaWrapper(expected, NUMBER, str);
			} else if (ctx instanceof StringLiteralExpressionContext) {
				str = translateCentura(((StringLiteralExpressionContext) ctx).stringLiteral());
				str = processaWrapper(expected, STRING, str);
			} else if (ctx instanceof ParenthesizedExpressionContext) {
				str = "( " + translateCentura(((ParenthesizedExpressionContext) ctx).singleExpression())
						+ " )";
			} else if (ctx instanceof IntegerLiteralExpressionContext) {
				str = translateCentura(((IntegerLiteralExpressionContext) ctx).integerLiteral());
				str = processaWrapper(expected, INTEGER, str);
			} else if (ctx instanceof DecimalLiteralExpressionContext) {
				str = translateCentura(((DecimalLiteralExpressionContext) ctx).decimalLiteral());
				str = processaWrapper(expected, NUMBER, str);
			} else if (ctx instanceof FunctionExpressionContext) {
				str = translateCentura(((FunctionExpressionContext) ctx).identifierName());
				TypeInfo typeInfo = resolveTypeInfo(str);
				if(typeInfo!=null){
					List<TypeInfo> listType = typeInfo.listType(TypeJavaEnum.PARAMETER);
					ArgumentsContext args = ((FunctionExpressionContext) ctx).arguments();
					String argsStr = "";
					if (args != null) {
						if (args.argumentList() != null) {
							List<SingleExpressionContext> argumentList = args.argumentList().singleExpression();
							int i = 0;
							for (SingleExpressionContext singleExpressionContext : argumentList) {
								if (!"".equals(argsStr)) {
									argsStr = argsStr + ", ";
								}
								TypeName argType = listType.size() > i ? resolveType(listType.get(i)) : OBJECT;
								argsStr = argsStr + translateCentura(singleExpressionContext, argType);
								i++;
							}
							str = str + "( " + argsStr + " )";
						} else {
							str = str + "()";
						}
					}
					else {
						str = str + "()";
					}
				}else{
					System.exit(0);
				}
				str = processaWrapper(expected, resolveType(typeInfo), str);
			} else if (ctx instanceof IdentifierNameExpressionContext) {
				str = translateCentura(((IdentifierNameExpressionContext) ctx).identifierName(), expected);
				TypeName resolveType = resolveType(str);
				DimsContext dims = ((IdentifierNameExpressionContext) ctx).dims();
				if (dims != null) {
					String dimsStr = "";
					List<SingleExpressionContext> singleExpression = dims.singleExpression();
					for (SingleExpressionContext singleExpressionContext : singleExpression) {
						dimsStr = dimsStr + ".pos(" + translateCentura(singleExpressionContext, TypeName.INT) + ")";
					}
					str = str + dimsStr;
				}
				str = processaWrapper(expected, resolveType, str);
			}
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_expressionStatement) {
			str = translateCentura(((ExpressionStatementContext) ctx).singleExpression(),expected);
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_ifStatement) {
			str = "if( " + translateCentura(((IfStatementContext) ctx).singleExpression(), BOOLEAN) + " )";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_whileStatement) {
			str = "while( " + translateCentura(((WhileStatementContext) ctx).singleExpression(), BOOLEAN)
					+ " )";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_elseIfStatement) {
			str = "else if( " + translateCentura(((ElseIfStatementContext) ctx).singleExpression(), BOOLEAN)
					+ " )";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_elseStatement) {
			str = "else";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_returnStatement) {
			str = "return " + translateCentura(((ReturnStatementContext) ctx).singleExpression(),
					resolveType(getContext().getReturnType()));
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_setStatement) {
			String identy = translateCentura(((SetStatementContext) ctx).singleExpression(0));
			TypeName resolveType = resolveType(identy);
			String assign = translateCentura(((SetStatementContext) ctx).singleExpression(1), resolveType);
			str = identy + ".set( " + assign + " )";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_callStatement) {
			str = translateCentura(((CallStatementContext) ctx).singleExpression());
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_stringLiteral) {
			str = ctx.getText();
			str = stringNormalize(str);
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_booleanLiteral) {
			str = ctx.getText();
			if ("FALSE".equals(str)) {
				str = "false";
			}
			if ("TRUE".equals(str)) {
				str = "true";
			}
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_nullLiteral) {
			str = ctx.getText();
			if ("STRING_Null".equals(str)) {
				str = "$" + genNameMap(CSTRING)+ ":T.STRING_Null";
			} else if ("NUMBER_Null".equals(str)) {
				str = "$" + genNameMap(CNUMBER)+ ":T.NUMBER_Null";
			} else if ("DATETIME_Null".equals(str)) {
				str = "$" + genNameMap(CNUMBER)+ ":T.DATETIME_Null";
			} else if ("OBJ_Null".equals(str)) {
				str = "null";
			}
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_decimalLiteral) {
			str = ctx.getText();
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_integerLiteral) {
			str = ctx.getText();
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_dims) {
			DimsContext dimsContext = (DimsContext) ctx;
			if (dimsContext.getChildCount() > 0) {
				for (int i = 0; i < dimsContext.getChildCount(); i++) {
					ParseTree element = dimsContext.getChild(i);
					if (element instanceof RuleContext) {
						str = str + ".pos(" + translateCentura((RuleContext) element, TypeName.INT) + ")";
					}
				}
			}
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_identifierName) {
			if (((IdentifierNameContext) ctx).reservedWord() != null) {
				if (((IdentifierNameContext) ctx).reservedWord().booleanLiteral() != null) {
					str = translateCentura(((IdentifierNameContext) ctx).reservedWord().booleanLiteral(), expected);
				} else if (((IdentifierNameContext) ctx).reservedWord().nullLiteral() != null) {
					str = translateCentura(((IdentifierNameContext) ctx).reservedWord().nullLiteral(), expected);
				} else {
					str = ctx.getText();
				}
			} else if (MapType.getFullOwnerJavaName(ctx.getText(), TypeJavaEnum.CONSTANT) != null) {
				str = ctx.getText();
			} else {
				str = ctx.getText();
			}
			str = processaWrapper(expected, resolveType(str), str);
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_concatCharOp) {
			str = " + ";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_andOp) {
			str = " && ";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_orOp) {
			str = " || ";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_notOp) {
			str = "!";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_equalsOp) {
			if (isParentStatement(ctx, ExpressionStatementContext.class)) {
				str = " = ";
			} else {
				str = " == ";
			}
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_notEqualsOp) {
			str = " != ";
		} else if (ctx.getRuleIndex() == Centura2000Parser.RULE_dotOp) {
			str = ".";
		} else if ((ctx.getRuleIndex() == Centura2000Parser.RULE_plusOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_minusOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_multiplyOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_divideOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_lessThanOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_moreThanOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_lessThanEqualsOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_greaterThanEqualsOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_bitAndOp)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_bitOrOp)) {
			str = " " + ctx.getText() + " ";
		} else if ((ctx.getRuleIndex() == Centura2000Parser.RULE_eof)
				|| (ctx.getRuleIndex() == Centura2000Parser.RULE_eos)) {
			str = "";
		} else {
			str = "/*" + ctx.getText() + "*/";
		}
		return str;
	}

	private TypeName resolveType(String identy) {
		TypeInfo resolveIdentify = resolveTypeInfo(identy);
		if (resolveIdentify != null)
			return resolveType(resolveIdentify);
		else if(identy.equals("true") || identy.equals("false")) {
			Logger.infoFase("nameJava: " + identy + " resolveType: " + BOOLEAN.toString());
			return BOOLEAN;
		}
		else{
			TypeName typeName = getTypeName(identy);
			if(typeName != null) {
				Logger.infoFase("nameJava: " + identy + " resolveType: " + typeName.toString());
				return typeName;
			}else {
				Logger.infoFase("nameJava: " + identy + " resolveType: " + OBJECT.toString());
				return OBJECT;
			}
		}
	}
	private TypeName resolveType(TypeInfo resolveIdentify) {
		if (resolveIdentify != null){
			if (resolveIdentify.getTypeJava().equals(TypeJavaEnum.CONSTANT)) {
				Logger.infoFase("nameJava: " + resolveIdentify.getNameJava() + " resolveTypeT: " + resolveIdentify.getReferenceType());
				if (resolveIdentify.getReferenceType().equals("Number"))
					return INTEGER;
				else if (resolveIdentify.getReferenceType().equals("String"))
					return STRING;
				else if (resolveIdentify.getReferenceType().equals("Boolean"))
						return BOOLEAN;
			} else if (resolveIdentify.getTypeJava().equals(TypeJavaEnum.GLOBALFUNCTION)) {
				Logger.infoFase("nameJava: " + resolveIdentify.getNameJava() + " resolveTypeT: " + resolveIdentify.getReturnType());
				return getTypeName(resolveIdentify.getReturnType());
			} else {
				Logger.infoFase("nameJava: " + resolveIdentify.getNameJava() + " resolveTypeT: " + resolveIdentify.getReferenceType());
				return getTypeName(resolveIdentify.getReferenceType());
			}
		}
		Logger.infoFase("nameJava: " + null + " resolveTypeT: " + OBJECT.toString());
		return OBJECT;
	}

	private TypeInfo resolveTypeInfo(String identy) {
		if (getContext() != null)
			return getContext().resolveIdentify(identy);
		return null;

	}

	private String processaWrapper(TypeName expected, TypeName resolved, String expr) {
		if(expected == null || resolved == null || expected.equals(resolved) || expected.equals(OBJECT) || expected.equals(VOID) ) {
			return expr;
		}
		else if(expected.equals(BOOLEAN)){
			if(resolved.equals(CBOOLEAN)) return expr + ".get()";
			else return "$" + genNameMap(CBOOLEAN)+ ":T.wrapper( " + expr + " ).get()";
		}
		else if(expected.equals(CBOOLEAN) && (resolved.equals(BOOLEAN) || resolved.equals(NUMBER))){
			return "$" + genNameMap(CBOOLEAN)+ ":T.wrapper( " + expr + " )";
		}
		else if(expected.equals(STRING) && resolved.equals(CSTRING)){
			return expr + ".get()";
		}
		else if(expected.equals(CSTRING) && resolved.equals(STRING)){
			return "$" + genNameMap(CSTRING)+ ":T.wrapper( " + expr + " )";
		}
		else if(expected.equals(NUMBER) && resolved.equals(CNUMBER)){
			return expr + ".get()";
		}
		else if(expected.equals(INTEGER) && resolved.equals(CNUMBER)){
			return expr + ".getiInt()";
		}
		else if(expected.equals(CNUMBER) && resolved.equals(NUMBER)){
			return "$" + genNameMap(CNUMBER)+ ":T.wrapper( " + expr + " )";
		}
		else return expr;
	}



	private boolean isParentStatement(RuleContext context, Class<?> clazz, Class<?>... aclazz) {
		if (isDirectParentStatement(context, clazz, aclazz))
			return true;
		else if (context.getParent() == null)
			return false;
		return isParentStatement(context.getParent(), clazz, aclazz);
	}

	private boolean isDirectParentStatement(RuleContext context, Class<?> clazz, Class<?>... aclazz) {
		if (context.getParent() == null)
			return false;
		else if (context.getParent().getClass().isAssignableFrom(clazz))
			return true;
		else if (aclazz != null && aclazz.length > 0) {
			for (Class<?> clazz1 : aclazz) {
				if (context.getParent().getClass().isAssignableFrom(clazz1))
					return true;
			}
		}
		return false;
	}


}
