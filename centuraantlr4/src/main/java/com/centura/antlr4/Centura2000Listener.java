// Generated from com\centura\antlr4\Centura2000.g4 by ANTLR 4.7
package com.centura.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link Centura2000Parser}.
 */
public interface Centura2000Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(Centura2000Parser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(Centura2000Parser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement(Centura2000Parser.ExpressionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement(Centura2000Parser.ExpressionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#elseIfStatement}.
	 * @param ctx the parse tree
	 */
	void enterElseIfStatement(Centura2000Parser.ElseIfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#elseIfStatement}.
	 * @param ctx the parse tree
	 */
	void exitElseIfStatement(Centura2000Parser.ElseIfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#elseStatement}.
	 * @param ctx the parse tree
	 */
	void enterElseStatement(Centura2000Parser.ElseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#elseStatement}.
	 * @param ctx the parse tree
	 */
	void exitElseStatement(Centura2000Parser.ElseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(Centura2000Parser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(Centura2000Parser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#selectCaseStatement}.
	 * @param ctx the parse tree
	 */
	void enterSelectCaseStatement(Centura2000Parser.SelectCaseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#selectCaseStatement}.
	 * @param ctx the parse tree
	 */
	void exitSelectCaseStatement(Centura2000Parser.SelectCaseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void enterCaseStatement(Centura2000Parser.CaseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void exitCaseStatement(Centura2000Parser.CaseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoopStatement(Centura2000Parser.LoopStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#loopStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoopStatement(Centura2000Parser.LoopStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(Centura2000Parser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(Centura2000Parser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(Centura2000Parser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(Centura2000Parser.ContinueStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(Centura2000Parser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(Centura2000Parser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(Centura2000Parser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(Centura2000Parser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#defaultStatement}.
	 * @param ctx the parse tree
	 */
	void enterDefaultStatement(Centura2000Parser.DefaultStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#defaultStatement}.
	 * @param ctx the parse tree
	 */
	void exitDefaultStatement(Centura2000Parser.DefaultStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#callStatement}.
	 * @param ctx the parse tree
	 */
	void enterCallStatement(Centura2000Parser.CallStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#callStatement}.
	 * @param ctx the parse tree
	 */
	void exitCallStatement(Centura2000Parser.CallStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#setStatement}.
	 * @param ctx the parse tree
	 */
	void enterSetStatement(Centura2000Parser.SetStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#setStatement}.
	 * @param ctx the parse tree
	 */
	void exitSetStatement(Centura2000Parser.SetStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#whenSqlErrorStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhenSqlErrorStatement(Centura2000Parser.WhenSqlErrorStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#whenSqlErrorStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhenSqlErrorStatement(Centura2000Parser.WhenSqlErrorStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#identifierName}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierName(Centura2000Parser.IdentifierNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#identifierName}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierName(Centura2000Parser.IdentifierNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#openBracket}.
	 * @param ctx the parse tree
	 */
	void enterOpenBracket(Centura2000Parser.OpenBracketContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#openBracket}.
	 * @param ctx the parse tree
	 */
	void exitOpenBracket(Centura2000Parser.OpenBracketContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#closeBracket}.
	 * @param ctx the parse tree
	 */
	void enterCloseBracket(Centura2000Parser.CloseBracketContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#closeBracket}.
	 * @param ctx the parse tree
	 */
	void exitCloseBracket(Centura2000Parser.CloseBracketContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#dotOp}.
	 * @param ctx the parse tree
	 */
	void enterDotOp(Centura2000Parser.DotOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#dotOp}.
	 * @param ctx the parse tree
	 */
	void exitDotOp(Centura2000Parser.DotOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#plusOp}.
	 * @param ctx the parse tree
	 */
	void enterPlusOp(Centura2000Parser.PlusOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#plusOp}.
	 * @param ctx the parse tree
	 */
	void exitPlusOp(Centura2000Parser.PlusOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#minusOp}.
	 * @param ctx the parse tree
	 */
	void enterMinusOp(Centura2000Parser.MinusOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#minusOp}.
	 * @param ctx the parse tree
	 */
	void exitMinusOp(Centura2000Parser.MinusOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#notOp}.
	 * @param ctx the parse tree
	 */
	void enterNotOp(Centura2000Parser.NotOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#notOp}.
	 * @param ctx the parse tree
	 */
	void exitNotOp(Centura2000Parser.NotOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#multiplyOp}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyOp(Centura2000Parser.MultiplyOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#multiplyOp}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyOp(Centura2000Parser.MultiplyOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#divideOp}.
	 * @param ctx the parse tree
	 */
	void enterDivideOp(Centura2000Parser.DivideOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#divideOp}.
	 * @param ctx the parse tree
	 */
	void exitDivideOp(Centura2000Parser.DivideOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#lessThanOp}.
	 * @param ctx the parse tree
	 */
	void enterLessThanOp(Centura2000Parser.LessThanOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#lessThanOp}.
	 * @param ctx the parse tree
	 */
	void exitLessThanOp(Centura2000Parser.LessThanOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#moreThanOp}.
	 * @param ctx the parse tree
	 */
	void enterMoreThanOp(Centura2000Parser.MoreThanOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#moreThanOp}.
	 * @param ctx the parse tree
	 */
	void exitMoreThanOp(Centura2000Parser.MoreThanOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#lessThanEqualsOp}.
	 * @param ctx the parse tree
	 */
	void enterLessThanEqualsOp(Centura2000Parser.LessThanEqualsOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#lessThanEqualsOp}.
	 * @param ctx the parse tree
	 */
	void exitLessThanEqualsOp(Centura2000Parser.LessThanEqualsOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#greaterThanEqualsOp}.
	 * @param ctx the parse tree
	 */
	void enterGreaterThanEqualsOp(Centura2000Parser.GreaterThanEqualsOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#greaterThanEqualsOp}.
	 * @param ctx the parse tree
	 */
	void exitGreaterThanEqualsOp(Centura2000Parser.GreaterThanEqualsOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#equalsOp}.
	 * @param ctx the parse tree
	 */
	void enterEqualsOp(Centura2000Parser.EqualsOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#equalsOp}.
	 * @param ctx the parse tree
	 */
	void exitEqualsOp(Centura2000Parser.EqualsOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#notEqualsOp}.
	 * @param ctx the parse tree
	 */
	void enterNotEqualsOp(Centura2000Parser.NotEqualsOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#notEqualsOp}.
	 * @param ctx the parse tree
	 */
	void exitNotEqualsOp(Centura2000Parser.NotEqualsOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#bitAndOp}.
	 * @param ctx the parse tree
	 */
	void enterBitAndOp(Centura2000Parser.BitAndOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#bitAndOp}.
	 * @param ctx the parse tree
	 */
	void exitBitAndOp(Centura2000Parser.BitAndOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#bitOrOp}.
	 * @param ctx the parse tree
	 */
	void enterBitOrOp(Centura2000Parser.BitOrOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#bitOrOp}.
	 * @param ctx the parse tree
	 */
	void exitBitOrOp(Centura2000Parser.BitOrOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#concatCharOp}.
	 * @param ctx the parse tree
	 */
	void enterConcatCharOp(Centura2000Parser.ConcatCharOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#concatCharOp}.
	 * @param ctx the parse tree
	 */
	void exitConcatCharOp(Centura2000Parser.ConcatCharOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#andOp}.
	 * @param ctx the parse tree
	 */
	void enterAndOp(Centura2000Parser.AndOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#andOp}.
	 * @param ctx the parse tree
	 */
	void exitAndOp(Centura2000Parser.AndOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#orOp}.
	 * @param ctx the parse tree
	 */
	void enterOrOp(Centura2000Parser.OrOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#orOp}.
	 * @param ctx the parse tree
	 */
	void exitOrOp(Centura2000Parser.OrOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#dims}.
	 * @param ctx the parse tree
	 */
	void enterDims(Centura2000Parser.DimsContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#dims}.
	 * @param ctx the parse tree
	 */
	void exitDims(Centura2000Parser.DimsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ParenthesizedExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterParenthesizedExpression(Centura2000Parser.ParenthesizedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ParenthesizedExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitParenthesizedExpression(Centura2000Parser.ParenthesizedExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AdditiveExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression(Centura2000Parser.AdditiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AdditiveExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression(Centura2000Parser.AdditiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RelationalExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpression(Centura2000Parser.RelationalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RelationalExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpression(Centura2000Parser.RelationalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalAndExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalAndExpression(Centura2000Parser.LogicalAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalAndExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalAndExpression(Centura2000Parser.LogicalAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StringLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteralExpression(Centura2000Parser.StringLiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StringLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteralExpression(Centura2000Parser.StringLiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DecimalLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterDecimalLiteralExpression(Centura2000Parser.DecimalLiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DecimalLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitDecimalLiteralExpression(Centura2000Parser.DecimalLiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalOrExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOrExpression(Centura2000Parser.LogicalOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalOrExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOrExpression(Centura2000Parser.LogicalOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MemberDotExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterMemberDotExpression(Centura2000Parser.MemberDotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MemberDotExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitMemberDotExpression(Centura2000Parser.MemberDotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NotExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(Centura2000Parser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NotExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(Centura2000Parser.NotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ConcatExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterConcatExpression(Centura2000Parser.ConcatExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ConcatExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitConcatExpression(Centura2000Parser.ConcatExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BoleanLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBoleanLiteralExpression(Centura2000Parser.BoleanLiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BoleanLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBoleanLiteralExpression(Centura2000Parser.BoleanLiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code InequalityExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterInequalityExpression(Centura2000Parser.InequalityExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code InequalityExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitInequalityExpression(Centura2000Parser.InequalityExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FunctionExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionExpression(Centura2000Parser.FunctionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionExpression(Centura2000Parser.FunctionExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code UnaryMinusExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpression(Centura2000Parser.UnaryMinusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code UnaryMinusExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpression(Centura2000Parser.UnaryMinusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitAndExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBitAndExpression(Centura2000Parser.BitAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitAndExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBitAndExpression(Centura2000Parser.BitAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IntegerLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterIntegerLiteralExpression(Centura2000Parser.IntegerLiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IntegerLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitIntegerLiteralExpression(Centura2000Parser.IntegerLiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitOrExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterBitOrExpression(Centura2000Parser.BitOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitOrExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitBitOrExpression(Centura2000Parser.BitOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code UnaryPlusExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryPlusExpression(Centura2000Parser.UnaryPlusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code UnaryPlusExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryPlusExpression(Centura2000Parser.UnaryPlusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IdentifierNameExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierNameExpression(Centura2000Parser.IdentifierNameExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IdentifierNameExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierNameExpression(Centura2000Parser.IdentifierNameExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code EqualityExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqualityExpression(Centura2000Parser.EqualityExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EqualityExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqualityExpression(Centura2000Parser.EqualityExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MultiplicativeExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression(Centura2000Parser.MultiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MultiplicativeExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression(Centura2000Parser.MultiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NullLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void enterNullLiteralExpression(Centura2000Parser.NullLiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NullLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 */
	void exitNullLiteralExpression(Centura2000Parser.NullLiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#formalParameterList}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameterList(Centura2000Parser.FormalParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#formalParameterList}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameterList(Centura2000Parser.FormalParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#elementList}.
	 * @param ctx the parse tree
	 */
	void enterElementList(Centura2000Parser.ElementListContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#elementList}.
	 * @param ctx the parse tree
	 */
	void exitElementList(Centura2000Parser.ElementListContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#elision}.
	 * @param ctx the parse tree
	 */
	void enterElision(Centura2000Parser.ElisionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#elision}.
	 * @param ctx the parse tree
	 */
	void exitElision(Centura2000Parser.ElisionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(Centura2000Parser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(Centura2000Parser.ArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#argumentList}.
	 * @param ctx the parse tree
	 */
	void enterArgumentList(Centura2000Parser.ArgumentListContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#argumentList}.
	 * @param ctx the parse tree
	 */
	void exitArgumentList(Centura2000Parser.ArgumentListContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#reservedWord}.
	 * @param ctx the parse tree
	 */
	void enterReservedWord(Centura2000Parser.ReservedWordContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#reservedWord}.
	 * @param ctx the parse tree
	 */
	void exitReservedWord(Centura2000Parser.ReservedWordContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(Centura2000Parser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(Centura2000Parser.KeywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#eos}.
	 * @param ctx the parse tree
	 */
	void enterEos(Centura2000Parser.EosContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#eos}.
	 * @param ctx the parse tree
	 */
	void exitEos(Centura2000Parser.EosContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#eof}.
	 * @param ctx the parse tree
	 */
	void enterEof(Centura2000Parser.EofContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#eof}.
	 * @param ctx the parse tree
	 */
	void exitEof(Centura2000Parser.EofContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#nullLiteral}.
	 * @param ctx the parse tree
	 */
	void enterNullLiteral(Centura2000Parser.NullLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#nullLiteral}.
	 * @param ctx the parse tree
	 */
	void exitNullLiteral(Centura2000Parser.NullLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiteral(Centura2000Parser.BooleanLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiteral(Centura2000Parser.BooleanLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void enterIntegerLiteral(Centura2000Parser.IntegerLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void exitIntegerLiteral(Centura2000Parser.IntegerLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#decimalLiteral}.
	 * @param ctx the parse tree
	 */
	void enterDecimalLiteral(Centura2000Parser.DecimalLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#decimalLiteral}.
	 * @param ctx the parse tree
	 */
	void exitDecimalLiteral(Centura2000Parser.DecimalLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link Centura2000Parser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteral(Centura2000Parser.StringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link Centura2000Parser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteral(Centura2000Parser.StringLiteralContext ctx);
}