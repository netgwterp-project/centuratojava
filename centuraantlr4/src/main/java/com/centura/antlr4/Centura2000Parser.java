// Generated from com\centura\antlr4\Centura2000.g4 by ANTLR 4.7
package com.centura.antlr4;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class Centura2000Parser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		WS=32, OpenParen=33, CloseParen=34, Comma=35, Break=36, Loop=37, Default=38, 
		ElseIf=39, Else=40, If=41, Return=42, Continue=43, Call=44, While=45, 
		Set=46, Case=47, SelectCase=48, WhenSqlError=49, HexIntegerLiteral=50, 
		Identifier=51, StringExpression=52, WhiteSpaces=53, SingleLineComment=54, 
		LETTER=55, DIGIT=56, LineTerminator=57;
	public static final int
		RULE_statement = 0, RULE_expressionStatement = 1, RULE_elseIfStatement = 2, 
		RULE_elseStatement = 3, RULE_ifStatement = 4, RULE_selectCaseStatement = 5, 
		RULE_caseStatement = 6, RULE_loopStatement = 7, RULE_whileStatement = 8, 
		RULE_continueStatement = 9, RULE_breakStatement = 10, RULE_returnStatement = 11, 
		RULE_defaultStatement = 12, RULE_callStatement = 13, RULE_setStatement = 14, 
		RULE_whenSqlErrorStatement = 15, RULE_identifierName = 16, RULE_openBracket = 17, 
		RULE_closeBracket = 18, RULE_dotOp = 19, RULE_plusOp = 20, RULE_minusOp = 21, 
		RULE_notOp = 22, RULE_multiplyOp = 23, RULE_divideOp = 24, RULE_lessThanOp = 25, 
		RULE_moreThanOp = 26, RULE_lessThanEqualsOp = 27, RULE_greaterThanEqualsOp = 28, 
		RULE_equalsOp = 29, RULE_notEqualsOp = 30, RULE_bitAndOp = 31, RULE_bitOrOp = 32, 
		RULE_concatCharOp = 33, RULE_andOp = 34, RULE_orOp = 35, RULE_dims = 36, 
		RULE_singleExpression = 37, RULE_formalParameterList = 38, RULE_elementList = 39, 
		RULE_elision = 40, RULE_arguments = 41, RULE_argumentList = 42, RULE_reservedWord = 43, 
		RULE_keyword = 44, RULE_eos = 45, RULE_eof = 46, RULE_nullLiteral = 47, 
		RULE_booleanLiteral = 48, RULE_integerLiteral = 49, RULE_decimalLiteral = 50, 
		RULE_stringLiteral = 51;
	public static final String[] ruleNames = {
		"statement", "expressionStatement", "elseIfStatement", "elseStatement", 
		"ifStatement", "selectCaseStatement", "caseStatement", "loopStatement", 
		"whileStatement", "continueStatement", "breakStatement", "returnStatement", 
		"defaultStatement", "callStatement", "setStatement", "whenSqlErrorStatement", 
		"identifierName", "openBracket", "closeBracket", "dotOp", "plusOp", "minusOp", 
		"notOp", "multiplyOp", "divideOp", "lessThanOp", "moreThanOp", "lessThanEqualsOp", 
		"greaterThanEqualsOp", "equalsOp", "notEqualsOp", "bitAndOp", "bitOrOp", 
		"concatCharOp", "andOp", "orOp", "dims", "singleExpression", "formalParameterList", 
		"elementList", "elision", "arguments", "argumentList", "reservedWord", 
		"keyword", "eos", "eof", "nullLiteral", "booleanLiteral", "integerLiteral", 
		"decimalLiteral", "stringLiteral"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'['", "']'", "'.'", "'+'", "'-'", "'Not'", "'not'", "'NOT'", "'*'", 
		"'/'", "'<'", "'>'", "'<='", "'>='", "'='", "'!='", "'<>'", "'&'", "'|'", 
		"'||'", "'And'", "'and'", "'AND'", "'Or'", "'or'", "'OR'", "'STRING_Null'", 
		"'NUMBER_Null'", "'DATETIME_Null'", "'TRUE'", "'FALSE'", null, "'('", 
		"')'", "','", "'Break'", "'Loop'", "'Default'", "'Else If'", "'Else'", 
		null, null, "'Continue'", "'Call'", "'While'", null, "'Case'", "'Select Case'", 
		"'When SqlError'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, "WS", "OpenParen", "CloseParen", 
		"Comma", "Break", "Loop", "Default", "ElseIf", "Else", "If", "Return", 
		"Continue", "Call", "While", "Set", "Case", "SelectCase", "WhenSqlError", 
		"HexIntegerLiteral", "Identifier", "StringExpression", "WhiteSpaces", 
		"SingleLineComment", "LETTER", "DIGIT", "LineTerminator"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Centura2000.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	  
	    private boolean here(final int type) {

	        // Get the token ahead of the current index.
	        int possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 1;
	        Token ahead = _input.get(possibleIndexEosToken);

	        // Check if the token resides on the HIDDEN channel and if it's of the
	        // provided type.
	        return (ahead.getChannel() == Lexer.HIDDEN) && (ahead.getType() == type);
	    }

	    /**
	     * Returns {@code true} iff on the current index of the parser's
	     * token stream a token exists on the {@code HIDDEN} channel which
	     * either is a line terminator, or is a multi line comment that
	     * contains a line terminator.
	     *
	     * @return {@code true} iff on the current index of the parser's
	     * token stream a token exists on the {@code HIDDEN} channel which
	     * either is a line terminator, or is a multi line comment that
	     * contains a line terminator.
	     */
	    private boolean lineTerminatorAhead() {

	        // Get the token ahead of the current index.
	        int possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 1;
	        Token ahead = _input.get(possibleIndexEosToken);

	        if (ahead.getChannel() != Lexer.HIDDEN) {
	            // We're only interested in tokens on the HIDDEN channel.
	            return false;
	        }

	        if (ahead.getType() == LineTerminator) {
	            // There is definitely a line terminator ahead.
	            return true;
	        }

	        if (ahead.getType() == WhiteSpaces) {
	            // Get the token ahead of the current whitespaces.
	            possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 2;
	            ahead = _input.get(possibleIndexEosToken);
	        }

	        // Get the token's text and type.
	        String text = ahead.getText();
	        int type = ahead.getType();

	        // Check if the token is, or contains a line terminator.
	        return ( (text.contains("\r") || text.contains("\n"))) ||
	                (type == LineTerminator);
	    }                                

	public Centura2000Parser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StatementContext extends ParserRuleContext {
		public ExpressionStatementContext expressionStatement() {
			return getRuleContext(ExpressionStatementContext.class,0);
		}
		public ElseIfStatementContext elseIfStatement() {
			return getRuleContext(ElseIfStatementContext.class,0);
		}
		public ElseStatementContext elseStatement() {
			return getRuleContext(ElseStatementContext.class,0);
		}
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public LoopStatementContext loopStatement() {
			return getRuleContext(LoopStatementContext.class,0);
		}
		public WhileStatementContext whileStatement() {
			return getRuleContext(WhileStatementContext.class,0);
		}
		public ReturnStatementContext returnStatement() {
			return getRuleContext(ReturnStatementContext.class,0);
		}
		public ContinueStatementContext continueStatement() {
			return getRuleContext(ContinueStatementContext.class,0);
		}
		public BreakStatementContext breakStatement() {
			return getRuleContext(BreakStatementContext.class,0);
		}
		public CallStatementContext callStatement() {
			return getRuleContext(CallStatementContext.class,0);
		}
		public SetStatementContext setStatement() {
			return getRuleContext(SetStatementContext.class,0);
		}
		public SelectCaseStatementContext selectCaseStatement() {
			return getRuleContext(SelectCaseStatementContext.class,0);
		}
		public CaseStatementContext caseStatement() {
			return getRuleContext(CaseStatementContext.class,0);
		}
		public DefaultStatementContext defaultStatement() {
			return getRuleContext(DefaultStatementContext.class,0);
		}
		public WhenSqlErrorStatementContext whenSqlErrorStatement() {
			return getRuleContext(WhenSqlErrorStatementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statement);
		try {
			setState(119);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(104);
				expressionStatement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(105);
				elseIfStatement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(106);
				elseStatement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(107);
				ifStatement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(108);
				loopStatement();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(109);
				whileStatement();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(110);
				returnStatement();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(111);
				continueStatement();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(112);
				breakStatement();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(113);
				callStatement();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(114);
				setStatement();
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(115);
				selectCaseStatement();
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(116);
				caseStatement();
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(117);
				defaultStatement();
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(118);
				whenSqlErrorStatement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionStatementContext extends ParserRuleContext {
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public ExpressionStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterExpressionStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitExpressionStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitExpressionStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionStatementContext expressionStatement() throws RecognitionException {
		ExpressionStatementContext _localctx = new ExpressionStatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_expressionStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121);
			if (!((_input.LA(1) != Break) && (_input.LA(1) != Return) && (_input.LA(1) != Loop) && (_input.LA(1) != ElseIf) && (_input.LA(1) != Else) && (_input.LA(1) != If) && (_input.LA(1) != Default) && (_input.LA(1) != Continue))) throw new FailedPredicateException(this, "(_input.LA(1) != Break) && (_input.LA(1) != Return) && (_input.LA(1) != Loop) && (_input.LA(1) != ElseIf) && (_input.LA(1) != Else) && (_input.LA(1) != If) && (_input.LA(1) != Default) && (_input.LA(1) != Continue)");
			setState(122);
			singleExpression(0);
			setState(123);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseIfStatementContext extends ParserRuleContext {
		public TerminalNode ElseIf() { return getToken(Centura2000Parser.ElseIf, 0); }
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public ElseIfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseIfStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterElseIfStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitElseIfStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitElseIfStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElseIfStatementContext elseIfStatement() throws RecognitionException {
		ElseIfStatementContext _localctx = new ElseIfStatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_elseIfStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			match(ElseIf);
			setState(126);
			singleExpression(0);
			setState(127);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseStatementContext extends ParserRuleContext {
		public TerminalNode Else() { return getToken(Centura2000Parser.Else, 0); }
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public ElseStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterElseStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitElseStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitElseStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElseStatementContext elseStatement() throws RecognitionException {
		ElseStatementContext _localctx = new ElseStatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_elseStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(129);
			match(Else);
			setState(130);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public TerminalNode If() { return getToken(Centura2000Parser.If, 0); }
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterIfStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitIfStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitIfStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_ifStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132);
			match(If);
			setState(133);
			singleExpression(0);
			setState(134);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectCaseStatementContext extends ParserRuleContext {
		public TerminalNode SelectCase() { return getToken(Centura2000Parser.SelectCase, 0); }
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public SelectCaseStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectCaseStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterSelectCaseStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitSelectCaseStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitSelectCaseStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectCaseStatementContext selectCaseStatement() throws RecognitionException {
		SelectCaseStatementContext _localctx = new SelectCaseStatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_selectCaseStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			match(SelectCase);
			setState(137);
			singleExpression(0);
			setState(138);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseStatementContext extends ParserRuleContext {
		public TerminalNode Case() { return getToken(Centura2000Parser.Case, 0); }
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public CaseStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterCaseStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitCaseStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitCaseStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseStatementContext caseStatement() throws RecognitionException {
		CaseStatementContext _localctx = new CaseStatementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_caseStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			match(Case);
			setState(141);
			singleExpression(0);
			setState(142);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoopStatementContext extends ParserRuleContext {
		public TerminalNode Loop() { return getToken(Centura2000Parser.Loop, 0); }
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(Centura2000Parser.Identifier, 0); }
		public LoopStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loopStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterLoopStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitLoopStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitLoopStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoopStatementContext loopStatement() throws RecognitionException {
		LoopStatementContext _localctx = new LoopStatementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_loopStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			match(Loop);
			setState(146);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
				setState(145);
				match(Identifier);
				}
			}

			setState(148);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStatementContext extends ParserRuleContext {
		public TerminalNode While() { return getToken(Centura2000Parser.While, 0); }
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public WhileStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitWhileStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitWhileStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileStatementContext whileStatement() throws RecognitionException {
		WhileStatementContext _localctx = new WhileStatementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_whileStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(150);
			match(While);
			setState(151);
			singleExpression(0);
			setState(152);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContinueStatementContext extends ParserRuleContext {
		public TerminalNode Continue() { return getToken(Centura2000Parser.Continue, 0); }
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(Centura2000Parser.Identifier, 0); }
		public ContinueStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continueStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterContinueStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitContinueStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitContinueStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ContinueStatementContext continueStatement() throws RecognitionException {
		ContinueStatementContext _localctx = new ContinueStatementContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_continueStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154);
			match(Continue);
			setState(156);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
				setState(155);
				match(Identifier);
				}
			}

			setState(158);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BreakStatementContext extends ParserRuleContext {
		public TerminalNode Break() { return getToken(Centura2000Parser.Break, 0); }
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(Centura2000Parser.Identifier, 0); }
		public BreakStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_breakStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterBreakStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitBreakStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitBreakStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BreakStatementContext breakStatement() throws RecognitionException {
		BreakStatementContext _localctx = new BreakStatementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_breakStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(Break);
			setState(162);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
				setState(161);
				match(Identifier);
				}
			}

			setState(164);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStatementContext extends ParserRuleContext {
		public TerminalNode Return() { return getToken(Centura2000Parser.Return, 0); }
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public ReturnStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterReturnStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitReturnStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitReturnStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnStatementContext returnStatement() throws RecognitionException {
		ReturnStatementContext _localctx = new ReturnStatementContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_returnStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			match(Return);
			setState(168);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << OpenParen) | (1L << Break) | (1L << Loop) | (1L << Default) | (1L << ElseIf) | (1L << Else) | (1L << If) | (1L << Return) | (1L << Continue) | (1L << Call) | (1L << While) | (1L << Set) | (1L << Case) | (1L << SelectCase) | (1L << WhenSqlError) | (1L << HexIntegerLiteral) | (1L << Identifier) | (1L << StringExpression) | (1L << DIGIT))) != 0)) {
				{
				setState(167);
				singleExpression(0);
				}
			}

			setState(170);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefaultStatementContext extends ParserRuleContext {
		public TerminalNode Default() { return getToken(Centura2000Parser.Default, 0); }
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public DefaultStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defaultStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterDefaultStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitDefaultStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitDefaultStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefaultStatementContext defaultStatement() throws RecognitionException {
		DefaultStatementContext _localctx = new DefaultStatementContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_defaultStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(172);
			match(Default);
			setState(173);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallStatementContext extends ParserRuleContext {
		public TerminalNode Call() { return getToken(Centura2000Parser.Call, 0); }
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public CallStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterCallStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitCallStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitCallStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallStatementContext callStatement() throws RecognitionException {
		CallStatementContext _localctx = new CallStatementContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_callStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(175);
			match(Call);
			setState(176);
			singleExpression(0);
			setState(177);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetStatementContext extends ParserRuleContext {
		public TerminalNode Set() { return getToken(Centura2000Parser.Set, 0); }
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public EqualsOpContext equalsOp() {
			return getRuleContext(EqualsOpContext.class,0);
		}
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public SetStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterSetStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitSetStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitSetStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetStatementContext setStatement() throws RecognitionException {
		SetStatementContext _localctx = new SetStatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_setStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(179);
			match(Set);
			setState(180);
			singleExpression(0);
			setState(181);
			equalsOp();
			setState(182);
			singleExpression(0);
			setState(183);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhenSqlErrorStatementContext extends ParserRuleContext {
		public TerminalNode WhenSqlError() { return getToken(Centura2000Parser.WhenSqlError, 0); }
		public EosContext eos() {
			return getRuleContext(EosContext.class,0);
		}
		public WhenSqlErrorStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whenSqlErrorStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterWhenSqlErrorStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitWhenSqlErrorStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitWhenSqlErrorStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhenSqlErrorStatementContext whenSqlErrorStatement() throws RecognitionException {
		WhenSqlErrorStatementContext _localctx = new WhenSqlErrorStatementContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_whenSqlErrorStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			match(WhenSqlError);
			setState(186);
			eos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierNameContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(Centura2000Parser.Identifier, 0); }
		public ReservedWordContext reservedWord() {
			return getRuleContext(ReservedWordContext.class,0);
		}
		public IdentifierNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifierName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterIdentifierName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitIdentifierName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitIdentifierName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierNameContext identifierName() throws RecognitionException {
		IdentifierNameContext _localctx = new IdentifierNameContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_identifierName);
		try {
			setState(190);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(188);
				match(Identifier);
				}
				break;
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case Break:
			case Loop:
			case Default:
			case ElseIf:
			case Else:
			case If:
			case Return:
			case Continue:
			case Call:
			case While:
			case Set:
			case Case:
			case SelectCase:
			case WhenSqlError:
				enterOuterAlt(_localctx, 2);
				{
				setState(189);
				reservedWord();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpenBracketContext extends ParserRuleContext {
		public OpenBracketContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_openBracket; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterOpenBracket(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitOpenBracket(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitOpenBracket(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpenBracketContext openBracket() throws RecognitionException {
		OpenBracketContext _localctx = new OpenBracketContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_openBracket);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloseBracketContext extends ParserRuleContext {
		public CloseBracketContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_closeBracket; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterCloseBracket(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitCloseBracket(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitCloseBracket(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CloseBracketContext closeBracket() throws RecognitionException {
		CloseBracketContext _localctx = new CloseBracketContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_closeBracket);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DotOpContext extends ParserRuleContext {
		public DotOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dotOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterDotOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitDotOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitDotOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DotOpContext dotOp() throws RecognitionException {
		DotOpContext _localctx = new DotOpContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_dotOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlusOpContext extends ParserRuleContext {
		public PlusOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plusOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterPlusOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitPlusOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitPlusOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PlusOpContext plusOp() throws RecognitionException {
		PlusOpContext _localctx = new PlusOpContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_plusOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(198);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MinusOpContext extends ParserRuleContext {
		public MinusOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_minusOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterMinusOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitMinusOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitMinusOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MinusOpContext minusOp() throws RecognitionException {
		MinusOpContext _localctx = new MinusOpContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_minusOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotOpContext extends ParserRuleContext {
		public NotOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterNotOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitNotOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitNotOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotOpContext notOp() throws RecognitionException {
		NotOpContext _localctx = new NotOpContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_notOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(202);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__6) | (1L << T__7))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplyOpContext extends ParserRuleContext {
		public MultiplyOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplyOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterMultiplyOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitMultiplyOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitMultiplyOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplyOpContext multiplyOp() throws RecognitionException {
		MultiplyOpContext _localctx = new MultiplyOpContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_multiplyOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(204);
			match(T__8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DivideOpContext extends ParserRuleContext {
		public DivideOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_divideOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterDivideOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitDivideOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitDivideOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DivideOpContext divideOp() throws RecognitionException {
		DivideOpContext _localctx = new DivideOpContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_divideOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(206);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LessThanOpContext extends ParserRuleContext {
		public LessThanOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lessThanOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterLessThanOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitLessThanOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitLessThanOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LessThanOpContext lessThanOp() throws RecognitionException {
		LessThanOpContext _localctx = new LessThanOpContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_lessThanOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(208);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MoreThanOpContext extends ParserRuleContext {
		public MoreThanOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_moreThanOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterMoreThanOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitMoreThanOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitMoreThanOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MoreThanOpContext moreThanOp() throws RecognitionException {
		MoreThanOpContext _localctx = new MoreThanOpContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_moreThanOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			match(T__11);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LessThanEqualsOpContext extends ParserRuleContext {
		public LessThanEqualsOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lessThanEqualsOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterLessThanEqualsOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitLessThanEqualsOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitLessThanEqualsOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LessThanEqualsOpContext lessThanEqualsOp() throws RecognitionException {
		LessThanEqualsOpContext _localctx = new LessThanEqualsOpContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_lessThanEqualsOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GreaterThanEqualsOpContext extends ParserRuleContext {
		public GreaterThanEqualsOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_greaterThanEqualsOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterGreaterThanEqualsOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitGreaterThanEqualsOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitGreaterThanEqualsOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GreaterThanEqualsOpContext greaterThanEqualsOp() throws RecognitionException {
		GreaterThanEqualsOpContext _localctx = new GreaterThanEqualsOpContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_greaterThanEqualsOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214);
			match(T__13);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EqualsOpContext extends ParserRuleContext {
		public EqualsOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalsOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterEqualsOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitEqualsOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitEqualsOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualsOpContext equalsOp() throws RecognitionException {
		EqualsOpContext _localctx = new EqualsOpContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_equalsOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(216);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotEqualsOpContext extends ParserRuleContext {
		public NotEqualsOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notEqualsOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterNotEqualsOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitNotEqualsOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitNotEqualsOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotEqualsOpContext notEqualsOp() throws RecognitionException {
		NotEqualsOpContext _localctx = new NotEqualsOpContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_notEqualsOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218);
			_la = _input.LA(1);
			if ( !(_la==T__15 || _la==T__16) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BitAndOpContext extends ParserRuleContext {
		public BitAndOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitAndOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterBitAndOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitBitAndOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitBitAndOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BitAndOpContext bitAndOp() throws RecognitionException {
		BitAndOpContext _localctx = new BitAndOpContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_bitAndOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(220);
			match(T__17);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BitOrOpContext extends ParserRuleContext {
		public BitOrOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitOrOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterBitOrOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitBitOrOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitBitOrOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BitOrOpContext bitOrOp() throws RecognitionException {
		BitOrOpContext _localctx = new BitOrOpContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_bitOrOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConcatCharOpContext extends ParserRuleContext {
		public ConcatCharOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_concatCharOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterConcatCharOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitConcatCharOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitConcatCharOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConcatCharOpContext concatCharOp() throws RecognitionException {
		ConcatCharOpContext _localctx = new ConcatCharOpContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_concatCharOp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(224);
			match(T__19);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AndOpContext extends ParserRuleContext {
		public AndOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterAndOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitAndOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitAndOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndOpContext andOp() throws RecognitionException {
		AndOpContext _localctx = new AndOpContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_andOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__20) | (1L << T__21) | (1L << T__22))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrOpContext extends ParserRuleContext {
		public OrOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterOrOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitOrOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitOrOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrOpContext orOp() throws RecognitionException {
		OrOpContext _localctx = new OrOpContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_orOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(228);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__23) | (1L << T__24) | (1L << T__25))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DimsContext extends ParserRuleContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public DimsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dims; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterDims(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitDims(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitDims(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DimsContext dims() throws RecognitionException {
		DimsContext _localctx = new DimsContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_dims);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(231); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(230);
				match(T__0);
				}
				}
				setState(233); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__0 );
			setState(236); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(235);
				singleExpression(0);
				}
				}
				setState(238); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << OpenParen) | (1L << Break) | (1L << Loop) | (1L << Default) | (1L << ElseIf) | (1L << Else) | (1L << If) | (1L << Return) | (1L << Continue) | (1L << Call) | (1L << While) | (1L << Set) | (1L << Case) | (1L << SelectCase) | (1L << WhenSqlError) | (1L << HexIntegerLiteral) | (1L << Identifier) | (1L << StringExpression) | (1L << DIGIT))) != 0) );
			setState(240);
			match(T__1);
			setState(255);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(242); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(241);
						match(T__0);
						}
						}
						setState(244); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__0 );
					setState(247); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(246);
						singleExpression(0);
						}
						}
						setState(249); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << OpenParen) | (1L << Break) | (1L << Loop) | (1L << Default) | (1L << ElseIf) | (1L << Else) | (1L << If) | (1L << Return) | (1L << Continue) | (1L << Call) | (1L << While) | (1L << Set) | (1L << Case) | (1L << SelectCase) | (1L << WhenSqlError) | (1L << HexIntegerLiteral) | (1L << Identifier) | (1L << StringExpression) | (1L << DIGIT))) != 0) );
					setState(251);
					match(T__1);
					}
					} 
				}
				setState(257);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SingleExpressionContext extends ParserRuleContext {
		public SingleExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_singleExpression; }
	 
		public SingleExpressionContext() { }
		public void copyFrom(SingleExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParenthesizedExpressionContext extends SingleExpressionContext {
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public ParenthesizedExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterParenthesizedExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitParenthesizedExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitParenthesizedExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AdditiveExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public PlusOpContext plusOp() {
			return getRuleContext(PlusOpContext.class,0);
		}
		public MinusOpContext minusOp() {
			return getRuleContext(MinusOpContext.class,0);
		}
		public AdditiveExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterAdditiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitAdditiveExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitAdditiveExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RelationalExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public LessThanOpContext lessThanOp() {
			return getRuleContext(LessThanOpContext.class,0);
		}
		public MoreThanOpContext moreThanOp() {
			return getRuleContext(MoreThanOpContext.class,0);
		}
		public LessThanEqualsOpContext lessThanEqualsOp() {
			return getRuleContext(LessThanEqualsOpContext.class,0);
		}
		public GreaterThanEqualsOpContext greaterThanEqualsOp() {
			return getRuleContext(GreaterThanEqualsOpContext.class,0);
		}
		public RelationalExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterRelationalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitRelationalExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitRelationalExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LogicalAndExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public AndOpContext andOp() {
			return getRuleContext(AndOpContext.class,0);
		}
		public LogicalAndExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterLogicalAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitLogicalAndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitLogicalAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringLiteralExpressionContext extends SingleExpressionContext {
		public StringLiteralContext stringLiteral() {
			return getRuleContext(StringLiteralContext.class,0);
		}
		public StringLiteralExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterStringLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitStringLiteralExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitStringLiteralExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DecimalLiteralExpressionContext extends SingleExpressionContext {
		public DecimalLiteralContext decimalLiteral() {
			return getRuleContext(DecimalLiteralContext.class,0);
		}
		public DecimalLiteralExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterDecimalLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitDecimalLiteralExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitDecimalLiteralExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LogicalOrExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public OrOpContext orOp() {
			return getRuleContext(OrOpContext.class,0);
		}
		public LogicalOrExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterLogicalOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitLogicalOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitLogicalOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MemberDotExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public DotOpContext dotOp() {
			return getRuleContext(DotOpContext.class,0);
		}
		public MemberDotExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterMemberDotExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitMemberDotExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitMemberDotExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotExpressionContext extends SingleExpressionContext {
		public NotOpContext notOp() {
			return getRuleContext(NotOpContext.class,0);
		}
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public NotExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterNotExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitNotExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitNotExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConcatExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public ConcatCharOpContext concatCharOp() {
			return getRuleContext(ConcatCharOpContext.class,0);
		}
		public ConcatExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterConcatExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitConcatExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitConcatExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoleanLiteralExpressionContext extends SingleExpressionContext {
		public BooleanLiteralContext booleanLiteral() {
			return getRuleContext(BooleanLiteralContext.class,0);
		}
		public BoleanLiteralExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterBoleanLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitBoleanLiteralExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitBoleanLiteralExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InequalityExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public NotEqualsOpContext notEqualsOp() {
			return getRuleContext(NotEqualsOpContext.class,0);
		}
		public InequalityExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterInequalityExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitInequalityExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitInequalityExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionExpressionContext extends SingleExpressionContext {
		public IdentifierNameContext identifierName() {
			return getRuleContext(IdentifierNameContext.class,0);
		}
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public FunctionExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterFunctionExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitFunctionExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitFunctionExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnaryMinusExpressionContext extends SingleExpressionContext {
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public UnaryMinusExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterUnaryMinusExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitUnaryMinusExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitUnaryMinusExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitAndExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public BitAndOpContext bitAndOp() {
			return getRuleContext(BitAndOpContext.class,0);
		}
		public BitAndExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterBitAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitBitAndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitBitAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntegerLiteralExpressionContext extends SingleExpressionContext {
		public IntegerLiteralContext integerLiteral() {
			return getRuleContext(IntegerLiteralContext.class,0);
		}
		public IntegerLiteralExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterIntegerLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitIntegerLiteralExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitIntegerLiteralExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BitOrExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public BitOrOpContext bitOrOp() {
			return getRuleContext(BitOrOpContext.class,0);
		}
		public BitOrExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterBitOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitBitOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitBitOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnaryPlusExpressionContext extends SingleExpressionContext {
		public SingleExpressionContext singleExpression() {
			return getRuleContext(SingleExpressionContext.class,0);
		}
		public UnaryPlusExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterUnaryPlusExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitUnaryPlusExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitUnaryPlusExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdentifierNameExpressionContext extends SingleExpressionContext {
		public IdentifierNameContext identifierName() {
			return getRuleContext(IdentifierNameContext.class,0);
		}
		public DimsContext dims() {
			return getRuleContext(DimsContext.class,0);
		}
		public IdentifierNameExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterIdentifierNameExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitIdentifierNameExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitIdentifierNameExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualityExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public EqualsOpContext equalsOp() {
			return getRuleContext(EqualsOpContext.class,0);
		}
		public EqualityExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterEqualityExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitEqualityExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitEqualityExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiplicativeExpressionContext extends SingleExpressionContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public MultiplyOpContext multiplyOp() {
			return getRuleContext(MultiplyOpContext.class,0);
		}
		public DivideOpContext divideOp() {
			return getRuleContext(DivideOpContext.class,0);
		}
		public MultiplicativeExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterMultiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitMultiplicativeExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitMultiplicativeExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullLiteralExpressionContext extends SingleExpressionContext {
		public NullLiteralContext nullLiteral() {
			return getRuleContext(NullLiteralContext.class,0);
		}
		public NullLiteralExpressionContext(SingleExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterNullLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitNullLiteralExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitNullLiteralExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SingleExpressionContext singleExpression() throws RecognitionException {
		return singleExpression(0);
	}

	private SingleExpressionContext singleExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SingleExpressionContext _localctx = new SingleExpressionContext(_ctx, _parentState);
		SingleExpressionContext _prevctx = _localctx;
		int _startState = 74;
		enterRecursionRule(_localctx, 74, RULE_singleExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(282);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				{
				_localctx = new UnaryPlusExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(259);
				match(T__3);
				setState(260);
				singleExpression(21);
				}
				break;
			case 2:
				{
				_localctx = new UnaryMinusExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(261);
				match(T__4);
				{
				setState(262);
				singleExpression(0);
				}
				}
				break;
			case 3:
				{
				_localctx = new NotExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(263);
				notOp();
				setState(264);
				singleExpression(12);
				}
				break;
			case 4:
				{
				_localctx = new FunctionExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(266);
				identifierName();
				setState(267);
				arguments();
				}
				break;
			case 5:
				{
				_localctx = new IdentifierNameExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(269);
				identifierName();
				setState(271);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
				case 1:
					{
					setState(270);
					dims();
					}
					break;
				}
				}
				break;
			case 6:
				{
				_localctx = new StringLiteralExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(273);
				stringLiteral();
				}
				break;
			case 7:
				{
				_localctx = new NullLiteralExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(274);
				nullLiteral();
				}
				break;
			case 8:
				{
				_localctx = new BoleanLiteralExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(275);
				booleanLiteral();
				}
				break;
			case 9:
				{
				_localctx = new DecimalLiteralExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(276);
				decimalLiteral();
				}
				break;
			case 10:
				{
				_localctx = new IntegerLiteralExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(277);
				integerLiteral();
				}
				break;
			case 11:
				{
				_localctx = new ParenthesizedExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(278);
				match(OpenParen);
				setState(279);
				singleExpression(0);
				setState(280);
				match(CloseParen);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(341);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(339);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
					case 1:
						{
						_localctx = new MemberDotExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(284);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(285);
						dotOp();
						setState(286);
						singleExpression(23);
						}
						break;
					case 2:
						{
						_localctx = new BitAndExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(288);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(289);
						bitAndOp();
						setState(290);
						singleExpression(20);
						}
						break;
					case 3:
						{
						_localctx = new BitOrExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(292);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(293);
						bitOrOp();
						setState(294);
						singleExpression(19);
						}
						break;
					case 4:
						{
						_localctx = new MultiplicativeExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(296);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(299);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
						case T__8:
							{
							setState(297);
							multiplyOp();
							}
							break;
						case T__9:
							{
							setState(298);
							divideOp();
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						setState(301);
						singleExpression(18);
						}
						break;
					case 5:
						{
						_localctx = new AdditiveExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(303);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(306);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
						case T__3:
							{
							setState(304);
							plusOp();
							}
							break;
						case T__4:
							{
							setState(305);
							minusOp();
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						setState(308);
						singleExpression(17);
						}
						break;
					case 6:
						{
						_localctx = new RelationalExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(310);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(315);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
						case T__10:
							{
							setState(311);
							lessThanOp();
							}
							break;
						case T__11:
							{
							setState(312);
							moreThanOp();
							}
							break;
						case T__12:
							{
							setState(313);
							lessThanEqualsOp();
							}
							break;
						case T__13:
							{
							setState(314);
							greaterThanEqualsOp();
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						setState(317);
						singleExpression(16);
						}
						break;
					case 7:
						{
						_localctx = new InequalityExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(319);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(320);
						notEqualsOp();
						setState(321);
						singleExpression(15);
						}
						break;
					case 8:
						{
						_localctx = new EqualityExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(323);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(324);
						equalsOp();
						setState(325);
						singleExpression(14);
						}
						break;
					case 9:
						{
						_localctx = new LogicalAndExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(327);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(328);
						andOp();
						setState(329);
						singleExpression(12);
						}
						break;
					case 10:
						{
						_localctx = new LogicalOrExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(331);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(332);
						orOp();
						setState(333);
						singleExpression(11);
						}
						break;
					case 11:
						{
						_localctx = new ConcatExpressionContext(new SingleExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_singleExpression);
						setState(335);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(336);
						concatCharOp();
						setState(337);
						singleExpression(10);
						}
						break;
					}
					} 
				}
				setState(343);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FormalParameterListContext extends ParserRuleContext {
		public List<TerminalNode> Identifier() { return getTokens(Centura2000Parser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(Centura2000Parser.Identifier, i);
		}
		public FormalParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameterList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterFormalParameterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitFormalParameterList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitFormalParameterList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FormalParameterListContext formalParameterList() throws RecognitionException {
		FormalParameterListContext _localctx = new FormalParameterListContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_formalParameterList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(344);
			match(Identifier);
			setState(349);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Comma) {
				{
				{
				setState(345);
				match(Comma);
				setState(346);
				match(Identifier);
				}
				}
				setState(351);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementListContext extends ParserRuleContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public List<ElisionContext> elision() {
			return getRuleContexts(ElisionContext.class);
		}
		public ElisionContext elision(int i) {
			return getRuleContext(ElisionContext.class,i);
		}
		public ElementListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elementList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterElementList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitElementList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitElementList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElementListContext elementList() throws RecognitionException {
		ElementListContext _localctx = new ElementListContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_elementList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(353);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(352);
				elision();
				}
			}

			setState(355);
			singleExpression(0);
			setState(363);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Comma) {
				{
				{
				setState(356);
				match(Comma);
				setState(358);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(357);
					elision();
					}
				}

				setState(360);
				singleExpression(0);
				}
				}
				setState(365);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElisionContext extends ParserRuleContext {
		public ElisionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elision; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterElision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitElision(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitElision(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElisionContext elision() throws RecognitionException {
		ElisionContext _localctx = new ElisionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_elision);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(367); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(366);
				match(Comma);
				}
				}
				setState(369); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==Comma );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitArguments(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitArguments(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentsContext arguments() throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_arguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(371);
			match(OpenParen);
			setState(373);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << OpenParen) | (1L << Break) | (1L << Loop) | (1L << Default) | (1L << ElseIf) | (1L << Else) | (1L << If) | (1L << Return) | (1L << Continue) | (1L << Call) | (1L << While) | (1L << Set) | (1L << Case) | (1L << SelectCase) | (1L << WhenSqlError) | (1L << HexIntegerLiteral) | (1L << Identifier) | (1L << StringExpression) | (1L << DIGIT))) != 0)) {
				{
				setState(372);
				argumentList();
				}
			}

			setState(375);
			match(CloseParen);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentListContext extends ParserRuleContext {
		public List<SingleExpressionContext> singleExpression() {
			return getRuleContexts(SingleExpressionContext.class);
		}
		public SingleExpressionContext singleExpression(int i) {
			return getRuleContext(SingleExpressionContext.class,i);
		}
		public ArgumentListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argumentList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterArgumentList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitArgumentList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitArgumentList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentListContext argumentList() throws RecognitionException {
		ArgumentListContext _localctx = new ArgumentListContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_argumentList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(377);
			singleExpression(0);
			setState(382);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Comma) {
				{
				{
				setState(378);
				match(Comma);
				setState(379);
				singleExpression(0);
				}
				}
				setState(384);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReservedWordContext extends ParserRuleContext {
		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class,0);
		}
		public NullLiteralContext nullLiteral() {
			return getRuleContext(NullLiteralContext.class,0);
		}
		public BooleanLiteralContext booleanLiteral() {
			return getRuleContext(BooleanLiteralContext.class,0);
		}
		public ReservedWordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reservedWord; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterReservedWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitReservedWord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitReservedWord(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReservedWordContext reservedWord() throws RecognitionException {
		ReservedWordContext _localctx = new ReservedWordContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_reservedWord);
		try {
			setState(390);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Break:
			case Loop:
			case Default:
			case ElseIf:
			case Else:
			case If:
			case Return:
			case Continue:
			case Call:
			case While:
			case Set:
			case Case:
			case SelectCase:
			case WhenSqlError:
				enterOuterAlt(_localctx, 1);
				{
				setState(385);
				keyword();
				}
				break;
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
				enterOuterAlt(_localctx, 2);
				{
				setState(388);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__26:
				case T__27:
				case T__28:
					{
					setState(386);
					nullLiteral();
					}
					break;
				case T__29:
				case T__30:
					{
					setState(387);
					booleanLiteral();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public TerminalNode Break() { return getToken(Centura2000Parser.Break, 0); }
		public TerminalNode Set() { return getToken(Centura2000Parser.Set, 0); }
		public TerminalNode ElseIf() { return getToken(Centura2000Parser.ElseIf, 0); }
		public TerminalNode Else() { return getToken(Centura2000Parser.Else, 0); }
		public TerminalNode If() { return getToken(Centura2000Parser.If, 0); }
		public TerminalNode Return() { return getToken(Centura2000Parser.Return, 0); }
		public TerminalNode Continue() { return getToken(Centura2000Parser.Continue, 0); }
		public TerminalNode Call() { return getToken(Centura2000Parser.Call, 0); }
		public TerminalNode While() { return getToken(Centura2000Parser.While, 0); }
		public TerminalNode Loop() { return getToken(Centura2000Parser.Loop, 0); }
		public TerminalNode SelectCase() { return getToken(Centura2000Parser.SelectCase, 0); }
		public TerminalNode Case() { return getToken(Centura2000Parser.Case, 0); }
		public TerminalNode Default() { return getToken(Centura2000Parser.Default, 0); }
		public TerminalNode WhenSqlError() { return getToken(Centura2000Parser.WhenSqlError, 0); }
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterKeyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitKeyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitKeyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(392);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Break) | (1L << Loop) | (1L << Default) | (1L << ElseIf) | (1L << Else) | (1L << If) | (1L << Return) | (1L << Continue) | (1L << Call) | (1L << While) | (1L << Set) | (1L << Case) | (1L << SelectCase) | (1L << WhenSqlError))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EosContext extends ParserRuleContext {
		public EofContext eof() {
			return getRuleContext(EofContext.class,0);
		}
		public EosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eos; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterEos(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitEos(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitEos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EosContext eos() throws RecognitionException {
		EosContext _localctx = new EosContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_eos);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(394);
			eof();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EofContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(Centura2000Parser.EOF, 0); }
		public EofContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eof; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterEof(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitEof(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitEof(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EofContext eof() throws RecognitionException {
		EofContext _localctx = new EofContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_eof);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(396);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NullLiteralContext extends ParserRuleContext {
		public NullLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterNullLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitNullLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitNullLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullLiteralContext nullLiteral() throws RecognitionException {
		NullLiteralContext _localctx = new NullLiteralContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_nullLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(398);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__26) | (1L << T__27) | (1L << T__28))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanLiteralContext extends ParserRuleContext {
		public BooleanLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterBooleanLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitBooleanLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitBooleanLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanLiteralContext booleanLiteral() throws RecognitionException {
		BooleanLiteralContext _localctx = new BooleanLiteralContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_booleanLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(400);
			_la = _input.LA(1);
			if ( !(_la==T__29 || _la==T__30) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerLiteralContext extends ParserRuleContext {
		public List<TerminalNode> DIGIT() { return getTokens(Centura2000Parser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(Centura2000Parser.DIGIT, i);
		}
		public TerminalNode HexIntegerLiteral() { return getToken(Centura2000Parser.HexIntegerLiteral, 0); }
		public IntegerLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterIntegerLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitIntegerLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitIntegerLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegerLiteralContext integerLiteral() throws RecognitionException {
		IntegerLiteralContext _localctx = new IntegerLiteralContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_integerLiteral);
		try {
			int _alt;
			setState(408);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DIGIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(403); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(402);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(405); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case HexIntegerLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(407);
				match(HexIntegerLiteral);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DecimalLiteralContext extends ParserRuleContext {
		public List<TerminalNode> DIGIT() { return getTokens(Centura2000Parser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(Centura2000Parser.DIGIT, i);
		}
		public DecimalLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decimalLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterDecimalLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitDecimalLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitDecimalLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DecimalLiteralContext decimalLiteral() throws RecognitionException {
		DecimalLiteralContext _localctx = new DecimalLiteralContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_decimalLiteral);
		try {
			int _alt;
			setState(424);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DIGIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(410);
				match(DIGIT);
				setState(411);
				match(T__2);
				setState(415);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(412);
						match(DIGIT);
						}
						} 
					}
					setState(417);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				}
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 2);
				{
				setState(418);
				match(T__2);
				setState(420); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(419);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(422); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringLiteralContext extends ParserRuleContext {
		public TerminalNode StringExpression() { return getToken(Centura2000Parser.StringExpression, 0); }
		public StringLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).enterStringLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Centura2000Listener ) ((Centura2000Listener)listener).exitStringLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Centura2000Visitor ) return ((Centura2000Visitor<? extends T>)visitor).visitStringLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringLiteralContext stringLiteral() throws RecognitionException {
		StringLiteralContext _localctx = new StringLiteralContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_stringLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(426);
			match(StringExpression);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return expressionStatement_sempred((ExpressionStatementContext)_localctx, predIndex);
		case 37:
			return singleExpression_sempred((SingleExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expressionStatement_sempred(ExpressionStatementContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return (_input.LA(1) != Break) && (_input.LA(1) != Return) && (_input.LA(1) != Loop) && (_input.LA(1) != ElseIf) && (_input.LA(1) != Else) && (_input.LA(1) != If) && (_input.LA(1) != Default) && (_input.LA(1) != Continue);
		}
		return true;
	}
	private boolean singleExpression_sempred(SingleExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 22);
		case 2:
			return precpred(_ctx, 19);
		case 3:
			return precpred(_ctx, 18);
		case 4:
			return precpred(_ctx, 17);
		case 5:
			return precpred(_ctx, 16);
		case 6:
			return precpred(_ctx, 15);
		case 7:
			return precpred(_ctx, 14);
		case 8:
			return precpred(_ctx, 13);
		case 9:
			return precpred(_ctx, 11);
		case 10:
			return precpred(_ctx, 10);
		case 11:
			return precpred(_ctx, 9);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3;\u01af\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\5\2z\n\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\6"+
		"\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\5\t\u0095\n\t\3\t\3\t\3\n"+
		"\3\n\3\n\3\n\3\13\3\13\5\13\u009f\n\13\3\13\3\13\3\f\3\f\5\f\u00a5\n\f"+
		"\3\f\3\f\3\r\3\r\5\r\u00ab\n\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\17\3"+
		"\17\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\5\22\u00c1"+
		"\n\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31"+
		"\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3"+
		" \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\6&\u00ea\n&\r&\16&\u00eb\3&\6&\u00ef"+
		"\n&\r&\16&\u00f0\3&\3&\6&\u00f5\n&\r&\16&\u00f6\3&\6&\u00fa\n&\r&\16&"+
		"\u00fb\3&\3&\7&\u0100\n&\f&\16&\u0103\13&\3\'\3\'\3\'\3\'\3\'\3\'\3\'"+
		"\3\'\3\'\3\'\3\'\3\'\3\'\5\'\u0112\n\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'"+
		"\3\'\5\'\u011d\n\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'"+
		"\3\'\3\'\5\'\u012e\n\'\3\'\3\'\3\'\3\'\3\'\5\'\u0135\n\'\3\'\3\'\3\'\3"+
		"\'\3\'\3\'\3\'\5\'\u013e\n\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3"+
		"\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\7\'\u0156\n\'\f\'\16\'"+
		"\u0159\13\'\3(\3(\3(\7(\u015e\n(\f(\16(\u0161\13(\3)\5)\u0164\n)\3)\3"+
		")\3)\5)\u0169\n)\3)\7)\u016c\n)\f)\16)\u016f\13)\3*\6*\u0172\n*\r*\16"+
		"*\u0173\3+\3+\5+\u0178\n+\3+\3+\3,\3,\3,\7,\u017f\n,\f,\16,\u0182\13,"+
		"\3-\3-\3-\5-\u0187\n-\5-\u0189\n-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62"+
		"\3\62\3\63\6\63\u0196\n\63\r\63\16\63\u0197\3\63\5\63\u019b\n\63\3\64"+
		"\3\64\3\64\7\64\u01a0\n\64\f\64\16\64\u01a3\13\64\3\64\3\64\6\64\u01a7"+
		"\n\64\r\64\16\64\u01a8\5\64\u01ab\n\64\3\65\3\65\3\65\2\3L\66\2\4\6\b"+
		"\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVX"+
		"Z\\^`bdfh\2\t\3\2\b\n\3\2\22\23\3\2\27\31\3\2\32\34\3\2&\63\3\2\35\37"+
		"\3\2 !\2\u01bb\2y\3\2\2\2\4{\3\2\2\2\6\177\3\2\2\2\b\u0083\3\2\2\2\n\u0086"+
		"\3\2\2\2\f\u008a\3\2\2\2\16\u008e\3\2\2\2\20\u0092\3\2\2\2\22\u0098\3"+
		"\2\2\2\24\u009c\3\2\2\2\26\u00a2\3\2\2\2\30\u00a8\3\2\2\2\32\u00ae\3\2"+
		"\2\2\34\u00b1\3\2\2\2\36\u00b5\3\2\2\2 \u00bb\3\2\2\2\"\u00c0\3\2\2\2"+
		"$\u00c2\3\2\2\2&\u00c4\3\2\2\2(\u00c6\3\2\2\2*\u00c8\3\2\2\2,\u00ca\3"+
		"\2\2\2.\u00cc\3\2\2\2\60\u00ce\3\2\2\2\62\u00d0\3\2\2\2\64\u00d2\3\2\2"+
		"\2\66\u00d4\3\2\2\28\u00d6\3\2\2\2:\u00d8\3\2\2\2<\u00da\3\2\2\2>\u00dc"+
		"\3\2\2\2@\u00de\3\2\2\2B\u00e0\3\2\2\2D\u00e2\3\2\2\2F\u00e4\3\2\2\2H"+
		"\u00e6\3\2\2\2J\u00e9\3\2\2\2L\u011c\3\2\2\2N\u015a\3\2\2\2P\u0163\3\2"+
		"\2\2R\u0171\3\2\2\2T\u0175\3\2\2\2V\u017b\3\2\2\2X\u0188\3\2\2\2Z\u018a"+
		"\3\2\2\2\\\u018c\3\2\2\2^\u018e\3\2\2\2`\u0190\3\2\2\2b\u0192\3\2\2\2"+
		"d\u019a\3\2\2\2f\u01aa\3\2\2\2h\u01ac\3\2\2\2jz\5\4\3\2kz\5\6\4\2lz\5"+
		"\b\5\2mz\5\n\6\2nz\5\20\t\2oz\5\22\n\2pz\5\30\r\2qz\5\24\13\2rz\5\26\f"+
		"\2sz\5\34\17\2tz\5\36\20\2uz\5\f\7\2vz\5\16\b\2wz\5\32\16\2xz\5 \21\2"+
		"yj\3\2\2\2yk\3\2\2\2yl\3\2\2\2ym\3\2\2\2yn\3\2\2\2yo\3\2\2\2yp\3\2\2\2"+
		"yq\3\2\2\2yr\3\2\2\2ys\3\2\2\2yt\3\2\2\2yu\3\2\2\2yv\3\2\2\2yw\3\2\2\2"+
		"yx\3\2\2\2z\3\3\2\2\2{|\6\3\2\2|}\5L\'\2}~\5\\/\2~\5\3\2\2\2\177\u0080"+
		"\7)\2\2\u0080\u0081\5L\'\2\u0081\u0082\5\\/\2\u0082\7\3\2\2\2\u0083\u0084"+
		"\7*\2\2\u0084\u0085\5\\/\2\u0085\t\3\2\2\2\u0086\u0087\7+\2\2\u0087\u0088"+
		"\5L\'\2\u0088\u0089\5\\/\2\u0089\13\3\2\2\2\u008a\u008b\7\62\2\2\u008b"+
		"\u008c\5L\'\2\u008c\u008d\5\\/\2\u008d\r\3\2\2\2\u008e\u008f\7\61\2\2"+
		"\u008f\u0090\5L\'\2\u0090\u0091\5\\/\2\u0091\17\3\2\2\2\u0092\u0094\7"+
		"\'\2\2\u0093\u0095\7\65\2\2\u0094\u0093\3\2\2\2\u0094\u0095\3\2\2\2\u0095"+
		"\u0096\3\2\2\2\u0096\u0097\5\\/\2\u0097\21\3\2\2\2\u0098\u0099\7/\2\2"+
		"\u0099\u009a\5L\'\2\u009a\u009b\5\\/\2\u009b\23\3\2\2\2\u009c\u009e\7"+
		"-\2\2\u009d\u009f\7\65\2\2\u009e\u009d\3\2\2\2\u009e\u009f\3\2\2\2\u009f"+
		"\u00a0\3\2\2\2\u00a0\u00a1\5\\/\2\u00a1\25\3\2\2\2\u00a2\u00a4\7&\2\2"+
		"\u00a3\u00a5\7\65\2\2\u00a4\u00a3\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6"+
		"\3\2\2\2\u00a6\u00a7\5\\/\2\u00a7\27\3\2\2\2\u00a8\u00aa\7,\2\2\u00a9"+
		"\u00ab\5L\'\2\u00aa\u00a9\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\3\2"+
		"\2\2\u00ac\u00ad\5\\/\2\u00ad\31\3\2\2\2\u00ae\u00af\7(\2\2\u00af\u00b0"+
		"\5\\/\2\u00b0\33\3\2\2\2\u00b1\u00b2\7.\2\2\u00b2\u00b3\5L\'\2\u00b3\u00b4"+
		"\5\\/\2\u00b4\35\3\2\2\2\u00b5\u00b6\7\60\2\2\u00b6\u00b7\5L\'\2\u00b7"+
		"\u00b8\5<\37\2\u00b8\u00b9\5L\'\2\u00b9\u00ba\5\\/\2\u00ba\37\3\2\2\2"+
		"\u00bb\u00bc\7\63\2\2\u00bc\u00bd\5\\/\2\u00bd!\3\2\2\2\u00be\u00c1\7"+
		"\65\2\2\u00bf\u00c1\5X-\2\u00c0\u00be\3\2\2\2\u00c0\u00bf\3\2\2\2\u00c1"+
		"#\3\2\2\2\u00c2\u00c3\7\3\2\2\u00c3%\3\2\2\2\u00c4\u00c5\7\4\2\2\u00c5"+
		"\'\3\2\2\2\u00c6\u00c7\7\5\2\2\u00c7)\3\2\2\2\u00c8\u00c9\7\6\2\2\u00c9"+
		"+\3\2\2\2\u00ca\u00cb\7\7\2\2\u00cb-\3\2\2\2\u00cc\u00cd\t\2\2\2\u00cd"+
		"/\3\2\2\2\u00ce\u00cf\7\13\2\2\u00cf\61\3\2\2\2\u00d0\u00d1\7\f\2\2\u00d1"+
		"\63\3\2\2\2\u00d2\u00d3\7\r\2\2\u00d3\65\3\2\2\2\u00d4\u00d5\7\16\2\2"+
		"\u00d5\67\3\2\2\2\u00d6\u00d7\7\17\2\2\u00d79\3\2\2\2\u00d8\u00d9\7\20"+
		"\2\2\u00d9;\3\2\2\2\u00da\u00db\7\21\2\2\u00db=\3\2\2\2\u00dc\u00dd\t"+
		"\3\2\2\u00dd?\3\2\2\2\u00de\u00df\7\24\2\2\u00dfA\3\2\2\2\u00e0\u00e1"+
		"\7\25\2\2\u00e1C\3\2\2\2\u00e2\u00e3\7\26\2\2\u00e3E\3\2\2\2\u00e4\u00e5"+
		"\t\4\2\2\u00e5G\3\2\2\2\u00e6\u00e7\t\5\2\2\u00e7I\3\2\2\2\u00e8\u00ea"+
		"\7\3\2\2\u00e9\u00e8\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00e9\3\2\2\2\u00eb"+
		"\u00ec\3\2\2\2\u00ec\u00ee\3\2\2\2\u00ed\u00ef\5L\'\2\u00ee\u00ed\3\2"+
		"\2\2\u00ef\u00f0\3\2\2\2\u00f0\u00ee\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1"+
		"\u00f2\3\2\2\2\u00f2\u0101\7\4\2\2\u00f3\u00f5\7\3\2\2\u00f4\u00f3\3\2"+
		"\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7"+
		"\u00f9\3\2\2\2\u00f8\u00fa\5L\'\2\u00f9\u00f8\3\2\2\2\u00fa\u00fb\3\2"+
		"\2\2\u00fb\u00f9\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd"+
		"\u00fe\7\4\2\2\u00fe\u0100\3\2\2\2\u00ff\u00f4\3\2\2\2\u0100\u0103\3\2"+
		"\2\2\u0101\u00ff\3\2\2\2\u0101\u0102\3\2\2\2\u0102K\3\2\2\2\u0103\u0101"+
		"\3\2\2\2\u0104\u0105\b\'\1\2\u0105\u0106\7\6\2\2\u0106\u011d\5L\'\27\u0107"+
		"\u0108\7\7\2\2\u0108\u011d\5L\'\2\u0109\u010a\5.\30\2\u010a\u010b\5L\'"+
		"\16\u010b\u011d\3\2\2\2\u010c\u010d\5\"\22\2\u010d\u010e\5T+\2\u010e\u011d"+
		"\3\2\2\2\u010f\u0111\5\"\22\2\u0110\u0112\5J&\2\u0111\u0110\3\2\2\2\u0111"+
		"\u0112\3\2\2\2\u0112\u011d\3\2\2\2\u0113\u011d\5h\65\2\u0114\u011d\5`"+
		"\61\2\u0115\u011d\5b\62\2\u0116\u011d\5f\64\2\u0117\u011d\5d\63\2\u0118"+
		"\u0119\7#\2\2\u0119\u011a\5L\'\2\u011a\u011b\7$\2\2\u011b\u011d\3\2\2"+
		"\2\u011c\u0104\3\2\2\2\u011c\u0107\3\2\2\2\u011c\u0109\3\2\2\2\u011c\u010c"+
		"\3\2\2\2\u011c\u010f\3\2\2\2\u011c\u0113\3\2\2\2\u011c\u0114\3\2\2\2\u011c"+
		"\u0115\3\2\2\2\u011c\u0116\3\2\2\2\u011c\u0117\3\2\2\2\u011c\u0118\3\2"+
		"\2\2\u011d\u0157\3\2\2\2\u011e\u011f\f\30\2\2\u011f\u0120\5(\25\2\u0120"+
		"\u0121\5L\'\31\u0121\u0156\3\2\2\2\u0122\u0123\f\25\2\2\u0123\u0124\5"+
		"@!\2\u0124\u0125\5L\'\26\u0125\u0156\3\2\2\2\u0126\u0127\f\24\2\2\u0127"+
		"\u0128\5B\"\2\u0128\u0129\5L\'\25\u0129\u0156\3\2\2\2\u012a\u012d\f\23"+
		"\2\2\u012b\u012e\5\60\31\2\u012c\u012e\5\62\32\2\u012d\u012b\3\2\2\2\u012d"+
		"\u012c\3\2\2\2\u012e\u012f\3\2\2\2\u012f\u0130\5L\'\24\u0130\u0156\3\2"+
		"\2\2\u0131\u0134\f\22\2\2\u0132\u0135\5*\26\2\u0133\u0135\5,\27\2\u0134"+
		"\u0132\3\2\2\2\u0134\u0133\3\2\2\2\u0135\u0136\3\2\2\2\u0136\u0137\5L"+
		"\'\23\u0137\u0156\3\2\2\2\u0138\u013d\f\21\2\2\u0139\u013e\5\64\33\2\u013a"+
		"\u013e\5\66\34\2\u013b\u013e\58\35\2\u013c\u013e\5:\36\2\u013d\u0139\3"+
		"\2\2\2\u013d\u013a\3\2\2\2\u013d\u013b\3\2\2\2\u013d\u013c\3\2\2\2\u013e"+
		"\u013f\3\2\2\2\u013f\u0140\5L\'\22\u0140\u0156\3\2\2\2\u0141\u0142\f\20"+
		"\2\2\u0142\u0143\5> \2\u0143\u0144\5L\'\21\u0144\u0156\3\2\2\2\u0145\u0146"+
		"\f\17\2\2\u0146\u0147\5<\37\2\u0147\u0148\5L\'\20\u0148\u0156\3\2\2\2"+
		"\u0149\u014a\f\r\2\2\u014a\u014b\5F$\2\u014b\u014c\5L\'\16\u014c\u0156"+
		"\3\2\2\2\u014d\u014e\f\f\2\2\u014e\u014f\5H%\2\u014f\u0150\5L\'\r\u0150"+
		"\u0156\3\2\2\2\u0151\u0152\f\13\2\2\u0152\u0153\5D#\2\u0153\u0154\5L\'"+
		"\f\u0154\u0156\3\2\2\2\u0155\u011e\3\2\2\2\u0155\u0122\3\2\2\2\u0155\u0126"+
		"\3\2\2\2\u0155\u012a\3\2\2\2\u0155\u0131\3\2\2\2\u0155\u0138\3\2\2\2\u0155"+
		"\u0141\3\2\2\2\u0155\u0145\3\2\2\2\u0155\u0149\3\2\2\2\u0155\u014d\3\2"+
		"\2\2\u0155\u0151\3\2\2\2\u0156\u0159\3\2\2\2\u0157\u0155\3\2\2\2\u0157"+
		"\u0158\3\2\2\2\u0158M\3\2\2\2\u0159\u0157\3\2\2\2\u015a\u015f\7\65\2\2"+
		"\u015b\u015c\7%\2\2\u015c\u015e\7\65\2\2\u015d\u015b\3\2\2\2\u015e\u0161"+
		"\3\2\2\2\u015f\u015d\3\2\2\2\u015f\u0160\3\2\2\2\u0160O\3\2\2\2\u0161"+
		"\u015f\3\2\2\2\u0162\u0164\5R*\2\u0163\u0162\3\2\2\2\u0163\u0164\3\2\2"+
		"\2\u0164\u0165\3\2\2\2\u0165\u016d\5L\'\2\u0166\u0168\7%\2\2\u0167\u0169"+
		"\5R*\2\u0168\u0167\3\2\2\2\u0168\u0169\3\2\2\2\u0169\u016a\3\2\2\2\u016a"+
		"\u016c\5L\'\2\u016b\u0166\3\2\2\2\u016c\u016f\3\2\2\2\u016d\u016b\3\2"+
		"\2\2\u016d\u016e\3\2\2\2\u016eQ\3\2\2\2\u016f\u016d\3\2\2\2\u0170\u0172"+
		"\7%\2\2\u0171\u0170\3\2\2\2\u0172\u0173\3\2\2\2\u0173\u0171\3\2\2\2\u0173"+
		"\u0174\3\2\2\2\u0174S\3\2\2\2\u0175\u0177\7#\2\2\u0176\u0178\5V,\2\u0177"+
		"\u0176\3\2\2\2\u0177\u0178\3\2\2\2\u0178\u0179\3\2\2\2\u0179\u017a\7$"+
		"\2\2\u017aU\3\2\2\2\u017b\u0180\5L\'\2\u017c\u017d\7%\2\2\u017d\u017f"+
		"\5L\'\2\u017e\u017c\3\2\2\2\u017f\u0182\3\2\2\2\u0180\u017e\3\2\2\2\u0180"+
		"\u0181\3\2\2\2\u0181W\3\2\2\2\u0182\u0180\3\2\2\2\u0183\u0189\5Z.\2\u0184"+
		"\u0187\5`\61\2\u0185\u0187\5b\62\2\u0186\u0184\3\2\2\2\u0186\u0185\3\2"+
		"\2\2\u0187\u0189\3\2\2\2\u0188\u0183\3\2\2\2\u0188\u0186\3\2\2\2\u0189"+
		"Y\3\2\2\2\u018a\u018b\t\6\2\2\u018b[\3\2\2\2\u018c\u018d\5^\60\2\u018d"+
		"]\3\2\2\2\u018e\u018f\7\2\2\3\u018f_\3\2\2\2\u0190\u0191\t\7\2\2\u0191"+
		"a\3\2\2\2\u0192\u0193\t\b\2\2\u0193c\3\2\2\2\u0194\u0196\7:\2\2\u0195"+
		"\u0194\3\2\2\2\u0196\u0197\3\2\2\2\u0197\u0195\3\2\2\2\u0197\u0198\3\2"+
		"\2\2\u0198\u019b\3\2\2\2\u0199\u019b\7\64\2\2\u019a\u0195\3\2\2\2\u019a"+
		"\u0199\3\2\2\2\u019be\3\2\2\2\u019c\u019d\7:\2\2\u019d\u01a1\7\5\2\2\u019e"+
		"\u01a0\7:\2\2\u019f\u019e\3\2\2\2\u01a0\u01a3\3\2\2\2\u01a1\u019f\3\2"+
		"\2\2\u01a1\u01a2\3\2\2\2\u01a2\u01ab\3\2\2\2\u01a3\u01a1\3\2\2\2\u01a4"+
		"\u01a6\7\5\2\2\u01a5\u01a7\7:\2\2\u01a6\u01a5\3\2\2\2\u01a7\u01a8\3\2"+
		"\2\2\u01a8\u01a6\3\2\2\2\u01a8\u01a9\3\2\2\2\u01a9\u01ab\3\2\2\2\u01aa"+
		"\u019c\3\2\2\2\u01aa\u01a4\3\2\2\2\u01abg\3\2\2\2\u01ac\u01ad\7\66\2\2"+
		"\u01adi\3\2\2\2\"y\u0094\u009e\u00a4\u00aa\u00c0\u00eb\u00f0\u00f6\u00fb"+
		"\u0101\u0111\u011c\u012d\u0134\u013d\u0155\u0157\u015f\u0163\u0168\u016d"+
		"\u0173\u0177\u0180\u0186\u0188\u0197\u019a\u01a1\u01a8\u01aa";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}