// Generated from com\centura\antlr4\Centura2000.g4 by ANTLR 4.7
package com.centura.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link Centura2000Parser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface Centura2000Visitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(Centura2000Parser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#expressionStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionStatement(Centura2000Parser.ExpressionStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#elseIfStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseIfStatement(Centura2000Parser.ElseIfStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#elseStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseStatement(Centura2000Parser.ElseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(Centura2000Parser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#selectCaseStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectCaseStatement(Centura2000Parser.SelectCaseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#caseStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseStatement(Centura2000Parser.CaseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#loopStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoopStatement(Centura2000Parser.LoopStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#whileStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(Centura2000Parser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#continueStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueStatement(Centura2000Parser.ContinueStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#breakStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakStatement(Centura2000Parser.BreakStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#returnStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStatement(Centura2000Parser.ReturnStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#defaultStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefaultStatement(Centura2000Parser.DefaultStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#callStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallStatement(Centura2000Parser.CallStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#setStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetStatement(Centura2000Parser.SetStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#whenSqlErrorStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhenSqlErrorStatement(Centura2000Parser.WhenSqlErrorStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#identifierName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifierName(Centura2000Parser.IdentifierNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#openBracket}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpenBracket(Centura2000Parser.OpenBracketContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#closeBracket}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCloseBracket(Centura2000Parser.CloseBracketContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#dotOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDotOp(Centura2000Parser.DotOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#plusOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusOp(Centura2000Parser.PlusOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#minusOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinusOp(Centura2000Parser.MinusOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#notOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotOp(Centura2000Parser.NotOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#multiplyOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplyOp(Centura2000Parser.MultiplyOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#divideOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivideOp(Centura2000Parser.DivideOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#lessThanOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLessThanOp(Centura2000Parser.LessThanOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#moreThanOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMoreThanOp(Centura2000Parser.MoreThanOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#lessThanEqualsOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLessThanEqualsOp(Centura2000Parser.LessThanEqualsOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#greaterThanEqualsOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreaterThanEqualsOp(Centura2000Parser.GreaterThanEqualsOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#equalsOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualsOp(Centura2000Parser.EqualsOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#notEqualsOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotEqualsOp(Centura2000Parser.NotEqualsOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#bitAndOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitAndOp(Centura2000Parser.BitAndOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#bitOrOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitOrOp(Centura2000Parser.BitOrOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#concatCharOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcatCharOp(Centura2000Parser.ConcatCharOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#andOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndOp(Centura2000Parser.AndOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#orOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrOp(Centura2000Parser.OrOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#dims}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDims(Centura2000Parser.DimsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParenthesizedExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenthesizedExpression(Centura2000Parser.ParenthesizedExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AdditiveExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpression(Centura2000Parser.AdditiveExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code RelationalExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpression(Centura2000Parser.RelationalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalAndExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalAndExpression(Centura2000Parser.LogicalAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StringLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLiteralExpression(Centura2000Parser.StringLiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code DecimalLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimalLiteralExpression(Centura2000Parser.DecimalLiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicalOrExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOrExpression(Centura2000Parser.LogicalOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MemberDotExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemberDotExpression(Centura2000Parser.MemberDotExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NotExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpression(Centura2000Parser.NotExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ConcatExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcatExpression(Centura2000Parser.ConcatExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoleanLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoleanLiteralExpression(Centura2000Parser.BoleanLiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code InequalityExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInequalityExpression(Centura2000Parser.InequalityExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FunctionExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionExpression(Centura2000Parser.FunctionExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code UnaryMinusExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinusExpression(Centura2000Parser.UnaryMinusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitAndExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitAndExpression(Centura2000Parser.BitAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntegerLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerLiteralExpression(Centura2000Parser.IntegerLiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitOrExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitOrExpression(Centura2000Parser.BitOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code UnaryPlusExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryPlusExpression(Centura2000Parser.UnaryPlusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IdentifierNameExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifierNameExpression(Centura2000Parser.IdentifierNameExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code EqualityExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualityExpression(Centura2000Parser.EqualityExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MultiplicativeExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicativeExpression(Centura2000Parser.MultiplicativeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NullLiteralExpression}
	 * labeled alternative in {@link Centura2000Parser#singleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullLiteralExpression(Centura2000Parser.NullLiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#formalParameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameterList(Centura2000Parser.FormalParameterListContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#elementList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElementList(Centura2000Parser.ElementListContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#elision}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElision(Centura2000Parser.ElisionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments(Centura2000Parser.ArgumentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#argumentList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgumentList(Centura2000Parser.ArgumentListContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#reservedWord}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReservedWord(Centura2000Parser.ReservedWordContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyword(Centura2000Parser.KeywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#eos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEos(Centura2000Parser.EosContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#eof}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEof(Centura2000Parser.EofContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#nullLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullLiteral(Centura2000Parser.NullLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#booleanLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanLiteral(Centura2000Parser.BooleanLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#integerLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerLiteral(Centura2000Parser.IntegerLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#decimalLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimalLiteral(Centura2000Parser.DecimalLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link Centura2000Parser#stringLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLiteral(Centura2000Parser.StringLiteralContext ctx);
}