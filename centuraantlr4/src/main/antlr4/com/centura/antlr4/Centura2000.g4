grammar Centura2000;

@parser::members {
  
    private boolean here(final int type) {

        // Get the token ahead of the current index.
        int possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 1;
        Token ahead = _input.get(possibleIndexEosToken);

        // Check if the token resides on the HIDDEN channel and if it's of the
        // provided type.
        return (ahead.getChannel() == Lexer.HIDDEN) && (ahead.getType() == type);
    }

    /**
     * Returns {@code true} iff on the current index of the parser's
     * token stream a token exists on the {@code HIDDEN} channel which
     * either is a line terminator, or is a multi line comment that
     * contains a line terminator.
     *
     * @return {@code true} iff on the current index of the parser's
     * token stream a token exists on the {@code HIDDEN} channel which
     * either is a line terminator, or is a multi line comment that
     * contains a line terminator.
     */
    private boolean lineTerminatorAhead() {

        // Get the token ahead of the current index.
        int possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 1;
        Token ahead = _input.get(possibleIndexEosToken);

        if (ahead.getChannel() != Lexer.HIDDEN) {
            // We're only interested in tokens on the HIDDEN channel.
            return false;
        }

        if (ahead.getType() == LineTerminator) {
            // There is definitely a line terminator ahead.
            return true;
        }

        if (ahead.getType() == WhiteSpaces) {
            // Get the token ahead of the current whitespaces.
            possibleIndexEosToken = this.getCurrentToken().getTokenIndex() - 2;
            ahead = _input.get(possibleIndexEosToken);
        }

        // Get the token's text and type.
        String text = ahead.getText();
        int type = ahead.getType();

        // Check if the token is, or contains a line terminator.
        return ( (text.contains("\r") || text.contains("\n"))) ||
                (type == LineTerminator);
    }                                
}

@lexer::members {
                 
    // A flag indicating if the lexer should operate in strict mode.
    // When set to true, FutureReservedWords are tokenized, when false,
    // an octal literal can be tokenized.
    private boolean strictMode = false;

    // The most recently produced token.
    private Token lastToken = null;

    /**
     * Returns {@code true} iff the lexer operates in strict mode.
     *
     * @return {@code true} iff the lexer operates in strict mode.
     */
    public boolean getStrictMode() {
        return this.strictMode;
    }

    /**
     * Sets whether the lexer operates in strict mode or not.
     *
     * @param strictMode
     *         the flag indicating the lexer operates in strict mode or not.
     */
    public void setStrictMode(boolean strictMode) {
        this.strictMode = strictMode;
    }

    /**
     * Return the next token from the character stream and records this last
     * token in case it resides on the default channel. This recorded token
     * is used to determine when the lexer could possibly match a regex
     * literal.
     *
     * @return the next token from the character stream.
     */
    @Override
    public Token nextToken() {
        
        // Get the next token.
        Token next = super.nextToken();
        
        if (next.getChannel() == Token.DEFAULT_CHANNEL) {
            // Keep track of the last token on the default channel.                                              
            this.lastToken = next;
        }
        
        return next;
    }

    /**
     * Returns {@code true} iff the lexer can match a regex literal.
     *
     * @return {@code true} iff the lexer can match a regex literal.
     */
    private boolean isRegexPossible() {
                                       
        if (this.lastToken == null) {
            // No token has been produced yet: at the start of the input,
            // no division is possible, so a regex literal _is_ possible.
            return true;
        }
        
        switch (this.lastToken.getType()) {
            case Identifier:
            //case NullLiteral:
            //case BooleanLiteral:
            case CloseParen:
            //case DecimalLiteral:
            case HexIntegerLiteral:
            //case StringLiteral:
                return false;
            default:
                // In all other cases, a regex literal _is_ possible.
                return true;
        }
    }
}

WS : [ \t\r\n]+ -> skip ;

statement
	: expressionStatement
	| elseIfStatement
	| elseStatement
	| ifStatement
	| loopStatement
	| whileStatement
	| returnStatement
	| continueStatement
	| breakStatement
	| callStatement
	| setStatement
	| selectCaseStatement
	| caseStatement
	| defaultStatement
	| whenSqlErrorStatement
	;

expressionStatement
 : {(_input.LA(1) != Break) && (_input.LA(1) != Return) && (_input.LA(1) != Loop) && (_input.LA(1) != ElseIf) && (_input.LA(1) != Else) && (_input.LA(1) != If) && (_input.LA(1) != Default) && (_input.LA(1) != Continue)}? singleExpression eos
 ;
 	
elseIfStatement
 : ElseIf singleExpression eos
 ;
elseStatement
 : Else eos
 ;
ifStatement
 : If singleExpression eos
 ;
 
selectCaseStatement
 : SelectCase singleExpression eos
 ; 

caseStatement
 : Case singleExpression eos
 ; 
 
loopStatement
 : Loop Identifier? eos                                                      
 ;
 
whileStatement
 : While singleExpression eos                             
 ;

continueStatement
 : Continue Identifier? eos
 ;

breakStatement
 : Break Identifier? eos
 ;

returnStatement
 : Return singleExpression? eos
 ;
 
defaultStatement
 : Default eos
 ;
callStatement
 : Call singleExpression eos
 ;

setStatement
 : Set singleExpression equalsOp singleExpression eos
 ;
 
whenSqlErrorStatement
 : WhenSqlError eos
 ;



identifierName
 : Identifier
 | reservedWord
 ;   
 
openBracket                : '[';
closeBracket               : ']';
OpenParen                  : '(';
CloseParen                 : ')';
Comma                      : ',';
dotOp                        : '.';
plusOp                       : '+';
minusOp                      : '-';
notOp                        : 'Not' | 'not' | 'NOT';
multiplyOp                   : '*';
divideOp                     : '/';
lessThanOp                   : '<';
moreThanOp                   : '>';
lessThanEqualsOp             : '<=';
greaterThanEqualsOp          : '>=';
equalsOp                     : '=';
notEqualsOp                  : '!=' | '<>';
bitAndOp                     : '&';
bitOrOp                      : '|';
concatCharOp                 : '||';
andOp                        : 'And' | 'and' | 'AND';
orOp                         : 'Or' | 'or' | 'OR';
   
dims
 : '[' + singleExpression + ']' ( '[' + singleExpression + ']' )*
 ;

singleExpression
 : singleExpression dotOp singleExpression                                    	# MemberDotExpression
 | '+' singleExpression                                                   		# UnaryPlusExpression
 | '-' ( singleExpression )             										# UnaryMinusExpression
 | singleExpression bitAndOp singleExpression                                  	# BitAndExpression
 | singleExpression bitOrOp singleExpression                                  	# BitOrExpression
 | singleExpression ( multiplyOp | divideOp ) singleExpression                  # MultiplicativeExpression
 | singleExpression ( plusOp | minusOp ) singleExpression                       # AdditiveExpression
 | singleExpression ( lessThanOp | moreThanOp | 
 	                  lessThanEqualsOp | greaterThanEqualsOp ) singleExpression # RelationalExpression
 | singleExpression notEqualsOp singleExpression              			  		# InequalityExpression
 | singleExpression equalsOp singleExpression              			  			# EqualityExpression
 | notOp singleExpression                                                 		# NotExpression
 | singleExpression andOp singleExpression                                		# LogicalAndExpression
 | singleExpression orOp singleExpression                                 		# LogicalOrExpression
 | singleExpression concatCharOp singleExpression                               # ConcatExpression
 | identifierName arguments    												    # FunctionExpression
 | identifierName dims?                                                         # IdentifierNameExpression
 | stringLiteral                                                                # StringLiteralExpression
 | nullLiteral 																	# NullLiteralExpression
 | booleanLiteral																# BoleanLiteralExpression
 | decimalLiteral																# DecimalLiteralExpression
 | integerLiteral                                                        		# IntegerLiteralExpression
 | '(' singleExpression ')'                                             		# ParenthesizedExpression
 ;
 
formalParameterList
 : Identifier ( ',' Identifier )*
 ;
 
 
elementList
 : elision? singleExpression ( ',' elision? singleExpression )*
 ;


elision
 : ','+
 ;
   
arguments
 : '(' argumentList? ')'
 ;
 
argumentList
 : singleExpression ( ',' singleExpression )*
 ; 
 
reservedWord
 : keyword
 | ( nullLiteral
   | booleanLiteral
   )
 ;
   

keyword
 : Break
 | Set
 | ElseIf
 | Else
 | If
 | Return
 | Continue
 | Call
 | While
 | Loop
 | While
 | SelectCase
 | Case
 | Default
 | WhenSqlError
 ;
 
eos
 : eof
 ;

eof
 : EOF
 ;


nullLiteral
 : 'STRING_Null'
 | 'NUMBER_Null'
 | 'DATETIME_Null'
 ;

booleanLiteral
 : 'TRUE'
 | 'FALSE'
 ;

Break        : 'Break';
Loop         : 'Loop';
Default      : 'Default';
ElseIf       : 'Else If';
Else         : 'Else';
If           : 'If' | 'if' ;
Return       : 'Return' | 'return';
Continue     : 'Continue';
Call         : 'Call';
While        : 'While';
Set          : 'Set' | 'set';
Case         : 'Case' ;
SelectCase   : 'Select Case' ;
WhenSqlError : 'When SqlError';

integerLiteral
 : DIGIT+
 | HexIntegerLiteral
 ;
 
decimalLiteral
 : DIGIT '.' DIGIT*
 | '.' DIGIT+
 ;
 
HexIntegerLiteral
 : '0' [xX] HexDigit+
 ;
  
Identifier
 : IdentifierStart IdentifierPart*
 ;


stringLiteral
 : StringExpression
 ;
 
StringExpression
 : '"' DoubleStringCharacter* '"'
 | '\'' SingleStringCharacter* '\''
 ;
 
WhiteSpaces
 : [\t\u000B\u000C\u0020\u00A0]+ -> channel(HIDDEN)
 ;

SingleLineComment
 : '!' ~[=\r\n\u2028\u2029]* -> channel(HIDDEN)
 ;
 

LETTER
   : ('a' .. 'z') | ('A' .. 'Z')
   ;


DIGIT
   : ('0' .. '9')
   ;

   
   
LineTerminator
 : EOF -> channel(HIDDEN)
 ;

   
fragment IdentifierStart
 : LETTER
 | [_]
 ;

fragment IdentifierPart
 : IdentifierStart
 | DIGIT
 ;
 
fragment DoubleStringCharacter
 : ~["\\\r\n]
 | '\\' EscapeSequence
 | LineContinuation
 ;
 
fragment SingleStringCharacter
 : ~['\\\r\n]
 | '\\' EscapeSequence
 | LineContinuation
 ;

fragment LineContinuation
 : '\r\n'
 | LineTerminatorSequence 
 ;

fragment LineTerminatorSequence
 : '\r\n'
 | LineTerminator
 ;
 
fragment EscapeSequence
 : CharacterEscapeSequence
 | '0' 
 | HexEscapeSequence
 ;

fragment CharacterEscapeSequence
 : SingleEscapeCharacter
 | NonEscapeCharacter
 ;

fragment HexEscapeSequence
 : 'x' HexDigit HexDigit
 ;
 
fragment SingleEscapeCharacter
 : ['"\\bfnrtv]
 ;
fragment NonEscapeCharacter
 : ~['"\\bfnrtv0-9xu\r\n]
 ;

fragment EscapeCharacter
 : SingleEscapeCharacter
 | DecimalDigit
 | [xu]
 ;

fragment DecimalDigit
 : [0-9]
 ;

fragment HexDigit
 : [0-9a-fA-F]
 ;
 
fragment DecimalIntegerLiteral
 : '0'
 | [1-9] DecimalDigit*
 ;
