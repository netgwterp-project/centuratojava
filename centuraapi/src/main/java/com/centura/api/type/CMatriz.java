package com.centura.api.type;

public class CMatriz<T extends Reference<?>> extends CArray<CArray<T>> {

	
	private static final long serialVersionUID = -4291040072974592041L;
	
	public CMatriz() {
		super();
	}
	
	public CMatriz(int size1) {
		super(size1);
	}
	public CMatriz(int size1, int size2) {
		super(size1);
	}

	public T pos(int i, int j) {
		return pos(i).pos(j);
	}

	
	public int getUpperBoud2(int i){
		return pos(i).getUpperBoud();
	}
	
	public void setUpperBoud2(int i, int size){
		pos(i).setUpperBoud(size);
	}
	
	@Override
	public boolean isEmpty(){
		return get().isEmpty();
	}
	
	public static class ByReference<T extends Reference<T>> extends CMatriz<T> implements Reference.ByReference{
		private static final long serialVersionUID = -4291040072974592041L;
	}
	
	public static class ByValue <T extends Reference<T>> extends CMatriz<T> implements Reference.ByValue{
		private static final long serialVersionUID = -4291040072974592041L;;
	}
}
