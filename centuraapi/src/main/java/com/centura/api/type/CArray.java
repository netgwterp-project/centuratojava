package com.centura.api.type;

import java.util.Vector;

public class CArray<T extends Reference<?>> extends Reference<T> {

	
	private static final long serialVersionUID = -4291040072974592041L;
	protected Vector<T> vector;
	protected int sizeFixed = 0;
	
	public CArray() {
		super(null,null);
		vector = new Vector<>();
	}
	
	public CArray(int size) {
		super(null,null);
		sizeFixed = size;
		vector = new Vector<>(size);
	}

	public T pos(int i) {
		if((sizeFixed>0 && i >= sizeFixed) || i < 0 ) {
			throw new IndexOutOfBoundsException("index fora do range");
		}
		return vector.get(i);
	}

	
	public int getUpperBoud(){
		if(vector.size()==0) return 0;
		return vector.size()-1;
	}
	
	public void setUpperBoud(int i){
		if(i < 0 ) vector.clear();
		vector.setSize(i+1);
	}
	
	public boolean isEmpty(){
		return vector.size()==0;
	}
	
	public static class ByReference<T extends Reference<?>> extends CArray<T> implements Reference.ByReference{
		private static final long serialVersionUID = -4291040072974592041L;
	}
	
	public static class ByValue<T extends Reference<?>> extends CArray<T> implements Reference.ByValue{
		private static final long serialVersionUID = -4291040072974592041L;
	}
}
