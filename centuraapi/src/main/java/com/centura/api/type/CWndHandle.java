package com.centura.api.type;

public class CWndHandle extends CHandle {

	private static final long serialVersionUID = -7990379914137725640L;
	public static final CWndHandle hWndNULL = new CWndHandle();
	
    public CWndHandle() {
		super();
	}
    
    protected CWndHandle(Integer val){
		super(val);
	}
	
	@Override
	public ByReference getByReference() {
		return (ByReference) this;
	}
	
	@Override
	public ByValue getByValue() {
		return new ByValue(this.get());
	}
    
    
    public static class ByReference extends CWndHandle implements Reference.ByReference{
		private static final long serialVersionUID = -7990379914137725640L;
	}
    
    public static class ByValue extends CWndHandle implements Reference.ByValue{
		public ByValue(Integer val) {
			super(val);
		}

		private static final long serialVersionUID = -7990379914137725640L;
	}
}
