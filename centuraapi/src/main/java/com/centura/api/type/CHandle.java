package com.centura.api.type;

public class CHandle extends CAbstractNumber<Integer> {

	private static final long serialVersionUID = -7990379914137725640L;

	public CHandle() {
		super(0,null);
	}
	
	protected CHandle(Integer val){
		super(val,null);
	}
	
	public static class ByReference extends CHandle implements Reference.ByReference{
		private static final long serialVersionUID = -7990379914137725640L;
	}
	
	public static class ByValue extends CHandle implements Reference.ByValue{
		private static final long serialVersionUID = -7990379914137725640L;
	}
	
}
