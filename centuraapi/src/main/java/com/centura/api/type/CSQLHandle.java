package com.centura.api.type;

public class CSQLHandle extends CHandle {

	private static final long serialVersionUID = -7990379914137725640L;
	public CSQLHandle() {
		super();
	}
	
	protected CSQLHandle(Integer val){
		super(val);
	}
	
	@Override
	public ByReference getByReference() {
		return (ByReference) this;
	}
	
	@Override
	public ByValue getByValue() {
		return new ByValue(this.get());
	}
    
    public static class ByReference extends CSQLHandle implements Reference.ByReference{
		private static final long serialVersionUID = -7990379914137725640L;
	}
    
    public static class ByValue extends CSQLHandle implements Reference.ByValue{
		public ByValue(Integer val) {
			super(val);
		}

		private static final long serialVersionUID = -7990379914137725640L;
	}
}
