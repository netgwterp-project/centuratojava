package com.centura.api.type;

public class CString extends Reference<String> {

	private static final long serialVersionUID = -2619338347947276806L;

	public static final CString STRING_Null = new CString(null);

	public static CString wrapper(String val){
		return new CString(val);
	}
	
	public static CString wrapper(CString val){
		return new CString(val.get());
	}
	
	public CString() {
		super("",null);
	}

	public CString(String string){
		super("",string);
	}
	
	@Override
	public ByReference getByReference(){
		return (ByReference) this;
	}
	
	@Override
	public ByValue getByValue(){
		return new ByValue(this.get());
	}

	public static class ByReference extends CString implements Reference.ByReference{
		private static final long serialVersionUID = -2619338347947276806L;
	}
	
	public static class ByValue extends CString implements Reference.ByValue{
		public ByValue(String string) {
			super(string);
		}

		private static final long serialVersionUID = -2619338347947276806L;
	}
}
