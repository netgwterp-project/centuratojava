package com.centura.api.type;

public class CFile extends CHandle {

	private static final long serialVersionUID = -2619338347947276806L;
	
	public CFile() {
		super();
	}
	
	protected CFile(Integer val){
		super(val);
	}
	
	@Override
	public ByReference getByReference() {
		return (ByReference) this;
	}
	
	@Override
	public ByValue getByValue() {
		return new ByValue(this.get());
	}
    
	public static class ByReference extends CFile implements Reference.ByReference{
		private static final long serialVersionUID = -2619338347947276806L;
	}
	public static class ByValue extends CFile implements Reference.ByValue{
		public ByValue(Integer integer) {
			super(integer);
		}

		private static final long serialVersionUID = -2619338347947276806L;
	}
}
