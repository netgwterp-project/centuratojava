package com.centura.api.type;

public class CBoolean extends Reference<Boolean> {

	public static CBoolean wrapper(int val){
		return new CBoolean(val!=0);
	}
	
	public static CBoolean wrapper(long val){
		return new CBoolean(val!=0);
	}
	
	public static CBoolean wrapper(float val){
		return new CBoolean(val!=0);
	}
	
	public static CBoolean wrapper(double val){
		return new CBoolean(val!=0);
	}
	
	public static CBoolean wrapper(Integer val){
		return new CBoolean(val != null &&  val!=0);
	}
	
	public static CBoolean wrapper(Long val){
		return new CBoolean(val != null &&  val!=0);
	}
	
	public static CBoolean wrapper(Float val){
		return new CBoolean(val != null &&  val!=0);
	}
	
	public static CBoolean wrapper(Double val){
		return new CBoolean(val != null &&  val != 0);
	}
	
	public static CBoolean wrapper(boolean val){
		return new CBoolean(val);
	}
	
	public static CBoolean wrapper(Boolean val){
		return new CBoolean(val != null &&  val);
	}
	
	public static CBoolean wrapper(CNumber val){
		return wrapper(val.get());
	}
	
	private static final long serialVersionUID = -6961293170240709153L;
	public static final CBoolean TRUE = (new CBoolean(true));
	public static final CBoolean FALSE = (new CBoolean());

	protected CBoolean() {
		super(false,false);
	}

	protected CBoolean(Boolean val) {
		super(false,val);
	}
	
	@Override
	public ByReference getByReference(){
		return (ByReference) this;
	}
	
	@Override
	public ByValue getByValue(){
		return new ByValue(this);
	}
	
	public static class ByReference extends CBoolean implements Reference.ByReference{
		private static final long serialVersionUID = 6961293170240709153L;
	}
	
	public static class ByValue extends CBoolean implements Reference.ByValue{
		
		private static final long serialVersionUID = 6961293170240709153L;
		
		public ByValue(CBoolean val) {
			super(val.get());
		}

		
	}

	public static Reference<Boolean> wrapper(CBoolean val) {
		return new CBoolean(val.get());
	}
	
}
