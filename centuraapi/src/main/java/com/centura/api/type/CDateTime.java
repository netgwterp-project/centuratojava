package com.centura.api.type;

import java.util.Date;

public class CDateTime extends Reference<Date> {
	private static final long serialVersionUID = 7877660823218724934L;
	public static final CDateTime DATETIME_Null = new CDateTime(null);

	public CDateTime() {
		super(null, new Date());
	}

	public CDateTime(Date date){
		super(null, date);
	}
	
	public ByReference getByReference(){
		return (ByReference) this;
	}
	
	public ByValue getByValue(){
		return new ByValue(this);
	}
	
	public static class ByReference extends CDateTime implements Reference.ByReference{
		private static final long serialVersionUID = 7877660823218724934L;
	}
	
	public static class ByValue extends CDateTime implements Reference.ByValue{
		private static final long serialVersionUID = 7877660823218724934L;
		
		public ByValue(CDateTime val) {
			super(val.get());
		}
	}
}
