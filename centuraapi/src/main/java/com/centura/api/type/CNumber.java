package com.centura.api.type;

public class CNumber extends CAbstractNumber<Double> {


	public static CNumber wrapper(int val){
		return new CNumber((double) val);
	}
	
	public static CNumber wrapper(long val){
		return new CNumber((double) val);
	}
	
	public static CNumber wrapper(float val){
		return new CNumber((double) val);
	}
	
	public static CNumber wrapper(double val){
		return new CNumber(val);
	}
	
	public static CNumber wrapper(boolean val){
		return new CNumber(val?1d:0d);
	}
	
	public static CNumber wrapper(Boolean val){
		return new CNumber(val != null && val?1d:0d);
	}
	
	public static CNumber wrapper(Integer val){
		return new CNumber(val.doubleValue());
	}
	
	public static CNumber wrapper(Long val){
		return new CNumber(val.doubleValue());
	}
	
	public static CNumber wrapper(Float val){
		return new CNumber(val.doubleValue());
	}
	
	public static CNumber wrapper(Double val){
		return new CNumber(val);
	}
	
	private static final long serialVersionUID = 6575250232653326318L;

	public static final CNumber NUMBER_Null = new CNumber();

	public static final CNumber ZERO = new CNumber(0d);

	public CNumber() {
		super(0d,null);
	}

	protected CNumber(Double number){
		super(0d,number);
	}
	
	public ByReference getByReference(){
		return (ByReference) this;
	}
	
	public ByValue getByValue(){
		return new ByValue(this.get());
	}
	
	public static class ByValue extends CNumber implements Reference.ByValue{
		public ByValue(Double number) {
			super(number);
		}

		private static final long serialVersionUID = 6575250232653326318L;
	}
	
	public static class ByReference extends CNumber implements Reference.ByReference{
		private static final long serialVersionUID = 6575250232653326318L;
	}
	
	public void set(int value) {
		super.set((double) value);
	}
	
	public void set(long value) {
		super.set((double) value);
	}
	
	public void set(float value) {
		super.set((double) value);
	}
	
}
