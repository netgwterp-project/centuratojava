package com.centura.api.type;

import java.io.Serializable;

public class CStruct extends Reference<Serializable> {

	
	private static final long serialVersionUID = 7751059641125236042L;

	public CStruct() {
		super(null,null);
	}
	
	public CStruct(Serializable init) {
		super(null,init);
	}
	
	public static class ByReference extends CStruct implements Reference.ByReference{
		private static final long serialVersionUID = 7751059641125236042L;
	}
	
	public static class ByValue extends CStruct implements Reference.ByValue{
		private static final long serialVersionUID = 7751059641125236042L;
	}

}
