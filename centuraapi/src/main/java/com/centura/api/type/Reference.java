package com.centura.api.type;

import java.io.Serializable;

public abstract class Reference<V extends Serializable> implements Serializable, Comparable<V> {

	private static final long serialVersionUID = 2861735301884760714L;
	private V value;
	private boolean nullValue = true;
	protected V valueNull;

	public interface ByReference { }
	public interface ByValue { }
	protected Reference(V valueNull, V initilize){
		this.valueNull = valueNull;
		set(initilize);
	}

	public V get() {
		return value;
	}
	

	public void set(V value){
		if(value != null){
			setNullValue(false);
			this.value = value;
		}else{
			setNullValue(true);
			this.value = valueNull;
		}
	}

	public void set(Reference<V> value){
		if(value != null){
			set(value.get());
		}else{
			set((V)null);
		}
	}

	protected boolean isNullValue() {
		return nullValue;
	}

	public boolean equalsTo(Reference<V> obj) {
		return get().equals(obj.get());
	}

	public boolean greaterThanTo(Reference<V> ref){
		return compareTo(ref.get())>0;
	}

	public boolean greaterThanOrEqualTo(Reference<V> ref){
		return equalsTo(ref)?true:greaterThanTo(ref);
	}

	public boolean lessThanTo(Reference<V> ref){
		return compareTo(ref.get())<0;
	}

	public boolean lessThanOrEqualTo(Reference<V> ref){
		return equalsTo(ref)?true:lessThanTo(ref);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public int compareTo(V o) {
		if(get() instanceof Comparable){
			return ((Comparable)get()).compareTo(o);
		}
		return 0;
	}

	protected void setNullValue(boolean nullValue) {
		this.nullValue = nullValue;
	}

	@Override
	public String toString() {
		return isNullValue() ? "NULL": value.toString();
	}
	
	public ByReference getByReference(){
		return (ByReference) this;
	}
	
	public ByValue getByValue(){
		return (ByValue) this;
	}
}
