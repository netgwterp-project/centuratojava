package com.centura.api;

import com.centura.api.cdk.AbstractApplication;
import com.centura.api.cdk.AbstractGeneralWindowClass;
import com.centura.api.cdk.HandleFactory;
import com.centura.api.cdk.IApplication;
import com.centura.api.cdk.IMessage;
import com.centura.api.type.CNumber;

public class DefaultApplication extends AbstractApplication {
	
	Form form = new Form();
	
	public DefaultApplication() {
		super();
		addMessageHandle(this,SAM_AppStartup,this::teste);
	}

	public static void main(String[] args) {
		IApplication.Run(new DefaultApplication() );
	}
	
	public CNumber teste(IMessage msg){
		System.out.println("Get Application: " + HandleFactory.getApplication());
		//sendMessage(form, VTM_LeftClick, msg);
		return sendMessageClass(SAM_AppStartup, msg);
	}
	
	class Form extends AbstractGeneralWindowClass{
		
		public Form() {
			super();
			//addMessageHandle(this, VTM_LeftClick, this::teste);
		}
		
		public CNumber teste(IMessage msg){
			System.out.println("Teste Form: " + HandleFactory.getApplication());
			return CNumber.NUMBER_Null;
		}
	}

}
