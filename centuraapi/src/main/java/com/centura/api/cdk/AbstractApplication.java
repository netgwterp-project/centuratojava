package com.centura.api.cdk;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import com.centura.api.type.CHandle;
import com.centura.api.type.CNumber;
import com.centura.api.type.CString;
import com.centura.api.type.CWndHandle;

public abstract class AbstractApplication implements IApplication, IWndHandle {

	private final Set<CFormat>								formats			= new HashSet<>();
	private final Map<String, Map<String, String>>			windowsDefaults	= new HashMap<>();
	private CWndHandle									appHandle;
	private CString											description		= CString.STRING_Null;

	public AbstractApplication() {
		HandleFactory.getNewHandle(this);
		addMessageHandle(this, SAM_AppStartup, this::OnAppStartup);
		addMessageHandle(this, SAM_AppExit, this::OnAppExit);
	}

	@Override
	public void addFormat(CFormat cFormat) {
		formats.add(cFormat);

	}


	@Override
	public CNumber exit() {
		CNumber ret = sendMessage(SAM_AppExit, new IMessage());
		int exit = 0;
		if(ret!=null && ret.equals(CNumber.NUMBER_Null))
			exit = ret.get().intValue();
		System.exit(exit);
		return null;
	}

	@Override
	public CHandle getAppHandle() {
		return appHandle;
	}

	@Override
	public CString getDescription() {
		return description;
	}

	@Override
	public CFormat getFormat(CString string) {
		Stream<CFormat> filter = formats.stream().filter(t -> t.getFormat().equals(string));
		return filter.count() == 1 ? (filter.toArray(size -> new CFormat[size]))[0] : null;
	}

	@Override
	public CFormat[] getFormats() {
		return formats.toArray(new CFormat[formats.size()]);
	}

	@Override
	public CFormat[] getFormats(FormatType formatType) {
		Stream<CFormat> filter = formats.stream().filter(t -> t.getFormatType().equals(formatType));
		return filter.toArray(size -> new CFormat[size]);
	}
	
	@Override
	public CString getWindowsDefaults(String clazz, String key) {
		Map<String, String> map = windowsDefaults.get(clazz);
		if (map != null) {
			return new CString(map.get(key));
		}
		return CString.STRING_Null;
	}


	@Override
	public void setDescription(CString description) {
		this.description = description;
	}

	@Override
	public void setWindowsDefaults(String clazz, String key, String val) {
		Map<String, String> map = windowsDefaults.get(clazz);
		if (map == null) {
			map = new HashMap<>();
		}
		map.put(key, val);
		windowsDefaults.put(clazz, map);
	}

	@Override
	public CNumber start() {
		return sendMessage(SAM_AppStartup, new IMessage());
	}
	
	public CNumber OnAppStartup(IMessage message){
		System.out.println("OnAppStartup");
		return CNumber.NUMBER_Null;
	}
	
	public CNumber OnAppExit(IMessage message){
		System.out.println("OnAppExit");
		return CNumber.NUMBER_Null;
	}
	
	@Override
	public void sqlError() {
		
	}

	@Override
	public CWndHandle getWndHandle() {
		return appHandle;
	}

	@Override
	public void setWndHandle(CWndHandle handle) {
		appHandle = handle;
		
	}
	
	@Override
	public String toString() {
		return "Application: " + this.getClass().getSimpleName() + " Handle: " + getWndHandle();
	}
}
