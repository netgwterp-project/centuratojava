package com.centura.api.cdk;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.centura.api.type.CWndHandle;

public class HandleFactory {
	
	private static final Map<CWndHandle, Map<Integer, IMessageHandle>>	messageHandles	 = new HashMap<>();
	private static final Map<CWndHandle, IWndHandle> wndHandles	 = new HashMap<>();
	private static IApplication application;
	
	private static Integer nextHandle = 1;
	

	public static IApplication getApplication(){
		return application;
	}

	public static CWndHandle getNewHandle(IWndHandle wndHandle) {
		if(wndHandle instanceof IApplication)
			application = (IApplication) wndHandle;
		CWndHandle cWndHandle = new CWndHandle();
		cWndHandle.set(nextHandle++);
		wndHandle.setWndHandle(cWndHandle);
		System.out.println("New Handle: " + wndHandle.getWndHandle() + " Class: " + wndHandle.getClass().getSimpleName());
		wndHandles.put(cWndHandle, wndHandle);
		return cWndHandle;
	}
	public static void addMessageHandle(IWndHandle wndHandle, IMessageHandle handle) {
		addMessageHandle(wndHandle.getWndHandle(), handle);
	}
	public static void addMessageHandle(CWndHandle wndHandle, IMessageHandle handle) {
		Map<Integer, IMessageHandle> map = messageHandles.get(wndHandle);
		if(map == null){
			map = new HashMap<>();
			messageHandles.put(wndHandle, map);
		}
		IMessageHandle messageHandle = map.get(handle.getMESSAGE());
		if(messageHandle==null){
			map.put(handle.getMESSAGE(), handle);
			System.out.println("Message: " + handle.getMESSAGE() + " for Handle: " + wndHandle);
		}else{
			map.remove(messageHandle.getMESSAGE());
			map.put(messageHandle.getCLASSMESSAGE(), messageHandle);
			map.put(handle.getMESSAGE(), handle);
			System.out.println("Message: " + handle.getMESSAGE() + " for Handle: " + wndHandle + " hasClassMessage: " + messageHandle.getCLASSMESSAGE() );
		}
		
	}
	public static IMessageHandle getMessageHandle(CWndHandle wndHandle, Integer mESSAGE) {
		Map<Integer, IMessageHandle> map = messageHandles.get(wndHandle);
		IMessageHandle messageHandle = null;
		if(map!=null) {
             messageHandle  = map.get(mESSAGE);
		}
		return messageHandle;
	}
	
	public static Set<IMessageHandle> getMessageHandles(CWndHandle wndHandle) {
		Map<Integer, IMessageHandle> map = messageHandles.get(wndHandle);
		Set<IMessageHandle> messageHandle = new TreeSet<>();
		if(map!=null) {
             messageHandle.addAll(map.values());
		}
		return messageHandle;
	}

}
