package com.centura.api.cdk;

import com.centura.api.impl.DefaultCenturaAPI;
import com.centura.api.type.CArray;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CFile;
import com.centura.api.type.CHandle;
import com.centura.api.type.CNumber;
import com.centura.api.type.CSQLHandle;
import com.centura.api.type.CString;
import com.centura.api.type.CStruct;
import com.centura.api.type.CWndHandle;

public interface CenturaAPI {
  static  ICenturaAPI centuraAPIimpl = new DefaultCenturaAPI();
  /**
  * #define AC_Dynamic      0x80000000 file: centura.h */
 public static  int AC_Dynamic = 0x80000000;

 /**
  * #define CF_CaptionGrayed   0x00000020L file: tb.h */
 public static  int CF_CaptionGrayed = 0x00000020;

 /**
  * #define CF_CenterJustify   0x00000100L file: tb.h */
 public static  int CF_CenterJustify = 0x00000100;

 /**
  * #define CF_ColCntrlEtched     0x01000000L file: tb.h */
 public static  int CF_ColCntrlEtched = 0x01000000;

 /**
  * #define CF_ColCntrlIgnoreCase 0x00200000L file: tb.h */
 public static  int CF_ColCntrlIgnoreCase = 0x00200000;

 /**
  * #define CF_ColIsBiDi          0x00400000L file: tb.h */
 public static  int CF_ColIsBiDi = 0x00400000;

 /**
  * #define CF_ColIsCheckBox      0x00080000L file: tb.h */
 public static  int CF_ColIsCheckBox = 0x00080000;

 /**
  * #define CF_ColIsMultiLine     0x00100000L file: tb.h */
 public static  int CF_ColIsMultiLine = 0x00100000;

 /**
  * #define CF_ColIsRadioButton   0x00040000L file: tb.h */
 public static  int CF_ColIsRadioButton = 0x00040000;

 /**
  * #define CF_Editable        0x00000002L file: tb.h */
 public static  int CF_Editable = 0x00000002;

 /**
  * #define CF_FmtInvisible    0x00000010L file: tb.h */
 public static  int CF_FmtInvisible = 0x00000010;

 /**
  * #define CF_FmtLowerCase    0x00000800L file: tb.h */
 public static  int CF_FmtLowerCase = 0x00000800;

 /**
  * #define CF_FmtUpperCase    0x00000400L file: tb.h */
 public static  int CF_FmtUpperCase = 0x00000400;

 /**
  * #define CF_FormView1Line      0x00010000L file: tb.h */
 public static  int CF_FormView1Line = 0x00010000;

 /**
  * #define CF_Inserted        0x00000008L file: tb.h */
 public static  int CF_Inserted = 0x00000008;

 /**
  * #define CF_LeftJustify     0x00000000L file: tb.h */
 public static  int CF_LeftJustify = 0x00000000;

 /**
  * #define CF_MultilineCell      0x00800000L file: tb.h */
 public static  int CF_MultilineCell = 0x00800000;

 /**
  * #define CF_NoChangeCaption 0x00000040L file: tb.h */
 public static  int CF_NoChangeCaption = 0x00000040;

 /**
  * #define CF_NoEditText      0x00000200L file: tb.h */
 public static  int CF_NoEditText = 0x00000200;

 /**
  * #define CF_Overflow        0x00001000L    // display overflow char  file: tb.h */
 public static  int CF_Overflow = 0x00001000;

 /**
  * #define CF_RightJustify    0x00000080L file: tb.h */
 public static  int CF_RightJustify = 0x00000080;

 /**
  * #define CF_Selected        0x00000004L file: tb.h */
 public static  int CF_Selected = 0x00000004;

 /**
  * #define CF_Text   1 file: centura.h */
 public static  int CF_Text = 1;

 /**
  * #define CF_User            0x00002000L file: tb.h */
 public static  int CF_User = 0x00002000;

 /**
  * #define CF_UserMax         0x00008000L file: tb.h */
 public static  int CF_UserMax = 0x00008000;

 /**
  * #define CF_UserRange       0x0000e000L file: tb.h */
 public static  int CF_UserRange = 0x0000e000;

 /**
  * #define CF_Visible         0x00000001L file: tb.h */
 public static  int CF_Visible = 0x00000001;

 /**
  * #define CF_XYPosSpecified     0x00020000L file: tb.h */
 public static  int CF_XYPosSpecified = 0x00020000;

 /**
  * #define CLASS_UDV_HEADER_SIZE 12  // size of header area for UDV file: centura200.h */
 public static  int CLASS_UDV_HEADER_SIZE = 12;

 /**
  * #define CLASS_UDV_OFFSET_SIZE  4  // size of offset pointer file: centura200.h */
 public static  int CLASS_UDV_OFFSET_SIZE = 4;

 /**
  * #define CLS_BOTH           2 file: tb.h */
 public static  int CLS_BOTH = 2;

 /**
  * #define CLS_COLUMNS        1 file: tb.h */
 public static  int CLS_COLUMNS = 1;

 /**
  * #define CLS_ROWS           0 file: tb.h */
 public static  int CLS_ROWS = 0;

 /**
  * #define COLOR_Black            0x000000 file: centura.h */
 public static  int COLOR_Black = 0x000000;

 /**
  * #define COLOR_Blue             0xff0000 file: centura.h */
 public static  int COLOR_Blue = 0xff0000;

 /**
  * #define COLOR_Burgundy         0x000050 file: centura.h */
 public static  int COLOR_Burgundy = 0x000050;

 /**
  * #define COLOR_Charcoal         0x404040 file: centura.h */
 public static  int COLOR_Charcoal = 0x404040;

 /**
  * #define COLOR_Chartreuse       0x008080 file: centura.h */
 public static  int COLOR_Chartreuse = 0x008080;

 /**
  * #define COLOR_Cyan             0xffff00 file: centura.h */
 public static  int COLOR_Cyan = 0xffff00;

 /**
  * #define COLOR_DarkAqua         0xbfbf25 file: centura.h */
 public static  int COLOR_DarkAqua = 0xbfbf25;

 /**
  * #define COLOR_DarkBlue         0x800000 file: centura.h */
 public static  int COLOR_DarkBlue = 0x800000;

 /**
  * #define COLOR_DarkGray         0x808080 file: centura.h */
 public static  int COLOR_DarkGray = 0x808080;

 /**
  * #define COLOR_DarkGreen        0x00ff00 file: centura.h */
 public static  int COLOR_DarkGreen = 0x00ff00;

 /**
  * #define COLOR_DarkRed          0x000080 file: centura.h */
 public static  int COLOR_DarkRed = 0x000080;

 /**
  * #define COLOR_Default          0x30000000 file: centura.h */
 public static  int COLOR_Default = 0x30000000;

 /**
  * #define COLOR_Gray             0xC0C0C0 file: centura.h */
 public static  int COLOR_Gray = 0xC0C0C0;

 /**
  * #define COLOR_Green            0x008000 file: centura.h */
 public static  int COLOR_Green = 0x008000;

 /**
  * #define COLOR_IndexCellText       100 file: centura.h */
 public static  int COLOR_IndexCellText = 100;

 /**
  * #define COLOR_IndexWindow         5 file: centura.h */
 public static  int COLOR_IndexWindow = 5;

 /**
  * #define COLOR_IndexWindowText     8 file: centura.h */
 public static  int COLOR_IndexWindowText = 8;

 /**
  * #define COLOR_Jade             0x404000 file: centura.h */
 public static  int COLOR_Jade = 0x404000;

 /**
  * #define COLOR_LightAqua        0xffff7f file: centura.h */
 public static  int COLOR_LightAqua = 0xffff7f;

 /**
  * #define COLOR_LightGray        0xe2e2e2 file: centura.h */
 public static  int COLOR_LightGray = 0xe2e2e2;

 /**
  * #define COLOR_LightGreen       0xbfffbf file: centura.h */
 public static  int COLOR_LightGreen = 0xbfffbf;

 /**
  * #define COLOR_LightPeriwinkle  0xbfffbf file: centura.h */
 public static  int COLOR_LightPeriwinkle = 0xbfffbf;

 /**
  * #define COLOR_Magenta          0xff00ff file: centura.h */
 public static  int COLOR_Magenta = 0xff00ff;

 /**
  * #define COLOR_Maize            0x80ffff file: centura.h */
 public static  int COLOR_Maize = 0x80ffff;

 /**
  * #define COLOR_Marigold         0x7f00ff file: centura.h */
 public static  int COLOR_Marigold = 0x7f00ff;

 /**
  * #define COLOR_MidnightBlue     0x800000 file: centura.h */
 public static  int COLOR_MidnightBlue = 0x800000;

 /**
  * #define COLOR_Orchid           0xff7fff file: centura.h */
 public static  int COLOR_Orchid = 0xff7fff;

 /**
  * #define COLOR_Periwinkle       0xff7f7f file: centura.h */
 public static  int COLOR_Periwinkle = 0xff7f7f;

 /**
  * #define COLOR_Purple           0x800080 file: centura.h */
 public static  int COLOR_Purple = 0x800080;

 /**
  * #define COLOR_Red              0x0000ff file: centura.h */
 public static  int COLOR_Red = 0x0000ff;

 /**
  * #define COLOR_RoyalBlue        0x5d0000 file: centura.h */
 public static  int COLOR_RoyalBlue = 0x5d0000;

 /**
  * #define COLOR_Salmon           0x7f7fff file: centura.h */
 public static  int COLOR_Salmon = 0x7f7fff;

 /**
  * #define COLOR_Sky              0xffffbf file: centura.h */
 public static  int COLOR_Sky = 0xffffbf;

 /**
  * #define COLOR_SysWindow        0x10000000 file: centura.h */
 public static  int COLOR_SysWindow = 0x10000000;

 /**
  * #define COLOR_SysWindowText    0x20000000 file: centura.h */
 public static  int COLOR_SysWindowText = 0x20000000;

 /**
  * #define COLOR_Teal             0x808000 file: centura.h */
 public static  int COLOR_Teal = 0x808000;

 /**
  * #define COLOR_White            0xffffff file: centura.h */
 public static  int COLOR_White = 0xffffff;

 /**
  * #define COLOR_Yellow           0x00ffff file: centura.h */
 public static  int COLOR_Yellow = 0x00ffff;

 /**
  * #define COLUMN_EXTRA    2 file: tb.h */
 public static  int COLUMN_EXTRA = 2;

 /**
  * #define CREATE_AsChild 0x0004 file: centura.h */
 public static  int CREATE_AsChild = 0x0004;

 /**
  * #define CREATE_Border 0x0002 file: centura.h */
 public static  int CREATE_Border = 0x0002;

 /**
  * #define CREATE_Hidden 0x0001 file: centura.h */
 public static  int CREATE_Hidden = 0x0001;

 /**
  * #define CT_EDIT            0 file: tb.h */
 public static  int CT_EDIT = 0;

 /**
  * #define CT_LONGEDIT        1 file: tb.h */
 public static  int CT_LONGEDIT = 1;

 /**
  * #define CT_OWNERDRAW       3 file: tb.h */
 public static  int CT_OWNERDRAW = 3;

 /**
  * #define DATA_DateFields    0x0004 file: centura.h */
 public static  int DATA_DateFields = 0x0004;

 /**
  * #define DATA_NumberFields  0x0001 file: centura.h */
 public static  int DATA_NumberFields = 0x0001;

 /**
  * #define DATA_StringFields  0x0002 file: centura.h */
 public static  int DATA_StringFields = 0x0002;

 /**
  * #define DATA_TableSources  0x0001 file: centura.h */
 public static  int DATA_TableSources = 0x0001;

 /**
  * #define DBP_AUTOCOMMIT   1 file: centura.h */
 public static  int DBP_AUTOCOMMIT = 1;

 /**
  * #define DBP_BRAND   2 file: centura.h */
 public static  int DBP_BRAND = 2;

 /**
  * #define DBP_ISOLEVEL   9 file: centura200.h */
 public static  int DBP_ISOLEVEL = 9;

 /**
  * #define DBP_LOCKWAITTIMEOUT   5 file: centura.h */
 public static  int DBP_LOCKWAITTIMEOUT = 5;

 /**
  * #define DBP_PRESERVE   3 file: centura.h */
 public static  int DBP_PRESERVE = 3;

 /**
  * #define DBP_ROLLBACKONTIMEOUT   6 file: centura.h */
 public static  int DBP_ROLLBACKONTIMEOUT = 6;

 /**
  * #define DBP_VERSION   4 file: centura.h */
 public static  int DBP_VERSION = 4;

 /**
  * #define DBV_BRAND_ALLBASE               10 file: centura.h */
 public static  int DBV_BRAND_ALLBASE = 10;

 /**
  * #define DBV_BRAND_AS400                 7 file: centura.h */
 public static  int DBV_BRAND_AS400 = 7;

 /**
  * #define DBV_BRAND_CINCOMSUPRA           20 file: centura.h */
 public static  int DBV_BRAND_CINCOMSUPRA = 20;

 /**
  * #define DBV_BRAND_DB2     2        // DB2 file: centura.h */
 public static  int DBV_BRAND_DB2 = 2;

 /**
  * #define DBV_BRAND_EDASQL                25 file: centura.h */
 public static  int DBV_BRAND_EDASQL = 25;

 /**
  * #define DBV_BRAND_INFORMIX              5 file: centura.h */
 public static  int DBV_BRAND_INFORMIX = 5;

 /**
  * #define DBV_BRAND_INFORMIXONLINE        24 file: centura.h */
 public static  int DBV_BRAND_INFORMIXONLINE = 24;

 /**
  * #define DBV_BRAND_INGRES                15 file: centura.h */
 public static  int DBV_BRAND_INGRES = 15;

 /**
  * #define DBV_BRAND_NETWARE 6        // Novell Netware SQL file: centura.h */
 public static  int DBV_BRAND_NETWARE = 6;

 /**
  * #define DBV_BRAND_ORACLE  4        // ORACLE file: centura.h */
 public static  int DBV_BRAND_ORACLE = 4;

 /**
  * #define DBV_BRAND_ORACLE7               28 file: centura.h */
 public static  int DBV_BRAND_ORACLE7 = 28;

 /**
  * #define DBV_BRAND_OS2EE   3        // OS/2 DBM file: centura.h */
 public static  int DBV_BRAND_OS2EE = 3;

 /**
  * #define DBV_BRAND_SQL     1        // SQLBase file: centura.h */
 public static  int DBV_BRAND_SQL = 1;

 /**
  * #define DBV_BRAND_SYBASE  8        // SQL Server/Sybase file: centura.h */
 public static  int DBV_BRAND_SYBASE = 8;

 /**
  * #define DDE_fAck        0x8000 file: centura.h */
 public static  int DDE_fAck = 0x8000;

 /**
  * #define DDE_fBusy       0x4000 file: centura.h */
 public static  int DDE_fBusy = 0x4000;

 /**
  * #define DDE_fNoData     DDE_fBusy file: centura.h */
 public static  int DDE_fNoData = DDE_fBusy;

 /**
  * #define DDE_fRelease    0x2000 file: centura.h */
 public static  int DDE_fRelease = 0x2000;

 /**
  * #define DDE_fResponse   0x1000 file: centura.h */
 public static  int DDE_fResponse = 0x1000;

 /**
  * #define DDM_ERROR_NO_HANDLER 0 file: cbtype.h */
 public static  int DDM_ERROR_NO_HANDLER = 0;

 /**
  * #define DDM_ERROR_NO_RETURN  1 file: cbtype.h */
 public static  int DDM_ERROR_NO_RETURN = 1;

 /**
  * #define DDM_ERROR_RETURNED   2 file: cbtype.h */
 public static  int DDM_ERROR_RETURNED = 2;

 /**
  * #define DLLDATAMODE_PrepareError        0 file: centura.h */
 public static  int DLLDATAMODE_PrepareError = 0;

 /**
  * #define DLLDATAMODE_PrepareNoDLLDataMode        2 file: centura.h */
 public static  int DLLDATAMODE_PrepareNoDLLDataMode = 2;

 /**
  * #define DLLDATAMODE_PrepareUseDLLDataMode       1 file: centura.h */
 public static  int DLLDATAMODE_PrepareUseDLLDataMode = 1;

 /**
  * #define DTDATECODEMASK   0x7       mask to get code as small int  file: datatype.h */
 public static  int DTDATECODEMASK = 0x7;

 /**
  * #define DTDATEFORMATCODE 0x80      /* flag to specify is special code  file: datatype.h */
 public static  int DTDATEFORMATCODE = 0x80;

 /**
  * #define DTDATETIMEMASK  0xF0       /* mask to get code in place  file: datatype.h */
 public static  int DTDATETIMEMASK = 0xF0;

 /**
  * #define DTDATETIMESHIFT 4          /* shift to insert code       file: datatype.h */
 public static  int DTDATETIMESHIFT = 4;

 /**
  * #define DTLENGTHMASK    0x0F file: cbtype.h */
 public static  int DTLENGTHMASK = 0x0F;

 /**
  * #define DT_Boolean      0x0001 file: centura.h */
 public static  int DT_Boolean = 0x0001;

 /**
  * #define DT_DateTime     0x0002 file: centura.h */
 public static  int DT_DateTime = 0x0002;

 /**
  * #define DT_LongString   0x0007 file: centura.h */
 public static  int DT_LongString = 0x0007;

 /**
  * #define DT_Number       0x0003 file: centura.h */
 public static  int DT_Number = 0x0003;

 /**
  * #define DT_String       0x0005 file: centura.h */
 public static  int DT_String = 0x0005;

 /**
  * #define DW_Default      -1 file: centura.h */
 public static  int DW_Default = -1;

 /**
  * #define ECT_CheckBox       1 file: tb.h */
 public static  int ECT_CheckBox = 1;

 /**
  * #define ECT_DropDownEdit   4 file: tb.h */
 public static  int ECT_DropDownEdit = 4;

 /**
  * #define ECT_DropDownList   2 file: tb.h */
 public static  int ECT_DropDownList = 2;

 /**
  * #define ECT_DropDownListAndEdit   3 file: tb.h */
 public static  int ECT_DropDownListAndEdit = 3;

 /**
  * #define ECT_DropDownUser   5 file: tb.h */
 public static  int ECT_DropDownUser = 5;

 /**
  * #define ECT_Standard       0 file: tb.h */
 public static  int ECT_Standard = 0;

 /**
  * #define EDT_CheckBoxOnOff  1   // Two null terminated strings.  The first file: tb.h */
 public static  int EDT_CheckBoxOnOff = 1;

 /**
  * #define FA_Archive     0x0020 file: centura.h */
 public static  int FA_Archive = 0x0020;

 /**
  * #define FA_Directory   0x0010 file: centura.h */
 public static  int FA_Directory = 0x0010;

 /**
  * #define FA_Drive       0x4000 file: centura.h */
 public static  int FA_Drive = 0x4000;

 /**
  * #define FA_Exclusive   0x8000 file: centura.h */
 public static  int FA_Exclusive = 0x8000;

 /**
  * #define FA_Hidden      0x0002 file: centura.h */
 public static  int FA_Hidden = 0x0002;

 /**
  * #define FA_Queued      0x2000 file: centura.h */
 public static  int FA_Queued = 0x2000;

 /**
  * #define FA_ReadOnly    0x0001 file: centura.h */
 public static  int FA_ReadOnly = 0x0001;

 /**
  * #define FA_Standard    0x0000 file: centura.h */
 public static  int FA_Standard = 0x0000;

 /**
  * #define FA_System      0x0004 file: centura.h */
 public static  int FA_System = 0x0004;

 /**
  * #define FETCH_Delete      3 file: centura.h */
 public static  int FETCH_Delete = 3;

 /**
  * #define FETCH_EOF         1 file: centura.h */
 public static  int FETCH_EOF = 1;

 /**
  * #define FETCH_Ok          0 file: centura.h */
 public static  int FETCH_Ok = 0;

 /**
  * #define FETCH_Update      2 file: centura.h */
 public static  int FETCH_Update = 2;

 /**
  * #define FILE_CopyDest   2 file: centura.h */
 public static  int FILE_CopyDest = 2;

 /**
  * #define FILE_CopyExist  3 file: centura.h */
 public static  int FILE_CopyExist = 3;

 /**
  * #define FILE_CopyOK     0 file: centura.h */
 public static  int FILE_CopyOK = 0;

 /**
  * #define FILE_CopyRead   4 file: centura.h */
 public static  int FILE_CopyRead = 4;

 /**
  * #define FILE_CopySrc    1 file: centura.h */
 public static  int FILE_CopySrc = 1;

 /**
  * #define FILE_CopyWrite  5 file: centura.h */
 public static  int FILE_CopyWrite = 5;

 /**
  * #define FILE_SeekBegin     0 file: centura.h */
 public static  int FILE_SeekBegin = 0;

 /**
  * #define FILE_SeekCurrent   1 file: centura.h */
 public static  int FILE_SeekCurrent = 1;

 /**
  * #define FILE_SeekEnd       2 file: centura.h */
 public static  int FILE_SeekEnd = 2;

 /**
  * #define FMT_Date_DMY   1 file: centura.h */
 public static  int FMT_Date_DMY = 1;

 /**
  * #define FMT_Date_JapanEmperor     3 file: centura.h */
 public static  int FMT_Date_JapanEmperor = 3;

 /**
  * #define FMT_Date_JapanGregorian   2 file: centura.h */
 public static  int FMT_Date_JapanGregorian = 2;

 /**
  * #define FMT_Date_LongGregorian    0 file: centura.h */
 public static  int FMT_Date_LongGregorian = 0;

 /**
  * #define FMT_Date_MDY   0 file: centura.h */
 public static  int FMT_Date_MDY = 0;

 /**
  * #define FMT_Date_ShortGregorian   1 file: centura.h */
 public static  int FMT_Date_ShortGregorian = 1;

 /**
  * #define FMT_Date_YMD   2 file: centura.h */
 public static  int FMT_Date_YMD = 2;

 /**
  * #define FMT_Format_Currency       1 file: centura.h */
 public static  int FMT_Format_Currency = 1;

 /**
  * #define FMT_Format_Date           2 file: centura.h */
 public static  int FMT_Format_Date = 2;

 /**
  * #define FMT_Format_DateTime       3 file: centura.h */
 public static  int FMT_Format_DateTime = 3;

 /**
  * #define FMT_Format_Decimal        4 file: centura.h */
 public static  int FMT_Format_Decimal = 4;

 /**
  * #define FMT_Format_Invisible      5 file: centura.h */
 public static  int FMT_Format_Invisible = 5;

 /**
  * #define FMT_Format_LowerCase      8 file: centura.h */
 public static  int FMT_Format_LowerCase = 8;

 /**
  * #define FMT_Format_Percentage     6 file: centura.h */
 public static  int FMT_Format_Percentage = 6;

 /**
  * #define FMT_Format_Time           7 file: centura.h */
 public static  int FMT_Format_Time = 7;

 /**
  * #define FMT_Format_Unformatted    0 file: centura.h */
 public static  int FMT_Format_Unformatted = 0;

 /**
  * #define FMT_Format_UpperCase      9 file: centura.h */
 public static  int FMT_Format_UpperCase = 9;

 /**
  * #define FMT_Parm_iCurrDigits     17 file: centura.h */
 public static  int FMT_Parm_iCurrDigits = 17;

 /**
  * #define FMT_Parm_iCurrency        1 file: centura.h */
 public static  int FMT_Parm_iCurrency = 1;

 /**
  * #define FMT_Parm_iDate            2 file: centura.h */
 public static  int FMT_Parm_iDate = 2;

 /**
  * #define FMT_Parm_iDateType       13 file: centura.h */
 public static  int FMT_Parm_iDateType = 13;

 /**
  * #define FMT_Parm_iDigits          3 file: centura.h */
 public static  int FMT_Parm_iDigits = 3;

 /**
  * #define FMT_Parm_iEmpDateBound   20 file: centura.h */
 public static  int FMT_Parm_iEmpDateBound = 20;

 /**
  * #define FMT_Parm_iLzero           4 file: centura.h */
 public static  int FMT_Parm_iLzero = 4;

 /**
  * #define FMT_Parm_iNegCurr        18 file: centura.h */
 public static  int FMT_Parm_iNegCurr = 18;

 /**
  * #define FMT_Parm_iTLZero         16 file: centura.h */
 public static  int FMT_Parm_iTLZero = 16;

 /**
  * #define FMT_Parm_iTime            5 file: centura.h */
 public static  int FMT_Parm_iTime = 5;

 /**
  * #define FMT_Parm_s1159            6 file: centura.h */
 public static  int FMT_Parm_s1159 = 6;

 /**
  * #define FMT_Parm_s2359            7 file: centura.h */
 public static  int FMT_Parm_s2359 = 7;

 /**
  * #define FMT_Parm_sCurrency        8 file: centura.h */
 public static  int FMT_Parm_sCurrency = 8;

 /**
  * #define FMT_Parm_sDate            9 file: centura.h */
 public static  int FMT_Parm_sDate = 9;

 /**
  * #define FMT_Parm_sDecimal        10 file: centura.h */
 public static  int FMT_Parm_sDecimal = 10;

 /**
  * #define FMT_Parm_sLongDate       15 file: centura.h */
 public static  int FMT_Parm_sLongDate = 15;

 /**
  * #define FMT_Parm_sShortDate      14 file: centura.h */
 public static  int FMT_Parm_sShortDate = 14;

 /**
  * #define FMT_Parm_sThousand       11 file: centura.h */
 public static  int FMT_Parm_sThousand = 11;

 /**
  * #define FMT_Parm_sTime           12 file: centura.h */
 public static  int FMT_Parm_sTime = 12;

 /**
  * #define FMT_Pic_DateTime        1 file: centura.h */
 public static  int FMT_Pic_DateTime = 1;

 /**
  * #define FMT_Pic_Number          2 file: centura.h */
 public static  int FMT_Pic_Number = 2;

 /**
  * #define FMT_Prefix_NoSeparation   0 file: centura.h */
 public static  int FMT_Prefix_NoSeparation = 0;

 /**
  * #define FMT_Prefix_Separation     2 file: centura.h */
 public static  int FMT_Prefix_Separation = 2;

 /**
  * #define FMT_Profile_Australia        61 file: centura.h */
 public static  int FMT_Profile_Australia = 61;

 /**
  * #define FMT_Profile_Belgium          32 file: centura.h */
 public static  int FMT_Profile_Belgium = 32;

 /**
  * #define FMT_Profile_Brazil           55 file: centura.h */
 public static  int FMT_Profile_Brazil = 55;

 /**
  * #define FMT_Profile_Default           0 file: centura.h */
 public static  int FMT_Profile_Default = 0;

 /**
  * #define FMT_Profile_Denmark          45 file: centura.h */
 public static  int FMT_Profile_Denmark = 45;

 /**
  * #define FMT_Profile_Finland         358 file: centura.h */
 public static  int FMT_Profile_Finland = 358;

 /**
  * #define FMT_Profile_France           33 file: centura.h */
 public static  int FMT_Profile_France = 33;

 /**
  * #define FMT_Profile_Italy            39 file: centura.h */
 public static  int FMT_Profile_Italy = 39;

 /**
  * #define FMT_Profile_Japan            81 file: centura.h */
 public static  int FMT_Profile_Japan = 81;

 /**
  * #define FMT_Profile_Netherlands      31 file: centura.h */
 public static  int FMT_Profile_Netherlands = 31;

 /**
  * #define FMT_Profile_Norway           47 file: centura.h */
 public static  int FMT_Profile_Norway = 47;

 /**
  * #define FMT_Profile_Program          -1 file: centura.h */
 public static  int FMT_Profile_Program = -1;

 /**
  * #define FMT_Profile_Spain            34 file: centura.h */
 public static  int FMT_Profile_Spain = 34;

 /**
  * #define FMT_Profile_Sweden           46 file: centura.h */
 public static  int FMT_Profile_Sweden = 46;

 /**
  * #define FMT_Profile_Switzerland      41 file: centura.h */
 public static  int FMT_Profile_Switzerland = 41;

 /**
  * #define FMT_Profile_USA               1 file: centura.h */
 public static  int FMT_Profile_USA = 1;

 /**
  * #define FMT_Profile_UnitedKingdom    44 file: centura.h */
 public static  int FMT_Profile_UnitedKingdom = 44;

 /**
  * #define FMT_Profile_WestGermany      49 file: centura.h */
 public static  int FMT_Profile_WestGermany = 49;

 /**
  * #define FMT_Suffix_NoSeparation   1 file: centura.h */
 public static  int FMT_Suffix_NoSeparation = 1;

 /**
  * #define FMT_Suffix_Separation     3 file: centura.h */
 public static  int FMT_Suffix_Separation = 3;

 /**
  * #define FMT_Time_12Hour   0 file: centura.h */
 public static  int FMT_Time_12Hour = 0;

 /**
  * #define FMT_Time_24Hour   1 file: centura.h */
 public static  int FMT_Time_24Hour = 1;

 /**
  * #define FMT_Validate_Dialog   1 file: centura.h */
 public static  int FMT_Validate_Dialog = 1;

 /**
  * #define FMT_Validate_None     0 file: centura.h */
 public static  int FMT_Validate_None = 0;

 /**
  * #define FONT_EnhBold           0x0008 file: centura.h */
 public static  int FONT_EnhBold = 0x0008;

 /**
  * #define FONT_EnhDefault        0x0001 file: centura.h */
 public static  int FONT_EnhDefault = 0x0001;

 /**
  * #define FONT_EnhItalic         0x0002 file: centura.h */
 public static  int FONT_EnhItalic = 0x0002;

 /**
  * #define FONT_EnhNone           0x0000 file: centura.h */
 public static  int FONT_EnhNone = 0x0000;

 /**
  * #define FONT_EnhStrikeOut      0x0010 file: centura.h */
 public static  int FONT_EnhStrikeOut = 0x0010;

 /**
  * #define FONT_EnhUnderline      0x0004 file: centura.h */
 public static  int FONT_EnhUnderline = 0x0004;

 /**
  * #define FORM_nFormHeight   2 file: centura.h */
 public static  int FORM_nFormHeight = 2;

 /**
  * #define FORM_nFormPages    3 file: centura.h */
 public static  int FORM_nFormPages = 3;

 /**
  * #define FORM_nFormWidth    1 file: centura.h */
 public static  int FORM_nFormWidth = 1;

	public static  int WM_USER = 0;

 /**
  * #define GTM_CHECKBOXCOLUMN           (WM_USER+103) file: tb.h */
 public static  int GTM_CHECKBOXCOLUMN = WM_USER+103;

 /**
  * #define GTM_CLEARSEL          (WM_USER+12) file: tb.h */
 public static  int GTM_CLEARSEL = WM_USER+12;

 /**
  * #define GTM_CONTEXTMENUSELECTROWS           (WM_USER+133) file: tb.h */
 public static  int GTM_CONTEXTMENUSELECTROWS = WM_USER+133;

 /**
  * #define GTM_DEBUG             (WM_USER+34) file: tb.h */
 public static  int GTM_DEBUG = WM_USER+34;

 /**
  * #define GTM_DELETEROW         (WM_USER+13) file: tb.h */
 public static  int GTM_DELETEROW = WM_USER+13;

 /**
  * #define GTM_DESIGNCACHECREATE          (WM_USER+111) file: tb.h */
 public static  int GTM_DESIGNCACHECREATE = WM_USER+111;

 /**
  * #define GTM_DESTROYCOLUMNS                (WM_USER + 98) file: tb.h */
 public static  int GTM_DESTROYCOLUMNS = WM_USER + 98;

 /**
  * #define GTM_FETCHROW          (WM_USER+14) file: tb.h */
 public static  int GTM_FETCHROW = WM_USER+14;

 /**
  * #define GTM_FETCHROWNOCACHE               (WM_USER + 96) file: tb.h */
 public static  int GTM_FETCHROWNOCACHE = WM_USER + 96;

 /**
  * #define GTM_FINDACTUALLASTROW              (WM_USER + 92) file: tb.h */
 public static  int GTM_FINDACTUALLASTROW = WM_USER + 92;

 /**
  * #define GTM_FINDNEXTROW       (WM_USER+15) file: tb.h */
 public static  int GTM_FINDNEXTROW = WM_USER+15;

 /**
  * #define GTM_FINDPREVROW       (WM_USER+16) file: tb.h */
 public static  int GTM_FINDPREVROW = WM_USER+16;

 /**
  * #define GTM_GETCELLLONGDATA       (WM_USER+60) file: tb.h */
 public static  int GTM_GETCELLLONGDATA = WM_USER+60;

 /**
  * #define GTM_GETCELLLONGDATALENGTH       (WM_USER+62) file: tb.h */
 public static  int GTM_GETCELLLONGDATALENGTH = WM_USER+62;

 /**
  * #define GTM_GETCELLTEXT       (WM_USER+41) file: tb.h */
 public static  int GTM_GETCELLTEXT = WM_USER+41;

 /**
  * #define GTM_GETCELLTEXTLENGTH       (WM_USER+56) file: tb.h */
 public static  int GTM_GETCELLTEXTLENGTH = WM_USER+56;

 /**
  * #define GTM_GETCOLCNTRLDATA            (WM_USER+105) file: tb.h */
 public static  int GTM_GETCOLCNTRLDATA = WM_USER+105;

 /**
  * #define GTM_GETCOLUMNHWND     (WM_USER+39) file: tb.h */
 public static  int GTM_GETCOLUMNHWND = WM_USER+39;

 /**
  * #define GTM_GETCOLUMNLONGDATA       (WM_USER+61) file: tb.h */
 public static  int GTM_GETCOLUMNLONGDATA = WM_USER+61;

 /**
  * #define GTM_GETCOLUMNLONGDATALENGTH     (WM_USER+63) file: tb.h */
 public static  int GTM_GETCOLUMNLONGDATALENGTH = WM_USER+63;

 /**
  * #define GTM_GETCOLUMNTEXT     (WM_USER+40) file: tb.h */
 public static  int GTM_GETCOLUMNTEXT = WM_USER+40;

 /**
  * #define GTM_GETCOLUMNTEXTLENGTH     (WM_USER+57) file: tb.h */
 public static  int GTM_GETCOLUMNTEXTLENGTH = WM_USER+57;

 /**
  * #define GTM_GETEXTENDEDCOLUMNDATA            (WM_USER+130) file: tb.h */
 public static  int GTM_GETEXTENDEDCOLUMNDATA = WM_USER+130;

 /**
  * #define GTM_GETEXTENDEDCOLUMNDATALENGTH            (WM_USER+131) file: tb.h */
 public static  int GTM_GETEXTENDEDCOLUMNDATALENGTH = WM_USER+131;

 /**
  * #define GTM_GETLONGDATAFORDISCARD      (WM_USER+113) file: tb.h */
 public static  int GTM_GETLONGDATAFORDISCARD = WM_USER+113;

 /**
  * #define GTM_GETMETRICS        (WM_USER+54) file: tb.h */
 public static  int GTM_GETMETRICS = WM_USER+54;

 /**
  * #define GTM_GETROWHEADERTITLETEXT    (WM_USER+81) file: tb.h */
 public static  int GTM_GETROWHEADERTITLETEXT = WM_USER+81;

 /**
  * #define GTM_GETROWHEADERTITLETEXTLENGTH    (WM_USER+82) file: tb.h */
 public static  int GTM_GETROWHEADERTITLETEXTLENGTH = WM_USER+82;

 /**
  * #define GTM_HITMOUSEMOVE      (WM_USER+53) file: tb.h */
 public static  int GTM_HITMOUSEMOVE = WM_USER+53;

 /**
  * #define GTM_INITHEADER        (WM_USER+37) file: tb.h */
 public static  int GTM_INITHEADER = WM_USER+37;

 /**
  * #define GTM_INSERTCOLUMN      (WM_USER+51) file: tb.h */
 public static  int GTM_INSERTCOLUMN = WM_USER+51;

 /**
  * #define GTM_INSERTROW         (WM_USER+17) file: tb.h */
 public static  int GTM_INSERTROW = WM_USER+17;

 /**
  * #define GTM_KILLEDIT          (WM_USER+18) file: tb.h */
 public static  int GTM_KILLEDIT = WM_USER+18;

 /**
  * #define GTM_KILLFOCUS         (WM_USER+19) file: tb.h */
 public static  int GTM_KILLFOCUS = WM_USER+19;

 /**
  * #define GTM_LIMITDISCARDABLEROWS  (WM_USER+66) file: tb.h */
 public static  int GTM_LIMITDISCARDABLEROWS = WM_USER+66;

 /**
  * #define GTM_MOVECOLUMN          (WM_USER+115) file: tb.h */
 public static  int GTM_MOVECOLUMN = WM_USER+115;

 /**
  * #define GTM_OBJECTSFROMPOINT            (WM_USER+132) file: tb.h */
 public static  int GTM_OBJECTSFROMPOINT = WM_USER+132;

 /**
  * #define GTM_PAINTROWHEADER     (WM_USER+58) file: tb.h */
 public static  int GTM_PAINTROWHEADER = WM_USER+58;

 /**
  * #define GTM_PAINTROWS         (WM_USER+20) file: tb.h */
 public static  int GTM_PAINTROWS = WM_USER+20;

 /**
  * #define GTM_QUERYCELLTEXTCOLOR  (WM_USER+69) file: tb.h */
 public static  int GTM_QUERYCELLTEXTCOLOR = WM_USER+69;

 /**
  * #define GTM_QUERYCELLVALIDATESTATUS       (WM_USER + 95) file: tb.h */
 public static  int GTM_QUERYCELLVALIDATESTATUS = WM_USER + 95;

 /**
  * #define GTM_QUERYCLICKONEMPTYROW       (WM_USER+112) file: tb.h */
 public static  int GTM_QUERYCLICKONEMPTYROW = WM_USER+112;

 /**
  * #define GTM_QUERYCOLCNTRLDATALEN       (WM_USER+104) file: tb.h */
 public static  int GTM_QUERYCOLCNTRLDATALEN = WM_USER+104;

 /**
  * #define GTM_QUERYCOLUMNCOUNT  (WM_USER+48) file: tb.h */
 public static  int GTM_QUERYCOLUMNCOUNT = WM_USER+48;

 /**
  * #define GTM_QUERYCOLUMNFLAGS  (WM_USER+49) file: tb.h */
 public static  int GTM_QUERYCOLUMNFLAGS = WM_USER+49;

 /**
  * #define GTM_QUERYCOLUMNPOS    (WM_USER+45) file: tb.h */
 public static  int GTM_QUERYCOLUMNPOS = WM_USER+45;

 /**
  * #define GTM_QUERYCOLUMNSIZE   (WM_USER+43) file: tb.h */
 public static  int GTM_QUERYCOLUMNSIZE = WM_USER+43;

 /**
  * #define GTM_QUERYCOLUMNTYPE   (WM_USER+42) file: tb.h */
 public static  int GTM_QUERYCOLUMNTYPE = WM_USER+42;

 /**
  * #define GTM_QUERYCOLUMNXYPOSITION   (WM_USER+100) file: tb.h */
 public static  int GTM_QUERYCOLUMNXYPOSITION = WM_USER+100;

 /**
  * #define GTM_QUERYCONTEXT      (WM_USER+21) file: tb.h */
 public static  int GTM_QUERYCONTEXT = WM_USER+21;

 /**
  * #define GTM_QUERYDEFAULTFIELDPOSITIONS (WM_USER+101) file: tb.h */
 public static  int GTM_QUERYDEFAULTFIELDPOSITIONS = WM_USER+101;

 /**
  * #define GTM_QUERYDEFRADIOBUTTON       (WM_USER+106) file: tb.h */
 public static  int GTM_QUERYDEFRADIOBUTTON = WM_USER+106;

 /**
  * #define GTM_QUERYDROPDOWNFLAGS       (WM_USER+118) file: tb.h */
 public static  int GTM_QUERYDROPDOWNFLAGS = WM_USER+118;

 /**
  * #define GTM_QUERYDROPDOWNWINDOW        (WM_USER+119) file: tb.h */
 public static  int GTM_QUERYDROPDOWNWINDOW = WM_USER+119;

 /**
  * #define GTM_QUERYDYNAMICSCROLL             (WM_USER + 90) file: tb.h */
 public static  int GTM_QUERYDYNAMICSCROLL = WM_USER + 90;

 /**
  * #define GTM_QUERYEXTENDEDCOLUMNTYPE        (WM_USER+123) file: tb.h */
 public static  int GTM_QUERYEXTENDEDCOLUMNTYPE = WM_USER+123;

 /**
  * #define GTM_QUERYFOCUS        (WM_USER+22) file: tb.h */
 public static  int GTM_QUERYFOCUS = WM_USER+22;

 /**
  * #define GTM_QUERYFOCUSCOL             (WM_USER+109) file: tb.h */
 public static  int GTM_QUERYFOCUSCOL = WM_USER+109;

 /**
  * #define GTM_QUERYFOCUSROW                  (WM_USER + 88) file: tb.h */
 public static  int GTM_QUERYFOCUSROW = WM_USER + 88;

 /**
  * #define GTM_QUERYFOCUSWIN             (WM_USER+110) file: tb.h */
 public static  int GTM_QUERYFOCUSWIN = WM_USER+110;

 /**
  * #define GTM_QUERYHOOK (WM_USER+72) file: tb.h */
 public static  int GTM_QUERYHOOK = WM_USER+72;

 /**
  * #define GTM_QUERYKILLFOCUSROW          (WM_USER+114) file: tb.h */
 public static  int GTM_QUERYKILLFOCUSROW = WM_USER+114;

 /**
  * #define GTM_QUERYLOCKEDCOLUMNCOUNT     (WM_USER+84) file: tb.h */
 public static  int GTM_QUERYLOCKEDCOLUMNCOUNT = WM_USER+84;

 /**
  * #define GTM_QUERYMAXDATAWIDTH              (WM_USER + 91) file: tb.h */
 public static  int GTM_QUERYMAXDATAWIDTH = WM_USER + 91;

 /**
  * #define GTM_QUERYMAXDROPDOWNLINES     (WM_USER+125) file: tb.h */
 public static  int GTM_QUERYMAXDROPDOWNLINES = WM_USER+125;

 /**
  * #define GTM_QUERYOVERFLOWINDICATOR     (WM_USER+86) file: tb.h */
 public static  int GTM_QUERYOVERFLOWINDICATOR = WM_USER+86;

 /**
  * #define GTM_QUERYPIXELORIGIN             (WM_USER+87) file: tb.h */
 public static  int GTM_QUERYPIXELORIGIN = WM_USER+87;

 /**
  * #define GTM_QUERYROWFLAGS     (WM_USER+23) file: tb.h */
 public static  int GTM_QUERYROWFLAGS = WM_USER+23;

 /**
  * #define GTM_QUERYROWHEADERFLAGS        (WM_USER+77) file: tb.h */
 public static  int GTM_QUERYROWHEADERFLAGS = WM_USER+77;

 /**
  * #define GTM_QUERYROWHEADERMAPCOLUMN        (WM_USER+79) file: tb.h */
 public static  int GTM_QUERYROWHEADERMAPCOLUMN = WM_USER+79;

 /**
  * #define GTM_QUERYROWHEADERSIZE         (WM_USER+78) file: tb.h */
 public static  int GTM_QUERYROWHEADERSIZE = WM_USER+78;

 /**
  * #define GTM_QUERYROWLINES         (WM_USER+121) file: tb.h */
 public static  int GTM_QUERYROWLINES = WM_USER+121;

 /**
  * #define GTM_QUERYSCROLL       (WM_USER+24) file: tb.h */
 public static  int GTM_QUERYSCROLL = WM_USER+24;

 /**
  * #define GTM_QUERYSETFOCUS      (WM_USER+116) file: tb.h */
 public static  int GTM_QUERYSETFOCUS = WM_USER+116;

 /**
  * #define GTM_QUERYSPLITWINDOW     (WM_USER+73) file: tb.h */
 public static  int GTM_QUERYSPLITWINDOW = WM_USER+73;

 /**
  * #define GTM_QUERYTABLEBIDI                (WM_USER+127) file: tb.h */
 public static  int GTM_QUERYTABLEBIDI = WM_USER+127;

 /**
  * #define GTM_QUERYTABLECAPABILITIES (WM_USER+71) file: tb.h */
 public static  int GTM_QUERYTABLECAPABILITIES = WM_USER+71;

 /**
  * #define GTM_QUERYUSERMODE         (WM_USER+36) file: tb.h */
 public static  int GTM_QUERYUSERMODE = WM_USER+36;

 /**
  * #define GTM_QUERYVIEW                     (WM_USER + 94) file: tb.h */
 public static  int GTM_QUERYVIEW = WM_USER + 94;

 /**
  * #define GTM_QUERYVIRTUALPOS   (WM_USER+52) file: tb.h */
 public static  int GTM_QUERYVIRTUALPOS = WM_USER+52;

 /**
  * #define GTM_QUERYVISIBLERANGE (WM_USER+26) file: tb.h */
 public static  int GTM_QUERYVISIBLERANGE = WM_USER+26;

 /**
  * #define GTM_RADIOBUTTONCOLUMN          (WM_USER+102) file: tb.h */
 public static  int GTM_RADIOBUTTONCOLUMN = WM_USER+102;

 /**
  * #define GTM_REFETCHROWS                   (WM_USER + 97) file: tb.h */
 public static  int GTM_REFETCHROWS = WM_USER + 97;

 /**
  * #define GTM_REPOSITIONDROPDOWN            (WM_USER+128) file: tb.h */
 public static  int GTM_REPOSITIONDROPDOWN = WM_USER+128;

 /**
  * #define GTM_RESET             (WM_USER+25) file: tb.h */
 public static  int GTM_RESET = WM_USER+25;

 /**
  * #define GTM_RESETCACHE        (WM_USER+44) file: tb.h */
 public static  int GTM_RESETCACHE = WM_USER+44;

 /**
  * #define GTM_SCROLL            (WM_USER+27) file: tb.h */
 public static  int GTM_SCROLL = WM_USER+27;

 /**
  * #define GTM_SETCELLTEXTCOLOR  (WM_USER+68) file: tb.h */
 public static  int GTM_SETCELLTEXTCOLOR = WM_USER+68;

 /**
  * #define GTM_SETCOLCNTRLDATA           (WM_USER+107) file: tb.h */
 public static  int GTM_SETCOLCNTRLDATA = WM_USER+107;

 /**
  * #define GTM_SETCOLUMNDATAWIDTH  (WM_USER+67) file: tb.h */
 public static  int GTM_SETCOLUMNDATAWIDTH = WM_USER+67;

 /**
  * #define GTM_SETCOLUMNFLAGS    (WM_USER+50) file: tb.h */
 public static  int GTM_SETCOLUMNFLAGS = WM_USER+50;

 /**
  * #define GTM_SETCOLUMNLONGDATA     (WM_USER+59) file: tb.h */
 public static  int GTM_SETCOLUMNLONGDATA = WM_USER+59;

 /**
  * #define GTM_SETCOLUMNPOS      (WM_USER+46) file: tb.h */
 public static  int GTM_SETCOLUMNPOS = WM_USER+46;

 /**
  * #define GTM_SETCOLUMNSIZE     (WM_USER+47) file: tb.h */
 public static  int GTM_SETCOLUMNSIZE = WM_USER+47;

 /**
  * #define GTM_SETCOLUMNTEXT     (WM_USER+38) file: tb.h */
 public static  int GTM_SETCOLUMNTEXT = WM_USER+38;

 /**
  * #define GTM_SETCOLUMNXYPOSITION   (WM_USER+99) file: tb.h */
 public static  int GTM_SETCOLUMNXYPOSITION = WM_USER+99;

 /**
  * #define GTM_SETCONTEXT        (WM_USER+28) file: tb.h */
 public static  int GTM_SETCONTEXT = WM_USER+28;

 /**
  * #define GTM_SETCONTEXTONLY    (WM_USER+29) file: tb.h */
 public static  int GTM_SETCONTEXTONLY = WM_USER+29;

 /**
  * #define GTM_SETDROPDOWNFLAGS       (WM_USER+117) file: tb.h */
 public static  int GTM_SETDROPDOWNFLAGS = WM_USER+117;

 /**
  * #define GTM_SETDROPDOWNUSER        (WM_USER+124) file: tb.h */
 public static  int GTM_SETDROPDOWNUSER = WM_USER+124;

 /**
  * #define GTM_SETEXTENDEDCOLUMNDATA            (WM_USER+129) file: tb.h */
 public static  int GTM_SETEXTENDEDCOLUMNDATA = WM_USER+129;

 /**
  * #define GTM_SETEXTENDEDCOLUMNTYPE        (WM_USER+122) file: tb.h */
 public static  int GTM_SETEXTENDEDCOLUMNTYPE = WM_USER+122;

 /**
  * #define GTM_SETFOCUSCELL      (WM_USER+30) file: tb.h */
 public static  int GTM_SETFOCUSCELL = WM_USER+30;

 /**
  * #define GTM_SETFOCUSROW       (WM_USER+31) file: tb.h */
 public static  int GTM_SETFOCUSROW = WM_USER+31;

 /**
  * #define GTM_SETHOOK           (WM_USER+11) file: tb.h */
 public static  int GTM_SETHOOK = WM_USER+11;

 /**
  * #define GTM_SETLOCKEDCOLUMNCOUNT     (WM_USER+83) file: tb.h */
 public static  int GTM_SETLOCKEDCOLUMNCOUNT = WM_USER+83;

 /**
  * #define GTM_SETMAXDROPDOWNLINES        (WM_USER+126) file: tb.h */
 public static  int GTM_SETMAXDROPDOWNLINES = WM_USER+126;

 /**
  * #define GTM_SETOVERFLOWINDICATOR       (WM_USER+85) file: tb.h */
 public static  int GTM_SETOVERFLOWINDICATOR = WM_USER+85;

 /**
  * #define GTM_SETRANGE          (WM_USER+33) file: tb.h */
 public static  int GTM_SETRANGE = WM_USER+33;

 /**
  * #define GTM_SETRANGEDYNAMIC  (WM_USER+65) file: tb.h */
 public static  int GTM_SETRANGEDYNAMIC = WM_USER+65;

 /**
  * #define GTM_SETROWFLAGS       (WM_USER+32) file: tb.h */
 public static  int GTM_SETROWFLAGS = WM_USER+32;

 /**
  * #define GTM_SETROWHEADERFLAGS        (WM_USER+74) file: tb.h */
 public static  int GTM_SETROWHEADERFLAGS = WM_USER+74;

 /**
  * #define GTM_SETROWHEADERMAPCOLUMN        (WM_USER+76) file: tb.h */
 public static  int GTM_SETROWHEADERMAPCOLUMN = WM_USER+76;

 /**
  * #define GTM_SETROWHEADERSIZE         (WM_USER+75) file: tb.h */
 public static  int GTM_SETROWHEADERSIZE = WM_USER+75;

 /**
  * #define GTM_SETROWHEADERTITLETEXT    (WM_USER+80) file: tb.h */
 public static  int GTM_SETROWHEADERTITLETEXT = WM_USER+80;

 /**
  * #define GTM_SETROWLINES            (WM_USER+120) file: tb.h */
 public static  int GTM_SETROWLINES = WM_USER+120;

 /**
  * #define GTM_SETTABLECAPABILITIES (WM_USER+70) file: tb.h */
 public static  int GTM_SETTABLECAPABILITIES = WM_USER+70;

 /**
  * #define GTM_SETUSERMODE       (WM_USER+35) file: tb.h */
 public static  int GTM_SETUSERMODE = WM_USER+35;

 /**
  * #define GTM_SETVIEW                        (WM_USER + 93) file: tb.h */
 public static  int GTM_SETVIEW = WM_USER + 93;

 /**
  * #define GTM_SETZEROCOLHIT     (WM_USER+55) file: tb.h */
 public static  int GTM_SETZEROCOLHIT = WM_USER+55;

 /**
  * #define GTM_SPLITWINDOW     (WM_USER+64) file: tb.h */
 public static  int GTM_SPLITWINDOW = WM_USER+64;

 /**
  * #define GTM_SWAPROWS                  (WM_USER+108) file: tb.h */
 public static  int GTM_SWAPROWS = WM_USER+108;

 /**
  * #define GTM_SWITCHDATASET                  (WM_USER + 89) file: tb.h */
 public static  int GTM_SWITCHDATASET = WM_USER + 89;

 /**
  * #define GTM_USER              (WM_USER+200) file: tb.h */
 public static  int GTM_USER = WM_USER+200;

 /**
  * #define HELP_Context      0x0001 file: centura.h */
 public static  int HELP_Context = 0x0001;

 /**
  * #define HELP_Finder		  HELP_FINDER file: centura.h */
 public static  int HELP_Finder = 0x0003;

 /**
  * #define HELP_HelpOnHelp   0x0004 file: centura.h */
 public static  int HELP_HelpOnHelp = 0x0004;

 /**
  * #define HELP_Index        0x0003 file: centura.h */
 public static  int HELP_Index = 0x0003;

 /**
  * #define HELP_Key          0x0101 file: centura.h */
 public static  int HELP_Key = 0x0101;

 /**
  * #define HELP_Quit         0x0002 file: centura.h */
 public static  int HELP_Quit = 0x0002;

 /**
  * #define HELP_SetIndex     0x0005 file: centura.h */
 public static  int HELP_SetIndex = 0x0005;

 /**
  * #define HT_ALLAREAS        0x003f file: tb.h */
 public static  int HT_ALLAREAS = 0x003f;

 /**
  * #define HT_INSERTAREA      0x0004 file: tb.h */
 public static  int HT_INSERTAREA = 0x0004;

 /**
  * #define HT_MOVEAREA        0x0002 file: tb.h */
 public static  int HT_MOVEAREA = 0x0002;

 /**
  * #define HT_NOAREA          0x0000 file: tb.h */
 public static  int HT_NOAREA = 0x0000;

 /**
  * #define HT_ROWHEADERSIZEAREA 0x0010 file: tb.h */
 public static  int HT_ROWHEADERSIZEAREA = 0x0010;

 /**
  * #define HT_ROWSIZEAREA     0x0020 file: tb.h */
 public static  int HT_ROWSIZEAREA = 0x0020;

 /**
  * #define HT_SELECTAREA      0x0008 file: tb.h */
 public static  int HT_SELECTAREA = 0x0008;

 /**
  * #define HT_SIZEAREA        0x0001 file: tb.h */
 public static  int HT_SIZEAREA = 0x0001;

 /**
  * #define INVALID            -1 file: tb.h */
 public static  int INVALID = -1;

 /**
  * #define LB_Err        -1 file: centura.h */
 public static  int LB_Err = -1;

 /**
  * #define LB_ErrSpace   -2 file: centura.h */
 public static  int LB_ErrSpace = -2;

 /**
  * #define LB_Ok          0 file: centura.h */
 public static  int LB_Ok = 0;

 /**
  * #define MAXTITLE           254 file: tb.h */
 public static  int MAXTITLE = 254;

 /**
  * #define MAXVALIDCOLOR  0x02ffffff file: tb.h */
 public static  int MAXVALIDCOLOR = 0x02ffffff;

 /**
  * #define MAX_DefaultDataField    100 file: centura.h */
 public static  int MAX_DefaultDataField = 100;

 /**
  * #define MAX_DefaultMultiline   1000 file: centura.h */
 public static  int MAX_DefaultMultiline = 1000;

 /**
  * #define MB_ABORTRETRYIGNORE         0x00000002L file: centura.h */
 public static  int MB_ABORTRETRYIGNORE = 0x00000002;

 /**
  * #define MB_APPLMODAL                0x00000000L file: centura.h */
 public static  int MB_APPLMODAL = 0x00000000;

 /**
  * #define MB_DEFAULT_DESKTOP_ONLY     0x00020000L file: centura.h */
 public static  int MB_DEFAULT_DESKTOP_ONLY = 0x00020000;

 /**
  * #define MB_DEFBUTTON1               0x00000000L file: centura.h */
 public static  int MB_DEFBUTTON1 = 0x00000000;

 /**
  * #define MB_DEFBUTTON2               0x00000100L file: centura.h */
 public static  int MB_DEFBUTTON2 = 0x00000100;

 /**
  * #define MB_DEFBUTTON3               0x00000200L file: centura.h */
 public static  int MB_DEFBUTTON3 = 0x00000200;

 /**
  * #define MB_ICONASTERISK             0x00000040L file: centura.h */
 public static  int MB_ICONASTERISK = 0x00000040;

 /**
  * #define MB_ICONEXCLAMATION          0x00000030L file: centura.h */
 public static  int MB_ICONEXCLAMATION = 0x00000030;

 /**
  * #define MB_ICONHAND                 0x00000010L file: centura.h */
 public static  int MB_ICONHAND = 0x00000010;

 /**
  * #define MB_ICONINFORMATION          MB_ICONASTERISK file: centura.h */
 public static  int MB_ICONINFORMATION = MB_ICONASTERISK;

 /**
  * #define MB_ICONQUESTION             0x00000020L file: centura.h */
 public static  int MB_ICONQUESTION = 0x00000020;

 /**
  * #define MB_ICONSTOP                 MB_ICONHAND file: centura.h */
 public static  int MB_ICONSTOP = MB_ICONHAND;

 /**
  * #define MB_NOFOCUS                  0x00008000L file: centura.h */
 public static  int MB_NOFOCUS = 0x00008000;

 /**
  * #define MB_OK                       0x00000000L file: centura.h */
 public static  int MB_OK = 0x00000000;

 /**
  * #define MB_OKCANCEL                 0x00000001L file: centura.h */
 public static  int MB_OKCANCEL = 0x00000001;

 /**
  * #define MB_RETRYCANCEL              0x00000005L file: centura.h */
 public static  int MB_RETRYCANCEL = 0x00000005;

 /**
  * #define MB_SERVICE_NOTIFICATION     0x00040000L file: centura.h */
 public static  int MB_SERVICE_NOTIFICATION = 0x00040000;

 /**
  * #define MB_SERVICE_NOTIFICATION_NT3X     0x00040000L file: centura200.h */
 public static  int MB_SERVICE_NOTIFICATION_NT3X = 0x00040000;

 /**
  * #define MB_SETFOREGROUND            0x00010000L file: centura.h */
 public static  int MB_SETFOREGROUND = 0x00010000;

 /**
  * #define MB_SYSTEMMODAL              0x00001000L file: centura.h */
 public static  int MB_SYSTEMMODAL = 0x00001000;

 /**
  * #define MB_TASKMODAL                0x00002000L file: centura.h */
 public static  int MB_TASKMODAL = 0x00002000;

 /**
  * #define MB_YESNO                    0x00000004L file: centura.h */
 public static  int MB_YESNO = 0x00000004;

 /**
  * #define MB_YESNOCANCEL              0x00000003L file: centura.h */
 public static  int MB_YESNOCANCEL = 0x00000003;

 /**
  * #define NUMBERSIZE   12		/* numeric buffer size  file: cbtype.h */
 public static  int NUMBERSIZE = 12;

 /**
  * #define OF_Append             3 file: centura.h */
 public static  int OF_Append = 3;

 /**
  * #define OF_Binary             0x10000 file: centura.h */
 public static  int OF_Binary = 0x10000;

 /**
  * #define OF_Cancel             0x0800 file: centura.h */
 public static  int OF_Cancel = 0x0800;

 /**
  * #define OF_Create             0x1000 file: centura.h */
 public static  int OF_Create = 0x1000;

 /**
  * #define OF_Delete             0x0200 file: centura.h */
 public static  int OF_Delete = 0x0200;

 /**
  * #define OF_Exist              0x4000 file: centura.h */
 public static  int OF_Exist = 0x4000;

 /**
  * #define OF_Parse              0x0100 file: centura.h */
 public static  int OF_Parse = 0x0100;

 /**
  * #define OF_Prompt             0x2000 file: centura.h */
 public static  int OF_Prompt = 0x2000;

 /**
  * #define OF_Read               0 file: centura.h */
 public static  int OF_Read = 0;

 /**
  * #define OF_ReadAppend         4 file: centura.h */
 public static  int OF_ReadAppend = 4;

 /**
  * #define OF_ReadWrite          2 file: centura.h */
 public static  int OF_ReadWrite = 2;

 /**
  * #define OF_Reopen             0x8000 file: centura.h */
 public static  int OF_Reopen = 0x8000;

 /**
  * #define OF_Share_Compat       0x0000 file: centura.h */
 public static  int OF_Share_Compat = 0x0000;

 /**
  * #define OF_Share_Deny_None    0x0040 file: centura.h */
 public static  int OF_Share_Deny_None = 0x0040;

 /**
  * #define OF_Share_Deny_Read    0x0030 file: centura.h */
 public static  int OF_Share_Deny_Read = 0x0030;

 /**
  * #define OF_Share_Deny_Write   0x0020 file: centura.h */
 public static  int OF_Share_Deny_Write = 0x0020;

 /**
  * #define OF_Share_Exclusive    0x0010 file: centura.h */
 public static  int OF_Share_Exclusive = 0x0010;

 /**
  * #define OF_Text               0x0000 file: centura.h */
 public static  int OF_Text = 0x0000;

 /**
  * #define OF_Verify             0x0400 file: centura.h */
 public static  int OF_Verify = 0x0400;

 /**
  * #define OF_Write              1 file: centura.h */
 public static  int OF_Write = 1;

 /**
  * #define PIC_FitBestFit         0x0002 file: centura.h */
 public static  int PIC_FitBestFit = 0x0002;

 /**
  * #define PIC_FitScale           0x0003 file: centura.h */
 public static  int PIC_FitScale = 0x0003;

 /**
  * #define PIC_FitSizeToFit       0x0001 file: centura.h */
 public static  int PIC_FitSizeToFit = 0x0001;

 /**
  * #define PIC_FormatBitmap       0x0001 file: centura.h */
 public static  int PIC_FormatBitmap = 0x0001;

 /**
  * #define PIC_FormatIcon         0x0002 file: centura.h */
 public static  int PIC_FormatIcon = 0x0002;

 /**
  * #define PIC_FormatObject       0x0003 file: centura.h */
 public static  int PIC_FormatObject = 0x0003;

 /**
  * #define PRT_nCopyCount          3 file: centura.h */
 public static  int PRT_nCopyCount = 3;

 /**
  * #define PRT_nDraftMode          1 file: centura.h */
 public static  int PRT_nDraftMode = 1;

 /**
  * #define PRT_nFromPage           7 file: centura.h */
 public static  int PRT_nFromPage = 7;

 /**
  * #define PRT_nMarginLeft         4 file: centura.h */
 public static  int PRT_nMarginLeft = 4;

 /**
  * #define PRT_nMarginTop          5 file: centura.h */
 public static  int PRT_nMarginTop = 5;

 /**
  * #define PRT_nPrintAll           6 file: centura.h */
 public static  int PRT_nPrintAll = 6;

 /**
  * #define PRT_nShowFormPageRect   9 file: centura.h */
 public static  int PRT_nShowFormPageRect = 9;

 /**
  * #define PRT_nToPage             8 file: centura.h */
 public static  int PRT_nToPage = 8;

 /**
  * #define QS_NORMAL       0 file: tb.h */
 public static  int QS_NORMAL = 0;

 /**
  * #define QS_SPLIT        1 file: tb.h */
 public static  int QS_SPLIT = 1;

 /**
  * #define RF_CellAttribute   0x1000 file: tb.h */
 public static  int RF_CellAttribute = 0x1000;

 /**
  * #define RF_Deleted         0x0010 file: tb.h */
 public static  int RF_Deleted = 0x0010;

 /**
  * #define RF_DontIsolate     0x8000 file: tb.h */
 public static  int RF_DontIsolate = 0x8000;

 /**
  * #define RF_Edited          0x0004 file: tb.h */
 public static  int RF_Edited = 0x0004;

 /**
  * #define RF_Hidden          0x2000 file: tb.h */
 public static  int RF_Hidden = 0x2000;

 /**
  * #define RF_InMemory        0x0001 file: tb.h */
 public static  int RF_InMemory = 0x0001;

 /**
  * #define RF_Inserted        0x0008 file: tb.h */
 public static  int RF_Inserted = 0x0008;

 /**
  * #define RF_Inverted        0x4000 file: tb.h */
 public static  int RF_Inverted = 0x4000;

 /**
  * #define RF_Selected        0x0002 file: tb.h */
 public static  int RF_Selected = 0x0002;

 /**
  * #define RF_User            0x0020 file: tb.h */
 public static  int RF_User = 0x0020;

 /**
  * #define RF_UserMax         0x0800 file: tb.h */
 public static  int RF_UserMax = 0x0800;

 /**
  * #define RF_UserRange       0x0fe0 file: tb.h */
 public static  int RF_UserRange = 0x0fe0;

 /**
  * #define RH_MapColumn       0x0002 file: tb.h */
 public static  int RH_MapColumn = 0x0002;

 /**
  * #define RH_None            0x0000 file: tb.h */
 public static  int RH_None = 0x0000;

 /**
  * #define RH_RowsSizeable    0x0020 file: tb.h */
 public static  int RH_RowsSizeable = 0x0020;

 /**
  * #define RH_ShareColor      0x0010 file: tb.h */
 public static  int RH_ShareColor = 0x0010;

 /**
  * #define RH_UserDraw        0x0001 file: tb.h */
 public static  int RH_UserDraw = 0x0001;

 /**
  * #define RH_UserSizeable    0x0004 file: tb.h */
 public static  int RH_UserSizeable = 0x0004;

 /**
  * #define RH_Visible         0x0008 file: tb.h */
 public static  int RH_Visible = 0x0008;

 /**
  * #define ROW_Edited         0x0004 file: centura.h */
 public static  int ROW_Edited = 0x0004;

 /**
  * #define ROW_Hidden         0x2000 file: centura.h */
 public static  int ROW_Hidden = 0x2000;

 /**
  * #define ROW_HideMars       0x0040 file: centura.h */
 public static  int ROW_HideMars = 0x0040;

 /**
  * #define ROW_MarkDeleted    0x0020 file: centura.h */
 public static  int ROW_MarkDeleted = 0x0020;

 /**
  * #define ROW_New            0x0008 file: centura.h */
 public static  int ROW_New = 0x0008;

 /**
  * #define ROW_Selected       0x0002 file: centura.h */
 public static  int ROW_Selected = 0x0002;

 /**
  * #define ROW_UnusedFlag1    0x0080 file: centura.h */
 public static  int ROW_UnusedFlag1 = 0x0080;

 /**
  * #define ROW_UnusedFlag2    0x0100 file: centura.h */
 public static  int ROW_UnusedFlag2 = 0x0100;

 /**
  * #define RPT_CmdFirstPage   1 file: centura.h */
 public static  int RPT_CmdFirstPage = 1;

 /**
  * #define RPT_CmdLastPage    2 file: centura.h */
 public static  int RPT_CmdLastPage = 2;

 /**
  * #define RPT_CmdNextPage    3 file: centura.h */
 public static  int RPT_CmdNextPage = 3;

 /**
  * #define RPT_CmdPrint       4 file: centura.h */
 public static  int RPT_CmdPrint = 4;

 /**
  * #define RPT_CmdPrinterSetup  7 file: centura.h */
 public static  int RPT_CmdPrinterSetup = 7;

 /**
  * #define RPT_CmdSizeActual  6 file: centura.h */
 public static  int RPT_CmdSizeActual = 6;

 /**
  * #define RPT_CmdSizeFit     5 file: centura.h */
 public static  int RPT_CmdSizeFit = 5;

 /**
  * #define RPT_ErrBind        3     // Bind failure file: centura.h */
 public static  int RPT_ErrBind = 3;

 /**
  * #define RPT_ErrCount       9     // Wrong number of bind/input variables file: centura.h */
 public static  int RPT_ErrCount = 9;

 /**
  * #define RPT_ErrFileOpen    5     // Cannot open report file file: centura.h */
 public static  int RPT_ErrFileOpen = 5;

 /**
  * #define RPT_ErrFilenameLength 4  // Report file name length too long file: centura.h */
 public static  int RPT_ErrFilenameLength = 4;

 /**
  * #define RPT_ErrInput       10    // Mismatch in input variable file: centura.h */
 public static  int RPT_ErrInput = 10;

 /**
  * #define RPT_ErrLoadDLL     1     // Cannot load DLLs file: centura.h */
 public static  int RPT_ErrLoadDLL = 1;

 /**
  * #define RPT_ErrMaxRpts     2     // Maximum # of reports exceeded file: centura.h */
 public static  int RPT_ErrMaxRpts = 2;

 /**
  * #define RPT_ErrPrtOpen     8     // Cannot open printer file: centura.h */
 public static  int RPT_ErrPrtOpen = 8;

 /**
  * #define RPT_ErrRptOpen     7     // Cannot open report for print file: centura.h */
 public static  int RPT_ErrRptOpen = 7;

 /**
  * #define RPT_ErrRptWindow   6     // Cannot open report window file: centura.h */
 public static  int RPT_ErrRptWindow = 6;

 /**
  * #define RPT_ErrType        11    // Mismatch in data type of input variable file: centura.h */
 public static  int RPT_ErrType = 11;

 /**
  * #define RPT_PrintAll   0 file: centura.h */
 public static  int RPT_PrintAll = 0;

 /**
  * #define RPT_PrintDraft   2 file: centura.h */
 public static  int RPT_PrintDraft = 2;

 /**
  * #define RPT_PrintRange   1 file: centura.h */
 public static  int RPT_PrintRange = 1;

 /**
  * #define SAM_AnyEdit        0x2004 file: centura.h */
 public static  int SAM_AnyEdit = 0x2004;

 /**
  * #define SAM_AppExit        0x2002 file: centura.h */
 public static  int SAM_AppExit = 0x2002;

 /**
  * #define SAM_AppStartup     0x2001 file: centura.h */
 public static  int SAM_AppStartup = 0x2001;

 /**
  * #define SAM_CacheFull      0x200B file: centura.h */
 public static  int SAM_CacheFull = 0x200B;

 /**
  * #define SAM_Click          0x2006 file: centura.h */
 public static  int SAM_Click = 0x2006;

 /**
  * #define SAM_Close          0x0010 file: centura.h */
 public static  int SAM_Close = 0x0010;

 /**
  * #define SAM_CountRows      0x2011 file: centura.h */
 public static  int SAM_CountRows = 0x2011;

 /**
  * #define SAM_Create         0x1001 file: centura.h */
 public static  int SAM_Create = 0x1001;

 /**
  * #define SAM_Destroy        0x1002 file: centura.h */
 public static  int SAM_Destroy = 0x1002;

 /**
  * #define SAM_DoubleClick    0x2009 file: centura.h */
 public static  int SAM_DoubleClick = 0x2009;

 /**
  * #define SAM_FetchRow       0x2007 file: centura.h */
 public static  int SAM_FetchRow = 0x2007;

 /**
  * #define SAM_FieldEdit      0x2005 file: centura.h */
 public static  int SAM_FieldEdit = 0x2005;

 /**
  * #define SAM_KillFocus      0x1008 file: centura.h */
 public static  int SAM_KillFocus = 0x1008;

 /**
  * #define SAM_ReportFetchInit    0x200D file: centura.h */
 public static  int SAM_ReportFetchInit = 0x200D;

 /**
  * #define SAM_ReportFetchNext    0x200E file: centura.h */
 public static  int SAM_ReportFetchNext = 0x200E;

 /**
  * #define SAM_ReportFinish   0x2010 file: centura.h */
 public static  int SAM_ReportFinish = 0x2010;

 /**
  * #define SAM_ReportStart    0x200F file: centura.h */
 public static  int SAM_ReportStart = 0x200F;

 /**
  * #define SAM_ScrollBar      0x200A file: centura.h */
 public static  int SAM_ScrollBar = 0x200A;

 /**
  * #define SAM_SetFocus       0x1007 file: centura.h */
 public static  int SAM_SetFocus = 0x1007;

 /**
  * #define SAM_SqlError       0x2003 file: centura.h */
 public static  int SAM_SqlError = 0x2003;

 /**
  * #define SAM_Timer          0x0113 file: centura.h */
 public static  int SAM_Timer = 0x0113;

 /**
  * #define SAM_User           0x4000 file: centura.h */
 public static  int SAM_User = 0x4000;

 /**
  * #define SAM_Validate       0x200C file: centura.h */
 public static  int SAM_Validate = 0x200C;

 /**
  * #define SB_Bottom          7 file: centura.h */
 public static  int SB_Bottom = 7;

 /**
  * #define SB_LineDown        1 file: centura.h */
 public static  int SB_LineDown = 1;

 /**
  * #define SB_LineUp          0 file: centura.h */
 public static  int SB_LineUp = 0;

 /**
  * #define SB_PageDown        3 file: centura.h */
 public static  int SB_PageDown = 3;

 /**
  * #define SB_PageUp          2 file: centura.h */
 public static  int SB_PageUp = 2;

 /**
  * #define SB_ThumbPosition   4 file: centura.h */
 public static  int SB_ThumbPosition = 4;

 /**
  * #define SB_ThumbTrack      5 file: centura.h */
 public static  int SB_ThumbTrack = 5;

 /**
  * #define SB_Top             6 file: centura.h */
 public static  int SB_Top = 6;

 /**
  * #define SWCC_ERROR          0xffff file: swcc.h */
 public static  int SWCC_ERROR = 0xffff;

 /**
  * #define SWCC_NEWSETTINGS    0x0002 file: swcc.h */
 public static  int SWCC_NEWSETTINGS = 0x0002;

 /**
  * #define SWCC_NEWSTYLE       0x0001 file: swcc.h */
 public static  int SWCC_NEWSTYLE = 0x0001;

 /**
  * #define SWCC_NOCHANGE       0x0000 file: swcc.h */
 public static  int SWCC_NOCHANGE = 0x0000;

 /**
  * #define SWCC_PAINTWINDOW    0x0004 file: swcc.h */
 public static  int SWCC_PAINTWINDOW = 0x0004;

 /**
  * #define SWCC_RECREATEWINDOW 0x0008 file: swcc.h */
 public static  int SWCC_RECREATEWINDOW = 0x0008;

 /**
  * #define TABLE_EXTRA     6 file: tb.h */
 public static  int TABLE_EXTRA = 6;

 /**
  * #define TABLE_FLAGS     4  file: tb.h */
 public static  int TABLE_FLAGS = 4;

 /**
  * #define TABLE_HEADER    0 file: tb.h */
 public static  int TABLE_HEADER = 0;

 /**
  * #define TBH_CLOSE             14 file: tb.h */
 public static  int TBH_CLOSE = 14;

 /**
  * #define TBH_COLFORMAT         7 file: tb.h */
 public static  int TBH_COLFORMAT = 7;

 /**
  * #define TBH_COUNTROWS             24 file: tb.h */
 public static  int TBH_COUNTROWS = 24;

 /**
  * #define TBH_CTLCOLOR          8 file: tb.h */
 public static  int TBH_CTLCOLOR = 8;

 /**
  * #define TBH_DEFWINDOW         12 file: tb.h */
 public static  int TBH_DEFWINDOW = 12;

 /**
  * #define TBH_DRAWROWHEADER       22 file: tb.h */
 public static  int TBH_DRAWROWHEADER = 22;

 /**
  * #define TBH_EDITCELL             29 file: tb.h */
 public static  int TBH_EDITCELL = 29;

 /**
  * #define TBH_ENDCELLTAB          23 file: tb.h */
 public static  int TBH_ENDCELLTAB = 23;

 /**
  * #define TBH_FETCHROW          9 file: tb.h */
 public static  int TBH_FETCHROW = 9;

 /**
  * #define TBH_GETCHARBOX           26 file: tb.h */
 public static  int TBH_GETCHARBOX = 26;

 /**
  * #define TBH_GLOBALALLOC       17 file: tb.h */
 public static  int TBH_GLOBALALLOC = 17;

 /**
  * #define TBH_GLOBALREALLOC     18 file: tb.h */
 public static  int TBH_GLOBALREALLOC = 18;

 /**
  * #define TBH_INITCELLEDIT        25 file: tb.h */
 public static  int TBH_INITCELLEDIT = 25;

 /**
  * #define TBH_INITMENU          2 file: tb.h */
 public static  int TBH_INITMENU = 2;

 /**
  * #define TBH_INSERTCOLUMN      19 file: tb.h */
 public static  int TBH_INSERTCOLUMN = 19;

 /**
  * #define TBH_LOCALALLOC        15 file: tb.h */
 public static  int TBH_LOCALALLOC = 15;

 /**
  * #define TBH_LOCALREALLOC      16 file: tb.h */
 public static  int TBH_LOCALREALLOC = 16;

 /**
  * #define TBH_LONGDATA          11 file: tb.h */
 public static  int TBH_LONGDATA = 11;

 /**
  * #define TBH_MARK              0 file: tb.h */
 public static  int TBH_MARK = 0;

 /**
  * #define TBH_MENUSELECT        4 file: tb.h */
 public static  int TBH_MENUSELECT = 4;

 /**
  * #define TBH_OPEN              13 file: tb.h */
 public static  int TBH_OPEN = 13;

 /**
  * #define TBH_OWNERDRAWCOL         27 file: tb.h */
 public static  int TBH_OWNERDRAWCOL = 27;

 /**
  * #define TBH_PAINTICON         3 file: tb.h */
 public static  int TBH_PAINTICON = 3;

 /**
  * #define TBH_QUERYYIELDSTATE      28 file: tb.h */
 public static  int TBH_QUERYYIELDSTATE = 28;

 /**
  * #define TBH_SCROLLTABLE       21 file: tb.h */
 public static  int TBH_SCROLLTABLE = 21;

 /**
  * #define TBH_SYSCOMMAND        5 file: tb.h */
 public static  int TBH_SYSCOMMAND = 5;

 /**
  * #define TBH_TABLEDESIGN       6 file: tb.h */
 public static  int TBH_TABLEDESIGN = 6;

 /**
  * #define TBH_TABLENOTIFY       10 file: tb.h */
 public static  int TBH_TABLENOTIFY = 10;

 /**
  * #define TBH_VALIDATEDATA      20 file: tb.h */
 public static  int TBH_VALIDATEDATA = 20;

 /**
  * #define TBH_YIELD             1 file: tb.h */
 public static  int TBH_YIELD = 1;

 /**
  * #define TBLDDNSTYLE_AlwaysTrapKeys   0x0800 file: tb.h */
 public static  int TBLDDNSTYLE_AlwaysTrapKeys = 0x0800;

 /**
  * #define TBLDDNSTYLE_ButtonOnSide  0x0001 // Position arrow button file: tb.h */
 public static  int TBLDDNSTYLE_ButtonOnSide = 0x0001;

 /**
  * #define TBLDDNSTYLE_ButtonOnTop   0x0002 file: tb.h */
 public static  int TBLDDNSTYLE_ButtonOnTop = 0x0002;

 /**
  * #define TBLDDNSTYLE_DropBelow     0x0008 // Position drop down file: tb.h */
 public static  int TBLDDNSTYLE_DropBelow = 0x0008;

 /**
  * #define TBLDDNSTYLE_DropOver      0x0010 file: tb.h */
 public static  int TBLDDNSTYLE_DropOver = 0x0010;

 /**
  * #define TBLDDNSTYLE_NoButton      0x0004 file: tb.h */
 public static  int TBLDDNSTYLE_NoButton = 0x0004;

 /**
  * #define TBLDDNSTYLE_ShowOnFocus   0x0020 file: tb.h */
 public static  int TBLDDNSTYLE_ShowOnFocus = 0x0020;

 /**
  * #define TBLDDNSTYLE_SizeToFitHeight  0x0400 file: tb.h */
 public static  int TBLDDNSTYLE_SizeToFitHeight = 0x0400;

 /**
  * #define TBLDDNSTYLE_SizeToFitWidth  0x0200 file: tb.h */
 public static  int TBLDDNSTYLE_SizeToFitWidth = 0x0200;

 /**
  * #define TBLDDNSTYLE_Sort          0x0080 file: tb.h */
 public static  int TBLDDNSTYLE_Sort = 0x0080;

 /**
  * #define TBLDDNSTYLE_UserCanHideShow  0x0100 file: tb.h */
 public static  int TBLDDNSTYLE_UserCanHideShow = 0x0100;

 /**
  * #define TBLDDNSTYLE_VScroll       0x0040 file: tb.h */
 public static  int TBLDDNSTYLE_VScroll = 0x0040;

 /**
  * #define TBLWS_DLGITEM     0x4000L file: tb.h */
 public static  int TBLWS_DLGITEM = 0x4000;

 /**
  * #define TBLWS_MDICHILD    0x0001L file: tb.h */
 public static  int TBLWS_MDICHILD = 0x0001;

 /**
  * #define TBL_ADJUST         1 file: tb.h */
 public static  int TBL_ADJUST = 1;

 /**
  * #define TBL_AUTOSCROLL     0 file: tb.h */
 public static  int TBL_AUTOSCROLL = 0;

 /**
  * #define TBL_Adjust         1 file: centura.h */
 public static  int TBL_Adjust = 1;

 /**
  * #define TBL_AutoScroll     0 file: centura.h */
 public static  int TBL_AutoScroll = 0;
 /**
  * #define TBL_MAX            0x7FF0 file: tb.h */
 public static  int TBL_MAX = 0x7FF0;
 
 /**
  * #define TBL_CACHELIMIT     TBL_MAX + 1 file: tb.h */
 public static  int TBL_CACHELIMIT = TBL_MAX + 1;

 /**
  * #define TBL_DONTSCROLL     0xffff file: tb.h */
 public static  int TBL_DONTSCROLL = 0xffff;

 /**
  * #define TBL_ERROR          0x8000 file: tb.h */
 public static  int TBL_ERROR = 0x8000;

 /**
  * #define TBL_Error          0x7FFFFFF1 file: centura.h */
 public static  int TBL_Error = 0x7FFFFFF1;

 /**
  * #define TBL_FETCHCANCELED  3 file: tb.h */
 public static  int TBL_FETCHCANCELED = 3;

 /**
  * #define           TBL_ISCOLUMNHWND        2 file: tb.h */
 public static  int TBL_ISCOLUMNHWND = 2;

 /**
  * #define           TBL_ISNOTWINDOWOFTABLE  0 file: tb.h */
 public static  int TBL_ISNOTWINDOWOFTABLE = 0;

 /**
  * #define           TBL_ISTABLEHWND         1 file: tb.h */
 public static  int TBL_ISTABLEHWND = 1;



 /**
  * #define TBL_MIN            -TBL_MAX file: tb.h */
 public static  int TBL_MIN = -TBL_MAX;

 /**
  * #define TBL_MINSPLIT       (TBL_MIN+2) file: tb.h */
 public static  int TBL_MINSPLIT = TBL_MIN+2;

 /**
  * #define TBL_MaxRow         0x7FFFFFF0 file: centura.h */
 public static  int TBL_MaxRow = 0x7FFFFFF0;

 /**
  * #define TBL_MinRow         -TBL_MaxRow file: centura.h */
 public static  int TBL_MinRow = -TBL_MaxRow;

 /**
  * #define TBL_NOADJUST       0 file: tb.h */
 public static  int TBL_NOADJUST = 0;

 /**
  * #define TBL_NOMOREROWS     0 file: tb.h */
 public static  int TBL_NOMOREROWS = 0;

 /**
  * #define TBL_NoAdjust       0 file: centura.h */
 public static  int TBL_NoAdjust = 0;

 /**
  * #define TBL_NoMoreRows     0 file: centura.h */
 public static  int TBL_NoMoreRows = 0;

 /**
  * #define TBL_ROWDELETED     2 file: tb.h */
 public static  int TBL_ROWDELETED = 2;

 /**
  * #define TBL_ROWFETCHED     1 file: tb.h */
 public static  int TBL_ROWFETCHED = 1;

 /**
  * #define TBL_RowDeleted     2 file: centura.h */
 public static  int TBL_RowDeleted = 2;

 /**
  * #define TBL_RowFetched     1 file: centura.h */
 public static  int TBL_RowFetched = 1;

 /**
  * #define TBL_SCROLLBOTTOM   2 file: tb.h */
 public static  int TBL_SCROLLBOTTOM = 2;

 /**
  * #define TBL_SCROLLCARET    3 file: tb.h */
 public static  int TBL_SCROLLCARET = 3;

 /**
  * #define TBL_SCROLLTOP      1 file: tb.h */
 public static  int TBL_SCROLLTOP = 1;

 /**
  * #define TBL_ScrollBottom   2 file: centura.h */
 public static  int TBL_ScrollBottom = 2;

 /**
  * #define TBL_ScrollTop      1 file: centura.h */
 public static  int TBL_ScrollTop = 1;

 /**
  * #define TBL_TEMPROW        TBL_ERROR file: tb.h */
 public static  int TBL_TEMPROW = TBL_ERROR;

 /**
  * #define TBL_TempRow        0x7FFFFFF1 file: centura.h */
 public static  int TBL_TempRow = 0x7FFFFFF1;

 /**
  * #define TBN_CACHEFULL      0x070b file: tb.h */
 public static  int TBN_CACHEFULL = 0x070b;

 /**
  * #define TBN_CAPTIONDOUBLECLK    0x0716 file: tb.h */
 public static  int TBN_CAPTIONDOUBLECLK = 0x0716;

 /**
  * #define TBN_CAPTIONRIGHTCLK     0x0722 file: tb.h */
 public static  int TBN_CAPTIONRIGHTCLK = 0x0722;

 /**
  * #define TBN_CHANGE         0x0702 file: tb.h */
 public static  int TBN_CHANGE = 0x0702;

 /**
  * #define TBN_CLICK          0x0708 file: tb.h */
 public static  int TBN_CLICK = 0x0708;

 /**
  * #define TBN_COLUMNDELETE   0x0710 file: tb.h */
 public static  int TBN_COLUMNDELETE = 0x0710;

 /**
  * #define TBN_COLUMNINSERT   0x070f file: tb.h */
 public static  int TBN_COLUMNINSERT = 0x070f;

 /**
  * #define TBN_COLUMNMOVE     0x070d file: tb.h */
 public static  int TBN_COLUMNMOVE = 0x070d;

 /**
  * #define TBN_COLUMNSELECT   0x070e file: tb.h */
 public static  int TBN_COLUMNSELECT = 0x070e;

 /**
  * #define TBN_COLUMNSELECTCLICK   0x0721 file: tb.h */
 public static  int TBN_COLUMNSELECTCLICK = 0x0721;

 /**
  * #define TBN_COLUMNSIZE     0x070c file: tb.h */
 public static  int TBN_COLUMNSIZE = 0x070c;

 /**
  * #define TBN_COLUMNUNSELECT 0x0712 file: tb.h */
 public static  int TBN_COLUMNUNSELECT = 0x0712;

 /**
  * #define TBN_CONTROLSTYLE        0x0727 file: tb.h */
 public static  int TBN_CONTROLSTYLE = 0x0727;

 /**
  * #define TBN_CORNERCLICK         0x0719 file: tb.h */
 public static  int TBN_CORNERCLICK = 0x0719;

 /**
  * #define TBN_CORNERDOUBLECLK     0x0720 file: tb.h */
 public static  int TBN_CORNERDOUBLECLK = 0x0720;

 /**
  * #define TBN_CORNERRIGHTCLK      0x0724 file: tb.h */
 public static  int TBN_CORNERRIGHTCLK = 0x0724;

 /**
  * #define TBN_DESTROY        0x070a file: tb.h */
 public static  int TBN_DESTROY = 0x070a;

 /**
  * #define TBN_DISCARDINGROW       0x0728 file: tb.h */
 public static  int TBN_DISCARDINGROW = 0x0728;

 /**
  * #define TBN_DOUBLECLK      0x0709 file: tb.h */
 public static  int TBN_DOUBLECLK = 0x0709;

 /**
  * #define TBN_DROPDOWN            0x0729 file: tb.h */
 public static  int TBN_DROPDOWN = 0x0729;

 /**
  * #define TBN_ERRHEAP        0x0707 file: tb.h */
 public static  int TBN_ERRHEAP = 0x0707;

 /**
  * #define TBN_ERRSPACE       0x0704 file: tb.h */
 public static  int TBN_ERRSPACE = 0x0704;

 /**
  * #define TBN_FIELDDOUBLECLICK    0x0725 file: tb.h */
 public static  int TBN_FIELDDOUBLECLICK = 0x0725;

 /**
  * #define TBN_FIRSTCHANGE    0x0713 file: tb.h */
 public static  int TBN_FIRSTCHANGE = 0x0713;

 /**
  * #define TBN_FOCUSROWCHANGED     0x0726 file: tb.h */
 public static  int TBN_FOCUSROWCHANGED = 0x0726;

 /**
  * #define TBN_HSCROLL        0x0705 file: tb.h */
 public static  int TBN_HSCROLL = 0x0705;

 /**
  * #define TBN_KILLEDIT       0x0714 file: tb.h */
 public static  int TBN_KILLEDIT = 0x0714;

 /**
  * #define TBN_KILLFOCUS      0x0701 file: tb.h */
 public static  int TBN_KILLFOCUS = 0x0701;

 /**
  * #define TBN_RIGHTCLK            0x0723 file: tb.h */
 public static  int TBN_RIGHTCLK = 0x0723;

 /**
  * #define TBN_ROWHEADERCLICK      0x0717 file: tb.h */
 public static  int TBN_ROWHEADERCLICK = 0x0717;

 /**
  * #define TBN_ROWHEADERDOUBLECLK  0x0718 file: tb.h */
 public static  int TBN_ROWHEADERDOUBLECLK = 0x0718;

 /**
  * #define TBN_ROWSIZE             0x072a file: tb.h */
 public static  int TBN_ROWSIZE = 0x072a;

 /**
  * #define TBN_SELCHANGE      0x0715 file: tb.h */
 public static  int TBN_SELCHANGE = 0x0715;

 /**
  * #define TBN_SETFOCUS       0x0700 file: tb.h */
 public static  int TBN_SETFOCUS = 0x0700;

 /**
  * #define TBN_TOGGLECAPTION  0x0711 file: tb.h */
 public static  int TBN_TOGGLECAPTION = 0x0711;

 /**
  * #define TBN_UPDATE         0x0703 file: tb.h */
 public static  int TBN_UPDATE = 0x0703;

 /**
  * #define TBN_VSCROLL        0x0706 file: tb.h */
 public static  int TBN_VSCROLL = 0x0706;

 /**
  * #define TBR_FETCHROW          (WM_USER+0) file: tb.h */
 public static  int TBR_FETCHROW = WM_USER+0;

 /**
  * #define TBR_INSERTCOLUMN      (WM_USER+1) file: tb.h */
 public static  int TBR_INSERTCOLUMN = WM_USER+1;

 /**
  * #define TC_ADJUSTSPLIT      0x0020      /* Split bar can be dragged       file: tb.h */
 public static  int TC_ADJUSTSPLIT = 0x0020;

 /**
  * #define TC_ALWAYSVERTSCROLL 0x0004      /* Always display scroll bar?     file: tb.h */
 public static  int TC_ALWAYSVERTSCROLL = 0x0004;

 /**
  * #define TC_EDITLEFTJUSTIFY  0x4000      /* Always left justify when edit  file: tb.h */
 public static  int TC_EDITLEFTJUSTIFY = 0x4000;

 /**
  * #define TC_GRAYHEADERS      0x0200      /* Column and row headers are gray  file: tb.h */
 public static  int TC_GRAYHEADERS = 0x0200;

 /**
  * #define TC_HSCROLLBYCOLUMNS 0x0100      /* Scroll by columns, like Excel    file: tb.h */
 public static  int TC_HSCROLLBYCOLUMNS = 0x0100;

 /**
  * #define TC_IGNOREINSKEY     0x0010      /* Insert key is not used         file: tb.h */
 public static  int TC_IGNOREINSKEY = 0x0010;

 /**
  * #define TC_MOVECOLUMNS      0x0002      /* Can columns be moved?          file: tb.h */
 public static  int TC_MOVECOLUMNS = 0x0002;

 /**
  * #define TC_NOHIDESEL        0x0040      /* Used ES_NOHIDESEL style        file: tb.h */
 public static  int TC_NOHIDESEL = 0x0040;

 /**
  * #define TC_SELECTCOLUMNS    0x0080      /* Click on caption selects column  file: tb.h */
 public static  int TC_SELECTCOLUMNS = 0x0080;

 /**
  * #define TC_SELECTROWS       0x0008      /* Hit area for rows?             file: tb.h */
 public static  int TC_SELECTROWS = 0x0008;

 /**
  * #define TC_SINGLESELECTION  0x1000      /* Single selection only ?          file: tb.h */
 public static  int TC_SINGLESELECTION = 0x1000;

 /**
  * #define TC_SIZECOLUMNS      0x0001      /* Can columns be sized?          file: tb.h */
 public static  int TC_SIZECOLUMNS = 0x0001;

 /**
  * #define TC_SUPPRESSLASTCOLLINE 0x2000   /* Don't display last vertical line  file: tb.h */
 public static  int TC_SUPPRESSLASTCOLLINE = 0x2000;

 /**
  * #define TC_SUPPRESSROWLINES 0x0800      /* Don't display horiz. grid lines  file: tb.h */
 public static  int TC_SUPPRESSROWLINES = 0x0800;

 /**
  * #define TC_WAITCURSOR       0x0400      /* Show wait cursor ?               file: tb.h */
 public static  int TC_WAITCURSOR = 0x0400;

 /**
  * #define TF_ICONSET    0x0001  file: tb.h */
 public static  int TF_ICONSET = 0x0001;

 /**
  * #define TV_FieldView   3    //  Form view without borders or ribbon file: tb.h */
 public static  int TV_FieldView = 3;

 /**
  * #define TV_FormView    2    //  Dialog box-like view  file: tb.h */
 public static  int TV_FormView = 2;

 /**
  * #define TV_ListView    1    //  Spreadsheet-like view. file: tb.h */
 public static  int TV_ListView = 1;

 /**
  * #define TV_NullView    0    //  No user-interface  file: tb.h */
 public static  int TV_NullView = 0;

 /**
  * #define TV_VCRView     4    //  VCR View file: tb.h */
 public static  int TV_VCRView = 4;

 /**
  * #define TV_ViewCount   5    //  Number of views file: tb.h */
 public static  int TV_ViewCount = 5;

 /**
  * #define TYPE_AccFrame           0x10000000L file: centura.h */
 public static  int TYPE_AccFrame = 0x10000000;

 /**
  * #define TYPE_ActiveX            0x08000000L file: centura.h */
 public static  int TYPE_ActiveX = 0x08000000;

 /**
  * #define TYPE_Any                0x7fffffff file: centura.h */
 public static  int TYPE_Any = 0x7fffffff;

 /**
  * #define TYPE_BkgdText           0x0800 file: centura.h */
 public static  int TYPE_BkgdText = 0x0800;

 /**
  * #define TYPE_CheckBox           0x0080 file: centura.h */
 public static  int TYPE_CheckBox = 0x0080;

 /**
  * #define TYPE_ChildTable         0x00080000L file: centura.h */
 public static  int TYPE_ChildTable = 0x00080000;

 /**
  * #define TYPE_ComboBox           0x4000 file: centura.h */
 public static  int TYPE_ComboBox = 0x4000;

 /**
  * #define TYPE_CustControl        0x04000000L file: centura.h */
 public static  int TYPE_CustControl = 0x04000000;

 /**
  * #define TYPE_DataField          0x0008 file: centura.h */
 public static  int TYPE_DataField = 0x0008;

 /**
  * #define TYPE_DialogBox          0x0004 file: centura.h */
 public static  int TYPE_DialogBox = 0x0004;

 /**
  * #define TYPE_FormToolBar        0x00200000L file: centura.h */
 public static  int TYPE_FormToolBar = 0x00200000;

 /**
  * #define TYPE_FormWindow         0x0001 file: centura.h */
 public static  int TYPE_FormWindow = 0x0001;

 /**
  * #define TYPE_Frame              0x00010000L file: centura.h */
 public static  int TYPE_Frame = 0x00010000;

 /**
  * #define TYPE_GroupBox           0x0100 file: centura.h */
 public static  int TYPE_GroupBox = 0x0100;

 /**
  * #define TYPE_HorizScrollBar     0x0200 file: centura.h */
 public static  int TYPE_HorizScrollBar = 0x0200;

 /**
  * #define TYPE_Line               0x8000 file: centura.h */
 public static  int TYPE_Line = 0x8000;

 /**
  * #define TYPE_ListBox            0x2000 file: centura.h */
 public static  int TYPE_ListBox = 0x2000;

 /**
  * #define TYPE_MDIWindow          0x00100000L file: centura.h */
 public static  int TYPE_MDIWindow = 0x00100000;

 /**
  * #define TYPE_MultilineText      0x0010 file: centura.h */
 public static  int TYPE_MultilineText = 0x0010;

 /**
  * #define TYPE_OptButton          0x02000000L file: centura.h */
 public static  int TYPE_OptButton = 0x02000000;

 /**
  * #define TYPE_Other              0x40000000L file: centura.h */
 public static  int TYPE_Other = 0x40000000;

 /**
  * #define TYPE_Pict               0x00020000L file: centura.h */
 public static  int TYPE_Pict = 0x00020000;

 /**
  * #define TYPE_PushButton         0x0020 file: centura.h */
 public static  int TYPE_PushButton = 0x0020;

 /**
  * #define TYPE_RadioButton        0x0040 file: centura.h */
 public static  int TYPE_RadioButton = 0x0040;

 /**
  * #define TYPE_SpinField          0x01000000L file: centura.h */
 public static  int TYPE_SpinField = 0x01000000;

 /**
  * #define TYPE_TableColumn        0x1000 file: centura.h */
 public static  int TYPE_TableColumn = 0x1000;

 /**
  * #define TYPE_TableWindow        0x0002 file: centura.h */
 public static  int TYPE_TableWindow = 0x0002;

 /**
  * #define TYPE_VertScrollBar      0x0400 file: centura.h */
 public static  int TYPE_VertScrollBar = 0x0400;

 /**
  * #define USER_TBL_FillAll        1 file: centura.h */
 public static  int USER_TBL_FillAll = 1;

 /**
  * #define USER_TBL_FillAllBkgd    2 file: centura.h */
 public static  int USER_TBL_FillAllBkgd = 2;

 /**
  * #define USER_TBL_FillNormal     0 file: centura.h */
 public static  int USER_TBL_FillNormal = 0;

 /**
  * #define VALIDATE_CANCEL    0 file: tb.h */
 public static  int VALIDATE_CANCEL = 0;

 /**
  * #define VALIDATE_Cancel        0 file: centura.h */
 public static  int VALIDATE_Cancel = 0;

 /**
  * #define VALIDATE_OK        1 file: tb.h */
 public static  int VALIDATE_OK = 1;

 /**
  * #define VALIDATE_OKCLEAR   2 file: tb.h */
 public static  int VALIDATE_OKCLEAR = 2;

 /**
  * #define VALIDATE_OKRESET   3  // Reset field to original value file: tb.h */
 public static  int VALIDATE_OKRESET = 3;

 /**
  * #define VALIDATE_Ok            1 file: centura.h */
 public static  int VALIDATE_Ok = 1;

 /**
  * #define VALIDATE_OkClearFlag   2 file: centura.h */
 public static  int VALIDATE_OkClearFlag = 2;

 /**
  * #define WINDOW_INVALID      1 file: centura.h */
 public static  int WINDOW_INVALID = 1;

 /**
  * #define WINDOW_MAXIMIZED    3 file: centura.h */
 public static  int WINDOW_MAXIMIZED = 3;

 /**
  * #define WINDOW_MINIMIZED    4 file: centura.h */
 public static  int WINDOW_MINIMIZED = 4;

 /**
  * #define WINDOW_NORMAL       5 file: centura.h */
 public static  int WINDOW_NORMAL = 5;

 /**
  * #define WINDOW_NOTVISIBLE   2 file: centura.h */
 public static  int WINDOW_NOTVISIBLE = 2;

 /**
  * #define WM_DDE_First       0x03E0 file: centura.h */
 public static  int WM_DDE_First = 0x03E0;
 
 /**
  * #define WM_DDE_Ack         WM_DDE_First + 4 file: centura.h */
 public static  int WM_DDE_Ack = WM_DDE_First + 4;

 /**
  * #define WM_DDE_Advise      WM_DDE_First + 2 file: centura.h */
 public static  int WM_DDE_Advise = WM_DDE_First + 2;

 /**
  * #define WM_DDE_Data        WM_DDE_First + 5 file: centura.h */
 public static  int WM_DDE_Data = WM_DDE_First + 5;

 /**
  * #define WM_DDE_Execute     WM_DDE_First + 8 file: centura.h */
 public static  int WM_DDE_Execute = WM_DDE_First + 8;

 

 /**
  * #define WM_DDE_Initiate    WM_DDE_First + 0 file: centura.h */
 public static  int WM_DDE_Initiate = WM_DDE_First + 0;

 /**
  * #define WM_DDE_Poke        WM_DDE_First + 7 file: centura.h */
 public static  int WM_DDE_Poke = WM_DDE_First + 7;

 /**
  * #define WM_DDE_Request     WM_DDE_First + 6 file: centura.h */
 public static  int WM_DDE_Request = WM_DDE_First + 6;

 /**
  * #define WM_DDE_Terminate   WM_DDE_First + 1 file: centura.h */
 public static  int WM_DDE_Terminate = WM_DDE_First + 1;

 /**
  * #define WM_DDE_Unadvise    WM_DDE_First + 3 file: centura.h */
 public static  int WM_DDE_Unadvise = WM_DDE_First + 3;

 /**
  * #define _WINBIND_H_ 1 file: winbind.h */
 public static  int _WINBIND_H_ = 1;
  /**
   * HWND CBEXPAPI SWinAppFind(LPSTR lpModuleName, BOOL bActivate); file: centura200.h */
  public static  CWndHandle SWinAppFind(CString lpModuleName, CBoolean bActivate) {
    return centuraAPIimpl.SWinAppFind(lpModuleName,bActivate);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtDoubleToNumber(double, LPNUMBER); file: centura200.h */
  public static  CBoolean SWinCvtDoubleToNumber(CNumber var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtDoubleToNumber(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtIntToNumber(INT, LPNUMBER); file: centura200.h */
  public static  CBoolean SWinCvtIntToNumber(CNumber var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtIntToNumber(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtLongToNumber(LONG, LPNUMBER); file: centura200.h */
  public static  CBoolean SWinCvtLongToNumber(CNumber var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtLongToNumber(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToDouble(LPNUMBER, double FAR *); file: centura200.h */
  public static  CBoolean SWinCvtNumberToDouble(CNumber.ByReference var1, CNumber var2) {
    return centuraAPIimpl.SWinCvtNumberToDouble(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToInt(LPNUMBER, LPINT); file: centura200.h */
  public static  CBoolean SWinCvtNumberToInt(CNumber.ByReference var1,
      CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtNumberToInt(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToLong(LPNUMBER, LPLONG); file: centura200.h */
  public static  CBoolean SWinCvtNumberToLong(CNumber.ByReference var1,
      CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtNumberToLong(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToULong(LPNUMBER, LPDWORD); file: centura200.h */
  public static  CBoolean SWinCvtNumberToULong(CNumber.ByReference var1, CNumber var2) {
    return centuraAPIimpl.SWinCvtNumberToULong(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToWord(LPNUMBER, LPWORD); file: centura200.h */
  public static  CBoolean SWinCvtNumberToWord(CNumber.ByReference var1,
      CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtNumberToWord(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtULongToNumber(ULONG, LPNUMBER); file: centura200.h */
  public static  CBoolean SWinCvtULongToNumber(CNumber var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtULongToNumber(var1,var2);
  }

  /**
   * BOOL CBEXPAPI  SWinCvtWordToNumber(WORD, LPNUMBER); file: centura200.h */
  public static  CBoolean SWinCvtWordToNumber(CNumber var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SWinCvtWordToNumber(var1,var2);
  }

  /**
   * HWND CBEXPAPI SWinFindWindow(HWND hWndContainer, LPSTR lpszSymbol); file: centura200.h */
  public static  CWndHandle SWinFindWindow(CWndHandle hWndContainer, CString lpszSymbol) {
    return centuraAPIimpl.SWinFindWindow(hWndContainer,lpszSymbol);
  }

  /**
   * int  CBEXPAPI SWinGetSymbol(HWND hWnd, LPSTR lpszSymbol, int nMaxLength); file: centura200.h */
  public static  CNumber SWinGetSymbol(CWndHandle hWnd, CString lpszSymbol,
      CNumber nMaxLength) {
    return centuraAPIimpl.SWinGetSymbol(hWnd,lpszSymbol,nMaxLength);
  }

  /**
   * LONG CBEXPAPI SWinGetType(HWND hWnd); file: centura200.h */
  public static  CNumber SWinGetType(CWndHandle hWnd) {
    return centuraAPIimpl.SWinGetType(hWnd);
  }

  /**
   * BOOL CBEXPAPI SWinHSCreateDesignHeap(VOID); file: centura200.h */
  public static  CBoolean SWinHSCreateDesignHeap() {
    return centuraAPIimpl.SWinHSCreateDesignHeap();
  }

  /**
   * BOOL CBEXPAPI SWinHSDestroyDesignHeap(VOID); file: centura200.h */
  public static  CBoolean SWinHSDestroyDesignHeap() {
    return centuraAPIimpl.SWinHSDestroyDesignHeap();
  }

  /**
   * BOOL CBEXPAPI SWinInitLPHSTRINGParam(LPHSTRING, LONG); file: centura200.h */
  public static  CBoolean SWinInitLPHSTRINGParam(CString.ByReference var1, CNumber var2) {
    return centuraAPIimpl.SWinInitLPHSTRINGParam(var1,var2);
  }

  /**
   * BOOL CBEXPAPI SWinIsOurWindow(HWND hWnd); file: centura200.h */
  public static  CBoolean SWinIsOurWindow(CWndHandle hWnd) {
    return centuraAPIimpl.SWinIsOurWindow(hWnd);
  }

  /**
   * BOOL CBEXPAPI  SWinMDArrayDateType(HARRAY, LPINT); file: centura200.h */
  public static  CBoolean SWinMDArrayDateType(CArray<?> var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SWinMDArrayDateType(var1,var2);
  }

  /**
   * LPSTR CBEXPAPI SWinStringGetBuffer(HSTRING, LPLONG); file: centura200.h */
  public static  CString SWinStringGetBuffer(CString var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SWinStringGetBuffer(var1,var2);
  }

  /**
   * LPSTR   CBEXPAPI SWinUdvDeref( HUDV ); file: centura200.h */
  public static  CString SWinUdvDeref(CHandle var1) {
    return centuraAPIimpl.SWinUdvDeref(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalAbort(INT); file: centura200.h */
  public static  CBoolean SalAbort(CNumber var1) {
    return centuraAPIimpl.SalAbort(var1);
  }

  /**
   * VOID      CBEXPAPI  SalActiveXAutoErrorMode(BOOL); file: centura200.h */
  public static  void SalActiveXAutoErrorMode(CBoolean var1) {
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXClose(HWND, BOOL); file: centura200.h */
  public static  CBoolean SalActiveXClose(CWndHandle var1, CBoolean var2) {
    return centuraAPIimpl.SalActiveXClose(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXCreate(HWND, LPCSTR); file: centura200.h */
  public static  CBoolean SalActiveXCreate(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalActiveXCreate(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXCreateFromData(HWND, HSTRING); file: centura200.h */
  public static  CBoolean SalActiveXCreateFromData(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalActiveXCreateFromData(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXCreateFromFile(HWND, LPCSTR); file: centura200.h */
  public static  CBoolean SalActiveXCreateFromFile(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalActiveXCreateFromFile(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXDelete(HWND); file: centura200.h */
  public static  CBoolean SalActiveXDelete(CWndHandle var1) {
    return centuraAPIimpl.SalActiveXDelete(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXDoVerb(HWND, LONG, BOOL); file: centura200.h */
  public static  CBoolean SalActiveXDoVerb(CWndHandle var1, CNumber var2, CBoolean var3) {
    return centuraAPIimpl.SalActiveXDoVerb(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXGetActiveObject(HUDV, LPSTR); file: centura200.h */
  public static  CBoolean SalActiveXGetActiveObject(CHandle var1, CString var2) {
    return centuraAPIimpl.SalActiveXGetActiveObject(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXGetData(HWND, LPHSTRING); file: centura200.h */
  public static  CBoolean SalActiveXGetData(CWndHandle var1, CString.ByReference var2) {
    return centuraAPIimpl.SalActiveXGetData(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXGetObject(HWND, HUDV); file: centura200.h */
  public static  CBoolean SalActiveXGetObject(CWndHandle var1, CHandle var2) {
    return centuraAPIimpl.SalActiveXGetObject(var1,var2);
  }

  /**
   * LONG      CBEXPAPI  SalActiveXInsertObjectDlg(HWND); file: centura200.h */
  public static  CNumber SalActiveXInsertObjectDlg(CWndHandle var1) {
    return centuraAPIimpl.SalActiveXInsertObjectDlg(var1);
  }

  /**
   * LONG      CBEXPAPI  SalActiveXOLEType(HWND); file: centura200.h */
  public static  CNumber SalActiveXOLEType(CWndHandle var1) {
    return centuraAPIimpl.SalActiveXOLEType(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalAppDisable(VOID); file: centura200.h */
  public static  CBoolean SalAppDisable() {
    return centuraAPIimpl.SalAppDisable();
  }

  /**
   * BOOL      CBEXPAPI  SalAppEnable(VOID); file: centura200.h */
  public static  CBoolean SalAppEnable() {
    return centuraAPIimpl.SalAppEnable();
  }

  /**
   * HWND      CBEXPAPI  SalAppFind(LPSTR, BOOL); file: centura200.h */
  public static  CWndHandle SalAppFind(CString var1, CBoolean var2) {
    return centuraAPIimpl.SalAppFind(var1,var2);
  }

  /**
   * NUMBER    CBEXPAPI  SalArrayAvg(HARRAY); file: centura200.h */
  public static  CNumber SalArrayAvg(CArray<?> var1) {
    return centuraAPIimpl.SalArrayAvg(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalArrayDimCount(HARRAY, LPLONG); file: centura200.h */
  public static  CBoolean SalArrayDimCount(CArray<?> var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SalArrayDimCount(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalArrayGetLowerBound(HARRAY, INT, LPLONG); file: centura200.h */
  public static  CBoolean SalArrayGetLowerBound(CArray<?> var1, CNumber var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalArrayGetLowerBound(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalArrayGetUpperBound(HARRAY, INT, LPLONG); file: centura200.h */
  public static  CBoolean SalArrayGetUpperBound(CArray<?> var1, CNumber var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalArrayGetUpperBound(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalArrayIsEmpty(HARRAY); file: centura200.h */
  public static  CBoolean SalArrayIsEmpty(CArray<?> var1) {
    return centuraAPIimpl.SalArrayIsEmpty(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalArrayMax(HARRAY); file: centura200.h */
  public static CNumber SalArrayMax(CArray<?> var1) {
    return centuraAPIimpl.SalArrayMax(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalArrayMin(HARRAY); file: centura200.h */
  public static  CNumber SalArrayMin(CArray<?> var1) {
    return centuraAPIimpl.SalArrayMin(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalArraySetUpperBound(HARRAY, INT, LONG); file: centura200.h */
  public static  CBoolean SalArraySetUpperBound(CArray<?> var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalArraySetUpperBound(var1,var2,var3);
  }

  /**
   * NUMBER    CBEXPAPI  SalArraySum(HARRAY); file: centura200.h */
  public static  CNumber SalArraySum(CArray<?> var1) {
    return centuraAPIimpl.SalArraySum(var1);
  }

  /**
   * VOID  CBEXPAPI SalAssignUDV(LPHANDLE A, LPHANDLE B, WORD fgs); file: centura200.h */
  public static  void SalAssignUDV(CHandle.ByReference A, CHandle.ByReference B, CNumber fgs) {
  }

  /**
   * BOOL      CBEXPAPI  SalBringWindowToTop(HWND); file: centura200.h */
  public static  CBoolean SalBringWindowToTop(CWndHandle var1) {
    return centuraAPIimpl.SalBringWindowToTop(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalCenterWindow(HWND); file: centura200.h */
  public static  CBoolean SalCenterWindow(CWndHandle var1) {
    return centuraAPIimpl.SalCenterWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalClearField(HWND); file: centura200.h */
  public static  CBoolean SalClearField(CWndHandle var1) {
    return centuraAPIimpl.SalClearField(var1);
  }

  /**
   * DWORD     CBEXPAPI  SalColorFromRGB(BYTE, BYTE, BYTE); file: centura200.h */
  public static  CNumber SalColorFromRGB(CNumber var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalColorFromRGB(var1,var2,var3);
  }

  /**
   * DWORD     CBEXPAPI  SalColorGet(HWND, INT); file: centura200.h */
  public static  CNumber SalColorGet(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalColorGet(var1,var2);
  }

  /**
   * DWORD     CBEXPAPI  SalColorGetSysColor(INT); file: centura200.h */
  public static  CNumber SalColorGetSysColor(CNumber var1) {
    return centuraAPIimpl.SalColorGetSysColor(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalColorSet(HWND, INT, DWORD); file: centura200.h */
  public static  CBoolean SalColorSet(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalColorSet(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalColorToRGB(DWORD, LPBYTE, LPBYTE, LPBYTE); file: centura200.h */
  public static  CBoolean SalColorToRGB(CNumber var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalColorToRGB(var1,var2,var3,var4);
  }

  /**
   * INT       CBEXPAPI  SalCompileAndEvaluate(LPSTR, LPINT, LPINT, LPNUMBER, LPHSTRING, LPDATETIME, LPHWND, BOOL, LPSTR); file: centura200.h */
  public static  CNumber SalCompileAndEvaluate(CString var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CString.ByReference var5,
      CDateTime.ByReference var6, CWndHandle.ByReference var7, CBoolean var8, CString var9) {
    return centuraAPIimpl.SalCompileAndEvaluate(var1,var2,var3,var4,var5,var6,var7,var8,var9);
  }

  /**
   * HSTRING   CBEXPAPI  SalContextBreak(VOID); file: centura200.h */
  public static  CString SalContextBreak() {
    return centuraAPIimpl.SalContextBreak();
  }

  /**
   * HSTRING   CBEXPAPI  SalContextCurrent(VOID); file: centura200.h */
  public static  CString SalContextCurrent() {
    return centuraAPIimpl.SalContextCurrent();
  }

  /**
   * BOOL      CBEXPAPI  SalContextMenuSetPopup(HWND, LPSTR, DWORD); file: centura200.h */
  public static  CBoolean SalContextMenuSetPopup(CWndHandle var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalContextMenuSetPopup(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalCursorClear(HWND, WORD); file: centura200.h */
  public static  CBoolean SalCursorClear(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalCursorClear(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalCursorSet(HWND, TEMPLATE, WORD); file: centura200.h */
  public static  CBoolean SalCursorSet(CWndHandle var1, CStruct var2, CNumber var3) {
    return centuraAPIimpl.SalCursorSet(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalCursorSetFile(HWND, LPSTR, WORD); file: centura200.h */
  public static  CBoolean SalCursorSetFile(CWndHandle var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalCursorSetFile(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalCursorSetString(HWND, HSTRING, WORD); file: centura200.h */
  public static  CBoolean SalCursorSetString(CWndHandle var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalCursorSetString(var1,var2,var3);
  }

  /**
   * ATOM      CBEXPAPI  SalDDEAddAtom(LPSTR); file: centura200.h */
  public static  CNumber SalDDEAddAtom(CString var1) {
    return centuraAPIimpl.SalDDEAddAtom(var1);
  }

  /**
   * HGLOBAL   CBEXPAPI  SalDDEAlloc(VOID); file: centura200.h */
  public static  CHandle SalDDEAlloc() {
    return centuraAPIimpl.SalDDEAlloc();
  }

  /**
   * ATOM      CBEXPAPI  SalDDEDeleteAtom(ATOM); file: centura200.h */
  public static  CNumber SalDDEDeleteAtom(CNumber var1) {
    return centuraAPIimpl.SalDDEDeleteAtom(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtract(WPARAM, LPARAM, LPHWND, LPDWORD, LPDWORD); file: centura200.h */
  public static  CBoolean SalDDEExtract(CNumber var1, CNumber var2,
      CWndHandle.ByReference var3, CNumber var4, CNumber var5) {
    return centuraAPIimpl.SalDDEExtract(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtractCmd(HGLOBAL, LPHSTRING, INT); file: centura200.h */
  public static  CBoolean SalDDEExtractCmd(CHandle var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SalDDEExtractCmd(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtractDataText(HGLOBAL, LPWORD, LPHSTRING, INT); file: centura200.h */
  public static  CBoolean SalDDEExtractDataText(CHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CNumber var4) {
    return centuraAPIimpl.SalDDEExtractDataText(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtractOptions(HGLOBAL, LPWORD, LPWORD); file: centura200.h */
  public static  CBoolean SalDDEExtractOptions(CHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalDDEExtractOptions(var1,var2,var3);
  }

  /**
   * ATOM      CBEXPAPI  SalDDEFindAtom(LPSTR); file: centura200.h */
  public static  CNumber SalDDEFindAtom(CString var1) {
    return centuraAPIimpl.SalDDEFindAtom(var1);
  }

  /**
   * HGLOBAL   CBEXPAPI  SalDDEFree(HGLOBAL); file: centura200.h */
  public static  CHandle SalDDEFree(CHandle var1) {
    return centuraAPIimpl.SalDDEFree(var1);
  }

  /**
   * UINT      CBEXPAPI  SalDDEGetAtomName(ATOM, LPHSTRING, INT); file: centura200.h */
  public static  CNumber SalDDEGetAtomName(CNumber var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SalDDEGetAtomName(var1,var2,var3);
  }

  /**
   * HSTRING   CBEXPAPI  SalDDEGetExecuteString(LPARAM); file: centura200.h */
  public static  CString SalDDEGetExecuteString(CNumber var1) {
    return centuraAPIimpl.SalDDEGetExecuteString(var1);
  }

  /**
   * LONG      CBEXPAPI  SalDDEPost(HWND, UINT, HWND, UINT, UINT); file: centura200.h */
  public static  CNumber SalDDEPost(CWndHandle var1, CNumber var2, CWndHandle var3,
      CNumber var4, CNumber var5) {
    return centuraAPIimpl.SalDDEPost(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalDDERequest(HWND, LPSTR, LPSTR, LPSTR, LONG, LPHSTRING); file: centura200.h */
  public static CBoolean SalDDERequest(CWndHandle var1, CString var2, CString var3,
      CString var4, CNumber var5, CString.ByReference var6) {
    return centuraAPIimpl.SalDDERequest(var1,var2,var3,var4,var5,var6);
  }

  /**
   * LONG      CBEXPAPI  SalDDESend(HWND, UINT, HWND, UINT, UINT); file: centura200.h */
  public static CNumber SalDDESend(CWndHandle var1, CNumber var2, CWndHandle var3,
      CNumber var4, CNumber var5) {
    return centuraAPIimpl.SalDDESend(var1,var2,var3,var4,var5);
  }

  /**
   * LONG      CBEXPAPI  SalDDESendAll(UINT, HWND, UINT, UINT); file: centura200.h */
  public static CNumber SalDDESendAll(CNumber var1, CWndHandle var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalDDESendAll(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalDDESendExecute(HWND, LPSTR, LPSTR, LPSTR, LONG, LPSTR); file: centura200.h */
  public static CBoolean SalDDESendExecute(CWndHandle var1, CString var2, CString var3,
      CString var4, CNumber var5, CString var6) {
    return centuraAPIimpl.SalDDESendExecute(var1,var2,var3,var4,var5,var6);
  }

  /**
   * BOOL      CBEXPAPI  SalDDESendToClient(HWND, LPSTR, WPARAM, LONG); file: centura200.h */
  public static CBoolean SalDDESendToClient(CWndHandle var1, CString var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalDDESendToClient(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalDDESetCmd(HGLOBAL, LPSTR); file: centura200.h */
  public static CBoolean SalDDESetCmd(CHandle var1, CString var2) {
    return centuraAPIimpl.SalDDESetCmd(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalDDESetDataText(HGLOBAL, WORD, LPSTR); file: centura200.h */
  public static CBoolean SalDDESetDataText(CHandle var1, CNumber var2, CString var3) {
    return centuraAPIimpl.SalDDESetDataText(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalDDESetOptions(HGLOBAL, WORD, WORD); file: centura200.h */
  public static CBoolean SalDDESetOptions(CHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalDDESetOptions(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStartServer(HWND, LPSTR, LPSTR, LPSTR); file: centura200.h */
  public static CBoolean SalDDEStartServer(CWndHandle var1, CString var2, CString var3,
      CString var4) {
    return centuraAPIimpl.SalDDEStartServer(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStartSession(HWND, LPSTR, LPSTR, LPSTR, LONG); file: centura200.h */
  public static CBoolean SalDDEStartSession(CWndHandle var1, CString var2, CString var3,
      CString var4, CNumber var5) {
    return centuraAPIimpl.SalDDEStartSession(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStopServer(HWND); file: centura200.h */
  public static CBoolean SalDDEStopServer(CWndHandle var1) {
    return centuraAPIimpl.SalDDEStopServer(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStopSession(HWND); file: centura200.h */
  public static CBoolean SalDDEStopSession(CWndHandle var1) {
    return centuraAPIimpl.SalDDEStopSession(var1);
  }

  /**
   * DATETIME  CBEXPAPI  SalDateConstruct(INT, INT, INT, INT, INT, INT); file: centura200.h */
  public static  CDateTime SalDateConstruct(CNumber var1, CNumber var2, CNumber var3,
      CNumber var4, CNumber var5, CNumber var6) {
    return centuraAPIimpl.SalDateConstruct(var1,var2,var3,var4,var5,var6);
  }

  /**
   * DATETIME  CBEXPAPI  SalDateCurrent(VOID); file: centura200.h */
  public static  CDateTime SalDateCurrent() {
    return centuraAPIimpl.SalDateCurrent();
  }

  /**
   * INT       CBEXPAPI  SalDateDay(DATETIME); file: centura200.h */
  public static  CNumber SalDateDay(CDateTime var1) {
    return centuraAPIimpl.SalDateDay(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateHour(DATETIME); file: centura200.h */
  public static  CNumber SalDateHour(CDateTime var1) {
    return centuraAPIimpl.SalDateHour(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateMinute(DATETIME); file: centura200.h */
  public static  CNumber SalDateMinute(CDateTime var1) {
    return centuraAPIimpl.SalDateMinute(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateMonth(DATETIME); file: centura200.h */
  public static  CNumber SalDateMonth(CDateTime var1) {
    return centuraAPIimpl.SalDateMonth(var1);
  }

  /**
   * DATETIME  CBEXPAPI  SalDateMonthBegin(DATETIME); file: centura200.h */
  public static  CDateTime SalDateMonthBegin(CDateTime var1) {
    return centuraAPIimpl.SalDateMonthBegin(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateQuarter(DATETIME); file: centura200.h */
  public static  CNumber SalDateQuarter(CDateTime var1) {
    return centuraAPIimpl.SalDateQuarter(var1);
  }

  /**
   * DATETIME  CBEXPAPI  SalDateQuarterBegin(DATETIME); file: centura200.h */
  public static  CDateTime SalDateQuarterBegin(CDateTime var1) {
    return centuraAPIimpl.SalDateQuarterBegin(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateSecond(DATETIME); file: centura200.h */
  public static  CNumber SalDateSecond(CDateTime var1) {
    return centuraAPIimpl.SalDateSecond(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateToStr(DATETIME, LPHSTRING); file: centura200.h */
  public static  CNumber SalDateToStr(CDateTime var1, CString.ByReference var2) {
    return centuraAPIimpl.SalDateToStr(var1,var2);
  }

  /**
   * DATETIME  CBEXPAPI  SalDateWeekBegin(DATETIME); file: centura200.h */
  public static  CDateTime SalDateWeekBegin(CDateTime var1) {
    return centuraAPIimpl.SalDateWeekBegin(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateWeekday(DATETIME); file: centura200.h */
  public static  CNumber SalDateWeekday(CDateTime var1) {
    return centuraAPIimpl.SalDateWeekday(var1);
  }

  /**
   * INT       CBEXPAPI  SalDateYear(DATETIME); file: centura200.h */
  public static  CNumber SalDateYear(CDateTime var1) {
    return centuraAPIimpl.SalDateYear(var1);
  }

  /**
   * DATETIME  CBEXPAPI  SalDateYearBegin(DATETIME); file: centura200.h */
  public static  CDateTime SalDateYearBegin(CDateTime var1) {
    return centuraAPIimpl.SalDateYearBegin(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDestroyWindow(HWND); file: centura200.h */
  public static  CBoolean SalDestroyWindow(CWndHandle var1) {
    return centuraAPIimpl.SalDestroyWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDisableWindow(HWND); file: centura200.h */
  public static  CBoolean SalDisableWindow(CWndHandle var1) {
    return centuraAPIimpl.SalDisableWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDisableWindowAndLabel(HWND); file: centura200.h */
  public static  CBoolean SalDisableWindowAndLabel(CWndHandle var1) {
    return centuraAPIimpl.SalDisableWindowAndLabel(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDlgChooseColor(HWND, LPDWORD); file: centura200.h */
  public static  CBoolean SalDlgChooseColor(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalDlgChooseColor(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalDlgChooseFont(HWND, LPHSTRING, LPINT, LPWORD, LPDWORD); file: centura200.h */
  public static  CBoolean SalDlgChooseFont(CWndHandle var1, CString.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CNumber var5) {
    return centuraAPIimpl.SalDlgChooseFont(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalDlgOpenFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING); file: centura200.h */
  public static  CBoolean SalDlgOpenFile(CWndHandle var1, CString var2, CArray<?> var3,
      CNumber var4, CNumber.ByReference var5, CString.ByReference var6, CString.ByReference var7) {
    return centuraAPIimpl.SalDlgOpenFile(var1,var2,var3,var4,var5,var6,var7);
  }

  /**
   * BOOL      CBEXPAPI  SalDlgSaveFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING); file: centura200.h */
  public static  CBoolean SalDlgSaveFile(CWndHandle var1, CString var2, CArray<?> var3,
      CNumber var4, CNumber.ByReference var5, CString.ByReference var6, CString.ByReference var7) {
    return centuraAPIimpl.SalDlgSaveFile(var1,var2,var3,var4,var5,var6,var7);
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropDisableDrop(VOID); file: centura200.h */
  public static  CBoolean SalDragDropDisableDrop() {
    return centuraAPIimpl.SalDragDropDisableDrop();
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropEnableDrop(VOID); file: centura200.h */
  public static  CBoolean SalDragDropEnableDrop() {
    return centuraAPIimpl.SalDragDropEnableDrop();
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropGetSource(LPHWND, LPINT, LPINT); file: centura200.h */
  public static  CBoolean SalDragDropGetSource(CWndHandle.ByReference var1,
      CNumber.ByReference var2, CNumber.ByReference var3) {
    return centuraAPIimpl.SalDragDropGetSource(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropGetTarget(LPHWND, LPINT, LPINT); file: centura200.h */
  public static  CBoolean SalDragDropGetTarget(CWndHandle.ByReference var1,
      CNumber.ByReference var2, CNumber.ByReference var3) {
    return centuraAPIimpl.SalDragDropGetTarget(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropStart(HWND); file: centura200.h */
  public static  CBoolean SalDragDropStart(CWndHandle var1) {
    return centuraAPIimpl.SalDragDropStart(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropStop(VOID); file: centura200.h */
  public static  CBoolean SalDragDropStop() {
    return centuraAPIimpl.SalDragDropStop();
  }

  /**
   * BOOL      CBEXPAPI  SalDrawMenuBar(HWND); file: centura200.h */
  public static  CBoolean SalDrawMenuBar(CWndHandle var1) {
    return centuraAPIimpl.SalDrawMenuBar(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalDropFilesAcceptFiles(HWND, BOOL); file: centura200.h */
  public static  CBoolean SalDropFilesAcceptFiles(CWndHandle var1, CBoolean var2) {
    return centuraAPIimpl.SalDropFilesAcceptFiles(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalDropFilesQueryFiles(HWND, HARRAY); file: centura200.h */
  public static  CNumber SalDropFilesQueryFiles(CWndHandle var1, CArray<?> var2) {
    return centuraAPIimpl.SalDropFilesQueryFiles(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalDropFilesQueryPoint(HWND, LPINT, LPINT); file: centura200.h */
  public static  CBoolean SalDropFilesQueryPoint(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalDropFilesQueryPoint(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanCopyTo(VOID); file: centura200.h */
  public static  CBoolean SalEditCanCopyTo() {
    return centuraAPIimpl.SalEditCanCopyTo();
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanCut(VOID); file: centura200.h */
  public static  CBoolean SalEditCanCut() {
    return centuraAPIimpl.SalEditCanCut();
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanPaste(VOID); file: centura200.h */
  public static  CBoolean SalEditCanPaste() {
    return centuraAPIimpl.SalEditCanPaste();
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanPasteFrom(VOID); file: centura200.h */
  public static  CBoolean SalEditCanPasteFrom() {
    return centuraAPIimpl.SalEditCanPasteFrom();
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanUndo(VOID); file: centura200.h */
  public static  CBoolean SalEditCanUndo() {
    return centuraAPIimpl.SalEditCanUndo();
  }

  /**
   * BOOL      CBEXPAPI  SalEditClear(VOID); file: centura200.h */
  public static  CBoolean SalEditClear() {
    return centuraAPIimpl.SalEditClear();
  }

  /**
   * BOOL      CBEXPAPI  SalEditCopy(VOID); file: centura200.h */
  public static  CBoolean SalEditCopy() {
    return centuraAPIimpl.SalEditCopy();
  }

  /**
   * BOOL      CBEXPAPI  SalEditCopyString(LPSTR); file: centura200.h */
  public static  CBoolean SalEditCopyString(CString var1) {
    return centuraAPIimpl.SalEditCopyString(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalEditCopyTo(VOID); file: centura200.h */
  public static  CBoolean SalEditCopyTo() {
    return centuraAPIimpl.SalEditCopyTo();
  }

  /**
   * BOOL      CBEXPAPI  SalEditCut(VOID); file: centura200.h */
  public static  CBoolean SalEditCut() {
    return centuraAPIimpl.SalEditCut();
  }

  /**
   * BOOL      CBEXPAPI  SalEditPaste(VOID); file: centura200.h */
  public static  CBoolean SalEditPaste() {
    return centuraAPIimpl.SalEditPaste();
  }

  /**
   * BOOL      CBEXPAPI  SalEditPasteFrom(VOID); file: centura200.h */
  public static  CBoolean SalEditPasteFrom() {
    return centuraAPIimpl.SalEditPasteFrom();
  }

  /**
   * BOOL      CBEXPAPI  SalEditPasteString(LPHSTRING); file: centura200.h */
  public static  CBoolean SalEditPasteString(CString.ByReference var1) {
    return centuraAPIimpl.SalEditPasteString(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalEditUndo(VOID); file: centura200.h */
  public static  CBoolean SalEditUndo() {
    return centuraAPIimpl.SalEditUndo();
  }

  /**
   * BOOL      CBEXPAPI  SalEnableWindow(HWND); file: centura200.h */
  public static  CBoolean SalEnableWindow(CWndHandle var1) {
    return centuraAPIimpl.SalEnableWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalEnableWindowAndLabel(HWND); file: centura200.h */
  public static  CBoolean SalEnableWindowAndLabel(CWndHandle var1) {
    return centuraAPIimpl.SalEnableWindowAndLabel(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalEndDialog(HWND, INT); file: centura200.h */
  public static  CBoolean SalEndDialog(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalEndDialog(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFileClose(LPHFFILE); file: centura200.h */
  public static  CBoolean SalFileClose(CFile.ByReference var1) {
    return centuraAPIimpl.SalFileClose(var1);
  }

  /**
   * INT       CBEXPAPI  SalFileCopy(LPSTR, LPSTR, BOOL); file: centura200.h */
  public static  CNumber SalFileCopy(CString var1, CString var2, CBoolean var3) {
    return centuraAPIimpl.SalFileCopy(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalFileCreateDirectory(LPSTR); file: centura200.h */
  public static  CBoolean SalFileCreateDirectory(CString var1) {
    return centuraAPIimpl.SalFileCreateDirectory(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetC(HFFILE, LPWORD); file: centura200.h */
  public static  CBoolean SalFileGetC(CFile var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SalFileGetC(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalFileGetChar(HFFILE); file: centura200.h */
  public static  CNumber SalFileGetChar(CFile var1) {
    return centuraAPIimpl.SalFileGetChar(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetCurrentDirectory(LPHSTRING); file: centura200.h */
  public static  CBoolean SalFileGetCurrentDirectory(CString.ByReference var1) {
    return centuraAPIimpl.SalFileGetCurrentDirectory(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetDateTime(LPSTR, LPDATETIME); file: centura200.h */
  public static  CBoolean SalFileGetDateTime(CString var1, CDateTime.ByReference var2) {
    return centuraAPIimpl.SalFileGetDateTime(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SalFileGetDrive(VOID); file: centura200.h */
  public static  CString SalFileGetDrive() {
    return centuraAPIimpl.SalFileGetDrive();
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetStr(HFFILE, LPHSTRING, INT); file: centura200.h */
  public static  CBoolean SalFileGetStr(CFile var1, CString.ByReference var2, CNumber var3) {
    return centuraAPIimpl.SalFileGetStr(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalFileOpen(LPHFFILE, LPSTR, LONG); file: centura200.h */
  public static  CBoolean SalFileOpen(CFile.ByReference var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalFileOpen(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalFileOpenExt(LPHFFILE, LPSTR, LONG, LPHSTRING); file: centura200.h */
  public static  CBoolean SalFileOpenExt(CFile.ByReference var1, CString var2, CNumber var3,
      CString.ByReference var4) {
    return centuraAPIimpl.SalFileOpenExt(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalFilePutC(HFFILE, WORD); file: centura200.h */
  public static  CBoolean SalFilePutC(CFile var1, CNumber var2) {
    return centuraAPIimpl.SalFilePutC(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFilePutChar(HFFILE, INT); file: centura200.h */
  public static  CBoolean SalFilePutChar(CFile var1, CNumber var2) {
    return centuraAPIimpl.SalFilePutChar(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalFilePutStr(HFFILE, LPSTR); file: centura200.h */
  public static  CNumber SalFilePutStr(CFile var1, CString var2) {
    return centuraAPIimpl.SalFilePutStr(var1,var2);
  }

  /**
   * LONG      CBEXPAPI  SalFileRead(HFFILE, LPHSTRING, LONG); file: centura200.h */
  public static  CNumber SalFileRead(CFile var1, CString.ByReference var2, CNumber var3) {
    return centuraAPIimpl.SalFileRead(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalFileRemoveDirectory(LPSTR); file: centura200.h */
  public static  CBoolean SalFileRemoveDirectory(CString var1) {
    return centuraAPIimpl.SalFileRemoveDirectory(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFileSeek(HFFILE, LONG, INT); file: centura200.h */
  public static  CBoolean SalFileSeek(CFile var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalFileSeek(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalFileSetCurrentDirectory(LPSTR); file: centura200.h */
  public static  CBoolean SalFileSetCurrentDirectory(CString var1) {
    return centuraAPIimpl.SalFileSetCurrentDirectory(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFileSetDateTime(LPSTR, DATETIME); file: centura200.h */
  public static  CBoolean SalFileSetDateTime(CString var1, CDateTime var2) {
    return centuraAPIimpl.SalFileSetDateTime(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFileSetDrive(LPSTR); file: centura200.h */
  public static  CBoolean SalFileSetDrive(CString var1) {
    return centuraAPIimpl.SalFileSetDrive(var1);
  }

  /**
   * LONG      CBEXPAPI  SalFileTell(HFFILE); file: centura200.h */
  public static  CNumber SalFileTell(CFile var1) {
    return centuraAPIimpl.SalFileTell(var1);
  }

  /**
   * LONG      CBEXPAPI  SalFileWrite(HFFILE, HSTRING, LONG); file: centura200.h */
  public static  CNumber SalFileWrite(CFile var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalFileWrite(var1,var2,var3);
  }

  /**
   * HWND      CBEXPAPI  SalFindWindow(HWND, LPSTR); file: centura200.h */
  public static  CWndHandle SalFindWindow(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalFindWindow(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFireEvent(HITEM, ...); file: centura200.h */
  public static  CBoolean SalFireEvent(CHandle var1, CArray<?> var2) {
    return centuraAPIimpl.SalFireEvent(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtFieldToStr(HWND, LPHSTRING, BOOL); file: centura200.h */
  public static  CBoolean SalFmtFieldToStr(CWndHandle var1, CString.ByReference var2,
      CBoolean var3) {
    return centuraAPIimpl.SalFmtFieldToStr(var1,var2,var3);
  }

  /**
   * HSTRING   CBEXPAPI  SalFmtFormatDateTime(DATETIME, LPSTR); file: centura200.h */
  public static  CString SalFmtFormatDateTime(CDateTime var1, CString var2) {
    return centuraAPIimpl.SalFmtFormatDateTime(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SalFmtFormatNumber(NUMBER, LPSTR); file: centura200.h */
  public static  CString SalFmtFormatNumber(CNumber var1, CString var2) {
    return centuraAPIimpl.SalFmtFormatNumber(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalFmtGetFormat(HWND); file: centura200.h */
  public static  CNumber SalFmtGetFormat(CWndHandle var1) {
    return centuraAPIimpl.SalFmtGetFormat(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtGetInputMask(HWND, LPHSTRING); file: centura200.h */
  public static  CBoolean SalFmtGetInputMask(CWndHandle var1, CString.ByReference var2) {
    return centuraAPIimpl.SalFmtGetInputMask(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtGetPicture(HWND, LPHSTRING); file: centura200.h */
  public static  CBoolean SalFmtGetPicture(CWndHandle var1, CString.ByReference var2) {
    return centuraAPIimpl.SalFmtGetPicture(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidField(HWND); file: centura200.h */
  public static  CBoolean SalFmtIsValidField(CWndHandle var1) {
    return centuraAPIimpl.SalFmtIsValidField(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidInputMask(LPSTR); file: centura200.h */
  public static  CBoolean SalFmtIsValidInputMask(CString var1) {
    return centuraAPIimpl.SalFmtIsValidInputMask(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidPicture(LPSTR, INT); file: centura200.h */
  public static  CBoolean SalFmtIsValidPicture(CString var1, CNumber var2) {
    return centuraAPIimpl.SalFmtIsValidPicture(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtKeepMask(BOOL); file: centura200.h */
  public static  CBoolean SalFmtKeepMask(CBoolean var1) {
    return centuraAPIimpl.SalFmtKeepMask(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtSetFormat(HWND, INT); file: centura200.h */
  public static  CBoolean SalFmtSetFormat(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalFmtSetFormat(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtSetInputMask(HWND, LPSTR); file: centura200.h */
  public static  CBoolean SalFmtSetInputMask(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalFmtSetInputMask(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtSetPicture(HWND, LPSTR); file: centura200.h */
  public static  CBoolean SalFmtSetPicture(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalFmtSetPicture(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtStrToField(HWND, LPSTR, BOOL); file: centura200.h */
  public static  CBoolean SalFmtStrToField(CWndHandle var1, CString var2, CBoolean var3) {
    return centuraAPIimpl.SalFmtStrToField(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtUnmaskInput(HWND, LPHSTRING); file: centura200.h */
  public static  CBoolean SalFmtUnmaskInput(CWndHandle var1, CString.ByReference var2) {
    return centuraAPIimpl.SalFmtUnmaskInput(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFmtValidateField(HWND, INT); file: centura200.h */
  public static  CBoolean SalFmtValidateField(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalFmtValidateField(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalFontGet(HWND, LPHSTRING, LPINT, LPWORD); file: centura200.h */
  public static  CBoolean SalFontGet(CWndHandle var1, CString.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4) {
    return centuraAPIimpl.SalFontGet(var1,var2,var3,var4);
  }

  /**
   * INT       CBEXPAPI  SalFontGetNames(WORD, HARRAY); file: centura200.h */
  public static  CNumber SalFontGetNames(CNumber var1, CArray<?> var2) {
    return centuraAPIimpl.SalFontGetNames(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalFontGetSizes(WORD, LPSTR, HARRAY); file: centura200.h */
  public static  CNumber SalFontGetSizes(CNumber var1, CString var2, CArray<?> var3) {
    return centuraAPIimpl.SalFontGetSizes(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalFontSet(HWND, LPSTR, INT, WORD); file: centura200.h */
  public static  CBoolean SalFontSet(CWndHandle var1, CString var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalFontSet(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalFormGetParmNum(HWND, INT, LPNUMBER); file: centura200.h */
  public static  CBoolean SalFormGetParmNum(CWndHandle var1, CNumber var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalFormGetParmNum(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalFormUnitsToPixels(HWND, NUMBER, BOOL); file: centura200.h */
  public static  CNumber SalFormUnitsToPixels(CWndHandle var1, CNumber var2, CBoolean var3) {
    return centuraAPIimpl.SalFormUnitsToPixels(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalGetDataType(HWND); file: centura200.h */
  public static  CNumber SalGetDataType(CWndHandle var1) {
    return centuraAPIimpl.SalGetDataType(var1);
  }

  /**
   * HWND      CBEXPAPI  SalGetDefButton(HWND); file: centura200.h */
  public static  CWndHandle SalGetDefButton(CWndHandle var1) {
    return centuraAPIimpl.SalGetDefButton(var1);
  }

  /**
   * HWND      CBEXPAPI  SalGetFirstChild(HWND, LONG); file: centura200.h */
  public static  CWndHandle SalGetFirstChild(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalGetFirstChild(var1,var2);
  }

  /**
   * HWND      CBEXPAPI  SalGetFocus(VOID); file: centura200.h */
  public static  CWndHandle SalGetFocus() {
    return centuraAPIimpl.SalGetFocus();
  }

  /**
   * BOOL      CBEXPAPI  SalGetItemName(HWND, LPHSTRING); file: centura200.h */
  public static  CBoolean SalGetItemName(CWndHandle var1, CString.ByReference var2) {
    return centuraAPIimpl.SalGetItemName(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalGetMaxDataLength(HWND); file: centura200.h */
  public static  CNumber SalGetMaxDataLength(CWndHandle var1) {
    return centuraAPIimpl.SalGetMaxDataLength(var1);
  }

  /**
   * HWND      CBEXPAPI  SalGetNextChild(HWND, LONG); file: centura200.h */
  public static  CWndHandle SalGetNextChild(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalGetNextChild(var1,var2);
  }

  /**
   * UINT      CBEXPAPI  SalGetProfileInt(LPCSTR, LPCSTR, INT, LPCSTR); file: centura200.h */
  public static  CNumber SalGetProfileInt(CString var1, CString var2, CNumber var3,
      CString var4) {
    return centuraAPIimpl.SalGetProfileInt(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalGetProfileString(LPCSTR, LPCSTR, LPCSTR, LPHSTRING, LPCSTR); file: centura200.h */
  public static  CBoolean SalGetProfileString(CString var1, CString var2, CString var3,
      CString.ByReference var4, CString var5) {
    return centuraAPIimpl.SalGetProfileString(var1,var2,var3,var4,var5);
  }

  /**
   * LONG      CBEXPAPI  SalGetType(HWND); file: centura200.h */
  public static  CNumber SalGetType(CWndHandle var1) {
    return centuraAPIimpl.SalGetType(var1);
  }

  /**
   * LPSTR CBEXPAPI SalGetUDVData(HANDLE h); file: centura200.h */
  public static  CString SalGetUDVData(CHandle h) {
    return centuraAPIimpl.SalGetUDVData(h);
  }

  /**
   * WORD      CBEXPAPI  SalGetVersion(VOID); file: centura200.h */
  public static  CNumber SalGetVersion() {
    return centuraAPIimpl.SalGetVersion();
  }

  /**
   * INT       CBEXPAPI  SalGetWindowLabelText(HWND, LPHSTRING, INT); file: centura200.h */
  public static  CNumber SalGetWindowLabelText(CWndHandle var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SalGetWindowLabelText(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalGetWindowLoc(HWND, LPNUMBER, LPNUMBER); file: centura200.h */
  public static  CBoolean SalGetWindowLoc(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalGetWindowLoc(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalGetWindowSize(HWND, LPNUMBER, LPNUMBER); file: centura200.h */
  public static  CBoolean SalGetWindowSize(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalGetWindowSize(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalGetWindowState(HWND); file: centura200.h */
  public static  CNumber SalGetWindowState(CWndHandle var1) {
    return centuraAPIimpl.SalGetWindowState(var1);
  }

  /**
   * INT       CBEXPAPI  SalGetWindowText(HWND, LPHSTRING, INT); file: centura200.h */
  public static  CNumber SalGetWindowText(CWndHandle var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SalGetWindowText(var1,var2,var3);
  }

  /**
   * void CBEXPAPI SalHStringRef(HSTRING hString); file: centura200.h */
  public static  void SalHStringRef(CString hString) {
  }

  /**
   * DWORD     CBEXPAPI  SalHStringToNumber(HSTRING); file: centura200.h */
  public static  CNumber SalHStringToNumber(CString var1) {
    return centuraAPIimpl.SalHStringToNumber(var1);
  }

  /**
   * HSTRING CBEXPAPI SalHStringUnRef(HSTRING hString); file: centura200.h */
  public static  CString SalHStringUnRef(CString hString) {
    return centuraAPIimpl.SalHStringUnRef(hString);
  }

  /**
   * BOOL      CBEXPAPI  SalHideWindow(HWND); file: centura200.h */
  public static  CBoolean SalHideWindow(CWndHandle var1) {
    return centuraAPIimpl.SalHideWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalHideWindowAndLabel(HWND); file: centura200.h */
  public static  CBoolean SalHideWindowAndLabel(CWndHandle var1) {
    return centuraAPIimpl.SalHideWindowAndLabel(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIdleKick(VOID); file: centura200.h */
  public static  CBoolean SalIdleKick() {
    return centuraAPIimpl.SalIdleKick();
  }

  /**
   * BOOL      CBEXPAPI  SalIdleRegisterWindow(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  public static  CBoolean SalIdleRegisterWindow(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalIdleRegisterWindow(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalIdleUnregisterWindow(HWND); file: centura200.h */
  public static  CBoolean SalIdleUnregisterWindow(CWndHandle var1) {
    return centuraAPIimpl.SalIdleUnregisterWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalInvalidateWindow(HWND); file: centura200.h */
  public static  CBoolean SalInvalidateWindow(CWndHandle var1) {
    return centuraAPIimpl.SalInvalidateWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIsButtonChecked(HWND); file: centura200.h */
  public static  CBoolean SalIsButtonChecked(CWndHandle var1) {
    return centuraAPIimpl.SalIsButtonChecked(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIsNull(HWND); file: centura200.h */
  public static  CBoolean SalIsNull(CWndHandle var1) {
    return centuraAPIimpl.SalIsNull(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidDateTime(HWND); file: centura200.h */
  public static  CBoolean SalIsValidDateTime(CWndHandle var1) {
    return centuraAPIimpl.SalIsValidDateTime(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidDecimal(HWND, INT, INT); file: centura200.h */
  public static  CBoolean SalIsValidDecimal(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalIsValidDecimal(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidInteger(HWND); file: centura200.h */
  public static  CBoolean SalIsValidInteger(CWndHandle var1) {
    return centuraAPIimpl.SalIsValidInteger(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidNumber(HWND); file: centura200.h */
  public static  CBoolean SalIsValidNumber(CWndHandle var1) {
    return centuraAPIimpl.SalIsValidNumber(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIsWindowEnabled(HWND); file: centura200.h */
  public static  CBoolean SalIsWindowEnabled(CWndHandle var1) {
    return centuraAPIimpl.SalIsWindowEnabled(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalIsWindowVisible(HWND); file: centura200.h */
  public static  CBoolean SalIsWindowVisible(CWndHandle var1) {
    return centuraAPIimpl.SalIsWindowVisible(var1);
  }

  /**
   * INT       CBEXPAPI  SalListAdd(HWND, LPSTR); file: centura200.h */
  public static  CNumber SalListAdd(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalListAdd(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalListClear(HWND); file: centura200.h */
  public static  CBoolean SalListClear(CWndHandle var1) {
    return centuraAPIimpl.SalListClear(var1);
  }

  /**
   * INT       CBEXPAPI  SalListDelete(HWND, INT); file: centura200.h */
  public static  CNumber SalListDelete(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalListDelete(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalListFiles(HWND, HWND, LPHSTRING, WORD); file: centura200.h */
  public static  CBoolean SalListFiles(CWndHandle var1, CWndHandle var2,
      CString.ByReference var3, CNumber var4) {
    return centuraAPIimpl.SalListFiles(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalListGetMultiSelect(HWND, HARRAY); file: centura200.h */
  public static  CBoolean SalListGetMultiSelect(CWndHandle var1, CArray<?> var2) {
    return centuraAPIimpl.SalListGetMultiSelect(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalListInsert(HWND, INT, LPSTR); file: centura200.h */
  public static  CNumber SalListInsert(CWndHandle var1, CNumber var2, CString var3) {
    return centuraAPIimpl.SalListInsert(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalListPopulate(HWND, SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SalListPopulate(CWndHandle var1, CSQLHandle var2, CString var3) {
    return centuraAPIimpl.SalListPopulate(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalListQueryCount(HWND); file: centura200.h */
  public static  CNumber SalListQueryCount(CWndHandle var1) {
    return centuraAPIimpl.SalListQueryCount(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalListQueryFile(HWND, LPHSTRING); file: centura200.h */
  public static  CBoolean SalListQueryFile(CWndHandle var1, CString.ByReference var2) {
    return centuraAPIimpl.SalListQueryFile(var1,var2);
  }

  /**
   * NUMBER    CBEXPAPI  SalListQueryMultiCount(HWND); file: centura200.h */
  public static  CNumber SalListQueryMultiCount(CWndHandle var1) {
    return centuraAPIimpl.SalListQueryMultiCount(var1);
  }

  /**
   * INT       CBEXPAPI  SalListQuerySelection(HWND); file: centura200.h */
  public static  CNumber SalListQuerySelection(CWndHandle var1) {
    return centuraAPIimpl.SalListQuerySelection(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalListQueryState(HWND, INT); file: centura200.h */
  public static  CBoolean SalListQueryState(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalListQueryState(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalListQueryText(HWND, INT, LPHSTRING); file: centura200.h */
  public static  CNumber SalListQueryText(CWndHandle var1, CNumber var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SalListQueryText(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalListQueryTextLength(HWND, INT); file: centura200.h */
  public static  CNumber SalListQueryTextLength(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalListQueryTextLength(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SalListQueryTextX(HWND, INT); file: centura200.h */
  public static  CString SalListQueryTextX(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalListQueryTextX(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalListRedraw(HWND, BOOL); file: centura200.h */
  public static  CBoolean SalListRedraw(CWndHandle var1, CBoolean var2) {
    return centuraAPIimpl.SalListRedraw(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalListSelectString(HWND, INT, LPSTR); file: centura200.h */
  public static  CNumber SalListSelectString(CWndHandle var1, CNumber var2, CString var3) {
    return centuraAPIimpl.SalListSelectString(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalListSetMultiSelect(HWND, INT, BOOL); file: centura200.h */
  public static  CBoolean SalListSetMultiSelect(CWndHandle var1, CNumber var2, CBoolean var3) {
    return centuraAPIimpl.SalListSetMultiSelect(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalListSetSelect(HWND, INT); file: centura200.h */
  public static  CBoolean SalListSetSelect(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalListSetSelect(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalListSetTabs(HWND, HARRAY); file: centura200.h */
  public static  CBoolean SalListSetTabs(CWndHandle var1, CArray<?> var2) {
    return centuraAPIimpl.SalListSetTabs(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalLoadApp(LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SalLoadApp(CString var1, CString var2) {
    return centuraAPIimpl.SalLoadApp(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalLoadAppAndWait(LPSTR, WORD, LPDWORD); file: centura200.h */
  public static  CBoolean SalLoadAppAndWait(CString var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalLoadAppAndWait(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalMDIArrangeIcons(HWND); file: centura200.h */
  public static  CBoolean SalMDIArrangeIcons(CWndHandle var1) {
    return centuraAPIimpl.SalMDIArrangeIcons(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalMDICascade(HWND); file: centura200.h */
  public static  CBoolean SalMDICascade(CWndHandle var1) {
    return centuraAPIimpl.SalMDICascade(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalMDITile(HWND, BOOL); file: centura200.h */
  public static  CBoolean SalMDITile(CWndHandle var1, CBoolean var2) {
    return centuraAPIimpl.SalMDITile(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalMTSCreateInstance(HUDV); file: centura200.h */
  public static  CBoolean SalMTSCreateInstance(CHandle var1) {
    return centuraAPIimpl.SalMTSCreateInstance(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalMTSDisableCommit(VOID); file: centura200.h */
  public static  CBoolean SalMTSDisableCommit() {
    return centuraAPIimpl.SalMTSDisableCommit();
  }

  /**
   * BOOL      CBEXPAPI  SalMTSEnableCommit(VOID); file: centura200.h */
  public static  CBoolean SalMTSEnableCommit() {
    return centuraAPIimpl.SalMTSEnableCommit();
  }

  /**
   * BOOL      CBEXPAPI  SalMTSGetObjectContext(LPLONG); file: centura200.h */
  public static  CBoolean SalMTSGetObjectContext(CNumber.ByReference var1) {
    return centuraAPIimpl.SalMTSGetObjectContext(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalMTSIsCallerInRole(LPSTR, LPBOOL); file: centura200.h */
  public static  CBoolean SalMTSIsCallerInRole(CString var1, CBoolean var2) {
    return centuraAPIimpl.SalMTSIsCallerInRole(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalMTSIsInTransaction(LPBOOL); file: centura200.h */
  public static  CBoolean SalMTSIsInTransaction(CBoolean var1) {
    return centuraAPIimpl.SalMTSIsInTransaction(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalMTSIsSecurityEnabled(LPBOOL); file: centura200.h */
  public static  CBoolean SalMTSIsSecurityEnabled(CBoolean var1) {
    return centuraAPIimpl.SalMTSIsSecurityEnabled(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalMTSSetAbort(VOID); file: centura200.h */
  public static  CBoolean SalMTSSetAbort() {
    return centuraAPIimpl.SalMTSSetAbort();
  }

  /**
   * BOOL      CBEXPAPI  SalMTSSetComplete(VOID); file: centura200.h */
  public static  CBoolean SalMTSSetComplete() {
    return centuraAPIimpl.SalMTSSetComplete();
  }

  /**
   * BOOL      CBEXPAPI  SalMapEnterToTab(BOOL); file: centura200.h */
  public static  CBoolean SalMapEnterToTab(CBoolean var1) {
    return centuraAPIimpl.SalMapEnterToTab(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalMessageBeep(WPARAM); file: centura200.h */
  public static  CBoolean SalMessageBeep(CNumber var1) {
    return centuraAPIimpl.SalMessageBeep(var1);
  }

  /**
   * INT       CBEXPAPI  SalMessageBox(LPSTR, LPSTR, UINT); file: centura200.h */
  public static  CNumber SalMessageBox(CString var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalMessageBox(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalMoveWindow(HWND, NUMBER, NUMBER); file: centura200.h */
  public static  CBoolean SalMoveWindow(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalMoveWindow(var1,var2,var3);
  }

  /**
   * LPSTR CBEXPAPI SalNewUDVObject(LPSTR); file: centura200.h */
  public static  CString SalNewUDVObject(CString var1) {
    return centuraAPIimpl.SalNewUDVObject(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberAbs(NUMBER); file: centura200.h */
  public static  CNumber SalNumberAbs(CNumber var1) {
    return centuraAPIimpl.SalNumberAbs(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcCos(NUMBER); file: centura200.h */
  public static  CNumber SalNumberArcCos(CNumber var1) {
    return centuraAPIimpl.SalNumberArcCos(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcSin(NUMBER); file: centura200.h */
  public static  CNumber SalNumberArcSin(CNumber var1) {
    return centuraAPIimpl.SalNumberArcSin(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcTan(NUMBER); file: centura200.h */
  public static  CNumber SalNumberArcTan(CNumber var1) {
    return centuraAPIimpl.SalNumberArcTan(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcTan2(NUMBER, NUMBER); file: centura200.h */
  public static  CNumber SalNumberArcTan2(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalNumberArcTan2(var1,var2);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberCos(NUMBER); file: centura200.h */
  public static  CNumber SalNumberCos(CNumber var1) {
    return centuraAPIimpl.SalNumberCos(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberCosH(NUMBER); file: centura200.h */
  public static  CNumber SalNumberCosH(CNumber var1) {
    return centuraAPIimpl.SalNumberCosH(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberExponent(NUMBER); file: centura200.h */
  public static  CNumber SalNumberExponent(CNumber var1) {
    return centuraAPIimpl.SalNumberExponent(var1);
  }

  /**
   * WORD      CBEXPAPI  SalNumberHigh(DWORD); file: centura200.h */
  public static  CNumber SalNumberHigh(CNumber var1) {
    return centuraAPIimpl.SalNumberHigh(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberHypot(NUMBER, NUMBER); file: centura200.h */
  public static  CNumber SalNumberHypot(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalNumberHypot(var1,var2);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberLog(NUMBER); file: centura200.h */
  public static  CNumber SalNumberLog(CNumber var1) {
    return centuraAPIimpl.SalNumberLog(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberLogBase10(NUMBER); file: centura200.h */
  public static  CNumber SalNumberLogBase10(CNumber var1) {
    return centuraAPIimpl.SalNumberLogBase10(var1);
  }

  /**
   * WORD      CBEXPAPI  SalNumberLow(DWORD); file: centura200.h */
  public static  CNumber SalNumberLow(CNumber var1) {
    return centuraAPIimpl.SalNumberLow(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberMax(NUMBER, NUMBER); file: centura200.h */
  public static  CNumber SalNumberMax(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalNumberMax(var1,var2);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberMin(NUMBER, NUMBER); file: centura200.h */
  public static  CNumber SalNumberMin(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalNumberMin(var1,var2);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberMod(NUMBER, NUMBER); file: centura200.h */
  public static  CNumber SalNumberMod(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalNumberMod(var1,var2);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberPi(NUMBER); file: centura200.h */
  public static  CNumber SalNumberPi(CNumber var1) {
    return centuraAPIimpl.SalNumberPi(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberPower(NUMBER, NUMBER); file: centura200.h */
  public static  CNumber SalNumberPower(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalNumberPower(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalNumberRandInit(WORD); file: centura200.h */
  public static  CBoolean SalNumberRandInit(CNumber var1) {
    return centuraAPIimpl.SalNumberRandInit(var1);
  }

  /**
   * INT       CBEXPAPI  SalNumberRandom(VOID); file: centura200.h */
  public static  CNumber SalNumberRandom() {
    return centuraAPIimpl.SalNumberRandom();
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberRound(NUMBER); file: centura200.h */
  public static  CNumber SalNumberRound(CNumber var1) {
    return centuraAPIimpl.SalNumberRound(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberSin(NUMBER); file: centura200.h */
  public static  CNumber SalNumberSin(CNumber var1) {
    return centuraAPIimpl.SalNumberSin(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberSinH(NUMBER); file: centura200.h */
  public static  CNumber SalNumberSinH(CNumber var1) {
    return centuraAPIimpl.SalNumberSinH(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberSqrt(NUMBER); file: centura200.h */
  public static  CNumber SalNumberSqrt(CNumber var1) {
    return centuraAPIimpl.SalNumberSqrt(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberTan(NUMBER); file: centura200.h */
  public static  CNumber SalNumberTan(CNumber var1) {
    return centuraAPIimpl.SalNumberTan(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberTanH(NUMBER); file: centura200.h */
  public static  CNumber SalNumberTanH(CNumber var1) {
    return centuraAPIimpl.SalNumberTanH(var1);
  }

  /**
   * HSTRING   CBEXPAPI  SalNumberToChar(WORD); file: centura200.h */
  public static  CString SalNumberToChar(CNumber var1) {
    return centuraAPIimpl.SalNumberToChar(var1);
  }

  /**
   * HSTRING   CBEXPAPI  SalNumberToHString(DWORD); file: centura200.h */
  public static  CString SalNumberToHString(CNumber var1) {
    return centuraAPIimpl.SalNumberToHString(var1);
  }

  /**
   * INT       CBEXPAPI  SalNumberToStr(NUMBER, INT, LPHSTRING); file: centura200.h */
  public static  CNumber SalNumberToStr(CNumber var1, CNumber var2, CString.ByReference var3) {
    return centuraAPIimpl.SalNumberToStr(var1,var2,var3);
  }

  /**
   * HSTRING   CBEXPAPI  SalNumberToStrX(NUMBER, INT); file: centura200.h */
  public static  CString SalNumberToStrX(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalNumberToStrX(var1,var2);
  }

  /**
   * HWND      CBEXPAPI  SalNumberToWindowHandle(UINT); file: centura200.h */
  public static  CWndHandle SalNumberToWindowHandle(CNumber var1) {
    return centuraAPIimpl.SalNumberToWindowHandle(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberTruncate(NUMBER, INT, INT); file: centura200.h */
  public static  CNumber SalNumberTruncate(CNumber var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalNumberTruncate(var1,var2,var3);
  }

  /**
   * HUDV      CBEXPAPI  SalObjCreateFromString(LPSTR); file: centura200.h */
  public static  CHandle SalObjCreateFromString(CString var1) {
    return centuraAPIimpl.SalObjCreateFromString(var1);
  }

  /**
   * HSTRING   CBEXPAPI  SalObjGetType(HUDV); file: centura200.h */
  public static  CString SalObjGetType(CHandle var1) {
    return centuraAPIimpl.SalObjGetType(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalObjIsDerived(HUDV, LPSTR); file: centura200.h */
  public static  CBoolean SalObjIsDerived(CHandle var1, CString var2) {
    return centuraAPIimpl.SalObjIsDerived(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalObjIsNull(HUDV); file: centura200.h */
  public static  CBoolean SalObjIsNull(CHandle var1) {
    return centuraAPIimpl.SalObjIsNull(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalObjIsValidClassName(LPSTR); file: centura200.h */
  public static  CBoolean SalObjIsValidClassName(CString var1) {
    return centuraAPIimpl.SalObjIsValidClassName(var1);
  }

  /**
   * HWND      CBEXPAPI  SalParentWindow(HWND); file: centura200.h */
  public static  CWndHandle SalParentWindow(CWndHandle var1) {
    return centuraAPIimpl.SalParentWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalPicClear(HWND); file: centura200.h */
  public static  CBoolean SalPicClear(CWndHandle var1) {
    return centuraAPIimpl.SalPicClear(var1);
  }

  /**
   * INT       CBEXPAPI  SalPicGetDescription(HWND, LPHSTRING, INT); file: centura200.h */
  public static  CNumber SalPicGetDescription(CWndHandle var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SalPicGetDescription(var1,var2,var3);
  }

  /**
   * LONG      CBEXPAPI  SalPicGetImage(HWND, LPHSTRING, LPINT); file: centura200.h */
  public static  CNumber SalPicGetImage(CWndHandle var1, CString.ByReference var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalPicGetImage(var1,var2,var3);
  }

  /**
   * LONG      CBEXPAPI  SalPicGetString(HWND, INT, LPHSTRING); file: centura200.h */
  public static  CNumber SalPicGetString(CWndHandle var1, CNumber var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SalPicGetString(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalPicSet(HWND, HITEM, INT); file: centura200.h */
  public static  CBoolean SalPicSet(CWndHandle var1, CHandle var2, CNumber var3) {
    return centuraAPIimpl.SalPicSet(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetFile(HWND, LPSTR); file: centura200.h */
  public static  CBoolean SalPicSetFile(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalPicSetFile(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetFit(HWND, INT, INT, INT); file: centura200.h */
  public static  CBoolean SalPicSetFit(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalPicSetFit(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetHandle(HWND, INT, DWORD); file: centura200.h */
  public static  CBoolean SalPicSetHandle(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalPicSetHandle(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetImage(HWND, HSTRING, INT); file: centura200.h */
  public static  CBoolean SalPicSetImage(CWndHandle var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalPicSetImage(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetString(HWND, INT, HSTRING); file: centura200.h */
  public static  CBoolean SalPicSetString(CWndHandle var1, CNumber var2, CString var3) {
    return centuraAPIimpl.SalPicSetString(var1,var2,var3);
  }

  /**
   * NUMBER    CBEXPAPI  SalPixelsToFormUnits(HWND, INT, BOOL); file: centura200.h */
  public static  CNumber SalPixelsToFormUnits(CWndHandle var1, CNumber var2, CBoolean var3) {
    return centuraAPIimpl.SalPixelsToFormUnits(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalPostMsg(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  public static  CBoolean SalPostMsg(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalPostMsg(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalPrtExtractRect(LONG, LPINT, LPINT, LPINT, LPINT); file: centura200.h */
  public static  CBoolean SalPrtExtractRect(CNumber var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CNumber.ByReference var5) {
    return centuraAPIimpl.SalPrtExtractRect(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalPrtGetDefault(LPHSTRING, LPHSTRING, LPHSTRING); file: centura200.h */
  public static  CBoolean SalPrtGetDefault(CString.ByReference var1, CString.ByReference var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SalPrtGetDefault(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalPrtGetParmNum(INT, LPNUMBER); file: centura200.h */
  public static  CBoolean SalPrtGetParmNum(CNumber var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SalPrtGetParmNum(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalPrtPrintForm(HWND); file: centura200.h */
  public static  CBoolean SalPrtPrintForm(CWndHandle var1) {
    return centuraAPIimpl.SalPrtPrintForm(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetDefault(LPSTR, LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SalPrtSetDefault(CString var1, CString var2, CString var3) {
    return centuraAPIimpl.SalPrtSetDefault(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetParmDefaults(VOID); file: centura200.h */
  public static  CBoolean SalPrtSetParmDefaults() {
    return centuraAPIimpl.SalPrtSetParmDefaults();
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetParmNum(INT, NUMBER); file: centura200.h */
  public static  CBoolean SalPrtSetParmNum(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SalPrtSetParmNum(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetup(LPHSTRING, LPHSTRING, LPHSTRING, BOOL); file: centura200.h */
  public static  CBoolean SalPrtSetup(CString.ByReference var1, CString.ByReference var2,
      CString.ByReference var3, CBoolean var4) {
    return centuraAPIimpl.SalPrtSetup(var1,var2,var3,var4);
  }

  /**
   * DWORD     CBEXPAPI  SalQueryArrayBounds(HARRAY, LPLONG, LPLONG); file: centura200.h */
  public static  CNumber SalQueryArrayBounds(CArray<?> var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalQueryArrayBounds(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalQueryFieldEdit(HWND); file: centura200.h */
  public static  CBoolean SalQueryFieldEdit(CWndHandle var1) {
    return centuraAPIimpl.SalQueryFieldEdit(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalQuit(VOID); file: centura200.h */
  public static  CBoolean SalQuit() {
    return centuraAPIimpl.SalQuit();
  }

  /**
   * BOOL      CBEXPAPI  SalReportClose(HWND); file: centura200.h */
  public static  CBoolean SalReportClose(CWndHandle var1) {
    return centuraAPIimpl.SalReportClose(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalReportCmd(HWND, INT); file: centura200.h */
  public static  CBoolean SalReportCmd(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalReportCmd(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalReportCreate(LPSTR, LPSTR, LPSTR, BOOL, LPINT); file: centura200.h */
  public static  CBoolean SalReportCreate(CString var1, CString var2, CString var3,
      CBoolean var4, CNumber.ByReference var5) {
    return centuraAPIimpl.SalReportCreate(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalReportDlgOptions(HWND, LPSTR, LPSTR, LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SalReportDlgOptions(CWndHandle var1, CString var2, CString var3,
      CString var4, CString var5) {
    return centuraAPIimpl.SalReportDlgOptions(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetDateTimeVar(HWND, LPSTR, LPDATETIME); file: centura200.h */
  public static  CBoolean SalReportGetDateTimeVar(CWndHandle var1, CString var2,
      CDateTime.ByReference var3) {
    return centuraAPIimpl.SalReportGetDateTimeVar(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetNumberVar(HWND, LPSTR, LPNUMBER); file: centura200.h */
  public static  CBoolean SalReportGetNumberVar(CWndHandle var1, CString var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalReportGetNumberVar(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetObjectVar(HWND, LPSTR, LPHSTRING); file: centura200.h */
  public static  CBoolean SalReportGetObjectVar(CWndHandle var1, CString var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SalReportGetObjectVar(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetStringVar(HWND, LPSTR, LPHSTRING); file: centura200.h */
  public static  CBoolean SalReportGetStringVar(CWndHandle var1, CString var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SalReportGetStringVar(var1,var2,var3);
  }

  /**
   * HWND      CBEXPAPI  SalReportPrint(HWND, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, LPINT); file: centura200.h */
  public static  CWndHandle SalReportPrint(CWndHandle var1, CString var2, CString var3,
      CString var4, CNumber var5, CNumber var6, CNumber var7, CNumber var8,
      CNumber.ByReference var9) {
    return centuraAPIimpl.SalReportPrint(var1,var2,var3,var4,var5,var6,var7,var8,var9);
  }

  /**
   * HWND      CBEXPAPI  SalReportPrintToFile(HWND, LPSTR, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, BOOL, LPINT); file: centura200.h */
  public static  CWndHandle SalReportPrintToFile(CWndHandle var1, CString var2, CString var3,
      CString var4, CString var5, CNumber var6, CNumber var7, CNumber var8, CNumber var9,
      CBoolean var10, CNumber.ByReference var11) {
    return centuraAPIimpl.SalReportPrintToFile(var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11);
  }

  /**
   * BOOL      CBEXPAPI  SalReportReset(HWND); file: centura200.h */
  public static  CBoolean SalReportReset(CWndHandle var1) {
    return centuraAPIimpl.SalReportReset(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetDateTimeVar(HWND, LPSTR, DATETIME); file: centura200.h */
  public static  CBoolean SalReportSetDateTimeVar(CWndHandle var1, CString var2,
      CDateTime var3) {
    return centuraAPIimpl.SalReportSetDateTimeVar(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetNumberVar(HWND, LPSTR, NUMBER); file: centura200.h */
  public static  CBoolean SalReportSetNumberVar(CWndHandle var1, CString var2, CNumber var3) {
    return centuraAPIimpl.SalReportSetNumberVar(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetObjectVar(HWND, LPSTR, HSTRING); file: centura200.h */
  public static  CBoolean SalReportSetObjectVar(CWndHandle var1, CString var2, CString var3) {
    return centuraAPIimpl.SalReportSetObjectVar(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetStringVar(HWND, LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SalReportSetStringVar(CWndHandle var1, CString var2, CString var3) {
    return centuraAPIimpl.SalReportSetStringVar(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalReportTableCreate(LPSTR, HWND, LPINT); file: centura200.h */
  public static  CBoolean SalReportTableCreate(CString var1, CWndHandle var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalReportTableCreate(var1,var2,var3);
  }

  /**
   * HWND      CBEXPAPI  SalReportTablePrint(HWND, LPSTR, HARRAY, LPINT); file: centura200.h */
  public static  CWndHandle SalReportTablePrint(CWndHandle var1, CString var2, CArray<?> var3,
      CNumber.ByReference var4) {
    return centuraAPIimpl.SalReportTablePrint(var1,var2,var3,var4);
  }

  /**
   * HWND      CBEXPAPI  SalReportTableView(HWND, HWND, LPSTR, LPINT); file: centura200.h */
  public static  CWndHandle SalReportTableView(CWndHandle var1, CWndHandle var2, CString var3,
      CNumber.ByReference var4) {
    return centuraAPIimpl.SalReportTableView(var1,var2,var3,var4);
  }

  /**
   * HWND      CBEXPAPI  SalReportView(HWND, HWND, LPSTR, LPSTR, LPSTR, LPINT); file: centura200.h */
  public static  CWndHandle SalReportView(CWndHandle var1, CWndHandle var2, CString var3,
      CString var4, CString var5, CNumber.ByReference var6) {
    return centuraAPIimpl.SalReportView(var1,var2,var3,var4,var5,var6);
  }

  /**
   * BOOL      CBEXPAPI  SalScrollGetPos(HWND, LPINT); file: centura200.h */
  public static  CBoolean SalScrollGetPos(CWndHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SalScrollGetPos(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalScrollGetRange(HWND, LPINT, LPINT, LPINT, LPINT); file: centura200.h */
  public static  CBoolean SalScrollGetRange(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CNumber.ByReference var5) {
    return centuraAPIimpl.SalScrollGetRange(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalScrollSetPos(HWND, INT); file: centura200.h */
  public static  CBoolean SalScrollSetPos(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalScrollSetPos(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalScrollSetRange(HWND, INT, INT, INT, INT); file: centura200.h */
  public static  CBoolean SalScrollSetRange(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4, CNumber var5) {
    return centuraAPIimpl.SalScrollSetRange(var1,var2,var3,var4,var5);
  }

  /**
   * LONG      CBEXPAPI  SalSendClassMessage(UINT, WPARAM, LPARAM); file: centura200.h */
  public static  CNumber SalSendClassMessage(CNumber var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalSendClassMessage(var1,var2,var3);
  }

  /**
   * LONG      CBEXPAPI  SalSendClassMessageNamed(HITEM, UINT, WPARAM, LPARAM); file: centura200.h */
  public static  CNumber SalSendClassMessageNamed(CHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalSendClassMessageNamed(var1,var2,var3,var4);
  }

  /**
   * LONG      CBEXPAPI  SalSendMsg(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  public static  CNumber SalSendMsg(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalSendMsg(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalSendMsgToChildren(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  public static  CBoolean SalSendMsgToChildren(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalSendMsgToChildren(var1,var2,var3,var4);
  }

  /**
   * WORD      CBEXPAPI  SalSendValidateMsg(VOID); file: centura200.h */
  public static  CNumber SalSendValidateMsg() {
    return centuraAPIimpl.SalSendValidateMsg();
  }

  /**
   * BOOL      CBEXPAPI  SalSetArrayBounds(HARRAY, LONG, LONG); file: centura200.h */
  public static  CBoolean SalSetArrayBounds(CArray<?> var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalSetArrayBounds(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalSetDefButton(HWND); file: centura200.h */
  public static  CBoolean SalSetDefButton(CWndHandle var1) {
    return centuraAPIimpl.SalSetDefButton(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalSetErrorInfo(LONG, LPSTR, LPSTR, DWORD); file: centura200.h */
  public static  CBoolean SalSetErrorInfo(CNumber var1, CString var2, CString var3,
      CNumber var4) {
    return centuraAPIimpl.SalSetErrorInfo(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalSetFieldEdit(HWND, BOOL); file: centura200.h */
  public static  CBoolean SalSetFieldEdit(CWndHandle var1, CBoolean var2) {
    return centuraAPIimpl.SalSetFieldEdit(var1,var2);
  }

  /**
   * HWND      CBEXPAPI  SalSetFocus(HWND); file: centura200.h */
  public static  CWndHandle SalSetFocus(CWndHandle var1) {
    return centuraAPIimpl.SalSetFocus(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalSetMaxDataLength(HWND, LONG); file: centura200.h */
  public static  CBoolean SalSetMaxDataLength(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalSetMaxDataLength(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalSetProfileString(LPCSTR, LPCSTR, LPCSTR, LPCSTR); file: centura200.h */
  public static  CBoolean SalSetProfileString(CString var1, CString var2, CString var3,
      CString var4) {
    return centuraAPIimpl.SalSetProfileString(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowLabelText(HWND, LPSTR); file: centura200.h */
  public static  CBoolean SalSetWindowLabelText(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalSetWindowLabelText(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowLoc(HWND, NUMBER, NUMBER); file: centura200.h */
  public static  CBoolean SalSetWindowLoc(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalSetWindowLoc(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowSize(HWND, NUMBER, NUMBER); file: centura200.h */
  public static  CBoolean SalSetWindowSize(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalSetWindowSize(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowText(HWND, LPSTR); file: centura200.h */
  public static  CBoolean SalSetWindowText(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalSetWindowText(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalShowWindow(HWND); file: centura200.h */
  public static  CBoolean SalShowWindow(CWndHandle var1) {
    return centuraAPIimpl.SalShowWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalShowWindowAndLabel(HWND); file: centura200.h */
  public static  CBoolean SalShowWindowAndLabel(CWndHandle var1) {
    return centuraAPIimpl.SalShowWindowAndLabel(var1);
  }

  /**
   * INT       CBEXPAPI  SalStatusGetText(HWND, LPHSTRING, INT); file: centura200.h */
  public static  CNumber SalStatusGetText(CWndHandle var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SalStatusGetText(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalStatusSetText(HWND, LPSTR); file: centura200.h */
  public static  CBoolean SalStatusSetText(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalStatusSetText(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalStatusSetVisible(HWND, BOOL); file: centura200.h */
  public static  CBoolean SalStatusSetVisible(CWndHandle var1, CBoolean var2) {
    return centuraAPIimpl.SalStatusSetVisible(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalStrCompress(LPHSTRING); file: centura200.h */
  public static  CBoolean SalStrCompress(CString.ByReference var1) {
    return centuraAPIimpl.SalStrCompress(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalStrFirstC(LPHSTRING, LPWORD); file: centura200.h */
  public static  CBoolean SalStrFirstC(CString.ByReference var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SalStrFirstC(var1,var2);
  }

  /**
   * LONG      CBEXPAPI  SalStrGetBufferLength(HSTRING); file: centura200.h */
  public static  CNumber SalStrGetBufferLength(CString var1) {
    return centuraAPIimpl.SalStrGetBufferLength(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalStrIsValidCurrency(LPSTR, INT, INT); file: centura200.h */
  public static  CBoolean SalStrIsValidCurrency(CString var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalStrIsValidCurrency(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalStrIsValidDateTime(LPSTR); file: centura200.h */
  public static  CBoolean SalStrIsValidDateTime(CString var1) {
    return centuraAPIimpl.SalStrIsValidDateTime(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalStrIsValidNumber(LPSTR); file: centura200.h */
  public static  CBoolean SalStrIsValidNumber(CString var1) {
    return centuraAPIimpl.SalStrIsValidNumber(var1);
  }

  /**
   * LONG      CBEXPAPI  SalStrLeft(HSTRING, LONG, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrLeft(CString var1, CNumber var2, CString.ByReference var3) {
    return centuraAPIimpl.SalStrLeft(var1,var2,var3);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrLeftX(HSTRING, LONG); file: centura200.h */
  public static  CString SalStrLeftX(CString var1, CNumber var2) {
    return centuraAPIimpl.SalStrLeftX(var1,var2);
  }

  /**
   * LONG      CBEXPAPI  SalStrLength(LPSTR); file: centura200.h */
  public static  CNumber SalStrLength(CString var1) {
    return centuraAPIimpl.SalStrLength(var1);
  }

  /**
   * INT       CBEXPAPI  SalStrLop(LPHSTRING); file: centura200.h */
  public static  CNumber SalStrLop(CString.ByReference var1) {
    return centuraAPIimpl.SalStrLop(var1);
  }

  /**
   * LONG      CBEXPAPI  SalStrLower(HSTRING, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrLower(CString var1, CString.ByReference var2) {
    return centuraAPIimpl.SalStrLower(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrLowerX(HSTRING); file: centura200.h */
  public static  CString SalStrLowerX(CString var1) {
    return centuraAPIimpl.SalStrLowerX(var1);
  }

  /**
   * LONG      CBEXPAPI  SalStrMid(HSTRING, LONG, LONG, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrMid(CString var1, CNumber var2, CNumber var3,
      CString.ByReference var4) {
    return centuraAPIimpl.SalStrMid(var1,var2,var3,var4);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrMidX(HSTRING, LONG, LONG); file: centura200.h */
  public static  CString SalStrMidX(CString var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalStrMidX(var1,var2,var3);
  }

  /**
   * LONG      CBEXPAPI  SalStrProper(HSTRING, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrProper(CString var1, CString.ByReference var2) {
    return centuraAPIimpl.SalStrProper(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrProperX(HSTRING); file: centura200.h */
  public static  CString SalStrProperX(CString var1) {
    return centuraAPIimpl.SalStrProperX(var1);
  }

  /**
   * LONG      CBEXPAPI  SalStrRepeat(HSTRING, INT, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrRepeat(CString var1, CNumber var2, CString.ByReference var3) {
    return centuraAPIimpl.SalStrRepeat(var1,var2,var3);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrRepeatX(HSTRING, INT); file: centura200.h */
  public static  CString SalStrRepeatX(CString var1, CNumber var2) {
    return centuraAPIimpl.SalStrRepeatX(var1,var2);
  }

  /**
   * LONG      CBEXPAPI  SalStrReplace(HSTRING, INT, INT, HSTRING, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrReplace(CString var1, CNumber var2, CNumber var3, CString var4,
      CString.ByReference var5) {
    return centuraAPIimpl.SalStrReplace(var1,var2,var3,var4,var5);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrReplaceX(HSTRING, INT, INT, HSTRING); file: centura200.h */
  public static  CString SalStrReplaceX(CString var1, CNumber var2, CNumber var3,
      CString var4) {
    return centuraAPIimpl.SalStrReplaceX(var1,var2,var3,var4);
  }

  /**
   * LONG      CBEXPAPI  SalStrRight(HSTRING, LONG, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrRight(CString var1, CNumber var2, CString.ByReference var3) {
    return centuraAPIimpl.SalStrRight(var1,var2,var3);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrRightX(HSTRING, LONG); file: centura200.h */
  public static  CString SalStrRightX(CString var1, CNumber var2) {
    return centuraAPIimpl.SalStrRightX(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalStrScan(LPSTR, LPSTR); file: centura200.h */
  public static  CNumber SalStrScan(CString var1, CString var2) {
    return centuraAPIimpl.SalStrScan(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalStrSetBufferLength(LPHSTRING, LONG); file: centura200.h */
  public static  CBoolean SalStrSetBufferLength(CString.ByReference var1, CNumber var2) {
    return centuraAPIimpl.SalStrSetBufferLength(var1,var2);
  }

  /**
   * DATETIME  CBEXPAPI  SalStrToDate(LPSTR); file: centura200.h */
  public static  CDateTime SalStrToDate(CString var1) {
    return centuraAPIimpl.SalStrToDate(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalStrToNumber(LPSTR); file: centura200.h */
  public static  CNumber SalStrToNumber(CString var1) {
    return centuraAPIimpl.SalStrToNumber(var1);
  }

  /**
   * INT       CBEXPAPI  SalStrTokenize(LPSTR, LPSTR, LPSTR, HARRAY); file: centura200.h */
  public static  CNumber SalStrTokenize(CString var1, CString var2, CString var3,
      CArray<?> var4) {
    return centuraAPIimpl.SalStrTokenize(var1,var2,var3,var4);
  }

  /**
   * INT       CBEXPAPI  SalStrTrim(HSTRING, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrTrim(CString var1, CString.ByReference var2) {
    return centuraAPIimpl.SalStrTrim(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrTrimX(HSTRING); file: centura200.h */
  public static  CString SalStrTrimX(CString var1) {
    return centuraAPIimpl.SalStrTrimX(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalStrUncompress(LPHSTRING); file: centura200.h */
  public static  CBoolean SalStrUncompress(CString.ByReference var1) {
    return centuraAPIimpl.SalStrUncompress(var1);
  }

  /**
   * LONG      CBEXPAPI  SalStrUpper(HSTRING, LPHSTRING); file: centura200.h */
  public static  CNumber SalStrUpper(CString var1, CString.ByReference var2) {
    return centuraAPIimpl.SalStrUpper(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SalStrUpperX(HSTRING); file: centura200.h */
  public static  CString SalStrUpperX(CString var1) {
    return centuraAPIimpl.SalStrUpperX(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTBarSetVisible(HWND, BOOL); file: centura200.h */
  public static  CBoolean SalTBarSetVisible(CWndHandle var1, CBoolean var2) {
    return centuraAPIimpl.SalTBarSetVisible(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblAnyRows(HWND, WORD, WORD); file: centura200.h */
  public static  CBoolean SalTblAnyRows(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalTblAnyRows(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblClearSelection(HWND); file: centura200.h */
  public static  CBoolean SalTblClearSelection(CWndHandle var1) {
    return centuraAPIimpl.SalTblClearSelection(var1);
  }

  /**
   * NUMBER    CBEXPAPI  SalTblColumnAverage(HWND, WORD, WORD, WORD); file: centura200.h */
  public static  CNumber SalTblColumnAverage(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalTblColumnAverage(var1,var2,var3,var4);
  }

  /**
   * NUMBER    CBEXPAPI  SalTblColumnSum(HWND, WORD, WORD, WORD); file: centura200.h */
  public static  CNumber SalTblColumnSum(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4) {
    return centuraAPIimpl.SalTblColumnSum(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblCopyRows(HWND, WORD, WORD); file: centura200.h */
  public static  CBoolean SalTblCopyRows(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalTblCopyRows(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalTblCreateColumn(HWND, UINT, NUMBER, INT, LPSTR); file: centura200.h */
  public static  CNumber SalTblCreateColumn(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber var4, CString var5) {
    return centuraAPIimpl.SalTblCreateColumn(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineCheckBoxColumn(HWND, DWORD, LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SalTblDefineCheckBoxColumn(CWndHandle var1, CNumber var2,
      CString var3, CString var4) {
    return centuraAPIimpl.SalTblDefineCheckBoxColumn(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineDropDownListColumn(HWND, DWORD, INT); file: centura200.h */
  public static  CBoolean SalTblDefineDropDownListColumn(CWndHandle var1, CNumber var2,
      CNumber var3) {
    return centuraAPIimpl.SalTblDefineDropDownListColumn(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefinePopupEditColumn(HWND, DWORD, INT); file: centura200.h */
  public static  CBoolean SalTblDefinePopupEditColumn(CWndHandle var1, CNumber var2,
      CNumber var3) {
    return centuraAPIimpl.SalTblDefinePopupEditColumn(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineRowHeader(HWND, LPSTR, INT, WORD, HWND); file: centura200.h */
  public static  CBoolean SalTblDefineRowHeader(CWndHandle var1, CString var2, CNumber var3,
      CNumber var4, CWndHandle var5) {
    return centuraAPIimpl.SalTblDefineRowHeader(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineSplitWindow(HWND, INT, BOOL); file: centura200.h */
  public static  CBoolean SalTblDefineSplitWindow(CWndHandle var1, CNumber var2,
      CBoolean var3) {
    return centuraAPIimpl.SalTblDefineSplitWindow(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDeleteRow(HWND, LONG, WORD); file: centura200.h */
  public static  CBoolean SalTblDeleteRow(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalTblDeleteRow(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDeleteSelected(HWND, SQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SalTblDeleteSelected(CWndHandle var1, CSQLHandle var2) {
    return centuraAPIimpl.SalTblDeleteSelected(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDestroyColumns(HWND); file: centura200.h */
  public static  CBoolean SalTblDestroyColumns(CWndHandle var1) {
    return centuraAPIimpl.SalTblDestroyColumns(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDoDeletes(HWND, SQLHANDLENUMBER, WORD); file: centura200.h */
  public static  CBoolean SalTblDoDeletes(CWndHandle var1, CSQLHandle var2, CNumber var3) {
    return centuraAPIimpl.SalTblDoDeletes(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDoInserts(HWND, SQLHANDLENUMBER, BOOL); file: centura200.h */
  public static  CBoolean SalTblDoInserts(CWndHandle var1, CSQLHandle var2, CBoolean var3) {
    return centuraAPIimpl.SalTblDoInserts(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblDoUpdates(HWND, SQLHANDLENUMBER, BOOL); file: centura200.h */
  public static  CBoolean SalTblDoUpdates(CWndHandle var1, CSQLHandle var2, CBoolean var3) {
    return centuraAPIimpl.SalTblDoUpdates(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalTblFetchRow(HWND, LONG); file: centura200.h */
  public static  CNumber SalTblFetchRow(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblFetchRow(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblFindNextRow(HWND, LPLONG, WORD, WORD); file: centura200.h */
  public static  CBoolean SalTblFindNextRow(CWndHandle var1, CNumber.ByReference var2,
      CNumber var3, CNumber var4) {
    return centuraAPIimpl.SalTblFindNextRow(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblFindPrevRow(HWND, LPLONG, WORD, WORD); file: centura200.h */
  public static  CBoolean SalTblFindPrevRow(CWndHandle var1, CNumber.ByReference var2,
      CNumber var3, CNumber var4) {
    return centuraAPIimpl.SalTblFindPrevRow(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblGetColumnText(HWND, UINT, LPHSTRING); file: centura200.h */
  public static  CBoolean SalTblGetColumnText(CWndHandle var1, CNumber var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SalTblGetColumnText(var1,var2,var3);
  }

  /**
   * INT       CBEXPAPI  SalTblGetColumnTitle(HWND, LPHSTRING, INT); file: centura200.h */
  public static  CNumber SalTblGetColumnTitle(CWndHandle var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SalTblGetColumnTitle(var1,var2,var3);
  }

  /**
   * HWND      CBEXPAPI  SalTblGetColumnWindow(HWND, INT, WORD); file: centura200.h */
  public static  CWndHandle SalTblGetColumnWindow(CWndHandle var1, CNumber var2,
      CNumber var3) {
    return centuraAPIimpl.SalTblGetColumnWindow(var1,var2,var3);
  }

  /**
   * LONG      CBEXPAPI  SalTblInsertRow(HWND, LONG); file: centura200.h */
  public static  CNumber SalTblInsertRow(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblInsertRow(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblKillEdit(HWND); file: centura200.h */
  public static  CBoolean SalTblKillEdit(CWndHandle var1) {
    return centuraAPIimpl.SalTblKillEdit(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblKillFocus(HWND); file: centura200.h */
  public static  CBoolean SalTblKillFocus(CWndHandle var1) {
    return centuraAPIimpl.SalTblKillFocus(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblObjectsFromPoint(HWND, INT, INT, LPLONG, LPHWND, LPDWORD); file: centura200.h */
  public static  CBoolean SalTblObjectsFromPoint(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber.ByReference var4, CWndHandle.ByReference var5, CNumber var6) {
    return centuraAPIimpl.SalTblObjectsFromPoint(var1,var2,var3,var4,var5,var6);
  }

  /**
   * BOOL      CBEXPAPI  SalTblPasteRows(HWND); file: centura200.h */
  public static  CBoolean SalTblPasteRows(CWndHandle var1) {
    return centuraAPIimpl.SalTblPasteRows(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblPopulate(HWND, SQLHANDLENUMBER, LPSTR, INT); file: centura200.h */
  public static  CBoolean SalTblPopulate(CWndHandle var1, CSQLHandle var2, CString var3,
      CNumber var4) {
    return centuraAPIimpl.SalTblPopulate(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryCheckBoxColumn(HWND, LPDWORD, LPHSTRING, LPHSTRING); file: centura200.h */
  public static  CBoolean SalTblQueryCheckBoxColumn(CWndHandle var1, CNumber var2,
      CString.ByReference var3, CString.ByReference var4) {
    return centuraAPIimpl.SalTblQueryCheckBoxColumn(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnCellType(HWND, LPINT); file: centura200.h */
  public static  CBoolean SalTblQueryColumnCellType(CWndHandle var1,
      CNumber.ByReference var2) {
    return centuraAPIimpl.SalTblQueryColumnCellType(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnFlags(HWND, DWORD); file: centura200.h */
  public static  CBoolean SalTblQueryColumnFlags(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblQueryColumnFlags(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalTblQueryColumnID(HWND); file: centura200.h */
  public static  CNumber SalTblQueryColumnID(CWndHandle var1) {
    return centuraAPIimpl.SalTblQueryColumnID(var1);
  }

  /**
   * INT       CBEXPAPI  SalTblQueryColumnPos(HWND); file: centura200.h */
  public static  CNumber SalTblQueryColumnPos(CWndHandle var1) {
    return centuraAPIimpl.SalTblQueryColumnPos(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnWidth(HWND, LPNUMBER); file: centura200.h */
  public static  CBoolean SalTblQueryColumnWidth(CWndHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SalTblQueryColumnWidth(var1,var2);
  }

  /**
   * LONG      CBEXPAPI  SalTblQueryContext(HWND); file: centura200.h */
  public static  CNumber SalTblQueryContext(CWndHandle var1) {
    return centuraAPIimpl.SalTblQueryContext(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryDropDownListColumn(HWND, LPDWORD, LPINT); file: centura200.h */
  public static  CBoolean SalTblQueryDropDownListColumn(CWndHandle var1, CNumber var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalTblQueryDropDownListColumn(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryFocus(HWND, LPLONG, LPHWND); file: centura200.h */
  public static  CBoolean SalTblQueryFocus(CWndHandle var1, CNumber.ByReference var2,
      CWndHandle.ByReference var3) {
    return centuraAPIimpl.SalTblQueryFocus(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryLinesPerRow(HWND, LPINT); file: centura200.h */
  public static  CBoolean SalTblQueryLinesPerRow(CWndHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SalTblQueryLinesPerRow(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SalTblQueryLockedColumns(HWND); file: centura200.h */
  public static  CNumber SalTblQueryLockedColumns(CWndHandle var1) {
    return centuraAPIimpl.SalTblQueryLockedColumns(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryPopupEditColumn(HWND, LPDWORD, LPINT); file: centura200.h */
  public static  CBoolean SalTblQueryPopupEditColumn(CWndHandle var1, CNumber var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalTblQueryPopupEditColumn(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryRowFlags(HWND, LONG, WORD); file: centura200.h */
  public static  CBoolean SalTblQueryRowFlags(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalTblQueryRowFlags(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryRowHeader(HWND, LPHSTRING, INT, LPINT, LPWORD, LPHWND); file: centura200.h */
  public static  CBoolean SalTblQueryRowHeader(CWndHandle var1, CString.ByReference var2,
      CNumber var3, CNumber.ByReference var4, CNumber.ByReference var5,
      CWndHandle.ByReference var6) {
    return centuraAPIimpl.SalTblQueryRowHeader(var1,var2,var3,var4,var5,var6);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryScroll(HWND, LPLONG, LPLONG, LPLONG); file: centura200.h */
  public static  CBoolean SalTblQueryScroll(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4) {
    return centuraAPIimpl.SalTblQueryScroll(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQuerySplitWindow(HWND, LPINT, LPBOOL); file: centura200.h */
  public static  CBoolean SalTblQuerySplitWindow(CWndHandle var1, CNumber.ByReference var2,
      CBoolean var3) {
    return centuraAPIimpl.SalTblQuerySplitWindow(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryTableFlags(HWND, WORD); file: centura200.h */
  public static  CBoolean SalTblQueryTableFlags(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblQueryTableFlags(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryVisibleRange(HWND, LPLONG, LPLONG); file: centura200.h */
  public static  CBoolean SalTblQueryVisibleRange(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SalTblQueryVisibleRange(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblReset(HWND); file: centura200.h */
  public static  CBoolean SalTblReset(CWndHandle var1) {
    return centuraAPIimpl.SalTblReset(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalTblScroll(HWND, LONG, HWND, WORD); file: centura200.h */
  public static  CBoolean SalTblScroll(CWndHandle var1, CNumber var2, CWndHandle var3,
      CNumber var4) {
    return centuraAPIimpl.SalTblScroll(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetCellTextColor(HWND, DWORD, BOOL); file: centura200.h */
  public static  CBoolean SalTblSetCellTextColor(CWndHandle var1, CNumber var2,
      CBoolean var3) {
    return centuraAPIimpl.SalTblSetCellTextColor(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnFlags(HWND, DWORD, BOOL); file: centura200.h */
  public static  CBoolean SalTblSetColumnFlags(CWndHandle var1, CNumber var2, CBoolean var3) {
    return centuraAPIimpl.SalTblSetColumnFlags(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnPos(HWND, INT); file: centura200.h */
  public static  CBoolean SalTblSetColumnPos(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblSetColumnPos(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnText(HWND, UINT, LPSTR); file: centura200.h */
  public static  CBoolean SalTblSetColumnText(CWndHandle var1, CNumber var2, CString var3) {
    return centuraAPIimpl.SalTblSetColumnText(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnTitle(HWND, LPSTR); file: centura200.h */
  public static  CBoolean SalTblSetColumnTitle(CWndHandle var1, CString var2) {
    return centuraAPIimpl.SalTblSetColumnTitle(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnWidth(HWND, NUMBER); file: centura200.h */
  public static  CBoolean SalTblSetColumnWidth(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblSetColumnWidth(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetContext(HWND, LONG); file: centura200.h */
  public static  CBoolean SalTblSetContext(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblSetContext(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetFlagsAnyRows(HWND, WORD, BOOL, WORD, WORD); file: centura200.h */
  public static  CBoolean SalTblSetFlagsAnyRows(CWndHandle var1, CNumber var2, CBoolean var3,
      CNumber var4, CNumber var5) {
    return centuraAPIimpl.SalTblSetFlagsAnyRows(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetFocusCell(HWND, LONG, HWND, INT, INT); file: centura200.h */
  public static  CBoolean SalTblSetFocusCell(CWndHandle var1, CNumber var2, CWndHandle var3,
      CNumber var4, CNumber var5) {
    return centuraAPIimpl.SalTblSetFocusCell(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetFocusRow(HWND, LONG); file: centura200.h */
  public static  CBoolean SalTblSetFocusRow(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblSetFocusRow(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetLinesPerRow(HWND, INT); file: centura200.h */
  public static  CBoolean SalTblSetLinesPerRow(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblSetLinesPerRow(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetLockedColumns(HWND, INT); file: centura200.h */
  public static  CBoolean SalTblSetLockedColumns(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblSetLockedColumns(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetRange(HWND, LONG, LONG); file: centura200.h */
  public static  CBoolean SalTblSetRange(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalTblSetRange(var1,var2,var3);
  }

  /**
   * LONG      CBEXPAPI  SalTblSetRow(HWND, INT); file: centura200.h */
  public static  CNumber SalTblSetRow(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTblSetRow(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetRowFlags(HWND, LONG, WORD, BOOL); file: centura200.h */
  public static  CBoolean SalTblSetRowFlags(CWndHandle var1, CNumber var2, CNumber var3,
      CBoolean var4) {
    return centuraAPIimpl.SalTblSetRowFlags(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetTableFlags(HWND, WORD, BOOL); file: centura200.h */
  public static  CBoolean SalTblSetTableFlags(CWndHandle var1, CNumber var2, CBoolean var3) {
    return centuraAPIimpl.SalTblSetTableFlags(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTblSortRows(HWND, WORD, INT); file: centura200.h */
  public static  CBoolean SalTblSortRows(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalTblSortRows(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTimerKill(HWND, INT); file: centura200.h */
  public static  CBoolean SalTimerKill(CWndHandle var1, CNumber var2) {
    return centuraAPIimpl.SalTimerKill(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalTimerSet(HWND, INT, WORD); file: centura200.h */
  public static  CBoolean SalTimerSet(CWndHandle var1, CNumber var2, CNumber var3) {
    return centuraAPIimpl.SalTimerSet(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalTrackPopupMenu(HWND, LPSTR, WORD, INT, INT); file: centura200.h */
  public static  CBoolean SalTrackPopupMenu(CWndHandle var1, CString var2, CNumber var3,
      CNumber var4, CNumber var5) {
    return centuraAPIimpl.SalTrackPopupMenu(var1,var2,var3,var4,var5);
  }

  /**
   * HUDV CBEXPAPI SalUdvGetCurrentHandle(void); file: centura200.h */
  public static  CHandle SalUdvGetCurrentHandle() {
    return centuraAPIimpl.SalUdvGetCurrentHandle();
  }

  /**
   * BOOL      CBEXPAPI  SalUpdateWindow(HWND); file: centura200.h */
  public static  CBoolean SalUpdateWindow(CWndHandle var1) {
    return centuraAPIimpl.SalUpdateWindow(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalUseRegistry(BOOL, LPSTR); file: centura200.h */
  public static  CBoolean SalUseRegistry(CBoolean var1, CString var2) {
    return centuraAPIimpl.SalUseRegistry(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalValidateSet(HWND, BOOL, LONG); file: centura200.h */
  public static  CBoolean SalValidateSet(CWndHandle var1, CBoolean var2, CNumber var3) {
    return centuraAPIimpl.SalValidateSet(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SalWaitCursor(BOOL); file: centura200.h */
  public static  CBoolean SalWaitCursor(CBoolean var1) {
    return centuraAPIimpl.SalWaitCursor(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalWinHelp(HWND, LPSTR, WORD, DWORD, LPSTR); file: centura200.h */
  public static  CBoolean SalWinHelp(CWndHandle var1, CString var2, CNumber var3, CNumber var4,
      CString var5) {
    return centuraAPIimpl.SalWinHelp(var1,var2,var3,var4,var5);
  }

  /**
   * HSTRING   CBEXPAPI  SalWindowClassName(HWND); file: centura200.h */
  public static  CString SalWindowClassName(CWndHandle var1) {
    return centuraAPIimpl.SalWindowClassName(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalWindowGetProperty(HWND, LPSTR, LPHSTRING); file: centura200.h */
  public static  CBoolean SalWindowGetProperty(CWndHandle var1, CString var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SalWindowGetProperty(var1,var2,var3);
  }

  /**
   * UINT      CBEXPAPI  SalWindowHandleToNumber(HWND); file: centura200.h */
  public static  CNumber SalWindowHandleToNumber(CWndHandle var1) {
    return centuraAPIimpl.SalWindowHandleToNumber(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalWindowIsDerivedFromClass(HWND, TEMPLATE); file: centura200.h */
  public static  CBoolean SalWindowIsDerivedFromClass(CWndHandle var1, CStruct var2) {
    return centuraAPIimpl.SalWindowIsDerivedFromClass(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SalYieldEnable(BOOL); file: centura200.h */
  public static  CBoolean SalYieldEnable(CBoolean var1) {
    return centuraAPIimpl.SalYieldEnable(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalYieldQueryState(VOID); file: centura200.h */
  public static  CBoolean SalYieldQueryState() {
    return centuraAPIimpl.SalYieldQueryState();
  }

  /**
   * BOOL      CBEXPAPI  SalYieldStartMessages(HWND); file: centura200.h */
  public static  CBoolean SalYieldStartMessages(CWndHandle var1) {
    return centuraAPIimpl.SalYieldStartMessages(var1);
  }

  /**
   * BOOL      CBEXPAPI  SalYieldStopMessages(VOID); file: centura200.h */
  public static  CBoolean SalYieldStopMessages() {
    return centuraAPIimpl.SalYieldStopMessages();
  }

  /**
   * BOOL      CBEXPAPI  SqlClearImmediate(VOID); file: centura200.h */
  public static  CBoolean SqlClearImmediate() {
    return centuraAPIimpl.SqlClearImmediate();
  }

  /**
   * BOOL      CBEXPAPI  SqlClose(SQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlClose(CSQLHandle var1) {
    return centuraAPIimpl.SqlClose(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlCloseAllSPResultSets(SQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlCloseAllSPResultSets(CSQLHandle var1) {
    return centuraAPIimpl.SqlCloseAllSPResultSets(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlCommit(SQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlCommit(CSQLHandle var1) {
    return centuraAPIimpl.SqlCommit(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlCommitSession(SESSIONHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlCommitSession(CSQLHandle var1) {
    return centuraAPIimpl.SqlCommitSession(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlConnect(LPSQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlConnect(CSQLHandle.ByReference var1) {
    return centuraAPIimpl.SqlConnect(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlConnectTransaction(LPSQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlConnectTransaction(CSQLHandle.ByReference var1, CString var2) {
    return centuraAPIimpl.SqlConnectTransaction(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlCreateSession(LPSESSIONHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlCreateSession(CSQLHandle.ByReference var1, CString var2) {
    return centuraAPIimpl.SqlCreateSession(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlCreateStatement(SESSIONHANDLENUMBER, LPSQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlCreateStatement(CSQLHandle var1, CSQLHandle.ByReference var2) {
    return centuraAPIimpl.SqlCreateStatement(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlDirectoryByName(LPSTR, HARRAY); file: centura200.h */
  public static  CBoolean SqlDirectoryByName(CString var1, CArray<?> var2) {
    return centuraAPIimpl.SqlDirectoryByName(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlDisconnect(LPSQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlDisconnect(CSQLHandle.ByReference var1) {
    return centuraAPIimpl.SqlDisconnect(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlDropStoredCmd(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlDropStoredCmd(CSQLHandle var1, CString var2) {
    return centuraAPIimpl.SqlDropStoredCmd(var1,var2);
  }

  /**
   * INT       CBEXPAPI  SqlError(SQLHANDLENUMBER); file: centura200.h */
  public static  CNumber SqlError(CSQLHandle var1) {
    return centuraAPIimpl.SqlError(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlErrorText(INT, INT, LPHSTRING, INT, LPINT); file: centura200.h */
  public static  CBoolean SqlErrorText(CNumber var1, CNumber var2, CString.ByReference var3,
      CNumber var4, CNumber.ByReference var5) {
    return centuraAPIimpl.SqlErrorText(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SqlExecute(SQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlExecute(CSQLHandle var1) {
    return centuraAPIimpl.SqlExecute(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlExecutionPlan(SQLHANDLENUMBER, LPHSTRING, INT); file: centura200.h */
  public static  CBoolean SqlExecutionPlan(CSQLHandle var1, CString.ByReference var2,
      CNumber var3) {
    return centuraAPIimpl.SqlExecutionPlan(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlExists(LPSTR, LPINT); file: centura200.h */
  public static  CBoolean SqlExists(CString var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SqlExists(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlExtractArgs(WPARAM, LPARAM, LPSQLHANDLENUMBER, LPINT, LPINT); file: centura200.h */
  public static  CBoolean SqlExtractArgs(CNumber var1, CNumber var2,
      CSQLHandle.ByReference var3, CNumber.ByReference var4, CNumber.ByReference var5) {
    return centuraAPIimpl.SqlExtractArgs(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SqlFetchNext(SQLHANDLENUMBER, LPINT); file: centura200.h */
  public static  CBoolean SqlFetchNext(CSQLHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SqlFetchNext(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlFetchPrevious(SQLHANDLENUMBER, LPINT); file: centura200.h */
  public static  CBoolean SqlFetchPrevious(CSQLHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SqlFetchPrevious(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlFetchRow(SQLHANDLENUMBER, LONG, LPINT); file: centura200.h */
  public static  CBoolean SqlFetchRow(CSQLHandle var1, CNumber var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SqlFetchRow(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlFreeSession(LPSESSIONHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlFreeSession(CSQLHandle.ByReference var1) {
    return centuraAPIimpl.SqlFreeSession(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetCmdOrRowsetPtr(SQLHANDLENUMBER, BOOL, LPLONG); file: centura200.h */
  public static  CBoolean SqlGetCmdOrRowsetPtr(CSQLHandle var1, CBoolean var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SqlGetCmdOrRowsetPtr(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetDSOrSessionPtr(SESSIONHANDLENUMBER, BOOL, LPLONG); file: centura200.h */
  public static  CBoolean SqlGetDSOrSessionPtr(CSQLHandle var1, CBoolean var2,
      CNumber.ByReference var3) {
    return centuraAPIimpl.SqlGetDSOrSessionPtr(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetError(SQLHANDLENUMBER, LPLONG, LPHSTRING); file: centura200.h */
  public static  CBoolean SqlGetError(CSQLHandle var1, CNumber.ByReference var2,
      CString.ByReference var3) {
    return centuraAPIimpl.SqlGetError(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetErrorPosition(SQLHANDLENUMBER, LPINT); file: centura200.h */
  public static  CBoolean SqlGetErrorPosition(CSQLHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SqlGetErrorPosition(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetErrorText(INT, LPHSTRING); file: centura200.h */
  public static  CBoolean SqlGetErrorText(CNumber var1, CString.ByReference var2) {
    return centuraAPIimpl.SqlGetErrorText(var1,var2);
  }

  /**
   * HSTRING   CBEXPAPI  SqlGetErrorTextX(INT); file: centura200.h */
  public static  CString SqlGetErrorTextX(CNumber var1) {
    return centuraAPIimpl.SqlGetErrorTextX(var1);
  }

  /**
   * HSTRING   CBEXPAPI  SqlGetLastStatement(VOID); file: centura200.h */
  public static  CString SqlGetLastStatement() {
    return centuraAPIimpl.SqlGetLastStatement();
  }

  /**
   * BOOL      CBEXPAPI  SqlGetModifiedRows(SQLHANDLENUMBER, LPLONG); file: centura200.h */
  public static  CBoolean SqlGetModifiedRows(CSQLHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SqlGetModifiedRows(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetNextSPResultSet(SQLHANDLENUMBER, LPSTR, LPBOOL); file: centura200.h */
  public static  CBoolean SqlGetNextSPResultSet(CSQLHandle var1, CString var2, CBoolean var3) {
    return centuraAPIimpl.SqlGetNextSPResultSet(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetParameter(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING); file: centura200.h */
  public static  CBoolean SqlGetParameter(CSQLHandle var1, CNumber var2,
      CNumber.ByReference var3, CString.ByReference var4) {
    return centuraAPIimpl.SqlGetParameter(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetParameterAll(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING, BOOL); file: centura200.h */
  public static  CBoolean SqlGetParameterAll(CSQLHandle var1, CNumber var2,
      CNumber.ByReference var3, CString.ByReference var4, CBoolean var5) {
    return centuraAPIimpl.SqlGetParameterAll(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetResultSetCount(SQLHANDLENUMBER, LPLONG); file: centura200.h */
  public static  CBoolean SqlGetResultSetCount(CSQLHandle var1, CNumber.ByReference var2) {
    return centuraAPIimpl.SqlGetResultSetCount(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetRollbackFlag(SQLHANDLENUMBER, LPBOOL); file: centura200.h */
  public static  CBoolean SqlGetRollbackFlag(CSQLHandle var1, CBoolean var2) {
    return centuraAPIimpl.SqlGetRollbackFlag(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetSessionErrorInfo(SESSIONHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING); file: centura200.h */
  public static  CBoolean SqlGetSessionErrorInfo(CSQLHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CString.ByReference var4) {
    return centuraAPIimpl.SqlGetSessionErrorInfo(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetSessionHandle(SQLHANDLENUMBER, LPSESSIONHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlGetSessionHandle(CSQLHandle var1, CSQLHandle.ByReference var2) {
    return centuraAPIimpl.SqlGetSessionHandle(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetSessionParameter(SESSIONHANDLENUMBER, WORD, LPLONG, LPHSTRING); file: centura200.h */
  public static  CBoolean SqlGetSessionParameter(CSQLHandle var1, CNumber var2,
      CNumber.ByReference var3, CString.ByReference var4) {
    return centuraAPIimpl.SqlGetSessionParameter(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SqlGetStatementErrorInfo(SQLHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING); file: centura200.h */
  public static  CBoolean SqlGetStatementErrorInfo(CSQLHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CString.ByReference var4) {
    return centuraAPIimpl.SqlGetStatementErrorInfo(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SqlImmediate(LPSTR); file: centura200.h */
  public static  CBoolean SqlImmediate(CString var1) {
    return centuraAPIimpl.SqlImmediate(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlOpen(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlOpen(CSQLHandle var1, CString var2) {
    return centuraAPIimpl.SqlOpen(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLExecute(SQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlOraPLSQLExecute(CSQLHandle var1) {
    return centuraAPIimpl.SqlOraPLSQLExecute(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLPrepare(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlOraPLSQLPrepare(CSQLHandle var1, CString var2) {
    return centuraAPIimpl.SqlOraPLSQLPrepare(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLStringBindType(SQLHANDLENUMBER, LPSTR, INT); file: centura200.h */
  public static  CBoolean SqlOraPLSQLStringBindType(CSQLHandle var1, CString var2,
      CNumber var3) {
    return centuraAPIimpl.SqlOraPLSQLStringBindType(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlPLSQLCommand(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlPLSQLCommand(CSQLHandle var1, CString var2) {
    return centuraAPIimpl.SqlPLSQLCommand(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlPrepare(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlPrepare(CSQLHandle var1, CString var2) {
    return centuraAPIimpl.SqlPrepare(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlPrepareAndExecute(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlPrepareAndExecute(CSQLHandle var1, CString var2) {
    return centuraAPIimpl.SqlPrepareAndExecute(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlPrepareSP(SQLHANDLENUMBER, LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SqlPrepareSP(CSQLHandle var1, CString var2, CString var3) {
    return centuraAPIimpl.SqlPrepareSP(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlRetrieve(SQLHANDLENUMBER, LPSTR, LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SqlRetrieve(CSQLHandle var1, CString var2, CString var3,
      CString var4) {
    return centuraAPIimpl.SqlRetrieve(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SqlRollbackSession(SESSIONHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlRollbackSession(CSQLHandle var1) {
    return centuraAPIimpl.SqlRollbackSession(var1);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetInMessage(SQLHANDLENUMBER, INT); file: centura200.h */
  public static  CBoolean SqlSetInMessage(CSQLHandle var1, CNumber var2) {
    return centuraAPIimpl.SqlSetInMessage(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetIsolationLevel(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  public static  CBoolean SqlSetIsolationLevel(CSQLHandle var1, CString var2) {
    return centuraAPIimpl.SqlSetIsolationLevel(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetLockTimeout(SQLHANDLENUMBER, INT); file: centura200.h */
  public static  CBoolean SqlSetLockTimeout(CSQLHandle var1, CNumber var2) {
    return centuraAPIimpl.SqlSetLockTimeout(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetLongBindDatatype(INT, INT); file: centura200.h */
  public static  CBoolean SqlSetLongBindDatatype(CNumber var1, CNumber var2) {
    return centuraAPIimpl.SqlSetLongBindDatatype(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetOutMessage(SQLHANDLENUMBER, INT); file: centura200.h */
  public static  CBoolean SqlSetOutMessage(CSQLHandle var1, CNumber var2) {
    return centuraAPIimpl.SqlSetOutMessage(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetParameter(SQLHANDLENUMBER, WORD, LONG, HSTRING); file: centura200.h */
  public static  CBoolean SqlSetParameter(CSQLHandle var1, CNumber var2, CNumber var3,
      CString var4) {
    return centuraAPIimpl.SqlSetParameter(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetParameterAll(SQLHANDLENUMBER, WORD, LONG, HSTRING, BOOL); file: centura200.h */
  public static  CBoolean SqlSetParameterAll(CSQLHandle var1, CNumber var2, CNumber var3,
      CString var4, CBoolean var5) {
    return centuraAPIimpl.SqlSetParameterAll(var1,var2,var3,var4,var5);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetResultSet(SQLHANDLENUMBER, BOOL); file: centura200.h */
  public static  CBoolean SqlSetResultSet(CSQLHandle var1, CBoolean var2) {
    return centuraAPIimpl.SqlSetResultSet(var1,var2);
  }

  /**
   * BOOL      CBEXPAPI  SqlSetSessionParameter(SESSIONHANDLENUMBER, WORD, LONG, HSTRING); file: centura200.h */
  public static  CBoolean SqlSetSessionParameter(CSQLHandle var1, CNumber var2, CNumber var3,
      CString var4) {
    return centuraAPIimpl.SqlSetSessionParameter(var1,var2,var3,var4);
  }

  /**
   * BOOL      CBEXPAPI  SqlStore(SQLHANDLENUMBER, LPSTR, LPSTR); file: centura200.h */
  public static  CBoolean SqlStore(CSQLHandle var1, CString var2, CString var3) {
    return centuraAPIimpl.SqlStore(var1,var2,var3);
  }

  /**
   * BOOL      CBEXPAPI  SqlVarSetup(SQLHANDLENUMBER); file: centura200.h */
  public static  CBoolean SqlVarSetup(CSQLHandle var1) {
	    return centuraAPIimpl.SqlVarSetup(var1);
	  }
  public static  CBoolean SalPause(CNumber var1) {
		    return centuraAPIimpl.SalPause(var1);
	  }
}
