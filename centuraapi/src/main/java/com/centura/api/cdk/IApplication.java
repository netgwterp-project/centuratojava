package com.centura.api.cdk;

import com.centura.api.type.CHandle;
import com.centura.api.type.CNumber;
import com.centura.api.type.CString;

public interface IApplication extends CenturaAPI {

	public static CNumber Run(IApplication application) {
		application.start();
		return application.exit();
	}

	void addFormat(CFormat cFormat);

	default void addFormat(FormatType formatType, CString format) {
		addFormat(new CFormat(formatType, format));
	}

	CNumber exit();

	CHandle getAppHandle();

	CString getDescription();

	CFormat getFormat(CString string);

	CFormat[] getFormats();

	CFormat[] getFormats(FormatType formatType);

	CString getWindowsDefaults(String clazz, String key);

	void setDescription(CString string);

	void setWindowsDefaults(String clazz, String key, String val);

	void sqlError();

	CNumber start();

}
