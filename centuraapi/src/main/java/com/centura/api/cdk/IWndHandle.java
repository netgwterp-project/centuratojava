package com.centura.api.cdk;

import java.util.Set;
import java.util.function.Function;

import com.centura.api.type.CNumber;
import com.centura.api.type.CWndHandle;

public interface IWndHandle {

	CWndHandle getWndHandle();
	void setWndHandle(CWndHandle handle);
	
	default Set<IMessageHandle> getMessageHandles(){
		return HandleFactory.getMessageHandles(getWndHandle());
	}
	
	default void addMessageHandle(IMessageHandle handle){
		HandleFactory.addMessageHandle(getWndHandle(), handle);
		
	}
	default void addMessageHandle(IWndHandle wndHandle, IMessageHandle handle){
		HandleFactory.addMessageHandle(wndHandle, handle);
	}
	default void addMessageHandle(IWndHandle wndHandle, Integer MESSAGE, Function<IMessage,CNumber> function){
		addMessageHandle(wndHandle, new IMessageHandle() {
			@Override
			public Integer getMESSAGE() {
				return MESSAGE;
			}
			
			@Override
			public CNumber apply(IMessage msg) {
				return function.apply(msg);
			}
		});
	};
	default CNumber sendMessageClass(Integer MESSAGE, IMessage msg){
		System.out.println("Run Class Message: " + MESSAGE);
		IMessageHandle messageHandle = getMessageHandle(MESSAGE);
		return messageHandle != null ? sendMessage(messageHandle.getCLASSMESSAGE(), msg) : CNumber.NUMBER_Null;
	}
	
	default IMessageHandle getMessageHandle(Integer MESSAGE){
		return getMessageHandle(getWndHandle(), MESSAGE);
	}

	default IMessageHandle getMessageHandle(CWndHandle wndHandle, Integer MESSAGE){
		return HandleFactory.getMessageHandle(wndHandle,MESSAGE);
	}
	
	default CNumber postMessage(Integer MESSAGE, IMessage msg) {
		IMessageHandle iHandle = getMessageHandle(MESSAGE);
		return iHandle != null ? iHandle.apply(msg) : CNumber.NUMBER_Null;
	}
	
	default CNumber sendMessage(Integer MESSAGE, IMessage msg) {
		return sendMessage(getWndHandle(), MESSAGE, msg);
	}
	
	default CNumber sendMessage(IWndHandle wndHandle, Integer MESSAGE, IMessage msg) {
		return sendMessage(wndHandle.getWndHandle(), MESSAGE, msg);
	}

	default CNumber sendMessage(CWndHandle wndHandle, Integer MESSAGE, IMessage msg) {
		System.out.println("Run Message: " + MESSAGE);
		IMessageHandle iHandle = getMessageHandle(wndHandle, MESSAGE);
		return iHandle != null ? iHandle.apply(msg) : CNumber.NUMBER_Null;
	}

}
