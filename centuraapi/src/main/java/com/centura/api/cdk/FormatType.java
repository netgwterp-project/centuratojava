package com.centura.api.cdk;

import com.centura.api.type.CDateTime;
import com.centura.api.type.CNumber;
import com.centura.api.type.Reference;

public enum FormatType {
	NUMBER(CNumber.class), DATETIME(CDateTime.class);
	private Class<?> clazz;

	private FormatType(Class<? extends Reference<?>> clazz) {
		this.setClazz(clazz);
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}
}
