package com.centura.api.cdk;

import com.centura.api.type.CNumber;
import com.centura.api.type.CWndHandle;

public class IMessage {
       public CWndHandle hWndForm = CWndHandle.hWndNULL;
       public CWndHandle hWndItem = CWndHandle.hWndNULL;
       public CNumber wParam = CNumber.NUMBER_Null;
       public CNumber lParam = CNumber.NUMBER_Null;
}
