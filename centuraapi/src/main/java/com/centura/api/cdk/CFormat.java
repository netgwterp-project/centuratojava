package com.centura.api.cdk;

import com.centura.api.type.CString;

public final class CFormat {
	
	public CFormat(FormatType formatType, CString format) {
		this.setFormatType(formatType);
		this.setFormat(format);
	}
	
	public CFormat() {
	}
	
	public FormatType getFormatType() {
		return formatType;
	}

	public void setFormatType(FormatType formatType) {
		this.formatType = formatType;
	}

	public CString getFormat() {
		return format;
	}

	public void setFormat(CString format) {
		this.format = format;
	}

	private FormatType formatType;
	private CString format;
}
