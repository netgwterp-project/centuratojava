package com.centura.api.cdk;

import com.centura.api.type.CArray;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CFile;
import com.centura.api.type.CHandle;
import com.centura.api.type.CNumber;
import com.centura.api.type.CSQLHandle;
import com.centura.api.type.CString;
import com.centura.api.type.CStruct;
import com.centura.api.type.CWndHandle;

public interface ICenturaAPI {
  /**
   * HWND CBEXPAPI SWinAppFind(LPSTR lpModuleName, BOOL bActivate); file: centura200.h */
  CWndHandle SWinAppFind(CString lpModuleName, CBoolean bActivate);

  /**
   * BOOL CBEXPAPI  SWinCvtDoubleToNumber(double, LPNUMBER); file: centura200.h */
  CBoolean SWinCvtDoubleToNumber(CNumber var1, CNumber.ByReference var2);

  /**
   * BOOL CBEXPAPI  SWinCvtIntToNumber(INT, LPNUMBER); file: centura200.h */
  CBoolean SWinCvtIntToNumber(CNumber var1, CNumber.ByReference var2);

  /**
   * BOOL CBEXPAPI  SWinCvtLongToNumber(LONG, LPNUMBER); file: centura200.h */
  CBoolean SWinCvtLongToNumber(CNumber var1, CNumber.ByReference var2);

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToDouble(LPNUMBER, double FAR *); file: centura200.h */
  CBoolean SWinCvtNumberToDouble(CNumber.ByReference var1, CNumber var2);

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToInt(LPNUMBER, LPINT); file: centura200.h */
  CBoolean SWinCvtNumberToInt(CNumber.ByReference var1, CNumber.ByReference var2);

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToLong(LPNUMBER, LPLONG); file: centura200.h */
  CBoolean SWinCvtNumberToLong(CNumber.ByReference var1, CNumber.ByReference var2);

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToULong(LPNUMBER, LPDWORD); file: centura200.h */
  CBoolean SWinCvtNumberToULong(CNumber.ByReference var1, CNumber var2);

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToWord(LPNUMBER, LPWORD); file: centura200.h */
  CBoolean SWinCvtNumberToWord(CNumber.ByReference var1, CNumber.ByReference var2);

  /**
   * BOOL CBEXPAPI  SWinCvtULongToNumber(ULONG, LPNUMBER); file: centura200.h */
  CBoolean SWinCvtULongToNumber(CNumber var1, CNumber.ByReference var2);

  /**
   * BOOL CBEXPAPI  SWinCvtWordToNumber(WORD, LPNUMBER); file: centura200.h */
  CBoolean SWinCvtWordToNumber(CNumber var1, CNumber.ByReference var2);

  /**
   * HWND CBEXPAPI SWinFindWindow(HWND hWndContainer, LPSTR lpszSymbol); file: centura200.h */
  CWndHandle SWinFindWindow(CWndHandle hWndContainer, CString lpszSymbol);

  /**
   * int  CBEXPAPI SWinGetSymbol(HWND hWnd, LPSTR lpszSymbol, int nMaxLength); file: centura200.h */
  CNumber SWinGetSymbol(CWndHandle hWnd, CString lpszSymbol, CNumber nMaxLength);

  /**
   * LONG CBEXPAPI SWinGetType(HWND hWnd); file: centura200.h */
  CNumber SWinGetType(CWndHandle hWnd);

  /**
   * BOOL CBEXPAPI SWinHSCreateDesignHeap(VOID); file: centura200.h */
  CBoolean SWinHSCreateDesignHeap();

  /**
   * BOOL CBEXPAPI SWinHSDestroyDesignHeap(VOID); file: centura200.h */
  CBoolean SWinHSDestroyDesignHeap();

  /**
   * BOOL CBEXPAPI SWinInitLPHSTRINGParam(LPHSTRING, LONG); file: centura200.h */
  CBoolean SWinInitLPHSTRINGParam(CString.ByReference var1, CNumber var2);

  /**
   * BOOL CBEXPAPI SWinIsOurWindow(HWND hWnd); file: centura200.h */
  CBoolean SWinIsOurWindow(CWndHandle hWnd);

  /**
   * BOOL CBEXPAPI  SWinMDArrayDateType(HARRAY, LPINT); file: centura200.h */
  CBoolean SWinMDArrayDateType(CArray<?> var1, CNumber.ByReference var2);

  /**
   * LPSTR CBEXPAPI SWinStringGetBuffer(HSTRING, LPLONG); file: centura200.h */
  CString SWinStringGetBuffer(CString var1, CNumber.ByReference var2);

  /**
   * LPSTR   CBEXPAPI SWinUdvDeref( HUDV ); file: centura200.h */
  CString SWinUdvDeref(CHandle var1);

  /**
   * BOOL      CBEXPAPI  SalAbort(INT); file: centura200.h */
  CBoolean SalAbort(CNumber var1);

  /**
   * VOID      CBEXPAPI  SalActiveXAutoErrorMode(BOOL); file: centura200.h */
  void SalActiveXAutoErrorMode(CBoolean var1);

  /**
   * BOOL      CBEXPAPI  SalActiveXClose(HWND, BOOL); file: centura200.h */
  CBoolean SalActiveXClose(CWndHandle var1, CBoolean var2);

  /**
   * BOOL      CBEXPAPI  SalActiveXCreate(HWND, LPCSTR); file: centura200.h */
  CBoolean SalActiveXCreate(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalActiveXCreateFromData(HWND, HSTRING); file: centura200.h */
  CBoolean SalActiveXCreateFromData(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalActiveXCreateFromFile(HWND, LPCSTR); file: centura200.h */
  CBoolean SalActiveXCreateFromFile(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalActiveXDelete(HWND); file: centura200.h */
  CBoolean SalActiveXDelete(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalActiveXDoVerb(HWND, LONG, BOOL); file: centura200.h */
  CBoolean SalActiveXDoVerb(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalActiveXGetActiveObject(HUDV, LPSTR); file: centura200.h */
  CBoolean SalActiveXGetActiveObject(CHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalActiveXGetData(HWND, LPHSTRING); file: centura200.h */
  CBoolean SalActiveXGetData(CWndHandle var1, CString.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalActiveXGetObject(HWND, HUDV); file: centura200.h */
  CBoolean SalActiveXGetObject(CWndHandle var1, CHandle var2);

  /**
   * LONG      CBEXPAPI  SalActiveXInsertObjectDlg(HWND); file: centura200.h */
  CNumber SalActiveXInsertObjectDlg(CWndHandle var1);

  /**
   * LONG      CBEXPAPI  SalActiveXOLEType(HWND); file: centura200.h */
  CNumber SalActiveXOLEType(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalAppDisable(VOID); file: centura200.h */
  CBoolean SalAppDisable();

  /**
   * BOOL      CBEXPAPI  SalAppEnable(VOID); file: centura200.h */
  CBoolean SalAppEnable();

  /**
   * HWND      CBEXPAPI  SalAppFind(LPSTR, BOOL); file: centura200.h */
  CWndHandle SalAppFind(CString var1, CBoolean var2);

  /**
   * NUMBER    CBEXPAPI  SalArrayAvg(HARRAY); file: centura200.h */
  CNumber SalArrayAvg(CArray<?> var1);

  /**
   * BOOL      CBEXPAPI  SalArrayDimCount(HARRAY, LPLONG); file: centura200.h */
  CBoolean SalArrayDimCount(CArray<?> var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalArrayGetLowerBound(HARRAY, INT, LPLONG); file: centura200.h */
  CBoolean SalArrayGetLowerBound(CArray<?> var1, CNumber var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalArrayGetUpperBound(HARRAY, INT, LPLONG); file: centura200.h */
  CBoolean SalArrayGetUpperBound(CArray<?> var1, CNumber var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalArrayIsEmpty(HARRAY); file: centura200.h */
  CBoolean SalArrayIsEmpty(CArray<?> var1);

  /**
   * NUMBER    CBEXPAPI  SalArrayMax(HARRAY); file: centura200.h */
  CNumber SalArrayMax(CArray<?> var1);

  /**
   * NUMBER    CBEXPAPI  SalArrayMin(HARRAY); file: centura200.h */
  CNumber SalArrayMin(CArray<?> var1);

  /**
   * BOOL      CBEXPAPI  SalArraySetUpperBound(HARRAY, INT, LONG); file: centura200.h */
  CBoolean SalArraySetUpperBound(CArray<?> var1, CNumber var2, CNumber var3);

  /**
   * NUMBER    CBEXPAPI  SalArraySum(HARRAY); file: centura200.h */
  CNumber SalArraySum(CArray<?> var1);

  /**
   * VOID  CBEXPAPI SalAssignUDV(LPHANDLE A, LPHANDLE B, WORD fgs); file: centura200.h */
  void SalAssignUDV(CHandle.ByReference A, CHandle.ByReference B, CNumber fgs);

  /**
   * BOOL      CBEXPAPI  SalBringWindowToTop(HWND); file: centura200.h */
  CBoolean SalBringWindowToTop(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalCenterWindow(HWND); file: centura200.h */
  CBoolean SalCenterWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalClearField(HWND); file: centura200.h */
  CBoolean SalClearField(CWndHandle var1);

  /**
   * DWORD     CBEXPAPI  SalColorFromRGB(BYTE, BYTE, BYTE); file: centura200.h */
  CNumber SalColorFromRGB(CNumber var1, CNumber var2, CNumber var3);

  /**
   * DWORD     CBEXPAPI  SalColorGet(HWND, INT); file: centura200.h */
  CNumber SalColorGet(CWndHandle var1, CNumber var2);

  /**
   * DWORD     CBEXPAPI  SalColorGetSysColor(INT); file: centura200.h */
  CNumber SalColorGetSysColor(CNumber var1);

  /**
   * BOOL      CBEXPAPI  SalColorSet(HWND, INT, DWORD); file: centura200.h */
  CBoolean SalColorSet(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalColorToRGB(DWORD, LPBYTE, LPBYTE, LPBYTE); file: centura200.h */
  CBoolean SalColorToRGB(CNumber var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * INT       CBEXPAPI  SalCompileAndEvaluate(LPSTR, LPINT, LPINT, LPNUMBER, LPHSTRING, LPDATETIME, LPHWND, BOOL, LPSTR); file: centura200.h */
  CNumber SalCompileAndEvaluate(CString var1, CNumber.ByReference var2, CNumber.ByReference var3,
      CNumber.ByReference var4, CString.ByReference var5, CDateTime.ByReference var6,
      CWndHandle.ByReference var7, CBoolean var8, CString var9);

  /**
   * HSTRING   CBEXPAPI  SalContextBreak(VOID); file: centura200.h */
  CString SalContextBreak();

  /**
   * HSTRING   CBEXPAPI  SalContextCurrent(VOID); file: centura200.h */
  CString SalContextCurrent();

  /**
   * BOOL      CBEXPAPI  SalContextMenuSetPopup(HWND, LPSTR, DWORD); file: centura200.h */
  CBoolean SalContextMenuSetPopup(CWndHandle var1, CString var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalCursorClear(HWND, WORD); file: centura200.h */
  CBoolean SalCursorClear(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalCursorSet(HWND, TEMPLATE, WORD); file: centura200.h */
  CBoolean SalCursorSet(CWndHandle var1, CStruct var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalCursorSetFile(HWND, LPSTR, WORD); file: centura200.h */
  CBoolean SalCursorSetFile(CWndHandle var1, CString var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalCursorSetString(HWND, HSTRING, WORD); file: centura200.h */
  CBoolean SalCursorSetString(CWndHandle var1, CString var2, CNumber var3);

  /**
   * ATOM      CBEXPAPI  SalDDEAddAtom(LPSTR); file: centura200.h */
  CNumber SalDDEAddAtom(CString var1);

  /**
   * HGLOBAL   CBEXPAPI  SalDDEAlloc(VOID); file: centura200.h */
  CHandle SalDDEAlloc();

  /**
   * ATOM      CBEXPAPI  SalDDEDeleteAtom(ATOM); file: centura200.h */
  CNumber SalDDEDeleteAtom(CNumber var1);

  /**
   * BOOL      CBEXPAPI  SalDDEExtract(WPARAM, LPARAM, LPHWND, LPDWORD, LPDWORD); file: centura200.h */
  CBoolean SalDDEExtract(CNumber var1, CNumber var2, CWndHandle.ByReference var3, CNumber var4,
      CNumber var5);

  /**
   * BOOL      CBEXPAPI  SalDDEExtractCmd(HGLOBAL, LPHSTRING, INT); file: centura200.h */
  CBoolean SalDDEExtractCmd(CHandle var1, CString.ByReference var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalDDEExtractDataText(HGLOBAL, LPWORD, LPHSTRING, INT); file: centura200.h */
  CBoolean SalDDEExtractDataText(CHandle var1, CNumber.ByReference var2, CString.ByReference var3,
      CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalDDEExtractOptions(HGLOBAL, LPWORD, LPWORD); file: centura200.h */
  CBoolean SalDDEExtractOptions(CHandle var1, CNumber.ByReference var2, CNumber.ByReference var3);

  /**
   * ATOM      CBEXPAPI  SalDDEFindAtom(LPSTR); file: centura200.h */
  CNumber SalDDEFindAtom(CString var1);

  /**
   * HGLOBAL   CBEXPAPI  SalDDEFree(HGLOBAL); file: centura200.h */
  CHandle SalDDEFree(CHandle var1);

  /**
   * UINT      CBEXPAPI  SalDDEGetAtomName(ATOM, LPHSTRING, INT); file: centura200.h */
  CNumber SalDDEGetAtomName(CNumber var1, CString.ByReference var2, CNumber var3);

  /**
   * HSTRING   CBEXPAPI  SalDDEGetExecuteString(LPARAM); file: centura200.h */
  CString SalDDEGetExecuteString(CNumber var1);

  /**
   * LONG      CBEXPAPI  SalDDEPost(HWND, UINT, HWND, UINT, UINT); file: centura200.h */
  CNumber SalDDEPost(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4, CNumber var5);

  /**
   * BOOL      CBEXPAPI  SalDDERequest(HWND, LPSTR, LPSTR, LPSTR, LONG, LPHSTRING); file: centura200.h */
  CBoolean SalDDERequest(CWndHandle var1, CString var2, CString var3, CString var4, CNumber var5,
      CString.ByReference var6);

  /**
   * LONG      CBEXPAPI  SalDDESend(HWND, UINT, HWND, UINT, UINT); file: centura200.h */
  CNumber SalDDESend(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4, CNumber var5);

  /**
   * LONG      CBEXPAPI  SalDDESendAll(UINT, HWND, UINT, UINT); file: centura200.h */
  CNumber SalDDESendAll(CNumber var1, CWndHandle var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalDDESendExecute(HWND, LPSTR, LPSTR, LPSTR, LONG, LPSTR); file: centura200.h */
  CBoolean SalDDESendExecute(CWndHandle var1, CString var2, CString var3, CString var4,
      CNumber var5, CString var6);

  /**
   * BOOL      CBEXPAPI  SalDDESendToClient(HWND, LPSTR, WPARAM, LONG); file: centura200.h */
  CBoolean SalDDESendToClient(CWndHandle var1, CString var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalDDESetCmd(HGLOBAL, LPSTR); file: centura200.h */
  CBoolean SalDDESetCmd(CHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalDDESetDataText(HGLOBAL, WORD, LPSTR); file: centura200.h */
  CBoolean SalDDESetDataText(CHandle var1, CNumber var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SalDDESetOptions(HGLOBAL, WORD, WORD); file: centura200.h */
  CBoolean SalDDESetOptions(CHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalDDEStartServer(HWND, LPSTR, LPSTR, LPSTR); file: centura200.h */
  CBoolean SalDDEStartServer(CWndHandle var1, CString var2, CString var3, CString var4);

  /**
   * BOOL      CBEXPAPI  SalDDEStartSession(HWND, LPSTR, LPSTR, LPSTR, LONG); file: centura200.h */
  CBoolean SalDDEStartSession(CWndHandle var1, CString var2, CString var3, CString var4,
      CNumber var5);

  /**
   * BOOL      CBEXPAPI  SalDDEStopServer(HWND); file: centura200.h */
  CBoolean SalDDEStopServer(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalDDEStopSession(HWND); file: centura200.h */
  CBoolean SalDDEStopSession(CWndHandle var1);

  /**
   * DATETIME  CBEXPAPI  SalDateConstruct(INT, INT, INT, INT, INT, INT); file: centura200.h */
  CDateTime SalDateConstruct(CNumber var1, CNumber var2, CNumber var3, CNumber var4, CNumber var5,
      CNumber var6);

  /**
   * DATETIME  CBEXPAPI  SalDateCurrent(VOID); file: centura200.h */
  CDateTime SalDateCurrent();

  /**
   * INT       CBEXPAPI  SalDateDay(DATETIME); file: centura200.h */
  CNumber SalDateDay(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateHour(DATETIME); file: centura200.h */
  CNumber SalDateHour(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateMinute(DATETIME); file: centura200.h */
  CNumber SalDateMinute(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateMonth(DATETIME); file: centura200.h */
  CNumber SalDateMonth(CDateTime var1);

  /**
   * DATETIME  CBEXPAPI  SalDateMonthBegin(DATETIME); file: centura200.h */
  CDateTime SalDateMonthBegin(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateQuarter(DATETIME); file: centura200.h */
  CNumber SalDateQuarter(CDateTime var1);

  /**
   * DATETIME  CBEXPAPI  SalDateQuarterBegin(DATETIME); file: centura200.h */
  CDateTime SalDateQuarterBegin(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateSecond(DATETIME); file: centura200.h */
  CNumber SalDateSecond(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateToStr(DATETIME, LPHSTRING); file: centura200.h */
  CNumber SalDateToStr(CDateTime var1, CString.ByReference var2);

  /**
   * DATETIME  CBEXPAPI  SalDateWeekBegin(DATETIME); file: centura200.h */
  CDateTime SalDateWeekBegin(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateWeekday(DATETIME); file: centura200.h */
  CNumber SalDateWeekday(CDateTime var1);

  /**
   * INT       CBEXPAPI  SalDateYear(DATETIME); file: centura200.h */
  CNumber SalDateYear(CDateTime var1);

  /**
   * DATETIME  CBEXPAPI  SalDateYearBegin(DATETIME); file: centura200.h */
  CDateTime SalDateYearBegin(CDateTime var1);

  /**
   * BOOL      CBEXPAPI  SalDestroyWindow(HWND); file: centura200.h */
  CBoolean SalDestroyWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalDisableWindow(HWND); file: centura200.h */
  CBoolean SalDisableWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalDisableWindowAndLabel(HWND); file: centura200.h */
  CBoolean SalDisableWindowAndLabel(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalDlgChooseColor(HWND, LPDWORD); file: centura200.h */
  CBoolean SalDlgChooseColor(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalDlgChooseFont(HWND, LPHSTRING, LPINT, LPWORD, LPDWORD); file: centura200.h */
  CBoolean SalDlgChooseFont(CWndHandle var1, CString.ByReference var2, CNumber.ByReference var3,
      CNumber.ByReference var4, CNumber var5);

  /**
   * BOOL      CBEXPAPI  SalDlgOpenFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING); file: centura200.h */
  CBoolean SalDlgOpenFile(CWndHandle var1, CString var2, CArray<?> var3, CNumber var4,
      CNumber.ByReference var5, CString.ByReference var6, CString.ByReference var7);

  /**
   * BOOL      CBEXPAPI  SalDlgSaveFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING); file: centura200.h */
  CBoolean SalDlgSaveFile(CWndHandle var1, CString var2, CArray<?> var3, CNumber var4,
      CNumber.ByReference var5, CString.ByReference var6, CString.ByReference var7);

  /**
   * BOOL      CBEXPAPI  SalDragDropDisableDrop(VOID); file: centura200.h */
  CBoolean SalDragDropDisableDrop();

  /**
   * BOOL      CBEXPAPI  SalDragDropEnableDrop(VOID); file: centura200.h */
  CBoolean SalDragDropEnableDrop();

  /**
   * BOOL      CBEXPAPI  SalDragDropGetSource(LPHWND, LPINT, LPINT); file: centura200.h */
  CBoolean SalDragDropGetSource(CWndHandle.ByReference var1, CNumber.ByReference var2,
      CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalDragDropGetTarget(LPHWND, LPINT, LPINT); file: centura200.h */
  CBoolean SalDragDropGetTarget(CWndHandle.ByReference var1, CNumber.ByReference var2,
      CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalDragDropStart(HWND); file: centura200.h */
  CBoolean SalDragDropStart(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalDragDropStop(VOID); file: centura200.h */
  CBoolean SalDragDropStop();

  /**
   * BOOL      CBEXPAPI  SalDrawMenuBar(HWND); file: centura200.h */
  CBoolean SalDrawMenuBar(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalDropFilesAcceptFiles(HWND, BOOL); file: centura200.h */
  CBoolean SalDropFilesAcceptFiles(CWndHandle var1, CBoolean var2);

  /**
   * INT       CBEXPAPI  SalDropFilesQueryFiles(HWND, HARRAY); file: centura200.h */
  CNumber SalDropFilesQueryFiles(CWndHandle var1, CArray<?> var2);

  /**
   * BOOL      CBEXPAPI  SalDropFilesQueryPoint(HWND, LPINT, LPINT); file: centura200.h */
  CBoolean SalDropFilesQueryPoint(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalEditCanCopyTo(VOID); file: centura200.h */
  CBoolean SalEditCanCopyTo();

  /**
   * BOOL      CBEXPAPI  SalEditCanCut(VOID); file: centura200.h */
  CBoolean SalEditCanCut();

  /**
   * BOOL      CBEXPAPI  SalEditCanPaste(VOID); file: centura200.h */
  CBoolean SalEditCanPaste();

  /**
   * BOOL      CBEXPAPI  SalEditCanPasteFrom(VOID); file: centura200.h */
  CBoolean SalEditCanPasteFrom();

  /**
   * BOOL      CBEXPAPI  SalEditCanUndo(VOID); file: centura200.h */
  CBoolean SalEditCanUndo();

  /**
   * BOOL      CBEXPAPI  SalEditClear(VOID); file: centura200.h */
  CBoolean SalEditClear();

  /**
   * BOOL      CBEXPAPI  SalEditCopy(VOID); file: centura200.h */
  CBoolean SalEditCopy();

  /**
   * BOOL      CBEXPAPI  SalEditCopyString(LPSTR); file: centura200.h */
  CBoolean SalEditCopyString(CString var1);

  /**
   * BOOL      CBEXPAPI  SalEditCopyTo(VOID); file: centura200.h */
  CBoolean SalEditCopyTo();

  /**
   * BOOL      CBEXPAPI  SalEditCut(VOID); file: centura200.h */
  CBoolean SalEditCut();

  /**
   * BOOL      CBEXPAPI  SalEditPaste(VOID); file: centura200.h */
  CBoolean SalEditPaste();

  /**
   * BOOL      CBEXPAPI  SalEditPasteFrom(VOID); file: centura200.h */
  CBoolean SalEditPasteFrom();

  /**
   * BOOL      CBEXPAPI  SalEditPasteString(LPHSTRING); file: centura200.h */
  CBoolean SalEditPasteString(CString.ByReference var1);

  /**
   * BOOL      CBEXPAPI  SalEditUndo(VOID); file: centura200.h */
  CBoolean SalEditUndo();

  /**
   * BOOL      CBEXPAPI  SalEnableWindow(HWND); file: centura200.h */
  CBoolean SalEnableWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalEnableWindowAndLabel(HWND); file: centura200.h */
  CBoolean SalEnableWindowAndLabel(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalEndDialog(HWND, INT); file: centura200.h */
  CBoolean SalEndDialog(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalFileClose(LPHFFILE); file: centura200.h */
  CBoolean SalFileClose(CFile.ByReference var1);

  /**
   * INT       CBEXPAPI  SalFileCopy(LPSTR, LPSTR, BOOL); file: centura200.h */
  CNumber SalFileCopy(CString var1, CString var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalFileCreateDirectory(LPSTR); file: centura200.h */
  CBoolean SalFileCreateDirectory(CString var1);

  /**
   * BOOL      CBEXPAPI  SalFileGetC(HFFILE, LPWORD); file: centura200.h */
  CBoolean SalFileGetC(CFile var1, CNumber.ByReference var2);

  /**
   * INT       CBEXPAPI  SalFileGetChar(HFFILE); file: centura200.h */
  CNumber SalFileGetChar(CFile var1);

  /**
   * BOOL      CBEXPAPI  SalFileGetCurrentDirectory(LPHSTRING); file: centura200.h */
  CBoolean SalFileGetCurrentDirectory(CString.ByReference var1);

  /**
   * BOOL      CBEXPAPI  SalFileGetDateTime(LPSTR, LPDATETIME); file: centura200.h */
  CBoolean SalFileGetDateTime(CString var1, CDateTime.ByReference var2);

  /**
   * HSTRING   CBEXPAPI  SalFileGetDrive(VOID); file: centura200.h */
  CString SalFileGetDrive();

  /**
   * BOOL      CBEXPAPI  SalFileGetStr(HFFILE, LPHSTRING, INT); file: centura200.h */
  CBoolean SalFileGetStr(CFile var1, CString.ByReference var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalFileOpen(LPHFFILE, LPSTR, LONG); file: centura200.h */
  CBoolean SalFileOpen(CFile.ByReference var1, CString var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalFileOpenExt(LPHFFILE, LPSTR, LONG, LPHSTRING); file: centura200.h */
  CBoolean SalFileOpenExt(CFile.ByReference var1, CString var2, CNumber var3,
      CString.ByReference var4);

  /**
   * BOOL      CBEXPAPI  SalFilePutC(HFFILE, WORD); file: centura200.h */
  CBoolean SalFilePutC(CFile var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalFilePutChar(HFFILE, INT); file: centura200.h */
  CBoolean SalFilePutChar(CFile var1, CNumber var2);

  /**
   * INT       CBEXPAPI  SalFilePutStr(HFFILE, LPSTR); file: centura200.h */
  CNumber SalFilePutStr(CFile var1, CString var2);

  /**
   * LONG      CBEXPAPI  SalFileRead(HFFILE, LPHSTRING, LONG); file: centura200.h */
  CNumber SalFileRead(CFile var1, CString.ByReference var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalFileRemoveDirectory(LPSTR); file: centura200.h */
  CBoolean SalFileRemoveDirectory(CString var1);

  /**
   * BOOL      CBEXPAPI  SalFileSeek(HFFILE, LONG, INT); file: centura200.h */
  CBoolean SalFileSeek(CFile var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalFileSetCurrentDirectory(LPSTR); file: centura200.h */
  CBoolean SalFileSetCurrentDirectory(CString var1);

  /**
   * BOOL      CBEXPAPI  SalFileSetDateTime(LPSTR, DATETIME); file: centura200.h */
  CBoolean SalFileSetDateTime(CString var1, CDateTime var2);

  /**
   * BOOL      CBEXPAPI  SalFileSetDrive(LPSTR); file: centura200.h */
  CBoolean SalFileSetDrive(CString var1);

  /**
   * LONG      CBEXPAPI  SalFileTell(HFFILE); file: centura200.h */
  CNumber SalFileTell(CFile var1);

  /**
   * LONG      CBEXPAPI  SalFileWrite(HFFILE, HSTRING, LONG); file: centura200.h */
  CNumber SalFileWrite(CFile var1, CString var2, CNumber var3);

  /**
   * HWND      CBEXPAPI  SalFindWindow(HWND, LPSTR); file: centura200.h */
  CWndHandle SalFindWindow(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalFireEvent(HITEM, ...); file: centura200.h */
  CBoolean SalFireEvent(CHandle var1, CArray<?> var2);

  /**
   * BOOL      CBEXPAPI  SalFmtFieldToStr(HWND, LPHSTRING, BOOL); file: centura200.h */
  CBoolean SalFmtFieldToStr(CWndHandle var1, CString.ByReference var2, CBoolean var3);

  /**
   * HSTRING   CBEXPAPI  SalFmtFormatDateTime(DATETIME, LPSTR); file: centura200.h */
  CString SalFmtFormatDateTime(CDateTime var1, CString var2);

  /**
   * HSTRING   CBEXPAPI  SalFmtFormatNumber(NUMBER, LPSTR); file: centura200.h */
  CString SalFmtFormatNumber(CNumber var1, CString var2);

  /**
   * INT       CBEXPAPI  SalFmtGetFormat(HWND); file: centura200.h */
  CNumber SalFmtGetFormat(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalFmtGetInputMask(HWND, LPHSTRING); file: centura200.h */
  CBoolean SalFmtGetInputMask(CWndHandle var1, CString.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalFmtGetPicture(HWND, LPHSTRING); file: centura200.h */
  CBoolean SalFmtGetPicture(CWndHandle var1, CString.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidField(HWND); file: centura200.h */
  CBoolean SalFmtIsValidField(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidInputMask(LPSTR); file: centura200.h */
  CBoolean SalFmtIsValidInputMask(CString var1);

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidPicture(LPSTR, INT); file: centura200.h */
  CBoolean SalFmtIsValidPicture(CString var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalFmtKeepMask(BOOL); file: centura200.h */
  CBoolean SalFmtKeepMask(CBoolean var1);

  /**
   * BOOL      CBEXPAPI  SalFmtSetFormat(HWND, INT); file: centura200.h */
  CBoolean SalFmtSetFormat(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalFmtSetInputMask(HWND, LPSTR); file: centura200.h */
  CBoolean SalFmtSetInputMask(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalFmtSetPicture(HWND, LPSTR); file: centura200.h */
  CBoolean SalFmtSetPicture(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalFmtStrToField(HWND, LPSTR, BOOL); file: centura200.h */
  CBoolean SalFmtStrToField(CWndHandle var1, CString var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalFmtUnmaskInput(HWND, LPHSTRING); file: centura200.h */
  CBoolean SalFmtUnmaskInput(CWndHandle var1, CString.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalFmtValidateField(HWND, INT); file: centura200.h */
  CBoolean SalFmtValidateField(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalFontGet(HWND, LPHSTRING, LPINT, LPWORD); file: centura200.h */
  CBoolean SalFontGet(CWndHandle var1, CString.ByReference var2, CNumber.ByReference var3,
      CNumber.ByReference var4);

  /**
   * INT       CBEXPAPI  SalFontGetNames(WORD, HARRAY); file: centura200.h */
  CNumber SalFontGetNames(CNumber var1, CArray<?> var2);

  /**
   * INT       CBEXPAPI  SalFontGetSizes(WORD, LPSTR, HARRAY); file: centura200.h */
  CNumber SalFontGetSizes(CNumber var1, CString var2, CArray<?> var3);

  /**
   * BOOL      CBEXPAPI  SalFontSet(HWND, LPSTR, INT, WORD); file: centura200.h */
  CBoolean SalFontSet(CWndHandle var1, CString var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalFormGetParmNum(HWND, INT, LPNUMBER); file: centura200.h */
  CBoolean SalFormGetParmNum(CWndHandle var1, CNumber var2, CNumber.ByReference var3);

  /**
   * INT       CBEXPAPI  SalFormUnitsToPixels(HWND, NUMBER, BOOL); file: centura200.h */
  CNumber SalFormUnitsToPixels(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * INT       CBEXPAPI  SalGetDataType(HWND); file: centura200.h */
  CNumber SalGetDataType(CWndHandle var1);

  /**
   * HWND      CBEXPAPI  SalGetDefButton(HWND); file: centura200.h */
  CWndHandle SalGetDefButton(CWndHandle var1);

  /**
   * HWND      CBEXPAPI  SalGetFirstChild(HWND, LONG); file: centura200.h */
  CWndHandle SalGetFirstChild(CWndHandle var1, CNumber var2);

  /**
   * HWND      CBEXPAPI  SalGetFocus(VOID); file: centura200.h */
  CWndHandle SalGetFocus();

  /**
   * BOOL      CBEXPAPI  SalGetItemName(HWND, LPHSTRING); file: centura200.h */
  CBoolean SalGetItemName(CWndHandle var1, CString.ByReference var2);

  /**
   * INT       CBEXPAPI  SalGetMaxDataLength(HWND); file: centura200.h */
  CNumber SalGetMaxDataLength(CWndHandle var1);

  /**
   * HWND      CBEXPAPI  SalGetNextChild(HWND, LONG); file: centura200.h */
  CWndHandle SalGetNextChild(CWndHandle var1, CNumber var2);

  /**
   * UINT      CBEXPAPI  SalGetProfileInt(LPCSTR, LPCSTR, INT, LPCSTR); file: centura200.h */
  CNumber SalGetProfileInt(CString var1, CString var2, CNumber var3, CString var4);

  /**
   * BOOL      CBEXPAPI  SalGetProfileString(LPCSTR, LPCSTR, LPCSTR, LPHSTRING, LPCSTR); file: centura200.h */
  CBoolean SalGetProfileString(CString var1, CString var2, CString var3, CString.ByReference var4,
      CString var5);

  /**
   * LONG      CBEXPAPI  SalGetType(HWND); file: centura200.h */
  CNumber SalGetType(CWndHandle var1);

  /**
   * LPSTR CBEXPAPI SalGetUDVData(HANDLE h); file: centura200.h */
  CString SalGetUDVData(CHandle h);

  /**
   * WORD      CBEXPAPI  SalGetVersion(VOID); file: centura200.h */
  CNumber SalGetVersion();

  /**
   * INT       CBEXPAPI  SalGetWindowLabelText(HWND, LPHSTRING, INT); file: centura200.h */
  CNumber SalGetWindowLabelText(CWndHandle var1, CString.ByReference var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalGetWindowLoc(HWND, LPNUMBER, LPNUMBER); file: centura200.h */
  CBoolean SalGetWindowLoc(CWndHandle var1, CNumber.ByReference var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalGetWindowSize(HWND, LPNUMBER, LPNUMBER); file: centura200.h */
  CBoolean SalGetWindowSize(CWndHandle var1, CNumber.ByReference var2, CNumber.ByReference var3);

  /**
   * INT       CBEXPAPI  SalGetWindowState(HWND); file: centura200.h */
  CNumber SalGetWindowState(CWndHandle var1);

  /**
   * INT       CBEXPAPI  SalGetWindowText(HWND, LPHSTRING, INT); file: centura200.h */
  CNumber SalGetWindowText(CWndHandle var1, CString.ByReference var2, CNumber var3);

  /**
   * void CBEXPAPI SalHStringRef(HSTRING hString); file: centura200.h */
  void SalHStringRef(CString hString);

  /**
   * DWORD     CBEXPAPI  SalHStringToNumber(HSTRING); file: centura200.h */
  CNumber SalHStringToNumber(CString var1);

  /**
   * HSTRING CBEXPAPI SalHStringUnRef(HSTRING hString); file: centura200.h */
  CString SalHStringUnRef(CString hString);

  /**
   * BOOL      CBEXPAPI  SalHideWindow(HWND); file: centura200.h */
  CBoolean SalHideWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalHideWindowAndLabel(HWND); file: centura200.h */
  CBoolean SalHideWindowAndLabel(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIdleKick(VOID); file: centura200.h */
  CBoolean SalIdleKick();

  /**
   * BOOL      CBEXPAPI  SalIdleRegisterWindow(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  CBoolean SalIdleRegisterWindow(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalIdleUnregisterWindow(HWND); file: centura200.h */
  CBoolean SalIdleUnregisterWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalInvalidateWindow(HWND); file: centura200.h */
  CBoolean SalInvalidateWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIsButtonChecked(HWND); file: centura200.h */
  CBoolean SalIsButtonChecked(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIsNull(HWND); file: centura200.h */
  CBoolean SalIsNull(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIsValidDateTime(HWND); file: centura200.h */
  CBoolean SalIsValidDateTime(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIsValidDecimal(HWND, INT, INT); file: centura200.h */
  CBoolean SalIsValidDecimal(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalIsValidInteger(HWND); file: centura200.h */
  CBoolean SalIsValidInteger(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIsValidNumber(HWND); file: centura200.h */
  CBoolean SalIsValidNumber(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIsWindowEnabled(HWND); file: centura200.h */
  CBoolean SalIsWindowEnabled(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalIsWindowVisible(HWND); file: centura200.h */
  CBoolean SalIsWindowVisible(CWndHandle var1);

  /**
   * INT       CBEXPAPI  SalListAdd(HWND, LPSTR); file: centura200.h */
  CNumber SalListAdd(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalListClear(HWND); file: centura200.h */
  CBoolean SalListClear(CWndHandle var1);

  /**
   * INT       CBEXPAPI  SalListDelete(HWND, INT); file: centura200.h */
  CNumber SalListDelete(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalListFiles(HWND, HWND, LPHSTRING, WORD); file: centura200.h */
  CBoolean SalListFiles(CWndHandle var1, CWndHandle var2, CString.ByReference var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalListGetMultiSelect(HWND, HARRAY); file: centura200.h */
  CBoolean SalListGetMultiSelect(CWndHandle var1, CArray<?> var2);

  /**
   * INT       CBEXPAPI  SalListInsert(HWND, INT, LPSTR); file: centura200.h */
  CNumber SalListInsert(CWndHandle var1, CNumber var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SalListPopulate(HWND, SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SalListPopulate(CWndHandle var1, CSQLHandle var2, CString var3);

  /**
   * INT       CBEXPAPI  SalListQueryCount(HWND); file: centura200.h */
  CNumber SalListQueryCount(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalListQueryFile(HWND, LPHSTRING); file: centura200.h */
  CBoolean SalListQueryFile(CWndHandle var1, CString.ByReference var2);

  /**
   * NUMBER    CBEXPAPI  SalListQueryMultiCount(HWND); file: centura200.h */
  CNumber SalListQueryMultiCount(CWndHandle var1);

  /**
   * INT       CBEXPAPI  SalListQuerySelection(HWND); file: centura200.h */
  CNumber SalListQuerySelection(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalListQueryState(HWND, INT); file: centura200.h */
  CBoolean SalListQueryState(CWndHandle var1, CNumber var2);

  /**
   * INT       CBEXPAPI  SalListQueryText(HWND, INT, LPHSTRING); file: centura200.h */
  CNumber SalListQueryText(CWndHandle var1, CNumber var2, CString.ByReference var3);

  /**
   * INT       CBEXPAPI  SalListQueryTextLength(HWND, INT); file: centura200.h */
  CNumber SalListQueryTextLength(CWndHandle var1, CNumber var2);

  /**
   * HSTRING   CBEXPAPI  SalListQueryTextX(HWND, INT); file: centura200.h */
  CString SalListQueryTextX(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalListRedraw(HWND, BOOL); file: centura200.h */
  CBoolean SalListRedraw(CWndHandle var1, CBoolean var2);

  /**
   * INT       CBEXPAPI  SalListSelectString(HWND, INT, LPSTR); file: centura200.h */
  CNumber SalListSelectString(CWndHandle var1, CNumber var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SalListSetMultiSelect(HWND, INT, BOOL); file: centura200.h */
  CBoolean SalListSetMultiSelect(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalListSetSelect(HWND, INT); file: centura200.h */
  CBoolean SalListSetSelect(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalListSetTabs(HWND, HARRAY); file: centura200.h */
  CBoolean SalListSetTabs(CWndHandle var1, CArray<?> var2);

  /**
   * BOOL      CBEXPAPI  SalLoadApp(LPSTR, LPSTR); file: centura200.h */
  CBoolean SalLoadApp(CString var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalLoadAppAndWait(LPSTR, WORD, LPDWORD); file: centura200.h */
  CBoolean SalLoadAppAndWait(CString var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalMDIArrangeIcons(HWND); file: centura200.h */
  CBoolean SalMDIArrangeIcons(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalMDICascade(HWND); file: centura200.h */
  CBoolean SalMDICascade(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalMDITile(HWND, BOOL); file: centura200.h */
  CBoolean SalMDITile(CWndHandle var1, CBoolean var2);

  /**
   * BOOL      CBEXPAPI  SalMTSCreateInstance(HUDV); file: centura200.h */
  CBoolean SalMTSCreateInstance(CHandle var1);

  /**
   * BOOL      CBEXPAPI  SalMTSDisableCommit(VOID); file: centura200.h */
  CBoolean SalMTSDisableCommit();

  /**
   * BOOL      CBEXPAPI  SalMTSEnableCommit(VOID); file: centura200.h */
  CBoolean SalMTSEnableCommit();

  /**
   * BOOL      CBEXPAPI  SalMTSGetObjectContext(LPLONG); file: centura200.h */
  CBoolean SalMTSGetObjectContext(CNumber.ByReference var1);

  /**
   * BOOL      CBEXPAPI  SalMTSIsCallerInRole(LPSTR, LPBOOL); file: centura200.h */
  CBoolean SalMTSIsCallerInRole(CString var1, CBoolean var2);

  /**
   * BOOL      CBEXPAPI  SalMTSIsInTransaction(LPBOOL); file: centura200.h */
  CBoolean SalMTSIsInTransaction(CBoolean var1);

  /**
   * BOOL      CBEXPAPI  SalMTSIsSecurityEnabled(LPBOOL); file: centura200.h */
  CBoolean SalMTSIsSecurityEnabled(CBoolean var1);

  /**
   * BOOL      CBEXPAPI  SalMTSSetAbort(VOID); file: centura200.h */
  CBoolean SalMTSSetAbort();

  /**
   * BOOL      CBEXPAPI  SalMTSSetComplete(VOID); file: centura200.h */
  CBoolean SalMTSSetComplete();

  /**
   * BOOL      CBEXPAPI  SalMapEnterToTab(BOOL); file: centura200.h */
  CBoolean SalMapEnterToTab(CBoolean var1);

  /**
   * BOOL      CBEXPAPI  SalMessageBeep(WPARAM); file: centura200.h */
  CBoolean SalMessageBeep(CNumber var1);

  /**
   * INT       CBEXPAPI  SalMessageBox(LPSTR, LPSTR, UINT); file: centura200.h */
  CNumber SalMessageBox(CString var1, CString var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalMoveWindow(HWND, NUMBER, NUMBER); file: centura200.h */
  CBoolean SalMoveWindow(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * LPSTR CBEXPAPI SalNewUDVObject(LPSTR); file: centura200.h */
  CString SalNewUDVObject(CString var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberAbs(NUMBER); file: centura200.h */
  CNumber SalNumberAbs(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberArcCos(NUMBER); file: centura200.h */
  CNumber SalNumberArcCos(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberArcSin(NUMBER); file: centura200.h */
  CNumber SalNumberArcSin(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberArcTan(NUMBER); file: centura200.h */
  CNumber SalNumberArcTan(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberArcTan2(NUMBER, NUMBER); file: centura200.h */
  CNumber SalNumberArcTan2(CNumber var1, CNumber var2);

  /**
   * NUMBER    CBEXPAPI  SalNumberCos(NUMBER); file: centura200.h */
  CNumber SalNumberCos(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberCosH(NUMBER); file: centura200.h */
  CNumber SalNumberCosH(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberExponent(NUMBER); file: centura200.h */
  CNumber SalNumberExponent(CNumber var1);

  /**
   * WORD      CBEXPAPI  SalNumberHigh(DWORD); file: centura200.h */
  CNumber SalNumberHigh(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberHypot(NUMBER, NUMBER); file: centura200.h */
  CNumber SalNumberHypot(CNumber var1, CNumber var2);

  /**
   * NUMBER    CBEXPAPI  SalNumberLog(NUMBER); file: centura200.h */
  CNumber SalNumberLog(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberLogBase10(NUMBER); file: centura200.h */
  CNumber SalNumberLogBase10(CNumber var1);

  /**
   * WORD      CBEXPAPI  SalNumberLow(DWORD); file: centura200.h */
  CNumber SalNumberLow(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberMax(NUMBER, NUMBER); file: centura200.h */
  CNumber SalNumberMax(CNumber var1, CNumber var2);

  /**
   * NUMBER    CBEXPAPI  SalNumberMin(NUMBER, NUMBER); file: centura200.h */
  CNumber SalNumberMin(CNumber var1, CNumber var2);

  /**
   * NUMBER    CBEXPAPI  SalNumberMod(NUMBER, NUMBER); file: centura200.h */
  CNumber SalNumberMod(CNumber var1, CNumber var2);

  /**
   * NUMBER    CBEXPAPI  SalNumberPi(NUMBER); file: centura200.h */
  CNumber SalNumberPi(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberPower(NUMBER, NUMBER); file: centura200.h */
  CNumber SalNumberPower(CNumber var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalNumberRandInit(WORD); file: centura200.h */
  CBoolean SalNumberRandInit(CNumber var1);

  /**
   * INT       CBEXPAPI  SalNumberRandom(VOID); file: centura200.h */
  CNumber SalNumberRandom();

  /**
   * NUMBER    CBEXPAPI  SalNumberRound(NUMBER); file: centura200.h */
  CNumber SalNumberRound(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberSin(NUMBER); file: centura200.h */
  CNumber SalNumberSin(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberSinH(NUMBER); file: centura200.h */
  CNumber SalNumberSinH(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberSqrt(NUMBER); file: centura200.h */
  CNumber SalNumberSqrt(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberTan(NUMBER); file: centura200.h */
  CNumber SalNumberTan(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberTanH(NUMBER); file: centura200.h */
  CNumber SalNumberTanH(CNumber var1);

  /**
   * HSTRING   CBEXPAPI  SalNumberToChar(WORD); file: centura200.h */
  CString SalNumberToChar(CNumber var1);

  /**
   * HSTRING   CBEXPAPI  SalNumberToHString(DWORD); file: centura200.h */
  CString SalNumberToHString(CNumber var1);

  /**
   * INT       CBEXPAPI  SalNumberToStr(NUMBER, INT, LPHSTRING); file: centura200.h */
  CNumber SalNumberToStr(CNumber var1, CNumber var2, CString.ByReference var3);

  /**
   * HSTRING   CBEXPAPI  SalNumberToStrX(NUMBER, INT); file: centura200.h */
  CString SalNumberToStrX(CNumber var1, CNumber var2);

  /**
   * HWND      CBEXPAPI  SalNumberToWindowHandle(UINT); file: centura200.h */
  CWndHandle SalNumberToWindowHandle(CNumber var1);

  /**
   * NUMBER    CBEXPAPI  SalNumberTruncate(NUMBER, INT, INT); file: centura200.h */
  CNumber SalNumberTruncate(CNumber var1, CNumber var2, CNumber var3);

  /**
   * HUDV      CBEXPAPI  SalObjCreateFromString(LPSTR); file: centura200.h */
  CHandle SalObjCreateFromString(CString var1);

  /**
   * HSTRING   CBEXPAPI  SalObjGetType(HUDV); file: centura200.h */
  CString SalObjGetType(CHandle var1);

  /**
   * BOOL      CBEXPAPI  SalObjIsDerived(HUDV, LPSTR); file: centura200.h */
  CBoolean SalObjIsDerived(CHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalObjIsNull(HUDV); file: centura200.h */
  CBoolean SalObjIsNull(CHandle var1);

  /**
   * BOOL      CBEXPAPI  SalObjIsValidClassName(LPSTR); file: centura200.h */
  CBoolean SalObjIsValidClassName(CString var1);

  /**
   * HWND      CBEXPAPI  SalParentWindow(HWND); file: centura200.h */
  CWndHandle SalParentWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalPicClear(HWND); file: centura200.h */
  CBoolean SalPicClear(CWndHandle var1);

  /**
   * INT       CBEXPAPI  SalPicGetDescription(HWND, LPHSTRING, INT); file: centura200.h */
  CNumber SalPicGetDescription(CWndHandle var1, CString.ByReference var2, CNumber var3);

  /**
   * LONG      CBEXPAPI  SalPicGetImage(HWND, LPHSTRING, LPINT); file: centura200.h */
  CNumber SalPicGetImage(CWndHandle var1, CString.ByReference var2, CNumber.ByReference var3);

  /**
   * LONG      CBEXPAPI  SalPicGetString(HWND, INT, LPHSTRING); file: centura200.h */
  CNumber SalPicGetString(CWndHandle var1, CNumber var2, CString.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalPicSet(HWND, HITEM, INT); file: centura200.h */
  CBoolean SalPicSet(CWndHandle var1, CHandle var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalPicSetFile(HWND, LPSTR); file: centura200.h */
  CBoolean SalPicSetFile(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalPicSetFit(HWND, INT, INT, INT); file: centura200.h */
  CBoolean SalPicSetFit(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalPicSetHandle(HWND, INT, DWORD); file: centura200.h */
  CBoolean SalPicSetHandle(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalPicSetImage(HWND, HSTRING, INT); file: centura200.h */
  CBoolean SalPicSetImage(CWndHandle var1, CString var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalPicSetString(HWND, INT, HSTRING); file: centura200.h */
  CBoolean SalPicSetString(CWndHandle var1, CNumber var2, CString var3);

  /**
   * NUMBER    CBEXPAPI  SalPixelsToFormUnits(HWND, INT, BOOL); file: centura200.h */
  CNumber SalPixelsToFormUnits(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalPostMsg(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  CBoolean SalPostMsg(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalPrtExtractRect(LONG, LPINT, LPINT, LPINT, LPINT); file: centura200.h */
  CBoolean SalPrtExtractRect(CNumber var1, CNumber.ByReference var2, CNumber.ByReference var3,
      CNumber.ByReference var4, CNumber.ByReference var5);

  /**
   * BOOL      CBEXPAPI  SalPrtGetDefault(LPHSTRING, LPHSTRING, LPHSTRING); file: centura200.h */
  CBoolean SalPrtGetDefault(CString.ByReference var1, CString.ByReference var2,
      CString.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalPrtGetParmNum(INT, LPNUMBER); file: centura200.h */
  CBoolean SalPrtGetParmNum(CNumber var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalPrtPrintForm(HWND); file: centura200.h */
  CBoolean SalPrtPrintForm(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalPrtSetDefault(LPSTR, LPSTR, LPSTR); file: centura200.h */
  CBoolean SalPrtSetDefault(CString var1, CString var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SalPrtSetParmDefaults(VOID); file: centura200.h */
  CBoolean SalPrtSetParmDefaults();

  /**
   * BOOL      CBEXPAPI  SalPrtSetParmNum(INT, NUMBER); file: centura200.h */
  CBoolean SalPrtSetParmNum(CNumber var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalPrtSetup(LPHSTRING, LPHSTRING, LPHSTRING, BOOL); file: centura200.h */
  CBoolean SalPrtSetup(CString.ByReference var1, CString.ByReference var2, CString.ByReference var3,
      CBoolean var4);

  /**
   * DWORD     CBEXPAPI  SalQueryArrayBounds(HARRAY, LPLONG, LPLONG); file: centura200.h */
  CNumber SalQueryArrayBounds(CArray<?> var1, CNumber.ByReference var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalQueryFieldEdit(HWND); file: centura200.h */
  CBoolean SalQueryFieldEdit(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalQuit(VOID); file: centura200.h */
  CBoolean SalQuit();

  /**
   * BOOL      CBEXPAPI  SalReportClose(HWND); file: centura200.h */
  CBoolean SalReportClose(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalReportCmd(HWND, INT); file: centura200.h */
  CBoolean SalReportCmd(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalReportCreate(LPSTR, LPSTR, LPSTR, BOOL, LPINT); file: centura200.h */
  CBoolean SalReportCreate(CString var1, CString var2, CString var3, CBoolean var4,
      CNumber.ByReference var5);

  /**
   * BOOL      CBEXPAPI  SalReportDlgOptions(HWND, LPSTR, LPSTR, LPSTR, LPSTR); file: centura200.h */
  CBoolean SalReportDlgOptions(CWndHandle var1, CString var2, CString var3, CString var4,
      CString var5);

  /**
   * BOOL      CBEXPAPI  SalReportGetDateTimeVar(HWND, LPSTR, LPDATETIME); file: centura200.h */
  CBoolean SalReportGetDateTimeVar(CWndHandle var1, CString var2, CDateTime.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalReportGetNumberVar(HWND, LPSTR, LPNUMBER); file: centura200.h */
  CBoolean SalReportGetNumberVar(CWndHandle var1, CString var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalReportGetObjectVar(HWND, LPSTR, LPHSTRING); file: centura200.h */
  CBoolean SalReportGetObjectVar(CWndHandle var1, CString var2, CString.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalReportGetStringVar(HWND, LPSTR, LPHSTRING); file: centura200.h */
  CBoolean SalReportGetStringVar(CWndHandle var1, CString var2, CString.ByReference var3);

  /**
   * HWND      CBEXPAPI  SalReportPrint(HWND, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, LPINT); file: centura200.h */
  CWndHandle SalReportPrint(CWndHandle var1, CString var2, CString var3, CString var4, CNumber var5,
      CNumber var6, CNumber var7, CNumber var8, CNumber.ByReference var9);

  /**
   * HWND      CBEXPAPI  SalReportPrintToFile(HWND, LPSTR, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, BOOL, LPINT); file: centura200.h */
  CWndHandle SalReportPrintToFile(CWndHandle var1, CString var2, CString var3, CString var4,
      CString var5, CNumber var6, CNumber var7, CNumber var8, CNumber var9, CBoolean var10,
      CNumber.ByReference var11);

  /**
   * BOOL      CBEXPAPI  SalReportReset(HWND); file: centura200.h */
  CBoolean SalReportReset(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalReportSetDateTimeVar(HWND, LPSTR, DATETIME); file: centura200.h */
  CBoolean SalReportSetDateTimeVar(CWndHandle var1, CString var2, CDateTime var3);

  /**
   * BOOL      CBEXPAPI  SalReportSetNumberVar(HWND, LPSTR, NUMBER); file: centura200.h */
  CBoolean SalReportSetNumberVar(CWndHandle var1, CString var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalReportSetObjectVar(HWND, LPSTR, HSTRING); file: centura200.h */
  CBoolean SalReportSetObjectVar(CWndHandle var1, CString var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SalReportSetStringVar(HWND, LPSTR, LPSTR); file: centura200.h */
  CBoolean SalReportSetStringVar(CWndHandle var1, CString var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SalReportTableCreate(LPSTR, HWND, LPINT); file: centura200.h */
  CBoolean SalReportTableCreate(CString var1, CWndHandle var2, CNumber.ByReference var3);

  /**
   * HWND      CBEXPAPI  SalReportTablePrint(HWND, LPSTR, HARRAY, LPINT); file: centura200.h */
  CWndHandle SalReportTablePrint(CWndHandle var1, CString var2, CArray<?> var3,
      CNumber.ByReference var4);

  /**
   * HWND      CBEXPAPI  SalReportTableView(HWND, HWND, LPSTR, LPINT); file: centura200.h */
  CWndHandle SalReportTableView(CWndHandle var1, CWndHandle var2, CString var3,
      CNumber.ByReference var4);

  /**
   * HWND      CBEXPAPI  SalReportView(HWND, HWND, LPSTR, LPSTR, LPSTR, LPINT); file: centura200.h */
  CWndHandle SalReportView(CWndHandle var1, CWndHandle var2, CString var3, CString var4,
      CString var5, CNumber.ByReference var6);

  /**
   * BOOL      CBEXPAPI  SalScrollGetPos(HWND, LPINT); file: centura200.h */
  CBoolean SalScrollGetPos(CWndHandle var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalScrollGetRange(HWND, LPINT, LPINT, LPINT, LPINT); file: centura200.h */
  CBoolean SalScrollGetRange(CWndHandle var1, CNumber.ByReference var2, CNumber.ByReference var3,
      CNumber.ByReference var4, CNumber.ByReference var5);

  /**
   * BOOL      CBEXPAPI  SalScrollSetPos(HWND, INT); file: centura200.h */
  CBoolean SalScrollSetPos(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalScrollSetRange(HWND, INT, INT, INT, INT); file: centura200.h */
  CBoolean SalScrollSetRange(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4,
      CNumber var5);

  /**
   * LONG      CBEXPAPI  SalSendClassMessage(UINT, WPARAM, LPARAM); file: centura200.h */
  CNumber SalSendClassMessage(CNumber var1, CNumber var2, CNumber var3);

  /**
   * LONG      CBEXPAPI  SalSendClassMessageNamed(HITEM, UINT, WPARAM, LPARAM); file: centura200.h */
  CNumber SalSendClassMessageNamed(CHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * LONG      CBEXPAPI  SalSendMsg(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  CNumber SalSendMsg(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalSendMsgToChildren(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  CBoolean SalSendMsgToChildren(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * WORD      CBEXPAPI  SalSendValidateMsg(VOID); file: centura200.h */
  CNumber SalSendValidateMsg();

  /**
   * BOOL      CBEXPAPI  SalSetArrayBounds(HARRAY, LONG, LONG); file: centura200.h */
  CBoolean SalSetArrayBounds(CArray<?> var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalSetDefButton(HWND); file: centura200.h */
  CBoolean SalSetDefButton(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalSetErrorInfo(LONG, LPSTR, LPSTR, DWORD); file: centura200.h */
  CBoolean SalSetErrorInfo(CNumber var1, CString var2, CString var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalSetFieldEdit(HWND, BOOL); file: centura200.h */
  CBoolean SalSetFieldEdit(CWndHandle var1, CBoolean var2);

  /**
   * HWND      CBEXPAPI  SalSetFocus(HWND); file: centura200.h */
  CWndHandle SalSetFocus(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalSetMaxDataLength(HWND, LONG); file: centura200.h */
  CBoolean SalSetMaxDataLength(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalSetProfileString(LPCSTR, LPCSTR, LPCSTR, LPCSTR); file: centura200.h */
  CBoolean SalSetProfileString(CString var1, CString var2, CString var3, CString var4);

  /**
   * BOOL      CBEXPAPI  SalSetWindowLabelText(HWND, LPSTR); file: centura200.h */
  CBoolean SalSetWindowLabelText(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalSetWindowLoc(HWND, NUMBER, NUMBER); file: centura200.h */
  CBoolean SalSetWindowLoc(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalSetWindowSize(HWND, NUMBER, NUMBER); file: centura200.h */
  CBoolean SalSetWindowSize(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalSetWindowText(HWND, LPSTR); file: centura200.h */
  CBoolean SalSetWindowText(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalShowWindow(HWND); file: centura200.h */
  CBoolean SalShowWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalShowWindowAndLabel(HWND); file: centura200.h */
  CBoolean SalShowWindowAndLabel(CWndHandle var1);

  /**
   * INT       CBEXPAPI  SalStatusGetText(HWND, LPHSTRING, INT); file: centura200.h */
  CNumber SalStatusGetText(CWndHandle var1, CString.ByReference var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalStatusSetText(HWND, LPSTR); file: centura200.h */
  CBoolean SalStatusSetText(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalStatusSetVisible(HWND, BOOL); file: centura200.h */
  CBoolean SalStatusSetVisible(CWndHandle var1, CBoolean var2);

  /**
   * BOOL      CBEXPAPI  SalStrCompress(LPHSTRING); file: centura200.h */
  CBoolean SalStrCompress(CString.ByReference var1);

  /**
   * BOOL      CBEXPAPI  SalStrFirstC(LPHSTRING, LPWORD); file: centura200.h */
  CBoolean SalStrFirstC(CString.ByReference var1, CNumber.ByReference var2);

  /**
   * LONG      CBEXPAPI  SalStrGetBufferLength(HSTRING); file: centura200.h */
  CNumber SalStrGetBufferLength(CString var1);

  /**
   * BOOL      CBEXPAPI  SalStrIsValidCurrency(LPSTR, INT, INT); file: centura200.h */
  CBoolean SalStrIsValidCurrency(CString var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalStrIsValidDateTime(LPSTR); file: centura200.h */
  CBoolean SalStrIsValidDateTime(CString var1);

  /**
   * BOOL      CBEXPAPI  SalStrIsValidNumber(LPSTR); file: centura200.h */
  CBoolean SalStrIsValidNumber(CString var1);

  /**
   * LONG      CBEXPAPI  SalStrLeft(HSTRING, LONG, LPHSTRING); file: centura200.h */
  CNumber SalStrLeft(CString var1, CNumber var2, CString.ByReference var3);

  /**
   * HSTRING   CBEXPAPI  SalStrLeftX(HSTRING, LONG); file: centura200.h */
  CString SalStrLeftX(CString var1, CNumber var2);

  /**
   * LONG      CBEXPAPI  SalStrLength(LPSTR); file: centura200.h */
  CNumber SalStrLength(CString var1);

  /**
   * INT       CBEXPAPI  SalStrLop(LPHSTRING); file: centura200.h */
  CNumber SalStrLop(CString.ByReference var1);

  /**
   * LONG      CBEXPAPI  SalStrLower(HSTRING, LPHSTRING); file: centura200.h */
  CNumber SalStrLower(CString var1, CString.ByReference var2);

  /**
   * HSTRING   CBEXPAPI  SalStrLowerX(HSTRING); file: centura200.h */
  CString SalStrLowerX(CString var1);

  /**
   * LONG      CBEXPAPI  SalStrMid(HSTRING, LONG, LONG, LPHSTRING); file: centura200.h */
  CNumber SalStrMid(CString var1, CNumber var2, CNumber var3, CString.ByReference var4);

  /**
   * HSTRING   CBEXPAPI  SalStrMidX(HSTRING, LONG, LONG); file: centura200.h */
  CString SalStrMidX(CString var1, CNumber var2, CNumber var3);

  /**
   * LONG      CBEXPAPI  SalStrProper(HSTRING, LPHSTRING); file: centura200.h */
  CNumber SalStrProper(CString var1, CString.ByReference var2);

  /**
   * HSTRING   CBEXPAPI  SalStrProperX(HSTRING); file: centura200.h */
  CString SalStrProperX(CString var1);

  /**
   * LONG      CBEXPAPI  SalStrRepeat(HSTRING, INT, LPHSTRING); file: centura200.h */
  CNumber SalStrRepeat(CString var1, CNumber var2, CString.ByReference var3);

  /**
   * HSTRING   CBEXPAPI  SalStrRepeatX(HSTRING, INT); file: centura200.h */
  CString SalStrRepeatX(CString var1, CNumber var2);

  /**
   * LONG      CBEXPAPI  SalStrReplace(HSTRING, INT, INT, HSTRING, LPHSTRING); file: centura200.h */
  CNumber SalStrReplace(CString var1, CNumber var2, CNumber var3, CString var4,
      CString.ByReference var5);

  /**
   * HSTRING   CBEXPAPI  SalStrReplaceX(HSTRING, INT, INT, HSTRING); file: centura200.h */
  CString SalStrReplaceX(CString var1, CNumber var2, CNumber var3, CString var4);

  /**
   * LONG      CBEXPAPI  SalStrRight(HSTRING, LONG, LPHSTRING); file: centura200.h */
  CNumber SalStrRight(CString var1, CNumber var2, CString.ByReference var3);

  /**
   * HSTRING   CBEXPAPI  SalStrRightX(HSTRING, LONG); file: centura200.h */
  CString SalStrRightX(CString var1, CNumber var2);

  /**
   * INT       CBEXPAPI  SalStrScan(LPSTR, LPSTR); file: centura200.h */
  CNumber SalStrScan(CString var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalStrSetBufferLength(LPHSTRING, LONG); file: centura200.h */
  CBoolean SalStrSetBufferLength(CString.ByReference var1, CNumber var2);

  /**
   * DATETIME  CBEXPAPI  SalStrToDate(LPSTR); file: centura200.h */
  CDateTime SalStrToDate(CString var1);

  /**
   * NUMBER    CBEXPAPI  SalStrToNumber(LPSTR); file: centura200.h */
  CNumber SalStrToNumber(CString var1);

  /**
   * INT       CBEXPAPI  SalStrTokenize(LPSTR, LPSTR, LPSTR, HARRAY); file: centura200.h */
  CNumber SalStrTokenize(CString var1, CString var2, CString var3, CArray<?> var4);

  /**
   * INT       CBEXPAPI  SalStrTrim(HSTRING, LPHSTRING); file: centura200.h */
  CNumber SalStrTrim(CString var1, CString.ByReference var2);

  /**
   * HSTRING   CBEXPAPI  SalStrTrimX(HSTRING); file: centura200.h */
  CString SalStrTrimX(CString var1);

  /**
   * BOOL      CBEXPAPI  SalStrUncompress(LPHSTRING); file: centura200.h */
  CBoolean SalStrUncompress(CString.ByReference var1);

  /**
   * LONG      CBEXPAPI  SalStrUpper(HSTRING, LPHSTRING); file: centura200.h */
  CNumber SalStrUpper(CString var1, CString.ByReference var2);

  /**
   * HSTRING   CBEXPAPI  SalStrUpperX(HSTRING); file: centura200.h */
  CString SalStrUpperX(CString var1);

  /**
   * BOOL      CBEXPAPI  SalTBarSetVisible(HWND, BOOL); file: centura200.h */
  CBoolean SalTBarSetVisible(CWndHandle var1, CBoolean var2);

  /**
   * BOOL      CBEXPAPI  SalTblAnyRows(HWND, WORD, WORD); file: centura200.h */
  CBoolean SalTblAnyRows(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTblClearSelection(HWND); file: centura200.h */
  CBoolean SalTblClearSelection(CWndHandle var1);

  /**
   * NUMBER    CBEXPAPI  SalTblColumnAverage(HWND, WORD, WORD, WORD); file: centura200.h */
  CNumber SalTblColumnAverage(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * NUMBER    CBEXPAPI  SalTblColumnSum(HWND, WORD, WORD, WORD); file: centura200.h */
  CNumber SalTblColumnSum(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalTblCopyRows(HWND, WORD, WORD); file: centura200.h */
  CBoolean SalTblCopyRows(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * INT       CBEXPAPI  SalTblCreateColumn(HWND, UINT, NUMBER, INT, LPSTR); file: centura200.h */
  CNumber SalTblCreateColumn(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4,
      CString var5);

  /**
   * BOOL      CBEXPAPI  SalTblDefineCheckBoxColumn(HWND, DWORD, LPSTR, LPSTR); file: centura200.h */
  CBoolean SalTblDefineCheckBoxColumn(CWndHandle var1, CNumber var2, CString var3, CString var4);

  /**
   * BOOL      CBEXPAPI  SalTblDefineDropDownListColumn(HWND, DWORD, INT); file: centura200.h */
  CBoolean SalTblDefineDropDownListColumn(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTblDefinePopupEditColumn(HWND, DWORD, INT); file: centura200.h */
  CBoolean SalTblDefinePopupEditColumn(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTblDefineRowHeader(HWND, LPSTR, INT, WORD, HWND); file: centura200.h */
  CBoolean SalTblDefineRowHeader(CWndHandle var1, CString var2, CNumber var3, CNumber var4,
      CWndHandle var5);

  /**
   * BOOL      CBEXPAPI  SalTblDefineSplitWindow(HWND, INT, BOOL); file: centura200.h */
  CBoolean SalTblDefineSplitWindow(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalTblDeleteRow(HWND, LONG, WORD); file: centura200.h */
  CBoolean SalTblDeleteRow(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTblDeleteSelected(HWND, SQLHANDLENUMBER); file: centura200.h */
  CBoolean SalTblDeleteSelected(CWndHandle var1, CSQLHandle var2);

  /**
   * BOOL      CBEXPAPI  SalTblDestroyColumns(HWND); file: centura200.h */
  CBoolean SalTblDestroyColumns(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblDoDeletes(HWND, SQLHANDLENUMBER, WORD); file: centura200.h */
  CBoolean SalTblDoDeletes(CWndHandle var1, CSQLHandle var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTblDoInserts(HWND, SQLHANDLENUMBER, BOOL); file: centura200.h */
  CBoolean SalTblDoInserts(CWndHandle var1, CSQLHandle var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalTblDoUpdates(HWND, SQLHANDLENUMBER, BOOL); file: centura200.h */
  CBoolean SalTblDoUpdates(CWndHandle var1, CSQLHandle var2, CBoolean var3);

  /**
   * INT       CBEXPAPI  SalTblFetchRow(HWND, LONG); file: centura200.h */
  CNumber SalTblFetchRow(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblFindNextRow(HWND, LPLONG, WORD, WORD); file: centura200.h */
  CBoolean SalTblFindNextRow(CWndHandle var1, CNumber.ByReference var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalTblFindPrevRow(HWND, LPLONG, WORD, WORD); file: centura200.h */
  CBoolean SalTblFindPrevRow(CWndHandle var1, CNumber.ByReference var2, CNumber var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalTblGetColumnText(HWND, UINT, LPHSTRING); file: centura200.h */
  CBoolean SalTblGetColumnText(CWndHandle var1, CNumber var2, CString.ByReference var3);

  /**
   * INT       CBEXPAPI  SalTblGetColumnTitle(HWND, LPHSTRING, INT); file: centura200.h */
  CNumber SalTblGetColumnTitle(CWndHandle var1, CString.ByReference var2, CNumber var3);

  /**
   * HWND      CBEXPAPI  SalTblGetColumnWindow(HWND, INT, WORD); file: centura200.h */
  CWndHandle SalTblGetColumnWindow(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * LONG      CBEXPAPI  SalTblInsertRow(HWND, LONG); file: centura200.h */
  CNumber SalTblInsertRow(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblKillEdit(HWND); file: centura200.h */
  CBoolean SalTblKillEdit(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblKillFocus(HWND); file: centura200.h */
  CBoolean SalTblKillFocus(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblObjectsFromPoint(HWND, INT, INT, LPLONG, LPHWND, LPDWORD); file: centura200.h */
  CBoolean SalTblObjectsFromPoint(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber.ByReference var4, CWndHandle.ByReference var5, CNumber var6);

  /**
   * BOOL      CBEXPAPI  SalTblPasteRows(HWND); file: centura200.h */
  CBoolean SalTblPasteRows(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblPopulate(HWND, SQLHANDLENUMBER, LPSTR, INT); file: centura200.h */
  CBoolean SalTblPopulate(CWndHandle var1, CSQLHandle var2, CString var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalTblQueryCheckBoxColumn(HWND, LPDWORD, LPHSTRING, LPHSTRING); file: centura200.h */
  CBoolean SalTblQueryCheckBoxColumn(CWndHandle var1, CNumber var2, CString.ByReference var3,
      CString.ByReference var4);

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnCellType(HWND, LPINT); file: centura200.h */
  CBoolean SalTblQueryColumnCellType(CWndHandle var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnFlags(HWND, DWORD); file: centura200.h */
  CBoolean SalTblQueryColumnFlags(CWndHandle var1, CNumber var2);

  /**
   * INT       CBEXPAPI  SalTblQueryColumnID(HWND); file: centura200.h */
  CNumber SalTblQueryColumnID(CWndHandle var1);

  /**
   * INT       CBEXPAPI  SalTblQueryColumnPos(HWND); file: centura200.h */
  CNumber SalTblQueryColumnPos(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnWidth(HWND, LPNUMBER); file: centura200.h */
  CBoolean SalTblQueryColumnWidth(CWndHandle var1, CNumber.ByReference var2);

  /**
   * LONG      CBEXPAPI  SalTblQueryContext(HWND); file: centura200.h */
  CNumber SalTblQueryContext(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblQueryDropDownListColumn(HWND, LPDWORD, LPINT); file: centura200.h */
  CBoolean SalTblQueryDropDownListColumn(CWndHandle var1, CNumber var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalTblQueryFocus(HWND, LPLONG, LPHWND); file: centura200.h */
  CBoolean SalTblQueryFocus(CWndHandle var1, CNumber.ByReference var2, CWndHandle.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalTblQueryLinesPerRow(HWND, LPINT); file: centura200.h */
  CBoolean SalTblQueryLinesPerRow(CWndHandle var1, CNumber.ByReference var2);

  /**
   * INT       CBEXPAPI  SalTblQueryLockedColumns(HWND); file: centura200.h */
  CNumber SalTblQueryLockedColumns(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblQueryPopupEditColumn(HWND, LPDWORD, LPINT); file: centura200.h */
  CBoolean SalTblQueryPopupEditColumn(CWndHandle var1, CNumber var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalTblQueryRowFlags(HWND, LONG, WORD); file: centura200.h */
  CBoolean SalTblQueryRowFlags(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTblQueryRowHeader(HWND, LPHSTRING, INT, LPINT, LPWORD, LPHWND); file: centura200.h */
  CBoolean SalTblQueryRowHeader(CWndHandle var1, CString.ByReference var2, CNumber var3,
      CNumber.ByReference var4, CNumber.ByReference var5, CWndHandle.ByReference var6);

  /**
   * BOOL      CBEXPAPI  SalTblQueryScroll(HWND, LPLONG, LPLONG, LPLONG); file: centura200.h */
  CBoolean SalTblQueryScroll(CWndHandle var1, CNumber.ByReference var2, CNumber.ByReference var3,
      CNumber.ByReference var4);

  /**
   * BOOL      CBEXPAPI  SalTblQuerySplitWindow(HWND, LPINT, LPBOOL); file: centura200.h */
  CBoolean SalTblQuerySplitWindow(CWndHandle var1, CNumber.ByReference var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalTblQueryTableFlags(HWND, WORD); file: centura200.h */
  CBoolean SalTblQueryTableFlags(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblQueryVisibleRange(HWND, LPLONG, LPLONG); file: centura200.h */
  CBoolean SalTblQueryVisibleRange(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SalTblReset(HWND); file: centura200.h */
  CBoolean SalTblReset(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalTblScroll(HWND, LONG, HWND, WORD); file: centura200.h */
  CBoolean SalTblScroll(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4);

  /**
   * BOOL      CBEXPAPI  SalTblSetCellTextColor(HWND, DWORD, BOOL); file: centura200.h */
  CBoolean SalTblSetCellTextColor(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnFlags(HWND, DWORD, BOOL); file: centura200.h */
  CBoolean SalTblSetColumnFlags(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnPos(HWND, INT); file: centura200.h */
  CBoolean SalTblSetColumnPos(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnText(HWND, UINT, LPSTR); file: centura200.h */
  CBoolean SalTblSetColumnText(CWndHandle var1, CNumber var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnTitle(HWND, LPSTR); file: centura200.h */
  CBoolean SalTblSetColumnTitle(CWndHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnWidth(HWND, NUMBER); file: centura200.h */
  CBoolean SalTblSetColumnWidth(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetContext(HWND, LONG); file: centura200.h */
  CBoolean SalTblSetContext(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetFlagsAnyRows(HWND, WORD, BOOL, WORD, WORD); file: centura200.h */
  CBoolean SalTblSetFlagsAnyRows(CWndHandle var1, CNumber var2, CBoolean var3, CNumber var4,
      CNumber var5);

  /**
   * BOOL      CBEXPAPI  SalTblSetFocusCell(HWND, LONG, HWND, INT, INT); file: centura200.h */
  CBoolean SalTblSetFocusCell(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4,
      CNumber var5);

  /**
   * BOOL      CBEXPAPI  SalTblSetFocusRow(HWND, LONG); file: centura200.h */
  CBoolean SalTblSetFocusRow(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetLinesPerRow(HWND, INT); file: centura200.h */
  CBoolean SalTblSetLinesPerRow(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetLockedColumns(HWND, INT); file: centura200.h */
  CBoolean SalTblSetLockedColumns(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetRange(HWND, LONG, LONG); file: centura200.h */
  CBoolean SalTblSetRange(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * LONG      CBEXPAPI  SalTblSetRow(HWND, INT); file: centura200.h */
  CNumber SalTblSetRow(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTblSetRowFlags(HWND, LONG, WORD, BOOL); file: centura200.h */
  CBoolean SalTblSetRowFlags(CWndHandle var1, CNumber var2, CNumber var3, CBoolean var4);

  /**
   * BOOL      CBEXPAPI  SalTblSetTableFlags(HWND, WORD, BOOL); file: centura200.h */
  CBoolean SalTblSetTableFlags(CWndHandle var1, CNumber var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SalTblSortRows(HWND, WORD, INT); file: centura200.h */
  CBoolean SalTblSortRows(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTimerKill(HWND, INT); file: centura200.h */
  CBoolean SalTimerKill(CWndHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SalTimerSet(HWND, INT, WORD); file: centura200.h */
  CBoolean SalTimerSet(CWndHandle var1, CNumber var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalTrackPopupMenu(HWND, LPSTR, WORD, INT, INT); file: centura200.h */
  CBoolean SalTrackPopupMenu(CWndHandle var1, CString var2, CNumber var3, CNumber var4,
      CNumber var5);

  /**
   * HUDV CBEXPAPI SalUdvGetCurrentHandle(void); file: centura200.h */
  CHandle SalUdvGetCurrentHandle();

  /**
   * BOOL      CBEXPAPI  SalUpdateWindow(HWND); file: centura200.h */
  CBoolean SalUpdateWindow(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalUseRegistry(BOOL, LPSTR); file: centura200.h */
  CBoolean SalUseRegistry(CBoolean var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SalValidateSet(HWND, BOOL, LONG); file: centura200.h */
  CBoolean SalValidateSet(CWndHandle var1, CBoolean var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SalWaitCursor(BOOL); file: centura200.h */
  CBoolean SalWaitCursor(CBoolean var1);

  /**
   * BOOL      CBEXPAPI  SalWinHelp(HWND, LPSTR, WORD, DWORD, LPSTR); file: centura200.h */
  CBoolean SalWinHelp(CWndHandle var1, CString var2, CNumber var3, CNumber var4, CString var5);

  /**
   * HSTRING   CBEXPAPI  SalWindowClassName(HWND); file: centura200.h */
  CString SalWindowClassName(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalWindowGetProperty(HWND, LPSTR, LPHSTRING); file: centura200.h */
  CBoolean SalWindowGetProperty(CWndHandle var1, CString var2, CString.ByReference var3);

  /**
   * UINT      CBEXPAPI  SalWindowHandleToNumber(HWND); file: centura200.h */
  CNumber SalWindowHandleToNumber(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalWindowIsDerivedFromClass(HWND, TEMPLATE); file: centura200.h */
  CBoolean SalWindowIsDerivedFromClass(CWndHandle var1, CStruct var2);

  /**
   * BOOL      CBEXPAPI  SalYieldEnable(BOOL); file: centura200.h */
  CBoolean SalYieldEnable(CBoolean var1);

  /**
   * BOOL      CBEXPAPI  SalYieldQueryState(VOID); file: centura200.h */
  CBoolean SalYieldQueryState();

  /**
   * BOOL      CBEXPAPI  SalYieldStartMessages(HWND); file: centura200.h */
  CBoolean SalYieldStartMessages(CWndHandle var1);

  /**
   * BOOL      CBEXPAPI  SalYieldStopMessages(VOID); file: centura200.h */
  CBoolean SalYieldStopMessages();

  /**
   * BOOL      CBEXPAPI  SqlClearImmediate(VOID); file: centura200.h */
  CBoolean SqlClearImmediate();

  /**
   * BOOL      CBEXPAPI  SqlClose(SQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlClose(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlCloseAllSPResultSets(SQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlCloseAllSPResultSets(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlCommit(SQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlCommit(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlCommitSession(SESSIONHANDLENUMBER); file: centura200.h */
  CBoolean SqlCommitSession(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlConnect(LPSQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlConnect(CSQLHandle.ByReference var1);

  /**
   * BOOL      CBEXPAPI  SqlConnectTransaction(LPSQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlConnectTransaction(CSQLHandle.ByReference var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlCreateSession(LPSESSIONHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlCreateSession(CSQLHandle.ByReference var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlCreateStatement(SESSIONHANDLENUMBER, LPSQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlCreateStatement(CSQLHandle var1, CSQLHandle.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlDirectoryByName(LPSTR, HARRAY); file: centura200.h */
  CBoolean SqlDirectoryByName(CString var1, CArray<?> var2);

  /**
   * BOOL      CBEXPAPI  SqlDisconnect(LPSQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlDisconnect(CSQLHandle.ByReference var1);

  /**
   * BOOL      CBEXPAPI  SqlDropStoredCmd(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlDropStoredCmd(CSQLHandle var1, CString var2);

  /**
   * INT       CBEXPAPI  SqlError(SQLHANDLENUMBER); file: centura200.h */
  CNumber SqlError(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlErrorText(INT, INT, LPHSTRING, INT, LPINT); file: centura200.h */
  CBoolean SqlErrorText(CNumber var1, CNumber var2, CString.ByReference var3, CNumber var4,
      CNumber.ByReference var5);

  /**
   * BOOL      CBEXPAPI  SqlExecute(SQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlExecute(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlExecutionPlan(SQLHANDLENUMBER, LPHSTRING, INT); file: centura200.h */
  CBoolean SqlExecutionPlan(CSQLHandle var1, CString.ByReference var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SqlExists(LPSTR, LPINT); file: centura200.h */
  CBoolean SqlExists(CString var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlExtractArgs(WPARAM, LPARAM, LPSQLHANDLENUMBER, LPINT, LPINT); file: centura200.h */
  CBoolean SqlExtractArgs(CNumber var1, CNumber var2, CSQLHandle.ByReference var3,
      CNumber.ByReference var4, CNumber.ByReference var5);

  /**
   * BOOL      CBEXPAPI  SqlFetchNext(SQLHANDLENUMBER, LPINT); file: centura200.h */
  CBoolean SqlFetchNext(CSQLHandle var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlFetchPrevious(SQLHANDLENUMBER, LPINT); file: centura200.h */
  CBoolean SqlFetchPrevious(CSQLHandle var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlFetchRow(SQLHANDLENUMBER, LONG, LPINT); file: centura200.h */
  CBoolean SqlFetchRow(CSQLHandle var1, CNumber var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SqlFreeSession(LPSESSIONHANDLENUMBER); file: centura200.h */
  CBoolean SqlFreeSession(CSQLHandle.ByReference var1);

  /**
   * BOOL      CBEXPAPI  SqlGetCmdOrRowsetPtr(SQLHANDLENUMBER, BOOL, LPLONG); file: centura200.h */
  CBoolean SqlGetCmdOrRowsetPtr(CSQLHandle var1, CBoolean var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SqlGetDSOrSessionPtr(SESSIONHANDLENUMBER, BOOL, LPLONG); file: centura200.h */
  CBoolean SqlGetDSOrSessionPtr(CSQLHandle var1, CBoolean var2, CNumber.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SqlGetError(SQLHANDLENUMBER, LPLONG, LPHSTRING); file: centura200.h */
  CBoolean SqlGetError(CSQLHandle var1, CNumber.ByReference var2, CString.ByReference var3);

  /**
   * BOOL      CBEXPAPI  SqlGetErrorPosition(SQLHANDLENUMBER, LPINT); file: centura200.h */
  CBoolean SqlGetErrorPosition(CSQLHandle var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlGetErrorText(INT, LPHSTRING); file: centura200.h */
  CBoolean SqlGetErrorText(CNumber var1, CString.ByReference var2);

  /**
   * HSTRING   CBEXPAPI  SqlGetErrorTextX(INT); file: centura200.h */
  CString SqlGetErrorTextX(CNumber var1);

  /**
   * HSTRING   CBEXPAPI  SqlGetLastStatement(VOID); file: centura200.h */
  CString SqlGetLastStatement();

  /**
   * BOOL      CBEXPAPI  SqlGetModifiedRows(SQLHANDLENUMBER, LPLONG); file: centura200.h */
  CBoolean SqlGetModifiedRows(CSQLHandle var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlGetNextSPResultSet(SQLHANDLENUMBER, LPSTR, LPBOOL); file: centura200.h */
  CBoolean SqlGetNextSPResultSet(CSQLHandle var1, CString var2, CBoolean var3);

  /**
   * BOOL      CBEXPAPI  SqlGetParameter(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING); file: centura200.h */
  CBoolean SqlGetParameter(CSQLHandle var1, CNumber var2, CNumber.ByReference var3,
      CString.ByReference var4);

  /**
   * BOOL      CBEXPAPI  SqlGetParameterAll(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING, BOOL); file: centura200.h */
  CBoolean SqlGetParameterAll(CSQLHandle var1, CNumber var2, CNumber.ByReference var3,
      CString.ByReference var4, CBoolean var5);

  /**
   * BOOL      CBEXPAPI  SqlGetResultSetCount(SQLHANDLENUMBER, LPLONG); file: centura200.h */
  CBoolean SqlGetResultSetCount(CSQLHandle var1, CNumber.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlGetRollbackFlag(SQLHANDLENUMBER, LPBOOL); file: centura200.h */
  CBoolean SqlGetRollbackFlag(CSQLHandle var1, CBoolean var2);

  /**
   * BOOL      CBEXPAPI  SqlGetSessionErrorInfo(SESSIONHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING); file: centura200.h */
  CBoolean SqlGetSessionErrorInfo(CSQLHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CString.ByReference var4);

  /**
   * BOOL      CBEXPAPI  SqlGetSessionHandle(SQLHANDLENUMBER, LPSESSIONHANDLENUMBER); file: centura200.h */
  CBoolean SqlGetSessionHandle(CSQLHandle var1, CSQLHandle.ByReference var2);

  /**
   * BOOL      CBEXPAPI  SqlGetSessionParameter(SESSIONHANDLENUMBER, WORD, LPLONG, LPHSTRING); file: centura200.h */
  CBoolean SqlGetSessionParameter(CSQLHandle var1, CNumber var2, CNumber.ByReference var3,
      CString.ByReference var4);

  /**
   * BOOL      CBEXPAPI  SqlGetStatementErrorInfo(SQLHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING); file: centura200.h */
  CBoolean SqlGetStatementErrorInfo(CSQLHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CString.ByReference var4);

  /**
   * BOOL      CBEXPAPI  SqlImmediate(LPSTR); file: centura200.h */
  CBoolean SqlImmediate(CString var1);

  /**
   * BOOL      CBEXPAPI  SqlOpen(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlOpen(CSQLHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLExecute(SQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlOraPLSQLExecute(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLPrepare(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlOraPLSQLPrepare(CSQLHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLStringBindType(SQLHANDLENUMBER, LPSTR, INT); file: centura200.h */
  CBoolean SqlOraPLSQLStringBindType(CSQLHandle var1, CString var2, CNumber var3);

  /**
   * BOOL      CBEXPAPI  SqlPLSQLCommand(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlPLSQLCommand(CSQLHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlPrepare(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlPrepare(CSQLHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlPrepareAndExecute(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlPrepareAndExecute(CSQLHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlPrepareSP(SQLHANDLENUMBER, LPSTR, LPSTR); file: centura200.h */
  CBoolean SqlPrepareSP(CSQLHandle var1, CString var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SqlRetrieve(SQLHANDLENUMBER, LPSTR, LPSTR, LPSTR); file: centura200.h */
  CBoolean SqlRetrieve(CSQLHandle var1, CString var2, CString var3, CString var4);

  /**
   * BOOL      CBEXPAPI  SqlRollbackSession(SESSIONHANDLENUMBER); file: centura200.h */
  CBoolean SqlRollbackSession(CSQLHandle var1);

  /**
   * BOOL      CBEXPAPI  SqlSetInMessage(SQLHANDLENUMBER, INT); file: centura200.h */
  CBoolean SqlSetInMessage(CSQLHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SqlSetIsolationLevel(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  CBoolean SqlSetIsolationLevel(CSQLHandle var1, CString var2);

  /**
   * BOOL      CBEXPAPI  SqlSetLockTimeout(SQLHANDLENUMBER, INT); file: centura200.h */
  CBoolean SqlSetLockTimeout(CSQLHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SqlSetLongBindDatatype(INT, INT); file: centura200.h */
  CBoolean SqlSetLongBindDatatype(CNumber var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SqlSetOutMessage(SQLHANDLENUMBER, INT); file: centura200.h */
  CBoolean SqlSetOutMessage(CSQLHandle var1, CNumber var2);

  /**
   * BOOL      CBEXPAPI  SqlSetParameter(SQLHANDLENUMBER, WORD, LONG, HSTRING); file: centura200.h */
  CBoolean SqlSetParameter(CSQLHandle var1, CNumber var2, CNumber var3, CString var4);

  /**
   * BOOL      CBEXPAPI  SqlSetParameterAll(SQLHANDLENUMBER, WORD, LONG, HSTRING, BOOL); file: centura200.h */
  CBoolean SqlSetParameterAll(CSQLHandle var1, CNumber var2, CNumber var3, CString var4,
      CBoolean var5);

  /**
   * BOOL      CBEXPAPI  SqlSetResultSet(SQLHANDLENUMBER, BOOL); file: centura200.h */
  CBoolean SqlSetResultSet(CSQLHandle var1, CBoolean var2);

  /**
   * BOOL      CBEXPAPI  SqlSetSessionParameter(SESSIONHANDLENUMBER, WORD, LONG, HSTRING); file: centura200.h */
  CBoolean SqlSetSessionParameter(CSQLHandle var1, CNumber var2, CNumber var3, CString var4);

  /**
   * BOOL      CBEXPAPI  SqlStore(SQLHANDLENUMBER, LPSTR, LPSTR); file: centura200.h */
  CBoolean SqlStore(CSQLHandle var1, CString var2, CString var3);

  /**
   * BOOL      CBEXPAPI  SqlVarSetup(SQLHANDLENUMBER); file: centura200.h */
  CBoolean SqlVarSetup(CSQLHandle var1);
  
  CBoolean SalPause(CNumber var1);
}
