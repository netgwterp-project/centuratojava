package com.centura.api.cdk;

import com.centura.api.type.CWndHandle;

public abstract class AbstractGeneralWindowClass implements IGeneralWindowClass {
	
	private CWndHandle cWndHandle;
	
	public AbstractGeneralWindowClass() {
		HandleFactory.getNewHandle(this);
	}
	
	@Override
	public CWndHandle getWndHandle() {
		// TODO Auto-generated method stub
		return cWndHandle;
	}
	
	@Override
	public 	void setWndHandle(CWndHandle handle) {
		cWndHandle = handle;
	}
	

}
