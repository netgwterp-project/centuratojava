package com.centura.api.cdk;

import java.util.function.Function;

import com.centura.api.type.CNumber;

@FunctionalInterface
public interface IMessageHandle extends Function<IMessage, CNumber> {
	
	Integer getMESSAGE();
	@Override
	default CNumber apply(IMessage msg){
		return CNumber.NUMBER_Null;
	}
	
	default Integer getCLASSMESSAGE(){
		return getMESSAGE() + 1000000;
	}


}
