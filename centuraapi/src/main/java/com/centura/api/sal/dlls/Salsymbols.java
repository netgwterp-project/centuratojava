package com.centura.api.sal.dlls;

import com.centura.api.cdk.IFunctionalClass;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CNumber;
import com.centura.api.type.CSQLHandle;
import com.centura.api.type.CString;
import com.centura.api.type.CStruct;
import com.centura.api.type.CWndHandle;

public final class Salsymbols {
  public static final ISalsymbols isalsymbolsImpl = new DefaultSalsymbols();

  public static CNumber HSGetRef(CString.ByValue HSTRING) {
    return isalsymbolsImpl.HSGetRef(HSTRING);
  }

  public static CNumber HSNumStringsInUse() {
    return isalsymbolsImpl.HSNumStringsInUse();
  }

  public static CBoolean OraPLSQLExecute(CSQLHandle.ByValue HSQLHANDLE) {
    return isalsymbolsImpl.OraPLSQLExecute(HSQLHANDLE);
  }

  public static CBoolean OraPLSQLPrepare(CSQLHandle.ByValue HSQLHANDLE, CString.ByValue HSTRING) {
    return isalsymbolsImpl.OraPLSQLPrepare(HSQLHANDLE,HSTRING);
  }

  public static CBoolean OraPLSQLStringBindType(CSQLHandle.ByValue HSQLHANDLE,
      CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.OraPLSQLStringBindType(HSQLHANDLE,HSTRING,LONG);
  }

  public static CNumber SalAutoNameObject(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalAutoNameObject(LONG,LONG2);
  }

  public static CBoolean SalBIDIDlgChooseFontsIntern(CWndHandle.ByValue HWND,
      CString.ByReference HSTRING, CNumber.ByReference LPLONG, CString.ByReference HSTRING4,
      CNumber.ByReference LPLONG5, CNumber.ByReference LPLONG6, CNumber.ByReference LPLONG7,
      CNumber.ByReference LPLONG8, CNumber.ByReference LPLONG9) {
    return isalsymbolsImpl.SalBIDIDlgChooseFontsIntern(HWND,HSTRING,LPLONG,HSTRING4,LPLONG5,LPLONG6,LPLONG7,LPLONG8,LPLONG9);
  }

  public static CNumber SalCDKGetChildByName(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalCDKGetChildByName(LONG,LONG2,LONG3,HSTRING);
  }

  public static CNumber SalCDKGetChildTypeByName(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalCDKGetChildTypeByName(LONG,HSTRING);
  }

  public static CString SalCDKGetName(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalCDKGetName(HSTRING);
  }

  public static CNumber SalCDKGetValidChildren(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalCDKGetValidChildren(LONG,HSTRING);
  }

  public static CString SalCDKParseTitle(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalCDKParseTitle(HSTRING,LONG,LONG3,LPLONG);
  }

  public static CNumber SalChangeLineCountLock() {
    return isalsymbolsImpl.SalChangeLineCountLock();
  }

  public static CNumber SalChangeLineCountNotify(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalChangeLineCountNotify(LONG);
  }

  public static CNumber SalChangeLineCountUnlock(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalChangeLineCountUnlock(LONG);
  }

  public static CWndHandle SalCreateWindow(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalCreateWindow(TEMPLATE,HWND);
  }

  public static CWndHandle SalCreateWindowEx(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CNumber.ByValue LONG6,
      CNumber.ByValue LONG7) {
    return isalsymbolsImpl.SalCreateWindowEx(TEMPLATE,HWND,LONG,LONG4,LONG5,LONG6,LONG7);
  }

  public static CWndHandle SalCreateWindowExFromStr(CString.ByValue HSTRING,
      CWndHandle.ByValue HWND, CNumber.ByValue LONG, CNumber.ByValue LONG4, CNumber.ByValue LONG5,
      CNumber.ByValue LONG6, CNumber.ByValue LONG7) {
    return isalsymbolsImpl.SalCreateWindowExFromStr(HSTRING,HWND,LONG,LONG4,LONG5,LONG6,LONG7);
  }

  public static CWndHandle SalCreateWindowFromStr(CString.ByValue HSTRING,
      CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalCreateWindowFromStr(HSTRING,HWND);
  }

  public static CNumber SalCurrentLineNumber() {
    return isalsymbolsImpl.SalCurrentLineNumber();
  }

  public static CNumber SalDataGetFieldData(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalDataGetFieldData(HWND);
  }

  public static CWndHandle SalDataGetFieldWindow(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalDataGetFieldWindow(HWND,HSTRING);
  }

  public static CWndHandle SalDataGetSourceWindow(CWndHandle.ByValue HWND,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalDataGetSourceWindow(HWND,HSTRING);
  }

  public static CNumber SalDataQueryFieldExtent(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalDataQueryFieldExtent(HWND);
  }

  public static CNumber SalDataQueryFields(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalDataQueryFields(HWND,LONG,HSTRING);
  }

  public static CNumber SalDataQuerySources(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalDataQuerySources(HWND,LONG,HSTRING);
  }

  public static CNumber SalDialogOpenExisting() {
    return isalsymbolsImpl.SalDialogOpenExisting();
  }

  public static CNumber SalDisableAllWindows(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalDisableAllWindows(HWND);
  }

  public static CBoolean SalEditCanInsertObject() {
    return isalsymbolsImpl.SalEditCanInsertObject();
  }

  public static CBoolean SalEditCanPasteLink() {
    return isalsymbolsImpl.SalEditCanPasteLink();
  }

  public static CBoolean SalEditCanPasteSpecial() {
    return isalsymbolsImpl.SalEditCanPasteSpecial();
  }

  public static CBoolean SalEditInsertObject() {
    return isalsymbolsImpl.SalEditInsertObject();
  }

  public static CBoolean SalEditPasteLink() {
    return isalsymbolsImpl.SalEditPasteLink();
  }

  public static CBoolean SalEditPasteSpecial() {
    return isalsymbolsImpl.SalEditPasteSpecial();
  }

  public static CNumber SalEnableAllWindows(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalEnableAllWindows(LONG);
  }

  public static CBoolean SalFlashWindow(CWndHandle.ByValue HWND, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalFlashWindow(HWND,BOOL);
  }

  public static CBoolean SalFmtCopyProfile(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalFmtCopyProfile(HWND,LONG);
  }

  public static CBoolean SalFmtGetParmInt(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalFmtGetParmInt(HWND,LONG,LPLONG);
  }

  public static CBoolean SalFmtGetParmStr(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByReference HSTRING) {
    return isalsymbolsImpl.SalFmtGetParmStr(HWND,LONG,HSTRING);
  }

  public static CNumber SalFmtGetProfile(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalFmtGetProfile(HWND);
  }

  public static CBoolean SalFmtSetParmInt(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalFmtSetParmInt(HWND,LONG,LONG3);
  }

  public static CBoolean SalFmtSetParmStr(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalFmtSetParmStr(HWND,LONG,HSTRING);
  }

  public static CBoolean SalFmtSetProfile(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalFmtSetProfile(HWND,LONG);
  }

  public static CBoolean SalGetBuildSettings(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByReference LPLONG, CString.ByReference HSTRING4, CString.ByReference HSTRING5,
      CString.ByReference HSTRING6, CString.ByReference HSTRING7, CNumber.ByReference LPLONG8,
      CNumber.ByReference LPLONG9, CNumber.ByReference LPLONG10, CNumber.ByReference LPLONG11,
      CNumber.ByReference LPLONG12) {
    return isalsymbolsImpl.SalGetBuildSettings(LONG,HSTRING,LPLONG,HSTRING4,HSTRING5,HSTRING6,HSTRING7,LPLONG8,LPLONG9,LPLONG10,LPLONG11,LPLONG12);
  }

  public static CWndHandle SalGetCurrentDesignWindow(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalGetCurrentDesignWindow(LONG);
  }

  public static CNumber SalGetRegistryString(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CString.ByValue HSTRING3, CString.ByReference HSTRING4, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalGetRegistryString(HSTRING,HSTRING2,HSTRING3,HSTRING4,LONG);
  }

  public static CWndHandle SalGetWindowLabel(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalGetWindowLabel(HWND);
  }

  public static CBoolean SalIsOutlineSecondary(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalIsOutlineSecondary(LONG);
  }

  public static CNumber SalItemGetLineNumber(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalItemGetLineNumber(LONG);
  }

  public static CBoolean SalLoadAppAndProcessMsgs(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalLoadAppAndProcessMsgs(HSTRING,LONG,LPLONG);
  }

  public static CBoolean SalLog(CString.ByValue HSTRING, CString.ByValue HSTRING2) {
    return isalsymbolsImpl.SalLog(HSTRING,HSTRING2);
  }

  public static CBoolean SalLogLine(CString.ByValue HSTRING, CString.ByValue HSTRING2) {
    return isalsymbolsImpl.SalLogLine(HSTRING,HSTRING2);
  }

  public static CBoolean SalLogResources(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalLogResources(HSTRING);
  }

  public static CNumber SalModalDialog(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalModalDialog(TEMPLATE,HWND);
  }

  public static CNumber SalModalDialogFromStr(CString.ByValue HSTRING, CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalModalDialogFromStr(HSTRING,HWND);
  }

  public static CBoolean SalOLEAnyActive(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOLEAnyActive(HWND);
  }

  public static CBoolean SalOLEAnyLinked() {
    return isalsymbolsImpl.SalOLEAnyLinked();
  }

  public static CBoolean SalOLEDoVerb(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOLEDoVerb(HWND,HSTRING);
  }

  public static CBoolean SalOLEFileInsert(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOLEFileInsert(HWND,HSTRING);
  }

  public static CNumber SalOLEGetServers(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOLEGetServers(HSTRING);
  }

  public static CNumber SalOLEGetVerbs(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOLEGetVerbs(HWND,HSTRING);
  }

  public static CBoolean SalOLELinkProperties(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOLELinkProperties(HWND);
  }

  public static CBoolean SalOLEServerInsert(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOLEServerInsert(HWND,HSTRING);
  }

  public static CBoolean SalOLEUpdateActive(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOLEUpdateActive(HWND);
  }

  public static CBoolean SalOutlineActivateUIView(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineActivateUIView(LONG,HSTRING);
  }

  public static CBoolean SalOutlineAddNotifyWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOutlineAddNotifyWindow(LONG,HWND);
  }

  public static CBoolean SalOutlineAlignWindows(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineAlignWindows(HWND,LONG);
  }

  public static CNumber SalOutlineBaseClassPropEditor(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineBaseClassPropEditor(LONG,LONG2,HWND,HSTRING);
  }

  public static CBoolean SalOutlineBlockNotifications(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineBlockNotifications(LONG,BOOL);
  }

  public static CBoolean SalOutlineCanAlignWindows(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineCanAlignWindows(HWND,LONG);
  }

  public static CNumber SalOutlineCanDoUICommand(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineCanDoUICommand(LONG,LONG2,LONG3,HSTRING);
  }

  public static CBoolean SalOutlineCanInsert(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineCanInsert(LONG,LONG2,LONG3,BOOL);
  }

  public static CBoolean SalOutlineCanMoveToBack(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineCanMoveToBack(LONG);
  }

  public static CBoolean SalOutlineCanMoveToFront(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineCanMoveToFront(LONG);
  }

  public static CBoolean SalOutlineChangeSelect(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL, CBoolean.ByValue BOOL4, CBoolean.ByValue BOOL5) {
    return isalsymbolsImpl.SalOutlineChangeSelect(LONG,LONG2,BOOL,BOOL4,BOOL5);
  }

  public static CNumber SalOutlineChildOfType(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineChildOfType(LONG,LONG2,LONG3,BOOL);
  }

  public static CNumber SalOutlineClassNameOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByReference HSTRING) {
    return isalsymbolsImpl.SalOutlineClassNameOfItem(LONG,LONG2,HSTRING);
  }

  public static CBoolean SalOutlineClearCustomCmds(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineClearCustomCmds(LONG);
  }

  public static CBoolean SalOutlineCloseDesignEdits(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineCloseDesignEdits(LONG,BOOL);
  }

  public static CNumber SalOutlineCopyItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CBoolean.ByValue BOOL,
      CBoolean.ByValue BOOL7) {
    return isalsymbolsImpl.SalOutlineCopyItem(LONG,LONG2,LONG3,LONG4,LONG5,BOOL,BOOL7);
  }

  public static CNumber SalOutlineCreateClassFromObject(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineCreateClassFromObject(LONG,LONG2,HSTRING,BOOL);
  }

  public static CWndHandle SalOutlineCreateDesignWindow(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND, CWndHandle.ByValue HWND4) {
    return isalsymbolsImpl.SalOutlineCreateDesignWindow(LONG,LONG2,HWND,HWND4);
  }

  public static CNumber SalOutlineCreateItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineCreateItem(LONG,LONG2);
  }

  public static CNumber SalOutlineCurrent() {
    return isalsymbolsImpl.SalOutlineCurrent();
  }

  public static CBoolean SalOutlineCustomizeItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOutlineCustomizeItem(LONG,LONG2,HWND);
  }

  public static CBoolean SalOutlineDeleteItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineDeleteItem(LONG,LONG2);
  }

  public static CNumber SalOutlineDoUICommand(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineDoUICommand(LONG,LONG2,LONG3,HSTRING);
  }

  public static CBoolean SalOutlineDontCustomizeType(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalOutlineDontCustomizeType(LONG,LONG2,LONG3);
  }

  public static CNumber SalOutlineEditGetItem(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineEditGetItem(LONG);
  }

  public static CNumber SalOutlineEditGetText(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4) {
    return isalsymbolsImpl.SalOutlineEditGetText(LONG,HSTRING,LPLONG,LPLONG4);
  }

  public static CBoolean SalOutlineEditSetText(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineEditSetText(LONG,HSTRING,BOOL);
  }

  public static CBoolean SalOutlineEnableDesignScaling(CNumber.ByValue LONG, CBoolean.ByValue BOOL,
      CString.ByValue HSTRING, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalOutlineEnableDesignScaling(LONG,BOOL,HSTRING,LONG4);
  }

  public static CNumber SalOutlineEnumItemProps(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineEnumItemProps(LONG,LONG2,HSTRING);
  }

  public static CBoolean SalOutlineEnumSymbols(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CBoolean.ByValue BOOL,
      CNumber.ByValue LONG7, CNumber.ByValue LONG8) {
    return isalsymbolsImpl.SalOutlineEnumSymbols(LONG,LONG2,LONG3,LONG4,LONG5,BOOL,LONG7,LONG8);
  }

  public static CNumber SalOutlineEnumWindowProps(CWndHandle.ByValue HWND,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineEnumWindowProps(HWND,HSTRING);
  }

  public static CNumber SalOutlineFindTemplateOfClass(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineFindTemplateOfClass(LONG,LONG2);
  }

  public static CNumber SalOutlineFirstChild(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineFirstChild(LONG,LONG2);
  }

  public static CNumber SalOutlineFirstDisplayedChild(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineFirstDisplayedChild(LONG,LONG2);
  }

  public static CNumber SalOutlineFirstMarked(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineFirstMarked(LONG);
  }

  public static CBoolean SalOutlineGetAppChanged(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineGetAppChanged(LONG);
  }

  public static CNumber SalOutlineGetAttributes(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalOutlineGetAttributes(LONG,LONG2,LONG3);
  }

  public static CBoolean SalOutlineGetDTData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalOutlineGetDTData(LONG,LONG2,LONG3,LPLONG);
  }

  public static CNumber SalOutlineGetDrawTool(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineGetDrawTool(LONG);
  }

  public static CNumber SalOutlineGetFileName(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineGetFileName(LONG,HSTRING,BOOL);
  }

  public static CNumber SalOutlineGetFirstUISelection(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineGetFirstUISelection(LONG);
  }

  public static CBoolean SalOutlineGetFormFlags(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineGetFormFlags(HWND,LONG);
  }

  public static CNumber SalOutlineGetFunParams(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByReference HSTRING4, CString.ByReference HSTRING5) {
    return isalsymbolsImpl.SalOutlineGetFunParams(LONG,LONG2,HSTRING,HSTRING4,HSTRING5);
  }

  public static CNumber SalOutlineGetIncludingItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineGetIncludingItem(LONG,LONG2);
  }

  public static CNumber SalOutlineGetItemFlags(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineGetItemFlags(LONG,LONG2);
  }

  public static CNumber SalOutlineGetNextUISelection(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineGetNextUISelection(LONG,LONG2);
  }

  public static CBoolean SalOutlineGetNotifyItem(CNumber.ByValue LONG, CNumber.ByReference LPLONG,
      CNumber.ByReference LPLONG3, CNumber.ByReference LPLONG4) {
    return isalsymbolsImpl.SalOutlineGetNotifyItem(LONG,LPLONG,LPLONG3,LPLONG4);
  }

  public static CNumber SalOutlineGetOneUISelection(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineGetOneUISelection(LONG);
  }

  public static CNumber SalOutlineGetUIViewInfo(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4, CNumber.ByReference LPLONG5) {
    return isalsymbolsImpl.SalOutlineGetUIViewInfo(LONG,HSTRING,LPLONG,LPLONG4,LPLONG5);
  }

  public static CBoolean SalOutlineHighlightItem(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalOutlineHighlightItem(HWND,LONG,LONG3,LONG4);
  }

  public static CBoolean SalOutlineInfo(CString.ByValue HSTRING, CBoolean.ByReference LPBOOL,
      CBoolean.ByReference LPBOOL3, CBoolean.ByReference LPBOOL4, CBoolean.ByReference LPBOOL5,
      CBoolean.ByReference LPBOOL6, CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalOutlineInfo(HSTRING,LPBOOL,LPBOOL3,LPBOOL4,LPBOOL5,LPBOOL6,LPLONG);
  }

  public static CBoolean SalOutlineInheritFromBaseClasses(CNumber.ByValue LONG,
      CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineInheritFromBaseClasses(LONG,LONG2);
  }

  public static CNumber SalOutlineInsertItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalOutlineInsertItem(LONG,LONG2,LONG3,LONG4);
  }

  public static CBoolean SalOutlineIsBreak(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineIsBreak(LONG,LONG2);
  }

  public static CBoolean SalOutlineIsClassChildRef(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineIsClassChildRef(LONG,LONG2);
  }

  public static CBoolean SalOutlineIsClassObject(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineIsClassObject(LONG,LONG2);
  }

  public static CBoolean SalOutlineIsCompiled(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineIsCompiled(LONG);
  }

  public static CBoolean SalOutlineIsDrawToolLocked(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineIsDrawToolLocked(LONG);
  }

  public static CBoolean SalOutlineIsItemMarked(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineIsItemMarked(LONG,LONG2);
  }

  public static CBoolean SalOutlineIsTemplateOfClass(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineIsTemplateOfClass(LONG,LONG2);
  }

  public static CBoolean SalOutlineIsUIViewActive(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineIsUIViewActive(LONG,HSTRING);
  }

  public static CNumber SalOutlineIsUserWindow(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineIsUserWindow(LONG);
  }

  public static CBoolean SalOutlineIsWindowItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineIsWindowItem(LONG,LONG2);
  }

  public static CBoolean SalOutlineItemChangeUpdate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalOutlineItemChangeUpdate(LONG,LONG2,LONG3);
  }

  public static CNumber SalOutlineItemGetBlob(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalOutlineItemGetBlob(LONG,LONG2,LPLONG);
  }

  public static CBoolean SalOutlineItemGetProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByReference HSTRING4) {
    return isalsymbolsImpl.SalOutlineItemGetProperty(LONG,LONG2,HSTRING,HSTRING4);
  }

  public static CBoolean SalOutlineItemGetPropertyBuffer(CNumber.ByValue LONG,
      CNumber.ByValue LONG2, CString.ByValue HSTRING, CNumber.ByValue LONG4,
      CNumber.ByValue LONG5) {
    return isalsymbolsImpl.SalOutlineItemGetPropertyBuffer(LONG,LONG2,HSTRING,LONG4,LONG5);
  }

  public static CBoolean SalOutlineItemIsIncluded(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineItemIsIncluded(LONG,LONG2);
  }

  public static CNumber SalOutlineItemLevel(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineItemLevel(LONG,LONG2);
  }

  public static CNumber SalOutlineItemLineCount(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineItemLineCount(LONG,LONG2);
  }

  public static CNumber SalOutlineItemOfWindow(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOutlineItemOfWindow(HWND);
  }

  public static CNumber SalOutlineItemOfWindowIndirect(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOutlineItemOfWindowIndirect(HWND);
  }

  public static CBoolean SalOutlineItemRemoveProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineItemRemoveProperty(LONG,LONG2,HSTRING);
  }

  public static CBoolean SalOutlineItemSetBlob(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByReference HSTRING, CString.ByReference HSTRING4, CNumber.ByValue LONG5) {
    return isalsymbolsImpl.SalOutlineItemSetBlob(LONG,LONG2,HSTRING,HSTRING4,LONG5);
  }

  public static CBoolean SalOutlineItemSetProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByValue HSTRING4, CNumber.ByValue LONG5) {
    return isalsymbolsImpl.SalOutlineItemSetProperty(LONG,LONG2,HSTRING,HSTRING4,LONG5);
  }

  public static CBoolean SalOutlineItemText(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL, CString.ByReference HSTRING) {
    return isalsymbolsImpl.SalOutlineItemText(LONG,LONG2,BOOL,HSTRING);
  }

  public static CString SalOutlineItemTextX(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineItemTextX(LONG,LONG2,BOOL);
  }

  public static CBoolean SalOutlineItemTitle(CNumber.ByValue LONG, CString.ByReference HSTRING) {
    return isalsymbolsImpl.SalOutlineItemTitle(LONG,HSTRING);
  }

  public static CString SalOutlineItemTitleX(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineItemTitleX(LONG);
  }

  public static CBoolean SalOutlineItemToTagged(CNumber.ByValue LONG, CStruct.ByValue TEMPLATE,
      CStruct.ByValue TEMPLATE3) {
    return isalsymbolsImpl.SalOutlineItemToTagged(LONG,TEMPLATE,TEMPLATE3);
  }

  public static CNumber SalOutlineItemType(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineItemType(LONG,LONG2);
  }

  public static CBoolean SalOutlineItemTypeText(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL, CString.ByReference HSTRING) {
    return isalsymbolsImpl.SalOutlineItemTypeText(LONG,LONG2,LONG3,BOOL,HSTRING);
  }

  public static CNumber SalOutlineLastChild(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineLastChild(LONG,LONG2);
  }

  public static CNumber SalOutlineLoad(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineLoad(HSTRING);
  }

  public static CBoolean SalOutlineLockDrawTool(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineLockDrawTool(LONG,BOOL);
  }

  public static CBoolean SalOutlineMergeIncludes(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineMergeIncludes(LONG);
  }

  public static CBoolean SalOutlineMoveToBack(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineMoveToBack(LONG);
  }

  public static CBoolean SalOutlineMoveToFront(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineMoveToFront(LONG);
  }

  public static CNumber SalOutlineNextDisplayedSibling(CNumber.ByValue LONG,
      CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineNextDisplayedSibling(LONG,LONG2);
  }

  public static CNumber SalOutlineNextLikeItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING, CNumber.ByValue LONG5,
      CNumber.ByValue LONG6) {
    return isalsymbolsImpl.SalOutlineNextLikeItem(LONG,LONG2,LONG3,HSTRING,LONG5,LONG6);
  }

  public static CNumber SalOutlineNextLine(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineNextLine(LONG,LONG2);
  }

  public static CNumber SalOutlineNextMarked(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineNextMarked(LONG,LONG2);
  }

  public static CNumber SalOutlineNextSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineNextSibling(LONG,LONG2);
  }

  public static CBoolean SalOutlineNotify(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalOutlineNotify(LONG,LONG2,LONG3);
  }

  public static CWndHandle SalOutlineOrderTabs(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineOrderTabs(HWND,HSTRING);
  }

  public static CNumber SalOutlineOutlineOfUIFrame(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOutlineOutlineOfUIFrame(HWND);
  }

  public static CNumber SalOutlineOutlineOfWindow(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOutlineOutlineOfWindow(HWND);
  }

  public static CNumber SalOutlineParent(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineParent(LONG,LONG2);
  }

  public static CNumber SalOutlinePreviousSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlinePreviousSibling(LONG,LONG2);
  }

  public static CBoolean SalOutlinePropertyChangeUpdate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4, CNumber.ByValue LONG5) {
    return isalsymbolsImpl.SalOutlinePropertyChangeUpdate(LONG,LONG2,LONG3,LONG4,LONG5);
  }

  public static CBoolean SalOutlineQueryUserMode(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineQueryUserMode(LONG);
  }

  public static CBoolean SalOutlineRefreshInclude(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineRefreshInclude(LONG,LONG2);
  }

  public static CBoolean SalOutlineRemoveNotifyWindow(CNumber.ByValue LONG,
      CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalOutlineRemoveNotifyWindow(LONG,HWND);
  }

  public static CBoolean SalOutlineReportError(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByValue HSTRING4) {
    return isalsymbolsImpl.SalOutlineReportError(LONG,LONG2,HSTRING,HSTRING4);
  }

  public static CBoolean SalOutlineSave(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineSave(LONG,HSTRING);
  }

  public static CBoolean SalOutlineSaveAsText(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineSaveAsText(LONG,HSTRING,BOOL);
  }

  public static CNumber SalOutlineSelectNewItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineSelectNewItem(LONG,LONG2);
  }

  public static CBoolean SalOutlineSetAppChanged(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineSetAppChanged(LONG);
  }

  public static CBoolean SalOutlineSetAppUncompiled(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineSetAppUncompiled(LONG);
  }

  public static CBoolean SalOutlineSetBreak(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineSetBreak(LONG,LONG2,BOOL);
  }

  public static CBoolean SalOutlineSetCustomCmd(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineSetCustomCmd(LONG,LONG2,HSTRING);
  }

  public static CBoolean SalOutlineSetDTData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalOutlineSetDTData(LONG,LONG2,LONG3,LONG4);
  }

  public static CBoolean SalOutlineSetData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalOutlineSetData(LONG,LONG2,HSTRING,LONG4);
  }

  public static CBoolean SalOutlineSetDrawTool(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineSetDrawTool(LONG,LONG2);
  }

  public static CBoolean SalOutlineSetFormFlags(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineSetFormFlags(HWND,LONG,BOOL);
  }

  public static CBoolean SalOutlineSetItemClassObject(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalOutlineSetItemClassObject(LONG,LONG2,HSTRING);
  }

  public static CBoolean SalOutlineSetItemFlags(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineSetItemFlags(LONG,LONG2,LONG3,BOOL);
  }

  public static CNumber SalOutlineSetOutlineHook(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineSetOutlineHook(LONG);
  }

  public static CBoolean SalOutlineSetTypeData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING, CNumber.ByValue LONG5) {
    return isalsymbolsImpl.SalOutlineSetTypeData(LONG,LONG2,LONG3,HSTRING,LONG5);
  }

  public static CBoolean SalOutlineShare(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineShare(LONG,BOOL);
  }

  public static CBoolean SalOutlineShowDesignWindow(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalOutlineShowDesignWindow(LONG,LONG2,BOOL);
  }

  public static CNumber SalOutlineTop(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineTop(LONG);
  }

  public static CWndHandle SalOutlineUIFrameOfOutline(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineUIFrameOfOutline(LONG);
  }

  public static CBoolean SalOutlineUnload(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineUnload(LONG);
  }

  public static CNumber SalOutlineUnlockBlob(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalOutlineUnlockBlob(LONG);
  }

  public static CNumber SalOutlineWindowItemOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineWindowItemOfItem(LONG,LONG2);
  }

  public static CWndHandle SalOutlineWindowOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineWindowOfItem(LONG,LONG2);
  }

  public static CWndHandle SalOutlineWindowOfItemIndirect(CNumber.ByValue LONG,
      CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalOutlineWindowOfItemIndirect(LONG,LONG2);
  }

  public static CBoolean SalParseStatement(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalParseStatement(HSTRING,HSTRING2,LPLONG);
  }

  public static CBoolean SalProfileRegisterWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalProfileRegisterWindow(LONG,HWND,LONG3,LONG4);
  }

  public static CBoolean SalProfileUnregisterWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalProfileUnregisterWindow(LONG,HWND);
  }

  public static CNumber SalQOConnect(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CString.ByValue HSTRING4, CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalQOConnect(LONG,HSTRING,HSTRING3,HSTRING4,LPLONG);
  }

  public static CNumber SalQODisplayConnectError(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalQODisplayConnectError(HSTRING,LONG);
  }

  public static CString SalQOGetColumnInfoFromIndex(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalQOGetColumnInfoFromIndex(LONG,LONG2);
  }

  public static CNumber SalQOGetConnectInfo(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CString.ByReference HSTRING3, CString.ByReference HSTRING4) {
    return isalsymbolsImpl.SalQOGetConnectInfo(LONG,HSTRING,HSTRING3,HSTRING4);
  }

  public static CString SalQOGetTableInfoFromIndex(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.SalQOGetTableInfoFromIndex(LONG,LONG2);
  }

  public static CNumber SalQOLoadCatalogTables(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalQOLoadCatalogTables(LONG);
  }

  public static CNumber SalQOLoadTableColumns(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CString.ByValue HSTRING4) {
    return isalsymbolsImpl.SalQOLoadTableColumns(LONG,HSTRING,HSTRING3,HSTRING4);
  }

  public static CNumber SalQOReleaseColumnDefs(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalQOReleaseColumnDefs(LONG);
  }

  public static CNumber SalQOReleaseTableDefs(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalQOReleaseTableDefs(LONG);
  }

  public static CWndHandle SalReportGetDateTimeVar363Boolean(CString.ByValue HSTRING,
      CDateTime.ByReference LPDATETIME) {
    return isalsymbolsImpl.SalReportGetDateTimeVar363Boolean(HSTRING,LPDATETIME);
  }

  public static CBoolean SalReportSetPrintParameter(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalReportSetPrintParameter(HWND,LONG,LONG3);
  }

  public static CStruct SalResId(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalResId(HSTRING);
  }

  public static CBoolean SalResLoad(CStruct.ByValue TEMPLATE, CString.ByReference HSTRING) {
    return isalsymbolsImpl.SalResLoad(TEMPLATE,HSTRING);
  }

  public static CBoolean SalResourceGet(CString.ByReference HSTRING, CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.SalResourceGet(HSTRING,LPLONG);
  }

  public static CBoolean SalResourceSet(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalResourceSet(HSTRING,LONG);
  }

  public static CBoolean SalSetBuildSettings(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByValue LONG3, CString.ByValue HSTRING4, CString.ByValue HSTRING5,
      CString.ByValue HSTRING6, CString.ByValue HSTRING7, CNumber.ByValue LONG8,
      CNumber.ByValue LONG9, CNumber.ByValue LONG10, CNumber.ByValue LONG11,
      CNumber.ByValue LONG12) {
    return isalsymbolsImpl.SalSetBuildSettings(LONG,HSTRING,LONG3,HSTRING4,HSTRING5,HSTRING6,HSTRING7,LONG8,LONG9,LONG10,LONG11,LONG12);
  }

  public static CBoolean SalSetRegistryString(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CString.ByValue HSTRING3) {
    return isalsymbolsImpl.SalSetRegistryString(HSTRING,HSTRING2,HSTRING3);
  }

  public static CNumber SalShowToolBoxWindow(CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.SalShowToolBoxWindow(BOOL);
  }

  public static CNumber SalStaticFirst(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalStaticFirst(HWND);
  }

  public static CNumber SalStaticGetItem(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalStaticGetItem(HWND,LONG);
  }

  public static CNumber SalStaticGetLabel(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalStaticGetLabel(HWND);
  }

  public static CBoolean SalStaticGetLoc(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4) {
    return isalsymbolsImpl.SalStaticGetLoc(HWND,LONG,LPLONG,LPLONG4);
  }

  public static CBoolean SalStaticGetSize(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4) {
    return isalsymbolsImpl.SalStaticGetSize(HWND,LONG,LPLONG,LPLONG4);
  }

  public static CBoolean SalStaticHide(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalStaticHide(HWND,LONG);
  }

  public static CBoolean SalStaticIsVisible(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalStaticIsVisible(HWND,LONG);
  }

  public static CNumber SalStaticNext(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalStaticNext(HWND,LONG);
  }

  public static CBoolean SalStaticSetLoc(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalStaticSetLoc(HWND,LONG,LONG3,LONG4);
  }

  public static CBoolean SalStaticSetSize(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return isalsymbolsImpl.SalStaticSetSize(HWND,LONG,LONG3,LONG4);
  }

  public static CBoolean SalStaticShow(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalStaticShow(HWND,LONG);
  }

  public static CNumber SalStrFull(CString.ByValue HSTRING, CString.ByReference HSTRING2) {
    return isalsymbolsImpl.SalStrFull(HSTRING,HSTRING2);
  }

  public static CString SalStrGetIdentifier(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalStrGetIdentifier(HSTRING);
  }

  public static CBoolean SalStrGetIdentifierInt(CString.ByValue HSTRING,
      CString.ByReference HSTRING2, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalStrGetIdentifierInt(HSTRING,HSTRING2,LONG);
  }

  public static CNumber SalStrHalf(CString.ByValue HSTRING, CString.ByReference HSTRING2) {
    return isalsymbolsImpl.SalStrHalf(HSTRING,HSTRING2);
  }

  public static CBoolean SalStrIsValidDecimal(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalStrIsValidDecimal(HSTRING,LONG,LONG3);
  }

  public static CBoolean SalStrIsValidIdentifierName(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalStrIsValidIdentifierName(HSTRING);
  }

  public static CNumber SalStrLeftB(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CString.ByReference HSTRING3) {
    return isalsymbolsImpl.SalStrLeftB(HSTRING,LONG,HSTRING3);
  }

  public static CNumber SalStrMidB(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CString.ByReference HSTRING4) {
    return isalsymbolsImpl.SalStrMidB(HSTRING,LONG,LONG3,HSTRING4);
  }

  public static CNumber SalStrReplaceB(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CString.ByValue HSTRING4, CString.ByReference HSTRING5) {
    return isalsymbolsImpl.SalStrReplaceB(HSTRING,LONG,LONG3,HSTRING4,HSTRING5);
  }

  public static CNumber SalStrRightB(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CString.ByReference HSTRING3) {
    return isalsymbolsImpl.SalStrRightB(HSTRING,LONG,HSTRING3);
  }

  public static CNumber SalStringLengthB(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalStringLengthB(HSTRING);
  }

  public static CBoolean SalTblPaintRows(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.SalTblPaintRows(HWND,LONG,LONG3);
  }

  public static CNumber SalTblQueryView(CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SalTblQueryView(HWND);
  }

  public static CBoolean SalTblSetView(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalTblSetView(HWND,LONG);
  }

  public static CBoolean SalWinDebugBreak() {
    return isalsymbolsImpl.SalWinDebugBreak();
  }

  public static CBoolean SalWindowRemoveProperty(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SalWindowRemoveProperty(HWND,HSTRING);
  }

  public static CBoolean SalWindowSetProperty(CWndHandle.ByValue HWND, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SalWindowSetProperty(HWND,HSTRING,HSTRING3,LONG);
  }

  public static CBoolean SqlConnectUsingCursor(CSQLHandle.ByReference LPHSQLHANDLE,
      CNumber.ByValue LONG) {
    return isalsymbolsImpl.SqlConnectUsingCursor(LPHSQLHANDLE,LONG);
  }

  public static CBoolean SqlContextClear(CSQLHandle.ByValue HSQLHANDLE) {
    return isalsymbolsImpl.SqlContextClear(HSQLHANDLE);
  }

  public static CBoolean SqlContextSet(CSQLHandle.ByValue HSQLHANDLE) {
    return isalsymbolsImpl.SqlContextSet(HSQLHANDLE);
  }

  public static CBoolean SqlContextSetToForm(CSQLHandle.ByValue HSQLHANDLE,
      CWndHandle.ByValue HWND) {
    return isalsymbolsImpl.SqlContextSetToForm(HSQLHANDLE,HWND);
  }

  public static CBoolean SqlDisconnectWithoutCursor(CSQLHandle.ByValue HSQLHANDLE) {
    return isalsymbolsImpl.SqlDisconnectWithoutCursor(HSQLHANDLE);
  }

  public static CNumber SqlGetCursor(CSQLHandle.ByValue HSQLHANDLE) {
    return isalsymbolsImpl.SqlGetCursor(HSQLHANDLE);
  }

  public static CSQLHandle SqlGetSqlHandle(CNumber.ByValue LONG) {
    return isalsymbolsImpl.SqlGetSqlHandle(LONG);
  }

  public static CBoolean SqlHandleSetParameters(CSQLHandle.ByValue HSQLHANDLE) {
    return isalsymbolsImpl.SqlHandleSetParameters(HSQLHANDLE);
  }

  public static CBoolean SqlImmediateContext(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SqlImmediateContext(HSTRING);
  }

  public static CWndHandle SqlPLSQLExecute(CString.ByValue HSTRING) {
    return isalsymbolsImpl.SqlPLSQLExecute(HSTRING);
  }

  public static CBoolean SqlSetDSOrSessionPtr(CSQLHandle.ByReference LPHSESSIONHANDLE,
      CBoolean.ByValue BOOL, CNumber.ByValue LONG) {
    return isalsymbolsImpl.SqlSetDSOrSessionPtr(LPHSESSIONHANDLE,BOOL,LONG);
  }

  public static CBoolean SqlXDirectory(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return isalsymbolsImpl.SqlXDirectory(LONG,HSTRING);
  }

  public static CNumber __ActiveXTypeVariant() {
    return isalsymbolsImpl.__ActiveXTypeVariant();
  }

  public static CBoolean __AsActiveXVariant(CNumber.ByValue LONG) {
    return isalsymbolsImpl.__AsActiveXVariant(LONG);
  }

  public static CBoolean __CoClassCreateObject() {
    return isalsymbolsImpl.__CoClassCreateObject();
  }

  public static CBoolean __CoClassCreateObjectEx(CString.ByValue HSTRING) {
    return isalsymbolsImpl.__CoClassCreateObjectEx(HSTRING);
  }

  public static CBoolean __CoClassGetInterface(IFunctionalClass HUDV, CString.ByValue HSTRING) {
    return isalsymbolsImpl.__CoClassGetInterface(HUDV,HSTRING);
  }

  public static CBoolean __CoClassRelease() {
    return isalsymbolsImpl.__CoClassRelease();
  }

  public static CNumber __EnumCount() {
    return isalsymbolsImpl.__EnumCount();
  }

  public static CBoolean __EnumIsCollection() {
    return isalsymbolsImpl.__EnumIsCollection();
  }

  public static CBoolean __EnumNext(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__EnumNext(HUDV);
  }

  public static CBoolean __EnumReset() {
    return isalsymbolsImpl.__EnumReset();
  }

  public static CBoolean __EnumSkip(CNumber.ByValue LONG) {
    return isalsymbolsImpl.__EnumSkip(LONG);
  }

  public static CBoolean __GetBlobVariant(CString.ByReference HSTRING) {
    return isalsymbolsImpl.__GetBlobVariant(HSTRING);
  }

  public static CBoolean __GetBooleanVariant(CBoolean.ByReference LPBOOL) {
    return isalsymbolsImpl.__GetBooleanVariant(LPBOOL);
  }

  public static CBoolean __GetDateVariant(CDateTime.ByReference LPDATETIME) {
    return isalsymbolsImpl.__GetDateVariant(LPDATETIME);
  }

  public static CBoolean __GetLastError(CNumber.ByReference LPLONG, CString.ByReference HSTRING,
      CString.ByReference HSTRING3, CString.ByReference HSTRING4, CNumber.ByReference LPLONG5,
      CNumber.ByReference LPLONG6) {
    return isalsymbolsImpl.__GetLastError(LPLONG,HSTRING,HSTRING3,HSTRING4,LPLONG5,LPLONG6);
  }

  public static CBoolean __GetNumberVariant(CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.__GetNumberVariant(LPLONG);
  }

  public static CBoolean __GetObjectVariant(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__GetObjectVariant(HUDV);
  }

  public static CBoolean __GetSafeArrayVariant(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__GetSafeArrayVariant(HUDV);
  }

  public static CBoolean __GetStringVariant(CString.ByReference HSTRING) {
    return isalsymbolsImpl.__GetStringVariant(HSTRING);
  }

  public static CNumber __MakeOptionalVariant() {
    return isalsymbolsImpl.__MakeOptionalVariant();
  }

  public static CBoolean __ObjectAttach(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectAttach(HUDV);
  }

  public static CBoolean __ObjectCreateObject(CString.ByValue HSTRING) {
    return isalsymbolsImpl.__ObjectCreateObject(HSTRING);
  }

  public static CBoolean __ObjectCreateObjectEx(CString.ByValue HSTRING, CString.ByValue HSTRING2) {
    return isalsymbolsImpl.__ObjectCreateObjectEx(HSTRING,HSTRING2);
  }

  public static CBoolean __ObjectDetach() {
    return isalsymbolsImpl.__ObjectDetach();
  }

  public static CBoolean __ObjectFlushArgs() {
    return isalsymbolsImpl.__ObjectFlushArgs();
  }

  public static CBoolean __ObjectGetInterface(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectGetInterface(HUDV);
  }

  public static CBoolean __ObjectInvoke(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__ObjectInvoke(HSTRING,LONG);
  }

  public static CBoolean __ObjectInvokeID(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.__ObjectInvokeID(LONG,LONG2);
  }

  public static CBoolean __ObjectIsValid() {
    return isalsymbolsImpl.__ObjectIsValid();
  }

  public static CBoolean __ObjectPopBoolean(CNumber.ByValue LONG, CBoolean.ByReference LPBOOL) {
    return isalsymbolsImpl.__ObjectPopBoolean(LONG,LPBOOL);
  }

  public static CBoolean __ObjectPopDate(CNumber.ByValue LONG, CDateTime.ByReference LPDATETIME) {
    return isalsymbolsImpl.__ObjectPopDate(LONG,LPDATETIME);
  }

  public static CBoolean __ObjectPopNumber(CNumber.ByValue LONG, CNumber.ByReference LPLONG) {
    return isalsymbolsImpl.__ObjectPopNumber(LONG,LPLONG);
  }

  public static CBoolean __ObjectPopObject(CNumber.ByValue LONG, IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPopObject(LONG,HUDV);
  }

  public static CBoolean __ObjectPopSafeArray(CNumber.ByValue LONG, IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPopSafeArray(LONG,HUDV);
  }

  public static CBoolean __ObjectPopString(CNumber.ByValue LONG, CString.ByReference HSTRING) {
    return isalsymbolsImpl.__ObjectPopString(LONG,HSTRING);
  }

  public static CBoolean __ObjectPopVariant(CNumber.ByValue LONG, IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPopVariant(LONG,HUDV);
  }

  public static CBoolean __ObjectPushBoolean(CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.__ObjectPushBoolean(BOOL);
  }

  public static CBoolean __ObjectPushBooleanByRef(CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.__ObjectPushBooleanByRef(BOOL);
  }

  public static CBoolean __ObjectPushDate(CDateTime.ByValue DATETIME) {
    return isalsymbolsImpl.__ObjectPushDate(DATETIME);
  }

  public static CBoolean __ObjectPushDateByRef(CDateTime.ByValue DATETIME) {
    return isalsymbolsImpl.__ObjectPushDateByRef(DATETIME);
  }

  public static CBoolean __ObjectPushNumber(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.__ObjectPushNumber(LONG,LONG2);
  }

  public static CBoolean __ObjectPushNumberByRef(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.__ObjectPushNumberByRef(LONG,LONG2);
  }

  public static CBoolean __ObjectPushObject(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPushObject(HUDV);
  }

  public static CBoolean __ObjectPushObjectByRef(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPushObjectByRef(HUDV);
  }

  public static CBoolean __ObjectPushSafeArray(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPushSafeArray(HUDV);
  }

  public static CBoolean __ObjectPushSafeArrayByRef(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPushSafeArrayByRef(HUDV);
  }

  public static CBoolean __ObjectPushString(CString.ByValue HSTRING) {
    return isalsymbolsImpl.__ObjectPushString(HSTRING);
  }

  public static CBoolean __ObjectPushStringByRef(CString.ByValue HSTRING) {
    return isalsymbolsImpl.__ObjectPushStringByRef(HSTRING);
  }

  public static CBoolean __ObjectPushVariant(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPushVariant(HUDV);
  }

  public static CBoolean __ObjectPushVariantByRef(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__ObjectPushVariantByRef(HUDV);
  }

  public static CBoolean __SafeArrayCreate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.__SafeArrayCreate(LONG,LONG2,LONG3);
  }

  public static CBoolean __SafeArrayCreateMD(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return isalsymbolsImpl.__SafeArrayCreateMD(LONG,LONG2,LONG3);
  }

  public static CBoolean __SafeArrayGetBoolean(CBoolean.ByReference LPBOOL, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetBoolean(LPBOOL,LONG);
  }

  public static CBoolean __SafeArrayGetBooleanMD(CBoolean.ByReference LPBOOL,
      CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetBooleanMD(LPBOOL,LONG);
  }

  public static CBoolean __SafeArrayGetDate(CDateTime.ByReference LPDATETIME,
      CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetDate(LPDATETIME,LONG);
  }

  public static CBoolean __SafeArrayGetDateMD(CDateTime.ByReference LPDATETIME,
      CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetDateMD(LPDATETIME,LONG);
  }

  public static CNumber __SafeArrayGetLowerBound(CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetLowerBound(LONG);
  }

  public static CNumber __SafeArrayGetLowerBoundMD(CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetLowerBoundMD(LONG);
  }

  public static CBoolean __SafeArrayGetNumber(CNumber.ByReference LPLONG, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetNumber(LPLONG,LONG);
  }

  public static CBoolean __SafeArrayGetNumberMD(CNumber.ByReference LPLONG, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetNumberMD(LPLONG,LONG);
  }

  public static CBoolean __SafeArrayGetObject(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetObject(HUDV,LONG);
  }

  public static CBoolean __SafeArrayGetObjectMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetObjectMD(HUDV,LONG);
  }

  public static CBoolean __SafeArrayGetString(CString.ByReference HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetString(HSTRING,LONG);
  }

  public static CBoolean __SafeArrayGetStringMD(CString.ByReference HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetStringMD(HSTRING,LONG);
  }

  public static CNumber __SafeArrayGetUpperBound(CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetUpperBound(LONG);
  }

  public static CNumber __SafeArrayGetUpperBoundMD(CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetUpperBoundMD(LONG);
  }

  public static CBoolean __SafeArrayGetVariant(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetVariant(HUDV,LONG);
  }

  public static CBoolean __SafeArrayGetVariantMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayGetVariantMD(HUDV,LONG);
  }

  public static CBoolean __SafeArrayPutBoolean(CBoolean.ByValue BOOL, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutBoolean(BOOL,LONG);
  }

  public static CBoolean __SafeArrayPutBooleanMD(CBoolean.ByValue BOOL, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutBooleanMD(BOOL,LONG);
  }

  public static CBoolean __SafeArrayPutDate(CDateTime.ByValue DATETIME, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutDate(DATETIME,LONG);
  }

  public static CBoolean __SafeArrayPutDateMD(CDateTime.ByValue DATETIME, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutDateMD(DATETIME,LONG);
  }

  public static CBoolean __SafeArrayPutNumber(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.__SafeArrayPutNumber(LONG,LONG2);
  }

  public static CBoolean __SafeArrayPutNumberMD(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.__SafeArrayPutNumberMD(LONG,LONG2);
  }

  public static CBoolean __SafeArrayPutObject(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutObject(HUDV,LONG);
  }

  public static CBoolean __SafeArrayPutObjectMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutObjectMD(HUDV,LONG);
  }

  public static CBoolean __SafeArrayPutString(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutString(HSTRING,LONG);
  }

  public static CBoolean __SafeArrayPutStringMD(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutStringMD(HSTRING,LONG);
  }

  public static CBoolean __SafeArrayPutVariant(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutVariant(HUDV,LONG);
  }

  public static CBoolean __SafeArrayPutVariantMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return isalsymbolsImpl.__SafeArrayPutVariantMD(HUDV,LONG);
  }

  public static CNumber __SalTypeVariant() {
    return isalsymbolsImpl.__SalTypeVariant();
  }

  public static CBoolean __SetBlobVariant(CString.ByReference HSTRING) {
    return isalsymbolsImpl.__SetBlobVariant(HSTRING);
  }

  public static CBoolean __SetBooleanVariant(CBoolean.ByValue BOOL) {
    return isalsymbolsImpl.__SetBooleanVariant(BOOL);
  }

  public static CBoolean __SetDateVariant(CDateTime.ByValue DATETIME) {
    return isalsymbolsImpl.__SetDateVariant(DATETIME);
  }

  public static CBoolean __SetNumberVariant(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return isalsymbolsImpl.__SetNumberVariant(LONG,LONG2);
  }

  public static CBoolean __SetObject(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__SetObject(HUDV);
  }

  public static CBoolean __SetSafeArray(IFunctionalClass HUDV) {
    return isalsymbolsImpl.__SetSafeArray(HUDV);
  }

  public static CBoolean __SetStringVariant(CString.ByValue HSTRING) {
    return isalsymbolsImpl.__SetStringVariant(HSTRING);
  }
}
