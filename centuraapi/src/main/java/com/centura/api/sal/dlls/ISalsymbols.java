package com.centura.api.sal.dlls;

import com.centura.api.cdk.IFunctionalClass;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CNumber;
import com.centura.api.type.CSQLHandle;
import com.centura.api.type.CString;
import com.centura.api.type.CStruct;
import com.centura.api.type.CWndHandle;

public interface ISalsymbols {
  CNumber HSGetRef(CString.ByValue HSTRING);

  CNumber HSNumStringsInUse();

  CBoolean OraPLSQLExecute(CSQLHandle.ByValue HSQLHANDLE);

  CBoolean OraPLSQLPrepare(CSQLHandle.ByValue HSQLHANDLE, CString.ByValue HSTRING);

  CBoolean OraPLSQLStringBindType(CSQLHandle.ByValue HSQLHANDLE, CString.ByValue HSTRING,
      CNumber.ByValue LONG);

  CNumber SalAutoNameObject(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalBIDIDlgChooseFontsIntern(CWndHandle.ByValue HWND, CString.ByReference HSTRING,
      CNumber.ByReference LPLONG, CString.ByReference HSTRING4, CNumber.ByReference LPLONG5,
      CNumber.ByReference LPLONG6, CNumber.ByReference LPLONG7, CNumber.ByReference LPLONG8,
      CNumber.ByReference LPLONG9);

  CNumber SalCDKGetChildByName(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CString.ByValue HSTRING);

  CNumber SalCDKGetChildTypeByName(CNumber.ByValue LONG, CString.ByValue HSTRING);

  CString SalCDKGetName(CString.ByValue HSTRING);

  CNumber SalCDKGetValidChildren(CNumber.ByValue LONG, CString.ByValue HSTRING);

  CString SalCDKParseTitle(CString.ByValue HSTRING, CNumber.ByValue LONG, CNumber.ByValue LONG3,
      CNumber.ByReference LPLONG);

  CNumber SalChangeLineCountLock();

  CNumber SalChangeLineCountNotify(CNumber.ByValue LONG);

  CNumber SalChangeLineCountUnlock(CNumber.ByValue LONG);

  CWndHandle SalCreateWindow(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND);

  CWndHandle SalCreateWindowEx(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CNumber.ByValue LONG6,
      CNumber.ByValue LONG7);

  CWndHandle SalCreateWindowExFromStr(CString.ByValue HSTRING, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CNumber.ByValue LONG6,
      CNumber.ByValue LONG7);

  CWndHandle SalCreateWindowFromStr(CString.ByValue HSTRING, CWndHandle.ByValue HWND);

  CNumber SalCurrentLineNumber();

  CNumber SalDataGetFieldData(CWndHandle.ByValue HWND);

  CWndHandle SalDataGetFieldWindow(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CWndHandle SalDataGetSourceWindow(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CNumber SalDataQueryFieldExtent(CWndHandle.ByValue HWND);

  CNumber SalDataQueryFields(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING);

  CNumber SalDataQuerySources(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING);

  CNumber SalDialogOpenExisting();

  CNumber SalDisableAllWindows(CWndHandle.ByValue HWND);

  CBoolean SalEditCanInsertObject();

  CBoolean SalEditCanPasteLink();

  CBoolean SalEditCanPasteSpecial();

  CBoolean SalEditInsertObject();

  CBoolean SalEditPasteLink();

  CBoolean SalEditPasteSpecial();

  CNumber SalEnableAllWindows(CNumber.ByValue LONG);

  CBoolean SalFlashWindow(CWndHandle.ByValue HWND, CBoolean.ByValue BOOL);

  CBoolean SalFmtCopyProfile(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CBoolean SalFmtGetParmInt(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG);

  CBoolean SalFmtGetParmStr(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByReference HSTRING);

  CNumber SalFmtGetProfile(CWndHandle.ByValue HWND);

  CBoolean SalFmtSetParmInt(CWndHandle.ByValue HWND, CNumber.ByValue LONG, CNumber.ByValue LONG3);

  CBoolean SalFmtSetParmStr(CWndHandle.ByValue HWND, CNumber.ByValue LONG, CString.ByValue HSTRING);

  CBoolean SalFmtSetProfile(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CBoolean SalGetBuildSettings(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByReference LPLONG, CString.ByReference HSTRING4, CString.ByReference HSTRING5,
      CString.ByReference HSTRING6, CString.ByReference HSTRING7, CNumber.ByReference LPLONG8,
      CNumber.ByReference LPLONG9, CNumber.ByReference LPLONG10, CNumber.ByReference LPLONG11,
      CNumber.ByReference LPLONG12);

  CWndHandle SalGetCurrentDesignWindow(CNumber.ByValue LONG);

  CNumber SalGetRegistryString(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CString.ByValue HSTRING3, CString.ByReference HSTRING4, CNumber.ByValue LONG);

  CWndHandle SalGetWindowLabel(CWndHandle.ByValue HWND);

  CBoolean SalIsOutlineSecondary(CNumber.ByValue LONG);

  CNumber SalItemGetLineNumber(CNumber.ByValue LONG);

  CBoolean SalLoadAppAndProcessMsgs(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG);

  CBoolean SalLog(CString.ByValue HSTRING, CString.ByValue HSTRING2);

  CBoolean SalLogLine(CString.ByValue HSTRING, CString.ByValue HSTRING2);

  CBoolean SalLogResources(CString.ByValue HSTRING);

  CNumber SalModalDialog(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND);

  CNumber SalModalDialogFromStr(CString.ByValue HSTRING, CWndHandle.ByValue HWND);

  CBoolean SalOLEAnyActive(CWndHandle.ByValue HWND);

  CBoolean SalOLEAnyLinked();

  CBoolean SalOLEDoVerb(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CBoolean SalOLEFileInsert(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CNumber SalOLEGetServers(CString.ByValue HSTRING);

  CNumber SalOLEGetVerbs(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CBoolean SalOLELinkProperties(CWndHandle.ByValue HWND);

  CBoolean SalOLEServerInsert(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CBoolean SalOLEUpdateActive(CWndHandle.ByValue HWND);

  CBoolean SalOutlineActivateUIView(CNumber.ByValue LONG, CString.ByValue HSTRING);

  CBoolean SalOutlineAddNotifyWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND);

  CBoolean SalOutlineAlignWindows(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CNumber SalOutlineBaseClassPropEditor(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CBoolean SalOutlineBlockNotifications(CNumber.ByValue LONG, CBoolean.ByValue BOOL);

  CBoolean SalOutlineCanAlignWindows(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CNumber SalOutlineCanDoUICommand(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING);

  CBoolean SalOutlineCanInsert(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CBoolean.ByValue BOOL);

  CBoolean SalOutlineCanMoveToBack(CNumber.ByValue LONG);

  CBoolean SalOutlineCanMoveToFront(CNumber.ByValue LONG);

  CBoolean SalOutlineChangeSelect(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL, CBoolean.ByValue BOOL4, CBoolean.ByValue BOOL5);

  CNumber SalOutlineChildOfType(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CBoolean.ByValue BOOL);

  CNumber SalOutlineClassNameOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByReference HSTRING);

  CBoolean SalOutlineClearCustomCmds(CNumber.ByValue LONG);

  CBoolean SalOutlineCloseDesignEdits(CNumber.ByValue LONG, CBoolean.ByValue BOOL);

  CNumber SalOutlineCopyItem(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CNumber.ByValue LONG4, CNumber.ByValue LONG5, CBoolean.ByValue BOOL, CBoolean.ByValue BOOL7);

  CNumber SalOutlineCreateClassFromObject(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CBoolean.ByValue BOOL);

  CWndHandle SalOutlineCreateDesignWindow(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND, CWndHandle.ByValue HWND4);

  CNumber SalOutlineCreateItem(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineCurrent();

  CBoolean SalOutlineCustomizeItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND);

  CBoolean SalOutlineDeleteItem(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineDoUICommand(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CString.ByValue HSTRING);

  CBoolean SalOutlineDontCustomizeType(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3);

  CNumber SalOutlineEditGetItem(CNumber.ByValue LONG);

  CNumber SalOutlineEditGetText(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4);

  CBoolean SalOutlineEditSetText(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CBoolean.ByValue BOOL);

  CBoolean SalOutlineEnableDesignScaling(CNumber.ByValue LONG, CBoolean.ByValue BOOL,
      CString.ByValue HSTRING, CNumber.ByValue LONG4);

  CNumber SalOutlineEnumItemProps(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING);

  CBoolean SalOutlineEnumSymbols(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CNumber.ByValue LONG4, CNumber.ByValue LONG5, CBoolean.ByValue BOOL, CNumber.ByValue LONG7,
      CNumber.ByValue LONG8);

  CNumber SalOutlineEnumWindowProps(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CNumber SalOutlineFindTemplateOfClass(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineFirstChild(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineFirstDisplayedChild(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineFirstMarked(CNumber.ByValue LONG);

  CBoolean SalOutlineGetAppChanged(CNumber.ByValue LONG);

  CNumber SalOutlineGetAttributes(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3);

  CBoolean SalOutlineGetDTData(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CNumber.ByReference LPLONG);

  CNumber SalOutlineGetDrawTool(CNumber.ByValue LONG);

  CNumber SalOutlineGetFileName(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CBoolean.ByValue BOOL);

  CNumber SalOutlineGetFirstUISelection(CNumber.ByValue LONG);

  CBoolean SalOutlineGetFormFlags(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CNumber SalOutlineGetFunParams(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByReference HSTRING4, CString.ByReference HSTRING5);

  CNumber SalOutlineGetIncludingItem(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineGetItemFlags(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineGetNextUISelection(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineGetNotifyItem(CNumber.ByValue LONG, CNumber.ByReference LPLONG,
      CNumber.ByReference LPLONG3, CNumber.ByReference LPLONG4);

  CNumber SalOutlineGetOneUISelection(CNumber.ByValue LONG);

  CNumber SalOutlineGetUIViewInfo(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4, CNumber.ByReference LPLONG5);

  CBoolean SalOutlineHighlightItem(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4);

  CBoolean SalOutlineInfo(CString.ByValue HSTRING, CBoolean.ByReference LPBOOL,
      CBoolean.ByReference LPBOOL3, CBoolean.ByReference LPBOOL4, CBoolean.ByReference LPBOOL5,
      CBoolean.ByReference LPBOOL6, CNumber.ByReference LPLONG);

  CBoolean SalOutlineInheritFromBaseClasses(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineInsertItem(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CNumber.ByValue LONG4);

  CBoolean SalOutlineIsBreak(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineIsClassChildRef(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineIsClassObject(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineIsCompiled(CNumber.ByValue LONG);

  CBoolean SalOutlineIsDrawToolLocked(CNumber.ByValue LONG);

  CBoolean SalOutlineIsItemMarked(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineIsTemplateOfClass(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineIsUIViewActive(CNumber.ByValue LONG, CString.ByValue HSTRING);

  CNumber SalOutlineIsUserWindow(CNumber.ByValue LONG);

  CBoolean SalOutlineIsWindowItem(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineItemChangeUpdate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3);

  CNumber SalOutlineItemGetBlob(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByReference LPLONG);

  CBoolean SalOutlineItemGetProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByReference HSTRING4);

  CBoolean SalOutlineItemGetPropertyBuffer(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CNumber.ByValue LONG4, CNumber.ByValue LONG5);

  CBoolean SalOutlineItemIsIncluded(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineItemLevel(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineItemLineCount(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineItemOfWindow(CWndHandle.ByValue HWND);

  CNumber SalOutlineItemOfWindowIndirect(CWndHandle.ByValue HWND);

  CBoolean SalOutlineItemRemoveProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING);

  CBoolean SalOutlineItemSetBlob(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByReference HSTRING, CString.ByReference HSTRING4, CNumber.ByValue LONG5);

  CBoolean SalOutlineItemSetProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByValue HSTRING4, CNumber.ByValue LONG5);

  CBoolean SalOutlineItemText(CNumber.ByValue LONG, CNumber.ByValue LONG2, CBoolean.ByValue BOOL,
      CString.ByReference HSTRING);

  CString SalOutlineItemTextX(CNumber.ByValue LONG, CNumber.ByValue LONG2, CBoolean.ByValue BOOL);

  CBoolean SalOutlineItemTitle(CNumber.ByValue LONG, CString.ByReference HSTRING);

  CString SalOutlineItemTitleX(CNumber.ByValue LONG);

  CBoolean SalOutlineItemToTagged(CNumber.ByValue LONG, CStruct.ByValue TEMPLATE,
      CStruct.ByValue TEMPLATE3);

  CNumber SalOutlineItemType(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineItemTypeText(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL, CString.ByReference HSTRING);

  CNumber SalOutlineLastChild(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineLoad(CString.ByValue HSTRING);

  CBoolean SalOutlineLockDrawTool(CNumber.ByValue LONG, CBoolean.ByValue BOOL);

  CBoolean SalOutlineMergeIncludes(CNumber.ByValue LONG);

  CBoolean SalOutlineMoveToBack(CNumber.ByValue LONG);

  CBoolean SalOutlineMoveToFront(CNumber.ByValue LONG);

  CNumber SalOutlineNextDisplayedSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineNextLikeItem(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CString.ByValue HSTRING, CNumber.ByValue LONG5, CNumber.ByValue LONG6);

  CNumber SalOutlineNextLine(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineNextMarked(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlineNextSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineNotify(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3);

  CWndHandle SalOutlineOrderTabs(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CNumber SalOutlineOutlineOfUIFrame(CWndHandle.ByValue HWND);

  CNumber SalOutlineOutlineOfWindow(CWndHandle.ByValue HWND);

  CNumber SalOutlineParent(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalOutlinePreviousSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlinePropertyChangeUpdate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4, CNumber.ByValue LONG5);

  CBoolean SalOutlineQueryUserMode(CNumber.ByValue LONG);

  CBoolean SalOutlineRefreshInclude(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineRemoveNotifyWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND);

  CBoolean SalOutlineReportError(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByValue HSTRING4);

  CBoolean SalOutlineSave(CNumber.ByValue LONG, CString.ByValue HSTRING);

  CBoolean SalOutlineSaveAsText(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CBoolean.ByValue BOOL);

  CNumber SalOutlineSelectNewItem(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineSetAppChanged(CNumber.ByValue LONG);

  CBoolean SalOutlineSetAppUncompiled(CNumber.ByValue LONG);

  CBoolean SalOutlineSetBreak(CNumber.ByValue LONG, CNumber.ByValue LONG2, CBoolean.ByValue BOOL);

  CBoolean SalOutlineSetCustomCmd(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING);

  CBoolean SalOutlineSetDTData(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CNumber.ByValue LONG4);

  CBoolean SalOutlineSetData(CNumber.ByValue LONG, CNumber.ByValue LONG2, CString.ByValue HSTRING,
      CNumber.ByValue LONG4);

  CBoolean SalOutlineSetDrawTool(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalOutlineSetFormFlags(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CBoolean.ByValue BOOL);

  CBoolean SalOutlineSetItemClassObject(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING);

  CBoolean SalOutlineSetItemFlags(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL);

  CNumber SalOutlineSetOutlineHook(CNumber.ByValue LONG);

  CBoolean SalOutlineSetTypeData(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3,
      CString.ByValue HSTRING, CNumber.ByValue LONG5);

  CBoolean SalOutlineShare(CNumber.ByValue LONG, CBoolean.ByValue BOOL);

  CBoolean SalOutlineShowDesignWindow(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL);

  CNumber SalOutlineTop(CNumber.ByValue LONG);

  CWndHandle SalOutlineUIFrameOfOutline(CNumber.ByValue LONG);

  CBoolean SalOutlineUnload(CNumber.ByValue LONG);

  CNumber SalOutlineUnlockBlob(CNumber.ByValue LONG);

  CNumber SalOutlineWindowItemOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CWndHandle SalOutlineWindowOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CWndHandle SalOutlineWindowOfItemIndirect(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean SalParseStatement(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CNumber.ByReference LPLONG);

  CBoolean SalProfileRegisterWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4);

  CBoolean SalProfileUnregisterWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND);

  CNumber SalQOConnect(CNumber.ByValue LONG, CString.ByValue HSTRING, CString.ByValue HSTRING3,
      CString.ByValue HSTRING4, CNumber.ByReference LPLONG);

  CNumber SalQODisplayConnectError(CString.ByValue HSTRING, CNumber.ByValue LONG);

  CString SalQOGetColumnInfoFromIndex(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalQOGetConnectInfo(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CString.ByReference HSTRING3, CString.ByReference HSTRING4);

  CString SalQOGetTableInfoFromIndex(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CNumber SalQOLoadCatalogTables(CNumber.ByValue LONG);

  CNumber SalQOLoadTableColumns(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CString.ByValue HSTRING4);

  CNumber SalQOReleaseColumnDefs(CNumber.ByValue LONG);

  CNumber SalQOReleaseTableDefs(CNumber.ByValue LONG);

  CWndHandle SalReportGetDateTimeVar363Boolean(CString.ByValue HSTRING,
      CDateTime.ByReference LPDATETIME);

  CBoolean SalReportSetPrintParameter(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3);

  CStruct SalResId(CString.ByValue HSTRING);

  CBoolean SalResLoad(CStruct.ByValue TEMPLATE, CString.ByReference HSTRING);

  CBoolean SalResourceGet(CString.ByReference HSTRING, CNumber.ByReference LPLONG);

  CBoolean SalResourceSet(CString.ByValue HSTRING, CNumber.ByValue LONG);

  CBoolean SalSetBuildSettings(CNumber.ByValue LONG, CString.ByValue HSTRING, CNumber.ByValue LONG3,
      CString.ByValue HSTRING4, CString.ByValue HSTRING5, CString.ByValue HSTRING6,
      CString.ByValue HSTRING7, CNumber.ByValue LONG8, CNumber.ByValue LONG9,
      CNumber.ByValue LONG10, CNumber.ByValue LONG11, CNumber.ByValue LONG12);

  CBoolean SalSetRegistryString(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CString.ByValue HSTRING3);

  CNumber SalShowToolBoxWindow(CBoolean.ByValue BOOL);

  CNumber SalStaticFirst(CWndHandle.ByValue HWND);

  CNumber SalStaticGetItem(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CNumber SalStaticGetLabel(CWndHandle.ByValue HWND);

  CBoolean SalStaticGetLoc(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4);

  CBoolean SalStaticGetSize(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4);

  CBoolean SalStaticHide(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CBoolean SalStaticIsVisible(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CNumber SalStaticNext(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CBoolean SalStaticSetLoc(CWndHandle.ByValue HWND, CNumber.ByValue LONG, CNumber.ByValue LONG3,
      CNumber.ByValue LONG4);

  CBoolean SalStaticSetSize(CWndHandle.ByValue HWND, CNumber.ByValue LONG, CNumber.ByValue LONG3,
      CNumber.ByValue LONG4);

  CBoolean SalStaticShow(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CNumber SalStrFull(CString.ByValue HSTRING, CString.ByReference HSTRING2);

  CString SalStrGetIdentifier(CString.ByValue HSTRING);

  CBoolean SalStrGetIdentifierInt(CString.ByValue HSTRING, CString.ByReference HSTRING2,
      CNumber.ByValue LONG);

  CNumber SalStrHalf(CString.ByValue HSTRING, CString.ByReference HSTRING2);

  CBoolean SalStrIsValidDecimal(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3);

  CBoolean SalStrIsValidIdentifierName(CString.ByValue HSTRING);

  CNumber SalStrLeftB(CString.ByValue HSTRING, CNumber.ByValue LONG, CString.ByReference HSTRING3);

  CNumber SalStrMidB(CString.ByValue HSTRING, CNumber.ByValue LONG, CNumber.ByValue LONG3,
      CString.ByReference HSTRING4);

  CNumber SalStrReplaceB(CString.ByValue HSTRING, CNumber.ByValue LONG, CNumber.ByValue LONG3,
      CString.ByValue HSTRING4, CString.ByReference HSTRING5);

  CNumber SalStrRightB(CString.ByValue HSTRING, CNumber.ByValue LONG, CString.ByReference HSTRING3);

  CNumber SalStringLengthB(CString.ByValue HSTRING);

  CBoolean SalTblPaintRows(CWndHandle.ByValue HWND, CNumber.ByValue LONG, CNumber.ByValue LONG3);

  CNumber SalTblQueryView(CWndHandle.ByValue HWND);

  CBoolean SalTblSetView(CWndHandle.ByValue HWND, CNumber.ByValue LONG);

  CBoolean SalWinDebugBreak();

  CBoolean SalWindowRemoveProperty(CWndHandle.ByValue HWND, CString.ByValue HSTRING);

  CBoolean SalWindowSetProperty(CWndHandle.ByValue HWND, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CNumber.ByValue LONG);

  CBoolean SqlConnectUsingCursor(CSQLHandle.ByReference LPHSQLHANDLE, CNumber.ByValue LONG);

  CBoolean SqlContextClear(CSQLHandle.ByValue HSQLHANDLE);

  CBoolean SqlContextSet(CSQLHandle.ByValue HSQLHANDLE);

  CBoolean SqlContextSetToForm(CSQLHandle.ByValue HSQLHANDLE, CWndHandle.ByValue HWND);

  CBoolean SqlDisconnectWithoutCursor(CSQLHandle.ByValue HSQLHANDLE);

  CNumber SqlGetCursor(CSQLHandle.ByValue HSQLHANDLE);

  CSQLHandle SqlGetSqlHandle(CNumber.ByValue LONG);

  CBoolean SqlHandleSetParameters(CSQLHandle.ByValue HSQLHANDLE);

  CBoolean SqlImmediateContext(CString.ByValue HSTRING);

  CWndHandle SqlPLSQLExecute(CString.ByValue HSTRING);

  CBoolean SqlSetDSOrSessionPtr(CSQLHandle.ByReference LPHSESSIONHANDLE, CBoolean.ByValue BOOL,
      CNumber.ByValue LONG);

  CBoolean SqlXDirectory(CNumber.ByValue LONG, CString.ByValue HSTRING);

  CNumber __ActiveXTypeVariant();

  CBoolean __AsActiveXVariant(CNumber.ByValue LONG);

  CBoolean __CoClassCreateObject();

  CBoolean __CoClassCreateObjectEx(CString.ByValue HSTRING);

  CBoolean __CoClassGetInterface(IFunctionalClass HUDV, CString.ByValue HSTRING);

  CBoolean __CoClassRelease();

  CNumber __EnumCount();

  CBoolean __EnumIsCollection();

  CBoolean __EnumNext(IFunctionalClass HUDV);

  CBoolean __EnumReset();

  CBoolean __EnumSkip(CNumber.ByValue LONG);

  CBoolean __GetBlobVariant(CString.ByReference HSTRING);

  CBoolean __GetBooleanVariant(CBoolean.ByReference LPBOOL);

  CBoolean __GetDateVariant(CDateTime.ByReference LPDATETIME);

  CBoolean __GetLastError(CNumber.ByReference LPLONG, CString.ByReference HSTRING,
      CString.ByReference HSTRING3, CString.ByReference HSTRING4, CNumber.ByReference LPLONG5,
      CNumber.ByReference LPLONG6);

  CBoolean __GetNumberVariant(CNumber.ByReference LPLONG);

  CBoolean __GetObjectVariant(IFunctionalClass HUDV);

  CBoolean __GetSafeArrayVariant(IFunctionalClass HUDV);

  CBoolean __GetStringVariant(CString.ByReference HSTRING);

  CNumber __MakeOptionalVariant();

  CBoolean __ObjectAttach(IFunctionalClass HUDV);

  CBoolean __ObjectCreateObject(CString.ByValue HSTRING);

  CBoolean __ObjectCreateObjectEx(CString.ByValue HSTRING, CString.ByValue HSTRING2);

  CBoolean __ObjectDetach();

  CBoolean __ObjectFlushArgs();

  CBoolean __ObjectGetInterface(IFunctionalClass HUDV);

  CBoolean __ObjectInvoke(CString.ByValue HSTRING, CNumber.ByValue LONG);

  CBoolean __ObjectInvokeID(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean __ObjectIsValid();

  CBoolean __ObjectPopBoolean(CNumber.ByValue LONG, CBoolean.ByReference LPBOOL);

  CBoolean __ObjectPopDate(CNumber.ByValue LONG, CDateTime.ByReference LPDATETIME);

  CBoolean __ObjectPopNumber(CNumber.ByValue LONG, CNumber.ByReference LPLONG);

  CBoolean __ObjectPopObject(CNumber.ByValue LONG, IFunctionalClass HUDV);

  CBoolean __ObjectPopSafeArray(CNumber.ByValue LONG, IFunctionalClass HUDV);

  CBoolean __ObjectPopString(CNumber.ByValue LONG, CString.ByReference HSTRING);

  CBoolean __ObjectPopVariant(CNumber.ByValue LONG, IFunctionalClass HUDV);

  CBoolean __ObjectPushBoolean(CBoolean.ByValue BOOL);

  CBoolean __ObjectPushBooleanByRef(CBoolean.ByValue BOOL);

  CBoolean __ObjectPushDate(CDateTime.ByValue DATETIME);

  CBoolean __ObjectPushDateByRef(CDateTime.ByValue DATETIME);

  CBoolean __ObjectPushNumber(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean __ObjectPushNumberByRef(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean __ObjectPushObject(IFunctionalClass HUDV);

  CBoolean __ObjectPushObjectByRef(IFunctionalClass HUDV);

  CBoolean __ObjectPushSafeArray(IFunctionalClass HUDV);

  CBoolean __ObjectPushSafeArrayByRef(IFunctionalClass HUDV);

  CBoolean __ObjectPushString(CString.ByValue HSTRING);

  CBoolean __ObjectPushStringByRef(CString.ByValue HSTRING);

  CBoolean __ObjectPushVariant(IFunctionalClass HUDV);

  CBoolean __ObjectPushVariantByRef(IFunctionalClass HUDV);

  CBoolean __SafeArrayCreate(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3);

  CBoolean __SafeArrayCreateMD(CNumber.ByValue LONG, CNumber.ByValue LONG2, CNumber.ByValue LONG3);

  CBoolean __SafeArrayGetBoolean(CBoolean.ByReference LPBOOL, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetBooleanMD(CBoolean.ByReference LPBOOL, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetDate(CDateTime.ByReference LPDATETIME, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetDateMD(CDateTime.ByReference LPDATETIME, CNumber.ByValue LONG);

  CNumber __SafeArrayGetLowerBound(CNumber.ByValue LONG);

  CNumber __SafeArrayGetLowerBoundMD(CNumber.ByValue LONG);

  CBoolean __SafeArrayGetNumber(CNumber.ByReference LPLONG, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetNumberMD(CNumber.ByReference LPLONG, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetObject(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetObjectMD(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetString(CString.ByReference HSTRING, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetStringMD(CString.ByReference HSTRING, CNumber.ByValue LONG);

  CNumber __SafeArrayGetUpperBound(CNumber.ByValue LONG);

  CNumber __SafeArrayGetUpperBoundMD(CNumber.ByValue LONG);

  CBoolean __SafeArrayGetVariant(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CBoolean __SafeArrayGetVariantMD(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutBoolean(CBoolean.ByValue BOOL, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutBooleanMD(CBoolean.ByValue BOOL, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutDate(CDateTime.ByValue DATETIME, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutDateMD(CDateTime.ByValue DATETIME, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutNumber(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean __SafeArrayPutNumberMD(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean __SafeArrayPutObject(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutObjectMD(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutString(CString.ByValue HSTRING, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutStringMD(CString.ByValue HSTRING, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutVariant(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CBoolean __SafeArrayPutVariantMD(IFunctionalClass HUDV, CNumber.ByValue LONG);

  CNumber __SalTypeVariant();

  CBoolean __SetBlobVariant(CString.ByReference HSTRING);

  CBoolean __SetBooleanVariant(CBoolean.ByValue BOOL);

  CBoolean __SetDateVariant(CDateTime.ByValue DATETIME);

  CBoolean __SetNumberVariant(CNumber.ByValue LONG, CNumber.ByValue LONG2);

  CBoolean __SetObject(IFunctionalClass HUDV);

  CBoolean __SetSafeArray(IFunctionalClass HUDV);

  CBoolean __SetStringVariant(CString.ByValue HSTRING);
}
