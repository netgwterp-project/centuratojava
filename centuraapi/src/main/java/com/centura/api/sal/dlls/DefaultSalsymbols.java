package com.centura.api.sal.dlls;

import com.centura.api.cdk.IFunctionalClass;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CNumber;
import com.centura.api.type.CSQLHandle;
import com.centura.api.type.CString;
import com.centura.api.type.CStruct;
import com.centura.api.type.CWndHandle;

public class DefaultSalsymbols implements ISalsymbols {
  public CNumber HSGetRef(CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CNumber HSNumStringsInUse() {
    return CNumber.NUMBER_Null;
  }

  public CBoolean OraPLSQLExecute(CSQLHandle.ByValue HSQLHANDLE) {
    return CBoolean.FALSE;
  }

  public CBoolean OraPLSQLPrepare(CSQLHandle.ByValue HSQLHANDLE, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean OraPLSQLStringBindType(CSQLHandle.ByValue HSQLHANDLE, CString.ByValue HSTRING,
      CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalAutoNameObject(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalBIDIDlgChooseFontsIntern(CWndHandle.ByValue HWND, CString.ByReference HSTRING,
      CNumber.ByReference LPLONG, CString.ByReference HSTRING4, CNumber.ByReference LPLONG5,
      CNumber.ByReference LPLONG6, CNumber.ByReference LPLONG7, CNumber.ByReference LPLONG8,
      CNumber.ByReference LPLONG9) {
    return CBoolean.FALSE;
  }

  public CNumber SalCDKGetChildByName(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalCDKGetChildTypeByName(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CString SalCDKGetName(CString.ByValue HSTRING) {
    return CString.STRING_Null;
  }

  public CNumber SalCDKGetValidChildren(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CString SalCDKParseTitle(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByReference LPLONG) {
    return CString.STRING_Null;
  }

  public CNumber SalChangeLineCountLock() {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalChangeLineCountNotify(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalChangeLineCountUnlock(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CWndHandle SalCreateWindow(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND) {
    return CWndHandle.hWndNULL;
  }

  public CWndHandle SalCreateWindowEx(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CNumber.ByValue LONG6,
      CNumber.ByValue LONG7) {
    return CWndHandle.hWndNULL;
  }

  public CWndHandle SalCreateWindowExFromStr(CString.ByValue HSTRING, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CNumber.ByValue LONG6,
      CNumber.ByValue LONG7) {
    return CWndHandle.hWndNULL;
  }

  public CWndHandle SalCreateWindowFromStr(CString.ByValue HSTRING, CWndHandle.ByValue HWND) {
    return CWndHandle.hWndNULL;
  }

  public CNumber SalCurrentLineNumber() {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalDataGetFieldData(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CWndHandle SalDataGetFieldWindow(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CWndHandle.hWndNULL;
  }

  public CWndHandle SalDataGetSourceWindow(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CWndHandle.hWndNULL;
  }

  public CNumber SalDataQueryFieldExtent(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalDataQueryFields(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalDataQuerySources(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalDialogOpenExisting() {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalDisableAllWindows(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalEditCanInsertObject() {
    return CBoolean.FALSE;
  }

  public CBoolean SalEditCanPasteLink() {
    return CBoolean.FALSE;
  }

  public CBoolean SalEditCanPasteSpecial() {
    return CBoolean.FALSE;
  }

  public CBoolean SalEditInsertObject() {
    return CBoolean.FALSE;
  }

  public CBoolean SalEditPasteLink() {
    return CBoolean.FALSE;
  }

  public CBoolean SalEditPasteSpecial() {
    return CBoolean.FALSE;
  }

  public CNumber SalEnableAllWindows(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalFlashWindow(CWndHandle.ByValue HWND, CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalFmtCopyProfile(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalFmtGetParmInt(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalFmtGetParmStr(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber SalFmtGetProfile(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalFmtSetParmInt(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CBoolean SalFmtSetParmStr(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalFmtSetProfile(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalGetBuildSettings(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByReference LPLONG, CString.ByReference HSTRING4, CString.ByReference HSTRING5,
      CString.ByReference HSTRING6, CString.ByReference HSTRING7, CNumber.ByReference LPLONG8,
      CNumber.ByReference LPLONG9, CNumber.ByReference LPLONG10, CNumber.ByReference LPLONG11,
      CNumber.ByReference LPLONG12) {
    return CBoolean.FALSE;
  }

  public CWndHandle SalGetCurrentDesignWindow(CNumber.ByValue LONG) {
    return CWndHandle.hWndNULL;
  }

  public CNumber SalGetRegistryString(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CString.ByValue HSTRING3, CString.ByReference HSTRING4, CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CWndHandle SalGetWindowLabel(CWndHandle.ByValue HWND) {
    return CWndHandle.hWndNULL;
  }

  public CBoolean SalIsOutlineSecondary(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalItemGetLineNumber(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalLoadAppAndProcessMsgs(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalLog(CString.ByValue HSTRING, CString.ByValue HSTRING2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalLogLine(CString.ByValue HSTRING, CString.ByValue HSTRING2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalLogResources(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber SalModalDialog(CStruct.ByValue TEMPLATE, CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalModalDialogFromStr(CString.ByValue HSTRING, CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOLEAnyActive(CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOLEAnyLinked() {
    return CBoolean.FALSE;
  }

  public CBoolean SalOLEDoVerb(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOLEFileInsert(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber SalOLEGetServers(CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOLEGetVerbs(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOLELinkProperties(CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOLEServerInsert(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOLEUpdateActive(CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineActivateUIView(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineAddNotifyWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineAlignWindows(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineBaseClassPropEditor(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineBlockNotifications(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineCanAlignWindows(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineCanDoUICommand(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineCanInsert(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineCanMoveToBack(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineCanMoveToFront(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineChangeSelect(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL, CBoolean.ByValue BOOL4, CBoolean.ByValue BOOL5) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineChildOfType(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineClassNameOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByReference HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineClearCustomCmds(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineCloseDesignEdits(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineCopyItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CBoolean.ByValue BOOL,
      CBoolean.ByValue BOOL7) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineCreateClassFromObject(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CBoolean.ByValue BOOL) {
    return CNumber.NUMBER_Null;
  }

  public CWndHandle SalOutlineCreateDesignWindow(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND, CWndHandle.ByValue HWND4) {
    return CWndHandle.hWndNULL;
  }

  public CNumber SalOutlineCreateItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineCurrent() {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineCustomizeItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineDeleteItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineDoUICommand(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineDontCustomizeType(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineEditGetItem(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineEditGetText(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineEditSetText(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineEnableDesignScaling(CNumber.ByValue LONG, CBoolean.ByValue BOOL,
      CString.ByValue HSTRING, CNumber.ByValue LONG4) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineEnumItemProps(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineEnumSymbols(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4, CNumber.ByValue LONG5, CBoolean.ByValue BOOL,
      CNumber.ByValue LONG7, CNumber.ByValue LONG8) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineEnumWindowProps(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineFindTemplateOfClass(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineFirstChild(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineFirstDisplayedChild(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineFirstMarked(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineGetAppChanged(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineGetAttributes(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineGetDTData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineGetDrawTool(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineGetFileName(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CBoolean.ByValue BOOL) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineGetFirstUISelection(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineGetFormFlags(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineGetFunParams(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByReference HSTRING4, CString.ByReference HSTRING5) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineGetIncludingItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineGetItemFlags(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineGetNextUISelection(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineGetNotifyItem(CNumber.ByValue LONG, CNumber.ByReference LPLONG,
      CNumber.ByReference LPLONG3, CNumber.ByReference LPLONG4) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineGetOneUISelection(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineGetUIViewInfo(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4, CNumber.ByReference LPLONG5) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineHighlightItem(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineInfo(CString.ByValue HSTRING, CBoolean.ByReference LPBOOL,
      CBoolean.ByReference LPBOOL3, CBoolean.ByReference LPBOOL4, CBoolean.ByReference LPBOOL5,
      CBoolean.ByReference LPBOOL6, CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineInheritFromBaseClasses(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineInsertItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineIsBreak(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineIsClassChildRef(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineIsClassObject(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineIsCompiled(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineIsDrawToolLocked(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineIsItemMarked(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineIsTemplateOfClass(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineIsUIViewActive(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineIsUserWindow(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineIsWindowItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineItemChangeUpdate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineItemGetBlob(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByReference LPLONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineItemGetProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByReference HSTRING4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineItemGetPropertyBuffer(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CNumber.ByValue LONG4, CNumber.ByValue LONG5) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineItemIsIncluded(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineItemLevel(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineItemLineCount(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineItemOfWindow(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineItemOfWindowIndirect(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineItemRemoveProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineItemSetBlob(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByReference HSTRING, CString.ByReference HSTRING4, CNumber.ByValue LONG5) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineItemSetProperty(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByValue HSTRING4, CNumber.ByValue LONG5) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineItemText(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL, CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CString SalOutlineItemTextX(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL) {
    return CString.STRING_Null;
  }

  public CBoolean SalOutlineItemTitle(CNumber.ByValue LONG, CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CString SalOutlineItemTitleX(CNumber.ByValue LONG) {
    return CString.STRING_Null;
  }

  public CBoolean SalOutlineItemToTagged(CNumber.ByValue LONG, CStruct.ByValue TEMPLATE,
      CStruct.ByValue TEMPLATE3) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineItemType(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineItemTypeText(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL, CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineLastChild(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineLoad(CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineLockDrawTool(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineMergeIncludes(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineMoveToBack(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineMoveToFront(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineNextDisplayedSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineNextLikeItem(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING, CNumber.ByValue LONG5,
      CNumber.ByValue LONG6) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineNextLine(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineNextMarked(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineNextSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineNotify(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CWndHandle SalOutlineOrderTabs(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CWndHandle.hWndNULL;
  }

  public CNumber SalOutlineOutlineOfUIFrame(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineOutlineOfWindow(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineParent(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlinePreviousSibling(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlinePropertyChangeUpdate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4, CNumber.ByValue LONG5) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineQueryUserMode(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineRefreshInclude(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineRemoveNotifyWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineReportError(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CString.ByValue HSTRING4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSave(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSaveAsText(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineSelectNewItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineSetAppChanged(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetAppUncompiled(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetBreak(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetCustomCmd(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetDTData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING, CNumber.ByValue LONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetDrawTool(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetFormFlags(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetItemClassObject(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineSetItemFlags(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineSetOutlineHook(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalOutlineSetTypeData(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3, CString.ByValue HSTRING, CNumber.ByValue LONG5) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineShare(CNumber.ByValue LONG, CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean SalOutlineShowDesignWindow(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineTop(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CWndHandle SalOutlineUIFrameOfOutline(CNumber.ByValue LONG) {
    return CWndHandle.hWndNULL;
  }

  public CBoolean SalOutlineUnload(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalOutlineUnlockBlob(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalOutlineWindowItemOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CNumber.NUMBER_Null;
  }

  public CWndHandle SalOutlineWindowOfItem(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CWndHandle.hWndNULL;
  }

  public CWndHandle SalOutlineWindowOfItemIndirect(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CWndHandle.hWndNULL;
  }

  public CBoolean SalParseStatement(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalProfileRegisterWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalProfileUnregisterWindow(CNumber.ByValue LONG, CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CNumber SalQOConnect(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CString.ByValue HSTRING4, CNumber.ByReference LPLONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalQODisplayConnectError(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CString SalQOGetColumnInfoFromIndex(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CString.STRING_Null;
  }

  public CNumber SalQOGetConnectInfo(CNumber.ByValue LONG, CString.ByReference HSTRING,
      CString.ByReference HSTRING3, CString.ByReference HSTRING4) {
    return CNumber.NUMBER_Null;
  }

  public CString SalQOGetTableInfoFromIndex(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CString.STRING_Null;
  }

  public CNumber SalQOLoadCatalogTables(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalQOLoadTableColumns(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CString.ByValue HSTRING4) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalQOReleaseColumnDefs(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalQOReleaseTableDefs(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CWndHandle SalReportGetDateTimeVar363Boolean(CString.ByValue HSTRING,
      CDateTime.ByReference LPDATETIME) {
    return CWndHandle.hWndNULL;
  }

  public CBoolean SalReportSetPrintParameter(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CStruct SalResId(CString.ByValue HSTRING) {
    return null;
  }

  public CBoolean SalResLoad(CStruct.ByValue TEMPLATE, CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalResourceGet(CString.ByReference HSTRING, CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalResourceSet(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalSetBuildSettings(CNumber.ByValue LONG, CString.ByValue HSTRING,
      CNumber.ByValue LONG3, CString.ByValue HSTRING4, CString.ByValue HSTRING5,
      CString.ByValue HSTRING6, CString.ByValue HSTRING7, CNumber.ByValue LONG8,
      CNumber.ByValue LONG9, CNumber.ByValue LONG10, CNumber.ByValue LONG11,
      CNumber.ByValue LONG12) {
    return CBoolean.FALSE;
  }

  public CBoolean SalSetRegistryString(CString.ByValue HSTRING, CString.ByValue HSTRING2,
      CString.ByValue HSTRING3) {
    return CBoolean.FALSE;
  }

  public CNumber SalShowToolBoxWindow(CBoolean.ByValue BOOL) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalStaticFirst(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalStaticGetItem(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalStaticGetLabel(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalStaticGetLoc(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalStaticGetSize(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByReference LPLONG, CNumber.ByReference LPLONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalStaticHide(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalStaticIsVisible(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalStaticNext(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalStaticSetLoc(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalStaticSetSize(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CNumber.ByValue LONG4) {
    return CBoolean.FALSE;
  }

  public CBoolean SalStaticShow(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalStrFull(CString.ByValue HSTRING, CString.ByReference HSTRING2) {
    return CNumber.NUMBER_Null;
  }

  public CString SalStrGetIdentifier(CString.ByValue HSTRING) {
    return CString.STRING_Null;
  }

  public CBoolean SalStrGetIdentifierInt(CString.ByValue HSTRING, CString.ByReference HSTRING2,
      CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber SalStrHalf(CString.ByValue HSTRING, CString.ByReference HSTRING2) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalStrIsValidDecimal(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CBoolean SalStrIsValidIdentifierName(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber SalStrLeftB(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CString.ByReference HSTRING3) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalStrMidB(CString.ByValue HSTRING, CNumber.ByValue LONG, CNumber.ByValue LONG3,
      CString.ByReference HSTRING4) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalStrReplaceB(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CNumber.ByValue LONG3, CString.ByValue HSTRING4, CString.ByReference HSTRING5) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalStrRightB(CString.ByValue HSTRING, CNumber.ByValue LONG,
      CString.ByReference HSTRING3) {
    return CNumber.NUMBER_Null;
  }

  public CNumber SalStringLengthB(CString.ByValue HSTRING) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalTblPaintRows(CWndHandle.ByValue HWND, CNumber.ByValue LONG,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CNumber SalTblQueryView(CWndHandle.ByValue HWND) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean SalTblSetView(CWndHandle.ByValue HWND, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SalWinDebugBreak() {
    return CBoolean.FALSE;
  }

  public CBoolean SalWindowRemoveProperty(CWndHandle.ByValue HWND, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean SalWindowSetProperty(CWndHandle.ByValue HWND, CString.ByValue HSTRING,
      CString.ByValue HSTRING3, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SqlConnectUsingCursor(CSQLHandle.ByReference LPHSQLHANDLE, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SqlContextClear(CSQLHandle.ByValue HSQLHANDLE) {
    return CBoolean.FALSE;
  }

  public CBoolean SqlContextSet(CSQLHandle.ByValue HSQLHANDLE) {
    return CBoolean.FALSE;
  }

  public CBoolean SqlContextSetToForm(CSQLHandle.ByValue HSQLHANDLE, CWndHandle.ByValue HWND) {
    return CBoolean.FALSE;
  }

  public CBoolean SqlDisconnectWithoutCursor(CSQLHandle.ByValue HSQLHANDLE) {
    return CBoolean.FALSE;
  }

  public CNumber SqlGetCursor(CSQLHandle.ByValue HSQLHANDLE) {
    return CNumber.NUMBER_Null;
  }

  public CSQLHandle SqlGetSqlHandle(CNumber.ByValue LONG) {
    return null;
  }

  public CBoolean SqlHandleSetParameters(CSQLHandle.ByValue HSQLHANDLE) {
    return CBoolean.FALSE;
  }

  public CBoolean SqlImmediateContext(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CWndHandle SqlPLSQLExecute(CString.ByValue HSTRING) {
    return CWndHandle.hWndNULL;
  }

  public CBoolean SqlSetDSOrSessionPtr(CSQLHandle.ByReference LPHSESSIONHANDLE,
      CBoolean.ByValue BOOL, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean SqlXDirectory(CNumber.ByValue LONG, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber __ActiveXTypeVariant() {
    return CNumber.NUMBER_Null;
  }

  public CBoolean __AsActiveXVariant(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __CoClassCreateObject() {
    return CBoolean.FALSE;
  }

  public CBoolean __CoClassCreateObjectEx(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __CoClassGetInterface(IFunctionalClass HUDV, CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __CoClassRelease() {
    return CBoolean.FALSE;
  }

  public CNumber __EnumCount() {
    return CNumber.NUMBER_Null;
  }

  public CBoolean __EnumIsCollection() {
    return CBoolean.FALSE;
  }

  public CBoolean __EnumNext(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __EnumReset() {
    return CBoolean.FALSE;
  }

  public CBoolean __EnumSkip(CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetBlobVariant(CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetBooleanVariant(CBoolean.ByReference LPBOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetDateVariant(CDateTime.ByReference LPDATETIME) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetLastError(CNumber.ByReference LPLONG, CString.ByReference HSTRING,
      CString.ByReference HSTRING3, CString.ByReference HSTRING4, CNumber.ByReference LPLONG5,
      CNumber.ByReference LPLONG6) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetNumberVariant(CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetObjectVariant(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetSafeArrayVariant(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __GetStringVariant(CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CNumber __MakeOptionalVariant() {
    return CNumber.NUMBER_Null;
  }

  public CBoolean __ObjectAttach(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectCreateObject(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectCreateObjectEx(CString.ByValue HSTRING, CString.ByValue HSTRING2) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectDetach() {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectFlushArgs() {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectGetInterface(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectInvoke(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectInvokeID(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectIsValid() {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPopBoolean(CNumber.ByValue LONG, CBoolean.ByReference LPBOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPopDate(CNumber.ByValue LONG, CDateTime.ByReference LPDATETIME) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPopNumber(CNumber.ByValue LONG, CNumber.ByReference LPLONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPopObject(CNumber.ByValue LONG, IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPopSafeArray(CNumber.ByValue LONG, IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPopString(CNumber.ByValue LONG, CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPopVariant(CNumber.ByValue LONG, IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushBoolean(CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushBooleanByRef(CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushDate(CDateTime.ByValue DATETIME) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushDateByRef(CDateTime.ByValue DATETIME) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushNumber(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushNumberByRef(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushObject(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushObjectByRef(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushSafeArray(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushSafeArrayByRef(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushString(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushStringByRef(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushVariant(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __ObjectPushVariantByRef(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayCreate(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayCreateMD(CNumber.ByValue LONG, CNumber.ByValue LONG2,
      CNumber.ByValue LONG3) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetBoolean(CBoolean.ByReference LPBOOL, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetBooleanMD(CBoolean.ByReference LPBOOL, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetDate(CDateTime.ByReference LPDATETIME, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetDateMD(CDateTime.ByReference LPDATETIME, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber __SafeArrayGetLowerBound(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber __SafeArrayGetLowerBoundMD(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean __SafeArrayGetNumber(CNumber.ByReference LPLONG, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetNumberMD(CNumber.ByReference LPLONG, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetObject(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetObjectMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetString(CString.ByReference HSTRING, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetStringMD(CString.ByReference HSTRING, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber __SafeArrayGetUpperBound(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CNumber __SafeArrayGetUpperBoundMD(CNumber.ByValue LONG) {
    return CNumber.NUMBER_Null;
  }

  public CBoolean __SafeArrayGetVariant(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayGetVariantMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutBoolean(CBoolean.ByValue BOOL, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutBooleanMD(CBoolean.ByValue BOOL, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutDate(CDateTime.ByValue DATETIME, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutDateMD(CDateTime.ByValue DATETIME, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutNumber(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutNumberMD(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutObject(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutObjectMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutString(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutStringMD(CString.ByValue HSTRING, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutVariant(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CBoolean __SafeArrayPutVariantMD(IFunctionalClass HUDV, CNumber.ByValue LONG) {
    return CBoolean.FALSE;
  }

  public CNumber __SalTypeVariant() {
    return CNumber.NUMBER_Null;
  }

  public CBoolean __SetBlobVariant(CString.ByReference HSTRING) {
    return CBoolean.FALSE;
  }

  public CBoolean __SetBooleanVariant(CBoolean.ByValue BOOL) {
    return CBoolean.FALSE;
  }

  public CBoolean __SetDateVariant(CDateTime.ByValue DATETIME) {
    return CBoolean.FALSE;
  }

  public CBoolean __SetNumberVariant(CNumber.ByValue LONG, CNumber.ByValue LONG2) {
    return CBoolean.FALSE;
  }

  public CBoolean __SetObject(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __SetSafeArray(IFunctionalClass HUDV) {
    return CBoolean.FALSE;
  }

  public CBoolean __SetStringVariant(CString.ByValue HSTRING) {
    return CBoolean.FALSE;
  }
}
