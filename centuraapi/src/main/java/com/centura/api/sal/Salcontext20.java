package com.centura.api.sal;

import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CNumber;
import com.centura.api.type.CString;
import com.centura.api.type.CWndHandle;

public final class Salcontext20 {
  public static CDateTime DATETIME_Null;

  public static CNumber NUMBER_Null;

  public static CWndHandle OBJ_Null;

  public static CString STRING_Null;

  public static CString SqlDatabase;

  public static CNumber SqlInMessage;

  public static CNumber SqlIsolationLevel;

  public static CNumber SqlNoRecovery;

  public static CNumber SqlOutMessage;

  public static CString SqlPassword;

  public static CNumber SqlResultSet;

  public static CString SqlUser;

  public static CBoolean bStaticsAsWindows;

  public static CWndHandle hWndNULL;

  public static CNumber lParam;

  public static CNumber nArgCount;

  public static CString strArgArray;

  public static CNumber wParam;
}
