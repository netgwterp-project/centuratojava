package com.centura.api.impl;

import com.centura.api.cdk.ICenturaAPI;
import com.centura.api.type.CArray;
import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CFile;
import com.centura.api.type.CHandle;
import com.centura.api.type.CNumber;
import com.centura.api.type.CSQLHandle;
import com.centura.api.type.CString;
import com.centura.api.type.CStruct;
import com.centura.api.type.CWndHandle;

public class DefaultCenturaAPI implements ICenturaAPI {
  /**
   * HWND CBEXPAPI SWinAppFind(LPSTR lpModuleName, BOOL bActivate); file: centura200.h */
  @Override
public CWndHandle SWinAppFind(CString lpModuleName, CBoolean bActivate) {
    return null;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtDoubleToNumber(double, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SWinCvtDoubleToNumber(CNumber var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtIntToNumber(INT, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SWinCvtIntToNumber(CNumber var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtLongToNumber(LONG, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SWinCvtLongToNumber(CNumber var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToDouble(LPNUMBER, double FAR *); file: centura200.h */
  @Override
public CBoolean SWinCvtNumberToDouble(CNumber.ByReference var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToInt(LPNUMBER, LPINT); file: centura200.h */
  @Override
public CBoolean SWinCvtNumberToInt(CNumber.ByReference var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToLong(LPNUMBER, LPLONG); file: centura200.h */
  @Override
public CBoolean SWinCvtNumberToLong(CNumber.ByReference var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToULong(LPNUMBER, LPDWORD); file: centura200.h */
  @Override
public CBoolean SWinCvtNumberToULong(CNumber.ByReference var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtNumberToWord(LPNUMBER, LPWORD); file: centura200.h */
  @Override
public CBoolean SWinCvtNumberToWord(CNumber.ByReference var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtULongToNumber(ULONG, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SWinCvtULongToNumber(CNumber var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinCvtWordToNumber(WORD, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SWinCvtWordToNumber(CNumber var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * HWND CBEXPAPI SWinFindWindow(HWND hWndContainer, LPSTR lpszSymbol); file: centura200.h */
  @Override
public CWndHandle SWinFindWindow(CWndHandle hWndContainer, CString lpszSymbol) {
    return null;
  }

  /**
   * int  CBEXPAPI SWinGetSymbol(HWND hWnd, LPSTR lpszSymbol, int nMaxLength); file: centura200.h */
  @Override
public CNumber SWinGetSymbol(CWndHandle hWnd, CString lpszSymbol, CNumber nMaxLength) {
    return null;
  }

  /**
   * LONG CBEXPAPI SWinGetType(HWND hWnd); file: centura200.h */
  @Override
public CNumber SWinGetType(CWndHandle hWnd) {
    return null;
  }

  /**
   * BOOL CBEXPAPI SWinHSCreateDesignHeap(VOID); file: centura200.h */
  @Override
public CBoolean SWinHSCreateDesignHeap() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI SWinHSDestroyDesignHeap(VOID); file: centura200.h */
  @Override
public CBoolean SWinHSDestroyDesignHeap() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI SWinInitLPHSTRINGParam(LPHSTRING, LONG); file: centura200.h */
  @Override
public CBoolean SWinInitLPHSTRINGParam(CString.ByReference var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI SWinIsOurWindow(HWND hWnd); file: centura200.h */
  @Override
public CBoolean SWinIsOurWindow(CWndHandle hWnd) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL CBEXPAPI  SWinMDArrayDateType(HARRAY, LPINT); file: centura200.h */
  @Override
public CBoolean SWinMDArrayDateType(CArray<?> var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * LPSTR CBEXPAPI SWinStringGetBuffer(HSTRING, LPLONG); file: centura200.h */
  @Override
public CString SWinStringGetBuffer(CString var1, CNumber.ByReference var2) {
    return null;
  }

  /**
   * LPSTR   CBEXPAPI SWinUdvDeref( HUDV ); file: centura200.h */
  @Override
public CString SWinUdvDeref(CHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalAbort(INT); file: centura200.h */
  @Override
public CBoolean SalAbort(CNumber var1) {
    return CBoolean.FALSE;
  }

  /**
   * VOID      CBEXPAPI  SalActiveXAutoErrorMode(BOOL); file: centura200.h */
  @Override
public void SalActiveXAutoErrorMode(CBoolean var1) {
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXClose(HWND, BOOL); file: centura200.h */
  @Override
public CBoolean SalActiveXClose(CWndHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXCreate(HWND, LPCSTR); file: centura200.h */
  @Override
public CBoolean SalActiveXCreate(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXCreateFromData(HWND, HSTRING); file: centura200.h */
  @Override
public CBoolean SalActiveXCreateFromData(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXCreateFromFile(HWND, LPCSTR); file: centura200.h */
  @Override
public CBoolean SalActiveXCreateFromFile(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXDelete(HWND); file: centura200.h */
  @Override
public CBoolean SalActiveXDelete(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXDoVerb(HWND, LONG, BOOL); file: centura200.h */
  @Override
public CBoolean SalActiveXDoVerb(CWndHandle var1, CNumber var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXGetActiveObject(HUDV, LPSTR); file: centura200.h */
  @Override
public CBoolean SalActiveXGetActiveObject(CHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXGetData(HWND, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalActiveXGetData(CWndHandle var1, CString.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalActiveXGetObject(HWND, HUDV); file: centura200.h */
  @Override
public CBoolean SalActiveXGetObject(CWndHandle var1, CHandle var2) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalActiveXInsertObjectDlg(HWND); file: centura200.h */
  @Override
public CNumber SalActiveXInsertObjectDlg(CWndHandle var1) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalActiveXOLEType(HWND); file: centura200.h */
  @Override
public CNumber SalActiveXOLEType(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalAppDisable(VOID); file: centura200.h */
  @Override
public CBoolean SalAppDisable() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalAppEnable(VOID); file: centura200.h */
  @Override
public CBoolean SalAppEnable() {
    return CBoolean.FALSE;
  }

  /**
   * HWND      CBEXPAPI  SalAppFind(LPSTR, BOOL); file: centura200.h */
  @Override
public CWndHandle SalAppFind(CString var1, CBoolean var2) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalArrayAvg(HARRAY); file: centura200.h */
  @Override
public CNumber SalArrayAvg(CArray<?> var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalArrayDimCount(HARRAY, LPLONG); file: centura200.h */
  @Override
public CBoolean SalArrayDimCount(CArray<?> var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalArrayGetLowerBound(HARRAY, INT, LPLONG); file: centura200.h */
  @Override
public CBoolean SalArrayGetLowerBound(CArray<?> var1, CNumber var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalArrayGetUpperBound(HARRAY, INT, LPLONG); file: centura200.h */
  @Override
public CBoolean SalArrayGetUpperBound(CArray<?> var1, CNumber var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalArrayIsEmpty(HARRAY); file: centura200.h */
  @Override
public CBoolean SalArrayIsEmpty(CArray<?> var1) {
    return CBoolean.FALSE;
  }

  /**
   * NUMBER    CBEXPAPI  SalArrayMax(HARRAY); file: centura200.h */
  @Override
public CNumber SalArrayMax(CArray<?> var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalArrayMin(HARRAY); file: centura200.h */
  @Override
public CNumber SalArrayMin(CArray<?> var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalArraySetUpperBound(HARRAY, INT, LONG); file: centura200.h */
  @Override
public CBoolean SalArraySetUpperBound(CArray<?> var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * NUMBER    CBEXPAPI  SalArraySum(HARRAY); file: centura200.h */
  @Override
public CNumber SalArraySum(CArray<?> var1) {
    return null;
  }

  /**
   * VOID  CBEXPAPI SalAssignUDV(LPHANDLE A, LPHANDLE B, WORD fgs); file: centura200.h */
  @Override
public void SalAssignUDV(CHandle.ByReference A, CHandle.ByReference B, CNumber fgs) {
  }

  /**
   * BOOL      CBEXPAPI  SalBringWindowToTop(HWND); file: centura200.h */
  @Override
public CBoolean SalBringWindowToTop(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalCenterWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalCenterWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalClearField(HWND); file: centura200.h */
  @Override
public CBoolean SalClearField(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * DWORD     CBEXPAPI  SalColorFromRGB(BYTE, BYTE, BYTE); file: centura200.h */
  @Override
public CNumber SalColorFromRGB(CNumber var1, CNumber var2, CNumber var3) {
    return null;
  }

  /**
   * DWORD     CBEXPAPI  SalColorGet(HWND, INT); file: centura200.h */
  @Override
public CNumber SalColorGet(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * DWORD     CBEXPAPI  SalColorGetSysColor(INT); file: centura200.h */
  @Override
public CNumber SalColorGetSysColor(CNumber var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalColorSet(HWND, INT, DWORD); file: centura200.h */
  @Override
public CBoolean SalColorSet(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalColorToRGB(DWORD, LPBYTE, LPBYTE, LPBYTE); file: centura200.h */
  @Override
public CBoolean SalColorToRGB(CNumber var1, CNumber var2, CNumber var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalCompileAndEvaluate(LPSTR, LPINT, LPINT, LPNUMBER, LPHSTRING, LPDATETIME, LPHWND, BOOL, LPSTR); file: centura200.h */
  @Override
public CNumber SalCompileAndEvaluate(CString var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CString.ByReference var5,
      CDateTime.ByReference var6, CWndHandle.ByReference var7, CBoolean var8, CString var9) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalContextBreak(VOID); file: centura200.h */
  @Override
public CString SalContextBreak() {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalContextCurrent(VOID); file: centura200.h */
  @Override
public CString SalContextCurrent() {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalContextMenuSetPopup(HWND, LPSTR, DWORD); file: centura200.h */
  @Override
public CBoolean SalContextMenuSetPopup(CWndHandle var1, CString var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalCursorClear(HWND, WORD); file: centura200.h */
  @Override
public CBoolean SalCursorClear(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalCursorSet(HWND, TEMPLATE, WORD); file: centura200.h */
  @Override
public CBoolean SalCursorSet(CWndHandle var1, CStruct var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalCursorSetFile(HWND, LPSTR, WORD); file: centura200.h */
  @Override
public CBoolean SalCursorSetFile(CWndHandle var1, CString var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalCursorSetString(HWND, HSTRING, WORD); file: centura200.h */
  @Override
public CBoolean SalCursorSetString(CWndHandle var1, CString var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * ATOM      CBEXPAPI  SalDDEAddAtom(LPSTR); file: centura200.h */
  @Override
public CNumber SalDDEAddAtom(CString var1) {
    return null;
  }

  /**
   * HGLOBAL   CBEXPAPI  SalDDEAlloc(VOID); file: centura200.h */
  @Override
public CHandle SalDDEAlloc() {
    return null;
  }

  /**
   * ATOM      CBEXPAPI  SalDDEDeleteAtom(ATOM); file: centura200.h */
  @Override
public CNumber SalDDEDeleteAtom(CNumber var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtract(WPARAM, LPARAM, LPHWND, LPDWORD, LPDWORD); file: centura200.h */
  @Override
public CBoolean SalDDEExtract(CNumber var1, CNumber var2, CWndHandle.ByReference var3,
      CNumber var4, CNumber var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtractCmd(HGLOBAL, LPHSTRING, INT); file: centura200.h */
  @Override
public CBoolean SalDDEExtractCmd(CHandle var1, CString.ByReference var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtractDataText(HGLOBAL, LPWORD, LPHSTRING, INT); file: centura200.h */
  @Override
public CBoolean SalDDEExtractDataText(CHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEExtractOptions(HGLOBAL, LPWORD, LPWORD); file: centura200.h */
  @Override
public CBoolean SalDDEExtractOptions(CHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * ATOM      CBEXPAPI  SalDDEFindAtom(LPSTR); file: centura200.h */
  @Override
public CNumber SalDDEFindAtom(CString var1) {
    return null;
  }

  /**
   * HGLOBAL   CBEXPAPI  SalDDEFree(HGLOBAL); file: centura200.h */
  @Override
public CHandle SalDDEFree(CHandle var1) {
    return null;
  }

  /**
   * UINT      CBEXPAPI  SalDDEGetAtomName(ATOM, LPHSTRING, INT); file: centura200.h */
  @Override
public CNumber SalDDEGetAtomName(CNumber var1, CString.ByReference var2, CNumber var3) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalDDEGetExecuteString(LPARAM); file: centura200.h */
  @Override
public CString SalDDEGetExecuteString(CNumber var1) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalDDEPost(HWND, UINT, HWND, UINT, UINT); file: centura200.h */
  @Override
public CNumber SalDDEPost(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4,
      CNumber var5) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalDDERequest(HWND, LPSTR, LPSTR, LPSTR, LONG, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalDDERequest(CWndHandle var1, CString var2, CString var3, CString var4,
      CNumber var5, CString.ByReference var6) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalDDESend(HWND, UINT, HWND, UINT, UINT); file: centura200.h */
  @Override
public CNumber SalDDESend(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4,
      CNumber var5) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalDDESendAll(UINT, HWND, UINT, UINT); file: centura200.h */
  @Override
public CNumber SalDDESendAll(CNumber var1, CWndHandle var2, CNumber var3, CNumber var4) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalDDESendExecute(HWND, LPSTR, LPSTR, LPSTR, LONG, LPSTR); file: centura200.h */
  @Override
public CBoolean SalDDESendExecute(CWndHandle var1, CString var2, CString var3, CString var4,
      CNumber var5, CString var6) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDESendToClient(HWND, LPSTR, WPARAM, LONG); file: centura200.h */
  @Override
public CBoolean SalDDESendToClient(CWndHandle var1, CString var2, CNumber var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDESetCmd(HGLOBAL, LPSTR); file: centura200.h */
  @Override
public CBoolean SalDDESetCmd(CHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDESetDataText(HGLOBAL, WORD, LPSTR); file: centura200.h */
  @Override
public CBoolean SalDDESetDataText(CHandle var1, CNumber var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDESetOptions(HGLOBAL, WORD, WORD); file: centura200.h */
  @Override
public CBoolean SalDDESetOptions(CHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStartServer(HWND, LPSTR, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SalDDEStartServer(CWndHandle var1, CString var2, CString var3, CString var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStartSession(HWND, LPSTR, LPSTR, LPSTR, LONG); file: centura200.h */
  @Override
public CBoolean SalDDEStartSession(CWndHandle var1, CString var2, CString var3, CString var4,
      CNumber var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStopServer(HWND); file: centura200.h */
  @Override
public CBoolean SalDDEStopServer(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDDEStopSession(HWND); file: centura200.h */
  @Override
public CBoolean SalDDEStopSession(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * DATETIME  CBEXPAPI  SalDateConstruct(INT, INT, INT, INT, INT, INT); file: centura200.h */
  @Override
public CDateTime SalDateConstruct(CNumber var1, CNumber var2, CNumber var3, CNumber var4,
      CNumber var5, CNumber var6) {
    return null;
  }

  /**
   * DATETIME  CBEXPAPI  SalDateCurrent(VOID); file: centura200.h */
  @Override
public CDateTime SalDateCurrent() {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateDay(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateDay(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateHour(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateHour(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateMinute(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateMinute(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateMonth(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateMonth(CDateTime var1) {
    return null;
  }

  /**
   * DATETIME  CBEXPAPI  SalDateMonthBegin(DATETIME); file: centura200.h */
  @Override
public CDateTime SalDateMonthBegin(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateQuarter(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateQuarter(CDateTime var1) {
    return null;
  }

  /**
   * DATETIME  CBEXPAPI  SalDateQuarterBegin(DATETIME); file: centura200.h */
  @Override
public CDateTime SalDateQuarterBegin(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateSecond(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateSecond(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateToStr(DATETIME, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalDateToStr(CDateTime var1, CString.ByReference var2) {
    return null;
  }

  /**
   * DATETIME  CBEXPAPI  SalDateWeekBegin(DATETIME); file: centura200.h */
  @Override
public CDateTime SalDateWeekBegin(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateWeekday(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateWeekday(CDateTime var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalDateYear(DATETIME); file: centura200.h */
  @Override
public CNumber SalDateYear(CDateTime var1) {
    return null;
  }

  /**
   * DATETIME  CBEXPAPI  SalDateYearBegin(DATETIME); file: centura200.h */
  @Override
public CDateTime SalDateYearBegin(CDateTime var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalDestroyWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalDestroyWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDisableWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalDisableWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDisableWindowAndLabel(HWND); file: centura200.h */
  @Override
public CBoolean SalDisableWindowAndLabel(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDlgChooseColor(HWND, LPDWORD); file: centura200.h */
  @Override
public CBoolean SalDlgChooseColor(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDlgChooseFont(HWND, LPHSTRING, LPINT, LPWORD, LPDWORD); file: centura200.h */
  @Override
public CBoolean SalDlgChooseFont(CWndHandle var1, CString.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CNumber var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDlgOpenFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalDlgOpenFile(CWndHandle var1, CString var2, CArray<?> var3, CNumber var4,
      CNumber.ByReference var5, CString.ByReference var6, CString.ByReference var7) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDlgSaveFile(HWND, LPSTR, HARRAY, INT, LPINT, LPHSTRING, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalDlgSaveFile(CWndHandle var1, CString var2, CArray<?> var3, CNumber var4,
      CNumber.ByReference var5, CString.ByReference var6, CString.ByReference var7) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropDisableDrop(VOID); file: centura200.h */
  @Override
public CBoolean SalDragDropDisableDrop() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropEnableDrop(VOID); file: centura200.h */
  @Override
public CBoolean SalDragDropEnableDrop() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropGetSource(LPHWND, LPINT, LPINT); file: centura200.h */
  @Override
public CBoolean SalDragDropGetSource(CWndHandle.ByReference var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropGetTarget(LPHWND, LPINT, LPINT); file: centura200.h */
  @Override
public CBoolean SalDragDropGetTarget(CWndHandle.ByReference var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropStart(HWND); file: centura200.h */
  @Override
public CBoolean SalDragDropStart(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDragDropStop(VOID); file: centura200.h */
  @Override
public CBoolean SalDragDropStop() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDrawMenuBar(HWND); file: centura200.h */
  @Override
public CBoolean SalDrawMenuBar(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalDropFilesAcceptFiles(HWND, BOOL); file: centura200.h */
  @Override
public CBoolean SalDropFilesAcceptFiles(CWndHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalDropFilesQueryFiles(HWND, HARRAY); file: centura200.h */
  @Override
public CNumber SalDropFilesQueryFiles(CWndHandle var1, CArray<?> var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalDropFilesQueryPoint(HWND, LPINT, LPINT); file: centura200.h */
  @Override
public CBoolean SalDropFilesQueryPoint(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanCopyTo(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCanCopyTo() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanCut(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCanCut() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanPaste(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCanPaste() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanPasteFrom(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCanPasteFrom() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCanUndo(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCanUndo() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditClear(VOID); file: centura200.h */
  @Override
public CBoolean SalEditClear() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCopy(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCopy() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCopyString(LPSTR); file: centura200.h */
  @Override
public CBoolean SalEditCopyString(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCopyTo(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCopyTo() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditCut(VOID); file: centura200.h */
  @Override
public CBoolean SalEditCut() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditPaste(VOID); file: centura200.h */
  @Override
public CBoolean SalEditPaste() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditPasteFrom(VOID); file: centura200.h */
  @Override
public CBoolean SalEditPasteFrom() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditPasteString(LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalEditPasteString(CString.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEditUndo(VOID); file: centura200.h */
  @Override
public CBoolean SalEditUndo() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEnableWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalEnableWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEnableWindowAndLabel(HWND); file: centura200.h */
  @Override
public CBoolean SalEnableWindowAndLabel(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalEndDialog(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalEndDialog(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileClose(LPHFFILE); file: centura200.h */
  @Override
public CBoolean SalFileClose(CFile.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalFileCopy(LPSTR, LPSTR, BOOL); file: centura200.h */
  @Override
public CNumber SalFileCopy(CString var1, CString var2, CBoolean var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalFileCreateDirectory(LPSTR); file: centura200.h */
  @Override
public CBoolean SalFileCreateDirectory(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetC(HFFILE, LPWORD); file: centura200.h */
  @Override
public CBoolean SalFileGetC(CFile var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalFileGetChar(HFFILE); file: centura200.h */
  @Override
public CNumber SalFileGetChar(CFile var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetCurrentDirectory(LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalFileGetCurrentDirectory(CString.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetDateTime(LPSTR, LPDATETIME); file: centura200.h */
  @Override
public CBoolean SalFileGetDateTime(CString var1, CDateTime.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * HSTRING   CBEXPAPI  SalFileGetDrive(VOID); file: centura200.h */
  @Override
public CString SalFileGetDrive() {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalFileGetStr(HFFILE, LPHSTRING, INT); file: centura200.h */
  @Override
public CBoolean SalFileGetStr(CFile var1, CString.ByReference var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileOpen(LPHFFILE, LPSTR, LONG); file: centura200.h */
  @Override
public CBoolean SalFileOpen(CFile.ByReference var1, CString var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileOpenExt(LPHFFILE, LPSTR, LONG, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalFileOpenExt(CFile.ByReference var1, CString var2, CNumber var3,
      CString.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFilePutC(HFFILE, WORD); file: centura200.h */
  @Override
public CBoolean SalFilePutC(CFile var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFilePutChar(HFFILE, INT); file: centura200.h */
  @Override
public CBoolean SalFilePutChar(CFile var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalFilePutStr(HFFILE, LPSTR); file: centura200.h */
  @Override
public CNumber SalFilePutStr(CFile var1, CString var2) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalFileRead(HFFILE, LPHSTRING, LONG); file: centura200.h */
  @Override
public CNumber SalFileRead(CFile var1, CString.ByReference var2, CNumber var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalFileRemoveDirectory(LPSTR); file: centura200.h */
  @Override
public CBoolean SalFileRemoveDirectory(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileSeek(HFFILE, LONG, INT); file: centura200.h */
  @Override
public CBoolean SalFileSeek(CFile var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileSetCurrentDirectory(LPSTR); file: centura200.h */
  @Override
public CBoolean SalFileSetCurrentDirectory(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileSetDateTime(LPSTR, DATETIME); file: centura200.h */
  @Override
public CBoolean SalFileSetDateTime(CString var1, CDateTime var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFileSetDrive(LPSTR); file: centura200.h */
  @Override
public CBoolean SalFileSetDrive(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalFileTell(HFFILE); file: centura200.h */
  @Override
public CNumber SalFileTell(CFile var1) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalFileWrite(HFFILE, HSTRING, LONG); file: centura200.h */
  @Override
public CNumber SalFileWrite(CFile var1, CString var2, CNumber var3) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalFindWindow(HWND, LPSTR); file: centura200.h */
  @Override
public CWndHandle SalFindWindow(CWndHandle var1, CString var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalFireEvent(HITEM, ...); file: centura200.h */
  @Override
public CBoolean SalFireEvent(CHandle var1, CArray<?> var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtFieldToStr(HWND, LPHSTRING, BOOL); file: centura200.h */
  @Override
public CBoolean SalFmtFieldToStr(CWndHandle var1, CString.ByReference var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * HSTRING   CBEXPAPI  SalFmtFormatDateTime(DATETIME, LPSTR); file: centura200.h */
  @Override
public CString SalFmtFormatDateTime(CDateTime var1, CString var2) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalFmtFormatNumber(NUMBER, LPSTR); file: centura200.h */
  @Override
public CString SalFmtFormatNumber(CNumber var1, CString var2) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalFmtGetFormat(HWND); file: centura200.h */
  @Override
public CNumber SalFmtGetFormat(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtGetInputMask(HWND, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalFmtGetInputMask(CWndHandle var1, CString.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtGetPicture(HWND, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalFmtGetPicture(CWndHandle var1, CString.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidField(HWND); file: centura200.h */
  @Override
public CBoolean SalFmtIsValidField(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidInputMask(LPSTR); file: centura200.h */
  @Override
public CBoolean SalFmtIsValidInputMask(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtIsValidPicture(LPSTR, INT); file: centura200.h */
  @Override
public CBoolean SalFmtIsValidPicture(CString var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtKeepMask(BOOL); file: centura200.h */
  @Override
public CBoolean SalFmtKeepMask(CBoolean var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtSetFormat(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalFmtSetFormat(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtSetInputMask(HWND, LPSTR); file: centura200.h */
  @Override
public CBoolean SalFmtSetInputMask(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtSetPicture(HWND, LPSTR); file: centura200.h */
  @Override
public CBoolean SalFmtSetPicture(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtStrToField(HWND, LPSTR, BOOL); file: centura200.h */
  @Override
public CBoolean SalFmtStrToField(CWndHandle var1, CString var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtUnmaskInput(HWND, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalFmtUnmaskInput(CWndHandle var1, CString.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFmtValidateField(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalFmtValidateField(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFontGet(HWND, LPHSTRING, LPINT, LPWORD); file: centura200.h */
  @Override
public CBoolean SalFontGet(CWndHandle var1, CString.ByReference var2, CNumber.ByReference var3,
      CNumber.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalFontGetNames(WORD, HARRAY); file: centura200.h */
  @Override
public CNumber SalFontGetNames(CNumber var1, CArray<?> var2) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalFontGetSizes(WORD, LPSTR, HARRAY); file: centura200.h */
  @Override
public CNumber SalFontGetSizes(CNumber var1, CString var2, CArray<?> var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalFontSet(HWND, LPSTR, INT, WORD); file: centura200.h */
  @Override
public CBoolean SalFontSet(CWndHandle var1, CString var2, CNumber var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalFormGetParmNum(HWND, INT, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SalFormGetParmNum(CWndHandle var1, CNumber var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalFormUnitsToPixels(HWND, NUMBER, BOOL); file: centura200.h */
  @Override
public CNumber SalFormUnitsToPixels(CWndHandle var1, CNumber var2, CBoolean var3) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalGetDataType(HWND); file: centura200.h */
  @Override
public CNumber SalGetDataType(CWndHandle var1) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalGetDefButton(HWND); file: centura200.h */
  @Override
public CWndHandle SalGetDefButton(CWndHandle var1) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalGetFirstChild(HWND, LONG); file: centura200.h */
  @Override
public CWndHandle SalGetFirstChild(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalGetFocus(VOID); file: centura200.h */
  @Override
public CWndHandle SalGetFocus() {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalGetItemName(HWND, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalGetItemName(CWndHandle var1, CString.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalGetMaxDataLength(HWND); file: centura200.h */
  @Override
public CNumber SalGetMaxDataLength(CWndHandle var1) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalGetNextChild(HWND, LONG); file: centura200.h */
  @Override
public CWndHandle SalGetNextChild(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * UINT      CBEXPAPI  SalGetProfileInt(LPCSTR, LPCSTR, INT, LPCSTR); file: centura200.h */
  @Override
public CNumber SalGetProfileInt(CString var1, CString var2, CNumber var3, CString var4) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalGetProfileString(LPCSTR, LPCSTR, LPCSTR, LPHSTRING, LPCSTR); file: centura200.h */
  @Override
public CBoolean SalGetProfileString(CString var1, CString var2, CString var3,
      CString.ByReference var4, CString var5) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalGetType(HWND); file: centura200.h */
  @Override
public CNumber SalGetType(CWndHandle var1) {
    return null;
  }

  /**
   * LPSTR CBEXPAPI SalGetUDVData(HANDLE h); file: centura200.h */
  @Override
public CString SalGetUDVData(CHandle h) {
    return null;
  }

  /**
   * WORD      CBEXPAPI  SalGetVersion(VOID); file: centura200.h */
  @Override
public CNumber SalGetVersion() {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalGetWindowLabelText(HWND, LPHSTRING, INT); file: centura200.h */
  @Override
public CNumber SalGetWindowLabelText(CWndHandle var1, CString.ByReference var2, CNumber var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalGetWindowLoc(HWND, LPNUMBER, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SalGetWindowLoc(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalGetWindowSize(HWND, LPNUMBER, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SalGetWindowSize(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalGetWindowState(HWND); file: centura200.h */
  @Override
public CNumber SalGetWindowState(CWndHandle var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalGetWindowText(HWND, LPHSTRING, INT); file: centura200.h */
  @Override
public CNumber SalGetWindowText(CWndHandle var1, CString.ByReference var2, CNumber var3) {
    return null;
  }

  /**
   * void CBEXPAPI SalHStringRef(HSTRING hString); file: centura200.h */
  @Override
public void SalHStringRef(CString hString) {
  }

  /**
   * DWORD     CBEXPAPI  SalHStringToNumber(HSTRING); file: centura200.h */
  @Override
public CNumber SalHStringToNumber(CString var1) {
    return null;
  }

  /**
   * HSTRING CBEXPAPI SalHStringUnRef(HSTRING hString); file: centura200.h */
  @Override
public CString SalHStringUnRef(CString hString) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalHideWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalHideWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalHideWindowAndLabel(HWND); file: centura200.h */
  @Override
public CBoolean SalHideWindowAndLabel(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIdleKick(VOID); file: centura200.h */
  @Override
public CBoolean SalIdleKick() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIdleRegisterWindow(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  @Override
public CBoolean SalIdleRegisterWindow(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIdleUnregisterWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalIdleUnregisterWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalInvalidateWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalInvalidateWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsButtonChecked(HWND); file: centura200.h */
  @Override
public CBoolean SalIsButtonChecked(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsNull(HWND); file: centura200.h */
  @Override
public CBoolean SalIsNull(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidDateTime(HWND); file: centura200.h */
  @Override
public CBoolean SalIsValidDateTime(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidDecimal(HWND, INT, INT); file: centura200.h */
  @Override
public CBoolean SalIsValidDecimal(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidInteger(HWND); file: centura200.h */
  @Override
public CBoolean SalIsValidInteger(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsValidNumber(HWND); file: centura200.h */
  @Override
public CBoolean SalIsValidNumber(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsWindowEnabled(HWND); file: centura200.h */
  @Override
public CBoolean SalIsWindowEnabled(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalIsWindowVisible(HWND); file: centura200.h */
  @Override
public CBoolean SalIsWindowVisible(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalListAdd(HWND, LPSTR); file: centura200.h */
  @Override
public CNumber SalListAdd(CWndHandle var1, CString var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalListClear(HWND); file: centura200.h */
  @Override
public CBoolean SalListClear(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalListDelete(HWND, INT); file: centura200.h */
  @Override
public CNumber SalListDelete(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalListFiles(HWND, HWND, LPHSTRING, WORD); file: centura200.h */
  @Override
public CBoolean SalListFiles(CWndHandle var1, CWndHandle var2, CString.ByReference var3,
      CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalListGetMultiSelect(HWND, HARRAY); file: centura200.h */
  @Override
public CBoolean SalListGetMultiSelect(CWndHandle var1, CArray<?> var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalListInsert(HWND, INT, LPSTR); file: centura200.h */
  @Override
public CNumber SalListInsert(CWndHandle var1, CNumber var2, CString var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalListPopulate(HWND, SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SalListPopulate(CWndHandle var1, CSQLHandle var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalListQueryCount(HWND); file: centura200.h */
  @Override
public CNumber SalListQueryCount(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalListQueryFile(HWND, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalListQueryFile(CWndHandle var1, CString.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * NUMBER    CBEXPAPI  SalListQueryMultiCount(HWND); file: centura200.h */
  @Override
public CNumber SalListQueryMultiCount(CWndHandle var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalListQuerySelection(HWND); file: centura200.h */
  @Override
public CNumber SalListQuerySelection(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalListQueryState(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalListQueryState(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalListQueryText(HWND, INT, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalListQueryText(CWndHandle var1, CNumber var2, CString.ByReference var3) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalListQueryTextLength(HWND, INT); file: centura200.h */
  @Override
public CNumber SalListQueryTextLength(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalListQueryTextX(HWND, INT); file: centura200.h */
  @Override
public CString SalListQueryTextX(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalListRedraw(HWND, BOOL); file: centura200.h */
  @Override
public CBoolean SalListRedraw(CWndHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalListSelectString(HWND, INT, LPSTR); file: centura200.h */
  @Override
public CNumber SalListSelectString(CWndHandle var1, CNumber var2, CString var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalListSetMultiSelect(HWND, INT, BOOL); file: centura200.h */
  @Override
public CBoolean SalListSetMultiSelect(CWndHandle var1, CNumber var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalListSetSelect(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalListSetSelect(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalListSetTabs(HWND, HARRAY); file: centura200.h */
  @Override
public CBoolean SalListSetTabs(CWndHandle var1, CArray<?> var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalLoadApp(LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SalLoadApp(CString var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalLoadAppAndWait(LPSTR, WORD, LPDWORD); file: centura200.h */
  @Override
public CBoolean SalLoadAppAndWait(CString var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMDIArrangeIcons(HWND); file: centura200.h */
  @Override
public CBoolean SalMDIArrangeIcons(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMDICascade(HWND); file: centura200.h */
  @Override
public CBoolean SalMDICascade(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMDITile(HWND, BOOL); file: centura200.h */
  @Override
public CBoolean SalMDITile(CWndHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSCreateInstance(HUDV); file: centura200.h */
  @Override
public CBoolean SalMTSCreateInstance(CHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSDisableCommit(VOID); file: centura200.h */
  @Override
public CBoolean SalMTSDisableCommit() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSEnableCommit(VOID); file: centura200.h */
  @Override
public CBoolean SalMTSEnableCommit() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSGetObjectContext(LPLONG); file: centura200.h */
  @Override
public CBoolean SalMTSGetObjectContext(CNumber.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSIsCallerInRole(LPSTR, LPBOOL); file: centura200.h */
  @Override
public CBoolean SalMTSIsCallerInRole(CString var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSIsInTransaction(LPBOOL); file: centura200.h */
  @Override
public CBoolean SalMTSIsInTransaction(CBoolean var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSIsSecurityEnabled(LPBOOL); file: centura200.h */
  @Override
public CBoolean SalMTSIsSecurityEnabled(CBoolean var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSSetAbort(VOID); file: centura200.h */
  @Override
public CBoolean SalMTSSetAbort() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMTSSetComplete(VOID); file: centura200.h */
  @Override
public CBoolean SalMTSSetComplete() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMapEnterToTab(BOOL); file: centura200.h */
  @Override
public CBoolean SalMapEnterToTab(CBoolean var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalMessageBeep(WPARAM); file: centura200.h */
  @Override
public CBoolean SalMessageBeep(CNumber var1) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalMessageBox(LPSTR, LPSTR, UINT); file: centura200.h */
  @Override
public CNumber SalMessageBox(CString var1, CString var2, CNumber var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalMoveWindow(HWND, NUMBER, NUMBER); file: centura200.h */
  @Override
public CBoolean SalMoveWindow(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * LPSTR CBEXPAPI SalNewUDVObject(LPSTR); file: centura200.h */
  @Override
public CString SalNewUDVObject(CString var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberAbs(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberAbs(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcCos(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberArcCos(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcSin(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberArcSin(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcTan(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberArcTan(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberArcTan2(NUMBER, NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberArcTan2(CNumber var1, CNumber var2) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberCos(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberCos(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberCosH(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberCosH(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberExponent(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberExponent(CNumber var1) {
    return null;
  }

  /**
   * WORD      CBEXPAPI  SalNumberHigh(DWORD); file: centura200.h */
  @Override
public CNumber SalNumberHigh(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberHypot(NUMBER, NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberHypot(CNumber var1, CNumber var2) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberLog(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberLog(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberLogBase10(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberLogBase10(CNumber var1) {
    return null;
  }

  /**
   * WORD      CBEXPAPI  SalNumberLow(DWORD); file: centura200.h */
  @Override
public CNumber SalNumberLow(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberMax(NUMBER, NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberMax(CNumber var1, CNumber var2) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberMin(NUMBER, NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberMin(CNumber var1, CNumber var2) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberMod(NUMBER, NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberMod(CNumber var1, CNumber var2) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberPi(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberPi(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberPower(NUMBER, NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberPower(CNumber var1, CNumber var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalNumberRandInit(WORD); file: centura200.h */
  @Override
public CBoolean SalNumberRandInit(CNumber var1) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalNumberRandom(VOID); file: centura200.h */
  @Override
public CNumber SalNumberRandom() {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberRound(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberRound(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberSin(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberSin(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberSinH(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberSinH(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberSqrt(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberSqrt(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberTan(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberTan(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberTanH(NUMBER); file: centura200.h */
  @Override
public CNumber SalNumberTanH(CNumber var1) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalNumberToChar(WORD); file: centura200.h */
  @Override
public CString SalNumberToChar(CNumber var1) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalNumberToHString(DWORD); file: centura200.h */
  @Override
public CString SalNumberToHString(CNumber var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalNumberToStr(NUMBER, INT, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalNumberToStr(CNumber var1, CNumber var2, CString.ByReference var3) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalNumberToStrX(NUMBER, INT); file: centura200.h */
  @Override
public CString SalNumberToStrX(CNumber var1, CNumber var2) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalNumberToWindowHandle(UINT); file: centura200.h */
  @Override
public CWndHandle SalNumberToWindowHandle(CNumber var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalNumberTruncate(NUMBER, INT, INT); file: centura200.h */
  @Override
public CNumber SalNumberTruncate(CNumber var1, CNumber var2, CNumber var3) {
    return null;
  }

  /**
   * HUDV      CBEXPAPI  SalObjCreateFromString(LPSTR); file: centura200.h */
  @Override
public CHandle SalObjCreateFromString(CString var1) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalObjGetType(HUDV); file: centura200.h */
  @Override
public CString SalObjGetType(CHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalObjIsDerived(HUDV, LPSTR); file: centura200.h */
  @Override
public CBoolean SalObjIsDerived(CHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalObjIsNull(HUDV); file: centura200.h */
  @Override
public CBoolean SalObjIsNull(CHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalObjIsValidClassName(LPSTR); file: centura200.h */
  @Override
public CBoolean SalObjIsValidClassName(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * HWND      CBEXPAPI  SalParentWindow(HWND); file: centura200.h */
  @Override
public CWndHandle SalParentWindow(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalPicClear(HWND); file: centura200.h */
  @Override
public CBoolean SalPicClear(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalPicGetDescription(HWND, LPHSTRING, INT); file: centura200.h */
  @Override
public CNumber SalPicGetDescription(CWndHandle var1, CString.ByReference var2, CNumber var3) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalPicGetImage(HWND, LPHSTRING, LPINT); file: centura200.h */
  @Override
public CNumber SalPicGetImage(CWndHandle var1, CString.ByReference var2,
      CNumber.ByReference var3) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalPicGetString(HWND, INT, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalPicGetString(CWndHandle var1, CNumber var2, CString.ByReference var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalPicSet(HWND, HITEM, INT); file: centura200.h */
  @Override
public CBoolean SalPicSet(CWndHandle var1, CHandle var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetFile(HWND, LPSTR); file: centura200.h */
  @Override
public CBoolean SalPicSetFile(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetFit(HWND, INT, INT, INT); file: centura200.h */
  @Override
public CBoolean SalPicSetFit(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetHandle(HWND, INT, DWORD); file: centura200.h */
  @Override
public CBoolean SalPicSetHandle(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetImage(HWND, HSTRING, INT); file: centura200.h */
  @Override
public CBoolean SalPicSetImage(CWndHandle var1, CString var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPicSetString(HWND, INT, HSTRING); file: centura200.h */
  @Override
public CBoolean SalPicSetString(CWndHandle var1, CNumber var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * NUMBER    CBEXPAPI  SalPixelsToFormUnits(HWND, INT, BOOL); file: centura200.h */
  @Override
public CNumber SalPixelsToFormUnits(CWndHandle var1, CNumber var2, CBoolean var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalPostMsg(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  @Override
public CBoolean SalPostMsg(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtExtractRect(LONG, LPINT, LPINT, LPINT, LPINT); file: centura200.h */
  @Override
public CBoolean SalPrtExtractRect(CNumber var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CNumber.ByReference var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtGetDefault(LPHSTRING, LPHSTRING, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalPrtGetDefault(CString.ByReference var1, CString.ByReference var2,
      CString.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtGetParmNum(INT, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SalPrtGetParmNum(CNumber var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtPrintForm(HWND); file: centura200.h */
  @Override
public CBoolean SalPrtPrintForm(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetDefault(LPSTR, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SalPrtSetDefault(CString var1, CString var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetParmDefaults(VOID); file: centura200.h */
  @Override
public CBoolean SalPrtSetParmDefaults() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetParmNum(INT, NUMBER); file: centura200.h */
  @Override
public CBoolean SalPrtSetParmNum(CNumber var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalPrtSetup(LPHSTRING, LPHSTRING, LPHSTRING, BOOL); file: centura200.h */
  @Override
public CBoolean SalPrtSetup(CString.ByReference var1, CString.ByReference var2,
      CString.ByReference var3, CBoolean var4) {
    return CBoolean.FALSE;
  }

  /**
   * DWORD     CBEXPAPI  SalQueryArrayBounds(HARRAY, LPLONG, LPLONG); file: centura200.h */
  @Override
public CNumber SalQueryArrayBounds(CArray<?> var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalQueryFieldEdit(HWND); file: centura200.h */
  @Override
public CBoolean SalQueryFieldEdit(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalQuit(VOID); file: centura200.h */
  @Override
public CBoolean SalQuit() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportClose(HWND); file: centura200.h */
  @Override
public CBoolean SalReportClose(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportCmd(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalReportCmd(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportCreate(LPSTR, LPSTR, LPSTR, BOOL, LPINT); file: centura200.h */
  @Override
public CBoolean SalReportCreate(CString var1, CString var2, CString var3, CBoolean var4,
      CNumber.ByReference var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportDlgOptions(HWND, LPSTR, LPSTR, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SalReportDlgOptions(CWndHandle var1, CString var2, CString var3, CString var4,
      CString var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetDateTimeVar(HWND, LPSTR, LPDATETIME); file: centura200.h */
  @Override
public CBoolean SalReportGetDateTimeVar(CWndHandle var1, CString var2,
      CDateTime.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetNumberVar(HWND, LPSTR, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SalReportGetNumberVar(CWndHandle var1, CString var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetObjectVar(HWND, LPSTR, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalReportGetObjectVar(CWndHandle var1, CString var2, CString.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportGetStringVar(HWND, LPSTR, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalReportGetStringVar(CWndHandle var1, CString var2, CString.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * HWND      CBEXPAPI  SalReportPrint(HWND, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, LPINT); file: centura200.h */
  @Override
public CWndHandle SalReportPrint(CWndHandle var1, CString var2, CString var3, CString var4,
      CNumber var5, CNumber var6, CNumber var7, CNumber var8, CNumber.ByReference var9) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalReportPrintToFile(HWND, LPSTR, LPSTR, LPSTR, LPSTR, INT, INT, INT, INT, BOOL, LPINT); file: centura200.h */
  @Override
public CWndHandle SalReportPrintToFile(CWndHandle var1, CString var2, CString var3, CString var4,
      CString var5, CNumber var6, CNumber var7, CNumber var8, CNumber var9, CBoolean var10,
      CNumber.ByReference var11) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalReportReset(HWND); file: centura200.h */
  @Override
public CBoolean SalReportReset(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetDateTimeVar(HWND, LPSTR, DATETIME); file: centura200.h */
  @Override
public CBoolean SalReportSetDateTimeVar(CWndHandle var1, CString var2, CDateTime var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetNumberVar(HWND, LPSTR, NUMBER); file: centura200.h */
  @Override
public CBoolean SalReportSetNumberVar(CWndHandle var1, CString var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetObjectVar(HWND, LPSTR, HSTRING); file: centura200.h */
  @Override
public CBoolean SalReportSetObjectVar(CWndHandle var1, CString var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportSetStringVar(HWND, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SalReportSetStringVar(CWndHandle var1, CString var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalReportTableCreate(LPSTR, HWND, LPINT); file: centura200.h */
  @Override
public CBoolean SalReportTableCreate(CString var1, CWndHandle var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * HWND      CBEXPAPI  SalReportTablePrint(HWND, LPSTR, HARRAY, LPINT); file: centura200.h */
  @Override
public CWndHandle SalReportTablePrint(CWndHandle var1, CString var2, CArray<?> var3,
      CNumber.ByReference var4) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalReportTableView(HWND, HWND, LPSTR, LPINT); file: centura200.h */
  @Override
public CWndHandle SalReportTableView(CWndHandle var1, CWndHandle var2, CString var3,
      CNumber.ByReference var4) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalReportView(HWND, HWND, LPSTR, LPSTR, LPSTR, LPINT); file: centura200.h */
  @Override
public CWndHandle SalReportView(CWndHandle var1, CWndHandle var2, CString var3, CString var4,
      CString var5, CNumber.ByReference var6) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalScrollGetPos(HWND, LPINT); file: centura200.h */
  @Override
public CBoolean SalScrollGetPos(CWndHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalScrollGetRange(HWND, LPINT, LPINT, LPINT, LPINT); file: centura200.h */
  @Override
public CBoolean SalScrollGetRange(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4, CNumber.ByReference var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalScrollSetPos(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalScrollSetPos(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalScrollSetRange(HWND, INT, INT, INT, INT); file: centura200.h */
  @Override
public CBoolean SalScrollSetRange(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4,
      CNumber var5) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalSendClassMessage(UINT, WPARAM, LPARAM); file: centura200.h */
  @Override
public CNumber SalSendClassMessage(CNumber var1, CNumber var2, CNumber var3) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalSendClassMessageNamed(HITEM, UINT, WPARAM, LPARAM); file: centura200.h */
  @Override
public CNumber SalSendClassMessageNamed(CHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalSendMsg(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  @Override
public CNumber SalSendMsg(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalSendMsgToChildren(HWND, UINT, WPARAM, LPARAM); file: centura200.h */
  @Override
public CBoolean SalSendMsgToChildren(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * WORD      CBEXPAPI  SalSendValidateMsg(VOID); file: centura200.h */
  @Override
public CNumber SalSendValidateMsg() {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalSetArrayBounds(HARRAY, LONG, LONG); file: centura200.h */
  @Override
public CBoolean SalSetArrayBounds(CArray<?> var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetDefButton(HWND); file: centura200.h */
  @Override
public CBoolean SalSetDefButton(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetErrorInfo(LONG, LPSTR, LPSTR, DWORD); file: centura200.h */
  @Override
public CBoolean SalSetErrorInfo(CNumber var1, CString var2, CString var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetFieldEdit(HWND, BOOL); file: centura200.h */
  @Override
public CBoolean SalSetFieldEdit(CWndHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * HWND      CBEXPAPI  SalSetFocus(HWND); file: centura200.h */
  @Override
public CWndHandle SalSetFocus(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalSetMaxDataLength(HWND, LONG); file: centura200.h */
  @Override
public CBoolean SalSetMaxDataLength(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetProfileString(LPCSTR, LPCSTR, LPCSTR, LPCSTR); file: centura200.h */
  @Override
public CBoolean SalSetProfileString(CString var1, CString var2, CString var3, CString var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowLabelText(HWND, LPSTR); file: centura200.h */
  @Override
public CBoolean SalSetWindowLabelText(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowLoc(HWND, NUMBER, NUMBER); file: centura200.h */
  @Override
public CBoolean SalSetWindowLoc(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowSize(HWND, NUMBER, NUMBER); file: centura200.h */
  @Override
public CBoolean SalSetWindowSize(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalSetWindowText(HWND, LPSTR); file: centura200.h */
  @Override
public CBoolean SalSetWindowText(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalShowWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalShowWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalShowWindowAndLabel(HWND); file: centura200.h */
  @Override
public CBoolean SalShowWindowAndLabel(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalStatusGetText(HWND, LPHSTRING, INT); file: centura200.h */
  @Override
public CNumber SalStatusGetText(CWndHandle var1, CString.ByReference var2, CNumber var3) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalStatusSetText(HWND, LPSTR); file: centura200.h */
  @Override
public CBoolean SalStatusSetText(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalStatusSetVisible(HWND, BOOL); file: centura200.h */
  @Override
public CBoolean SalStatusSetVisible(CWndHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalStrCompress(LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalStrCompress(CString.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalStrFirstC(LPHSTRING, LPWORD); file: centura200.h */
  @Override
public CBoolean SalStrFirstC(CString.ByReference var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalStrGetBufferLength(HSTRING); file: centura200.h */
  @Override
public CNumber SalStrGetBufferLength(CString var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalStrIsValidCurrency(LPSTR, INT, INT); file: centura200.h */
  @Override
public CBoolean SalStrIsValidCurrency(CString var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalStrIsValidDateTime(LPSTR); file: centura200.h */
  @Override
public CBoolean SalStrIsValidDateTime(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalStrIsValidNumber(LPSTR); file: centura200.h */
  @Override
public CBoolean SalStrIsValidNumber(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalStrLeft(HSTRING, LONG, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrLeft(CString var1, CNumber var2, CString.ByReference var3) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrLeftX(HSTRING, LONG); file: centura200.h */
  @Override
public CString SalStrLeftX(CString var1, CNumber var2) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalStrLength(LPSTR); file: centura200.h */
  @Override
public CNumber SalStrLength(CString var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalStrLop(LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrLop(CString.ByReference var1) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalStrLower(HSTRING, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrLower(CString var1, CString.ByReference var2) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrLowerX(HSTRING); file: centura200.h */
  @Override
public CString SalStrLowerX(CString var1) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalStrMid(HSTRING, LONG, LONG, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrMid(CString var1, CNumber var2, CNumber var3, CString.ByReference var4) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrMidX(HSTRING, LONG, LONG); file: centura200.h */
  @Override
public CString SalStrMidX(CString var1, CNumber var2, CNumber var3) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalStrProper(HSTRING, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrProper(CString var1, CString.ByReference var2) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrProperX(HSTRING); file: centura200.h */
  @Override
public CString SalStrProperX(CString var1) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalStrRepeat(HSTRING, INT, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrRepeat(CString var1, CNumber var2, CString.ByReference var3) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrRepeatX(HSTRING, INT); file: centura200.h */
  @Override
public CString SalStrRepeatX(CString var1, CNumber var2) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalStrReplace(HSTRING, INT, INT, HSTRING, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrReplace(CString var1, CNumber var2, CNumber var3, CString var4,
      CString.ByReference var5) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrReplaceX(HSTRING, INT, INT, HSTRING); file: centura200.h */
  @Override
public CString SalStrReplaceX(CString var1, CNumber var2, CNumber var3, CString var4) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalStrRight(HSTRING, LONG, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrRight(CString var1, CNumber var2, CString.ByReference var3) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrRightX(HSTRING, LONG); file: centura200.h */
  @Override
public CString SalStrRightX(CString var1, CNumber var2) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalStrScan(LPSTR, LPSTR); file: centura200.h */
  @Override
public CNumber SalStrScan(CString var1, CString var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalStrSetBufferLength(LPHSTRING, LONG); file: centura200.h */
  @Override
public CBoolean SalStrSetBufferLength(CString.ByReference var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * DATETIME  CBEXPAPI  SalStrToDate(LPSTR); file: centura200.h */
  @Override
public CDateTime SalStrToDate(CString var1) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalStrToNumber(LPSTR); file: centura200.h */
  @Override
public CNumber SalStrToNumber(CString var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalStrTokenize(LPSTR, LPSTR, LPSTR, HARRAY); file: centura200.h */
  @Override
public CNumber SalStrTokenize(CString var1, CString var2, CString var3, CArray<?> var4) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalStrTrim(HSTRING, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrTrim(CString var1, CString.ByReference var2) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrTrimX(HSTRING); file: centura200.h */
  @Override
public CString SalStrTrimX(CString var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalStrUncompress(LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalStrUncompress(CString.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalStrUpper(HSTRING, LPHSTRING); file: centura200.h */
  @Override
public CNumber SalStrUpper(CString var1, CString.ByReference var2) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SalStrUpperX(HSTRING); file: centura200.h */
  @Override
public CString SalStrUpperX(CString var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTBarSetVisible(HWND, BOOL); file: centura200.h */
  @Override
public CBoolean SalTBarSetVisible(CWndHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblAnyRows(HWND, WORD, WORD); file: centura200.h */
  @Override
public CBoolean SalTblAnyRows(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblClearSelection(HWND); file: centura200.h */
  @Override
public CBoolean SalTblClearSelection(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * NUMBER    CBEXPAPI  SalTblColumnAverage(HWND, WORD, WORD, WORD); file: centura200.h */
  @Override
public CNumber SalTblColumnAverage(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return null;
  }

  /**
   * NUMBER    CBEXPAPI  SalTblColumnSum(HWND, WORD, WORD, WORD); file: centura200.h */
  @Override
public CNumber SalTblColumnSum(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblCopyRows(HWND, WORD, WORD); file: centura200.h */
  @Override
public CBoolean SalTblCopyRows(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalTblCreateColumn(HWND, UINT, NUMBER, INT, LPSTR); file: centura200.h */
  @Override
public CNumber SalTblCreateColumn(CWndHandle var1, CNumber var2, CNumber var3, CNumber var4,
      CString var5) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineCheckBoxColumn(HWND, DWORD, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SalTblDefineCheckBoxColumn(CWndHandle var1, CNumber var2, CString var3,
      CString var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineDropDownListColumn(HWND, DWORD, INT); file: centura200.h */
  @Override
public CBoolean SalTblDefineDropDownListColumn(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefinePopupEditColumn(HWND, DWORD, INT); file: centura200.h */
  @Override
public CBoolean SalTblDefinePopupEditColumn(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineRowHeader(HWND, LPSTR, INT, WORD, HWND); file: centura200.h */
  @Override
public CBoolean SalTblDefineRowHeader(CWndHandle var1, CString var2, CNumber var3, CNumber var4,
      CWndHandle var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDefineSplitWindow(HWND, INT, BOOL); file: centura200.h */
  @Override
public CBoolean SalTblDefineSplitWindow(CWndHandle var1, CNumber var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDeleteRow(HWND, LONG, WORD); file: centura200.h */
  @Override
public CBoolean SalTblDeleteRow(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDeleteSelected(HWND, SQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SalTblDeleteSelected(CWndHandle var1, CSQLHandle var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDestroyColumns(HWND); file: centura200.h */
  @Override
public CBoolean SalTblDestroyColumns(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDoDeletes(HWND, SQLHANDLENUMBER, WORD); file: centura200.h */
  @Override
public CBoolean SalTblDoDeletes(CWndHandle var1, CSQLHandle var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDoInserts(HWND, SQLHANDLENUMBER, BOOL); file: centura200.h */
  @Override
public CBoolean SalTblDoInserts(CWndHandle var1, CSQLHandle var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblDoUpdates(HWND, SQLHANDLENUMBER, BOOL); file: centura200.h */
  @Override
public CBoolean SalTblDoUpdates(CWndHandle var1, CSQLHandle var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalTblFetchRow(HWND, LONG); file: centura200.h */
  @Override
public CNumber SalTblFetchRow(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblFindNextRow(HWND, LPLONG, WORD, WORD); file: centura200.h */
  @Override
public CBoolean SalTblFindNextRow(CWndHandle var1, CNumber.ByReference var2, CNumber var3,
      CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblFindPrevRow(HWND, LPLONG, WORD, WORD); file: centura200.h */
  @Override
public CBoolean SalTblFindPrevRow(CWndHandle var1, CNumber.ByReference var2, CNumber var3,
      CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblGetColumnText(HWND, UINT, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalTblGetColumnText(CWndHandle var1, CNumber var2, CString.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalTblGetColumnTitle(HWND, LPHSTRING, INT); file: centura200.h */
  @Override
public CNumber SalTblGetColumnTitle(CWndHandle var1, CString.ByReference var2, CNumber var3) {
    return null;
  }

  /**
   * HWND      CBEXPAPI  SalTblGetColumnWindow(HWND, INT, WORD); file: centura200.h */
  @Override
public CWndHandle SalTblGetColumnWindow(CWndHandle var1, CNumber var2, CNumber var3) {
    return null;
  }

  /**
   * LONG      CBEXPAPI  SalTblInsertRow(HWND, LONG); file: centura200.h */
  @Override
public CNumber SalTblInsertRow(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblKillEdit(HWND); file: centura200.h */
  @Override
public CBoolean SalTblKillEdit(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblKillFocus(HWND); file: centura200.h */
  @Override
public CBoolean SalTblKillFocus(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblObjectsFromPoint(HWND, INT, INT, LPLONG, LPHWND, LPDWORD); file: centura200.h */
  @Override
public CBoolean SalTblObjectsFromPoint(CWndHandle var1, CNumber var2, CNumber var3,
      CNumber.ByReference var4, CWndHandle.ByReference var5, CNumber var6) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblPasteRows(HWND); file: centura200.h */
  @Override
public CBoolean SalTblPasteRows(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblPopulate(HWND, SQLHANDLENUMBER, LPSTR, INT); file: centura200.h */
  @Override
public CBoolean SalTblPopulate(CWndHandle var1, CSQLHandle var2, CString var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryCheckBoxColumn(HWND, LPDWORD, LPHSTRING, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalTblQueryCheckBoxColumn(CWndHandle var1, CNumber var2, CString.ByReference var3,
      CString.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnCellType(HWND, LPINT); file: centura200.h */
  @Override
public CBoolean SalTblQueryColumnCellType(CWndHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnFlags(HWND, DWORD); file: centura200.h */
  @Override
public CBoolean SalTblQueryColumnFlags(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalTblQueryColumnID(HWND); file: centura200.h */
  @Override
public CNumber SalTblQueryColumnID(CWndHandle var1) {
    return null;
  }

  /**
   * INT       CBEXPAPI  SalTblQueryColumnPos(HWND); file: centura200.h */
  @Override
public CNumber SalTblQueryColumnPos(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryColumnWidth(HWND, LPNUMBER); file: centura200.h */
  @Override
public CBoolean SalTblQueryColumnWidth(CWndHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalTblQueryContext(HWND); file: centura200.h */
  @Override
public CNumber SalTblQueryContext(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryDropDownListColumn(HWND, LPDWORD, LPINT); file: centura200.h */
  @Override
public CBoolean SalTblQueryDropDownListColumn(CWndHandle var1, CNumber var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryFocus(HWND, LPLONG, LPHWND); file: centura200.h */
  @Override
public CBoolean SalTblQueryFocus(CWndHandle var1, CNumber.ByReference var2,
      CWndHandle.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryLinesPerRow(HWND, LPINT); file: centura200.h */
  @Override
public CBoolean SalTblQueryLinesPerRow(CWndHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SalTblQueryLockedColumns(HWND); file: centura200.h */
  @Override
public CNumber SalTblQueryLockedColumns(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryPopupEditColumn(HWND, LPDWORD, LPINT); file: centura200.h */
  @Override
public CBoolean SalTblQueryPopupEditColumn(CWndHandle var1, CNumber var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryRowFlags(HWND, LONG, WORD); file: centura200.h */
  @Override
public CBoolean SalTblQueryRowFlags(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryRowHeader(HWND, LPHSTRING, INT, LPINT, LPWORD, LPHWND); file: centura200.h */
  @Override
public CBoolean SalTblQueryRowHeader(CWndHandle var1, CString.ByReference var2, CNumber var3,
      CNumber.ByReference var4, CNumber.ByReference var5, CWndHandle.ByReference var6) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryScroll(HWND, LPLONG, LPLONG, LPLONG); file: centura200.h */
  @Override
public CBoolean SalTblQueryScroll(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3, CNumber.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQuerySplitWindow(HWND, LPINT, LPBOOL); file: centura200.h */
  @Override
public CBoolean SalTblQuerySplitWindow(CWndHandle var1, CNumber.ByReference var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryTableFlags(HWND, WORD); file: centura200.h */
  @Override
public CBoolean SalTblQueryTableFlags(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblQueryVisibleRange(HWND, LPLONG, LPLONG); file: centura200.h */
  @Override
public CBoolean SalTblQueryVisibleRange(CWndHandle var1, CNumber.ByReference var2,
      CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblReset(HWND); file: centura200.h */
  @Override
public CBoolean SalTblReset(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblScroll(HWND, LONG, HWND, WORD); file: centura200.h */
  @Override
public CBoolean SalTblScroll(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetCellTextColor(HWND, DWORD, BOOL); file: centura200.h */
  @Override
public CBoolean SalTblSetCellTextColor(CWndHandle var1, CNumber var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnFlags(HWND, DWORD, BOOL); file: centura200.h */
  @Override
public CBoolean SalTblSetColumnFlags(CWndHandle var1, CNumber var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnPos(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalTblSetColumnPos(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnText(HWND, UINT, LPSTR); file: centura200.h */
  @Override
public CBoolean SalTblSetColumnText(CWndHandle var1, CNumber var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnTitle(HWND, LPSTR); file: centura200.h */
  @Override
public CBoolean SalTblSetColumnTitle(CWndHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetColumnWidth(HWND, NUMBER); file: centura200.h */
  @Override
public CBoolean SalTblSetColumnWidth(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetContext(HWND, LONG); file: centura200.h */
  @Override
public CBoolean SalTblSetContext(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetFlagsAnyRows(HWND, WORD, BOOL, WORD, WORD); file: centura200.h */
  @Override
public CBoolean SalTblSetFlagsAnyRows(CWndHandle var1, CNumber var2, CBoolean var3, CNumber var4,
      CNumber var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetFocusCell(HWND, LONG, HWND, INT, INT); file: centura200.h */
  @Override
public CBoolean SalTblSetFocusCell(CWndHandle var1, CNumber var2, CWndHandle var3, CNumber var4,
      CNumber var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetFocusRow(HWND, LONG); file: centura200.h */
  @Override
public CBoolean SalTblSetFocusRow(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetLinesPerRow(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalTblSetLinesPerRow(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetLockedColumns(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalTblSetLockedColumns(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetRange(HWND, LONG, LONG); file: centura200.h */
  @Override
public CBoolean SalTblSetRange(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * LONG      CBEXPAPI  SalTblSetRow(HWND, INT); file: centura200.h */
  @Override
public CNumber SalTblSetRow(CWndHandle var1, CNumber var2) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetRowFlags(HWND, LONG, WORD, BOOL); file: centura200.h */
  @Override
public CBoolean SalTblSetRowFlags(CWndHandle var1, CNumber var2, CNumber var3, CBoolean var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSetTableFlags(HWND, WORD, BOOL); file: centura200.h */
  @Override
public CBoolean SalTblSetTableFlags(CWndHandle var1, CNumber var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTblSortRows(HWND, WORD, INT); file: centura200.h */
  @Override
public CBoolean SalTblSortRows(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTimerKill(HWND, INT); file: centura200.h */
  @Override
public CBoolean SalTimerKill(CWndHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTimerSet(HWND, INT, WORD); file: centura200.h */
  @Override
public CBoolean SalTimerSet(CWndHandle var1, CNumber var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalTrackPopupMenu(HWND, LPSTR, WORD, INT, INT); file: centura200.h */
  @Override
public CBoolean SalTrackPopupMenu(CWndHandle var1, CString var2, CNumber var3, CNumber var4,
      CNumber var5) {
    return CBoolean.FALSE;
  }

  /**
   * HUDV CBEXPAPI SalUdvGetCurrentHandle(void); file: centura200.h */
  @Override
public CHandle SalUdvGetCurrentHandle() {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalUpdateWindow(HWND); file: centura200.h */
  @Override
public CBoolean SalUpdateWindow(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalUseRegistry(BOOL, LPSTR); file: centura200.h */
  @Override
public CBoolean SalUseRegistry(CBoolean var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalValidateSet(HWND, BOOL, LONG); file: centura200.h */
  @Override
public CBoolean SalValidateSet(CWndHandle var1, CBoolean var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalWaitCursor(BOOL); file: centura200.h */
  @Override
public CBoolean SalWaitCursor(CBoolean var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalWinHelp(HWND, LPSTR, WORD, DWORD, LPSTR); file: centura200.h */
  @Override
public CBoolean SalWinHelp(CWndHandle var1, CString var2, CNumber var3, CNumber var4,
      CString var5) {
    return CBoolean.FALSE;
  }

  /**
   * HSTRING   CBEXPAPI  SalWindowClassName(HWND); file: centura200.h */
  @Override
public CString SalWindowClassName(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalWindowGetProperty(HWND, LPSTR, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SalWindowGetProperty(CWndHandle var1, CString var2, CString.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * UINT      CBEXPAPI  SalWindowHandleToNumber(HWND); file: centura200.h */
  @Override
public CNumber SalWindowHandleToNumber(CWndHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SalWindowIsDerivedFromClass(HWND, TEMPLATE); file: centura200.h */
  @Override
public CBoolean SalWindowIsDerivedFromClass(CWndHandle var1, CStruct var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalYieldEnable(BOOL); file: centura200.h */
  @Override
public CBoolean SalYieldEnable(CBoolean var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalYieldQueryState(VOID); file: centura200.h */
  @Override
public CBoolean SalYieldQueryState() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalYieldStartMessages(HWND); file: centura200.h */
  @Override
public CBoolean SalYieldStartMessages(CWndHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SalYieldStopMessages(VOID); file: centura200.h */
  @Override
public CBoolean SalYieldStopMessages() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlClearImmediate(VOID); file: centura200.h */
  @Override
public CBoolean SqlClearImmediate() {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlClose(SQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlClose(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlCloseAllSPResultSets(SQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlCloseAllSPResultSets(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlCommit(SQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlCommit(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlCommitSession(SESSIONHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlCommitSession(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlConnect(LPSQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlConnect(CSQLHandle.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlConnectTransaction(LPSQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlConnectTransaction(CSQLHandle.ByReference var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlCreateSession(LPSESSIONHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlCreateSession(CSQLHandle.ByReference var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlCreateStatement(SESSIONHANDLENUMBER, LPSQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlCreateStatement(CSQLHandle var1, CSQLHandle.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlDirectoryByName(LPSTR, HARRAY); file: centura200.h */
  @Override
public CBoolean SqlDirectoryByName(CString var1, CArray<?> var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlDisconnect(LPSQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlDisconnect(CSQLHandle.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlDropStoredCmd(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlDropStoredCmd(CSQLHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * INT       CBEXPAPI  SqlError(SQLHANDLENUMBER); file: centura200.h */
  @Override
public CNumber SqlError(CSQLHandle var1) {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SqlErrorText(INT, INT, LPHSTRING, INT, LPINT); file: centura200.h */
  @Override
public CBoolean SqlErrorText(CNumber var1, CNumber var2, CString.ByReference var3, CNumber var4,
      CNumber.ByReference var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlExecute(SQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlExecute(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlExecutionPlan(SQLHANDLENUMBER, LPHSTRING, INT); file: centura200.h */
  @Override
public CBoolean SqlExecutionPlan(CSQLHandle var1, CString.ByReference var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlExists(LPSTR, LPINT); file: centura200.h */
  @Override
public CBoolean SqlExists(CString var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlExtractArgs(WPARAM, LPARAM, LPSQLHANDLENUMBER, LPINT, LPINT); file: centura200.h */
  @Override
public CBoolean SqlExtractArgs(CNumber var1, CNumber var2, CSQLHandle.ByReference var3,
      CNumber.ByReference var4, CNumber.ByReference var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlFetchNext(SQLHANDLENUMBER, LPINT); file: centura200.h */
  @Override
public CBoolean SqlFetchNext(CSQLHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlFetchPrevious(SQLHANDLENUMBER, LPINT); file: centura200.h */
  @Override
public CBoolean SqlFetchPrevious(CSQLHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlFetchRow(SQLHANDLENUMBER, LONG, LPINT); file: centura200.h */
  @Override
public CBoolean SqlFetchRow(CSQLHandle var1, CNumber var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlFreeSession(LPSESSIONHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlFreeSession(CSQLHandle.ByReference var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetCmdOrRowsetPtr(SQLHANDLENUMBER, BOOL, LPLONG); file: centura200.h */
  @Override
public CBoolean SqlGetCmdOrRowsetPtr(CSQLHandle var1, CBoolean var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetDSOrSessionPtr(SESSIONHANDLENUMBER, BOOL, LPLONG); file: centura200.h */
  @Override
public CBoolean SqlGetDSOrSessionPtr(CSQLHandle var1, CBoolean var2, CNumber.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetError(SQLHANDLENUMBER, LPLONG, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SqlGetError(CSQLHandle var1, CNumber.ByReference var2, CString.ByReference var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetErrorPosition(SQLHANDLENUMBER, LPINT); file: centura200.h */
  @Override
public CBoolean SqlGetErrorPosition(CSQLHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetErrorText(INT, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SqlGetErrorText(CNumber var1, CString.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * HSTRING   CBEXPAPI  SqlGetErrorTextX(INT); file: centura200.h */
  @Override
public CString SqlGetErrorTextX(CNumber var1) {
    return null;
  }

  /**
   * HSTRING   CBEXPAPI  SqlGetLastStatement(VOID); file: centura200.h */
  @Override
public CString SqlGetLastStatement() {
    return null;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetModifiedRows(SQLHANDLENUMBER, LPLONG); file: centura200.h */
  @Override
public CBoolean SqlGetModifiedRows(CSQLHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetNextSPResultSet(SQLHANDLENUMBER, LPSTR, LPBOOL); file: centura200.h */
  @Override
public CBoolean SqlGetNextSPResultSet(CSQLHandle var1, CString var2, CBoolean var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetParameter(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SqlGetParameter(CSQLHandle var1, CNumber var2, CNumber.ByReference var3,
      CString.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetParameterAll(SQLHANDLENUMBER, WORD, LPLONG, LPHSTRING, BOOL); file: centura200.h */
  @Override
public CBoolean SqlGetParameterAll(CSQLHandle var1, CNumber var2, CNumber.ByReference var3,
      CString.ByReference var4, CBoolean var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetResultSetCount(SQLHANDLENUMBER, LPLONG); file: centura200.h */
  @Override
public CBoolean SqlGetResultSetCount(CSQLHandle var1, CNumber.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetRollbackFlag(SQLHANDLENUMBER, LPBOOL); file: centura200.h */
  @Override
public CBoolean SqlGetRollbackFlag(CSQLHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetSessionErrorInfo(SESSIONHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SqlGetSessionErrorInfo(CSQLHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CString.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetSessionHandle(SQLHANDLENUMBER, LPSESSIONHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlGetSessionHandle(CSQLHandle var1, CSQLHandle.ByReference var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetSessionParameter(SESSIONHANDLENUMBER, WORD, LPLONG, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SqlGetSessionParameter(CSQLHandle var1, CNumber var2, CNumber.ByReference var3,
      CString.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlGetStatementErrorInfo(SQLHANDLENUMBER, LPLONG, LPHSTRING, LPHSTRING); file: centura200.h */
  @Override
public CBoolean SqlGetStatementErrorInfo(CSQLHandle var1, CNumber.ByReference var2,
      CString.ByReference var3, CString.ByReference var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlImmediate(LPSTR); file: centura200.h */
  @Override
public CBoolean SqlImmediate(CString var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlOpen(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlOpen(CSQLHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLExecute(SQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlOraPLSQLExecute(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLPrepare(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlOraPLSQLPrepare(CSQLHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlOraPLSQLStringBindType(SQLHANDLENUMBER, LPSTR, INT); file: centura200.h */
  @Override
public CBoolean SqlOraPLSQLStringBindType(CSQLHandle var1, CString var2, CNumber var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlPLSQLCommand(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlPLSQLCommand(CSQLHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlPrepare(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlPrepare(CSQLHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlPrepareAndExecute(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlPrepareAndExecute(CSQLHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlPrepareSP(SQLHANDLENUMBER, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlPrepareSP(CSQLHandle var1, CString var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlRetrieve(SQLHANDLENUMBER, LPSTR, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlRetrieve(CSQLHandle var1, CString var2, CString var3, CString var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlRollbackSession(SESSIONHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlRollbackSession(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetInMessage(SQLHANDLENUMBER, INT); file: centura200.h */
  @Override
public CBoolean SqlSetInMessage(CSQLHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetIsolationLevel(SQLHANDLENUMBER, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlSetIsolationLevel(CSQLHandle var1, CString var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetLockTimeout(SQLHANDLENUMBER, INT); file: centura200.h */
  @Override
public CBoolean SqlSetLockTimeout(CSQLHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetLongBindDatatype(INT, INT); file: centura200.h */
  @Override
public CBoolean SqlSetLongBindDatatype(CNumber var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetOutMessage(SQLHANDLENUMBER, INT); file: centura200.h */
  @Override
public CBoolean SqlSetOutMessage(CSQLHandle var1, CNumber var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetParameter(SQLHANDLENUMBER, WORD, LONG, HSTRING); file: centura200.h */
  @Override
public CBoolean SqlSetParameter(CSQLHandle var1, CNumber var2, CNumber var3, CString var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetParameterAll(SQLHANDLENUMBER, WORD, LONG, HSTRING, BOOL); file: centura200.h */
  @Override
public CBoolean SqlSetParameterAll(CSQLHandle var1, CNumber var2, CNumber var3, CString var4,
      CBoolean var5) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetResultSet(SQLHANDLENUMBER, BOOL); file: centura200.h */
  @Override
public CBoolean SqlSetResultSet(CSQLHandle var1, CBoolean var2) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlSetSessionParameter(SESSIONHANDLENUMBER, WORD, LONG, HSTRING); file: centura200.h */
  @Override
public CBoolean SqlSetSessionParameter(CSQLHandle var1, CNumber var2, CNumber var3,
      CString var4) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlStore(SQLHANDLENUMBER, LPSTR, LPSTR); file: centura200.h */
  @Override
public CBoolean SqlStore(CSQLHandle var1, CString var2, CString var3) {
    return CBoolean.FALSE;
  }

  /**
   * BOOL      CBEXPAPI  SqlVarSetup(SQLHANDLENUMBER); file: centura200.h */
  @Override
public CBoolean SqlVarSetup(CSQLHandle var1) {
    return CBoolean.FALSE;
  }

@Override
public CBoolean SalPause(CNumber var1) {
	return CBoolean.FALSE;
}
}
