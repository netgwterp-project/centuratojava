package com.centura.api.teste;

import static com.centura.api.cdk.CenturaAPI.*;

import com.centura.api.type.CBoolean;
import com.centura.api.type.CDateTime;
import com.centura.api.type.CFile;
import com.centura.api.type.CNumber;
import com.centura.api.type.CString;
import com.centura.api.type.CWndHandle;

public final class Teste {
  public static final Integer ID_TESTE = 0x00008;

  public static final String NAME_TESTE = "HILTON \"BONITO\"";

  public static final Boolean SE_TESTE = true;

  public static CString PegaTeste(CDateTime dtData, CString.ByReference lsData, CFile hFile) {
    return PegaTesteFunction.PegaTeste(dtData,lsData,hFile);
  }

  public static CNumber PegaTesteNumber() {
    return CNumber.wrapper( -1 );
  }

  static class PegaTesteFunction {
    public static CNumber nI;

    public static CString sTeste;

    public static CString PegaTeste(CDateTime dtData, CString.ByReference lsData, CFile hFile) {
      CWndHandle hWndF = new CWndHandle();
      if( CBoolean.wrapper( SalStrIsValidNumber( sTeste ) ).get() ){
        return CString.wrapper( "NADA" );
      }
      dtData.set( SalDateCurrent() );
      return CString.wrapper( "-1" );
    }
  }
}
