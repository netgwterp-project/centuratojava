package com.centura.api.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.centura.dll.DATETIME;
import com.centura.dll.cdlli20.Cdlli20Library;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.NativeLongByReference;

public class NativeCenturaUtils {

	public static Date toDate(DATETIME.ByValue datetime) {
		int ano = Cdlli20Library.INSTANCE.SalDateYear(datetime);
		int mes = Cdlli20Library.INSTANCE.SalDateMonth(datetime);
		int dia = Cdlli20Library.INSTANCE.SalDateDay(datetime);
		int hora = Cdlli20Library.INSTANCE.SalDateHour(datetime);
		int minuto = Cdlli20Library.INSTANCE.SalDateMinute(datetime);
		int segundo = Cdlli20Library.INSTANCE.SalDateSecond(datetime);
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(ano, mes-1, dia, hora, minuto, segundo);
		return calendar.getTime();
	}

	public static Date toDate(DATETIME datetime) {
		return toDate((DATETIME.ByValue)datetime);
	}

	public static DATETIME toDATETIME(Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		int ano = calendar.get(Calendar.YEAR);
		int mes = calendar.get(Calendar.MONTH);
		int dia = calendar.get(Calendar.DAY_OF_MONTH);
		int hora = calendar.get(Calendar.HOUR_OF_DAY);
		int minuto = calendar.get(Calendar.MINUTE);
		int segundo = calendar.get(Calendar.SECOND);
		return Cdlli20Library.INSTANCE.SalDateConstruct(ano, mes, dia, hora, minuto, segundo);
	}

	public static NativeLong toHSTRING(String word) {
		NativeLong HSTRING = null;
		NativeLong SIZE = new NativeLong(word.length()+1);
		NativeLongByReference LPHSTRING = new NativeLongByReference();
		Cdlli20Library.INSTANCE.SWinInitLPHSTRINGParam(LPHSTRING, SIZE);
		HSTRING = LPHSTRING.getValue();
		NativeLongByReference LPSIZE = new NativeLongByReference();
		Pointer buffer = Cdlli20Library.INSTANCE.SWinStringGetBuffer(HSTRING, LPSIZE);
		buffer.setString(0, word);
		return HSTRING;
	}
	
	public static NativeLong toHSTRING(int size) {
		NativeLong HSTRING = null;
		NativeLong SIZE = new NativeLong(size+1);
		NativeLongByReference LPHSTRING = new NativeLongByReference();
		Cdlli20Library.INSTANCE.SWinInitLPHSTRINGParam(LPHSTRING, SIZE);
		HSTRING = LPHSTRING.getValue();
		return HSTRING;
	}

	public static String toString(NativeLong HSTRING) {
		NativeLongByReference LPSIZEE = new NativeLongByReference();
		Pointer buffer = Cdlli20Library.INSTANCE.SWinStringGetBuffer(HSTRING, LPSIZEE);
		return buffer.getString(0);
	}

}
