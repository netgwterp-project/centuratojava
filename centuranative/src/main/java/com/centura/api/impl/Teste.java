package com.centura.api.impl;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import com.centura.dll.DATETIME;
import com.centura.dll.cdlli20.Cdlli20Library;
import com.centura.dll.vti20.Vti20Library;
import com.google.common.primitives.Longs;
import com.sun.jna.Native;
import com.sun.jna.ptr.NativeLongByReference;

public class Teste {

	public static void main(String[] args) {
		Native.setProtected(true);
		//Cdlli20Library.INSTANCE.SalMessageBeep();
		//System.out.println(testeDateCurrentDate());
		//System.out.println(testeDate(2018, 2, 13, 18, 0, 0));
		//System.out.println(testeDate(2018, 1, 1, 12, 0, 0));
		//NUMBER number = new NUMBER.ByReference();
		//Cdlli20Library.INSTANCE.SWinCvtIntToNumber(16, number);
		//NUMBER.ByValue ret = Cdlli20Library.INSTANCE.SalNumberSqrt(number);
		//System.out.println(ret);
		//DoubleBuffer allocate = DoubleBuffer.allocate(5);
		//Cdlli20Library.INSTANCE.SWinCvtNumberToDouble(ret, allocate);
		//System.out.println("" + allocate.get());
		//ByteBuffer byteBuffer = ByteBuffer.allocateDirect(6);
		Cdlli20Library.INSTANCE.SWinHSCreateDesignHeap();
		System.out.println(NativeCenturaUtils.toString(Vti20Library.INSTANCE.VisGetVersion()));
		String source = "TESTE      ";
		NativeLongByReference dateToStr = new NativeLongByReference();
		Cdlli20Library.INSTANCE.SalDateToStr(Cdlli20Library.INSTANCE.SalDateCurrent(), dateToStr);
		String dest = NativeCenturaUtils.toString(dateToStr.getValue());
		System.out.println(source + " s:" + source.length() + " - " + dest + " s:" + dest.length() );
		Cdlli20Library.INSTANCE.SWinHSDestroyDesignHeap();
		//NativeLong nativeLong2 = new NativeLong();
		//nativeLongByReference.setPointer(memory);
		//System.out.println(nativeLongByReference.getPointer());
		//System.out.println(nativeLongByReference);
		//boolean length = Cdlli20Library.INSTANCE.SWinInitLPHSTRINGParam(nativeLongByReference, nativeLong);
		//System.out.println(length);
		//Cdlli20Library.INSTANCE.SalStrTrimX(nativeLongByReference.getValue());
		//int salNumberToStrX = Cdlli20Library.INSTANCE.SalNumberToStrX(ret, 3);
		//System.out.println(salNumberToStrX);
		//int current = Cdlli20Library.SalDateCurrent();
		//Memory memory = new Memory(teste.length()+1);
		//memory.nativeValue(pointerToString);
		/*Pointer<Byte> pointerToString = Pointer.pointerToCString(teste);
		long visStrTrim = Vti20Library.VisStrLeftTrim(pointerToString);
		Pointer<CLong> pointerToCLong = Pointer.pointerToCLong(visStrTrim);
		String teste2 = pointerToCLong.getCString();*/
		//System.out.println(current);
		/*String teste = " TESTE ";
		NativeLong hstring = new NativeLong(0);
		NativeLong hsize = new NativeLong(200l);
		if(Cdlli20Library.SWinInitLPHSTRINGParam(new NativeLongByReference(hstring), hsize))
			System.out.println(teste);*/

	}

	public static String testeDateCurrentDate() {
		Calendar calendar = GregorianCalendar.getInstance();
		Date curr = calendar.getTime();
		long time = curr.getTime();
		DATETIME.ByValue salDateConstruct = Cdlli20Library.INSTANCE.SalDateCurrent();
		Date dateC = NativeCenturaUtils.toDate(salDateConstruct);
		if(DateUtils.isSameDay(curr, dateC))
			return formatTeste(calendar, time, salDateConstruct);
		else
			return "erro";
	}

	public static String testeDate(int ano, int mes, int dia, int hora, int minuto, int segundo){
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(ano + 1900, mes-1, dia, hora, minuto, segundo);
		long time = calendar.getTime().getTime();
		DATETIME.ByValue salDateConstruct = Cdlli20Library.INSTANCE.SalDateConstruct(ano, mes, dia, hora, minuto, segundo);
		return formatTeste(calendar, time, salDateConstruct);
	}

	public static String formatTeste(Calendar calendar, long time, DATETIME.ByValue salDateConstruct) {
		int salDateYear = Cdlli20Library.INSTANCE.SalDateYear(salDateConstruct);
		int salDateMonth = Cdlli20Library.INSTANCE.SalDateMonth(salDateConstruct);
		int salDateDay = Cdlli20Library.INSTANCE.SalDateDay(salDateConstruct);
		int salDateHour = Cdlli20Library.INSTANCE.SalDateHour(salDateConstruct);
		int salDateMinute = Cdlli20Library.INSTANCE.SalDateMinute(salDateConstruct);
		int salDateSecond = Cdlli20Library.INSTANCE.SalDateSecond(salDateConstruct);
		return "CENTURA DATE: " + StringUtils.leftPad(""+salDateDay,2,'0') + "/" + StringUtils.leftPad(""+salDateMonth,2,'0') + "/" + salDateYear + " " + StringUtils.leftPad(""+salDateHour,2,'0') + ":" + StringUtils.leftPad(""+salDateMinute,2,'0') + ":" + StringUtils.leftPad(""+salDateSecond,2,'0') + "  JAVA DATE: " + SimpleDateFormat.getInstance().format(calendar.getTime());
	}

	public static String getHexString(DATETIME salDateConstruct){
		return getHexString(salDateConstruct.DATETIME_Buffer,Byte.toUnsignedInt(salDateConstruct.DATETIME_Length),0,true);
	}

	public static String getHexString(byte[] buffer, int length, int precision, boolean signed){
		long fromByteArray = Longs.fromByteArray(Arrays.copyOfRange(buffer, 0, 8));
		return StringUtils.rightPad(Long.toUnsignedString(fromByteArray),30) + Long.toHexString(fromByteArray);
		//StringBuilder str = new StringBuilder();
/*
		int offset = 0;
		try {
			for (int i = 0; i < length; i++) {
				if (precision != 0 && length - i == precision) {
					str.append('.');
				}

				str.append(StringUtils.leftPad(Integer.toHexString(buffer[offset + i] & 0xff),2,'0'));
			}

			if (str.length() == 0) {
				return "";
			}

			return str.toString();
		} catch (NumberFormatException ex) {
			return null;
		}*/
	}

}
