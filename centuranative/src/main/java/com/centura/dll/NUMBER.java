package com.centura.dll;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;
/**
 * <i>native declaration : number.h</i><br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class NUMBER extends Structure {
	/** size of number */
	public byte numLength;
	/**
	 * value of number<br>
	 * C type : BYTE[24]
	 */
	public byte[] numValue = new byte[24];
	public NUMBER() {
		super();
	}
	protected List<String> getFieldOrder() {
		return Arrays.asList("numLength", "numValue");
	}
	/**
	 * @param numLength size of number<br>
	 * @param numValue value of number<br>
	 * C type : BYTE[24]
	 */
	public NUMBER(byte numLength, byte numValue[]) {
		super();
		this.numLength = numLength;
		if ((numValue.length != this.numValue.length))
			throw new IllegalArgumentException("Wrong array size !");
		this.numValue = numValue;
	}
	public NUMBER(Pointer peer) {
		super(peer);
	}
	public static class ByReference extends NUMBER implements Structure.ByReference {

	};
	public static class ByValue extends NUMBER implements Structure.ByValue {

	};
}
